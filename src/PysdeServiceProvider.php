<?php

namespace Pasifai\Pysde;

use Pasifai\Pysde\commands\statistics;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;
use Pasifai\Pysde\commands\CheckAdeiesLeitourgika;
use Pasifai\Pysde\commands\InsertAnaplirwtes;
use Pasifai\Pysde\commands\LockLeitourgikaEidikotites;
use Pasifai\Pysde\commands\LockLeitourgikaTeachers;
use Pasifai\Pysde\commands\UpdateMySchoolFromEdata;
use Pasifai\Pysde\commands\UpdateNewOrganikes;
use Pasifai\Pysde\commands\UpdateOrganikaSumHours;
use Pasifai\Pysde\commands\UpdateProfileFromEdata;

class PysdeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->loadViewsFrom(__DIR__.'/views', 'pysde');

        $configPath = __DIR__ . '/../config/pysde.php';
        $publishPath = base_path('config/pysde.php');

        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/pasifai/pysde'),
            $configPath => $publishPath
        ]);

        $this->publishes([
            __DIR__.'/assets' => resource_path('assets/vendor/pysde'),
        ], 'vue-components');

        $this->publishes([
            __DIR__.'/published' => public_path('vendor/pysde'),
        ], 'public');

        $this->commands([
            \Pasifai\Pysde\commands\LessonsInput::class,
            // \Pasifai\Pysde\commands\InsertEidikotites::class,
            \Pasifai\Pysde\commands\InsertOrganikes::class,
            \Pasifai\Pysde\commands\InsertOrganikesEidikis::class,
            \Pasifai\Pysde\commands\PinakasAalgorith::class,
            \Pasifai\Pysde\commands\InsertGenikis::class,
            \Pasifai\Pysde\commands\InsertGenikisLyceum::class,
            \Pasifai\Pysde\commands\UpdateFromVeltiwseis::class,
            \Pasifai\Pysde\commands\CopyOrganikaToLeitourgika::class,
            \Pasifai\Pysde\commands\UpdateNumberOfTeachersLeitourgika::class,
            \Pasifai\Pysde\commands\UpdateProfileUserFromEdata::class,
            \Pasifai\Pysde\commands\CalculateApospasmenoiEntos::class,
            \Pasifai\Pysde\commands\LeitourgikaRemoveAnapliroteAndUpdateLeitourgika::class,
            CheckAdeiesLeitourgika::class,
            LockLeitourgikaEidikotites::class,
            LockLeitourgikaTeachers::class,
            InsertAnaplirwtes::class,
            UpdateProfileFromEdata::class,
            UpdateNewOrganikes::class,
            statistics::class,
            UpdateOrganikaSumHours::class,
            UpdateMySchoolFromEdata::class,
        ]);

        $this->app->booted(function(){
            $schedule = $this->app->make(Schedule::class);
            $schedule->command('leitourgika:adeies')->dailyAt('03:40');
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Pasifai\Pysde\repositories\PlacementRepositoryInterface',
            'Pasifai\Pysde\repositories\PlacementRepository'
        );

        $configPath = __DIR__ . '/../config/pysde.php';

        $this->mergeConfigFrom($configPath, 'requests');

        include __DIR__.'/routes/web.php';
        include __DIR__.'/routes/api.php';
//        $this->app->make('Pasifai\Pysde\controllers\PysdeController');
    }
}
