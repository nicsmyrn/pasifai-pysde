<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use Pasifai\Pysde\commands\traits\AlgorithmTrait;
class PinakasAalgorith extends Command {

    use AlgorithmTrait;

    // artisan kena:pinakas_a
    protected $signature = 'kena:pinakas_a {type} {one_school} {sch_id?}';

    protected $description = 'Command description';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $this->oneOrAllschools($this->argument('type'));
                
        foreach($this->schools as $school){
            
            // $this->array_A = [];
            // $this->organikes_list = [];
            // $this->divisions = $school->divisions; 

            // $this->addTeachersToArrayA($school); // ΠΡΟΣΘΕΤΕΙ ΤΙΣ ΕΙΔΙΚΟΤΗΤΕΣ ΣΤΟΝ ΠΙΝΑΚΑ Α ΠΟΥ ΕΧΟΥΝ ΟΡΓΑΝΙΚΗ ΣΤΟ ΣΧΟΛΕΙΟ

            // if(!$this->divisions->isEmpty()){
            
            //     $this->executeOneAnathesi($school); // 1. ΜΟΝΟ ΤΑ ΜΑΘΗΜΑΤΑ ΜΕ 1 (ΜΙΑ) ΑΝΑΘΕΣΗ
            
            //     $this->executeManyAnatheseis($school); // 2. ΤΑ ΜΑΘΗΜΑΤΑ ΜΕ ΠΟΛΛΕΣ ΑΝΑΘΕΣΕΙΣ

            //     $this->executeProjectAnathesis($school); // 3. ΤΑ PROJECT ΓΙΑ ΛΥΚΕΙΑ - ΕΠΑΛ
            // }

            $this->executePe04Summary($school);

            // $this->displayArrayC();
            // $this->displayArrayA();
            
            // $this->comment("\n Ο Αλγόριθμος εκτελέστηκε με επιτυχία...");
        
        } // end foreach school
    }
}