<?php

namespace Pasifai\Pysde\commands;

use App\Teacher;
use Illuminate\Console\Command;
use Pasifai\Pysde\models\OrganikiRequest;
use Pasifai\Pysde\models\Placement;

class statistics extends Command
{

    protected $types = [
        [
            'type' => 'E1',
            'satisfied' => 0,
            'not_satisfied' => 0
        ],
        [
            'type' => 'E2',
            'satisfied' => 0,
            'not_satisfied' => 0
        ],
        [
            'type' => 'E3',
            'satisfied' => 0,
            'not_satisfied' => 0
        ]
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statistics:leitourgika';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Information about placements & requests of teachers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $total = OrganikiRequest::with('prefrences')
            ->where('recall_situation', null)
            ->count(); 

        $bar = $this->output->createProgressBar($total);

        foreach($this->types as $type){

            $requests = OrganikiRequest::with('prefrences')
                ->where('aitisi_type', $type)
                ->where('recall_situation', null)
                ->get();

            foreach($requests as $key=>$request){

                $teacher = Teacher::with('placements.praxi')->where('id', $request['teacher_id'])->get();


                dd($teacher);

                dd($request->prefrences->pluck('school_id'));

                $bar->advance();

            }
        }

        $bar->finish();


    }
}
