<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\Models\Edata;
use App\MySchoolTeacher;
use App\School;
use App\Teacher;
use DB;
use Pasifai\Pysde\models\Organiki;

class UpdateMySchoolFromEdata extends Command
{
    protected $userCounter = 0;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'myschool:update_organikes_to_myschool';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update organikes to myschool from _kena_organikes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $organikes = Organiki::where('created_at', '2023-07-25 00:00:00')->get();

        $updated = 0;

        foreach($organikes as $organiki){
            $myschool =    MySchoolTeacher::where('afm', $organiki['afm'])->first();
            
            if($myschool != null){
                $school = School::find($organiki['sch_id']);

                if($school != null){
                    $myschool->topothetisi = 'Οργανικά';
                    $myschool->organiki_prosorini = $school->identifier;
                    $myschool->type = 'ΓΕΝΙΚΗΣ';

                    $myschool->save();
                    $updated++;
                }else{
                    dd('error 24084');
                }
            }else{
                dd('error 2324');
            }
        }

        $this->comment("Done... Updated : $updated");

    }
}
