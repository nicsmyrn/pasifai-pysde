<?php

namespace Pasifai\Pysde\commands\traits;

use App\NewEidikotita;
use App\School;
use Pasifai\Pysde\models\Division;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AlgorithmTrait
{
    protected $update = true;
    protected $temp_school_id = 50;  // 37 1ο Γσιο, 46 6γσιο, 39gym neas kyd, 25 3gym, 30 Γ/σιο Σούδας
    protected $temp_lesson = 'Μαθηματικά';
    protected $temp_eidikotita = 'ΠΕ02';

    protected $globalDifferenceForManyEidikotites = 0;

    protected $array_C; 
    protected $array_B = [];
    protected $sch_id;
    protected $array_A = [];
    protected $organikes_list = [];

    protected $hours;
    protected $eidikotites;

    protected $schools;
    protected $divisions;
    protected $eidikotites_list;
    protected $hoursTakenByOtherEidikotita;
    protected $array_of_intersect;
    protected $countDiffererentEidikotitesHasLessonAnathesi;
    protected $sum;
    protected $all_diff;
    protected $each_diff;
    protected $mod_each_diff;
    protected $flag_special;
    protected $flag_special_Second;
    protected $diference;
    protected $total;
    protected $count;
    protected $temp;

    protected $lesson_name = '';
    protected $division_class = '';
    protected $division_name = '';

    protected $oligomeli = [];

    protected $testtest = 0;

    private function executeAlgorithm($school, $onlyOneAnathesi)
    {
        foreach($this->divisions as $division){                       // ΓΙΑ ΚΑΘΕ ΤΜΗΜΑ ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ ΚΑΤΕΥΘΥΝΣΗΣ ΚΤΛ...
            $numberOfDivision = $division->pivot->number;

            foreach($division->wrologio as $lesson){            // ΓΙΑ ΚΑΘΕ ΜΑΘΗΜΑ TOY ΤΜΗΜΑΤΟΣ ΠΧ Α ΤΑΞΗ ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ (ΘΡΗΣΚΕΥΤΙΚΑ, ΜΑΘΗΜΑΤ ...)

                $this->initializeProperties($lesson, $numberOfDivision, $division->class, $division->name);

                if($onlyOneAnathesi){
                    if ($this->eidikotites->count() == 1){          // ΑΝ ΟΙ ΕΙΔΙΚΟΤΗΤΕΣ ΠΟΥ ΕΧΟΥΝ Α' ΑΝΑΘΕΣΗ ΤΟ ΜΑΘΗΜΑ ΕΙΝΑΙ 1
                        if($this->lessonIsCalculated($division->pivot->oligomeles, $division->id, $lesson->id, $division->pivot->pedio2, $division->pivot->pedio3)) // ΕΛΕΓΧΕΙ ΑΝ ΤΟ ΜΑΘΗΜΑ ΘΑ ΓΙΝΕΙ ΣΥΝΔΙΔΑΣΚΑΛΙΑ
                            $this->pushToArrayAwithOneEidikotita();
                    }
                }else{
                    // ###########################################################################  T E S T                    
                    if ($this->eidikotites->count() > 1){          // ΑΝ ΟΙ ΕΙΔΙΚΟΤΗΤΕΣ ΠΟΥ ΕΧΟΥΝ Α' ΑΝΑΘΕΣΗ ΤΟ ΜΑΘΗΜΑ ΕΙΝΑΙ ΠΟΛΛΕΣ

                        if($lesson->name == 'Σύγχρονος Κόσμος: Πολίτης και Δημοκρατία'){
                            if($this->lessonIsCalculated($division->pivot->oligomeles, $division->id, $lesson->id)){ // ΕΛΕΓΧΕΙ ΑΝ ΤΟ ΜΑΘΗΜΑ ΘΑ ΓΙΝΕΙ ΣΥΝΔΙΔΑΣΚΑΛΙΑ                                
                                $this->executeForManyEidikotites('flag');   
                            }   
                        }else{

                            if($this->lessonIsCalculated($division->pivot->oligomeles, $division->id, $lesson->id)){ // ΕΛΕΓΧΕΙ ΑΝ ΤΟ ΜΑΘΗΜΑ ΘΑ ΓΙΝΕΙ ΣΥΝΔΙΔΑΣΚΑΛΙΑ
                                $this->executeForManyEidikotites();   
                            }      
                        }
           
                    }
                    // ###########################################################################  T E S T
                }
            } // foreach lesson

            $this->sum = 1;

        } //foreach division

        $this->syncDatabase($school);
    }


    private function displayArrayA()
    {
        $this->comment("\n********************** Π Ι Ν Α Κ Α Σ   Α  ********************");
        foreach($this->array_A as $key=>$data){
            $this->comment("*------------------------------------------------------------*");
            $this->comment("*                          $key                              *");
            $this->comment("*------------------------------------------------------------*");
            $this->comment("*   ΠΡΟΒΛΕΨΗ    |   ΔΙΑΘΕΣΙΜΕΣ  |   ΔΙΑΦΟΡΑ |   ΚΑΘΗΓΗΤΕΣ    *");
            $this->comment("*       {$data['forseen']}             {$data['available']}              {$data['difference']}             {$data['number_of_teachers']}");
        }
        $this->comment("**************************************************************");
    }

    private function displayArrayC()
    {
        $this->comment("\n******** Πίνακας Γ ********");
        if(!empty($this->array_C)){
            foreach($this->array_C as $key=>$value){
                $this->comment("---------------------------");
                $this->comment(" $key        : $value  ");
            }
        }
    }

    /**
     * @param $school
     */
    private function addTeachersToArrayA($school)
    {
        foreach ($school->organikes as $organiki) {               // ADD TEACHERS WITH ORGANIKI TO ARRAY A
            $this->array_A[$organiki->teacher->eidikotita] = [
                'forseen' => 0,
                'council_forseen' => 0,
                'available' => array_key_exists($organiki->teacher->eidikotita, $this->array_A) ? $this->array_A[$organiki->teacher->eidikotita]['available'] + $organiki->teacher->ypoxreotiko : $organiki->teacher->ypoxreotiko,
                'difference' => array_key_exists($organiki->teacher->eidikotita, $this->array_A) ? $this->array_A[$organiki->teacher->eidikotita]['available'] + $organiki->teacher->ypoxreotiko : $organiki->teacher->ypoxreotiko,
                'council_difference' => array_key_exists($organiki->teacher->eidikotita, $this->array_A) ? $this->array_A[$organiki->teacher->eidikotita]['available'] + $organiki->teacher->ypoxreotiko : $organiki->teacher->ypoxreotiko,
                'number_of_teachers' => array_key_exists($organiki->teacher->eidikotita, $this->array_A) ? $this->array_A[$organiki->teacher->eidikotita]['number_of_teachers'] + 1 : 1
            ];
            $this->organikes_list[] = $organiki->teacher->eidikotita;
        } // foreach organiki at school
    }

    /**
     * @param $lesson
     * @param $numberOfDivision
     */
    private function initializeProperties($lesson, $numberOfDivision, $class, $division_name)
    {
        $this->hours = $lesson->pivot->hours;                 // ΩΡΕΣ ΤΟΥ ΜΑΘΗΜΑΤΟΣ ΑΝΑ ΤΜΗΜΑ

        $this->sum = $numberOfDivision * $this->hours;              // ΣΥΝΟΛΟ ΩΡΩΝ ΜΑΘΗΜΑΤΟΣ ΜΕ ΤΜΗΜΑΤΑ

        $this->eidikotites = $lesson->pivot->eidikotites;

        $this->array_B = array();

        $this->eidikotites_list = $this->eidikotites->pluck('eidikotita_slug'); // ΕΙΔΙΚΟΤΗΤΕΣ ΠΟΥ ΕΧΟΥΝ Α' ΑΝΑΘΕΣΗ ΤΟ ΜΑΘΗΜΑ
        $this->hoursTakenByOtherEidikotita = false;
        $this->array_of_intersect = array_intersect($this->eidikotites_list->toArray(), $this->organikes_list); // ΥΠΟΛΟΓΙΖΕΙ ΠΟΙΕΣ ΕΙΔΙΚΟΤΗΤΕΣ ΤΟΥ ΜΑΘΗΜΑΤΟΣ ΥΠΑΡΧΟΥΝ ΣΤΟ ΣΧΟΛΕΙΟ ΜΕ ΟΡΓΑΝΙΚΗ
        $this->countDiffererentEidikotitesHasLessonAnathesi = count($this->array_of_intersect); // ΠΛΗΘΟΣ ΕΙΔΙΚΟΤΗΤΩΝ ΠΟΥ ΕΧΟΥΝ Α' ΑΝΑΘΕΣΗ ΤΟ ΜΑΘΗΜΑ

        $this->all_diff = $this->sum;
        $this->each_diff = 0;
        $this->mod_each_diff = 0;
        $this->flag_special = [];
        $this->flag_special_Second = [];

        $this->lesson_name = $lesson->name;
        $this->division_class = $class;
        $this->division_name  = $division_name;

        $this->oligomeli[28] = [5];     // ΟΙΚΟΝΟΜΙΑΣ & ΠΛΗΡΟΦ. --> Μαθηματικά
        $this->oligomeli[52] = [6, 7];    // ΣΠΟΥΔΩΝ ΥΓΕΙΑΣ       --> Φυσική - Χημεία
    }


    private function pushToArrayAwithOneEidikotita()
    {
        $this->pushArrayC($this->lesson_name, $this->division_class, $this->division_name, $this->eidikotites[0]['eidikotita_slug'], $this->sum);

        $this->array_A[$this->eidikotites[0]['eidikotita_slug']] = [
            'forseen'               => array_key_exists($this->eidikotites[0]['eidikotita_slug'], $this->array_A) ? $this->array_A[$this->eidikotites[0]['eidikotita_slug']]['forseen'] + $this->sum : $this->sum,
            'council_forseen'       => array_key_exists($this->eidikotites[0]['eidikotita_slug'], $this->array_A) ? $this->array_A[$this->eidikotites[0]['eidikotita_slug']]['forseen'] + $this->sum : $this->sum,
            'available'             => array_key_exists($this->eidikotites[0]['eidikotita_slug'], $this->array_A) ? $this->array_A[$this->eidikotites[0]['eidikotita_slug']]['available'] : 0,
            'difference'            => array_key_exists($this->eidikotites[0]['eidikotita_slug'], $this->array_A) ? $this->array_A[$this->eidikotites[0]['eidikotita_slug']]['difference'] - $this->sum : (-$this->sum),
            'council_difference'    => array_key_exists($this->eidikotites[0]['eidikotita_slug'], $this->array_A) ? $this->array_A[$this->eidikotites[0]['eidikotita_slug']]['difference'] - $this->sum : (-$this->sum),
            'number_of_teachers'    => array_key_exists($this->eidikotites[0]['eidikotita_slug'], $this->array_A) ? $this->array_A[$this->eidikotites[0]['eidikotita_slug']]['number_of_teachers'] : 0
        ];
    }

    private function pushArrayC($lesson_name, $division_class, $division_name, $slug, $value)
    {
        $key_name = "$lesson_name-$division_class.$division_name-$slug";
        
        if(!empty($this->array_C)){
            if(array_key_exists($key_name, $this->array_C)){
                $this->array_C[$key_name] += $value;             
            }else{
                $this->array_C[$key_name] = $value;             
            }
        }else{
            $this->array_C[$key_name] = $value;             
        }
    }


    private function syncDatabase($school)
    {
        foreach($this->array_A as $key=>$value) {
            if ($this->update) {
                if ($value['forseen'] != 0 || $value['available'] != 0) {
                    $eidikotita = NewEidikotita::where('eidikotita_slug', $key)->first();
                    if ($eidikotita != null) {
                        $school->kena()->detach($eidikotita->id);

                        $school->kena()->attach($eidikotita->id, [
                            'forseen' => $value['forseen'],
                            'council_forseen' => $value['forseen'],
                            'available' => $value['available'],
                            'difference' => $value['difference'],
                            'council_difference' => $value['difference'],
                            'number_of_teachers' => $value['number_of_teachers']
                        ]);
                    }
                }
            }
        }

    }


    private function calculateDifference($key, $value)
    {
        if ($this->all_diff > 0) {
            if (array_key_exists($key, $this->flag_special)) {
                $this->diference = -($this->all_diff + $this->each_diff + $this->mod_each_diff);
            } elseif (array_key_exists($key, $this->flag_special_Second)) {
                $this->diference = -$value;
            } else {
                if ($this->count == 0) {
                    $this->diference = -($this->each_diff + $this->mod_each_diff);
                } else {
                    $this->diference = -$this->each_diff;
                }
            }

        } else {
            $this->diference = -$value;
        }
    }



    private function pushToCollection($manyEidikotites, $slug, $hasOrganiki)
    {
        $klados_diff = array_key_exists($slug, $this->array_A) ? $this->array_A[$slug]['difference'] : 0;

        $manyEidikotites->push([
            'slug_name'     => $slug,
            'exists'        => true,
            'hasOrganiki'   => $hasOrganiki,
            'priorityA'     => false,
            'priorityB'     => false,
            'klados_diff'   => $klados_diff,
            'total'         => 0
        ]);
    }

    private function oneOrAllschools($type)
    {        
        if ($this->argument('one_school') == 'true') {            
            $this->sch_id = $this->argument('sch_id');
            $this->temp_school_id = $this->sch_id;
            $this->schools = School::where('id', $this->sch_id)->get();
        } else {
            $this->schools = School::where('type', $type)->get();
        }        
    }

    private function executeOneAnathesi($school)
    {
        $this->executeAlgorithm($school, true);
    }

    private function executeManyAnatheseis($school)
    {
        $this->executeAlgorithm($school, false);
    }

    private function executeProjectAnathesis($school)
    {
        // if($school->type == 'ΕΠΑΛ' || $school->type == 'Λύκειο'){
            $projects = $school->projects;

            foreach($projects as $project){
                $record = DB::table('_kena_sum_hours')->where('eid_id', $project->pivot->eid_id)->where('school_id', $school->id)->first();

                if($record != null){
                    $project_hours = $project->pivot->hours;
                    $new_difference = $record->available - $record->forseen - $project_hours;

                    Log::warning("New Diff = {$record->available}  - {$record->forseen} - {$project_hours}");

                    $school->kena()->updateExistingPivot($project->pivot->eid_id, [
                        'project_hours' => $project_hours,
                        'difference'    => $new_difference,
                        'council_difference'   => $new_difference
                    ]);
                }else{
                    Log::alert("Record is NULL");
                    Log::alert($project);

                    Log::alert([
                        'forseen' => 0,
                        'council_forseen' => 0,
                        'available' => 0,
                        'difference' => (-1) * $project->pivot->hours,
                        'council_difference' => (-1) * $project->pivot->hours,
                        'project_hours' => $project->pivot->hours,
                        'number_of_teachers' => 0
                    ]);
                    $school->kena()->attach($project->id, [
                        'forseen' => 0,
                        'council_forseen' => 0,
                        'available' => 0,
                        'difference' => (-1) * $project->pivot->hours,
                        'council_difference' => (-1) * $project->pivot->hours,
                        'project_hours' => $project->pivot->hours,
                        'number_of_teachers' => 0
                    ]);
                }
            }
        // }
    }

    private function lessonIsCalculated($isOligomeles, $div_id, $lesson_id, $pedio2 = false, $pedio3 = false)
    {        
        $calculate_the_lesson = true;

        if($isOligomeles){
            if(array_key_exists($div_id, $this->oligomeli) && $div_id != 27){   // ##### ΠΡΟΣΟΧΗ! ΟΧΙ ΓΙΑ ΤΩΝ ΘΕΤΙΚΩΝ ΣΠΥΟΥΔΩΝ
                foreach($this->oligomeli[$div_id] as $k=>$v){
                    if($lesson_id == $v){
                        $calculate_the_lesson = false;
                    }
                }
            }
        }    
        
        if($div_id == 27 && $lesson_id == 5 && !$pedio2){ // ΓΙΑ ΤΩΝ ΘΕΤΙΚΩΝ ΣΠΟΥΔΩΝ && ΤΟ ΜΑΘΗΜΑΤΑ ΤΩΝ ΜΑΘΗΜΑΤΙΚΩΝ && ΔΕΝ ΥΠΑΡΧΕΙ 2ο ΠΕΔΙΟ
            $calculate_the_lesson = false;
        }

        if($div_id == 27 && $lesson_id == 8 && !$pedio3){ // ΓΙΑ ΤΩΝ ΘΕΤΙΚΩΝ ΣΠΟΥΔΩΝ && ΤΟ ΜΑΘΗΜΑΤΑ ΤΗΣ ΒΙΟΛΟΓΙΑΣ && ΔΕΝ ΥΠΑΡΧΕΙ 3ο ΠΕΔΙΟ
            $calculate_the_lesson = false;
        }

        return $calculate_the_lesson;
    }


    private function calculateDivMod($collection, $sum)
    {
        if(!$collection->isEmpty()){
            if($sum > 0){
                $length = $collection->count(); // ΠΛΗΘΟΣ ΕΙΔΙΚΟΤΗΤΩΝ
                $this->each_diff = intdiv($sum, $length);  // ΠΗΛΙΚΟ ΤΟΥ sum με το length
                $this->mod_each_diff = $sum % $length;      // ΥΠΟΛΟΙΠΟ ΤΟΥ SUM με το length
            }
        }
    }

    private function updateArrayA($item)
    {
        $slug = $item['slug_name'];

        if(array_key_exists($slug, $this->array_A)){
            $this->array_A[$slug] = [
                'forseen'                   => $this->array_A[$slug]['forseen'] +  $item['total'],
                'council_forseen'           => $this->array_A[$slug]['council_forseen'] +  $item['total'],
                'available'                 => $this->array_A[$slug]['available'],
                'difference'                => $item['klados_diff'],
                'council_difference'        => $item['klados_diff'],
                'number_of_teachers'        => $this->array_A[$slug]['number_of_teachers']
            ];
        }else{
            $this->array_A[$slug] = [
                'forseen'               => $item['total'],
                'council_forseen'       => $item['total'],
                'available'             => 0,
                'difference'            => $item['klados_diff'],
                'council_difference'    => $item['klados_diff'],
                'number_of_teachers'    => 0
            ];
        }

    }

    private function createCollectionRows($manyEidikotites)
    {
        foreach($this->eidikotites as $eidikotita){   // ΓΙΑ ΚΑΘΕ ΕΙΔΙΚΟΤΗΤΑ ΠΟΥ ΕΧΕΙ ΤΟ ΜΑΘΗΜΑ Α ΑΝΑΘΕΣΗ       
            
            $slug = $eidikotita->eidikotita_slug;

            // $this->comment("Ειδικότητα: $slug");

            if(in_array($eidikotita->eidikotita_slug, $this->array_of_intersect)){ // ΥΠΑΡΧΟΥΝ ΟΡΓΑΝΙΚΕΣ;  $hasOrganiki
                $this->pushToCollection($manyEidikotites, $slug, true);
            }else{
                $this->pushToCollection($manyEidikotites, $slug, false);
            }
        }    
    }

    private function transformCollection($collection, $flag = null)
    {        
        // if($flag == 'flag2'){
        //     dd($collection);
        // }

        $collection->transform(function($item, $key) use($flag){
            if($key == 0){
                $item['total'] = $this->each_diff + $this->mod_each_diff;
                $item['klados_diff'] -= $this->each_diff + $this->mod_each_diff;
            }else{
                $item['total'] = $this->each_diff;
                $item['klados_diff'] -= $this->each_diff;
            }

            $this->updateArrayA($item);


            $this->pushArrayC($this->lesson_name, $this->division_class, $this->division_name, $item['slug_name'], $item['total']);

            return $item;
        });

    }

    private function transformCollectionExactlyToOrganikes($filteredOrganikes)
    {
        $filteredOrganikes->transform(function($item, $key){
            $total = $item['klados_diff'];

            $item['total'] = $total;
            $item['klados_diff'] = 0;

            $this->updateArrayA($item);
            $this->pushArrayC($this->lesson_name, $this->division_class, $this->division_name, $item['slug_name'], $item['total']);
            
            return $item;
        });
    }

    private function findOrganikes($manyEidikotites)
    {
        return $manyEidikotites->filter(function($item, $key){
            if($item['hasOrganiki']) 
                return $item;
        });
    }

    private function executeForManyEidikotites($flag = null)
    {      
        $manyEidikotites = collect();

        // ΥΠΑΡΧΕΙ ΣΤΟΝ ΝΟΜΟ;   $exists
        // ΥΠΑΡΧΕΙ ΠΡΟΤΕΡΑΙΟΤΗΤΑ; $priorityA
        // ΥΠΑΡΧΕΙ ΠΑΛΑΙΟΤΗΤΑ ΜΕΤΑΞΥ ΚΛΑΔΩΝ; $priorityB

        $this->createCollectionRows($manyEidikotites);  // ΛΙΣΤΑ ΕΙΔΙΚΟΤΗΤΩΝ Α' ΑΝΑΘΕΣΗΣ ΤΟΥ ΜΑΘΗΜΑΤΟΣ

        $filteredOrganikes = $this->findOrganikes($manyEidikotites);    // ΟΡΓΑΝΙΚΕΣ (ΕΙΔΙΚΟΤΗΤΕΣ) ΠΟΥ ΑΝΗΚΟΥΝ ΣΤΗ ΛΙΣΤΑ ΕΙΔΙΚΟΤΗΤΩΝ 

        if($filteredOrganikes->isEmpty()){  // ΔΕΝ ΥΠΑΡΧΟΥΝ ΟΡΓΑΝΙΚΕΣ (ΕΙΔΙΚΟΤΗΤΕΣ)    
            $this->calculateDivMod($manyEidikotites, $this->sum);   // ΜΟΙΡΑΣΜΑ ΤΩΝ ΩΡΩΝ. Η 1 (ΜΙΑ) ΠΕΡΙΣΣΙΑ ΣΤΗΝ ΠΡΩΤΗ ΕΙΔΙΚΟΤΗΤΑ
            $this->transformCollection($manyEidikotites);           // ΕΝΗΜΕΡΩΣΗ ΤΗΣ ΛΙΣΤΑΣ ΕΙΔΙΚΟΤΗΤΩΝ ΜΕ ΤΙΣ ΩΡΕΣ            
        }else{

            $sumDifferenceOfOrganikes = $filteredOrganikes->sum('klados_diff'); // ΑΘΡΟΙΣΜΑ ΩΡΩΝ ΟΛΩΝ ΤΩΝ ΟΡΓΑΝΙΚΩΝ (ΕΙΔΙΚΟΤΗΤΩΝ)
            
            if($sumDifferenceOfOrganikes > 0){
                $temp = $sumDifferenceOfOrganikes - $this->sum; // ΕΧΟΥΝ ΩΡΕΣ ΟΙ ΟΡΓΑΝΙΚΕΣ (ΕΙΔΙΚΟΤΗΤΕΣ) ΝΑ ΔΩΣΟΥΝ;
            }else{
                $temp = $this->sum;
            }

            if($temp >= 0){     // ΕΧΟΥΝ ΩΡΕΣ ΝΑ ΔΩΣΟΥΝ      
                $this->calculateDivMod($filteredOrganikes, $this->sum); // ΜΟΙΡΑΣΜΑ ΤΩΝ ΩΡΩΝ. Η 1 (ΜΙΑ) ΠΕΡΙΣΣΙΑ ΣΤΗΝ ΠΡΩΤΗ ΕΙΔΙΚΟΤΗΤΑ

                $this->transformCollection($filteredOrganikes, 'flag2'); // ΔΩΣΕ ΟΛΕΣ ΤΙΣ ΩΡΕΣ ΣΤΟΥΣ ΟΡΓΑΝΙΚΑ ΑΝΟΙΚΟΝΤΕΣ
                if($flag == 'flag'){
                    $this->testtest++;
                    // dd($this->array_C);
                    // dd($this->testtest);


                    // dd($this->each_diff);
                    // dd($this->mod_each_diff);
                    // dd($filteredOrganikes);
                }
            }else{
                // if($flag == 'flag'){
                //     // dd($this->sum);
                //     dd($sumDifferenceOfOrganikes);
                //     // dd($temp);
                //     dd($filteredOrganikes);
                // }
                // $this->transformCollectionExactlyToOrganikes($filteredOrganikes); // ΔΩΣΕ ΟΛΟ ΤΟ sumDifferenceOfOrganikes ΣΤΟΥΣ ΟΡΓΑΝΙΚΑ ΑΝΗΚΟΝΤΕΣ

                // if($flag == 'flag'){
                //     // dd($this->sum);
                //     // dd($sumDifferenceOfOrganikes);
                //     // dd($temp);
                //     dd($filteredOrganikes);
                // }
                
                $perisio_wrario = abs($temp);

                // if($flag == 'flag'){
                //     dd($perisio_wrario);
                // }
                
                $this->calculateDivMod($manyEidikotites, $perisio_wrario);
                // if($flag == 'flag'){
                //     dd($temp);
                //     dd($perisio_wrario);

                //     dd($this->each_diff);
                //     dd($this->mod_each_diff);
                // }
                $this->transformCollection($manyEidikotites);                   // ΔΩΣΕ ΤΟ ΥΠΟΛΟΙΠΟ ΩΡΑΡΙΟ (perisio_wrario) ΣΕ ΟΛΕΣ ΤΙΣ ΕΙΔΙΚΟΤΗΤΕΣ
            }
        }

    }

    protected function executePe04Summary($school)
    {
        $pe04Collection = $school->kena->where('klados_slug', 'ΠΕ04')->values();

        $sumPe04 = $this->calculateSumPe04($pe04Collection);
        
        $max = 0;
        $position = 0;

        // τι γίνεται όταν κανένας κλάδος δεν έχει έλλειμα πάνω από 12 ώρες
        // τι γίνεται όταν δεν υπάρχει οργανική στο ΠΕ04

        
        // #### TEST THIS ################################################################
        if($sumPe04 <= -12){
            // εαν είναι Γυμνασιο
            if($school->type == 'Γυμνάσιο'){
                foreach($pe04Collection as $pe04){
                    $this->updateKena($school, $pe04, $sumPe04);
                }
            }else{ // εαν είναι Λύκειο
            
                foreach($pe04Collection as $key=>$pe04){
                    if($pe04->pivot->difference < $max){
                        $max = $pe04->pivot->difference;
                        $position = $key;
                    }
                }

                $this->updateKena($school, $pe04Collection[$position], $sumPe04);
                
            }
        }elseif($sumPe04 >= 12){
            foreach($pe04Collection as $pe04){
                $this->updateKena($school, $pe04, $sumPe04);
            }
        }else{
            // $this->updateKena($school, $pe04);
        }
        // #### TEST THIS ################################################################

    }

    protected function updateKena($school, $klados, $sumPe04 = null)
    {
        $school->kena()->updateExistingPivot($klados->id, [
            'council_forseen'    => $klados->pivot->council_forseen,
            'council_difference'    => $klados->pivot->council_difference,
            'sum_pe04'    => $sumPe04
        ]);
    }

    protected function calculateSumPe04($pe04Collection)
    {
        return ($pe04Collection->sum('pivot.available') - ($pe04Collection->sum('pivot.forseen') + $pe04Collection->sum('pivot.project_hours')));
        // dd($pe04Collection->sum('pivot.forseen') + $pe04Collection->sum('pivot.project_hours')) - $pe04Collection->sum('pivot.available');
        // return ($pe04Collection->sum('pivot.available') + $pe04Collection->sum('pivot.project_hours')) - $pe04Collection->sum('pivot.forseen');
    }
}