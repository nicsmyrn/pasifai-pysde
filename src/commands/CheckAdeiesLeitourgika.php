<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\School;
use Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;

class CheckAdeiesLeitourgika extends Command
{
    private $lessons = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leitourgika:adeies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::warning("Run Scheduled command update adeies...");
        
        $schools = School::all();

        $bar = $this->output->createProgressBar($schools->count());

        foreach($schools as $school){
            foreach($school->topothetisis as $topothetisi){
                if($topothetisi->pivot->date_ends != null){
                    $today = Carbon::now();

                    $date = Carbon::createFromFormat('d/m/Y',$topothetisi->pivot->date_ends);
                    
                    if($date <= $today){
                        // dd("prin {$date->format('d/m/Y')} -- {$date->diffInDays()} -- {$today->format('d/m/Y')}");
                        if(in_array($topothetisi->pivot->teacher_type, [5,6])){
                            $school->topothetisis()->detach($topothetisi->afm);
                        }else{
                            $school->topothetisis()->updateExistingPivot($topothetisi->afm, [
                                'teacher_type'  => 0,   // Οργανικά
                                'date_ends'     => null,
                                'locked'        => true,
                                'hours'         => $topothetisi->ypoxreotiko
                            ]);
                        }
                    }
                    // dd(Carbon::createFromFormat('d/m/Y',$topothetisi->pivot->date_ends));
                }
            }
            $bar->advance();
        }
        Artisan::call('leitourgika:update');

        $bar->finish();

        $this->comment("\nDone\n");
    }
}