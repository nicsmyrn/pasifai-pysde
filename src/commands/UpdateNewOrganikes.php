<?php

namespace Pasifai\Pysde\commands;

use App\MySchoolTeacher;
use Illuminate\Console\Command;
use App\School;
use Pasifai\Pysde\models\Topothetisi;
use App\NewEidikotita;
use DB;
use Illuminate\Foundation\Auth\User;
use Pasifai\Pysde\models\Organiki;

class UpdateNewOrganikes extends Command
{
    private $lessons = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kena:update_nees_organikes';

    protected $file_name = 'manual_nees_organikes.csv';

    protected $root_path;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update nees organikes from csv file MANUAL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->root_path = base_path('vendor/pasifai/pysde/src/database/data/'. $this->file_name);

        $columns_names = array();

        $total = $this->getNumberOfTeachers();
        
        if ( ($handle = fopen($this->root_path,'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names);

            $bar = $this->output->createProgressBar($total);

            $counter = 1;

            DB::beginTransaction();

            foreach($this->teachers as $teacher){
                $myschool_teacher = MySchoolTeacher::where('am', $teacher['am'])->first();

                if($myschool_teacher != null){

                    $myschool_teacher->organiki_prosorini = $teacher['identifier'];
                    $myschool_teacher->topothetisi = 'Οργανικά';
                    $myschool_teacher->dieuthinsi = '5001a';
                    $myschool_teacher->type = 'ΓΕΝΙΚΗΣ';

                    $myschool_teacher->save();

                    $organikiModel = Organiki::find($myschool_teacher->afm);

                    $school = School::where('identifier', $teacher['identifier'])->first();

                    if($organikiModel != null){

                        if($school != null){
                            $organikiModel->sch_id = $school->id;
                            $organikiModel->checked = true;

                            $organikiModel->save();
                        }else{
                            $this->comment("School NULL for teacher with am {$teacher['am']}");
                        }
                    }else{
                        if($school != null){
                            Organiki::create([
                                'sch_id'    => $school->id,
                                'afm'       => $myschool_teacher->afm,
                                'checked'   => true
                            ]);
                        }else{
                            $this->comment("School NULL for teacher with am {$teacher['am']}");
                        }
                    }
                }else{
                    $this->comment("Teacher NOT in MYSCHOOL with am {$teacher['am']}");
                }

                $bar->advance();
                $counter++;
            }

            DB::commit();

            $bar->finish();
            
            $this->comment('Finished');

        }else{
            $this->comment('Error Opening File');
        }
    }

        /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->teachers[] = $record;
        }
        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file($this->root_path);
        return  count($fp) - 1;
    }

}
