<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\School;
use App\NewEidikotita;
use App\User;
use Log;

class UpdateNumberOfTeachersLeitourgika extends Command
{
    private $lessons = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leitourgika:update {to?} {from?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all records leitourgika hours table with the right number of teachers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list = [];

        $to = $this->argument('to');
        $from = $this->argument('from');

        $admin_id = User::where('userable_type', "App\Admin")->first()->id;

        if($to != null) $list[] = $to;
        if($from != null) $list[] = $from;
        
        if(empty($list)){
            $schools = School::all();
        }else{
            $schools = School::whereIn('id', $list)->get();
        }

        $bar = $this->output->createProgressBar($schools->count());

        foreach($schools as $school){

            // Log::warning(" Update Leitourgika for School : {$school->name}\n");

            $list_of_eidikotites = array();

            $leitourgikes_topothetisis = $school->topothetisis;

            // $leitourgikes_topothetisis =  $leitourgikes_topothetisis->whereIn('pivot.teacher_type', [0,5,6,10]);

            $groupByEidikotita = $leitourgikes_topothetisis->groupBy('eidikotita');
            
            foreach($groupByEidikotita as $key=>$eidikotita){     

                $available = 0;
                
                    foreach($eidikotita as $teacher){
                        if(in_array($teacher->pivot->teacher_type, [0,5,6,10])){
                            $special_topothetisis = $teacher->special_topothetisis;
                            // $other_topothetisis = $teacher->topothetisis->where('identifier', '<>', $school->identifier);
                            $other_topothetisis = $teacher->topothetisis->where('identifier', '<>', $school->identifier)->whereNotIn('pivot.teacher_type', [1,2,3,4,9,11]);
            
                            $sum_special = $special_topothetisis == null ? 0 : $special_topothetisis->sum('pivot.hours');
                            $sum_other = $other_topothetisis == null ? 0 : $other_topothetisis->sum('pivot.hours');
            
                            if($teacher->meiwsi > 0){
                                // Log::alert("Teacher $teacher->last_name has $teacher->meiwsi μείωση and its ok");
                            }
                            
                            $sum_other_topothetisis =  $sum_special + $sum_other;
                            $sum_ypoxrewtiko = $teacher->ypoxreotiko - $teacher->meiwsi;
                            $sum_wrario = $teacher->pivot->hours + $teacher->pivot->project_hours;
    
                            // $differece = $teacher->ypoxreotiko - $teacher->meiwsi - $teacher->sum_other_topothetisis - $teacher->pivot->hours - $teacher->pivot->project_hours;
                            
                            if(in_array($teacher->pivot->teacher_type, [5,6])){ // ΜΟΝΟ ΓΙΑ ΣΥΜΠΛΗΡΩΣΗ ΩΡΑΡΙΟΥ ΚΑΙ ΠΡΟΣΩΡΙΝΗ ΤΟΠΟΘΕΤΗΣΗ
                                $available += $sum_wrario;
                            }else{
                                if( ($sum_wrario + $sum_other_topothetisis) <= $sum_ypoxrewtiko){
                                    $available += $sum_ypoxrewtiko - $sum_other_topothetisis;
                                }else{
                                    $available += $sum_wrario;
                                }
                                // if($differece < 0){
                                    // $available += $teacher->ypoxreotiko - $teacher->meiwsi - $teacher->sum_other_topothetisis - $differece;
                                    // $available += $teacher->ypoxreotiko - $teacher->meiwsi - $differece;
                                // }else{
                                    // $available += $teacher->ypoxreotiko - $teacher->meiwsi;
                                // }
                            }
                        }
                    }

                    $current_eidikotita = NewEidikotita::where('eidikotita_slug', $key)->first();

                    // $available = $eidikotita->sum('ypoxreotiko') - $eidikotita->sum('meiwsi') - $eidikotita->sum('sum_other_topothetisis');
                    
                    $count_teachers = $eidikotita->count();
    
                    if($current_eidikotita != null){

                        $list_of_eidikotites[] = $current_eidikotita->id;

                        $record = $school->leitourgika_kena->where('id', $current_eidikotita->id)->first();

                        if($record != null){
                            $project_hours = $record->pivot->project_hours;

                            $school->leitourgika_kena()->updateExistingPivot($current_eidikotita->id,[
                                'available'             => $available,
                                'difference'            => $available - ($record->pivot->forseen + $project_hours),
                                'number_of_teachers'    => $count_teachers,
                                'user_id'               => $to != null ? $school->user->id : $admin_id
                            ]);
                        }else{
                            $school->leitourgika_kena()->attach($current_eidikotita->id, [
                                'available'             => $available,
                                'difference'            => 0,
                                'user_id'               => $school->user->id,
                                'number_of_teachers'    => $count_teachers,
                                'locked'                => false,
                                'forseen'               => 0
                            ]);
                        }
                        
                    }
            }

            foreach($school->leitourgika_kena as $keno_eidikotitas){
                if(!in_array($keno_eidikotitas->id, $list_of_eidikotites)){
                    $school->leitourgika_kena()->updateExistingPivot($keno_eidikotitas->id, [
                        'available'             => 0,
                        'forseen'               => $keno_eidikotitas->pivot->forseen,
                        'difference'            => - ($keno_eidikotitas->pivot->forseen + $keno_eidikotitas->pivot->project_hours),
                        'number_of_teachers'    => 0
                    ]);
                }
            }
            $bar->advance();
            
        } // end for each school

        $bar->finish();

        $this->comment("\nDone\n");
    }

}
