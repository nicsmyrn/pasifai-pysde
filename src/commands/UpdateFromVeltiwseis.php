<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\Models\Edata;

class UpdateFromVeltiwseis extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kena:google_sheet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ενημέρωση Πασιφάη για Συνολικά Μόρια - Εντοπιότητα - Συνυπηρέτηση από Μητρώο Google Sheet File as csv';

    protected $path = 'seeds/data/googleSheet.csv';
    protected $records = [];
    protected $not_found = 0;
    protected $updated = 0;

    private $dimos = [
        'ΧΑΝΙΩΝ'                => 1,
        'ΠΛΑΤΑΝΙΑ'              => 2,
        'ΑΠΟΚΟΡΩΝΟΥ'            => 3,
        'ΚΙΣΣΑΜΟΥ'              => 4,
        'ΚΑΝΤΑΝΟΥ'              => 5,
        'ΣΦΑΚΙΩΝ'               => 6
    ];


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $columns_names = array();

        $total = $this->getNumberOfRecords($this->path);

        if ( ($handle = fopen(database_path($this->path),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);
            $this->getRecords($handle, $columns_names);

            $bar = $this->output->createProgressBar($total);

            foreach($this->records as $division){

                $eDataModel = Edata::where('am', $division['ariomos-mhtrwoy'])
                    ->first();

                if($eDataModel != null){
                        $eDataModel->update([
                            'special_disease' => 0,
                            'special_many_children' => 0,
                            'special_judiciary' => 0,
                            'sum_moria' => $division['synolo-moriwn'],
                            'dimos_entopiotitas' => array_key_exists($division['dhmos-h-koinothta-entopiothtas'],$this->dimos) ? $this->dimos[$division['dhmos-h-koinothta-entopiothtas']] : 0,
                            'dimos_sinipiretisis' => array_key_exists($division['dhmos-h-koinot-ergasias-syzygoy'],$this->dimos) ? $this->dimos[$division['dhmos-h-koinot-ergasias-syzygoy']] : 0,
                            'aitisi_veltiwsis' => 1,
                            'ex_years' => $division['eth-synol-yphr'],
                            'ex_months' => $division['mhnes-synol-yphr'],
                            'ex_days' => $division['hmeres-synol-yphr'],
                            'moria_ypiresias' => $division['moria-synol-yphr'],
                            'moria_sinthikes' => $division['moria-dysm-syno'],
                            'moria_family_situation' => $division['moria-oikog-katast'],
                            'number_of_kids' => $division['tekna-mexri-18'],
                            'number_of_kids_spoudes' => $division['tekna-mexri-25-sp'],
                            'sum_tekna' => $division['synol-plhoos-teknwn'],
                            'moria_tekna' => $division['moria-teknwn']
                            // 'special_situation' => 0,
                            // 'family_situation' => 0
                        ]);
                    $this->updated++;
                }else{
                    $this->comment("\n{$division['ariomos-mhtrwoy']}");
                    $this->not_found++;
                }

                $bar->advance();
            }

            $bar->finish();

            $this->comment("\n Ενημερώθηκαν: $this->updated  || Δεν βρέθηκαν $this->not_found \n");

        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getRecords($handle, $columns_names)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH LESSON
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->records[] = $record;
        }
        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ',');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfRecords($path)
    {
        $fp = file(database_path($path));
        return  count($fp) - 1;
    }


}
