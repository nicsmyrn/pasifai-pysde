<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\School;

class InsertGenikis extends Command
{
    private $records = array();
    private $computerSplitNumber = 11;

    private $genikisDivisions = [
        'a' => [1,13,16],   // Α Γενικής Παιδείας, Πληροφορικής και Τεχνολογία
        'b' => [2,14,17],   // Β Γενικής Παιδείας, Πληροφορικής και Τεχνολογία
        'c' => [3,15,18]    // Γ Γενικής Παιδείας, Πληροφορικής και Τεχνολογία
    ];

    private $xenesDivisions = [
        'Α' => ['Αγγλικά' =>  4, 'Γαλλικά' => 7, 'Γερμανικά' =>  10],
        'Β' => ['Αγγλικά' =>  5, 'Γαλλικά' => 8, 'Γερμανικά' =>  11],
        'Γ' => ['Αγγλικά' =>  6, 'Γαλλικά' => 9, 'Γερμανικά' =>  12],
    ];


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kena:genikis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert Number of Divisions for Genikis Paideias';

    protected $pathGenikis = 'seeds/data/genikis.csv';
    protected $pathXenes = 'seeds/data/xenes.csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $columns_names = array();

        $this->comment("\nΕΙΣΑΓΩΓΗ ΤΜΗΜΑΤΩΝ ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ - ΠΛΗΡΟΦΟΡΙΚΗΣ - ΤΕΧΝΟΛΟΓΙΑΣ");

        $total = $this->getNumberOfRecords($this->pathGenikis);

        if ( ($handle = fopen(database_path($this->pathGenikis),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getRecords($handle, $columns_names);

            $bar = $this->output->createProgressBar($total);

            foreach($this->records as $division){
                $computerDiv = intdiv($division['students'], $this->computerSplitNumber);

                $school = School::where('identifier', $division['id'])->first();

                if($school != null){
                    foreach($this->genikisDivisions as $key=>$value){
                        if($division['class'] == $key){
                            foreach($value as $divNumberOfDatabase){
                                if($divNumberOfDatabase <=3){                               // ΑΝ ΕΙΝΑΙ ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ
                                    $school->divisions()->attach($divNumberOfDatabase,[
                                        'number'    => $division['provlepomeno'],
                                        'students'  => $division['students'],
                                        'numberSchoolProvide'   => $division['number']
                                    ]);
                                }else{                                                      // ΠΛΗΡΟΦΟΡΙΚΗΣ ή   ΤΕΧΝΟΛΟΓΙΑΣ
                                    $school->divisions()->attach($divNumberOfDatabase,[
                                        'number'    => $computerDiv > 0 ? $computerDiv : 1,
                                        'students'  => $division['students'],
                                        'numberSchoolProvide'   => 0
                                    ]);
                                }
                            }
                        }
                    }
                }

                $bar->advance();
            }

            $bar->finish();
            
        }else{
            $this->comment('Error Opening File');
        }

        $columns_names = array();
        $this->records = array();

        $this->comment("\nΕΙΣΑΓΩΓΗ ΑΓΓΛΙΚΩΝ - ΓΑΛΛΙΚΩΝ - ΓΕΡΜΑΝΙΚΩΝ");

        $total = $this->getNumberOfRecords($this->pathXenes);

        $bar = $this->output->createProgressBar($total);

        if ( ($handle = fopen(database_path($this->pathXenes),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getRecords($handle, $columns_names);

            foreach($this->records as $division){

                $school = School::where('identifier', $division['kwdikos-yp'])->first();

                if($school != null){
                    if(array_key_exists($division['taxh'], $this->xenesDivisions)){
                        if(array_key_exists($division['glwssa'],$this->xenesDivisions[$division['taxh']])){
                            $school->divisions()->attach($this->xenesDivisions[$division['taxh']][$division['glwssa']],[
                                'number'    => $division['problepomeno'],
                                'students'  => $division['synolo'],
                                'numberSchoolProvide'   => $division['ariomos-tmhmatwn']
                            ]);
                        }
                    }
                }

                $bar->advance();
            }

            $bar->finish();

        }else{
            $this->comment('Error Opening File');
        }

        $this->comment("\n Script Finished");
    }

            /**
     * @param $handle
     * @param $columns_names
     */
    private function getRecords($handle, $columns_names)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH LESSON
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->records[] = $record;
        }
        fclose($handle);
    }

        /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfRecords($path)
    {
        $fp = file(database_path($path));
        return  count($fp) - 1;
    }


}
