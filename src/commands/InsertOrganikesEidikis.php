<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use Pasifai\Pysde\models\Lesson;
use App\NewEidikotita;
use App\MySchoolTeacher;
use Pasifai\Pysde\models\Organiki;
use App\School;
use DB;
use Pasifai\Pysde\controllers\ExcelExports\Placements\OrganikesOfPraxi;
use Pasifai\Pysde\models\OrganikiEidikis;

class InsertOrganikesEidikis extends Command
{
    private $types = [
        'Οργανικά από Αρση Υπεραριθμίας',
        'Οργανικά', 
        'Οργανικά από Αμοιβαία Μετάθεση',
        'Οργανικά σε Τμήμα Ένταξης'
//        'Από Διάθεση ΠΥΣΠΕ/ΠΥΣΔΕ',
//        'Έδρα ΠΥΣΠΕ/ΠΥΣΔΕ λόγω μη τοποθέτησης'
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kena:organikes_EIDIKIS';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Εισαγωγή Οργανικών Ειδικής Αγωγής από το MySchool για τα Οργανικά Κενά';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('_kena_organikes_eidikis')->truncate();

        $all_teachers = MySchoolTeacher::all();

        $total = $all_teachers->count();

        $bar = $this->output->createProgressBar($total);

        foreach($all_teachers as $teacher){
            if($teacher->am != null || $teacher->am != ''){
                if(in_array($teacher->topothetisi, $this->types)){ 

                    if($teacher->type === 'ΕΙΔΙΚΗΣ'){
                        $school = School::where('identifier', $teacher->organiki_prosorini)->first();
    
                        if ($school == null){
                            $this->comment("\n school : {$teacher->organiki_prosorini} not found \n");
                        }else{
                            OrganikiEidikis::create([
                                'sch_id'    => $school->id,
                                'afm'       => $teacher->afm
                            ]);
                        }
                    }
                }
            }

            $bar->advance();
        }

        $bar->finish();

        $this->comment("\n Script Finished. \n");

    }


}
