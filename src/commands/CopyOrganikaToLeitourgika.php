<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\School;
use Pasifai\Pysde\models\Topothetisi;
use App\NewEidikotita;
use Illuminate\Foundation\Auth\User;

class CopyOrganikaToLeitourgika extends Command
{
    private $lessons = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kena:copy_leitourgika';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy organika kena to leitourgika & organikes to topothetisis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $schools = School::all();

        foreach($schools as $school){
            $this->comment($school->name);
            
            $organikes = $school->organikes;

            foreach($organikes as $organiki){
                $topothetisi = Topothetisi::updateOrCreate([
                    'afm'   => $organiki->afm,
                    'sch_id'    => $school->id
                ]);             
                $topothetisi->update([
                    'teacher_type'  => 0,   // Τύπος 0 --> ανήκει οργανικά στη Σχολική Μονάδα
                    'date_ends'     => null,
                    'hours'         => $organiki->teacher->ypoxreotiko - $organiki->teacher->meiwsi,
                    'project_hours' => 0,
                    'locked'        => false
                ]);
            }

            $organika_kena = $school->kena;
            $school->leitourgika_kena()->detach();

            $user = User::where('userable_type', 'App\Admin')->first();

            foreach($organika_kena as $keno){
                
                $eidikotita = NewEidikotita::find($keno->pivot->eid_id);

                $school->leitourgika_kena()->attach($eidikotita,[
                    'forseen'   => $keno->pivot->available,
                    'available' => $keno->pivot->available,
                    'user_id'   => $user->id,
                    'locked'    => true
                ]);
                //df sdlfks
            }
        }

        $this->comment("\nDone\n");
    }

}
