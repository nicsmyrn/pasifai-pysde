<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\NewEidikotita;

class InsertEidikotites extends Command
{
    private $lessons = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kena:eidikotites';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $path = 'seeds/data/all_eidikotites.csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $columns_names = array();

        $total = $this->getNumberOfLessons();

        if ( ($handle = fopen(database_path($this->path),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getLessons($handle, $columns_names);

            // dd(array_filter(explode("\t", $this->lessons[0]["eidikothtes-poy-exoyn-to-maohma-ws-b-anaoesh"])));

            $bar = $this->output->createProgressBar($total);

            $counter = 1;

            foreach($this->lessons as $lesson){
                NewEidikotita::create([
                    'klados_name'       => 'aaa',
                    'klados_slug'       => $lesson['slug'],
                    'eidikotita_name'   => $lesson['name'],
                    'eidikotita_slug'   => $lesson['slug']
                ]);

                $bar->advance();
                $counter++;
            }

            $bar->finish();
            
            $this->comment('Finished');

        }else{
            $this->comment('Error Opening File');
        }
        
    }

        /**
     * @param $handle
     * @param $columns_names
     */
    private function getLessons($handle, $columns_names)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH LESSON
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->lessons[] = $record;
        }
        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfLessons()
    {
        $fp = file(database_path($this->path));
        return  count($fp) - 1;
    }

}
