<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\School;
use Artisan;
use Log;

class LeitourgikaRemoveAnapliroteAndUpdateLeitourgika extends Command
{
    protected $signature = 'leitourgika:anaplirotes';

    protected $description = 'Remove anaplirotes & Update Leitourgika';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schools = School::all();

        $bar = $this->output->createProgressBar($schools->count());

        foreach($schools as $school){

            Log::warning(" Remove Anaplirotes for School : {$school->name}\n");

            $leitourgikes_topothetisis = $school->topothetisis;

            foreach($leitourgikes_topothetisis as $key=>$topothetisi){     
                if($topothetisi->am == "" || $topothetisi->am == null){
                    $afm = $topothetisi->afm;
                    $school->topothetisis()->detach($afm);
                }
            }

            $bar->advance();
            
        } // end for each school

        // leitourgika:update all schools....
        Log::warning("Run command Leitourgika update");

        Artisan::call('leitourgika:update');

        $bar->finish();

        $this->comment("\nDone\n");
    }
}
