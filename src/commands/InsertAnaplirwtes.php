<?php

namespace Pasifai\Pysde\commands;

use App\Console\Commands\MySchoolManual;
use Illuminate\Console\Command;
use App\Models\Edata;
use App\Models\Year;
use App\MySchoolTeacher;
use App\NewEidikotita;
use App\School;
use App\Teacher;
use Artisan;
use DB;
use Log;
use Pasifai\Pysde\controllers\traits\MoriaCalculator;
use Pasifai\Pysde\models\Placement;
use Pasifai\Pysde\models\Praxi;

class InsertAnaplirwtes extends Command
{
    use MoriaCalculator;

    protected $userCounter = 0;

    protected $teachers = [];
    protected $updatedTeachers = 0;
    protected $createdTeachers = 0;
    protected $newModelEidikotita;

    protected $eurosDays = [
        5 => 1,
        10 => 2,
        15 => 3,
        20 => 4,
        23 => 5 // adglaksjdglaskdjgl
    ];


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anaplirwtes:insert {praxi_id?} {praxi_date?} {update_leitourgika?}';
    // example art anaplirwtes:insert 111 2021-09-05 yes/No

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Εισαγωγή Αναπληρωτών και τοποθετήσεών τους, με ενημέρωση των κενών';

    protected $file_name = 'anaplirwtes.csv';

    protected $root_path;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->root_path = base_path('vendor/pasifai/pysde/src/database/data/'. $this->file_name);

        $columns_names = array();

        $total = $this->getNumberOfTeachers();
        
        if ( ($handle = fopen($this->root_path,'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names);

            $bar = $this->output->createProgressBar($total);

            $counter = 1;


            /*
                ####################
            */

            $current_year = Year::where('current', true)->first();

            $praxi_id = $this->ask('Δώσε τον αριθμό της πράξης Διευθυντή:');
            $praxi_date = $this->ask('Δώσε την Ημερομηνία της πράξης (2021-09-05):');
            $from_date = $this->ask('Απο ποια ημερομηνία είναι η τοποθέτηση;');
            $to_date = $this->ask('Μεχρι ποια ημερομηνία είναι η τοποθέτηση;');

            $answer = $this->ask('Θέλεις να υπολογιστούν οι τοποθετήσεις στα κενά/πλεονάσματα (yes/no)?');

            $praxi = Praxi::where('decision_number', $praxi_id)->where('year_id', 6)->first();

            if($praxi != null){
                 $new_praxi = $praxi;
            }else{
                $new_praxi = Praxi::create([
                    'decision_number' => $praxi_id,
                    'decision_date'   => $praxi_date,
                    'praxi_type'      => 12, // Πράξη Διευθυντή ΔΔΕ 
                    'description'     => '',
                    'year_id'         => $current_year->id
                ]);
            }



            if($answer === 'yes'){
                //calculate kena
                $this->comment('Υπολογίζει τα κενά....');
            }
            /*
                #####################
            */


            foreach($this->teachers as $teacher){
                $t = MySchoolTeacher::withTrashed()->find($teacher['afm']);

                if ($t == null){
                    $t = $this->createTeacher($teacher, $counter);
                    Log::warning(" {$teacher['epioeto']} - ΑΦΜ {$teacher['afm']} ### CREATED ###");
                }else{
                    if($t->trashed()){
                        Log::alert("was deleted...");
                        $t->restore();
                    }

                    Log::alert(" {$teacher['epioeto']} - ΑΦΜ {$teacher['afm']} Exists...");
                    $this->updateOrNothing($t, $teacher);
                }

                /*
                    1. Εισαγωγή Αναπληρωτή στο myschool_teachers ή UPdate
                        - προσοχή την ειδικότητα
                        - υποχρεωτικό ωράριο
                    2. Δημιουργία Πράξης --> "113" "2021-09-03"
                    3. Δημιουργία τοποθέτησης 
                    4. Ενημέρωση πίνακα _kena_leitourgika
                        Ενημέρωση των σχολικών μονάδων με leitourgika:update <number of school>
                */

                // ###############################################
                    $school = School::where('identifier', $teacher['kwd-sxoleioy'])->first();

                    if($school != null){
                        $placement = Placement::create([
                            'afm' => $teacher['afm'],
                            'praxi_id' => $new_praxi->id,
                            'teacher_name' => $teacher['epioeto'] . ' ' . $teacher['onoma'],
                            'hours' => $teacher['wres'],
                            'from' => 'ΑΝΑΠΛΗΡΩΤΗΣ',
                            'to' => $school->name,
                            'description' => $teacher['typos-kenoy'],
                            'from_id' => 3000,
                            'to_id' => $school->id,
                            'klados' => $this->newModelEidikotita->eidikotita_name,
                            'klados_id' => $this->newModelEidikotita->id,
                            'placements_type' => $teacher['sx-analhpshs'] == 'NAI' ? 1 : 3,
                            'days' => 5,
                            'me_aitisi' => 1,
                            'starts_at' => $from_date,
                            'ends_at' => $to_date
                        ]);

                        if($answer === 'yes'){
                            //Update Leitourgika Kena
                            if($teacher['typos-kenoy'] === 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'){
                                Log::warning("Kena {$teacher['typos-kenoy']} - {$teacher['afm']}");
                                $t->topothetisis()->detach($school->id);
                                
                                $t->topothetisis()->attach($school->id, [
                                    'teacher_type'        => $teacher['sx-analhpshs'] == 'NAI' ? 5 : 6,
                                    'date_ends'           => $placement->ends_at,
                                    'hours'               => $placement->hours,
                                    'project_hours'       => 0,
                                    'locked'              => true
                                ]);
                            }
                        }
                    }

                // ###############################################

                $bar->advance();
                $counter++;
            }

            Artisan::call('leitourgika:update');

            $bar->finish();
            
            $this->comment("\n Script Finished!");

        }else{
            $this->comment('Error Opening File');
        }
        
    }

        /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->teachers[] = $record;
        }
        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file($this->root_path);
        return  count($fp) - 1;
    }

        /**
     * @param $t
     * @param $teacher
     */
    private function updateOrNothing($t, $teacher)
    {
        if(str_contains($teacher['eidikothta'], 'ΕΑΕ')){
            if(str_contains($teacher['eidikothta'], 'ΠΕ11')){
                $name_eidikotita = 'ΠΕ11.01';
            }else{
                $name_eidikotita = str_replace('ΕΑΕ', '50', $teacher['eidikothta']);
            }
        }else{
            $name_eidikotita = $teacher['eidikothta'];
        }

        $t->ypoxreotiko = $teacher['synolo-wrwn'];
        
        $t->type_pinakas = $teacher['typos-kenoy'];

        $t->eidikotita = $name_eidikotita;

        if($teacher['typos-kenoy'] === 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'){
            $t->type = 'ΓΕΝΙΚΗΣ';
        }else{
            $t->type = 'ΕΙΔΙΚΗΣ';
        }

        // $t->email = $teacher['e-mail'];

        // $t->yadescription = $teacher['typos'];

        // $t->position = $teacher['seira-pinaka'];

        // if($teacher['epioeto'] === 'ΑΔΑΜΟΠΟΥΛΟΣ'){
        //     dd($name_eidikotita);

        // }
        $this->newModelEidikotita = NewEidikotita::where('eidikotita_slug', $name_eidikotita)->first();

        if ($t->new_eidikotita != $this->newModelEidikotita->id) {
            $t->new_eidikotita = $this->newModelEidikotita->id;
        }

        $t->save();
        $this->updatedTeachers++;
    }

    /**
     * @param $teacher
     */
    private function createTeacher($teacher, $counter)
    {
            if(isset($teacher['eidikothta'])){
                if(str_contains($teacher['eidikothta'], 'ΕΑΕ')){
                    if(str_contains($teacher['eidikothta'], 'ΠΕ11')){
                        $name_eidikotita = 'ΠΕ11.01';
                    }else{
                        $name_eidikotita = str_replace('ΕΑΕ', '50', $teacher['eidikothta']);
                    }
                }else{
                    $name_eidikotita = $teacher['eidikothta'];
                }

                $this->newModelEidikotita = NewEidikotita::where('eidikotita_slug', $name_eidikotita)->first();

                if ($this->newModelEidikotita != null){
                    $new_eidikotita = $this->newModelEidikotita->id;
                }else{
                    $new_eidikotita = 0;    
                }
            }else{
                $new_eidikotita = 0;
            }

            if($teacher['typos-kenoy'] === 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'){
                $teacher_type = 'ΓΕΝΙΚΗΣ';
            }else{
                $teacher_type = 'ΕΙΔΙΚΗΣ';
            }

            if($new_eidikotita != 0){                
                $myschool = MySchoolTeacher::create([
                    'afm' => $teacher['afm'],
                    'sex' => isset($teacher['sex']) ?  ($teacher['sex'] == 'Θ' ? 0 : 1) : 0,
                    'last_name' => isset($teacher['epioeto']) ? $teacher['epioeto'] : '',
                    'first_name' => isset($teacher['onoma']) ? $teacher['onoma'] : '',
                    'middle_name' => isset($teacher['on-patrws']) ? $teacher['on-patrws'] : '',
                    'mothers_name' => '',
                    'mobile' => '',
                    'eidikotita' => $name_eidikotita,
                    'organiki_prosorini' => 5001010,
                    'topothetisi' => 'Από Διάθεση ΠΥΣΠΕ/ΠΥΣΔΕ',
                    'dieuthinsi' => '5001a',
                    'new_eidikotita' => $new_eidikotita,
                    'am' => '',
                    'phone' => '',
                    'vathmos' => '',
                    'type' => $teacher_type,
                    // 'type_pinakas' => '',
                    // 'position'  => $teacher['seira-pinaka'],
                    // 'email' => $teacher['e-mail'],
                    // 'yadescription' => $teacher['typos'],
                    // 'sex'   => $teacher['fylo'],
                    'ypoxreotiko' => $teacher['synolo-wrwn'],
                    'type_pinakas' => $teacher['typos-kenoy']
                ]);

                $this->createdTeachers++;
            }
        
            return $myschool;

    }
}
