<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\School;
use App\NewEidikotita;
use App\User;
use Log;

class UpdateOrganikaSumHours extends Command
{
    private $lessons = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'organika:update {to?} {from?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update organika sum hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list = [];

        $to = $this->argument('to');
        $from = $this->argument('from');

        if($to != null) $list[] = $to;
        if($from != null) $list[] = $from;
        
        if(empty($list)){
            $schools = School::all();
        }else{
            $schools = School::whereIn('id', $list)->get();
        }

        $bar = $this->output->createProgressBar($schools->count());

        foreach($schools as $school){

            // Log::warning(" Update Leitourgika for School : {$school->name}\n");

            $list_of_eidikotites = array();

            $organikes_topothetisis = $school->organikes_topothetisis;

            // $leitourgikes_topothetisis =  $leitourgikes_topothetisis->whereIn('pivot.teacher_type', [0,5,6,10]);

            $groupByEidikotita = $organikes_topothetisis->groupBy('eidikotita');
            
            foreach($groupByEidikotita as $key=>$eidikotita){     

                $available = 0;
                
                    foreach($eidikotita as $teacher){

                        
                        $available += $teacher['ypoxreotiko'];
                    }

                    $current_eidikotita = NewEidikotita::where('eidikotita_slug', $key)->first();

                    // $available = $eidikotita->sum('ypoxreotiko') - $eidikotita->sum('meiwsi') - $eidikotita->sum('sum_other_topothetisis');
                    
                    $count_teachers = $eidikotita->count();
    
                    if($current_eidikotita != null){

                        $list_of_eidikotites[] = $current_eidikotita->id;


                        $record = $school->kena->where('id', $current_eidikotita->id)->first();

                        if($record != null){
                            $school->kena()->updateExistingPivot($current_eidikotita->id,[
                                'available'             => $available,
                                'difference'            => $available - ($record->pivot->forseen),
                                'number_of_teachers'    => $count_teachers,
                                'council_difference'    => $available - ($record->pivot->forseen),
                            ]);
                        }else{
                            //
                        }
                        
                    }
            }

            $bar->advance();
            
        } // end for each school

        $bar->finish();

        $this->comment("\nDone\n");
    }

}
