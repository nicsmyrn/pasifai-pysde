<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\Models\Edata;
use App\MySchoolTeacher;
use App\School;
use App\Teacher;
use DB;

class UpdateProfileFromEdata extends Command
{
    protected $userCounter = 0;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edata:monimos_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Pasifai Teacher Profile from E-Data & wrario from My-School';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mySchoolAllTeachers = MySchoolTeacher::all();

        if($mySchoolAllTeachers->isEmpty()){
            $this->comment("\n No Teacher Users in Database.");
        }else{
            foreach($mySchoolAllTeachers as $mySchool){
                $edata = $mySchool->edata;
                $teacherModel = $mySchool->teacher;

                if($teacherModel != null){
                    if($teacherModel->teacherable_type == "App\Monimos"){
                        $monimos = $teacherModel->teacherable;
        
                        if($edata != null){
    
                            DB::beginTransaction();
                                $teacherModel->update([
                                    'dimos_sinipiretisis'   => $edata->dimos_sinipiretisis,
                                    'dimos_entopiotitas'    => $edata->dimos_entopiotitas,
                                    'special_situation'     => $edata->special_situation,
                                    'ex_years'              => $edata->ex_years,
                                    'ex_months'             => $edata->ex_months,
                                    'ex_days'               => $edata->ex_days,
                                    'childs'                => $edata->sum_tekna,
                                    'family_situation'      => $edata->family_situation
                                ]);

                                if($monimos != null){
                                    $monimos->update([
                                        'am'    => $edata->am,
                                        'moria' => $edata->sum_moria,
                                        'organiki'  => $mySchool->organiki_id,
                                        'county'    => config('requests.default_county'),
                                        'orario'    => $mySchool->ypoxreotiko,
                                        'dieuthinsi'    => $mySchool->dieuthinsi
                                    ]);
                                }
    

                            DB::commit();
    
                            $this->userCounter++;
                            $this->comment("Ο {$mySchool->last_name} {$mySchool->first_name} [{$edata->am}] ενημερώθηκε με επιτυχία...\n");
                        }else{
                            $this->comment("Ο {$teacherModel->user->full_name} ΔΕΝ ενημερώθηκε ...\n");
                        }
                    }else{
                        $not = 'not_monimos';
                        //
                    }
                }

            }
        }

        $this->comment('Users: ' . $this->userCounter);
    }
}
