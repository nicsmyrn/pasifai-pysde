<?php

namespace Pasifai\Pysde\commands;

use App\Console\Commands\MySchoolManual;
use Illuminate\Console\Command;
use App\Models\Edata;
use App\MySchoolTeacher;
use App\School;
use App\Teacher;
use DB;
use Pasifai\Pysde\controllers\traits\MoriaCalculator;

class CalculateApospasmenoiEntos extends Command
{
    use MoriaCalculator;

    protected $userCounter = 0;

    protected $teachers = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edata:apospasmenoi_entos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all moria for apospasmenous entos';

    protected $file_name = 'apospaseis_stoixeia.csv';

    protected $root_path;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->root_path = base_path('vendor/pasifai/pysde/src/database/data/'. $this->file_name);

        $columns_names = array();

        $total = $this->getNumberOfTeachers();
        
        if ( ($handle = fopen($this->root_path,'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names);

            $bar = $this->output->createProgressBar($total);

            $counter = 1;

            foreach($this->teachers as $teacher){                
                // update E-data
                $myschool_teacher = MySchoolTeacher::where('am', $teacher['am'])->first();

                if($myschool_teacher != null){
                    
                    $teacher_model = $myschool_teacher->teacher;

                    if($teacher_model != null){

                    
                    $edata_model = $myschool_teacher->edata;

                    $family_situation = array_search($teacher['family'], config('requests.family_situation')) ? array_search($teacher['family'], config('requests.family_situation')) : '0' ;
                    $childs = intval($teacher['kids']);
                    $dimos_sinipiretisis = array_search($teacher['sinipiretisi'], config('requests.dimos')) ? array_search($teacher['sinipiretisi'], config('requests.dimos')) : 0;
                    $dimos_entopiotitas = array_search($teacher['entopiotita'], config('requests.dimos')) ? array_search($teacher['entopiotita'], config('requests.dimos')) : 0;

                    $ex_years = intval($teacher['years']);
                    $ex_months = intval($teacher['months']);
                    $ex_days = intval($teacher['days']);

                    $idiou = array_search($teacher['ygeia-idiou'], config('requests.logoi_ygeias.idiou.data')) ? array_search($teacher['ygeia-idiou'], config('requests.logoi_ygeias.idiou.data')) : 0;
                    $sizigou = array_search($teacher['ygeia-sizigou'], config('requests.logoi_ygeias.sizigou.data')) ? array_search($teacher['ygeia-sizigou'], config('requests.logoi_ygeias.sizigou.data')) : 0;
                    $paidiwn = array_search($teacher['ygeia-paidiwn'], config('requests.logoi_ygeias.paidiou.data')) ? array_search($teacher['ygeia-paidiwn'], config('requests.logoi_ygeias.paidiou.data')) : 0;
                    $goneon = array_search($teacher['ygeia-gonewn'], config('requests.logoi_ygeias.goneon.data')) ? array_search($teacher['ygeia-gonewn'], config('requests.logoi_ygeias.goneon.data')) : 0;
                    $aderfon = array_search($teacher['ygeia-aderfwn'], config('requests.logoi_ygeias.aderfon.data')) ? array_search($teacher['ygeia-aderfwn'], config('requests.logoi_ygeias.aderfon.data')) : 0;
                    $exosomatiki = $teacher['exosomatiki'] == 'ΝΑΙ' ? 1 : 0;
                    
                    $this->comment("Counter : $counter");

                    $teacher_model->family_situation = $family_situation;
                    $teacher_model->childs = $childs;
                    $teacher_model->dimos_sinipiretisis = $dimos_sinipiretisis;
                    $teacher_model->dimos_entopiotitas = $dimos_entopiotitas;
                    $teacher_model->ex_years = $ex_years;
                    $teacher_model->ex_months = $ex_months;
                    $teacher_model->ex_days = $ex_days;
                    $teacher_model->logoi_ygeias_idiou = $idiou;
                    $teacher_model->logoi_ygeias_sizigou = $sizigou;
                    $teacher_model->logoi_ygeias_paidiwn = $paidiwn;
                    $teacher_model->logoi_ygeias_goneon = $goneon;
                    $teacher_model->logoi_ygeias_aderfon = $aderfon;
                    $teacher_model->logoi_ygeias_exosomatiki = $exosomatiki;

                    $teacher_model->save();

                    $sum = $this->getMoriaApospasis($teacher_model);

                    $teacher_model->teacherable->moria_apospasis = round($sum,3);
                    $teacher_model->teacherable->save();

                    $edata_model->dimos_entopiotitas = $dimos_entopiotitas;
                    $edata_model->dimos_sinipiretisis = $dimos_sinipiretisis;
                    $edata_model->ex_years = $ex_years;
                    $edata_model->ex_months = $ex_months;
                    $edata_model->ex_days = $ex_days;
                    $edata_model->sum_tekna = $childs;
                    $edata_model->family_situation = $family_situation;
                    $edata_model->moria_apospasis = $sum;
                    $edata_model->save();
                    } // end of teacher model null if
                }else{
                    dd("myschool is null am: {$teacher['am']}");
                }

                $bar->advance();
                $counter++;
            }

            $bar->finish();
            
            $this->comment('Finished');

        }else{
            $this->comment('Error Opening File');
        }
        
    }

        /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names)
    {
        while (($data = fgetcsv($handle, 0, ',')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->teachers[] = $record;
        }
        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ',');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file($this->root_path);
        return  count($fp) - 1;
    }
}
