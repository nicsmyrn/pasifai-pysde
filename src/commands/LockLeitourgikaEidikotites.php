<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\School;
use App\NewEidikotita;
use App\User;
use DB;
use Log;

class LockLeitourgikaEidikotites extends Command
{
    private $lessons = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leitourgika:eidikotites {action}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lock or Unlock all leitourgika eidikotites';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument('action');

        if($action == 'true'){
            $m = 'true';
            $value = true;
        }else{
            $m = 'false';
            $value = false;
        }

        DB::table('_kena_leitourgika_sum_hours')->update(['locked' => $value]);

        $this->comment("\n Table Leitourgika Eidikotites setted locked value to : $m");
    }

}
