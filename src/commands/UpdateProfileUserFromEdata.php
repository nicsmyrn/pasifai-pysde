<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\Models\Edata;
use App\MySchoolTeacher;
use App\School;
use App\Teacher;
use DB;

class UpdateProfileUserFromEdata extends Command
{
    protected $userCounter = 0;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edata:profile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Pasifai Teacher Profile from E-Data & wrario from My-School';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $teachers = Teacher::all();

        if($teachers->isEmpty()){
            $this->comment("\n No Teacher Users in Database.");
        }else{
            foreach($teachers as $teacher){
                if($teacher->teacherable_type == "App\Monimos"){
                    $monimos = $teacher->teacherable;
                    $edataProfile = Edata::find($monimos->am);

                    $mySchool = MySchoolTeacher::where('am', $monimos->am)->first();

                    if($edataProfile != null && $mySchool != null){

                        DB::beginTransaction();
                            $teacher->update([
                                'dimos_sinipiretisis'   => $edataProfile->dimos_sinipiretisis,
                                'dimos_entopiotitas'    => $edataProfile->dimos_entopiotitas,
                                'special_situation'     => $edataProfile->special_situation,
                                'ex_years'              => $edataProfile->ex_years,
                                'ex_months'             => $edataProfile->ex_months,
                                'ex_days'               => $edataProfile->ex_days,
                                'childs'                => $edataProfile->sum_tekna,
                                'family_situation'      => $edataProfile->family_situation
                            ]);

                            $monimos->update([
                                'am'    => $edataProfile->am,
                                'moria' => $edataProfile->sum_moria,
                                'organiki'  => $mySchool->organiki_id,
                                'county'    => config('requests.default_county'),
                                'orario'    => $mySchool->ypoxreotiko,
                                'dieuthinsi'    => $mySchool->dieuthinsi
                            ]);
                        DB::commit();

                        $this->userCounter++;
                        $this->comment("Ο {$mySchool->last_name} {$mySchool->first_name} [{$edataProfile->am}] ενημερώθηκε με επιτυχία...\n");
                    }else{
                        $this->comment("Ο {$teacher->user->full_name} ΔΕΝ ενημερώθηκε ...\n");
                    }
                }else{
                    $not = 'not_monimos';
                    $this->comment("Not Monimos {$teacher['afm']}");
                    // dd(compact('not', 'teacher'));
                }
            }
        }

        $this->comment('Users: ' . $this->userCounter);
    }
}
