<?php

namespace Pasifai\Pysde\commands;

use Illuminate\Console\Command;
use App\School;

class InsertGenikisLyceum extends Command
{
    private $records = array();

    protected $genikisDivisions = [
        'a' => 19,
        'b' => 20,
        'c' => 21
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kena:genikis_lykeiou';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert Number of Divisions for Genikis Paideias For Lykeia';

    protected $pathGenikis = 'seeds/data/genikis_lyceum.csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $columns_names = array();

        $this->comment("\nΕΙΣΑΓΩΓΗ ΤΜΗΜΑΤΩΝ ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ ΓΙΑ ΤΑ ΛΥΚΕΙΑ");

        $total = $this->getNumberOfRecords($this->pathGenikis);

        if ( ($handle = fopen(database_path($this->pathGenikis),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getRecords($handle, $columns_names);

            $bar = $this->output->createProgressBar($total);

            foreach($this->records as $division){

                $school = School::where('identifier', $division['id'])->first();

                if($school != null){
                    foreach($this->genikisDivisions as $key=>$value){
                        if($division['class'] == $key){
                            $school->divisions()->attach($value,[
                                'number'    => $division['provlepomeno'],
                                'students'  => $division['students'],
                                'numberSchoolProvide'   => $division['number']
                            ]);
                        }
                    }
                }

                $bar->advance();
            }

            $bar->finish();

        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getRecords($handle, $columns_names)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH LESSON
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->records[] = $record;
        }
        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfRecords($path)
    {
        $fp = file(database_path($path));
        return  count($fp) - 1;
    }


}
