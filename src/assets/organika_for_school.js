// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import VTooltip from 'v-tooltip';

import Notifications from 'vue-notification';
import velocity from 'velocity-animate';

import Swalify from './Classes/Swalify';
import DatePickerCustomize from './Classes/DatePickerCustomize';

import Alert from './../../../../../resources/assets/js/components/Alert.vue';
import NewAlert from './../../../../../resources/assets/js/components/NewAlert.vue';

import DatePicker from 'vue2-datepicker';

import VueHtmlToPaper from 'vue-html-to-paper';

import countdown from './components/Tests/Countdown.vue';

import Hours from './Classes/Hours'
import { turquoise } from 'color-name';

Vue.use(velocity, {
    name : 'foo'
});

Vue.use(Notifications);

const options = {
    name: '_blank',
    specs: [
      'fullscreen=yes',
      'titlebar=yes',
      'scrollbars=yes'
    ],
    styles: [
      'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
      'https://unpkg.com/kidlat-css/css/kidlat.css'
    ]
  }

Vue.use(VueHtmlToPaper, options);

window.Swalify = Swalify;

Vue.use(VTooltip);

new Vue({
    el : "#organika_school_teachers",

    components : {
        Alert, DatePicker, countdown, NewAlert
    },

    data : {
        set_countdown_organika_anikontes : window.set_countdown_organika_anikontes,
        countdown_for_organika_teachers : new Date(`${window.countdown_for_organika_teachers}`),

        hideLoader : false,
    
        loading : true,

        token : window.api_token,
        base_url : window.base_url,

        organika_anikontes : [],

        datePickerClass : new DatePickerCustomize(),

        updated : false,

        school_type : null,

        school_name : null,

    },

    mounted(){
        this.fetchOrganika();
    },

    methods : {

        updateTeacher(teacher){
            axios.post('/api/school/organika/updateTeacherOrganika?api_token='+this.token,{
                teacher : teacher
            })
                .then(r => {
                    if(r.data === 'is_checked'){
                        this.notifyMessage({
                            title: 'Σημείωση!',
                            body: `Ο Εκπαιδευτικός <strong>${teacher.last_name} ${teacher.first_name} [${teacher.am}]</strong> ελέγχθηκε...`,
                            type: 'success',
                            duration: 2000,
                            speed: 1000
                        })

                        // this.isUpdated();
                    }else if(r.data === 'not_checked'){
                        this.notifyMessage({
                            title: 'Προσοχή!',
                            body: `Ο Εκπαιδευτικός <strong>${teacher.last_name} ${teacher.first_name} [${teacher.am}]</strong> είναι σε ΣΤΑΔΙΟ ΕΛΕΓΧΟΥ`,
                            type: 'warning',
                            duration: 40000,
                            speed: 500
                        })
                    }else{
                        this.notifyMessage({
                            title: 'Προσοχή!',
                            body: `Κάτι πήγε στραβά!!! Error 7495`,
                            type: 'error',
                            duration: 40000,
                            speed: 500
                        })
                    }
                })
                .catch(e => this.notifyMessage({
                    title: 'Σφάλμα 459!',
                    body: `Κάτι πήγε στραβά`,
                    type: 'error',
                    duration: 40000,
                    speed: 500
                }))  

        },

        printPage(){
            // this.$htmlToPaper('eidikotites_table'); //table_eidikotites
            var divToPrint=document.getElementById("table_eidikotites");
            var newWin= window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
        },


        fetchOrganika(){
            axios.get('/api/school/organika/getOrganikaTeachers?api_token='+this.token)
                .then(r => {   
                    console.log(r.data)    
                    this.organika_anikontes = r.data;             
                    // this.eidikotites = r.data['leitourgika_kena'];
                    // this.other_eidikotites = r.data['other_eidikotites'];
                    // this.school_type = r.data['school_type'];
                    // this.school_name = r.data['school_name'];
                    // this.last_update = r.data['last_update'];
                    this.loading = false;
                    // this.has_unlocked = Number.isInteger(r.data['has_unlocked']);
                })
                .catch(e => console.log(e.response))  
        },
 
        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },

        countdown_finished(){
            this.loading = true;

            setTimeout(() => {
                this.fetchOrganika()
                this.set_countdown_organika_anikontes = false;
            }, 7000);
        },

        notifyMessage(data){
            this.$refs.alert.show('custom-template', data.title, data.body, data.type, 1000, 1000);
        },
    }

});