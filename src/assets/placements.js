// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import VTooltip from 'v-tooltip';

import Swalify from './Classes/Swalify';
import DatePickerCustomize from './Classes/DatePickerCustomize';

import OldPlacements from './components/placements/OldPlacements.vue';
import NewPlacement from './components/placements/NewPlacement.vue';
import TeacherProfileInPlacement from './components/placements/TeacherProfileInPlacement.vue';
import PlacementsForInsertInDatabase from './components/placements/PlacementsForInsertInDatabase.vue';

import Alert from './../../../../../resources/assets/js/components/Alert.vue';
import DatePicker from 'vue2-datepicker';

window.Swalify = Swalify;

Vue.use(VTooltip);

new Vue({
    el : "#app",

    components : {
        Alert, DatePicker, OldPlacements, NewPlacement, TeacherProfileInPlacement, PlacementsForInsertInDatabase
    },

    data : {
        deletePermanent : new Swalify({
            title : 'Προσοχή!!',
            type : 'info',
            text : 'Θα γίνει Οριστική ΔΙΑΓΡΑΦΗ χωρίς αποθήκευση της τοποθέτησης',
            buttonText : 'Ναι, Διαγράψτε την',
            success_title : 'Συγχαρητήρια!',
            success_text  : 'η τοποθέτηση διεγράφη'
        }),

        covid19 : "yes",
        dieuthidi: false,
        current_data_plus_one : window.current_data_plus_one,
        next_june_of_cuurent_year : window.next_june_of_cuurent_year,
        
        loading : true,
        displayTeachersList : false,
        teacherLoader : false,
        displayCurrentTeacherProfile : false,
        hideLoader : true,


        datePickerClass : new DatePickerCustomize(),

        token : window.api_token,
        base_url : window.base_url,

        kladoi : [],
        teachers : [],
        allPlacementsTypes : [],
        placements : [],

        praxi_date : null,
        klados_name : null,
        praxi_number : null,
        currentTeacher : null
    },

    computed : {
        displaySubmitButton(){
            return (this.praxi_number !== null && this.praxi_date !== null && this.klados_name !== null && this.currentTeacher !== null && this.placements.length);
        }
    },

    mounted(){
        this.fetchData();
    },

    methods : {
        initializeForm(){
            this.currentTeacher = null;
            this.displayCurrentTeacherProfile = false;
            this.displayTeachersList = false;
            this.klados_name = null;
            this.teachers = [];
            this.placements = [];
        },

        oldPlacementDeleted(){
            this.displayAlert('Συγχαρητήρια!', ['Η διαγραφή της τοποθέτησης έγινε με επιτυχία...'], 'danger');
        },

        oldPermanentPlacementDeleted(){
            this.deletePermanent.submit('https://www.google.gr');
            // this.displayAlert('Προσοχή!', ['Έγινε ΟΡΙΣΤΙΚΗ ΔΙΑΓΡΑΦΗ της τοποθέτησης'], 'danger');
        },

        oldPlacementRecalled(){
            this.displayAlert('Συγχαρητήρια!', ['Η ανάκληση της τοποθέτησης έγινε με επιτυχία...'], 'success');
        },


        createExcel(){
            this.hideLoader = false;

            axios.post('/api/admin/placements/createExcel?api_token='+this.token, {
                praxi_number : this.praxi_number
            })
            .then(r => {
                this.initializeForm();
                this.hideLoader = true;
                this.displayAlert('Συγχαρητήρια!', r.data.message, 'success');
            })
            .catch(e => console.log(e.response)) 
        },

        submitPlacements(){
            this.hideLoader = false;

            axios.post('/api/admin/placements/savePlacements?api_token='+this.token, {
                placements : this.placements,
                praxi_date : this.praxi_date,
                praxi_number : this.praxi_number,
                covid19 : this.covid19,
                dieuthidi: this.dieuthidi
            })
            .then(r => {
                console.log(r.data)
                this.initializeForm();
                this.hideLoader = true;
                this.displayAlert('Συγχαρητήρια!', r.data.message, 'success');
            })
            .catch(e => console.log(e.response))  
        },

        addPlacement(newRec){
            this.pushPlacement(newRec);
        },

        pushPlacement (newRec){
            this.currentTeacher.placements.push(Object.assign({}, newRec));

            this.deleteTeacherFromPlacements(this.currentTeacher);
            this.placements.push(this.currentTeacher);
        },

        deleteTeacherFromPlacements(teacher){
            let indexOf = this.placements.findIndex(obj => obj.afm === teacher.afm);
            this.$delete(this.placements, indexOf);
        },

        deleteCurrentTeacherPlacements(){
            this.currentTeacher.placements = [];
        },

        fetchData(){
            axios.get('/api/admin/placements/fetchData?api_token='+this.token)
                .then(r => {
                    this.kladoi = r.data['kladoi'];
                    this.allPlacementsTypes = r.data['allPlacementsTypes'];
                    this.loading = false;
                })
                .catch(e => console.log(e.response))    
        },

        getTeachers(){
            this.teacherLoader = true;
            this.displayTeachersList = true;
            this.displayCurrentTeacherProfile = false;

            axios.post('/api/admin/placements/getTeachers?api_token='+this.token, {
                klados_name : this.klados_name
            })
            .then(r => {
                this.teachers = r.data;
                this.teacherLoader = false;
                this.displayCurrentTeacherProfile = false;
                this.covid19 = this.klados_name.includes(".50") ? "no" : "yes";
            })
            .catch(e => console.log(e.response))  
        },

        displayProfile(){
            this.displayCurrentTeacherProfile = true;
        },

        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },
    }
});