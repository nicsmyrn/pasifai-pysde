// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import Modal from './components/Modal.vue';

var parent = new Vue ({
    el : '#app',

    data : {
        showModal : false,
        hideLoader : true,
        wants : true,
        openAgreementModal : false,

    },

    methods : {
        openModal (){
            this.openAgreementModal = true;
        },

        cancelOperation(){
            console.log('cancel');
        },

        sentRequest (){
            this.hideLoader = false;
            axios.post('/aj/yperarithmia/save',{
                want_yperarithmia : parent.wants
            })
            .then(r => {
                location.href = '/ΑΙΤΗΣΕΙΣ/Οργανικές/Αίτηση-Υπεραριθμίας';
                this.hideLoader = true;
            })
            .catch(r => console.log('Error'));
        }
    },

    components : {
        Modal
    }
});
