// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import Notifications from 'vue-notification';
import velocity from 'velocity-animate';

import NewAlert from './../../../../../resources/assets/js/components/NewAlert.vue';
import GeneralTable from './components/Organika/Admin/GeneralTable.vue';


Vue.use(velocity, {
    name : 'foo'
});

Vue.use(Notifications);

new Vue({
    el : "#vacuum",

    components : {
        NewAlert, GeneralTable
    },

    data : {
        hideLoader : true,

        token : window.api_token,
        base_url : window.base_url
    },

    methods : {
        displayLoader(){
            this.hideLoader = false;
        },

        notDisplayLoader(){
            this.hideLoader = true;
        },

        notifyMessage(data){
            this.$refs.alert.show('custom-template', data.title, data.body, data.type, data.duration, data.speed);
        }
    }
});