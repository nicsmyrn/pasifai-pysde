window.axios = require('axios');

let Vue = require('vue');

window.Event = new Vue();

import VTooltip from 'v-tooltip';

import Swalify from './Classes/Swalify';

window.Swalify = Swalify;

Vue.use(VTooltip);

new Vue({
    el: '#app',

    data : {
        token : window.api_token,
        base_url : window.base_url,
        requests : [],
        loading : true,
        dataRecall : new Swalify({
            title : 'Προσοχή!!',
            type : 'warning',
            text : 'Η ανάκληση της αίτησης θα σταλεί στη Γραμματεία του ΠΥΣΔΕ. Σε περίπτωση έγκρισης μπορείτε να υποβάλετε εκ νέου Αίτηση.',
            buttonText : 'Ναι, αιτούμαι ανάκληση της',
            success_title : 'Συγχαρητήρια!',
            success_text  : 'το αίτημα ανάκλησης στάλθηκε με επιτυχία'
        }),
        dataDelete : new Swalify({
            title : 'Προσοχή!!',
            type : 'error',
            text : 'Η Αίτηση διαγράφεται Οριστικά και ΔΕ λαμβάνεται υπόψη',
            buttonText : 'Ναι, διέγραψέ την',
            success_title : 'Συγχαρητήρια!',
            success_text  : 'η αίτηση διεγράφη'
        })
    },


    mounted ()  {
        this.fetchRequests();
    },

    methods : {
        fetchRequests (){
            axios.get('/api/requests/allRequests?api_token='+this.token)
                .then(r => {
                    this.requests = r.data;
                    this.loading = false;
                })
                .catch(e => console.log(e));
        },

        recall(unique_id){
            this.dataRecall.submit(`${this.base_url}/api/requests/recall/${unique_id}?api_token=${this.token}`);
        },

        destroy(unique_id){
            this.dataDelete.submit(`${this.base_url}/api/requests/destroy/${unique_id}?api_token=${this.token}`);
        }
    }
});