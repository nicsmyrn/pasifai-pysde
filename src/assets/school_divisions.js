// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import Lyceum from './OrganikaLyceum.vue';
import Gymnasium from './OrganikaGymnasium.vue';
import Other from './OrganikaOther.vue';

import Notifications from 'vue-notification';
import velocity from 'velocity-animate';

import NewAlert from './../../../../../resources/assets/js/components/NewAlert.vue';

Vue.use(velocity, {
    name : 'foo'
});

Vue.use(Notifications);

new Vue({
    el : "#app",

    components : {
        Lyceum, Gymnasium, NewAlert, Other
    },

    data : {
        hideLoader : true,

        token : window.api_token,
        base_url : window.base_url
    },

    methods : {
        displayLoader(){
            this.hideLoader = false;
        },

        notDisplayLoader(){
            this.hideLoader = true;
        },

        notifyMessage(data){
            this.$refs.alert.show('custom-template', data.title, data.body, data.type, data.duration, data.speed);
        }
    }
});