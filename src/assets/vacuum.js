// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import Notifications from 'vue-notification';
import velocity from 'velocity-animate';

import NewAlert from './../../../../../resources/assets/js/components/NewAlert.vue';

import KenaPleonasmata from './components/Organika/Admin/KenaPleonasmata.vue';
import PhysikesEpistimes from './components/Organika/Admin/Pe04.vue';
import LyceumDivisions from './components/Organika/Admin/LyceumDivisions.vue';
import GymnasiumDivisions from './components/Organika/Admin/GymnasiumDivisions.vue';
import EpalDivisions from './components/Organika/Admin/EpalDivisions.vue';

Vue.use(velocity, {
    name : 'foo'
});

Vue.use(Notifications);

new Vue({
    el : "#vacuum",

    components : {
        NewAlert, KenaPleonasmata, PhysikesEpistimes, LyceumDivisions, GymnasiumDivisions, EpalDivisions
    },

    data : {
        hideLoader : false,
        openTeachersModal : false,
        runningAlgorithm : false,

        flag : true,
        schools : null,
        school_selected : window.school_name,
        loading : false,
        eidikotites : [],
        divisions : null,
        divisionsChanged : false,

        teachers : [],
        students : [],
        eidikotitaName : null,
        school_type : null,

        isPositive : true,
        notInteger : false,
        displaySaveButton : true,
        displayCouncilButton : false,

        projects : [],
        list_projects : [],

        sumPe04Diff : 0
    },

    mounted(){        
        this.fetchSchools();
        if(this.school_selected !== 'null'){
            this.getSchoolName(this.school_selected);
        }
    },

    methods : {
        getSchoolName (name){
            this.loading = true;
            let school_name = name.replace(/ /g, '-');

            axios.get('/aj/vacuum/getKenaOfSchool/'+school_name)
                .then(r => {
                    // console.log(r.data);
                    this.eidikotites = r.data['kena'];
                    this.divisions = r.data['divisions'];
                    this.students = r.data['students'];
                    this.school_type = r.data['school_type'];

                    if(r.data['school_type'] === 'lyk'){
                        this.projects = r.data['projects'];
                        this.list_projects = r.data['list_projects'];
                    }

                    this.loading = false;
                })
                .catch(e => console.log(e.response))
        },

        displayLoader(){
            this.hideLoader = false;
        },

        notDisplayLoader(){
            this.hideLoader = true;
        },

        notifyMessage(data){
            this.$refs.alert.show('custom-template', data.title, data.body, data.type, data.duration, data.speed);
        },

        saveCouncilChanges(){
            let school_name = this.school_selected.replace(/ /g, '-');

            axios.post('/aj/vacuum/saveCouncilChanges/'+school_name, {
                eidikotites : this.eidikotites,
                diffPe04 : this.sumPe04Diff
            })
                .then(r => {                    
                    this.getSchoolName(this.school_selected);
                    this.notifyMessage({
                        title : 'Συγχαρητήρια!',
                        body : 'οι αλλαγές του Συμβουλίου ΑΠΟΘΗΚΕΥΤΗΚΑΝ',
                        type : 'success'
                    });
                    this.displayCouncilButton = false;
                })
                .catch(r => {
                    this.notifyMessage({
                        title : 'Error 583!',
                        body : r.response.data.message,
                        type : 'error'
                    });
                })
                
        },

        fetchSchools(){
            axios.get('/aj/vacuum/fetchSchools')
                .then(r => this.schools = r.data)
                .catch(e => console.log(e.response))     
        },



        runAlgorithm(){
            this.runningAlgorithm = true;
            let school_name = this.school_selected.replace(/ /g, '-');

            axios.post('/aj/vacuum/runAlgorithm/'+school_name, {
                divisions : this.divisions,
                projects  : this.projects
            })
                .then(r => {
                    this.divisionsChanged = false;
                    this.getSchoolName(this.school_selected);

                    this.notifyMessage({
                        title : 'Συγχαρητήρια!',
                        body : 'οι αλλαγές αποθηκεύτηκαν με επιτυχία και ο αλγόριθμος έτρεξε...',
                        type : 'success'
                    });

                    this.runningAlgorithm = false;
                })
                .catch(r => this.displayAlert('Προσοχή!', 'Λάθος 30.94', 'danger', true));
        },

        initializeAlgorithm(){
            console.log('run... initializeAlgorithm ...');
            let school_name = this.school_selected.replace(/ /g, '-');
            console.log(school_name);

            axios.post('/aj/vacuum/initializeAlgorithm/'+school_name, {
                divisions : this.divisions,
                students  : this.students
            })
                .then(r => {
                    console.log(r.data);
                })
                .catch(r => console.log('Error242456667'));
        },

        changeSumDiff(value){
            this.sumPe04Diff = value;
        }

    }
});