// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

new Vue({
    el : "#app",

    data : {
        years : '',
        months : '',
        days : '',
        kids : 0,
        sinipiretisi : 0,
        entopiotita : 0,
        family : 0,

        idiou : 0,
        sizigou : 0,
        paidion : 0,
        goneon : 0,
        aderfon : 0,
        exosomatiki : 0,

        sovaroi : false
    },

    computed : {
        calculator : function(){
            let sum = 0;
            let years = Number.parseInt(this.years);
            let months = Number.parseInt(this.months);
            let days = Number.parseInt(this.days);
            let kids = Number.parseInt(this.kids);
            let sinipiretisi = Number.parseInt(this.sinipiretisi);
            let entopiotita = Number.parseInt(this.entopiotita);
            let family = Number.parseInt(this.family);
            let idiou = Number.parseInt(this.idiou);
            let sizigou = Number.parseInt(this.sizigou);
            let paidion = Number.parseInt(this.paidion);
            let goneon = Number.parseInt(this.goneon);
            let aderfon = Number.parseInt(this.aderfon);
            let exosomatiki = Number.parseInt(this.exosomatiki);
            let proipiresia = 0;


            idiou = this.calculateIdionSizigouTeknon(idiou);
            sizigou = this.calculateIdionSizigouTeknon(sizigou);
            paidion = this.calculateIdionSizigouTeknon(paidion);

            goneon = this.calculateGoneon(goneon);
            aderfon = this.calculateAderfon(aderfon);
            exosomatiki = this.calculateExosomatiki(exosomatiki);

            kids = this.calculateKids(kids);
            family = this.calculateFamilyMoria(family, kids);

            proipiresia = this.calculateProipiresia(years, days, months);

            sinipiretisi = this.getSinipiretisiMoria(this.sinipiretisi);
            entopiotita = this.getEntopiotita(this.entopiotita);

            sum = proipiresia + sinipiretisi + entopiotita + family + kids + exosomatiki + aderfon + goneon + paidion + sizigou + idiou;

            if (this.isInteger(sum)) return sum;
            else return sum.toFixed(3);

        }
    },
    
    methods : {
        initSovaroi : function(){
            this.idiou = 0;
            this.sizigou = 0;
            this.paidion = 0;
            this.goneon = 0;
            this.aderfon = 0;
            this.exosomatiki = 0;
        },
        changeSovaroi : function(){
            if (this.sovaroi){
                this.sovaroi = false;
                this.initSovaroi();
            }
            else this.sovaroi = true;
        },

        isInteger : function(x){
            return x % 1 === 0;
        },
        calculateIdionSizigouTeknon : function(value){
            switch (value){
                case 0:
                    value = 0;
                    break;
                case 1:
                    value = 5;
                    break;
                case 2:
                    value = 20;
                    break;
                case 3:
                    value = 30;
                    break;
                default :
                    value = 0;
            }
            return value;
        },
        calculateGoneon : function(value){
            switch (value){
                case 0:
                    value = 0;
                    break;
                case 1:
                    value = 1;
                    break;
                case 2:
                    value = 3;
                    break;
                default:
                    value = 0;
            }

            return value;
        },

        calculateAderfon : function(value){
            switch (value){
                case 0:
                    value = 0;
                    break;
                case 1:
                    value = 5;
                    break;
                default:
                    value = 0;
            }

            return value;
        },

        calculateExosomatiki : function(value){
            switch (value){
                case 0:
                    value = 0;
                    break;
                case 1:
                    value = 3;
                    break;
                default:
                    value = 0;
            }

            return value;
        },

        calculateKids : function(kids){
            if (kids == 1) kids = 5;
            else if (kids == 2) kids = 5 + 6;
            else if (kids == 3) kids = 5 + 6 + 8;
            else if (kids > 3) kids = 5 + 6 + 8 + (kids - 3) * 10;
            else kids = 0;

            return kids;
        },

        calculateFamilyMoria : function(family, kids){
            if (family == 1 || family == 2 || family == 4 || family == 6) family = 4;

            if (family == 3 && kids > 0) family = 12;
            else if (family == 3 && kids == 0) family = 4;

            if (family == 5 && kids > 0) family = 6;

            return family;
        },

        calculateProipiresia : function(years, days, months){
            let sintelestis = 1;
            let years_s = 0;

            if (years > 10 && years <= 20) {
                sintelestis = 1.5;
                years_s = 10 + (years - 10) * sintelestis;
            } else if (years > 20) {
                sintelestis = 2;
                years_s = 25 + (years - 20) * sintelestis;
            } else if (years >= 1 && years <= 10) {
                years_s = years;
            } else years_s = 0;

            if (days >= 15 && days <= 31) months = months + 1;

            if (years == 10) sintelestis = 1.5;
            if (years == 20) sintelestis = 2;

            if (months >= 1 && months <= 12) {
                months = (months / 12) * sintelestis;
            } else  months = 0;

            return  years_s + months;
        },

        getSinipiretisiMoria : function(value){
            let sinipiretisi = 0;

            if (value == 1) sinipiretisi = 10;

            return sinipiretisi;
        },

        getEntopiotita : function(value){
            let entopiotita = 0;

            if (value == 1) entopiotita = 4;

            return entopiotita;
        }
    }
});