// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import VTooltip from 'v-tooltip';

import Swalify from './Classes/Swalify';
import DatePickerCustomize from './Classes/DatePickerCustomize';

import Alert from './../../../../../resources/assets/js/components/Alert.vue';
import DatePicker from 'vue2-datepicker';

import countdown from './components/Tests/Countdown.vue';

window.Swalify = Swalify;

Vue.use(VTooltip);

new Vue({
    el : "#leitourgika_school",

    components : {
        Alert, DatePicker, countdown
    },

    data : {

        set_countdown : window.set_countdown,
        countdown_date : new Date(`${window.countdown_date}`),
        can_edit_hours : window.can_edit_hours,

        hideLoader : false,
    
        flag : true,
        loading : true,

        school_type : null,

        token : window.api_token,
        base_url : window.base_url,

        teachers : [],

        all_types : [],
        editable_types : [],
        editable_types_list : [],

        datePickerClass : new DatePickerCustomize(),

        updated : false,

        loading_button : false,

        uploadFile: null,

        global_file_upload : null,
        globalLoadingSpinner : false,

        list_of_types : [0,1,5,6,7,8,10,12,13]
    },

    mounted(){
        console.log('start fetching 2332323232')

        this.fetchTeachers();
    },

    methods : {
        clickTheFileElement(teacher){
            console.log(teacher.afm)
            this.$refs[`${teacher.afm}`][0].click()

        },

        clickTheFileElementGlobal(teacher){
            console.log('global ')
            this.$refs.globalFile.click()
        },

        prepareForAttachment(teacher){
            let formData = new FormData()

            formData.append('uploadFile', this.$refs[`${teacher.afm}`][0].files[0])

            teacher.file_upload = formData
        },

        prepareForAttachmentGlobal(){
            let formData = new FormData()

            formData.append('uploadFile', this.$refs.globalFile.files[0])

            this.global_file_upload = formData
        },

        resetUpload(teacher){
            teacher.file_upload = null
        },

        uploadTeacherAttach(teacher){
            teacher.loadingSpinner = true

            axios.post('/api/school/leitourgika/uploadTeacherFile?api_token='+this.token+'&afm='+teacher.afm+'&sch_id='+teacher.pivot.sch_id, teacher.file_upload, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
                .then(r => {
                    teacher.file_uploaded = true
                    this.displayAlert('Συγχαρητήρια!', ['Η πράξη ανάληψης έχει σταλεί στη ΔΔΕ'], 'success');
                    teacher.loadingSpinner = false
                })
                .catch(e => {
                    if(e.response.status === 413){
                        this.displayAlert('Προσοχή!', 'Nginx problem!, είναι αδύνατο το ανέβασμα του συγκεκριμένου εγγράφου', 'danger');
                    }else if(e.response.status === 422){
                        this.displayAlert('Προσοχή!', e.response.data.errors.uploadFile[0], 'danger');
                    }else{
                        this.displayAlert('Προσοχή!', 'General Error!!! No 324485', 'danger');
                        console.log(e.response)
                    }
                    teacher.file_uploaded = false
                    teacher.loadingSpinner = false
                    teacher.file_upload = null
                })  

        },

        uploadGlobalAttach(){
            this.globalLoadingSpinner = true

            axios.post('/api/school/leitourgika/uploadGlobalFile?api_token='+this.token, this.global_file_upload, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
                .then(r => {
                    this.globalLoadingSpinner = true
                    this.displayAlert('Συγχαρητήρια!', ['Η ΚΕΝΤΡΙΚΗ πράξη ανάληψης έχει σταλεί στη ΔΔΕ'], 'success');
                    this.globalLoadingSpinner = false
                    this.global_file_upload = null
                })
                .catch(e => console.log(e.response))  
        },

        isUpdated(teacher){
            teacher.pivot.rowUpdated = true;
            this.updated = true;
        },

        updateTeacher(teacher){
            // let type = parseInt(teacher.pivot.teacher_type);
            // if (type !== 0){
                // teacher.pivot.hours = 0;
                // teacher.pivot.project_hours = 0;
            // }else{
                teacher.pivot.hours = 0;
                teacher.pivot.project_hours = 0;
            // }
            this.isUpdated(teacher);
        },

        isOrganika(teacher){
            let type = parseInt(teacher.pivot.teacher_type);

            return type === 0;
        },

        isSchoolAdmin(teacher){
            let type = parseInt(teacher.pivot.teacher_type);

            return type === 10;
        },
        

        fetchTeachers(){
            axios.get('/api/school/leitourgika/getTeachers?api_token='+this.token)
                .then(r => {
                    this.teachers = r.data['topothetisis'];
                    this.all_types = r.data['all_types'];
                    this.editable_types = r.data['editable_types'];
                    this.editable_types_list = r.data['editable_types_list'];
                    this.school_type = r.data['school_type'];

                    this.loading = false;
                })
                .catch(e => console.log(e.response))  
        },

        isEditable(teacher){
            return this.editable_types_list.includes(parseInt(teacher.pivot.teacher_type));
        },

        changeDate(teacher){
            this.teachersUpdated = true;
            this.setUpdated(teacher);
        },

        updateDifferenceHours(teacher){
            teacher.pivot.hours = this.checkIfIsPositive(teacher.pivot.hours);
            teacher.pivot.project_hours = this.checkIfIsPositive(teacher.pivot.project_hours);
            this.isUpdated(teacher);
        },

        updateMeiwsiHours(teacher){
            teacher.meiwsi = this.checkIfIsPositive(teacher.meiwsi);
            teacher.pivot.hours = teacher.ypoxreotiko - teacher.meiwsi
            this.isUpdated(teacher);
        },

        calculateSumHours(teacher){
            return (teacher.ypoxreotiko - teacher.meiwsi);
        },

        calculateDifference(teacher){
            return (this.calculateSumHours(teacher) - teacher.pivot.hours - teacher.pivot.project_hours - teacher.sum_other_topothetisis);
        },

        saveTeachers(){
            this.loading_button = true;
            axios.post('/api/school/leitourgika/saveTeachers?api_token='+this.token,{
                teachers : this.teachers
            })
                .then(r => {
                    this.updated = false;
                    this.loading_button = false;
                    this.displayAlert(r.data['header'], r.data['message'], r.data['type']);

                    setTimeout(() => {
                        location.reload()
                    },2000)
                })
                .catch(e => console.log(e.response))  

        },

        checkIfIsPositive(number){
            if(this.checkIfIsInteger(number)){
                if(number > 0){
                    return parseInt(number);
                }else if(number < 0){
                    this.displayAlert('Προσοχή!', 'ΔΕΝ έχετε εισάγει θετικό αριθμό', 'danger');
                }else{
                    return null;
                }
            }
            this.displayAlert('Προσοχή!', 'ο αριθμός ΔΕΝ είναι ακέραιος', 'danger');
            return null;
        },

        checkIfIsInteger(number){
            return ! isNaN(number);
        },
 
        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },

        countdown_finished(){
            this.loading = true;

            setTimeout(() => {
                this.fetchTeachers()
                this.set_countdown = false;
            }, 2000);
        }
    }

});