// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import Notifications from 'vue-notification';
import velocity from 'velocity-animate';

import SchoolType from '../assets/components/Organika/Admin/Anatheseis/SchoolType.vue'
import NewAlert from './../../../../../resources/assets/js/components/NewAlert.vue';

import Swalify from './Classes/Swalify';

window.Swalify = Swalify;

Vue.use(velocity, {
    name : 'foo'
});

Vue.use(Notifications);


new Vue({
    el : "#anatheseis",

    components: {
        SchoolType, NewAlert
    },

    data : {
        message: "Ωρολόγιο Πρόγραμμα - Αναθέσεις Μαθημάτων",
        token : window.api_token,
        base_url : window.base_url,
        divisions: null
    },

    mounted(){        
        this.fetchEidikotites()
    },

    methods: {
        fetchEidikotites(){
            axios.get('/api/admin/organika/getDivisions?api_token='+this.token)
                .then(r => {  
                    this.divisions = r.data
                })
                .catch(e => console.log(e.response))  
        },

        notifyMessage(data){
            this.$refs.alert.show('custom-template', data.title, data.body, data.type, 1000, 1000);
        }

    }

});