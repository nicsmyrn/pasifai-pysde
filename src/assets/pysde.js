// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();


// import Form from '../core/Forms';
import TeacherModule from './components/TeacherRequestModule.vue';
import ButtonsRequest from './components/ButtonsRequest.vue';
import Modal from './components/Modal.vue';

import Alert from './../../../../../resources/assets/js/components/Alert.vue';


import VTooltip from 'v-tooltip';

Vue.use(VTooltip);

const pysde = new Vue({
    el: '#app',

    components : {
        TeacherModule, ButtonsRequest, Modal, Alert
    },

    data : {
        organiki : null,

        listStay : [0, 1],

        currentReason : '',

        hasOrganiki : null,
        stay_to_organiki : 0, // -1,
        hours_for_request : 0,

        loader : true,
        hasMadeRequest : false,

        unique_id : window.UniqueId,
        old_unique_id : '',
        aitisi_type : window.RequestType,

        allSchoolsOther : [],
        allSchoolsIdiasOmadas : [],
        allSchoolsOmoron : [],
        description_idia : '',
        description_omores : '',
        description_other : '',

        selectedSchoolsIdiasOmadas : [],
        selectedSchoolsOmores : [],
        selectedSchoolsOther : [],

        sameTeams : null,
        omoresTeams : null,

        requestExistsInDatabase : false,
        openDeleteModal : false,
        openAgreementModal : false,
        hideLoader : true,
        weFoundOtherRequest : false,
        url : '',

        sistegazomeno : 0,
        sizigos_stratiotikou : 0,
    },

    computed : {
        showDeleteModal (){
            return this.requestExistsInDatabase && this.openDeleteModal;
        },

        showYperarithmosQuestions (){
            return this.currentReason === 'Λειτουργικά Υπεράριθμος';
        },

        showYperarithmosModule (){
            // return (this.currentReason === 'Λειτουργικά Υπεράριθμος' && this.listStay.includes(this.stay_to_organiki) && this.hours_for_request <= 11 && this.hours_for_request >=0);
            return (this.currentReason === 'Λειτουργικά Υπεράριθμος');
        },
        showDiathesiModule (){
            return this.currentReason === 'Μετάθεση' || this.currentReason === 'Διάθεση' || this.currentReason === 'Διάθεση ΠΥΣΔΕ - ΝΕΟΔΙΟΡΙΣΤΟΣ';
        },
        showSiblirosiModule (){
            return this.currentReason === 'Συμπλήρωση' && this.hours_for_request > 0  && this.hours_for_request <= 30;
        },
        showApospasiEntosModule(){
            return this.currentReason === 'Απόσπαση Εντός'
        },
        showApospasiEktosModule(){
            return this.currentReason === 'Αναπληρωτής'
        },
        showCovid19(){
            return this.currentReason === 'Αναπληρωτής - Covid 19'
        },
        isCovid(){
            return this.aitisi_type === 'E5'
        }
    },

    mounted ()  {
        this.fetchSchools();
    },

    methods : {
        openAlert(){
            this.displayAlert('header...', 'body...', 'success');
        },
        goToArchives(){
            window.location.href = window.urlArchives;
        },

        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },
        cancelOperation (){
            this.displayAlert('Προσοχή!', 'Δεν πραγματοποιήθηκε καμία αλλαγή', 'danger');
            this.openDeleteModal = false;
            this.openAgreementModal = false;
            this.$refs.buttons.closeLoader();
        },
        deleteRequestOperation(){
            this.openDeleteModal = false;
            this.hideLoader = false;
            axios.post('/aj/leitourgika/permanentlyDeleteRequest', {
                unique_id : this.unique_id
            })
                .then(r => {
                    window.location.href = r.data;
                })
                .catch(r => console.log('Error 9824y92487'));
        },
        goToUrl (){
            window.location.href = this.url;
        },
        canMakeNewRequest(){
            this.weFoundOtherRequest = false;
        },

        makeDeleteModalOpen (){
            this.openDeleteModal = true;
        },

        makeAgreementModalOpen (){
            this.openAgreementModal = true;
        },

        saveRequest(){
            axios.post('/aj/leitourgika/saveRequestLeitourgika', {
                selectedSchoolsOther : this.selectedSchoolsOther,
                selectedSchoolsIdiasOmadas : this.selectedSchoolsIdiasOmadas,
                selectedSchoolsOmores : this.selectedSchoolsOmores,
                currentReason : this.currentReason,
                aitisi_type : this.aitisi_type,
                unique_id : this.unique_id,
                stay_to_organiki : this.stay_to_organiki,
                hours_for_request : this.hours_for_request,
                description_other : this.description_other,
                description_idia : this.description_idia,
                description_omores : this.description_omores,
                sistegazomeno : this.sistegazomeno,
                sizigos_stratiotikou : this.sizigos_stratiotikou
            })
                .then(r => {       
                    this.unique_id = r.data.unique_id;
                    this.displayAlert('Συγχαρητήρια!', r.data.message, 'success', true);
                    this.$refs.buttons.closeLoader();
                    this.requestExistsInDatabase = true;
                })
                .catch(r => console.log('Error242456667'));
        },

        sentRequest (){            
            this.openDeleteModal = false;
            this.openAgreementModal = false;
            this.hideLoader = false;

            axios.post('/aj/leitourgika/sentRequestForProtocolLeitourgika',{
                selectedSchoolsOther : this.selectedSchoolsOther,
                selectedSchoolsIdiasOmadas : this.selectedSchoolsIdiasOmadas,
                selectedSchoolsOmores : this.selectedSchoolsOmores,
                currentReason : this.currentReason,
                aitisi_type : this.aitisi_type,
                unique_id : this.unique_id,
                stay_to_organiki : this.stay_to_organiki,
                hours_for_request : this.hours_for_request,
                description_other : this.description_other,
                description_idia : this.description_idia,
                description_omores : this.description_omores,
                sistegazomeno : this.sistegazomeno,
                sizigos_stratiotikou : this.sizigos_stratiotikou
            })
                .then(r => {                  
                    location.href = r.data;
                    this.$refs.buttons.closeLoader();
                })
                .catch(r => console.log('Error 472497'));
        },

        fetchSchools (){     
            if(this.aitisi_type === 'E5'){                  // COVID 19
                axios.post('/aj/requests/ajaxFetchSchoolsCovid19', {
                    unique_id : this.unique_id,
                    request_type : this.aitisi_type
                })
                    .then(r => {        
                        this.currentReason = r.data.currentReason;
    
                        if(r.data.isMadeValue === 'protocol_given' ){
                            this.hasMadeRequest = true;
                        }else{ // no protocol given
                            this.checkExistanceInDatabase(r.data);
                            this.initDataForOrganiki(r.data);
                        } // end of protocol given check
    
                        setTimeout(function(){
                            this.loader = false;
                        }.bind(this),1500);

                    })
                    .catch(e => console.log(e));     

            }else{
                axios.post('/aj/requests/ajaxFetchSchools', {
                    unique_id : this.unique_id,
                    request_type : this.aitisi_type
                })
                    .then(r => {    
                        this.currentReason = r.data.currentReason;
    
                        if(r.data.isMadeValue === 'protocol_given' ){
                            this.hasMadeRequest = true;
                        }else{ // no protocol given
                            this.checkExistanceInDatabase(r.data);
                            this.initDataForOrganiki(r.data);
                        } // end of protocol given check
    
                        setTimeout(function(){
                            this.loader = false;
                        }.bind(this),1500);
    
                        // Vue.nextTick(function () {
                        //     $('[data-toggle="tooltip"]').tooltip()
                        // })
                    })
                    .catch(e => console.log(e));
            }
        },
        checkExistanceInDatabase(data){
            this.requestExistsInDatabase = data.requestExistsInDatabase;

            if(this.requestExistsInDatabase){                
                this.weFoundOtherRequest = data.weFoundOtherRequest;
                if(this.weFoundOtherRequest){
                    this.old_unique_id = data.old_unique_id;
                    this.url = data.url;
                }else{
                    this.hours_for_request = data.hours_for_request;
                    this.stay_to_organiki = data.stay_to_organiki;
                }
            }
        },
        initDataForOrganiki(data){
            console.log(data);

            this.hasOrganiki = data.hasOrganiki;
            this.description_other = data.description_other;

            if(this.hasOrganiki){
                this.organiki = data.organiki;

                this.allSchoolsIdiasOmadas = data.allSchoolsIdiasOmadas;
                this.allSchoolsOmoron = data.allSchoolsOmoron;
                this.allSchoolsOther = data.allSchools;
                this.sameTeam = data.sameTeam;
                this.omoresTeams = data.omoresTeams;

                this.selectedSchoolsIdiasOmadas = data.selectedSchoolsIdiaOmada;

                this.selectedSchoolsOmores = data.selectedSchoolsOmores;

                this.selectedSchoolsOther = data.selectedSchoolsOther;

                this.sistegazomeno = data.sistegazomeno;

                this.description_idia = data.description_idia;
                this.description_omores = data.description_omores;
            }else{
                this.allSchoolsOther = data.allSchools;
                this.selectedSchoolsOther = data.selectedSchoolsOther;
                this.sizigos_stratiotikou = data.sizigos_stratiotikou;
            }
        },

        updateIdiaOmadaSelectedSchools (selectedSchools){
            this.selectedSchoolsIdiasOmadas = selectedSchools;
        },
        updateOmoresSelectedSchools (selectedSchools){
            this.selectedSchoolsOmores = selectedSchools;
        },

        updateOtherSelectedSchools (selectedSchools){
            this.selectedSchoolsOther = selectedSchools;
        },

        updateIdiaOmadaDescription(description){
            this.description_idia = description;
        },
        updateOmoresDescription(description){
            this.description_omores = description;
        },
        updateOtherDescription(description){
            this.description_other = description;
        },

        checkHours(){
            this.hours_for_request = 0;
        }
    }
});