// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import ButtonsRequest from './components/ButtonsRequest.vue';
import Modal from './components/Modal.vue';
import Alert from './../../../../../resources/assets/js/components/Alert.vue';

var vacuum = new Vue({
    el : "#organikes",

    data : {
        loading : true,

        hideLoader : true,
        openAgreementModal : false,

        divisions : null,
        students : [],
        maxStudents : 27,
        maxComputer : 11,

        notInteger : false,
        isPositive : false
    },

    mounted(){
        this.fetchDivisions();
    },

    computed : {
        studentsLength() {
            return Object.keys(this.students).length;
        }
    },


    methods : {
        fetchDivisions(){
            this.loading = true;

            axios.get('/aj/divisions/fetchDivisions')
                .then(r => {
                    this.divisions = r.data['divisions'];
                    this.students = r.data['students'];
                    this.loading = false;
                })
                .catch(r => {
                    this.hideLoader = true;
                    this.displayAlert('Προσοχή!', 'Λάθος 130.346', 'danger', true)
                });
        },

        sentRequest(){
            this.openAgreementModal = false;
            this.hideLoader = false;
            axios.post('/aj/divisions/saveDivisions', {
                divisions : this.divisions,
                students  : this.students
            })
                .then(r => {
                    this.displayAlert('Συγχαρητήρια!', r.data, 'info', true);
                    this.hideLoader = true;
                })
                .catch(r => {
                    this.hideLoader = true;
                    this.displayAlert('Προσοχή!', 'Λάθος 130.394', 'danger', true);
                });
        },

        openModal (){
            this.openAgreementModal = true;
        },

        calculateClass(className){
            this.notInteger = false;
            for(let cell in this.students){
                var cellInt = parseInt(this.students[cell]);
                if(isNaN(cellInt)){
                    this.notInteger = true;
                }
            }

            if(!this.notInteger){
                for(let key in this.divisions){
                    if(this.divisions.hasOwnProperty(key)){
                        if(key === 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'){
                            this.calculateAllDivs(className);
                        }
                    }
                }
            }
        },

        calculateAllDivs(className){
            let numberOfStudents = this.students[className];

            let mod_value = numberOfStudents % this.maxStudents;

            var digitalDivision = 0;
            if(mod_value == 0){
                digitalDivision = Math.floor(numberOfStudents/this.maxStudents);
            }else{
                digitalDivision = Math.floor(numberOfStudents/this.maxStudents) + 1;
            }

            let computerDivision = Math.floor(numberOfStudents/this.maxComputer);

            if(computerDivision <= 0) computerDivision = 1;

            if(this.cellIsInteger(digitalDivision)){
                this.putNewDivisionValue('ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ', className, digitalDivision, true);
                this.calculateNotGeneralDivs(className, digitalDivision, computerDivision, true);
            }
        },

        cellIsInteger(value){
            this.notInteger = false;
            if(isNaN(parseInt(value))){
                this.notInteger = true;
            }else{
                this.cellIsPositive(value);
            }
            return ! this.notInteger;
        },

        cellIsPositive(value){
            this.isPositive = false;
            if(value <= 0){
                this.isPositive = true;
            }
            return ! this.isPositive;
        },

        putNewDivisionValue(divisionName, className, value, autocalculate){
            if(!autocalculate){
                this.divisions[divisionName].find(e=> e.class === className).numberSchoolProvide = value;
            }else{
                this.divisions[divisionName].find(e=> e.class === className).numberSchoolProvide = value;
                this.divisions[divisionName].find(e=> e.class === className).number = value;
            }
        },

        getDivisionValue(divisionName, className){
            return this.divisions[divisionName].find(e=> e.class === className).numberSchoolProvide;
        },

        setSecondLanguage(div){
            let generalNumber = this.getDivisionValue('ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ', div.class);

            if(this.cellIsInteger(div.numberSchoolProvide)){
                if(div.status === 'unlocked'){
                    if(div.name === 'ΓΕΡΜΑΝΙΚΑ'){
                        this.putNewDivisionValue('ΓΑΛΛΙΚΑ', div.class, (generalNumber - div.numberSchoolProvide), false);
                    }else if(div.name === 'ΓΑΛΛΙΚΑ'){
                        this.putNewDivisionValue('ΓΕΡΜΑΝΙΚΑ', div.class, (generalNumber - div.numberSchoolProvide), false);
                    }else if(div.name === 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'){
                        generalNumber = div.numberSchoolProvide;
                        let computerDivision = Math.floor(this.students[div.class]/this.maxComputer);

                        this.putNewDivisionValue('ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ', div.class, generalNumber);
                        this.calculateNotGeneralDivs(div.class, generalNumber, computerDivision, false);
                    }
                }
            }
        },

        calculateNotGeneralDivs(className, digitalDivision, computerDivision, autocalculate){
            let french = Math.floor(digitalDivision/2) + (digitalDivision % 2);
            let german = digitalDivision - french;
            this.putNewDivisionValue('ΑΓΓΛΙΚΑ', className, digitalDivision,autocalculate);
            this.putNewDivisionValue('ΓΑΛΛΙΚΑ', className, french, autocalculate);
            this.putNewDivisionValue('ΓΕΡΜΑΝΙΚΑ', className, german, autocalculate);
            this.putNewDivisionValue('ΠΛΗΡΟΦΟΡΙΚΗΣ', className, computerDivision, autocalculate);
            this.putNewDivisionValue('ΤΕΧΝΟΛΟΓΙΑΣ', className, computerDivision, autocalculate);
        },

        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },
        cancelOperation (){
            this.displayAlert('Προσοχή!', 'Δεν πραγματοποιήθηκε καμία αλλαγή', 'danger');
            this.openAgreementModal = false;
        }
    },

    components : {
        ButtonsRequest, Modal, Alert
    }

});