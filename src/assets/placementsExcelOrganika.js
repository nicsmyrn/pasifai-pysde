// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import Swalify from './Classes/Swalify';

window.Swalify = Swalify;

new Vue({
    el : "#app",

    data : {
        hideLoader : true,
        token : window.api_token,
        praxi_number : null
    },

    methods : {
        createExcel(){
            this.hideLoader = false;

            axios.get('/api/admin/placements/createExcelOrganika/'+this.praxi_number+'?api_token='+this.token, {
                responseType: 'blob', // important
            })
            .then(response => {
                // console.log(response.data);

                let file_name = `${this.praxi_number}_ΠΡΑΞΗ_${this.getCurrentDate()}_ΟΡΓΑΝΙΚΑ.xlsx`;

                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', file_name); //or any other extension
                document.body.appendChild(link);
                link.click();
                this.hideLoader = true;
            })
            .catch(e => {
                alert('Δεν βρέθηκε η συγκεκριμένη πράξη');
                this.hideLoader = true;
                console.log(e.response);
            }) 
        },

        getCurrentDate(){
            var today = new Date();
            var dd = today.getDate();

            var mm = today.getMonth()+1; 
            var yyyy = today.getFullYear();

            if(dd<10) 
            {
                dd='0'+dd;
            } 

            if(mm<10) 
            {
                mm='0'+mm;
            } 

            return  dd+'_'+mm+'_'+yyyy;
        }
    }
});