
class DatePickerCustomize {
    constructor(){
        this.title = 'Επέλεξε Ημερομηνία';

        this.shortcuts = [
            {
              text: 'Σήμερα',
              onClick: () => {
                this.time3 = [ new Date(), new Date() ]
              }
            }
        ];

        this.timePickerOptions = {
            start: '00:00',
            step: '00:30',
            end: '23:30'
        };

    }

    setLang(title){
      this.title = title;

      return this.getLang();
    }

    getLang(){
      return {
        days: ['Κυρ', 'Δευ', 'Τρι', 'Τετ', 'Πεμ', 'Παρ', 'Σαβ'],
        months: ['Ιαν', 'Φεβ', 'Μαρ', 'Απρ', 'Μαι', 'Ιουν', 'Ιουλ', 'Αυγ', 'Σεπ', 'Οκτ', 'Νοε', 'Δεκ'],
        pickers: ['επόμενες 7 ημ', 'επόμενες 30 ημ', 'προηγούμενες 7 ημ', 'προηγούμενες 30 ημ'],
        placeholder: {
          date: this.title,
          dateRange: 'Επέλεξε εύρος Ημερομηνίας'
        }
      };
    }

    getShortcuts(){
      return this.shortcuts;
    }

    getTimePickerOptions(){
      return this.timePickerOptions;
    }

    getFormat(){
      return "DD/MM/YYYY";
    }

    getWidth(){
      return 120;
    }

    firstDayOfWeek(){
      return 1;
    }

    getClearable(){
      return false;
    }

    setDefaultValue(date){
      let newDate = new Date(date);

      return newDate; 
    }

    defaultValue(){
      let newDate = new Date('2024-06-30');
      // let newDate = new Date();

      return newDate;
    }

}

export default DatePickerCustomize