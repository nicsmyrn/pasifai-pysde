
class Swalify {
    constructor(data){
        this.originalData = data;

        for(let field in data){
            this[field] = data[field];
        }
    }

    data(){
        let data = {};

        for(let property in this.originalData){
            data[property] = this[property];
        }

        return data;
    }

    confirm(data){
        return new Promise(function(resolve, reject){
            Swal.fire({
                title: data.title,
                text: data.text,
                confirmButtonText: data.confirmText,
                cancelButtonText: data.denyText,
                showCancelButton : true,  
                type: data.type,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
              })
              .then((result) => {
                if (result.value) {
                    return resolve()
                } else  {
                    return reject()
                }
              })
              .catch(e => {
                return reject();
            })
        })
    }

    deletePlacement(url, data){
        return new Promise(function(resolve, reject){
            Swal.fire({
                title: data.title,
                text: data.message,
                confirmButtonText: data.buttonText,
                confirmButtonColor: data.color,
                showCancelButton: true,
                cancelButtonText: 'Ακύρωση',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return new Promise(function(resolve, reject) {
                        axios.post(url, data)
                            .then(function(r){
                                resolve(r.data);
                            })
                            .catch(function(e){
                                reject(e);
                            })
                    });
                }
              }).then((result) => {
                if (result.value) {
                    Swal.fire(data.successMsg, '', data.successType)
                    return resolve(result.value)
                } 
                Swal.fire(data.errorMsg, '', data.errorType)
                return reject();
              })
              .catch(e => {
                Swal.fire(data.errorMsg, '', data.errorType)
                return reject()
            })
        })
    }

    submit(ipAPI){
        Swal.fire({
            title: this.title,
            type: this.type,
            text: this.text,
            showCancelButton: true,
            cancelButtonText: 'Ακύρωση',
            confirmButtonText: this.buttonText,
            confirmButtonColor: "#DD6B55",
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return fetch(ipAPI)
                .then(response => {      
                    if (!response.ok) {        
                        throw new Error(response.statusText)
                    }                    
                    return response.json()
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `Σφάλμα 432: ${error}`
                    )
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title : this.success_title,
                        text :  this.success_text,
                        type : 'success'
                    });
                    setTimeout(function(){location.reload(true);}, 2000);
                    
                }
            });
    }


}

export default Swalify