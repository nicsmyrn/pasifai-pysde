
class Division {
    constructor(name, className, students){

        this.maxStudents = 27;

        this.maxStudentsGLykeiou = 25;

        this.minComputerStudents = 22;

        this.number = 0;

        this.french = 0;
        this.german = 0;
        this.computerNumber = 0;
        this.oldValue = 0;

        this.name = name;
        this.className = className;
        this.students = students;
    }

    getStudents(){
        return this.students;
    }

    setOldValue(value){
        this.oldValue = value;
    }

    getOldValue(){
        return this.oldValue;
    }

    setClassName(value){
        this.className = value;
    }

    getClassName(){
        return this.className;
    }
    
    setNumber(){
        let mod = this.students % this.maxStudents;
        let divisionNumber = 0;

        if(mod === 0){
            divisionNumber = Math.floor(this.students/this.maxStudents);
        }else{
            divisionNumber = Math.floor(this.students/this.maxStudents) + 1;
        }

        this.setFrench(divisionNumber);
        this.setGerman(divisionNumber);

        this.number = divisionNumber;
    }

    setNumberGlykeiou(){
        let mod = this.students % this.maxStudentsGLykeiou;
        let divisionNumber = 0;

        if(mod === 0){
            divisionNumber = Math.floor(this.students/this.maxStudentsGLykeiou);
        }else{
            divisionNumber = Math.floor(this.students/this.maxStudentsGLykeiou) + 1;
        }

        this.setFrench(divisionNumber);
        this.setGerman(divisionNumber);

        this.number = divisionNumber; 
    }
    
    setMyNumber(value){
        // this.setFrench(value);
        // this.setGerman(value);

        this.number = value;
    }

    getNumber (){
        return this.number;
    }

    setFrench(number){
        this.french = Math.floor(number / 2) + (number % 2);
    }

    getFrench(){
        return this.french;
    }

    setGerman(number){
        this.german = number - this.getFrench();
    }

    getGerman(){
        return this.german;
    }

    setComputerNumber(){
        let x = Math.floor(this.students / this.getNumber());
        let mod = this.students % this.getNumber();

        if(x >= this.minComputerStudents){
            this.computerNumber = 2 * this.getNumber();
        }else if(x < (this.minComputerStudents - 1)){
            this.computerNumber = this.getNumber();
        }else if(x === (this.minComputerStudents -1)){
            this.computerNumber = (2 * mod) + (this.getNumber() - mod);
        }
    }

    getComputerNumber(){
        return this.computerNumber;
    }

    setMaxStudents(value){
        this.maxStudents = value;
    }

    getMaxStudents(){
        return this.maxStudents;
    }

    checkIfIsPositive(){
        if(this.checkIfIsInteger(this.number)){
            if(this.number > 0){
                // this.$emit('change-made');
                let parsedNumber = parseInt(this.number);

                this.setMyNumber(parsedNumber)

                return parsedNumber;
            }
        }
        // this.$emit('zero-values');
        // this.displayAlert('Ο αριθμός <strong>ΔΕΝ</strong> είναι ακέραιος');
        this.setMyNumber(this.getOldValue());
        return null;
    }

    checkIfStudentsArePositive(){
        if(this.checkIfIsInteger(this.students)){
            if(this.students > 0){
                let parsedNumber = parseInt(this.students);

                this.students = parsedNumber;

                return parsedNumber;
            }
        }
        return null;
    }

    checkIfIsInteger(number){
        return ! isNaN(number);
    }
}

export default Division