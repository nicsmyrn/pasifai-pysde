
class Hours {
    constructor(value){
        this.number = value;
    }

    getNumber(){
        return this.number;
    }

    checkIfIsPositive(){
        if(this.checkIfIsInteger(this.number)){
            if(this.number > 0){
                let parsedNumber = parseInt(this.number);

                this.number = parsedNumber;

                return parsedNumber;
            }
        }
        return null;
    }

    checkIfIsInteger(number){
        return ! isNaN(number);
    }

    convertToInt (){
        var parsed = parseInt(this.number);
        if(isNaN(parsed)){
            return 0;
        }
        return parsed;
    }
}

export default Hours