
class NumberTextBox {
    constructor(forseen){

        this.number = forseen;

        this.oldValue = 0;

    }

    setOldValue(value){
        this.oldValue = value;
    }

    getOldValue(){
        return this.oldValue;
    }

    setMyNumber(value){
        this.number = value;
    }

    getNumber (){
        return this.number;
    }

    checkIfIsPositive(){
        if(this.checkIfIsInteger(this.number)){
            if(this.number > 0){
                // this.$emit('change-made');
                let parsedNumber = parseInt(this.number);

                this.setMyNumber(parsedNumber)

                return parsedNumber;
            }
        }
        // this.$emit('zero-values');
        // this.displayAlert('Ο αριθμός <strong>ΔΕΝ</strong> είναι ακέραιος');
        this.setMyNumber(this.getOldValue());
        return null;
    }


    checkIfIsInteger(number){
        return ! isNaN(number);
    }
}

export default NumberTextBox