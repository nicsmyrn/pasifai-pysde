
class Alert {
    constructor(){
        this.title = '';
        this.message = '';
        this.type = 'success';
        this.duration = 1000;
        this.speed = 300;
    }

    success(message){
        this.type = 'success';
        this.message = message;
        this.title = 'Συγχαρητήρια!'
        this.duration = 3000;

        return this.emit(message);
    }

    warning(message){
        this.type = 'warning';
        this.message = message;
        this.title = 'Σημείωση!'
        this.duration = 2000;

        return this.emit(message);
    }

    error(message){
        this.message = message;
        this.type = 'error';
        this.title = 'Προσοχή!'
        this.duration = 1000;

        return this.emit(message);
    }

    alwaysError(message){
        this.message = message;
        this.type = 'error';
        this.title = 'Προσοχή!'
        this.duration = -1;

        return this.emit(message);
    }

    emit(message){
        return {
            title : this.title,
            message,
            type : this.type,
            duration : this.duration,
            speed : this.speed
        }
    }
}

export default Alert