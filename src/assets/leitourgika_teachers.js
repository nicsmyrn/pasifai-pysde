// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import VTooltip from 'v-tooltip';

import Swalify from './Classes/Swalify';

import Modal from './components/Modal.vue';
import Alert from './../../../../../resources/assets/js/components/Alert.vue';
import DatePicker from 'vue2-datepicker';

window.Swalify = Swalify;

Vue.use(VTooltip);

new Vue({
    el : "#leitourgika",

    components : {
        Modal, Alert, DatePicker
    },

    data : {
        loading : false,
        teachers : [],
        schools : [],
        eidikotites : [],
        eidikotita_slug : null,
        school_id : null,
        teacher_types_that_is_in_school : [],
        teacher_types : [],

        without_topothetisi : false,
        pleonazoun : false,
        yperoria : false,

        allEidikotites : false,

        token : window.api_token,
        base_url : window.base_url,
        user_id : window.user_id
    },

    computed : {
        filteredTeachers(){
            if (this.school_id === null || this.school_id === "null"){
                if(this.pleonazoun && this.without_topothetisi)
                    return this.teachers.filter(teacher => (teacher.ypoxreotiko - teacher.sum > 0));

                else if(this.pleonazoun)
                   return this.teachers.filter(teacher => ((teacher.ypoxreotiko - teacher.sum) > 0) && (teacher.topothetisis.length || teacher.special_topothetisis.length));                     

                else if(this.without_topothetisi)
                    return this.teachers.filter(teacher => (!teacher.topothetisis.length && !teacher.special_topothetisis.length));                  

                else if(this.yperoria)
                    return this.teachers.filter(teacher => (teacher.ypoxreotiko - teacher.sum < 0));    

                return this.teachers;
            }

            return this.teachers.filter(teacher => teacher.school_id === this.school_id);
        }
    },

    mounted(){
        console.log('get teachers')
        this.fetchEidikotites();
    },

    methods : {
        getTeachersForAllEidikotites(){
            this.eidikotita_slug = null;
            if(!this.allEidikotites){
                this.school_id = null;
                this.teachers = [];
            }
            else{
                this.getTeachers();
            } 
        },

        fetchEidikotites(){
            this.loading = true;
            axios.get('/api/admin/leitourgika/fetchEidikotites?api_token='+this.token)
                .then(r => {
                    this.eidikotites = r.data['eidikotites'];
                    this.teacher_types_that_is_in_school = r.data['teacher_types_that_is_in_school'];
                    this.teacher_types = r.data['teacher_types'];
                    this.loading = false;
                })
                .catch(e => console.log(e.response))    
        },

        getTeachers(){
            this.loading = true;

            axios.post('/api/admin/leitourgika/fetchTeachersByEidikotita?api_token='+this.token, {
                eidikotita_slug : this.eidikotita_slug
            })
                .then(r => {
                    // console.log(r.data);
                    this.schools = r.data['schools'];
                    this.teachers = r.data['teachers'];
                    this.loading = false;
                })
                .catch(e => console.log(e.response))     
        },

    }
});