// require('./../../../../../resources/assets/js/bootstrap');
// 
let Vue = require('vue');

window.Event = new Vue();

import VTooltip from 'v-tooltip';

import Notifications from 'vue-notification';
import velocity from 'velocity-animate';

import Swalify from './Classes/Swalify';
import DatePickerCustomize from './Classes/DatePickerCustomize';

import Alert from './../../../../../resources/assets/js/components/Alert.vue';
import NewAlert from './../../../../../resources/assets/js/components/NewAlert.vue';

import DatePicker from 'vue2-datepicker';

import VueHtmlToPaper from 'vue-html-to-paper';

import countdown from './components/Tests/Countdown.vue';

import Hours from './Classes/Hours'

Vue.use(velocity, {
    name : 'foo'
});

Vue.use(Notifications);

const options = {
    name: '_blank',
    specs: [
      'fullscreen=yes',
      'titlebar=yes',
      'scrollbars=yes'
    ],
    styles: [
      'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
      'https://unpkg.com/kidlat-css/css/kidlat.css'
    ]
  }

Vue.use(VueHtmlToPaper, options);

window.Swalify = Swalify;

Vue.use(VTooltip);

new Vue({
    el : "#leitourgika_school",

    components : {
        Alert, DatePicker, countdown, NewAlert
    },

    data : {
        set_countdown_eidikotites : window.set_countdown_eidikotites,
        countdown_date_eidikotites : new Date(`${window.countdown_date_eidikotites}`),
        can_edit_hours_eidikotites : window.can_edit_hours_eidikotites,
        school_can_edit_hours_eidikotites : window.school_can_edit_hours_eidikotites,

        hideLoader : false,
    
        loading : true,

        token : window.api_token,
        base_url : window.base_url,

        eidikotites : [],
        other_eidikotites : [],

        datePickerClass : new DatePickerCustomize(),

        updated : false,

        new_forseen : null,

        new_eidikotita : null,

        has_unlocked : null,

        loading_button : false,

        school_type : null,

        school_name : null,

        last_update : null

    },

    mounted(){
        this.fetchEidikotites();
    },

    methods : {
        substr(string){
            if(string !== null && string !== ''){
                return `${string.substring(0,15)}...`;
            }
            return '';
        },

        printPage(){
            // this.$htmlToPaper('eidikotites_table'); //table_eidikotites
            var divToPrint=document.getElementById("table_eidikotites");
            var newWin= window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
        },

        addNewEidikotita(){
            axios.post('/api/school/leitourgika/attachNewEidikotita?api_token='+this.token,{
                new_eidikotita : this.new_eidikotita,
                new_forseen : this.new_forseen
            })
                .then(r => {
                    if(r.data === 'all_ok'){
                        this.eidikotites.push({
                            id : this.new_eidikotita.id,
                            eidikotita_slug : this.new_eidikotita.eidikotita_slug,
                            eidikotita_name : this.new_eidikotita.eidikotita_name,
                            pivot : {
                                available : 0,
                                difference : 0,
                                sch_difference : - this.new_forseen,
                                forseen : 0,
                                number_of_teachers : 0,
                                project_hours : 0,
                                locked : 0,
                                sch_locked : 0,
                                sch_description : null
                            }
                        });
            
                        this.eidikotites.sort(this.sortItems);
            
                        this.deleteSelectedNewEidikotita(this.new_eidikotita.id);

                        this.notifyMessage({
                            title: 'Συγχαρητήρια!',
                            body: `Η Ειδικότητα  ${ this.new_eidikotita.eidikotita_slug } καταχωρήστηκε στα κενά με επιτυχία...`,
                            type: 'success',
                            duration: 2000,
                            speed: 1000
                        })
                    }else{
                        this.notifyMessage({
                            title: 'Προσοχή!',
                            body: `Κάτι πήγε στραβά`,
                            type: 'error',
                            duration: 40000,
                            speed: 500
                        })
                    }
                    this.new_eidikotita = null;
                    this.new_forseen = null;
                })
                .catch(e => this.notifyMessage({
                    title: 'Σφάλμα 249!',
                    body: `Κάτι πήγε στραβά`,
                    type: 'error',
                    duration: 40000,
                    speed: 500
                }))  

            // this.isUpdated();
        },

        deleteSelectedNewEidikotita(id){
            let index = this.other_eidikotites.findIndex(v => v.id === id);
            this.deleteNode(this.other_eidikotites, index);
        },

        isUpdated(){
            this.updated = true;
        },

        deleteEidikotita(eidikotita, index){
            axios.post('/api/school/leitourgika/detachEidikotita?api_token='+this.token,{
                eidikotita_id : eidikotita.id
            })
                .then(r => {
                    if(r.data === 'all_ok'){
                        this.notifyMessage({
                            title: 'Σημείωση!',
                            body: `Η Ειδικότητα <strong>${ eidikotita.eidikotita_slug } ${ eidikotita.eidikotita_name }</strong> ΑΦΑΙΡΕΘΗΚΕ...`,
                            type: 'warning',
                            duration: 2000,
                            speed: 1000
                        })

                        this.deleteNode(this.eidikotites, index);
                        this.addToOtherEidikotites(eidikotita);
                        // this.isUpdated();
                    }else{
                        this.notifyMessage({
                            title: 'Προσοχή!',
                            body: `Κάτι πήγε στραβά`,
                            type: 'error',
                            duration: 40000,
                            speed: 500
                        })
                    }
                })
                .catch(e => this.notifyMessage({
                    title: 'Σφάλμα 459!',
                    body: `Κάτι πήγε στραβά`,
                    type: 'error',
                    duration: 40000,
                    speed: 500
                }))  

        },

        addToOtherEidikotites(eidikotita){
            this.other_eidikotites.push({
                eidikotita_name : eidikotita.eidikotita_name,
                eidikotita_slug : eidikotita.eidikotita_slug,
                id              : eidikotita.id
            });
            this.other_eidikotites.sort(this.sortItems);
        },

        deleteNode(obj, index){
            this.$delete(obj, index);
        },

        fetchEidikotites(){
            axios.get('/api/school/leitourgika/getEidikotites?api_token='+this.token)
                .then(r => {                    
                    this.eidikotites = r.data['leitourgika_kena'];
                    this.other_eidikotites = r.data['other_eidikotites'];
                    this.school_type = r.data['school_type'];
                    this.school_name = r.data['school_name'];
                    this.last_update = r.data['last_update'];
                    this.loading = false;
                    this.has_unlocked = Number.isInteger(r.data['has_unlocked']);
                })
                .catch(e => console.log(e.response))  
        },

        updateDifferenceHours(eidikotita){
            eidikotita.pivot.forseen = this.checkIfIsPositive(eidikotita.pivot.forseen);
            eidikotita.pivot.project_hours = this.checkIfIsPositive(eidikotita.pivot.project_hours);
            this.isUpdated();
        },

        schoolUpdateDifferenceHours(eidikotita){
            let cell = new Hours(eidikotita.pivot.sch_difference)

            eidikotita.pivot.sch_difference = cell.convertToInt()

            if(eidikotita.pivot.sch_difference === eidikotita.pivot.difference){
                eidikotita.pivot.sch_description = ''
            }


            axios.post('/api/school/leitourgika/saveEidikotita?api_token='+this.token,{
                eidikotita : eidikotita
            })
                .then(r => {
                    if(r.data['message'] === 'all_ok'){
                        eidikotita.pivot.sch_difference = r.data['difference']

                        this.notifyMessage({
                            title: 'Συγχαρητήρια!',
                            body: `Η καταχώρηση "<strong>${eidikotita.pivot.sch_difference}</strong> για την ειδικότητα ${ eidikotita.eidikotita_slug } ${ eidikotita.eidikotita_name } έγινε με επιτυχία...`,
                            type: 'success',
                            duration: 2000,
                            speed: 1000
                        })
                    }else{
                        this.notifyMessage({
                            title: 'Προσοχή!',
                            body: `Η καταχώρηση "<strong>${eidikotita.pivot.sch_difference}</strong> για την ειδικότητα ${ eidikotita.eidikotita_slug } ${ eidikotita.eidikotita_name } <strong>ΔΕΝ ΕΓΙΝΕ</strong>`,
                            type: 'error',
                            duration: 40000,
                            speed: 1000
                        })
                    }
                })
                .catch(e => this.notifyMessage({
                    title: 'ΣΦΑΛΜΑ 345!',
                    body: `Η καταχώρηση "<strong>${eidikotita.pivot.sch_difference}</strong> για την ειδικότητα ${ eidikotita.eidikotita_slug } ${ eidikotita.eidikotita_name } <strong>ΔΕΝ ΕΓΙΝΕ</strong>. Ξαναπροσπαθήστε`,
                    type: 'error',
                    duration: 10000,
                    speed: 1000
                }))  
        },

        checkNewAvailable(){
            this.new_forseen = this.checkIfIsPositive(this.new_forseen);
        },

        calculateDifference(eidikotita){
            return (eidikotita.pivot.available - eidikotita.pivot.project_hours  - eidikotita.pivot.forseen );
        },

        saveEidikotites(){
            this.loading_button = true;
            axios.post('/api/school/leitourgika/saveEidikotites?api_token='+this.token,{
                eidikotites : this.eidikotites
            })
                .then(r => {
                    this.updated = false;
                    this.loading_button = false;

                    this.displayAlert(r.data['header'], r.data['message'], r.data['type']);
                })
                .catch(e => console.log(e.response))  

        },

        checkIfIsPositive(number){
            if(this.checkIfIsInteger(number)){
                if(number > 0){
                    return parseInt(number);
                }else if(number < 0){
                    this.notifyMessage({
                        title: 'Προσοχή!',
                        body: `ΔΕΝ έχετε εισάγει θετικό αριθμό`,
                        type: 'error',
                        duration: 1000,
                        speed: 1000
                    })
                    // this.displayAlert('Προσοχή!', 'ΔΕΝ έχετε εισάγει θετικό αριθμό', 'danger');
                }else{
                    return null;
                }
            }
            this.notifyMessage({
                title: 'Προσοχή!',
                body: `ο αριθμός ΔΕΝ είναι ακέραιος`,
                type: 'error',
                duration: 1000,
                speed: 1000
            })
            // this.displayAlert('Προσοχή!', 'ο αριθμός ΔΕΝ είναι ακέραιος', 'danger');
            return null;
        },

        checkIfIsInteger(number){
            return ! isNaN(number);
        },
 
        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },

        sortItems(a,b){
            if (a.id < b.id){
                return -1;
            }
            if (a.id > b.id){
                return 1;
            }
        },

        countdown_finished_eidikotites(){
            this.loading = true;

            setTimeout(() => {
                this.fetchEidikotites()
                this.set_countdown_eidikotites = false;
            }, 7000);
        },

        notifyMessage(data){
            this.$refs.alert.show('custom-template', data.title, data.body, data.type, 1000, 1000);
        },
    }

});