window.axios = require('axios');

let Vue = require('vue');

window.Event = new Vue();

import VTooltip from 'v-tooltip';

import Swalify from './Classes/Swalify';

window.Swalify = Swalify;

Vue.use(VTooltip);

new Vue({
    el: '#app',

    data : {
        token : window.api_token,
        base_url : window.base_url,
        requests : [],
        loading : true,
        dataCancelRecall : new Swalify({
            title : 'Προσοχή!!',
            type : 'warning',
            text : 'Η Αίτηση θα παραμείνει ως έχει...',
            buttonText : 'Ναι, να μην ανακληθεί',
            success_title : '',
            success_text  : 'η αίτηση παραμένει ως έχει'
        }),
        dataApproveRecall : new Swalify({
            title : 'Προσοχή!!',
            type : 'info',
            text : 'Η ανάκληση της αίτησης είναι οριστική',
            buttonText : 'Ναι, ανακλήστε την',
            success_title : 'Συγχαρητήρια!',
            success_text  : 'η αίτηση ανακλήθη'
        })
    },


    mounted ()  {
        this.fetchRequests();
    },

    methods : {
        fetchRequests (){
            axios.get('/api/admin/requests/allRequests?api_token='+this.token)
                .then(r => {
                    this.requests = r.data;
                    this.loading = false;
                })
                .catch(e => console.log(e));
        },

        cancelRecall(unique_id){
            let action = 'cancelRecall';
            this.dataCancelRecall.submit(`${this.base_url}/api/admin/requests/recall/${unique_id}/${action}?api_token=${this.token}`);
        },

        approvedRecall(unique_id){
            let action = 'approvedRecall';
            this.dataApproveRecall.submit(`${this.base_url}/api/admin/requests/recall/${unique_id}/${action}?api_token=${this.token}`);
        }
    }
});