// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import VTooltip from 'v-tooltip';


import Notifications from 'vue-notification';
import velocity from 'velocity-animate';

import Swalify from './Classes/Swalify';
import DatePickerCustomize from './Classes/DatePickerCustomize';

import Modal from './components/Modal.vue';
import NewAlert from './../../../../../resources/assets/js/components/NewAlert.vue';

import DatePicker from 'vue2-datepicker';

window.Swalify = Swalify;

Vue.use(velocity, {
    name : 'foo'
});

Vue.use(Notifications);

Vue.use(VTooltip);

new Vue({
    el : "#app",

    components : {
        Modal, NewAlert, DatePicker
    },

    data : {
        acheck : true,
        bcheck : true,
        ccheck : true,
        dcheck : true,

        school_id : null,

        new_eidikotita : null,
        other_eidikotites : [],
        new_forseen : null,

        hideLoader : true,
        eidikotitaName : null,
    
        flag : true,
        schools : null,
        kena : [],
        loading : false,

        school_type : null,

        school_selected : window.school_name,
        user_id : window.user_id,
        token : window.api_token,
        base_url : window.base_url,
        teacher_types : [],

        loadOrganikaAlert : new Swalify({
            title : 'Προσοχή!!',
            type : 'warning',
            text : "Θα αντιγραφούν όλα τα οργανικά κενά στα Λειτουργικά. Όλα τα προηγούμενα δεδομένα θα χαθούν.",
            buttonText : 'Εντάξει',
            success_title : 'Συγχαρητήρια!',
            success_text  : 'τα κενά μεταφέρθηκαν με επιτυχία'
        }),
        
        loadTeachersAlert : new Swalify({
            title : 'Προσοχή!!',
            type : 'warning',
            text : "Θα αντιγραφούν όλοι οι οργανικά ανήκοντες. Όλα τα προηγούμενα δεδομένα θα χαθούν.",
            buttonText : 'Εντάξει',
            success_title : 'Συγχαρητήρια!',
            success_text  : 'οι εκπαιδευτικοί αντιγράφηκαν με επιτυχία'
        }),

        deleteEidikotita : new Swalify(),
        transfer : new Swalify(),

        openTeachersModal : false,
        openDescriptionModal : false,

        tempDescription : '',
        tempDescriptionName : '',

        teachers : [],

        datePickerClass : new DatePickerCustomize(),

        updated : false,
        teachersUpdated : false,

        teacher_types_that_is_in_school : [],

        counter_locked : 0,
        counter_unlocked : 0,

        lockAllKena : new Swalify({
            title : 'Προσοχή!!',
            type : 'warning',
            text : "ΟΛΑ τα κενά θα κλειδωθούν...",
            buttonText : 'Εντάξει',
            success_title : 'Συγχαρητήρια!',
            success_text  : 'τα ΚΕΝΑ κλειδώθηκαν με επιτυχία'
        }),

        unlockAllKena : new Swalify({
            title : 'Προσοχή!!',
            type : 'error',
            text : "ΟΛΑ τα κενά θα ΞΕΚΛΕΙΔΩΘΟΥΝ...",
            buttonText : 'Εντάξει',
            success_title : 'Συγχαρητήρια!',
            success_text  : 'τα ΚΕΝΑ ΞΕΚΛΕΙΔΩΘΗΚΑΝ με επιτυχία'  
        })
},
    computed :{

        allChecked(){
            let oneNotChecked = false

            this.kena.forEach(eidikotita => {
                if(eidikotita.sch_difference !== eidikotita.difference){
                    oneNotChecked = true
                }
            })

            return !oneNotChecked
        },

        notSynchronized (){
            let temp = this.kena.filter(keno => {
                return keno.difference !== keno.sch_difference
            })

            return temp.length
        },

        colspan (){
            let a = this.acheck ? 1 : 0;
            let b = this.bcheck ? 1 : 0;
            let c = this.ccheck ? 1 : 0;
            let d = this.dcheck ? 1 : 0;
            
            return 4 + a + b + c + d;
        },

        locked_diff (){
            return (this.counter_locked - this.counter_unlocked) > 0 ? 'more_locked' : 'more_unlocked';   
        },

        kladosPe04 (){
            return this.kena.filter(e => e['klados_slug'] === 'ΠΕ04');
        },

        sumPe04Forseen (){
            var total = 0;
            for(var i = 0, _len = this.kladosPe04.length; i < _len; i++){
                total += this.convertToInt(this.kladosPe04[i]['forseen']);
            }
            return total;
        },
        sumPe04Available(){
            var total = 0;
            for(var i = 0, _len = this.kladosPe04.length; i < _len; i++){
                total += this.convertToInt(this.kladosPe04[i]['available']);
            }
            return total;   
        },

        diffPe04sch(){
            var total = 0;
            for(var i = 0, _len = this.kladosPe04.length; i < _len; i++){
                total += this.convertToInt(this.kladosPe04[i]['sch_difference']);
            }
            return total;   
        },

        diffPe04(){
            return this.sumPe04Available - this.sumPe04Forseen;
        },

        kladosPe81 (){
            return this.kena.filter(e => e['klados_slug'] === 'ΠΕ81tech');
        },

        sumPe81Forseen (){
            var total = 0;
            for(var i = 0, _len = this.kladosPe81.length; i < _len; i++){
                total += this.convertToInt(this.kladosPe81[i]['forseen']);
            }
            return total;
        },
        sumPe81Available(){
            var total = 0;
            for(var i = 0, _len = this.kladosPe81.length; i < _len; i++){
                total += this.convertToInt(this.kladosPe81[i]['available']);
            }
            return total;   
        },

        diffPe81sch(){
            var total = 0;
            for(var i = 0, _len = this.kladosPe81.length; i < _len; i++){
                total += this.convertToInt(this.kladosPe81[i]['sch_difference']);
            }
            return total;   
        },

        diffPe81(){
            return this.sumPe81Available - this.sumPe81Forseen;
        }
    },

    mounted(){
        this.fetchSchools();
        if(this.school_selected !== 'null'){
            this.getSchoolTeachers(this.school_selected);
        }
    },

    methods : {
        createExcelforCouncil(){
            this.hideLoader = false;

            axios.get('/api/admin/leitourgika/getExcelForCouncil/?api_token='+this.token, {
                responseType: 'blob', // important
            })
            .then(response => {
                this.hideLoader = true

                this.notifyMessage({
                    title: 'Σημείωση!',
                    body: `All ok Nick...`,
                    type: 'success',
                    duration: 3000,
                    speed: 1000
                })
                let file_name = `ΛΕΙΤΟΥΡΓΙΚΑ_ΚΕΝΑ_final_${this.getCurrentDate()}.xlsx`;

                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', file_name); //or any other extension
                document.body.appendChild(link);
                link.click();
                this.hideLoader = true;
            })
            .catch(e => {
                this.hideLoader = true;
                this.notifyMessage({
                    title: 'Σφάλμα!',
                    body: `Κάτι πήγε Λάθος...`,
                    type: 'error',
                    duration: 4000,
                    speed: 1000
                })
            }) 
        },

        createExcelforSchool(){
            this.hideLoader = false;

            axios.get('/api/admin/leitourgika/getExcelForSchools/?api_token='+this.token, {
                responseType: 'blob', // important
            })
            .then(response => {
                this.hideLoader = true

                this.notifyMessage({
                    title: 'Σημείωση!',
                    body: `All ok Nick...666 NOT READY YET`,
                    type: 'success',
                    duration: 3000,
                    speed: 1000
                })
                let file_name = `ΛΕΙΤΟΥΡΓΙΚΑ_ΚΕΝΑ_ΣΧΟΛΙΚΩΝ_ΜΟΝΑΔΩΝ_${this.getCurrentDate()}.xlsx`;

                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', file_name); //or any other extension
                document.body.appendChild(link);
                link.click();
                this.hideLoader = true;
            })
            .catch(e => {
                this.hideLoader = true;
                this.notifyMessage({
                    title: 'Σφάλμα!',
                    body: `Κάτι πήγε Λάθος...`,
                    type: 'error',
                    duration: 4000,
                    speed: 1000
                })
            }) 
        },

        getCurrentDate(){
            var today = new Date();
            var dd = today.getDate();

            var mm = today.getMonth()+1; 
            var yyyy = today.getFullYear();

            if(dd<10) 
            {
                dd='0'+dd;
            } 

            if(mm<10) 
            {
                mm='0'+mm;
            } 

            return  dd+'_'+mm+'_'+yyyy;
        },

        transferAll(){
            this.transfer.confirm({
                'title'             : 'Προσοχή!',
                'text'              : 'Όλα τα κενά που δηλώνουν τα Σχολεία θα μεταφερθούν στα κενά του Συμβουλίου. Η Ενέργεια αυτή ΔΕΝ θα αποθηκευτεί μέχρι να πατηθεί η Αποθήκευση',
                'confirmText'       : 'Επιβεβαίωση',
                'denyText'          : 'Ακύρωση',
                'type'              : 'warning'
            })
            .then(r => {
                this.kena.forEach(eidikotita => {
                    if(eidikotita.difference !== eidikotita.sch_difference){
                        let new_forseen = - (eidikotita.sch_difference - eidikotita.available)
                        if(new_forseen >= 0){
                            eidikotita.old_difference = eidikotita.forseen
                            eidikotita.forseen = new_forseen
                            this.updateDifference(eidikotita)
                        }
                    }
                })

                this.notifyMessage({
                    title: 'Σημείωση!',
                    body: `<strong>ΟΛΑ μεταφέρθηκαν</strong> με επιτυχία...`,
                    type: 'success',
                    duration: 3000,
                    speed: 1000
                })
            })
            .catch(e => {
                this.notifyMessage({
                    title: 'Σφάλμα!',
                    body: `Κάτι πήγε Λάθος...`,
                    type: 'error',
                    duration: 4000,
                    speed: 1000
                })
            })
        },

        transferSchoolValue(eidikotita){
            let new_forseen = - (eidikotita.sch_difference - eidikotita.available)
            if(new_forseen < 0){
                this.notifyMessage({
                    title: 'Σφάλμα!',
                    body: `<strong>ΔΕ γίνεται</strong> αυτή  η ενέργεια. Έχει γίνει λάθος από το Σχολείο`,
                    type: 'error',
                    duration: 2000,
                    speed: 1000
                })
                eidikotita.alter_school = true
            }else{
                eidikotita.old_difference = eidikotita.forseen
                eidikotita.forseen = new_forseen
                this.updateDifference(eidikotita)
                this.notifyMessage({
                    title: 'Σημείωση!',
                    body: `<strong>Έγινε η μεταφορά...</strong>`,
                    type: 'success',
                    duration: 2000,
                    speed: 1000
                })
            }
        },

        globalTransfer(){
            this.kena.forEach(eidikotita => {
                let new_forseen = - (eidikotita.sch_difference - eidikotita.available)
                if(new_forseen < 0){
                    eidikotita.alter_school = true
                }else{
                    eidikotita.old_difference = eidikotita.forseen
                    eidikotita.forseen = new_forseen
                    this.updateDifference(eidikotita)
                }
            })
            this.notifyMessage({
                title: 'Σημείωση!',
                body: `<strong>Έγινε η μεταφορά από ΟΛΕΣ τις ειδικότητες</strong>`,
                type: 'success',
                duration: 2000,
                speed: 1000
            })
        },

        globalUndo(){
            this.kena.forEach(eidikotita => {
                eidikotita.forseen = eidikotita.old_difference
                eidikotita.old_difference = null
                this.updateDifference(eidikotita)
            })

            this.notifyMessage({
                title: 'Σημείωση!',
                body: `<strong>Έγινε η αναίρεση για ΟΛΕΣ τις ειδικότητες</strong>`,
                type: 'warning',
                duration: 2000,
                speed: 1000
            })
        },

        transferCouncil(eidikotita){
            eidikotita.sch_difference = eidikotita.difference
            eidikotita.sch_description = null
            eidikotita.alter_school = false

            eidikotita.locked = true;
            eidikotita.sch_locked = true
            eidikotita.user_id = user_id;
            eidikotita.updated = true;
            this.updated = true;

            this.notifyMessage({
                title: 'Σημείωση!',
                body: `<strong>Το κενό της Σχολικής Μονάδας ενημερώθηκε από το συμβούλιο</strong>`,
                type: 'warning',
                duration: 3000,
                speed: 1000
            })
        },

        undoTransfer(eidikotita){
            eidikotita.forseen = eidikotita.old_difference
            eidikotita.old_difference = null
            this.updateDifference(eidikotita)
            this.notifyMessage({
                title: 'Σημείωση!',
                body: `<strong>Έγινε η αναίρεση</strong>`,
                type: 'warning',
                duration: 2000,
                speed: 1000
            })
        },

        unLockAll(){
            this.unlockAllKena.submit(`${this.base_url}/api/admin/leitourgika/lockUnlockAllKena?api_token=${this.token}&locked=0`);
        },

        lockAll(){
            this.lockAllKena.submit(`${this.base_url}/api/admin/leitourgika/lockUnlockAllKena?api_token=${this.token}&locked=1`);
        },

        updateTeacherType(teacher){
            let type = this.teacher_types_that_is_in_school.find(e => e === teacher.teacher_type);

            if (type === undefined){
                teacher.hours = null;
                teacher.project_hours = null;
                teacher.difference = null;
                teacher.locked = true;
                teacher.belongsToSchool = false;
            }else{
                teacher.hours = teacher.ypoxreotiko - teacher.sum_other_topothetisis;
                teacher.project_hours = null;
                teacher.difference = 0;
                teacher.locked = true;
                teacher.belongsToSchool = true;
                teacher.end_date = null
            }
            teacher.updated = true;
            this.teachersUpdated = true;
        },
        saveChangesForEidikotites(){
            let school_name = this.school_selected.replace(/ /g, '-');

            axios.post('/api/admin/leitourgika/saveEidikotita/'+school_name+'?api_token='+this.token, {
                eidikotites : this.kena
            })
                .then(r => {
                    this.notifyMessage({
                        title: r.data['header'],
                        body: r.data['message'],
                        type: r.data['type'],
                        duration: 3000,
                        speed: 1000
                    })
                    this.updated = false;
                    this.teachersUpdated = false;
                    this.counter_unlocked = r.data['counter_unlocked'];
                    this.counter_locked = r.data['counter_locked'];
                })
                .catch(r => console.log('Error242456667'));
        },  

        lockEidikotita(eidikotita){
            this.updated = true;
            this.lock(eidikotita);

            this.notifyMessage({
                title: 'Σημείωση!',
                body: `Η Ειδικότητα <strong>${eidikotita.eidikotita_slug} ${eidikotita.eidikotita_name}</strong> κλειδώθηκε...`,
                type: 'warning',
                duration: 2000,
                speed: 1000
            })
        },

        unlockEidikotita(eidikotita){
            this.updated = true;
            this.lock(eidikotita);

            this.notifyMessage({
                title: 'Σημείωση!',
                body: `Η Ειδικότητα <strong>${eidikotita.eidikotita_slug} ${eidikotita.eidikotita_name}</strong> Ξεκλειδώθηκε...`,
                type: 'success',
                duration: 2000,
                speed: 1000
            })
        },

        deleleEidikotita(eidikotita){

            this.saveChangesForEidikotites(); // Test this...
            
            let school_name = this.school_selected.replace(/ /g, '-');

            var url = '/api/admin/leitourgika/deleteEidikotita/'+school_name+'?api_token='+this.token

            this.deleteEidikotita.deletePlacement(url, {
                eidikotita_id : eidikotita.id, 
                title       : 'Προσοχή!',
                message     : `Θα γίνει ΔΙΑΓΡΑΦΗ της Ειδικότητας ${eidikotita.eidikotita_slug} ${eidikotita.eidikotita_name}. Είστε σίγουροι;`,
                buttonText  : 'Ναι, διαγράψτε την',
                successMsg  : 'Έγινε ΔΙΑΓΡΑΦΗ της ειδικότητας',
                successType : 'success',
                errorMsg    : 'καμία αλλαγή...',
                errorType   : 'info',
                color       : '#DD6B55'
            })
                .then(data => {
                    this.updated = false;
                    this.teachersUpdated = false;
                    this.kena = data;
                    this.other_eidikotites.push(eidikotita)
                    this.other_eidikotites.sort(this.sortItems)
                })
                .catch(e => console.log('error 235235'))

            // this.deleteEidikotita.

            // axios.post('/api/admin/leitourgika/deleteEidikotita/'+school_name+'?api_token='+this.token, {
            //     eidikotita_id : eidikotita.id
            // })
            //     .then(r => {
            //         this.notifyMessage({
            //             title: 'Προσοχή!',
            //             body: `Η Ειδικότητα <strong>Διεγράφη</strong>`,
            //             type: 'error',
            //             duration: 4000,
            //             speed: 1000
            //         })
            //         this.updated = false;
            //         this.teachersUpdated = false;
            //         this.kena = r.data;
            //         this.other_eidikotites.push(eidikotita)
            //         this.other_eidikotites.sort(this.sortItems)
            //         // this.counter_unlocked = r.data['counter_unlocked'];
            //         // this.counter_locked = r.data['counter_locked'];
            //     })
            //     .catch(r => console.log('Erroradg4432'));
        },

        lockTeacher(teacher){
            this.teachersUpdated = true;
            teacher.locked = true;
            this.lock(teacher);
        },

        unlockTeacher(teacher){
            console.log('un lock teacher');

            this.teachersUpdated = true;
            teacher.locked = false;
            this.unlock(teacher);
        },
        
        lock(object){
            this.toggleLock(object);
        },

        unlock(object){
            this.toggleLock(object);
        },

        toggleLock(object){
            object.sch_locked = !object.sch_locked;
            this.setUpdated(object);
        },

        changeDate(teacher){
            teacher.locked = true;
            this.teachersUpdated = true;
            this.setUpdated(teacher);
        },

        setUpdated(object){
            object.updated = true;
        },

        updateDifferenceHours(teacher){
            teacher.ypoxreotiko = this.checkIfIsPositive(teacher.ypoxreotiko);
            teacher.meiwsi = this.checkIfIsPositive(teacher.meiwsi);
            teacher.hours = this.checkIfIsPositive(teacher.hours);
            teacher.project_hours = this.checkIfIsPositive(teacher.project_hours);

            teacher.difference =  (teacher.ypoxreotiko - teacher.meiwsi) - (teacher.hours + teacher.project_hours + teacher.sum_other_topothetisis);
            teacher.locked = true;
            this.teachersUpdated = true;
            this.setUpdated(teacher);
        },

        modalTeachersByEidikotita(eidikotita){
            let school_name = this.school_selected.replace(/ /g, '-');
            
            axios.post('/api/admin/leitourgika/getTeachersOfSchool/'+school_name+'?api_token='+this.token, {
                eidikotita : eidikotita
            })
                .then(r => {
                    this.teachers = r.data;
                    this.eidikotitaName = eidikotita;
                    this.openTeachersModal = true;
                })
                .catch(r => console.log('Error242456667'));
        },

        confirmLoadingOrganika(){
            this.loadOrganikaAlert.submit(`${this.base_url}/api/admin/leitourgika/loadOrganika?api_token=${this.token}`);
        },

        confirmLoadingTeachers(){
            this.loadTeachersAlert.submit(`${this.base_url}/api/admin/leitourgika/loadTeachers?api_token=${this.token}`);
        },

        updateDifference(eidikotita){
            eidikotita.forseen = this.checkIfIsPositive(eidikotita.forseen);
            eidikotita.project_hours = this.checkIfIsPositive(eidikotita.project_hours);
            eidikotita.difference = eidikotita.available - (eidikotita.forseen + eidikotita.project_hours);
            eidikotita.locked = true;
            eidikotita.user_id = user_id;
            eidikotita.updated = true;
            this.updated = true;
        },

        cancelOperation (){
            this.openTeachersModal = false;
            this.teachersUpdated = false;
        },

        showDescriptionModal(description, name){
            this.openDescriptionModal = true;
            this.tempDescription = description;
            this.tempDescriptionName = name;
        },

        close(){
            this.openDescriptionModal = false;
            this.tempDescription = '';
            this.tempDescriptionName = '';
        },

        checkDate(){
            console.log('check end date');
        },

        saveTeachers(){
            let school_name = this.school_selected.replace(/ /g, '-');
            
            axios.post('/api/admin/leitourgika/saveTeachersOfEidikotita/'+school_name+'?api_token='+this.token, {
                teachers : this.teachers,
                eidikotitaName : this.eidikotitaName,
                teacher_types_that_is_in_school : this.teacher_types_that_is_in_school
            })
                .then(r => {
                    Vue.set(this.kena, this.kena.findIndex(k => k.id === r.data['eidikotita']['id']), r.data['eidikotita']);                   

                    this.notifyMessage({
                        title: r.data['header'],
                        body: r.data['message'],
                        type: r.data['type'],
                        duration: 2000,
                        speed: 1000
                    })

                    this.openTeachersModal = false;
                    this.teachersUpdated = false;
                })
                .catch(r => 
                    this.notifyMessage({
                        title: 'Σφάλμα!',
                        body: `Κάτι πήγε Λάθος...`,
                        type: 'error',
                        duration: 4000,
                        speed: 1000
                    }));

        },

        getSchoolTeachers(name){
            this.loading = true;
            let school_name = name.replace(/ /g, '-');

            axios.get('/api/admin/leitourgika/getKenaOfSchool/'+school_name+'?api_token='+this.token)
                .then(r => {
                    this.kena = r.data['kena'];
                    this.school_type = r.data['school_type'];
                    this.teacher_types = r.data['teacher_types'];
                    this.school_id = r.data['school_id']

                    this.other_eidikotites = r.data['other_eidikotites'];

                    this.loading = false;
                })
                .catch(e => console.log(e.response))
        },
        fetchSchools(){
            axios.get('/api/admin/leitourgika/fetchSchools?api_token='+this.token)
                .then(r => {
                    this.schools = r.data['schools'];
                    this.teacher_types_that_is_in_school = r.data['teacher_types_that_is_in_school'];
                    this.counter_locked = r.data['counter_locked'];
                    this.counter_unlocked = r.data['counter_unlocked'];
                })
                .catch(e => console.log(e.response))     
        },

        checkIfIsPositive(number){
            if(this.checkIfIsInteger(number)){
                if(number > 0){
                    return parseInt(number);
                }else if(number < 0){
                    this.notifyMessage({
                        title: 'Προσοχή!',
                        body: `ΔΕΝ έχετε εισάγει θετικό αριθμό`,
                        type: 'error',
                        duration: 1000,
                        speed: 1000
                    })
                }else{
                    return null;
                }
            }
            this.notifyMessage({
                title: 'Προσοχή!',
                body: `ο αριθμός ΔΕΝ είναι ακέραιος`,
                type: 'error',
                duration: 1000,
                speed: 1000
            })
            return null;
        },

        checkIfIsInteger(number){
            return ! isNaN(number);
        },

        convertToInt (x){
            var parsed = parseInt(x);
            if(isNaN(parsed)){
                return 0;
            }
            return parsed;
        },
 
        notifyMessage(data){
            this.$refs.alert.show('custom-template', data.title, data.body, data.type, 1000, 1000);
        },

        checkNewAvailable(){
            this.new_forseen = this.checkIfIsPositive(this.new_forseen);
        },

        addNewEidikotita(){
            if(this.new_eidikotita === null || this.new_eidikotita === undefined){
                this.notifyMessage({
                    title: 'Σφάλμα!',
                    body: `Κάποιο λάθος έγινε στην εισαγωγή της ειδικότητα, ξαναπροσπαθήστε`,
                    type: 'error',
                    duration: 2000,
                    speed: 1000
                })
            }else{
                this.kena.push({
                    eidikotita_name : this.new_eidikotita.eidikotita_name,
                    eidikotita_slug : this.new_eidikotita.eidikotita_slug,
                    available : 0,
                    difference : -(this.new_forseen),
                    forseen: this.new_forseen,
                    id : this.new_eidikotita.id,
                    klados_name : this.new_eidikotita.klados_name,
                    klados_slug : this.new_eidikotita.klados_slug,
                    locked : 1,
                    number_of_teachers : 0,
                    project_hours: 0,
                    school_id : this.school_id,
                    updated : true,
                    user_id : this.user_id,
                    sch_difference : 0,
                    sch_description : null,
                    sch_locked : 0
                });
    
                this.kena.sort(this.sortItems);
    
                this.notifyMessage({
                    title: 'Σημείωση!',
                    body: `Η Ειδικότητα <strong>${ this.new_eidikotita.eidikotita_slug } ${ this.new_eidikotita.eidikotita_name }</strong> προστέθηκε με ωράριο: <strong>${this.new_forseen}h</strong>`,
                    type: 'success',
                    duration: 3000,
                    speed: 1000
                })

                let id = this.new_eidikotita.id

                this.new_eidikotita = null;
    
                this.deleteSelectedNewEidikotita(id);
    
                this.new_forseen = null;
    
                this.isUpdated();
            }
        },

        focusHours(){
            this.$refs.neweidikotita.focus()
        },

        sortItems(a,b){
            if (a.id < b.id){
                return -1;
            }
            if (a.id > b.id){
                return 1;
            }
        },

        deleteSelectedNewEidikotita(id){
            let index = this.other_eidikotites.findIndex(v => v.id === id);
            this.deleteNode(this.other_eidikotites, index);
        },

        deleteNode(obj, index){
            this.$delete(obj, index);
        },

        isUpdated(){
            this.updated = true;
        },
    }

});