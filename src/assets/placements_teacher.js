window.axios = require('axios');

let Vue = require('vue');

window.Event = new Vue();

import Swalify from './Classes/Swalify';

window.Swalify = Swalify;


new Vue({
    el: '#app',

    data : {
        token : window.api_token,
        base_url : window.base_url,
        dataRecall : new Swalify({
            title : 'Προσοχή!!',
            type : 'warning',
            text : 'Η τοποθέτηση θα διαγραφεί ΟΡΙΣΤΙΚΑ.',
            buttonText : 'Ναι, να διαγραφεί',
            success_title : 'Σημείωση!',
            success_text  : 'Η τοποθέτηση διεγράφη'
        })
    },

    methods : {
        permanentDelete(placement_id){
            this.dataRecall.submit(`${this.base_url}/api/placements/permanentDelete/${placement_id}?api_token=${this.token}`);
        }
    }
});