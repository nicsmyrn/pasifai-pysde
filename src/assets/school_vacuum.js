// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import Alert from './../../../../../resources/assets/js/components/Alert.vue';


var vacuum = new Vue({
    el : "#vacuum",

    data : {
        loading : false,
        eidikotites : [],
        school_type : window.school_type
    },

    computed : {
        kladosPe04 (){
            return this.eidikotites.filter(e => e['klados_slug'] === 'ΠΕ04');
        },
        sumPe04Forseen (){
            var total = 0;
            for(var i = 0, _len = this.kladosPe04.length; i < _len; i++){
                total += this.convertToInt(this.kladosPe04[i]['pivot']['forseen']);
            }
            return total;
        },
        sumPe04Available(){
            var total = 0;
            for(var i = 0, _len = this.kladosPe04.length; i < _len; i++){
                total += this.convertToInt(this.kladosPe04[i]['pivot']['available']);
            }
            return total;   
        },
        sumPe04Projects(){
            var total = 0;
            for(var i = 0, _len = this.kladosPe04.length; i < _len; i++){
                total += this.convertToInt(this.kladosPe04[i]['pivot']['project_hours']);
            }
            return total;   
        },

        diffPe04(){
            return (this.sumPe04Available + this.sumPe04Projects) - this.sumPe04Forseen;
        },

        sumPe04Diff(){
            var diff = this.diffPe04;

            if(diff >= 12){
                return diff;
            }
            else if(diff <= -12){
                return diff;
            }else return '';
        }
    },

    mounted(){
        this.fetchDivisions();
    },

    methods : {

        fetchDivisions(){
            this.loading = true;

            let ajax_link = '/aj/divisions/fetchVacuums';

            axios.get(ajax_link)
                .then(r => {
                    this.eidikotites = r.data['kena'];
                    console.log(r.data['name']);
                    this.loading = false;
                })
                .catch(e => this.displayAlert('Σφάλμα 503!!!', 'παρακαλώ επικοινωνήστε με τον κύριο Σμυρναίο στην Γραμματεία του ΠΥΣΔΕ','danger', true))
        },
        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },

        convertToInt (x){
            var parsed = parseInt(x);
            if(isNaN(parsed)){
                return 0;
            }
            return parsed;
        }
    },

    components : {
        Alert
    }

});