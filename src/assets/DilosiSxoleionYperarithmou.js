// require('./../../../../../resources/assets/js/bootstrap');

let Vue = require('vue');

window.Event = new Vue();

import Modal from './components/Modal.vue';
import Alert from './../../../../../resources/assets/js/components/Alert.vue';

var dilosi = new Vue ({
    el : '#app',

    data : {
        isMadeValueApospasi : false,
        idia_omada_checked : false,
        omori_omada_checked : false,
        taktikes_apospaseis_checked : false,

        stay_to_organiki : 0,
        return_to_organiki : 0,

        idiaomada : {
            name : '',
            description : ''
        },

        omoresArray: {
            name : '',
            description : ''
        },


        type_of_aitisi : '',
        moriaDieuthidi : 0,
        moriaApospasis : 0,
        maxSelections : 100,
        schoolType : 'lyk',
        requestInDatabase : false,
        loader : false,

        allSchools : null,
        allSchoolsIdiasOmadas : null,
        allSchoolsOmoron : null,

        schoolsChoices : null,

        selectedSchoolsIdiasOmadas : [],
        selectedSchoolsOmoron : [],
        selectedSchools : [],

        hideLoader : true,
        showModal : false,
        showDeleteModal : false,
        hasMadeRequest : false,
        alert : {
            message : [''],
            type : 'info',
            important : false
        }
    },

    mounted(){
        this.fetchSchools();
    },

    methods : {
        permanentlyDelete(typeAitisi){
            console.log(typeAitisi);
            axios.post('/aj/organikes/permanentlyDeleteYperarithmos',{
                type : typeAitisi
            })
                .then(r => location.href = r.data)
                //.then(r => console.log(r.data))
                .catch(r => console.log('Error'));
        },

        openDeleteModal (){
            this.showDeleteModal = true;
        },
        saveRequest (type){
            console.log('Stay: ' + this.stay_to_organiki + ' Return: ' + this.return_to_organiki);

            axios.post('/aj/organikes/saveRequestYperarithmos',{
                selectedSchools : this.selectedSchools,
                selectedSchoolsIdiaOmada : this.selectedSchoolsIdiasOmadas,
                selectedSchoolsOmores : this.selectedSchoolsOmoron,
                aitisi_type : 'Απόσπαση',
                stay_to_organiki : this.stay_to_organiki,
                return_to_organiki : this.return_to_organiki
            })
                .then(r => {
                    this.displayAlert(r.data, 'success', true);
                    this.requestInDatabase = true;
                })
                .catch(r => console.log('Error'));
        },

        sentRequest (){
            console.log('Stay: ' + this.stay_to_organiki + ' Return: ' + this.return_to_organiki);
            axios.post('/aj/organikes/sentRequestForProtocolYperarithmos',{
                selectedSchools : dilosi.selectedSchools,
                selectedSchoolsIdiaOmada : dilosi.selectedSchoolsIdiasOmadas,
                selectedSchoolsOmores : dilosi.selectedSchoolsOmoron,
                aitisi_type : 'Απόσπαση',
                stay_to_organiki : dilosi.stay_to_organiki,
                return_to_organiki : dilosi.return_to_organiki
            })
                .then(r => location.href = r.data)
                //.then(r => console.log('data:' + r.data))
                .catch(r => console.log('Error'));
        },

        openModal(){
            this.showModal = true;
        },

        initIdiaOmada (){
            this.selectedSchoolsIdiasOmadas = [];
            if( this.selectedSchoolsOmoron.length == 0 && this.taktikes_apospaseis_checked){
                this.selectedSchools = [];
                this.taktikes_apospaseis_checked = false;
                this.alert.message = ['Οι επιλογές σας για τακτικές αποσπάσεις διαγράφηκαν. Αν επιθυμείτε ΜΟΝΟ τακτικές αποσπάσεις πηγαίνετε στις Αιτήσεις για εκπαιδευτικούς' +
                'τακτικές αποσπάσεις'];
                this.displayAlert(this.alert.message, 'danger', true);
            }
        },

        initOmoriOmada(){
            this.selectedSchoolsOmoron = [];
            if( this.selectedSchoolsIdiasOmadas.length == 0 && this.taktikes_apospaseis_checked){
                this.selectedSchools = [];
                this.taktikes_apospaseis_checked = false;
                this.alert.message = ['Οι επιλογές σας για τακτικές αποσπάσεις διαγράφηκαν. Αν επιθυμείτε ΜΟΝΟ τακτικές αποσπάσεις πηγαίνετε στις Αιτήσεις για εκπαιδευτικούς' +
                'τακτικές αποσπάσεις'];
                this.displayAlert(this.alert.message, 'danger', true);
            }
        },

        initApospasi(){
            this.selectedSchools = [];
            if(this.selectedSchoolsIdiasOmadas.length == 0 && this.selectedSchoolsOmoron.length == 0){
                this.taktikes_apospaseis_checked = false;
                this.alert.message = ['Για την ενεργοποίηση αυτής της επιλογής, προϋπόθεση είναι να έχετε επιλέξει τουλάχιστον ένα Σχολείο ίδιας ή όμορης Ομάδας' +
                ' διαφορετικά θα πρέπει να κάνετε ΜΟΝΟ αίτηση για απόσπαση'];
                this.displayAlert(this.alert.message, 'danger', true);
            }
        },

        removeSchool(id){
            return this.allSchools.filter(obj => {
                if(obj.id == id) this.allSchools.$remove(obj);
            });
        },
        removeSchoolIdiaOmada(id){
            return this.allSchoolsIdiasOmadas.filter(obj => {
                if(obj.id == id) this.allSchoolsIdiasOmadas.$remove(obj);
            });
        },
        removeSchoolOmoriOmada(id){
            return this.allSchoolsOmoron.filter(obj => {
                if(obj.id == id) this.allSchoolsOmoron.$remove(obj);
            });
        },


        addSchoolToChoices (obj){
            this.selectedSchools.push(obj);
            this.allSchools.$remove(obj);
        },
        addSchoolToChoicesIdiaOmada (obj){
            this.selectedSchoolsIdiasOmadas.push(obj);
            this.allSchoolsIdiasOmadas.$remove(obj);
        },
        addSchoolToChoicesOmoriOmada (obj){
            this.selectedSchoolsOmoron.push(obj);
            this.allSchoolsOmoron.$remove(obj);
        },


        fetchSchools (){
            this.loader = true;
            axios.post('/aj/organika/ajaxFetchSchools', {
                schoolType : this.schoolType,
                aitisi_type : this.type_of_aitisi
            })
                .then(r => {
                    console.log(r.data);
                    //if(r.data.isMadeValue === 'protocol_given' ){
                    //    this.hasMadeRequest = true;
                    //}else{
                    //
                    //    this.stay_to_organiki = r.data.stay_to_organiki;
                    //    this.return_to_organiki = r.data.return_to_organiki;
                    //
                    //    this.isMadeValueApospasi = r.data.isMadeValueApospasi;
                    //
                    //    this.schoolType = r.data.schoolType;
                    //
                    //    this.allSchools = r.data.allSchools;
                    //    this.allSchoolsIdiasOmadas = r.data.allSchoolsIdiasOmadas;
                    //    this.allSchoolsOmoron = r.data.allSchoolsOmoron;
                    //    this.idiaomada = r.data.idiaomada;
                    //    this.omoresArray = r.data.omoresArray;
                    //
                    //    this.selectedSchools = r.data.selectedSchools;
                    //    if(this.selectedSchools.length > 0) this.taktikes_apospaseis_checked = true;
                    //
                    //    this.selectedSchoolsIdiasOmadas = r.data.selectedSchoolsIdiaOmada;
                    //    if(this.selectedSchoolsIdiasOmadas.length > 0) this.idia_omada_checked = true;
                    //
                    //    this.selectedSchoolsOmoron = r.data.selectedSchoolsOmores;
                    //    if(this.selectedSchoolsOmoron.length > 0) this.omori_omada_checked = true;
                    //
                    //
                    //    if(this.selectedSchoolsIdiasOmadas.length > 0 || this.selectedSchoolsOmoron.length > 0){
                    //        this.requestInDatabase = true;
                    //    }
                    //
                    //}
                    //setTimeout(function(){
                    //    this.loader = false;
                    //}.bind(this),1000);
                    //
                    //Vue.nextTick(function () {
                    //    console.log('ok .sd');
                    //    $('[data-toggle="tooltip"]').tooltip()
                    //})
                })
                .catch(r => console.log('error345'))
        },
        swapUpIdiaOmada(index){
            this.swapIdiaOmada(index-1, index)
        },



        swapDown(index){
            this.swap(index, index +1);
        },
        swapDownIdiaOmada(index){
            this.swapIdiaOmada(index, index +1);
        },

        swapUp(index){
            this.swap(index-1, index)
        },

        swapUpOmoriOmada(index){
            this.swapOmoriOmada(index-1, index)
        },

        swapDownOmoriOmada(index){
            this.swapOmoriOmada(index, index +1);
        },

        swap(a, b){
            let temp = this.selectedSchools[a];
            this.selectedSchools.splice(a, 1, this.selectedSchools[b]);
            this.selectedSchools.$set(b, temp);
        },

        swapIdiaOmada(a, b){
            let temp = this.selectedSchoolsIdiasOmadas[a];
            this.selectedSchoolsIdiasOmadas.splice(a, 1, this.selectedSchoolsIdiasOmadas[b]);
            this.selectedSchoolsIdiasOmadas.$set(b, temp);
        },

        swapOmoriOmada(a, b){
            let temp = this.selectedSchoolsOmoron[a];
            this.selectedSchoolsOmoron.splice(a, 1, this.selectedSchoolsOmoron[b]);
            this.selectedSchoolsOmoron.$set(b, temp);
        },

        deleteAllSelected (){
            this.selectedSchools.forEach(r => this.putBackSchool(r));
            this.selectedSchools = [];
        },

        deleteAllSelectedIdiaOmada (){
            this.selectedSchoolsIdiasOmadas.forEach(r => this.putBackSchoolIdiaOmada(r));
            this.selectedSchoolsIdiasOmadas = [];
        },

        deleteAllSelectedOmoriOmada (){
            this.selectedSchoolsOmoron.forEach(r => this.putBackSchoolOmoriOmada(r));
            this.selectedSchoolsOmoron = [];
        },

        deleteSchool(obj){
            this.selectedSchools.$remove(obj);
        },

        deleteSchoolIdiaOmada(obj){
            this.selectedSchoolsIdiasOmadas.$remove(obj);
        },

        deleteSchoolOmoriOmada(obj){
            this.selectedSchoolsOmoron.$remove(obj);
        },


        putBackSchool(obj){
            this.allSchools.push(obj);
            this.allSchools.sort(this.sortItems);
        },
        putBackSchoolIdiaOmada(obj){
            this.allSchoolsIdiasOmadas.push(obj);
            this.allSchoolsIdiasOmadas.sort(this.sortItems);
        },
        putBackSchoolOmoriOmada(obj){
            this.allSchoolsOmoron.push(obj);
            this.allSchoolsOmoron.sort(this.sortItems);
        },


        deleteSelectedSchool(obj){
            this.deleteSchool(obj);
            this.putBackSchool(obj);
        },
        deleteSelectedSchoolIdiaOmada(obj){
            this.deleteSchoolIdiaOmada(obj);
            this.putBackSchoolIdiaOmada(obj);
        },
        deleteSelectedSchoolOmoriOmada(obj){
            this.deleteSchoolOmoriOmada(obj);
            this.putBackSchoolOmoriOmada(obj);
        },

        displayAlert(header, body, type, important = false){
            this.$refs.alert.broadcast(header, body, type, important);
        },

        sortItems(a,b){
            if (a.order < b.order){
                return -1;
            }
            if (a.order > b.order){
                return 1;
            }
        }
    },

    components : {
        Alert, Modal
    }
});