@extends('app')

@section('header.style')
    <style>
        .loading{
            width: 80px;
            height: 80px;
        }
        .locked{
            /*background-color: #008000;*/
            text-align: center;
        }
        .unlocked{
            /*background-color: #0000cc;*/
            text-align: center;
            font-size: 12pt;
        }
        .computed{
            /*background-color: #183c60;*/
            text-align: center;
        }
        .table_th{
            text-align: center;
        }
        .locked_cell{
            background-color: #99A3A4;
        }
        .span_header{
            background-color: #3498DB;
            color: #f5f5f5;
        }

    </style>
@endsection

@section('content')
    <div id="organikes">
        <h2 class="page-heading">Αριθμός Τμημάτων</h2>

        <div v-cloak v-if="loading" class="panel-body text-center">
            <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
            <h5>παρακαλώ περιμένετε...</h5>
        </div>

        <div v-else v-cloak class="row">
            <div class="col-md-4">
                <div v-cloak v-if="studentsLength > 0">
                    <table border="1">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="table_th span_header" colspan="3">ΤΑΞΕΙΣ</th>
                            </tr>
                            <tr>
                                <th width="40%" class="table_th">Τμήματα</th>
                                <th width="20%" class="table_th">Α</th>
                                <th width="20%" class="table_th">Β</th>
                                <th width="20%" class="table_th">Γ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-cloak v-for="(div, index) in divisions">
                                <td style="font-size: 12pt">@{{ index }}</td>
                                <td class="locked_cell" align="center" v-for="i in 3">
                                    <span v-cloak v-if="div[i-1]['status'] === 'unlocked'">
                                        <input size="7" maxlength="2"  @keyup="setSecondLanguage(div[i-1])" v-bind:class="{
                                            'locked' : div[i-1]['status'] === 'locked',
                                            'unlocked' : div[i-1]['status'] === 'unlocked'
                                        }" type="text" v-model="div[i-1]['numberSchoolProvide']">
                                    </span>
                                    <span v-cloak v-else>
                                        @{{ div[i-1]['numberSchoolProvide'] }}
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-3">
                <table border="1">
                    <thead>
                        <tr>
                            <th class="table_th span_header" colspan="3"><h5>Αριθμός Μαθητών</h5></th>
                        </tr>
                        <tr>
                            <th class="table_th locked_cell">Α Τάξη</th>
                            <th class="table_th locked_cell">Β Τάξη</th>
                            <th class="table_th locked_cell">Γ Τάξη</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input class="unlocked" size="7" maxlength="3" @keyup="calculateClass('Α')" type="text" v-model="students['Α']"/></td>
                            <td><input class="unlocked" size="7" maxlength="3" @keyup="calculateClass('Β')" type="text" v-model="students['Β']"/></td>
                            <td><input class="unlocked" size="7" maxlength="3" @keyup="calculateClass('Γ')" type="text" v-model="students['Γ']"/></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <button v-bind:disabled="notInteger" @click="openModal" class="btn btn-success btn-lg">
                    Αποστολή
                </button>

                <div v-cloak v-show="notInteger" class="alert alert-danger text-center" role="alert">
                    <p><strong>Προσοχή!!! </strong>Σε κάποιο κελί έχετε εισάγει ΜΗ ακέραιο αριθμό.</p>
                </div>
                <div v-cloak v-show="isPositive" class="alert alert-danger text-center" role="alert">
                    <p><strong>Προσοχή!!! </strong>Σε κάποιο κελί έχετε εισάγει αρνητικό αριθμό.</p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="alert alert-warning" role="alert">
                    <h4 class="text-center">Οδηγίες:</h4>
                    <p><strong>1. </strong>Ενημερώστε τον δεξιό Πίνακα (Αριθμός Μαθητών) με τον σωστό αριθμό μαθητών ανά Τάξη στο αντίστοιχο κουτάκι.</p>
                    <p><strong>2. </strong>Σε περίπτωση που διαφωνείτε με την πρόβλεψη του αριθμού των τμημάτων ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ ανά Τάξη στον αριστερό πίνακα (ΤΑΞΕΙΣ) διορθώστε τον αριθμό στο κουτάκι.</p>
                    <p><strong>3. </strong>Εαν το επιθυμείτε, τροποποιήστε στον αριστερό πίνακα τα τμήματα των Γαλλικών ή των Γερμανικών έτσι ώστε το άθροισμά τους να είναι το ίδιο με τον αριθμό τμημάτων στα ΑΓΓΛΙΚΑ.</p>
                </div>
            </div>
        </div>

        <modal v-cloak
            v-on:cancel-the-request="cancelOperation"
            v-on:emit-the-request="sentRequest"
            :show="openAgreementModal"
            :loader="hideLoader"
            type="agreement"
        >
            <h5 slot="title">Όροι και προϋποθέσεις</h5>
            <ol slot="body">
                    <li>
                        Θα πρέπει να συμφωνείτε με όλα τα στοιχεία των πινάκων
                    </li>
                    <li>
                        Με την αποστολή:
                        <ul>
                            <li>στέλνεται E-mail, με όλα τα δεδομένα, στη γραμματεία του ΠΥΣΔΕ</li>
                            <li>στέλνεται ειδοποίηση στη ΔΔΕ (στην ΠΑΣΙΦΑΗ) ότι το Σχολείο σας ενημέρωσε τους πίνακες με τον αριθμό Τμημάτων και το σύνολο των μαθητών</li>
                        </ul>
                    </li>
                    <li>
                        Αν συμφωνείτε με τους παραπάνω όρους, πατήστε το κουμπί της Αποστολής
                    </li>
            </ol>
            <span slot="button">Αποστολή</span>
        </modal>

        <alert ref="alert"></alert>

        @include('vueloader')

    </div>
@stop


@section('scripts.footer')
    <script src="{{ mix('vendor/pysde/js/divisions.js') }}"></script>
@endsection
