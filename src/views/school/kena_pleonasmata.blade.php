@extends('app')

@section('header.style')
    <style>
        .loading{
            width: 80px;
            height: 80px;
        }
        .differentNumbers {
            background-color: #ff8566;
        }
        .buttons-margin{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .zero_div{
            color: red;
            text-align: center;
            padding-left: 10px;
        }

        .council-text{
            text-align: center;
            background-color: #787878;
            color: white;
            width: 100%;
        }

        .council-cell{
            text-align: center;
        }
        .pleonasma{
            color : #991f00;
            font-size: 12pt;
            font-weight: 800;
        }
        .elleima{
            color : #008000;
            font-size: 12pt;
            font-weight: 800;
        }
        .different-from-council{
            color: red;
        }

        .t-center{
            text-align: center;
        }

        .t-title{
            padding-left: 10px;
            padding-right: 10px;
        }
        .t-results{
            text-align: center;
            font-weight: bold;
            font-size : 14pt;
            color: blue;
        }

        .stripes {
            background-image: linear-gradient(135deg, #000 25%, transparent 25%, transparent 50%, #000 50%, #000 75%, transparent 75%, #fff);
            background-size: 5px 5px;
        }

    </style>
@endsection

@section('content')
    <div id="vacuum">
        <h2 class="page-heading">Κενά - Πλεονάσματα για Βελτιώσεις / Οριστικές Τοποθετήσεις</h2>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">

                    <div v-cloak v-if="loading" class="panel-body text-center">
                        <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                        <h5>παρακαλώ περιμένετε...</h5>
                    </div>

                    <div v-cloak v-else class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <table class="table-pe04" border="1">
                                    <thead>
                                        <tr>
                                            <th class="t-center" colspan="5">Πίνακας κλάδου ΠΕ04</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th class="t-center">Ανάγκες</th>
                                            <th class="t-center">Διαθέσιμες</th>
                                            <th class="t-center">Project</th>
                                            <th class="t-center">Αποτέλεσμα</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(eid04, index) in kladosPe04">
                                            <td class="t-title">@{{ eid04.eidikotita_slug }}</td>
                                            <td class="t-center">@{{ eid04.pivot.council_forseen }}</td>
                                            <td class="t-center">@{{ eid04.pivot.available }}</td>
                                            <td class="t-center">@{{ eid04.pivot.project_hours }}</td>

                                            <td class="t-results" v-if="index==0" v-bind:rowspan="kladosPe04.length">
                                                @{{ sumPe04Diff }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="t-center">Σύνολα</td>
                                            <td class="t-center">@{{ this.sumPe04Forseen }}</td>
                                            <td class="t-center">@{{ this.sumPe04Available }}</td>
                                            <td class="t-center">@{{ this.sumPe04Projects }}</td>
                                            <td class="t-center">@{{ (this.diffPe04) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <div v-cloak v-if="eidikotites!=null">

                                    <table border="1">
                                        <thead>
                                            <tr>
                                                <th align="center">Ειδικότητα</th>
                                                <th align="center">Διδακτικές Ώρες</th>
                                                <th align="center">Διαθέσιμες Ωρες</th>
                                                <th v-show="school_type === 'Λύκειο'" align="center">Project</th>
                                                <th align="center">Διαφορά</th>
                                                <th align="center">Αρ. Καθηγητών</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-cloak v-for="eidikotita in eidikotites">
                                                <td style="padding-left:3px">
                                                    @{{eidikotita.eidikotita_slug}} - @{{ eidikotita.eidikotita_name }}
                                                </td>
                                                <td align="center">
                                                    @{{ eidikotita.pivot.council_forseen }}
                                                </td>
                                                <td align="center">@{{ eidikotita.pivot.available }}</td>
                                                <td v-cloak v-show="school_type === 'Λύκειο'" align="center">
                                                    <span v-cloak v-show="eidikotita.pivot.project_hours > 0">@{{ eidikotita.pivot.project_hours }}</span></td>
                                                <td align="center" :class="{'stripes' : eidikotita.klados_slug === 'ΠΕ04'}">@{{ eidikotita.pivot.council_difference }}</td>
                                                <td align="center">@{{ eidikotita.pivot.number_of_teachers }}</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
@stop

@section('scripts.footer')
    <script>
        window.school_type = "{{ Auth::user()->userable->type }}";
    </script>
    <script src="{{ mix('vendor/pysde/js/school_vacuum.js') }}"></script>

@endsection


