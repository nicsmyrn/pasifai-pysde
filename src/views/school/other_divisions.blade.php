@extends('app')

@section('title')
    Δήλωση Κενών - Πλεονασμάτων για Οργανικές Θέσεις στη Σχολική Μονάδα.
@endsection

@section('loader')
    @include('vueloader')
@endsection

@section('content')
    <other
        @display-loader="displayLoader"
        @not-display-loader="notDisplayLoader"
        @notification-message="notifyMessage"
    ></other>

    <new-alert 
        ref="alert"
    ></new-alert>
@stop


@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
    </script>
    <script src="{{ mix('vendor/pysde/js/school_divisions.js') }}"></script>
@endsection
