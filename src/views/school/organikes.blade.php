@extends('app')

@section('header.style')
    <style>
        .veltiwsi{
            background-color: #008B8B;
            color: white
        }
    </style>

@endsection

@section('content')
    <div id="organikes">
        <h2 class="page-heading">Οργανικά ανήκοντες</h2>

        <div class="row">
            <div class="col-md-12">

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th align="center">Α/Α</th>
                                <th align="center">Ειδικότητα</th>
                                <th align="center">Επώνυμο</th>
                                <th align="center">Όνομα</th>
                                <th align="center">Πατρώνυμο</th>
                                <th align="center">ΑΜ</th>
                                {{-- <th align="center">Τοποθέτηση</th> --}}
                                <th align="center">Υπ. Ωράριο</th>
                                <th align="center">Κατηγορία</th>
                                <td align="center">Αίτηση Βελτίωσης</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($teachers as $index=>$teacher)
                                <tr @if($teacher['aitisi_veltiwsis']) class="veltiwsi" @endif>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $teacher['eidikotita'] }}</td>
                                    <td>{{ $teacher['last_name'] }}</td>
                                    <td>{{ $teacher['first_name'] }}</td>
                                    <td>{{ $teacher['middle_name'] }}</td>
                                    <td>{{ $teacher['am'] }}</td>
                                    {{-- <td>{{ $teacher['topothetisi'] }}</td> --}}
                                    <td class="text-center">{{ $teacher['ypoxreotiko'] }}</td>
                                    <td class="text-center">{{ $teacher['type'] }} ΑΓΩΓΗΣ</td>
                                    <td class="text-center">{{ $teacher['aitisi_veltiwsis'] }}</td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

            </div>
        </div>

    </div>
@stop


