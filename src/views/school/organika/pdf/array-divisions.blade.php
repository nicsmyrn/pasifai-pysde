<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    @include('pysde::school.organika.pdf._styles')

</head>
<body class="page" marginwidth="0" marginheight="0">

    <table>
        <tbody>
            <tr id="header-menu">
                <td width="50%">
                    <div>
                        <span id="t1c3s1">Αρ. Πρωτοκόλλου:</span>
                    </div>
                </td>
                <td width="50%">
                    <div class="to">
                        Προς ΠΥΣΔΕ ΧΑΝΙΩΝ
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" width="100%">
                    <div class="dilosi">
                        <div class="header">
                            ΠΙΝΑΚΑΣ ΟΡΓΑΝΙΚΩΝ ΚΕΝΩΝ - ΠΛΕΟΝΑΣΜΑΤΩΝ
                        </div>
                        <div class="header">
                            &laquo;{{ $school }}&raquo;
                        </div>
                        <div class="little-header">
                            {{ $description }}
                        </div>
                    </div>

                </td>
            </tr>
        </tbody>
    </table>

    @if($general_divisions != null)
            @include('pysde::school.organika.pdf._lyceum-divs')
    @endif

    <table>
        <tbody>
            <tr>
                <td>
                    <table class="withBorders vacuum-table">
                        <tbody>
                            <tr>
                                <td class="with-borders-header" colspan="6">ΠΙΝΑΚΑΣ Α - Οργανικών Κενών/Πλεονασμάτων</td>
                            </tr>
                            <tr>
                                <td class="with-borders-field-title">Ειδικότητα</td>
                                <td class="with-borders-field-title">Ωρες Διδασκαλίας(1)</td>
                                <td class="with-borders-field-title">Ωράριο Εκπ/κών(3)</td>
                                <td class="with-borders-field-title">Εργ. Δεξ.</td>
                                <td class="with-borders-field-title">Έλλειμμα(4α)</td>
                                <td class="with-borders-field-title">Πλεόνασμα(4β)</td>
                            </tr>
                            @foreach($kena as $eidikotita)
                                <tr>
                                    <td>{{ $eidikotita['eidikotita_slug'] }} {{ $eidikotita['eidikotita_name'] }}</td>
                                    <td>{{ $eidikotita['pivot']['forseen'] }}</td>
                                    <td>{{ $eidikotita['pivot']['available'] }}</td>
                                    <td>{{ $eidikotita['pivot']['project_hours'] }}</td>
                                    @if($eidikotita['klados_slug'] == 'ΠΕ04')
                                        <td class="stripes"></td>
                                        <td class="stripes"></td>
                                    @else
                                        <td>{{ $eidikotita['pivot']['difference'] < 0 ? $eidikotita['pivot']['difference'] : '' }}</td>
                                        <td>{{ $eidikotita['pivot']['difference'] > 0 ? $eidikotita['pivot']['difference'] : '' }}</td>
                                    @endif

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <table>
        <tbody>
            <tr>
                <td>
                    <table class="withBorders vacuum-table">
                        <tbody>
                            <tr>
                                <td class="with-borders-header" colspan="5">Πίνακας Κλάδου ΠΕ04</td>
                            </tr>
                            <tr>
                                <td class="with-borders-field-title">Ειδικότητες</td>
                                <td class="with-borders-field-title">Διδακτικές Ώρες(1)</td>
                                <td class="with-borders-field-title">Διαθέσιμες Ώρες(3)</td>
                                <td class="with-borders-field-title">Εργ. Δεξ.</td>
                                <td class="with-borders-field-title">Αποτέλεσμα</td>
                            </tr>
                            @foreach($kenaPe04 as $k=>$e)
                                <tr>
                                    <td>{{ $e['eidikotita_slug'] }} {{ $e['eidikotita_name'] }}</td>
                                    <td>{{ $e['pivot']['forseen'] }}</td>
                                    <td>{{ $e['pivot']['available'] }}</td>
                                    <td>{{ $e['pivot']['project_hours'] }}</td>
                                    @if($k == 0)
                                        <td rowspan="{{ $kenaPe04->count() }}">{{ $sumPe04 }}</td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <table>
        <tbody>
            <tr>
                <td><u>Παρατηρήσεις:</u></td>
            </tr>
            <tr><td>______________________________________________________________________________________</td></tr>
            <tr><td>______________________________________________________________________________________</td></tr>
            <tr><td>______________________________________________________________________________________</td></tr>
            <tr><td>______________________________________________________________________________________</td></tr>
            <tr><td>______________________________________________________________________________________</td></tr>
            <tr><td>______________________________________________________________________________________</td></tr>
        </tbody>
    </table>

    <table>
        <tbody>
            <tr>
                <td colspan="2" width="50%">
                    <div class="who">
                        @if($sex == 1)
                            Ο Διευθυντής
                        @else
                            Η Διευθύντρια
                        @endif
                    </div>
                    <div class="signature">
                        {{ $admin }}
                    </div>
                    <div class="line-data">
                        {{ $now }}
                    </div>
                    <div class="line-data">
                        <span style="border-top: 2px solid #000000">ΔΗΜΙΟΥΡΓΙΑ ΕΓΓΡΑΦΟΥ</span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</body>
</html>
