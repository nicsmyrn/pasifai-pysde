<style>
        body{
            font-family: DejaVu Sans, sans-serif;
            margin: 0;
            font-size: 11pt;
            text-align: justify;
        }

        table {
            width: 100%;
            margin-left: 30px;
            margin-right: 30px;
            margin-top: 3px;
            margin-bottom: 15px;
        }

        .vacuum-table{
            border-collapse: collapse;
        }

        .withBorders{
            /* width: 50%; */
            margin-left: 10px;
            margin-right: 10px;
        }

        .withBorders td{
            border: 1px solid #000000;
            text-align: center;
            font-size: 10pt;
        }

        .with-borders-header{
            font-weight: bold;
            font-size: 11pt !important;
            background-color: #808B96;
        }

        .with-borders-field-title{
            background-color: #D6DBDF;
        }

        .t1c1{
            font-size: 18pt;
            font-weight: bold;
            text-align: center;
            letter-spacing: 5px;
        }


        .t1c3s1{
            font-size: 10pt;
        }

        #t1c3s2{
            font-size: 9pt;
            font-weight: bold;
        }
        .to{
            font-size: 10pt;
            text-align: right;
        }

        .t1c4{
            /*text-align: center;*/
            font-size: 12pt;
            font-weight: 800;
            padding-left: 30px;
        }

        #description{
            padding-left: 20px;
            line-height: 200%;
            text-align: justify;
            font-size: 12pt;
        }

        .line-data{
            font-size: 8pt;
            text-align: center;
        }

        .text-center {
            text-align: center;
        }

        .header{
            font-size: 12pt;
            font-weight: bold;
            text-align: center;
        }

        .little-header{
            font-size: 10pt;
            text-align: center;
        }

        #content-body{
            margin-top: 50px;
            margin-bottom: 50px;
            padding-top: 30px;
            padding-bottom: 50px;
            margin-left: 10px;
            margin-right: 10px;
            font-size: 11pt;
            line-height: 30px;
            text-align: justify;
        }

        .dilosi{
            width: 70%;
            margin: auto;
            padding-top: 20px;
            padding-bottom: 20px;
        }

        .who{
            font-size: 11pt;
            text-align: center;     
            margin-top: 15px;
            margin-bottom: 50px
        }

        .signature{
            font-size: 11pt;
            text-align: center;
        }

        #header-menu{
            margin-bottom: 30px;
        }

        .asterisk{
            color: red;
            font-weight: bold;
        }

        .stripes {
            background-color: #808B96;
        }
    </style>