<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    @include('pysde::school.organika.pdf._styles')

</head>
<body class="page" marginwidth="0" marginheight="0">

    <table>
        <tbody>
            <tr id="header-menu">
                <td width="50%">
                    <div>
                        <span id="t1c3s1">Αρ. Πρωτοκόλλου:</span>
                    </div>
                </td>
                <td width="50%">
                    <div class="to">
                        Προς ΠΥΣΔΕ ΧΑΝΙΩΝ
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" width="100%">
                    <div class="dilosi">
                        <div class="header">
                            ΠΙΝΑΚΑΣ ΔΗΛΩΣΗΣ ΤΜΗΜΑΤΩΝ
                        </div>
                        <div class="header">
                            &laquo;{{ $school }}&raquo;
                        </div>
                        <div class="little-header">
                            {{ $description }}
                        </div>
                    </div>

                </td>
            </tr>
        </tbody>
    </table>

    @if($general_divisions != null)
        @if($type === 'gym')
            @include('pysde::school.organika.pdf._gymnasium-divs')
        @else
            @include('pysde::school.organika.pdf._lyceum-divs')
        @endif
    @endif

    @if($division_direction != null)
        <table>
            <tbody>
                <tr>
                    @foreach($division_direction as $key=>$divs)
                        @if(!empty($divs))
                            <td>
                                <table class="withBorders">
                                    <tbody>
                                        <tr>
                                            <td class="with-borders-header" colspan="3">ΠΡΟΣΑΝΑΤΟΛΙΣΜΟΣ {{ $key }} ΛΥΚΕΙΟΥ</td>
                                        </tr>
                                        <tr>
                                            <td class="with-borders-field-title">Μάθημα</td>
                                            <td class="with-borders-field-title">Αρ. Μαθητών</td>
                                            <td class="with-borders-field-title">Αρ. Τμημάτων</td>
                                        </tr>
                                        @foreach($divs as $div)
                                            <tr>
                                                <td>{{ $div['name'] }} @if($div['oligomeles'])<span class="asterisk">&#8258;</span>@endif @if($div['pedio2'])<span class="asterisk">Πεδ_2</span>@endif @if($div['pedio3'])<span class="asterisk">Πεδ_3</span>@endif</td>
                                                <td>{{ $div['students'] }}</td>
                                                <td>{{ $div['numberSchoolProvide'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @if($key == 'Γ')
                                    <div>
                                        <label><span class="asterisk">&#8258;</span> Συνδιδασκαλία</label>
                                    </div>
                                @endif
                            </td>
                        @endif
                    @endforeach
                </tr>
            </tbody>
        </table>
    @endif

    @if($foreign_languages != null)
        <table>
            <tbody>
                <tr>
                    @foreach($foreign_languages as $key=>$divs)
                        @if(!empty($divs))
                            <td>
                                <table class="withBorders">
                                    <tbody>
                                        <tr>
                                            <td class="with-borders-header" colspan="3">ΞΕΝΕΣ ΓΛΩΣΣΕΣ {{ $key }} ΛΥΚΕΙΟΥ</td>
                                        </tr>
                                        <tr>
                                            <td class="with-borders-field-title">Μάθημα</td>
                                            <td class="with-borders-field-title">Αρ. Μαθητών</td>
                                            <td class="with-borders-field-title">Αρ. Τμημάτων</td>
                                        </tr>
                                        @foreach($divs as $div)
                                            <tr>
                                                <td>{{ $div['name'] }}</td>
                                                <td>{{ $div['students'] }}</td>
                                                <td>{{ $div['numberSchoolProvide'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </td>
                        @endif
                    @endforeach
                </tr>
            </tbody>
        </table>
    @endif

    @if($division_choice != null)
        <table>
            <tbody>
                <tr>
                    @foreach($division_choice as $key=>$divs)
                        @if(!empty($divs))
                            <td>
                                <table class="withBorders">
                                    <tbody>
                                        <tr>
                                            <td class="with-borders-header" colspan="3">ΕΠΙΛΟΓΗΣ {{ $key }} ΛΥΚΕΙΟΥ</td>
                                        </tr>
                                        <tr>
                                            <td class="with-borders-field-title">Μάθημα</td>
                                            <td class="with-borders-field-title">Αρ. Μαθητών</td>
                                            <td class="with-borders-field-title">Αρ. Τμημάτων</td>
                                        </tr>
                                        @foreach($divs as $div)
                                            <tr>
                                                <td>{{ $div['name'] }}</td>
                                                <td>{{ $div['students'] }}</td>
                                                <td>{{ $div['numberSchoolProvide'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </td>
                        @endif
                    @endforeach
                </tr>
            </tbody>
        </table>
    @endif

    @if($divisions_projects)
        <table>
            <tbody>
                <tr>
                    <td>
                        <table class="withBorders">
                            <tbody>
                                <tr>
                                    <td class="with-borders-header" colspan="2">ΕΡΓΑΣΤΗΡΙΑ ΔΕΞΙΟΤΗΤΩΝ</td>
                                </tr>
                                <tr>
                                    <td class="with-borders-field-title">Ειδικότητα</td>
                                    <td class="with-borders-field-title">Ώρες</td>
                                </tr>
                                @foreach($divisions_projects as $div)
                                    <tr>
                                        <td>{{ $div['slug'] . ' [' . $div['name'] . ']' }}</td>
                                        <td>{{ $div['hours'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    @endif

    <table>
        <tbody>
            <tr>
                <td colspan="2" width="50%">
                    <div class="who">
                        @if($sex == 1)
                            Ο Διευθυντής
                        @else
                            Η Διευθύντρια
                        @endif
                    </div>
                    <div class="signature">
                        {{ $admin }}
                    </div>
                    <div class="line-data">
                        {{ $now }}
                    </div>
                    <div class="line-data">
                        <span style="border-top: 2px solid #000000">ΔΗΜΙΟΥΡΓΙΑ ΕΓΓΡΑΦΟΥ</span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</body>
</html>
