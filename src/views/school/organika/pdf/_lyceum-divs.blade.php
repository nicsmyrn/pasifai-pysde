<table>
    <tbody>
        <tr>
            <td>
                <table class="withBorders">
                    <tbody>
                        <tr>
                            <td class="with-borders-header" colspan="3">ΤΜΗΜΑΤΑ ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ</td>
                        </tr>
                        <tr>
                            <td class="with-borders-field-title">Τάξη</td>
                            <td class="with-borders-field-title">Αρ. Μαθητών</td>
                            <td class="with-borders-field-title">Αρ. Τμημάτων</td>
                        </tr>
                        @foreach($general_divisions as $div)
                            <tr>
                                <td>{{ $div['class'] }}</td>
                                <td>{{ $div['students'] }}</td>
                                <td>{{ $div['numberSchoolProvide'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>