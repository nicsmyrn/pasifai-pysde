<table>
    <tbody>
        <tr>
            <td>
                <table class="withBorders">
                    <tbody>
                        <tr>
                            <td class="with-borders-header" colspan="3">ΑΡΙΘΜΟΣ ΜΑΘΗΤΩΝ</td>
                        </tr>
                        <tr>
                            <td class="with-borders-field-title">Α' ΤΑΞΗ</td>
                            <td class="with-borders-field-title">Β' ΤΑΞΗ</td>
                            <td class="with-borders-field-title">Γ' ΤΑΞΗ</td>
                        </tr>
                        <tr>
                            @foreach($students as $value)
                                <td>{{ $value }}</td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<table>
    <tbody>
        <tr>
            <td>
                <table class="withBorders">
                    <tbody>
                        <tr>
                            <td class="with-borders-header" colspan="4">ΤΜΗΜΑΤΑ ΓΥΜΝΑΣΙΟΥ</td>
                        </tr>
                        <tr>
                            <td class="with-borders-field-title"></td>
                            <td class="with-borders-field-title">Α</td>
                            <td class="with-borders-field-title">Β</td>
                            <td class="with-borders-field-title">Γ</td>
                        </tr>
                        @foreach($divisions as $key=>$group)
                            <tr>
                                <td>{{ $key }}</td>
                                @foreach($group as $div)
                                    <td>{{ $div['numberSchoolProvide'] }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>