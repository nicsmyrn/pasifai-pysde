<table id="requests" class="table table-bordered table-hover" cellspacing="0" width="100%">
    <thead>
        <tr class="active">
            <th class="text-center">Χρήστης</th>
            <th class="text-center">Ονοματεπώνυμο</th>
            <th class="text-center">ΑΦΜ</th>
            <th class="text-center">Κωδικός Αίτησης</th>
            <th class="text-center">Ημ/νια</th>
            <th class="text-center">Αρ. Αίτησης</th>
            <th class="text-center">Είδος</th>
            <th class="text-center">Ενέργειες</th>
            <th class="text-center">Μεταμόρφωση</th>
        </tr>
    </thead>

    <tbody>
            <tr v-for="request in requests">
                <td>@{{ request.sch_mail }} </td>
                <td>@{{ request.full_name }} </td>
                <td>@{{ request.afm }} </td>
                <td>@{{ request.unique_id }} </td>
                <td>@{{ request.req_date  }} </td>
                <td>@{{ request.protocol_name }} </td>
                <td>@{{ request.type }} </td>
                <td> 
                    <span v-if="request.description == null">
                        tik
                    </span>

                    <span v-else>
                        <span class="label label-danger">@{{ request.description }}</span>
                        <span v-if="request.description === 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ'">
                            <div class="btn-group" data-toggle="buttons">
                                <button type="button" @click="cancelRecall(request.unique_id)" title="Ακύρωση Ανάκλησης" class="btn btn-warning btn-xs">ΜΗ Έγκριση</button>
                                <button type="button" @click="approvedRecall(request.unique_id)" title="Έγκριση Ανάκλσης"  class="btn btn-success btn-xs">ΕΓΚΡΙΣΗ</button>
                            </div>
                        </span>
                    </span>
                </td>
                <td class="text-center">
                    <a :href="request.downloadUrl"><img style="width: 30px; height: 30px" src="/img/download_icon.jpg"/> </a>
                </td>
            </tr>
    </tbody>
</table>