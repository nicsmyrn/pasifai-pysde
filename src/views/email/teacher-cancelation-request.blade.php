@component('mail::message')

Ο Εκπαιδευτικός {{ $teacher_name }}  σας έστειλε αίτημα για ανάκληση παλαιότερης αίτησής του.


το μήνυμα περιλαμβάνει συννημμένη την αίτηση

@component('mail::signature', ['email' => Config::get('requests.MAIL_PYSDE')])
| {{Config::get('requests.arxika')}}          |
| -------------------------------- |
| {{Config::get('requests.address')}} |
@endcomponent

@endcomponent

