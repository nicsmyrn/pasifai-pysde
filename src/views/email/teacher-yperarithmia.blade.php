@component('mail::message')

Ο Εκπαιδευτικός {{ $teacher_name }}  σας έστειλε αίτηση για χαρακτηρισμό ως Υπεράριθμος

το μήνυμα περιλαμβάνει συννημμένη την αίτηση 

@component('mail::signature', ['email' => Config::get('requests.MAIL_PYSDE')])
| {{Config::get('requests.arxika')}}          |
| -------------------------------- |
| {{Config::get('requests.address')}} |
@endcomponent

@endcomponent

