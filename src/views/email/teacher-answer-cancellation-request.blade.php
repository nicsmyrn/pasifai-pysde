@component('mail::message')

Αγαπητέ/ή {{ $teacher_name }}. Το E-mail περιλαμβάνει συννημμένη απάντηση στο αίτημα 
για ανάκληση της αίτησης σας.

{{ $answer }}


@component('mail::signature', ['email' => Config::get('requests.MAIL_PYSDE')])
| {{Config::get('requests.arxika')}}          |
| -------------------------------- |
| {{Config::get('requests.address')}} |
@endcomponent

@endcomponent

