@component('mail::message')

@component('mail::panel')
Η Σχολική Μονάδα <strong>{{ $school_name }}</strong> άλλαξε την κατάσταση των παρακάτω εκπαιδευτικών:
<ul>
    @foreach($apousies as $k=>$apousia)
        <li>{{ $apousia['myschool']['last_name'] . ' ' . $apousia['myschool']['first_name'] }} |#| {{ config('requests.topothetisis_types')[$apousia['teacher_type']] }} |#| {{ $apousia['date_ends'] }} |#| {{ $apousia['hours'] }}</li>
    @endforeach
</ul>
@endcomponent

@component('mail::button', ['url' => $url, 'color' => 'green'])
    Προβολή Λειτουργικών Κενών
@endcomponent

@component('mail::signature', ['email' => Config::get('requests.MAIL_PYSDE')])
| {{Config::get('requests.arxika')}}          |
| -------------------------------- |
| {{Config::get('requests.address')}} |
@endcomponent

@endcomponent