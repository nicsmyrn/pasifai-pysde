@component('mail::message')

Ο Εκπαιδευτικός {{ $teacher_name }}  άλλαξε τα στοιχεία μοριοδότησής του.


@component('mail::signature', ['email' => Config::get('requests.MAIL_PYSDE')])
| {{Config::get('requests.arxika')}}          |
| -------------------------------- |
| {{Config::get('requests.address')}} |
@endcomponent

@endcomponent

