@component('mail::message')

@component('mail::panel')
Η Σχολική Μονάδα <strong>{{ $school_name }}</strong> έστειλε συννημμένα:
<ul>
    <li>Πίνακα Δηλώσεων Τμημάτων Σχολικής Μονάδας</li>
    <li>Πίνακα Κενών - Πλεονασμάτων</li>
</ul>
@endcomponent

@component('mail::button', ['url' => $url, 'color' => 'green'])
Προβολή Οργανικών Κενών - Πλεονασμάτων
@endcomponent

@component('mail::signature', ['email' => Config::get('requests.MAIL_PYSDE')])
| {{Config::get('requests.arxika')}}          |
| -------------------------------- |
| {{Config::get('requests.address')}} |
@endcomponent

@endcomponent