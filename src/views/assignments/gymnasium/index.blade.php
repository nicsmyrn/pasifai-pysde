@extends('app')

@section('content')
    <table border="1">
        <thead>
            <tr>
                <th style="text-align:center">Μάθημα</th>
                <th style="text-align:center">Α</th>
                <th style="text-align:center">Β</th>
                <th style="text-align:center">Γ</th>
                <th style="text-align:center">Α Ανάθεση</th>
                <th style="text-align:center">Β Ανάθεση</th>

            </tr>
        </thead>
        <tbody>
            @foreach($lessons as $lesson)
                <tr>
                        <td>{{$lesson->name}}</td>                      
                        <td style="width: 20px; text-align:center">
                            {{ $lesson->wrologio->first(function($value, $key){
                                return $value['class'] == 'Α';
                            })['pivot']['hours'] }}
                        </td>
                        <td style="width: 20px; text-align:center">
                            {{ $lesson->wrologio->first(function($value, $key){
                                return $value['class'] == 'Β';
                            })['pivot']['hours'] }}
                        </td>
                        <td style="width: 20px; text-align:center"> 
                            {{ $lesson->wrologio->first(function($value, $key){
                                return $value['class'] == 'Γ';
                            })['pivot']['hours'] }}
                        </td>
                        <td style="width: 180px;">
                            @if(!$lesson->wrologio->where('class', "Α")->isEmpty())
                                <?php 
                                    $a_anathesi = $lesson->wrologio->first(function($value, $key){
                                        return $value['class'] == "Α";
                                    })->pivot->eidikotites->where('pivot.type', "α");
                                ?>
                                    @foreach($a_anathesi as $key=>$a)
                                        @if($key != (count($a_anathesi)-1))
                                            {{ $a['eidikotita_slug'] }},
                                        @else
                                            {{ $a['eidikotita_slug'] }}
                                        @endif
                                    @endforeach
                            @elseif(!$lesson->wrologio->where('class', "Β")->isEmpty())
                                <?php 
                                    $a_anathesi = $lesson->wrologio->first(function($value, $key){
                                        return $value['class'] == "Β";
                                    })->pivot->eidikotites->where('pivot.type', "α");
                                ?>
                                    @foreach($a_anathesi as $key=>$a)
                                        @if($key != (count($a_anathesi)-1))
                                            {{ $a['eidikotita_slug'] }},
                                        @else
                                            {{ $a['eidikotita_slug'] }}
                                        @endif
                                    @endforeach                                    
                            @elseif(!$lesson->wrologio->where('class', "Γ")->isEmpty())
                                <?php 
                                    $a_anathesi = $lesson->wrologio->first(function($value, $key){
                                        return $value['class'] == "Γ";
                                    })->pivot->eidikotites->where('pivot.type', "α");
                                ?>
                                    @foreach($a_anathesi as $key=>$a)
                                        @if($key != (count($a_anathesi)-1))
                                            {{ $a['eidikotita_slug'] }},
                                        @else
                                            {{ $a['eidikotita_slug'] }}
                                        @endif
                                    @endforeach  
                            @endif  
                        </td>
                        <td style="width: 5px;">
                            @if(!$lesson->wrologio->where('class', 'Α')->isEmpty())
                                <?php 
                                    $b_anathesi = $lesson->wrologio->first(function($value, $key){
                                        return $value['class'] == 'Α';
                                    })->pivot->eidikotites->where('pivot.type', 'β');
                                ?>
                                    @foreach($b_anathesi as $b)
                                        {{ $b['eidikotita_slug'] }},
                                    @endforeach
                            @elseif(!$lesson->wrologio->where('class', "Β")->isEmpty())
                                <?php 
                                    $b_anathesi = $lesson->wrologio->first(function($value, $key){
                                        return $value['class'] == "Β";
                                    })->pivot->eidikotites->where('pivot.type', "β");
                                ?>
                                    @foreach($b_anathesi as $b)
                                        {{ $b['eidikotita_slug'] }},
                                    @endforeach                                    
                            @elseif(!$lesson->wrologio->where('class', "Γ")->isEmpty())
                                <?php 
                                    $b_anathesi = $lesson->wrologio->first(function($value, $key){
                                        return $value['class'] == "Γ";
                                    })->pivot->eidikotites->where('pivot.type', "β");
                                ?>
                                    @foreach($b_anathesi as $b)
                                        {{ $b['eidikotita_slug'] }},
                                    @endforeach                                     
                            @endif 
                        </td>

                    
                </tr>
            @endforeach

        </tbody>
    </table>
@stop


