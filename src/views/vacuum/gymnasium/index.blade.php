@extends('app')

@section('header.style')
    <style>
        .loading{
            width: 80px;
            height: 80px;
        }
        .differentNumbers {
            background-color: #ff8566;
        }
        .buttons-margin{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .zero_div{
            color: red;
            text-align: center;
            padding-left: 10px;
        }

        .council-text{
            text-align: center;
            background-color: #787878;
            color: white;
            width: 100%;
        }

        .council-cell{
            text-align: center;
        }
        .pleonasma{
            color : #991f00;
            font-size: 12pt;
            font-weight: 800;
        }
        .elleima{
            color : #008000;
            font-size: 12pt;
            font-weight: 800;
        }
        .different-from-council{
            color: red;
        }

        .is_yperarithmos{
            background-color: #90EE90;
            font-weight: bold;
        }
        .t-center{
            text-align: center;
        }

        .t-title{
            padding-left: 10px;
            padding-right: 10px;
        }
        .t-results{
            text-align: center;
            font-weight: bold;
            font-size : 14pt;
            color: blue;
        }

    </style>
@endsection

@section('content')
    <div id="vacuum">
        <h2 class="page-heading">Κενά - Πλεονάσματα για Μεταθέσεις</h2>

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <label>
                    Σχολείο: 
                </label>
                <select v-cloak class="form-control" v-model="school_selected" @change="getSchoolName(school_selected)">
                    <option v-cloak v-for="school in schools">
                        @{{ school.name }}
                    </option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    
                    <div v-cloak v-if="loading" class="panel-body text-center">
                        <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                        <h5>παρακαλώ περιμένετε...</h5>
                    </div>
                    
                    <div v-cloak v-else class="panel-body">

                        <div class="col-md-7">
                            <div v-cloak v-if="eidikotites.length">
                                <button v-show="displayCouncilButton" class="btn btn-success" @click="saveCouncilChanges">Αποθήκευση αλλαγών Συμβουλίου</button>

                                <physikes-epistimes
                                    :eidikotites="eidikotites"
                                    :school-type="school_type"
                                    @sum-pe04-diff="changeSumDiff"
                                ></physikes-epistimes>

                                <hr>

                                <kena-pleonasmata 
                                    :eidikotites="eidikotites"
                                    :school-type="school_type"
                                    :school-selected="school_selected"
                                    @display-council-button="displayCouncilButton=true"
                                    @notification-message="notifyMessage"
                                ></kena-pleonasmata>

                            </div>
                        </div>

                        <div class="col-md-5">
                            <div v-cloak v-if="divisions!=null">
                                <div v-cloak v-if="school_type === 'gym'"  class="row">
                                    <gymnasium-divisions
                                        :divisions="divisions"
                                        @notification-message="notifyMessage"
                                    >
                                    </gymnasium-divisions>
                                </div>

                                <div v-cloak v-if="school_type === 'lyk'" class="row">
                                    <lyceum-divisions
                                        :divisions="divisions"
                                        :projects="projects"
                                        :list-projects="list_projects"
                                        @notification-message="notifyMessage"
                                    ></lyceum-divisions>
                                </div>

                                <div v-cloak v-if="school_type === 'epal'" class="row">
                                    <epal-divisions
                                        :divisions="divisions"
                                        :projects="projects"
                                        :list-projects="list_projects"
                                        @notification-message="notifyMessage"
                                    ></epal-divisions>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group buttons-margin">
                                            <button v-if="!runningAlgorithm" @click="runAlgorithm" class="btn btn-success">
                                                Εκτέλεση αλγορίθμου
                                            </button>
                                                <button v-else class="btn btn-success" disabled>
                                                <i class="fa fa-spinner fa-spin"></i>
                                                    παρακαλώ περιμένετε ...
                                                </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>  
            </div>



        </div>

        <new-alert 
            ref="alert"
        ></new-alert>
    </div>

@stop

@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
        window.school_name = "{{ $school->exists ? $school->name : 'null' }}";
    </script>

    <script src="{{ mix('vendor/pysde/js/vacuum.js') }}"></script>
@endsection


