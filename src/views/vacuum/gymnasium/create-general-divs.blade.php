@extends('app')

@section('title')
    Καταχώρηση Τμημάτων Γενικής Παιδείας
@endsection

@section('loader')
    @include('vueloader')
@endsection

@section('content')
    <div id="vacuum">

        <general-table
            @display-loader="displayLoader"
            @not-display-loader="notDisplayLoader"
            @notification-message="notifyMessage"
        ></general-table>

        <new-alert 
            ref="alert"
        ></new-alert>
    </div>
@stop


@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
    </script>
    <script src="{{ mix('vendor/pysde/js/vacuumCreateGeneralDivs.js') }}"></script>
@endsection