@extends('app')

@section('header.style')
    <style>
        .loading{
            width: 80px;
            height: 80px;
        }
        .differentNumbers {
            background-color: #ff8566;
        }
        .buttons-margin{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .zero_div{
            color: red;
            text-align: center;
            padding-left: 10px;
        }

        .council-text{
            text-align: center;
            background-color: #787878;
            color: white;
            width: 100%;
        }

        .council-cell{
            text-align: center;
        }
        .pleonasma{
            color : #991f00;
            font-size: 12pt;
            font-weight: 800;
        }
        .elleima{
            color : #008000;
            font-size: 12pt;
            font-weight: 800;
        }
        .different-from-council{
            color: red;
        }

        .is_yperarithmos{
            background-color: #90EE90;
            font-weight: bold;
        }
        .t-center{
            text-align: center;
        }

        .t-title{
            padding-left: 10px;
            padding-right: 10px;
        }
        .t-results{
            text-align: center;
            font-weight: bold;
            font-size : 14pt;
            color: blue;
        }

    </style>
@endsection

@section('content')
    <div id="anatheseis">
        <h2 v-cloak class="page-heading">
            @{{ message }}
        </h2>

        <div class="row">
            <div class="col-md-12">
                <school-type 
                    v-cloak
                    v-for="(division, div_key) in divisions"
                    v-bind:division_type="division"
                    v-bind:type_key="div_key"
                    :key="div_key"
                    :token="token"
                    v-on:notify="notifyMessage"
                />
            </div>
        </div>

        <new-alert 
            ref="alert"
        ></new-alert>

    </div>
@stop

@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
    </script>

    <script src="{{ mix('vendor/pysde/js/anatheseis.js') }}"></script>
@endsection


