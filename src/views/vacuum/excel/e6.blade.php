<table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>A/A</th>
            <th>Ονοματεπώνυμο</th>
            <th>Πατρώνυμο</th>
            <th>Κλάδος</th>
            <th>Τύπος</th>
            <th>Σειρά</th>
            <th>Ειδ. Κατηγορία</th>
            <th>Υποχρ.</th>
            <td>Μόρια</td>
            <th>Εντοπιότητα</th>
            <th>Συνυπηρέτηση</th>
        </tr>
    </thead>

    <tbody>
        <?php $counter = 1; ?>
        @foreach($requests as $request)
            <tr>
                <td>{!! $counter !!}</td>
                <?php $counter = $counter + 1; ?>
                <td>{!! $request->teacher->user->full_name !!}</td>
                <td>{!! $request->teacher->middle_name !!}</td>
                <td>{!! $request->teacher->myschool->new_klados !!}</td>

                <td>{!! $request->type !!} </td>
                
                <td> {!! $request->seira_pinaka !!}</td>
                
                <td>{!! $request->special_situation !!}</td>

                <td>{!! $request->teacher->myschool->ypoxreotiko !!}</td>

                <td>{!! $request->teacher->teacherable->moria !!}</td>
               
                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] !!}</td>
                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] !!}</td>

        @endforeach
    </tbody>
</table>