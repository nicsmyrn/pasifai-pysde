<table>
    <thead>
    <tr>
        <th>ΟΜΑΔΑ</th>
        <th>ΣΧΟΛΕΙΟ</th>
        <th>ΚΛΑΔΟΣ</th>
        <th>ΟΝΟΜΑΤΕΠΩΝΥΜΟ</th>
        <th>ΠΑΡΑΤΗΡΗΣΕΙΣ</th>
        <th>ΣΥΝΟΛΙΚΑ ΜΟΡΙΑ (χωρίς Εντοπιότητα-Συνυπηρέτηση)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($groups as $k=>$group)
        @foreach($group as $key=>$school)
            @foreach($school as $teacher)
                <tr>
                    <td>{{ $k }}</td>
                    <td>{{ $key }}</td>
                    <td>{{ $teacher['eidikotita'] }}</td>
                    <td>{{ $teacher['last_name'] .' '. $teacher['first_name'] }}</td>
                    <td>{{ $teacher['description'] }}</td>
                    <td>{{ $teacher['moria'] }}</td>
                </tr>
            @endforeach
        @endforeach
    @endforeach
    </tbody>
</table>