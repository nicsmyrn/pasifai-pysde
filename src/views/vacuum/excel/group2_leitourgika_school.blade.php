<table>
    <thead>
        <tr>
            <th rowspan="2">ΚΛΑΔΟΣ</th>
            <th rowspan="2">Ειδικότητα</th>
            <th colspan="8">
                ΟΜΑΔΑ 2
            </th>
            <th colspan="4">
                ΟΜΑΔΑ 3
            </th>
            <th colspan="4">
                ΟΜΑΔΑ 4
            </th>
            <th colspan="4">
                ΟΜΑΔΑ 5
            </th>
            <th colspan="2">
                ΟΜΑΔΑ 6
            </th>
        </tr>
        <tr>
            @foreach($header as $h)
                <th>{{ $h->name }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
    @foreach($eidikotites as $e)
        <tr>
            <td>{{ $e->eidikotita_slug }}</td>
            <td>{{ str_limit($e->eidikotita_name,15) }}</td>
            @foreach($header as $school)
                @if($e->klados_slug != 'ΠΕ04')
                    <td>
                        {!! $school->cellLeitourgikaSchool($e->id) !!}
                    </td>
                @else
                    <td></td>
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>