<table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>A/A</th>
            <th>Ονοματεπώνυμο</th>
            <th>ΑΜ</th>
            <th>Ειδικότητα</th>
            <th>Οργανική</th>
            <th>Υπχρ. Ωρ.</th>
            <td>Μόρια Απόσπασης</td>
            <th>Εντοπιότητα</th>
            <th>Σύζυγος Στρατιωτικού</th>
            <th>Συνυπηρέτηση</th>
        </tr>
    </thead>

    <tbody>
        <?php $counter = 1; ?>
        @foreach($requests as $request)
            <tr>
                <td>{!! $counter !!}</td>
                <?php $counter = $counter + 1; ?>
                @if($request->type == "ΕΙΔΙΚΗΣ")
                    <td style="background-color: #F0B27A;">{!! $request->full_name !!}</td>
                @else
                    <td>{!! $request->full_name !!}</td>
                @endif
                <td>{!! $request->am !!}</td>
                <td>{!! $request->klados !!}</td>

                <td>{!! $request->organiki !!}</td>

                <td>{!! $request->ypoxreotiko !!}</td>

                <td>{!! $request->moria_apospasis !!}</td>

                <td>{!! $request->dimos_entopiotitas!!}</td>

                <td>
                    {!! $request->stratiwtikos !!}
                </td>

                <td>{!! $request->dimos_sinipiretisis !!}</td>

        @endforeach
    </tbody>
</table>