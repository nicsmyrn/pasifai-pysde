<table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>A/A</th>
                            <th>Ονοματεπώνυμο</th>
                            <th>Πατρώνυμο</th>
                            <th>Κλάδος</th>
                            <th>Ειδ. Κατηγορία</th>
                            <th>Οργανική</th>
                            <th>Παραμονή</th>
                            <th>Υποχρ.</th>
                            <th>Ώρες</th>
                            <td>Μόρια</td>
                            <th>Εντοπιότητα</th>
                            <th>Συνυπηρέτηση</th>
                            <th>Ομ-1</th>
                            <th>Ομ-2</th>
                            <th>Ομ-3</th>
                            <th>Ομ-4</th>
                            <th>Ομ-5</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $counter = 1; ?>
                        @foreach($requests as $request)
                            <tr>
                                <td>{!! $counter !!}</td>
                                <?php $counter = $counter + 1; ?>
                                <td>{!! $request->teacher->user->full_name !!}</td>
                                <td>{!! $request->teacher->middle_name !!}</td>
                                <td>{!! $request->teacher->myschool->new_klados !!}</td>

                                <td>{!! $request->special_situation !!}</td>

                                <td>{!! $request->school_organiki !!}</td>
                                <td>
                                    @if($request->stay_to_organiki == 1)
                                        ΝΑΙ
                                    @elseif($request->stay_to_organiki == 0)
                                        ΟΧΙ
                                    @else
                                        -
                                    @endif
                                </td>

                                <td>{!! $request->teacher->myschool->ypoxreotiko !!}</td>
                                <td>
                                    @if($request->stay_to_organiki == 1)
                                        {!! $request->hours_for_request !!}
                                    @else
                                        -
                                    @endif
                                </td>

                                <td>{!! $request->teacher->teacherable->moria !!}</td>
                               
                                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] !!}</td>
                                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] !!}</td>

                                <td>
                                    @if(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Χανίων' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Χανίων')
                                        {!! ($request->teacher->teacherable->moria + 8) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] != 'Χανίων' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Χανίων')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Χανίων' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] != 'Χανίων')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @endif
                                </td>
                                <td>
                                    @if(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Πλατανιά' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Πλατανιά')
                                        {!! ($request->teacher->teacherable->moria + 8) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] != 'Πλατανιά' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Πλατανιά')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Πλατανιά' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] != 'Πλατανιά')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @endif
                                </td>

                                <td>
                                    @if(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Κισσάμου' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Κισσάμου')
                                        {!! ($request->teacher->teacherable->moria + 8) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] != 'Κισσάμου' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Κισσάμου')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Κισσάμου' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] != 'Κισσάμου')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @endif
                                </td>

                                <td>
                                    @if(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Αποκορώνου' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Αποκορώνου')
                                        {!! ($request->teacher->teacherable->moria + 8) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] != 'Αποκορώνου' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Αποκορώνου')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Αποκορώνου' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] != 'Αποκορώνου')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @endif
                                </td>

                                <td>
                                    @if(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Καντάνου-Σελίνου' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Καντάνου-Σελίνου')
                                        {!! ($request->teacher->teacherable->moria + 8) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] != 'Καντάνου-Σελίνου' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] == 'Καντάνου-Σελίνου')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @elseif(\Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] == 'Καντάνου-Σελίνου' && \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] != 'Καντάνου-Σελίνου')
                                        {!! ($request->teacher->teacherable->moria + 4) !!}
                                    @endif
                                </td>


                                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] !!}</td>
                                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] !!}</td>

                        @endforeach
                    </tbody>
                </table>