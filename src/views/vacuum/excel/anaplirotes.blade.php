<table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>A/A</th>
            <th>Ονοματεπώνυμο</th>
            <th>Πατρώνυμο</th>
            <th>Ειδικότητα</th>
            <th>Τύπος</th>
            <th>Πίνακας</th>
            <th>Σειρά</th>
            <th>Υπχρ. Ωρ.</th>
        </tr>
    </thead>

    <tbody>
        <?php $counter = 1; ?>
        @foreach($requests as $request)
            <tr>
                <td>{!! $counter !!}</td>
                <?php $counter = $counter + 1; ?>
                <td>{!! $request->teacher->user->full_name !!}</td>
                <td>{!! $request->teacher->middle_name !!}</td>
                <td>{!! $request->teacher->myschool->new_klados !!}</td>
                <td>{!! $request->teacher->myschool->yadescription !!}</td>
                <td>{!! $request->teacher->myschool->type_pinakas !!}</td>
                <td>{!! $request->teacher->myschool->position !!}</td>
                <td>{!! $request->teacher->myschool->ypoxreotiko !!}</td>
        @endforeach
    </tbody>
</table>