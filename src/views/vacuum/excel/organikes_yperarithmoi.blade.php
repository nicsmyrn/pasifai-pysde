<table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>A/A</th>
                            <th>Aρ. Μητρώου</th>
                            <th>Ονοματεπώνυμο</th>
                            <th>Πατρώνυμο</th>
                            <th>Κλάδος</th>
                            <th>Οργανική</th>
                            @if($type == 'Βελτιώση - Οριστική Τοποθέτηση')
                                <th>Ειδ. Κατηγορία</th>
                            @else
                                <th>Τύπος αίτησης</th>
                                <th>Ομάδα Οργ.</th>
                            @endif  
                            <td>Μόρια</td>
                            <th>Εντοπιότητα</th>
                            <th>Συνυπηρέτηση</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $counter = 1; ?>
                        @foreach($requests as $request)
                            <tr>
                                <td>{!! $counter !!}</td>
                                <?php $counter = $counter + 1; ?>
                                <td>{!! $request->teacher->myschool->am !!}</td>
                                <td>{!! $request->teacher->user->full_name !!}</td>
                                <td>{!! $request->teacher->middle_name !!}</td>
                                <td>{!! $request->teacher->myschool->new_klados !!}</td>

                                <td>{!! $request->organiki; !!}</td>

                                @if($type == 'Βελτιώση - Οριστική Τοποθέτηση')
                                    <td>{!! $request->special_situation !!}</td>
                                @else
                                    <td>{!! $request['aitisi_type'] !!}</td>
                                    <td>{!! $request->group_name !!}</td>
                                @endif

                                <td>{!! $request->teacher->teacherable->moria !!}</td>
                                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] !!}</td>
                                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] !!}</td>

                            </tr>

                        @endforeach
                    </tbody>
                </table>