<table>
    <thead>
    <tr>
        <th>ΚΛΑΔΟΣ</th>
        <th>Ειδικότητα</th>
        @foreach($header as $h)
            <th>{{ $h->name }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($eidikotites as $e)
        <tr>
            <td>{{ $e->eidikotita_slug }}</td>
            <td>{{ str_limit($e->eidikotita_name,15) }}</td>
            @foreach($header as $school)
                @if($e->klados_slug != 'ΠΕ04')
                    <td>
                        {!! $school->cellLeitourgika($e->id) !!}
                    </td>
                @else
                    <td></td>
                @endif

            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>