<modal v-cloak
                v-on:cancel-the-request="cancelOperation"
                v-on:emit-the-request="saveTeachers"
                :show="openTeachersModal"
                :loader="hideLoader"
                type="agreement"
                size="large"
                :disabled="!teachersUpdated"
            >
                <h5 slot="title">@{{ eidikotitaName }}</h5>
                <table slot="body" class="table table-bordered">
                    <thead>
                        <th align="center">A/A</th>
                        <th align="center">Επώνυμο</th>
                        <th align="center">Όνομα</th>
                        <th align="center">ΑΜ</th>
                        <th align="center">Υποχρ.</th>
                        <th align="center">Μείωση</th>
                        <th align="center">SUM</th>
                        <th align="center">Κατάσταση</th>
                        <th align="center">Διδ. (ώρες)</th>
                        <th align="center">Διάθεση αλλού</th>
                        <th align="center">Περισσεύει</th>
                        <th align="center">Λήξη</th>
                        <th align="center">Ενέργειες</th>
                    </thead>
                    <tbody>
                        <tr v-for="(teacher, index) in teachers" v-bind:class="{
                            'is_yperarithmos' : teacher.yperarithmos
                        }">
                            <td>@{{ index + 1 }}</td>
                            <td><a href="#">@{{ teacher.last_name }}</a></td>
                            <td>@{{ teacher.first_name }}</td>
                            <td align="center">@{{ teacher.am }}</td>
                            <td align="center">
                                <input @keyup="updateDifferenceHours(teacher)" type="text" v-model="teacher.ypoxreotiko" size="3" maxlength="2" class="council-text"/>                                                                            
                            </td>
                            <td align="center">
                                <input @keyup="updateDifferenceHours(teacher)" type="text" v-model="teacher.meiwsi" size="3" maxlength="2" class="council-text"/>                                                                            
                            </td>
                            <td align="center">
                                @{{ teacher.ypoxreotiko - teacher.meiwsi }}
                            </td>
                            <td align="center">
                                <select @change="updateTeacherType(teacher)" class="teacher-types" v-model="teacher.teacher_type">
                                    <option disabled value="">Επέλεξε:</option>
                                    <option v-bind:value="index" v-for="(type, index) in teacher_types">
                                        @{{ type }}
                                    </option>
                                </select>
                            </td>
                            <td align="center">
                                <span v-show="teacher.belongsToSchool">
                                    <input @keyup="updateDifferenceHours(teacher)" type="text" v-model="teacher.hours" size="3" maxlength="2" class="council-text"/>                                            
                                </span>
                            </td>

                            <td align="center">
                                <span v-if="teacher.sum_other_topothetisis !== 0">
                                    @{{ teacher.sum_other_topothetisis }}
                                </span>
                            </td>

                            <td align="center">
                                <span v-show="teacher.belongsToSchool">
                                    <span v-if="teacher.difference === 0" class="ok">
                                        <i class="fa fa-check"></i>
                                    </span>
                                    <span v-else>
                                        <span class="different-from-council" v-if="teacher.difference > 0">
                                            @{{ teacher.difference }} [πλεόνασμα]
                                        </span>
                                        <span class="ok" v-else>
                                            @{{ -teacher.difference }} [υπερωρία]
                                        </span>
                                    </span>
                                </span>
                            </td>

                            <td>
                                <date-picker v-model="teacher.end_date" 
                                            :clearable="datePickerClass.getClearable()" 
                                            :width="datePickerClass.getWidth()" 
                                            value-type="format" 
                                            :format="datePickerClass.getFormat()" 
                                            :lang="datePickerClass.getLang()" 
                                            :shortcuts="datePickerClass.getShortcuts()" 
                                            :first-day-of-week="datePickerClass.firstDayOfWeek()"
                                            @change="changeDate(teacher)"
                                ></date-picker>                                       
                            </td>
                            <td>
                                <span v-if="teacher.locked">
                                    <i @click="unlockTeacher(teacher)" class="fa fa-lock lock-button"></i>
                                </span>
                                <span v-else>
                                    <i @click="lockTeacher(teacher)" class="fa fa-unlock unlock-button"></i>
                                </span>
                                |<i class="fa fa-trash delete-button"></i>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div slot="button">Αποθήκευση Εκπαιδευτικών</div>
</modal>