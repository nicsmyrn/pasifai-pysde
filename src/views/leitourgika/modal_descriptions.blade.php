<modal v-cloak
        v-on:cancel-the-request="close"
        v-on:emit-the-request="close"
        :show="openDescriptionModal"
        type="agreement"
        size="large"
    >
        <h5 slot="title">@{{ tempDescriptionName }}</h5>
        <p slot="body">
            @{{ tempDescription }}
        </p>
        <div slot="button">...</div>
</modal>