<h1 class="page-heading">
        {!! $message !!}
    </h1>

    @if($teachers->isEmpty())
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει κανένας καθηγητής</div>
                </div>
            </div>
        </div>
    @else
        <table id="teachers" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr class="active">
                    <th class="text-center">Ονοματεπώνυμο</th>
                    <th class="text-center">Πατρώνυμο</th>
                    <th class="text-center">Οργανική</th>
                    <th class="text-center">Κλάδος</th>
                    <th class="text-center">Τοποθετήρια</th>
                </tr>
            </thead>

            <tbody>
                @foreach($teachers as $myschool)
                    <tr>
                        <td>
                            @if(Auth::user()->isRole('oikonomika'))
                                <a href="{!! route('Oikonomikou::Placements::placementsDetails', [$myschool->afm]) !!}"
                                    class="btn btn-default btn-sm"
                                    data-toggle="tooltip"
                                    title="Λεπτομέρειες τοποθετήσεων..."
                                >
                            @else
                                <a href="{!! route('Leitourgika::Placements::placementsDetails', [$myschool->afm]) !!}"
                                    title="Λεπτομέρειες τοποθετήσεων..."
                                >
                            @endif
                                {!! $myschool->full_name !!}
                            </a>
                        </td>
                        <td>{!! $myschool->middle_name !!}</td>
                        <td>{!! $myschool->organiki_name !!}</td>
                        <td class="text-center">{!! $myschool->eidikotita !!}</td>
                        <td class="text-center">
                            @if(Auth::user()->isRole('oikonomika'))
                                <a href="{!! route('Oikonomikou::Placements::placementsDetails', [$myschool->afm]) !!}"
                                    class="btn btn-default btn-sm"
                                    data-toggle="tooltip"
                                    title="Λεπτομέρειες τοποθετήσεων..."
                                >
                            @else
                                <a href="{!! route('Leitourgika::Placements::placementsDetails', [$myschool->afm]) !!}"
                                    class="btn btn-default btn-sm"
                                    data-toggle="tooltip"
                                    title="Λεπτομέρειες τοποθετήσεων..."
                                >
                            @endif
                                <i class="fa fa-info" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif