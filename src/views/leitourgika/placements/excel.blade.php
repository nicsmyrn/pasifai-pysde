@extends('app')

@section('header.style')
    <style>
        .loading{
            width: 80px;
            height: 80px;
        }
    </style>
@endsection

@section('loader')
    @include('vueloader')
@endsection

@section('content')
    <div id="excel_placements">
        <h2 class="page-heading">Δημιουργία Excel Τοποθετήσεων</h2>

        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Προσοχή!</strong> Ο αριθμός της πράξης πρέπει να έχει καταχωρηθεί στο σύστημα.
                </div>
            </div>
            <div class="col-md-12">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="praxi">Δώσε τον αριθμό της Πράξης τους ΠΥΣΔΕ:</label>
                        <input v-model="praxi_number" type="text" class="form-control" id="praxi" placeholder="αρ. πράξης...">
                    </div>
                    <div class="form-group">
                        <button @click.prevent="createExcel" type="submit" class="btn btn-default">Δημιουργία EXCEL</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
@stop

@section('scripts.footer')    
    <script>
        window.api_token = "{{ Auth::user()->api_token }}";
    </script>
    <script src="{{ mix('vendor/pysde/js/placementsExcel.js') }}"></script>
@endsection