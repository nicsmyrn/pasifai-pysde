
    <div class="form-group">
        {!! Form::label('dde_protocol', 'Αριθμός Πρωτοκόλλου:', ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-3">
            {!! Form::text('dde_protocol',null, ['class'=>'form-control', 'id'=>'dde_protocol']) !!}
        </div>
    </div>

    <div class="form-group">
            {!! Form::label('dde_protocol_date', 'Ημερομηνία:', ['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('dde_protocol_date',$praxi->dde_protocol_date, ['class'=>'form-control', 'id'=>'dde_protocol_date']) !!}
            </div>
        </div>



    <div class="col-md-6 col-md-offset-3 text-center">
        <div class="btn-group">
            {!! Form::submit($submitButton, ['class'=>'btn btn-warning btn-lg']) !!}
        </div>
    </div>
