@extends('app')

@section('title')
    Επεξεργασία
@endsection

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <style>
        #hours{
            background-color: #ffbf00;
            text-align: center;
            font-weight: bold;
            color: #000000;
        }
    </style>
@endsection

@section('content')

        <h4 class="page-heading">
            Επεξεργασία τοποθέτησης
        </h4>

        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <a href="{!! URL::previous() !!}" class="btn btn-primary brn-lg">Επιστροφή...</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <strong style="color: #0000ff">
                                        {!! $teacher->full_name !!} του {!! $teacher->middle_name !!}
                                    </strong>
                                </h3>
                            </div>
                            <div class="panel-body">
                                {!! Form::model($placement, ['method'=> 'PATCH', 'route' => ['Leitourgika::Placements::store', $teacher->afm, $placement->id]]) !!}
                                    @include('pysde::leitourgika.placements.teacherPlacements._form',  ['submitButton' => 'Αποθήκευση Αλλαγών', 'creating'=>false])
                                {!! Form::close() !!}
                            </div>
                        </div>
            </div>
        </div>

@endsection

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap-datepicker.gr.js" charset="UTF-8"></script>
<script>
        $('.school_select').select2({
            placeholder: "Επιλογή Σχολείου",
            theme: "classic",
            language: {
                noResults: function() {
                    return "Δεν υπάρχει αποτέλεσμα αναζήτησης";
                }
            }
        });

        $('#date_from').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            daysOfWeekDisabled: [0,6],
            language: 'gr'
        });

        $('#date_to').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            daysOfWeekDisabled: [0,6],
            language: 'gr'
        });
</script>
@endsection