    <table id="header">
        <tr>
            <td class="logo_cell_name">
                <img src="img/ethnosimo.jpg" id="logo">
                <div>ΕΛΛΗΝΙΚΗ ΔΗΜΟΚΡΑΤΙΑ</div>
                <div>ΥΠΟΥΡΓΕΙΟ ΠΑΙΔΕΙΑΣ, ΕΡΕΥΝΑΣ ΚΑΙ ΘΡΗΣΚΕΥΜΑΤΩΝ</div>
                <div class="div_perifiereia">ΠΕΡ. Δ/ΝΣΗ Α/ΘΜΙΑΣ & Β/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΚΡΗΤΗΣ</div>
                <div>Δ/ΝΣΗ Β/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΧΑΝΙΩΝ</div>

                @if(intval(\Carbon\Carbon::parse(\Config::get('requests.date_of_new_placements'))->format('Ymd')) > intval(\Carbon\Carbon::parse($praxi->decision_date)->format('Ymd')))
                    <div>ΤΜΗΜΑ ΔΙΟΙΚΗΤΙΚΩΝ ΘΕΜΑΤΩΝ</div>
                @else
                    <div>ΤΜΗΜΑ Γ΄ ΠΡΟΣΩΠΙΚΟΥ</div>
                @endif

                <hr class="line">
                <div>Ταχ. Διεύθυνση: Γκερόλα 48 β</div>
                <div>Πληροφορίες: {{ config('requests.information_name') }}</div>
                <div>Τηλέφωνο: 2821047152 Fax: 2821047137</div>
                <div>E-mail: mail&#64;dide.chan.sch.gr</div>
            </td>
            <td id="protocol">
                <div class="protocol_position">ΧΑΝΙΑ {!! $praxi->dde_protocol_date !!}</div>
                <div class="protocol_position">ΑΡΙΘΜ ΠΡΩΤ: Φ.10.3/{!! $praxi->dde_protocol !!} </div>
                <br>

                <table>
                    <tr>
                        <td style="vertical-align:top">Προς:</td>
                        <td>
                            <span class="span_bold">
                                {!! $myschool->full_name !!}
                            </span>
                            του
                            <span class="span_bold">
                                {!!  $myschool->middle_name !!}
                            </span>

                            εκπαιδευτικό κλ.
                            @if(\Carbon\Carbon::parse($praxi->decision_date)->lessThan(\Carbon\Carbon::parse(\Config::get('requests.date_of_new_placements'))))
                                {!! $myschool->eidikotita . '('. $myschool->klados . ')' !!}
                            @else
                                {!! $myschool->new_klados . '('. $myschool->new_eidikotita_name . ')' !!}
                            @endif

                            με οργανική στο - <span class="span_bold">{{ $myschool->organiki_name }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td>Κοιν.:</td>
                        <td>
                            @if($base_placement != null)
                                <ul>
                                    <li>{{ $base_placement->to }}</li>
                                </ul>
                            @endif
                            <ul>
                                @foreach($new_placements as $placement)
                                    <li> {!! $placement->to !!} </li>
                                @endforeach
                            </ul>
                            <ul>
                                @foreach($old_placements as $placement)
                                    <li> {!! $placement->to !!} </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

    <!-- TODO: ###### subject ####### -->
    <div id="subject">
            Θέμα: &laquo;Ανακοίνωση {!! $praxi->title !!}&raquo;
    </div>

    <div id="apofasis">
        <div class="line_apofasi">
            <p>Σας ανακοινώνουμε ότι με την υπ' αριθμ. Φ.10.3/{!! $praxi->dde_protocol !!}/{!! $praxi->dde_protocol_date !!}
            απόφαση του Δ/ντή της Δ/θμιας Εκπ/σης Χανίων<a target="_blank" class="url_links" href="{!! $praxi->url !!}"><span class="span_bold">{!! $praxi->ada !!}</span></a>,
            σύμφωνα με την υπ' αριθμ. <span class="span_bold">{!! $praxi->decision_number !!}/{!! $praxi->decision_date !!}</span>
            Πράξη του ΠΥΣΔΕ Χανίων και με βάση το υποχρεωτικό σας ωράριο ({{ $praxi->calculated_ypoxreotiko }} ώρες) 
            @if($base_placement != null)
                @if($praxi->decision_number == $base_placement->praxi->decision_number)
                    <p id="title">
                        τοποθετηθήκατε προσωρινά
                    </p>
                    {!! $praxi->base_aitisis['text']  !!} σας στο: </p>
                    <p>
                        <span class="span_bold">
                            {!! $base_placement->to !!}
                        </span>

                        @if($base_placement->days >=1 && $base_placement->days < 5)
                            για
                            <span class="span_bold">
                                {!! $base_placement->hours !!} ώρες
                            </span>
                                / {!! $base_placement->days !!}
                                @if($base_placement->days == 1)
                                    ημέρα
                                @else
                                    ημέρες
                                @endif
                        @elseif($base_placement->days == 5)
                            εξ ολοκλήρου
                            <?php
                                $ex_oloklirou = true;
                            ?>
                        @endif
                        @if($base_placement->description != '')
                            [{!! $base_placement->description !!}]
                        @endif
                    </p>
                @else
                    @if(!str_contains('Οργανικά',$myschool->topothetisi))
                        <br>
                        με προσωρινή τοποθέτηση στο
                        <span class="span_bold">
                            {!! $base_placement->to !!}
                        </span>
                        {{--(Πράξη Δ/ντή ΔΔΕ Χανίων {!! $base_placement->praxi->decision_number .'/'.$base_placement->praxi->decision_date !!})--}}
                        <br>
                    @endif
                @endif
            @else
                {{--- με οργανική στο--}}
                {{--<span class="span_bold">--}}
                    {{--{!! $myschool->organiki_name !!}--}}
                {{--</span> ---}}
            @endif

            @if(!$new_placements->isEmpty())
                @if(!$new_placements->where('placements_type', 2)->isEmpty())
                    <p id="title">
                        αποσπαστήκατε
                    </p>
                    @if($praxi->aitisis['value'] != 'true&false')
                        {!! $praxi->aitisis['text'] !!} σας
                    @endif
                    στο: </p>

                                            <ol>
                                                @foreach($new_placements as $new_placement)
                                                    <li>
                                                        <span class="span_bold">
                                                            {!! $new_placement->to !!}
                                                        </span>
                                                        για
                                                        <span class="span_bold">
                                                            {!! $new_placement->hours !!} ώρες
                                                        </span>
                                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                                / {!! $new_placement->days !!}
                                                                @if($new_placement->days == 1)
                                                                    ημέρα
                                                                @else
                                                                    ημέρες
                                                                @endif
                                                        @endif
                                                        @if($praxi->aitisis['value'] == 'true&false')
                                                            @if($new_placement->me_aitisi)
                                                                με αίτηση
                                                            @else
                                                                χωρίς αίτηση
                                                            @endif
                                                        @endif
                                                        @if($new_placement->description != '')
                                                            [{!! $new_placement->description !!}]
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ol>

                @endif
                @if(!$new_placements->where('placements_type', 7)->isEmpty())
                    <p id="title">
                        αποσπαστήκατε
                    </p>
                    @if($praxi->aitisis['value'] != 'true&false')
                        {!! $praxi->aitisis['text'] !!} σας
                    @endif
                    στο: </p>

                                            <ol>
                                                @foreach($new_placements->where('placements_type', 7) as $new_placement)
                                                    <li>
                                                        <span class="span_bold">
                                                            {!! $new_placement->to !!}
                                                        </span>
                                                        για
                                                        <span class="span_bold">
                                                            {!! $new_placement->hours !!} ώρες
                                                        </span>
                                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                                / {!! $new_placement->days !!}
                                                                @if($new_placement->days == 1)
                                                                    ημέρα
                                                                @else
                                                                    ημέρες
                                                                @endif
                                                        @endif
                                                        @if($praxi->aitisis['value'] == 'true&false')
                                                            @if($new_placement->me_aitisi)
                                                                με αίτηση
                                                            @else
                                                                χωρίς αίτηση
                                                            @endif
                                                        @endif
                                                        @if($new_placement->description != '')
                                                            [{!! $new_placement->description !!}]
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ol>
                @endif
                @if(!$new_placements->where('placements_type', 9)->isEmpty())
                    <p id="title">
                        ανακαλείται
                    </p>
                    η τοποθέτησή σας στο: </p>
                        <ol>
                            @foreach($new_placements->where('placements_type', 9) as $new_placement)
                                <li>
                                    <span class="span_bold">
                                        {!! $new_placement->to !!}
                                    </span>
                                    για
                                    <span class="span_bold">
                                        {!! $new_placement->hours !!} ώρες
                                    </span>
                                    @if($new_placement->days >=1 && $new_placement->days <= 5)
                                            / {!! $new_placement->days !!}
                                            @if($new_placement->days == 1)
                                                ημέρα
                                            @else
                                                ημέρες
                                            @endif
                                    @endif
                                    @if($praxi->aitisis['value'] == 'true&false')
                                        @if($new_placement->me_aitisi)
                                            με αίτηση
                                        @else
                                            χωρίς αίτηση
                                        @endif
                                    @endif
                                    @if($new_placement->description != '')
                                        [{!! $new_placement->description !!}]
                                    @endif
                                </li>
                            @endforeach
                        </ol>
                @endif
                @if(!$new_placements->where('placements_type', 3)->isEmpty() || !$new_placements->where('placements_type', 5)->isEmpty() || !$new_placements->where('placements_type', 8)->isEmpty())
                    <p id="title">
                        διατεθήκατε
                    </p>

                    @if($praxi->aitisis['value'] != 'true&false')
                        {!! $praxi->aitisis['text'] !!} σας
                    @endif
                    @if(!$new_placements->where('placements_type', 8)->isEmpty())
                        στην Π/θμια Εκπαίδευση για
                        <span class="span_bold">
                            {!! $new_placements->first()->hours !!} ώρες
                        </span>
                        @if($new_placements->first()->days >=1 && $new_placements->first()->days <= 5)
                                / {!! $new_placements->first()->days !!}
                                @if($new_placements->first()->days == 1)
                                    ημέρα
                                @else
                                    ημέρες
                                @endif
                        @endif

                    @else
                        για συμπλήρωση του υποχρεωτικού ωραρίου σας στο: </p>
                        <p>
                            <ol>
                                @foreach($new_placements->where('placements_type', 3) as $new_placement)
                                    <li>
                                        <span class="span_bold">
                                            {!! $new_placement->to !!}
                                        </span>
                                        για
                                        <span class="span_bold">
                                            {!! $new_placement->hours !!} ώρες
                                        </span>
                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                / {!! $new_placement->days !!}
                                                @if($new_placement->days == 1)
                                                    ημέρα
                                                @else
                                                    ημέρες
                                                @endif
                                        @endif
                                        @if($praxi->aitisis['value'] == 'true&false')
                                            @if($new_placement->me_aitisi)
                                                με αίτηση
                                            @else
                                                χωρίς αίτηση
                                            @endif
                                        @endif
                                        @if($new_placement->description != '')
                                            [{!! $new_placement->description !!}]
                                        @endif
                                    </li>
                                @endforeach
                            </ol>
                            <ol>
                                @foreach($new_placements->where('placements_type', 5) as $new_placement)
                                    <li>
                                        <span class="span_bold">
                                            {!! $new_placement->to !!}
                                        </span>
                                        για
                                        <span class="span_bold">
                                            {!! $new_placement->hours !!} ώρες
                                        </span>
                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                / {!! $new_placement->days !!}
                                                @if($new_placement->days == 1)
                                                    ημέρα
                                                @else
                                                    ημέρες
                                                @endif
                                        @endif
                                        @if($praxi->aitisis['value'] == 'true&false')
                                            @if($new_placement->me_aitisi)
                                                με αίτηση
                                            @else
                                                χωρίς αίτηση
                                            @endif
                                        @endif
                                        @if($new_placement->description != '')
                                            [{!! $new_placement->description !!}]
                                        @endif
                                    </li>
                                @endforeach
                            </ol>
                        </p>

                    @endif

                @endif

            @endif
        </div>

        @if($ex_oloklirou)
                <div class="line_apofasi">
                    @if($new_placements->count() > 0)
                        από {!! $new_placements->first()->starts_at !!} μέχρι τις {!! $new_placements->first()->ends_at !!}.
                    @elseif($base_placement->count() > 0)
                        από {!! $base_placement->starts_at !!} μέχρι τις {!! $base_placement->ends_at !!}.
                    @endif
                </div>
        @else
            <div class="line_apofasi">
                @if($new_placements->count() > 0)
                    από {!! $new_placements->first()->starts_at !!} μέχρι τις {!! $new_placements->first()->ends_at !!}.
                @elseif($base_placement->count() > 0)
                    από {!! $base_placement->starts_at !!} μέχρι τις {!! $base_placement->ends_at !!}.
                @endif
            </div>
        @endif

        @if($base_placement == null && $old_placements->isEmpty())
            <div class="line_apofasi">
                - Σχολείο Οργανικής:  <span class="span_bold">{{ $myschool->organiki_name }}</span> για @if(config('requests.manual_organiki_hours')) {{ config('requests.organiki_hours') }} @else {{ ($myschool->ypoxreotiko - $sum_hours_of_placements) }} @endif Ωρ @if($sum_days_of_placements > 0) /{{$sum_days_of_placements}}Ημ @endif
            </div>
        @endif

        @if(config('requests.display_old_placements'))
            @if(!$old_placements->isEmpty())
                <table id="old_placements">
                    <thead>
                        <tr>
                            <th class="header" colspan="6">
                                Τοποθετήσεις στην Τρέχουσα Σχολική Χρονιά
                            </th>
                        </tr>
                        <tr>
                            <th class="header">Πράξη</th>
                            <th class="header">Σχολική Μονάδα</th>
                            <th class="header">Ώρες/Ημ</th>
                            <th class="header">Τοποθέτηση</th>
                            <th class="header">Με αίτηση</th>
                            <th class="header">Παρατηρήσεις</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if($base_placement != null)
                            @if($current_praxi_decision_number != $base_placement->praxi->decision_number)
                                <tr>
                                    <td class="content_centered"><a href="{{ $base_placement->praxi->ada }}">{{ $base_placement->praxi->decision_number . '/' . $base_placement->praxi->decision_date }}</a></td>
                                    <td class="content_left">{{ $base_placement->to }}</td>
                                    <td class="content_centered">{!! $base_placement->hours !!}Ωρ @if($base_placement->days > 0) /{{$base_placement->days}}Ημ @endif</td>
                                    <td class="content_centered">{{ config('requests.placements_type')[$base_placement->placements_type] }}</td>
                                    <td class="content_centered">@if($base_placement->me_aitisi)&#10003;@else&#10007;@endif</td>
                                    <td class="content_centered">από {{$base_placement->praxi->dde_protocol_date . '. '. $base_placement->description }}</td>
                                </tr>
                            @endif
                        @else
                            <tr>
                                <td class="content_centered"><a href="#"></a></td>
                                <td class="content_left">{{ $myschool->organiki_name }}</td>
                                <td class="content_centered">{{ ($myschool->ypoxreotiko - $sum_hours_of_placements) }}Ωρ @if($sum_days_of_placements > 0) /{{$sum_days_of_placements}}Ημ @endif</td>
                                <td class="content_centered">Οργανικά</td>
                                <td class="content_centered"></td>
                                <td class="content_centered"></td>
                            </tr>                        
                        @endif

                        @foreach($old_placements as $old_placement)
                            <tr>
                                <td class="content_centered"><a href="{{ $old_placement->praxi->ada }}">{{ $old_placement->praxi->decision_number. '/' .$old_placement->praxi->decision_date }}</a></td>
                                <td class="content_left">{{ $old_placement->to }}</td>
                                <td class="content_centered">{!! $old_placement->hours !!}Ωρ@if($old_placement->days >=1 && $old_placement->days <= 5)/{!! $old_placement->days !!} @if($old_placement->days == 1)Ημ @else Ημ @endif @endif</td>
                                <td class="content_centered">{{ config('requests.placements_type')[$old_placement->placements_type] }}</td>
                                <td class="content_centered">@if($old_placement->me_aitisi)&#10003;@else &#10007;@endif</td>
                                <td class="content_centered">από {{ $old_placement->praxi->dde_protocol_date . '. '. $old_placement->description }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
        @endif

        @if($new_placements->count() == 1 &&  in_array($new_placements->first()->placements_type, [9]))

        @else
            <div class="line_apofasi">
                <br>
                Κατόπιν αυτού παρακαλούμε να παρουσιαστείτε στο Σχολείο
                @if(!$new_placements->isEmpty())
                    @if(in_array($new_placements->first()->placements_type, [2,7]))
                        της απόσπασή σας,
                    @else
                        @if($base_placement != null)
                            @if($praxi->decision_number == $base_placement->praxi->decision_number)
                                της τοποθέτησή σας,
                            @endif
                        @else
                            της διάθεσής σας
                        @endif
                    @endif
                @else
                    @if($base_placement != null)
                        @if($praxi->decision_number == $base_placement->praxi->decision_number)
                            της τοποθέτησή σας,
                        @endif
                    @else
                        της διάθεσής σας
                    @endif
                @endif

                για να αναλάβετε υπηρεσία. Το Σχολείο

                @if(!$new_placements->isEmpty())
                    @if(in_array($new_placements->first()->placements_type, [2,7]))
                        απόσπασης
                    @else
                        @if($base_placement != null)
                            @if($praxi->decision_number == $base_placement->praxi->decision_number)
                                τοποθέτησης
                            @endif
                        @else
                            Διάθεση
                        @endif
                    @endif
                @else
                        @if($base_placement != null)
                            @if($praxi->decision_number == $base_placement->praxi->decision_number)
                                τοποθέτησης
                            @endif
                        @else
                            Διάθεση
                        @endif
                @endif

                παρακαλείται να μας αποστείλει άμεσα την πράξη ανάληψή σας.
            </div>
        @endif

    </div>

    <table id="footer">
        <tr>
            <td id="left_footer">

            </td>
            <td id="center_footer"></td>
            <td id="right_footer">
                <div>Ο Διευθυντής της Δ.Δ.Ε. Χανίων</div>
                <div>
                    <img src="img/$2y$10$M7lTqmCckaPkU0uEd87RpulMkrDY95L2V8onvVodtDfxtQr24vUCG.jpg" height="113px" width="184px">
                 </div>
                <div>{{ config('requests.administrator_name') }}</div>
                <div>{{ config('requests.administrator_job_title') }}</div>
            </td>
        </tr>
    </table>

