<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            /*font-family: "DejaVu Serif";*/
            margin: 0 15 0 15;
            font-size: 9pt;
            text-align: justify;
        }

        table{
            width: 100%;
            margin-top: 0px;
            margin-bottom: 0px;
            border-collapse: collapse;
        }

        #old_placements{
            width: 100%;
        }
        /* #old_placements th, td{
        } */

        #old_placements .header{
            text-align: center;
            font-size: 9pt;
            font-weight:normal;
            border: 1px solid black;
        }

        #old_placements .content_left{
            font-size : 9pt;
            text-align:left;
            border: 1px solid black;
            padding-left: 5px;
        }

        #old_placements .content_centered{
            font-size : 9pt;
            text-align:center;
            border: 1px solid black;
        }


        #signature{
            background-image: url("http://srv-dide.chan.sch.gr/hidden/$2y$10$M7lTqmCckaPkU0uEd87RpulMkrDY95L2V8onvVodtDfxtQr24vUCG.jpg");
        }

        #logo{
            height: 40px;
            width:40px;
        }
        .logo_cell_name{
            text-align: center;
            width: 300px;
        }
        .line{
            width: 200px;
        }

        #subject{
            font-size: 11pt;
            margin-top: 10px;
            margin-bottom: 0px;
            margin-left: 20px;
            font-weight: bold;
        }

        .nomoi{
            font-size: 9pt;
            /* color: red; */
        }

        #footer{
            /* position: absolute; */
            /* bottom: 120; */
            font-size: 10pt;
        }
        #footer #left_footer{
            width: 30%;
        }
        #footer #right_footer{
            text-align: center;
            width: 40%
        }
        #protocol{
            padding-left: 70px;
        }

        #title{
            text-align: center;
            font-size: 11pt;
            letter-spacing: 4px;
            font-weight: bold;
        }
        #title div{
            text-decoration: none;
            border: 3px solid #000000;
            width: 170px;
            padding: 6px;
        }
        #apofasis{
            font-size: 11pt;
            margin-bottom: 5px;
        }

        #wrapper{
            padding-bottom: 6px;
            /* margin-bottom: 150px; */
        }

        #apofasis #apofasi{
            text-align: center;
            font-size: 14;
            font-weight: bold;
            letter-spacing: 5px;
            /* margin-bottom: 20px; */
        }
        .span_bold{
            font-weight: bold;
            font-size: 10pt;
        }

        .schools{
            margin-right: 100px
        }

        .line_apofasi{
            padding: 0 0 6px 0;
            margin-right: 10px;
            font-size: 10pt;
        }

        .from_line_apofasi{
            padding: 0 0 6px 0;
            margin-right: 10px;
            font-size: 10pt;
            /* margin-bottom: 110px; */
            /* background-color: red;  */
        }

        .end_of_apofasi{
            font-size: 10pt;
            padding: 2px 0 5px 0;
        }

        .school_name{
            margin: 0 0 5px 0;;
        }

        .page-break {
            page-break-after: always;
        }

        #div_table_destinations{
            display: table;
            /* width: 100%; */
            max-width: 100%;
        }

        .div_row{
            display: table-row;
            padding-bottom: 10px;
        }

        .div_table_cell_title{
            font-weight: bold;
            display: table-cell;
            /* background-color: red; */
        }

        .div_table_cell{
            /* background-color: yellow; */
            display: table-cell;
        }

        .url_links{
            text-decoration: none;
            color: red;
        }

        .print_date{
            position: absolute;
            top:0;
            right: 0;
            font-size: 8pt;
        }

        .protocol_position{
            text-align: center;
        }
        .div_perifiereia{
            font-size: 8pt;
        }
    </style>
</head>