<table>
        <tr>
            <td class="logo_cell_name">
                <img src="img/ethnosimo.jpg" id="logo">
                <div>ΕΛΛΗΝΙΚΗ ΔΗΜΟΚΡΑΤΙΑ</div>
                <div>ΥΠΟΥΡΓΕΙΟ ΠΑΙΔΕΙΑΣ, ΕΡΕΥΝΑΣ ΚΑΙ ΘΡΗΣΚΕΥΜΑΤΩΝ</div>
                <div class="div_perifiereia">ΠΕΡ. Δ/ΝΣΗ Α/ΘΜΙΑΣ & Β/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΚΡΗΤΗΣ</div>
                <div>Δ/ΝΣΗ Β/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΧΑΝΙΩΝ</div>

                @if(intval(\Carbon\Carbon::parse(\Config::get('requests.date_of_new_placements'))->format('Ymd')) > intval(\Carbon\Carbon::parse($praxi->decision_date)->format('Ymd')))
                    <div>ΤΜΗΜΑ ΔΙΟΙΚΗΤΙΚΩΝ ΘΕΜΑΤΩΝ</div>
                @else
                    <div>ΤΜΗΜΑ Γ΄ ΠΡΟΣΩΠΙΚΟΥ</div>
                @endif

                <hr class="line">
                <div>Ταχ. Διεύθυνση: Γκερόλα 48 β</div>
                <div>Πληροφορίες: {{ config('requests.information_name') }}</div>
                <div>Τηλέφωνο: 2821047152 Fax: 2821047137</div>
                <div>E-mail: mail&#64;dide.chan.sch.gr</div>
            </td>
            <td id="protocol">
                <div class="protocol_position">ΧΑΝΙΑ {!! $praxi->dde_protocol_date !!}</div>
                <div class="protocol_position">ΑΡΙΘΜ ΠΡΩΤ: Φ.10.3/{!! $praxi->dde_protocol !!} </div>
                <br>
                <div id="div_table_destinations">
                    <div class="div_row">
                        <div class="div_table_cell_title">
                            Προς:
                        </div>
                        <div class="div_table_cell">
                            <span class="span_bold">
                                {!! $myschool->full_name !!}
                             </span>
                             του
                             <span class="span_bold">
                                {!!  $myschool->middle_name !!}
                                @if(intval(\Carbon\Carbon::parse(\Config::get('requests.date_of_new_placements'))->format('Ymd')) > intval(\Carbon\Carbon::parse($praxi->decision_date)->format('Ymd')))
                                     </span> εκπαιδευτικό κλ. {!! $myschool->eidikotita . '('. $myschool->klados . ')' !!}
                                @else
                                     </span> εκπαιδευτικό κλ. {!! $myschool->new_klados . '('. $myschool->new_eidikotita_name . ')' !!}
                                @endif
                             @if($myschool->organiki_name == 'ΑΝΑΠΛΗΡΩΤΗΣ')
                                - <span class="span_bold">
                                    {!! $myschool->organiki_name !!}
                                </span>
                             @elseif($myschool->organiki_name == 'Διάθεση ΠΥΣΔΕ')
                                - <span class="span_bold">
                                    {!! $myschool->organiki_name !!}
                                </span>
                             @elseif(str_contains($myschool->organiki_name, 'Απόσπαση από'))
                                - <span class="span_bold">
                                     {!! $myschool->organiki_name !!}
                                </span>
                             @else
                                 με οργανική στο
                                 <span class="span_bold">
                                     {!! $myschool->organiki_name !!}
                                 </span>
                             @endif
                        </div>
                    </div>
                    <div class="div_row">
                        <div class="div_table_cell_title">
                            Κοιν.:
                        </div>
                        <div class="div_table_cell">
                            <?php $counter = 1; ?>
                            @if($myschool->dieuthinsi != Config::get('requests.my_dieuthinsi'))
                                <div> {!! $counter . '. ' . $myschool->dieuthinsi_title !!}</div>
                                <?php $counter = $counter + 1; ?>
                            @else
                                @if(str_contains('Οργανικά', $myschool->topothetisi))
                                <div> {!! $counter . '. ' . $myschool->organiki_name !!}</div>
                                <?php $counter = $counter + 1; ?>
                                @endif
                            @endif


                            @if($base_placement != null)
                                <div> {!! $counter . '. ' . $base_placement->to !!}</div>
                                <?php $counter = $counter + 1; ?>
                            @endif
                            @foreach($new_placements as $placement)
                                <div> {!! $counter . '. ' .  $placement->to !!}</div>
                                <?php $counter = $counter + 1; ?>
                            @endforeach
                            @foreach($old_placements as $placement)
                                <div> {!! $counter . '. ' .  $placement->to !!}</div>
                                <?php $counter = $counter + 1; ?>
                            @endforeach
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <!-- TODO: ###### subject ####### -->
    <div id="subject">
            Θέμα: &laquo;Ανακοίνωση {!! $praxi->title !!}&raquo;
    </div>

    <div id="apofasis">
        <div class="line_apofasi">
            <p>Σας ανακοινώνουμε ότι με την υπ' αριθμ. Φ.10.3/{!! $praxi->dde_protocol !!}/{!! $praxi->dde_protocol_date !!}
            απόφαση του Δ/ντή της Δ/θμιας Εκπ/σης Χανίων <a target="_blank" class="url_links" href="{!! $praxi->url !!}"><span class="span_bold">{!! $praxi->ada !!}</span></a> και
            σύμφωνα με την υπ' αριθμ. <span class="span_bold">{!! $praxi->decision_number !!}/{!! $praxi->decision_date !!}</span>
            Πράξη του ΠΥΣΔΕ Χανίων,
            @if($base_placement != null)
                @if($praxi->decision_number == $base_placement->praxi->decision_number)
                    <p id="title">
                        τοποθετηθήκατε προσωρινά
                    </p>
                    {!! $praxi->base_aitisis['text']  !!} σας στο: </p>
                    <p>
                        <span class="span_bold">
                            {!! $base_placement->to !!}
                        </span>

                        @if($base_placement->days >=1 && $base_placement->days < 5)
                            για
                            <span class="span_bold">
                                {!! $base_placement->hours !!} ώρες
                            </span>
                            <span>
                                / {!! $base_placement->days !!}
                                @if($base_placement->days == 1)
                                    ημέρα
                                @else
                                    ημέρες
                                @endif
                            </span>
                        @elseif($base_placement->days == 5)
                            εξ ολοκλήρου
                            <?php
                                $ex_oloklirou = true;
                            ?>
                        @endif
                        @if($base_placement->description != '')
                            [{!! $base_placement->description !!}]
                        @endif
                    </p>
                @else
                    @if(!str_contains('Οργανικά',$myschool->topothetisi))
                        <br>
                        με προσωρινή τοποθέτηση στο
                        <span class="span_bold">
                            {!! $base_placement->to !!}
                        </span>
                        {{--(Πράξη Δ/ντή ΔΔΕ Χανίων {!! $base_placement->praxi->decision_number .'/'.$base_placement->praxi->decision_date !!})--}}
                        <br>
                    @endif
                @endif
            @else
                {{--- με οργανική στο--}}
                {{--<span class="span_bold">--}}
                    {{--{!! $myschool->organiki_name !!}--}}
                {{--</span> ---}}
            @endif

            @if(!$new_placements->isEmpty())
                @if($base_placement != null)
                    @if($praxi->decision_number == $base_placement->praxi->decision_number)
                        <p> και
                    @endif
                @endif

                @if(!$new_placements->where('placements_type', 2)->isEmpty())
                    <p id="title">
                        αποσπαστήκατε
                    </p>
                    @if($praxi->aitisis['value'] != 'true&false')
                        {!! $praxi->aitisis['text'] !!} σας
                    @endif
                    στο: </p>

                                            <ol>
                                                @foreach($new_placements as $new_placement)
                                                    <li>
                                                        <span class="span_bold">
                                                            {!! $new_placement->to !!}
                                                        </span>
                                                        για
                                                        <span class="span_bold">
                                                            {!! $new_placement->hours !!} ώρες
                                                        </span>
                                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                            <span>
                                                                / {!! $new_placement->days !!}
                                                                @if($new_placement->days == 1)
                                                                    ημέρα
                                                                @else
                                                                    ημέρες
                                                                @endif
                                                            </span>
                                                        @endif
                                                        @if($praxi->aitisis['value'] == 'true&false')
                                                            @if($new_placement->me_aitisi)
                                                                με αίτηση
                                                            @else
                                                                χωρίς αίτηση
                                                            @endif
                                                        @endif
                                                        @if($new_placement->description != '')
                                                            [{!! $new_placement->description !!}]
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ol>

                @endif
                @if(!$new_placements->where('placements_type', 7)->isEmpty())
                    <p id="title">
                        αποσπαστήκατε
                    </p>
                    @if($praxi->aitisis['value'] != 'true&false')
                        {!! $praxi->aitisis['text'] !!} σας
                    @endif
                    στο: </p>

                                            <ol>
                                                @foreach($new_placements->where('placements_type', 7) as $new_placement)
                                                    <li>
                                                        <span class="span_bold">
                                                            {!! $new_placement->to !!}
                                                        </span>
                                                        για
                                                        <span class="span_bold">
                                                            {!! $new_placement->hours !!} ώρες
                                                        </span>
                                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                            <span>
                                                                / {!! $new_placement->days !!}
                                                                @if($new_placement->days == 1)
                                                                    ημέρα
                                                                @else
                                                                    ημέρες
                                                                @endif
                                                            </span>
                                                        @endif
                                                        @if($praxi->aitisis['value'] == 'true&false')
                                                            @if($new_placement->me_aitisi)
                                                                με αίτηση
                                                            @else
                                                                χωρίς αίτηση
                                                            @endif
                                                        @endif
                                                        @if($new_placement->description != '')
                                                            [{!! $new_placement->description !!}]
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ol>
                @endif
                @if(!$new_placements->where('placements_type', 9)->isEmpty())
                    <p id="title">
                        ανακαλείται
                    </p>
                    η τοποθέτησή σας στο: </p>
                                                                <ol>
                                                                    @foreach($new_placements->where('placements_type', 9) as $new_placement)
                                                                        <li>
                                                                            <span class="span_bold">
                                                                                {!! $new_placement->to !!}
                                                                            </span>
                                                                            για
                                                                            <span class="span_bold">
                                                                                {!! $new_placement->hours !!} ώρες
                                                                            </span>
                                                                            @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                                                <span>
                                                                                    / {!! $new_placement->days !!}
                                                                                    @if($new_placement->days == 1)
                                                                                        ημέρα
                                                                                    @else
                                                                                        ημέρες
                                                                                    @endif
                                                                                </span>
                                                                            @endif
                                                                            @if($praxi->aitisis['value'] == 'true&false')
                                                                                @if($new_placement->me_aitisi)
                                                                                    με αίτηση
                                                                                @else
                                                                                    χωρίς αίτηση
                                                                                @endif
                                                                            @endif
                                                                            @if($new_placement->description != '')
                                                                                [{!! $new_placement->description !!}]
                                                                            @endif
                                                                        </li>
                                                                    @endforeach
                                                                </ol>
                @endif
                @if(!$new_placements->where('placements_type', 3)->isEmpty() || !$new_placements->where('placements_type', 5)->isEmpty() || !$new_placements->where('placements_type', 8)->isEmpty())
                    <p id="title">
                        διατεθήκατε
                    </p>

                    @if($praxi->aitisis['value'] != 'true&false')
                        {!! $praxi->aitisis['text'] !!} σας
                    @endif
                    @if(!$new_placements->where('placements_type', 8)->isEmpty())
                        στην Π/θμια Εκπαίδευση για
                        <span class="span_bold">
                            {!! $new_placements->first()->hours !!} ώρες
                        </span>
                        @if($new_placements->first()->days >=1 && $new_placements->first()->days <= 5)
                            <span>
                                / {!! $new_placements->first()->days !!}
                                @if($new_placements->first()->days == 1)
                                    ημέρα
                                @else
                                    ημέρες
                                @endif
                            </span>
                        @endif

                    @else
                        για συμπλήρωση του υποχρεωτικού ωραρίου σας ({{ $myschool->ypoxreotiko }} ώρες) στο: </p>
                        <ol>
                            @foreach($new_placements->where('placements_type', 3) as $new_placement)
                                <li>
                                    <span class="span_bold">
                                        {!! $new_placement->to !!}
                                    </span>
                                    για
                                    <span class="span_bold">
                                        {!! $new_placement->hours !!} ώρες
                                    </span>
                                    @if($new_placement->days >=1 && $new_placement->days <= 5)
                                        <span>
                                            / {!! $new_placement->days !!}
                                            @if($new_placement->days == 1)
                                                ημέρα
                                            @else
                                                ημέρες
                                            @endif
                                        </span>
                                    @endif
                                    @if($praxi->aitisis['value'] == 'true&false')
                                        @if($new_placement->me_aitisi)
                                            με αίτηση
                                        @else
                                            χωρίς αίτηση
                                        @endif
                                    @endif
                                    @if($new_placement->description != '')
                                        [{!! $new_placement->description !!}]
                                    @endif
                                </li>
                            @endforeach
                            @foreach($new_placements->where('placements_type', 5) as $new_placement)
                                <li>
                                    <span class="span_bold">
                                        {!! $new_placement->to !!}
                                    </span>
                                    για
                                    <span class="span_bold">
                                        {!! $new_placement->hours !!} ώρες
                                    </span>
                                    @if($new_placement->days >=1 && $new_placement->days <= 5)
                                        <span>
                                            / {!! $new_placement->days !!}
                                            @if($new_placement->days == 1)
                                                ημέρα
                                            @else
                                                ημέρες
                                            @endif
                                        </span>
                                    @endif
                                    @if($praxi->aitisis['value'] == 'true&false')
                                        @if($new_placement->me_aitisi)
                                            με αίτηση
                                        @else
                                            χωρίς αίτηση
                                        @endif
                                    @endif
                                    @if($new_placement->description != '')
                                        [{!! $new_placement->description !!}]
                                    @endif
                                </li>
                            @endforeach
                        </ol>
                    @endif

                @endif

            @endif
        </div>

        @if($ex_oloklirou)
                <div class="line_apofasi">
                    @if($new_placements->count() > 0)
                        από {!! $new_placements->first()->starts_at !!} μέχρι τις {!! $new_placements->first()->ends_at !!}.
                    @elseif($base_placement->count() > 0)
                        από {!! $base_placement->starts_at !!} μέχρι τις {!! $base_placement->ends_at !!}.
                    @endif
                </div>
        @else
            <div class="line_apofasi">
                @if($new_placements->count() > 0)
                    από {!! $new_placements->first()->starts_at !!} μέχρι τις {!! $new_placements->first()->ends_at !!}.
                @elseif($base_placement->count() > 0)
                    από {!! $base_placement->starts_at !!} μέχρι τις {!! $base_placement->ends_at !!}.
                @endif
            </div>
        @endif


        @if($new_placements->count() == 1 &&  in_array($new_placements->first()->placements_type, [9]))

        @else
            <div class="line_apofasi">
                <br>
                Κατόπιν αυτού παρακαλούμε να παρουσιαστείτε στο Σχολείο
                @if(!$new_placements->isEmpty())
                    @if(in_array($new_placements->first()->placements_type, [2,7]))
                        της απόσπασή σας,
                    @else
                        @if($base_placement != null)
                            @if($praxi->decision_number == $base_placement->praxi->decision_number)
                                της τοποθέτησή σας,
                            @endif
                        @else
                            της διάθεσής σας
                        @endif
                    @endif
                @else
                    @if($base_placement != null)
                        @if($praxi->decision_number == $base_placement->praxi->decision_number)
                            της τοποθέτησή σας,
                        @endif
                    @else
                        της διάθεσής σας
                    @endif
                @endif

                για να αναλάβετε υπηρεσία. Το Σχολείο

                @if(!$new_placements->isEmpty())
                    @if(in_array($new_placements->first()->placements_type, [2,7]))
                        απόσπασης
                    @else
                        @if($base_placement != null)
                            @if($praxi->decision_number == $base_placement->praxi->decision_number)
                                τοποθέτησης
                            @endif
                        @else
                            Διάθεση
                        @endif
                    @endif
                @else
                        @if($base_placement != null)
                            @if($praxi->decision_number == $base_placement->praxi->decision_number)
                                τοποθέτησης
                            @endif
                        @else
                            Διάθεση
                        @endif
                @endif

                παρακαλείται να μας αποστείλει άμεσα την πράξη ανάληψή σας.
            </div>
        @endif

    </div>

    <table id="footer">
        <tr>
            <td id="left_footer">

            </td>
            <td id="center_footer"></td>
            <td id="right_footer">
                <div>Ο Διευθυντής της Δ.Δ.Ε. Χανίων</div>
                <div>
                    <img src="img/$2y$10$M7lTqmCckaPkU0uEd87RpulMkrDY95L2V8onvVodtDfxtQr24vUCG.jpg" height="140px" width="228px">
                 </div>
                <div>{{ config('requests.administrator_name') }}</div>
                <div>{{ config('requests.administrator_job_title') }}</div>
            </td>
        </tr>
    </table>

