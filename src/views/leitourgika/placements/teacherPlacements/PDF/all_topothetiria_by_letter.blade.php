<!DOCTYPE html>
<html lang="gr">
    @include('pysde::leitourgika.placements.teacherPlacements.PDF._template_head')
<body>
    @foreach($pages as $index=>$praxi)
        @if($beta)
            @include('pysde::leitourgika.placements.teacherPlacements.PDF._template6apofasi', [
                'new_placements' => $praxi->new_placements,
                'old_placements' => $praxi->old_placements,
                'base_placement'=> $praxi->last_BASE_placement,
                'current_praxi_decision_number'      => $praxi->decision_number,
                'old_aitisis_value'                  => $praxi->old_aitisis,
                'base_aitisis'                       => $praxi->base_aitisis,
                'sum_hours_of_placements'            => $praxi->sum_hours_of_placements,
                'sum_days_of_placements'             => $praxi->sum_days_of_placements,
                'myschool'                           => $praxi->teacher
            ])        
        @else
            @include('pysde::leitourgika.placements.teacherPlacements.PDF._template6', [
                'new_placements' => $praxi->new_placements,
                'old_placements' => $praxi->old_placements,
                'base_placement'=> $praxi->last_BASE_placement,
                'sum_hours_of_placements'            => $praxi->sum_hours_of_placements,
                'sum_days_of_placements'             => $praxi->sum_days_of_placements
            ])        
        @endif

        <div class="print_date">
            Ημερομηνία εκτύπωσης: {!! \Carbon\Carbon::now()->format('d-m-Y H:i:s') !!}
        </div>
        @if($pages->count() != ($index + 1))
            <div class="page-break"></div>
        @endif

    @endforeach

</body>
</html>
