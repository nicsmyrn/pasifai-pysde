    <table>
        <tr>
            <td class="logo_cell_name">
                <img src="img/ethnosimo.jpg" id="logo">
                <div>ΕΛΛΗΝΙΚΗ ΔΗΜΟΚΡΑΤΙΑ</div>
                <div>ΥΠΟΥΡΓΕΙΟ ΠΑΙΔΕΙΑΣ, ΘΡΗΣΚΕΥΜΑΤΩΝ και ΑΘΛΗΤΙΣΜΟΥ</div>
                <div>ΠΕΡ. Δ/ΝΣΗ Α/ΘΜΙΑΣ & Β/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΚΡΗΤΗΣ</div>
                <div>Δ/ΝΣΗ Δ/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΧΑΝΙΩΝ</div>
                @if(intval(\Carbon\Carbon::parse(\Config::get('requests.date_of_new_placements'))->format('Ymd')) > intval(\Carbon\Carbon::parse($praxi->decision_date)->format('Ymd')))
                    <div>ΤΜΗΜΑ ΔΙΟΙΚΗΤΙΚΩΝ ΘΕΜΑΤΩΝ</div>
                @else
                    <div>ΤΜΗΜΑ Γ΄ ΠΡΟΣΩΠΙΚΟΥ</div>
                @endif
                <hr class="line">
            </td>
            <td></td>
            <td id="protocol">
                <div>ΧΑΝΙΑ {!! $praxi->dde_protocol_date !!}</div>
                <div>ΑΡΙΘΜ ΠΡΩΤ: Φ.10.3/{!! $praxi->dde_protocol !!}</div>
            </td>
        </tr>
        <tr>
            <td>
                <div>Ταχ. Διεύθυνση: Γκερόλα 48 Β, 73132 ΧΑΝΙΑ</div>
                <div>Πληροφορίες: {{ config('requests.information_name') }}</div>
                <div>Τηλέφωνο: 2821047152 Fax: 2821047137</div>
                <div>E-mail: mail&#64;dide.chan.sch.gr</div>
            </td>
            <td></td>
            <td>
                <div id="title"><div>ΑΠΟΦΑΣΗ</div></div>
            </td>
        </tr>
    </table>

    <!-- TODO: ###### subject ####### -->
    <div id="subject">
        Θέμα: &laquo;{!! $praxi->title !!}&raquo;
    </div>

    <div id="apofasis">
        <div>
            <ol class="nomoi">
                Έχοντας υπόψη:
                <li>
                    Τις διατάξεις του Ν. 1566/1985 (ΦΕΚ 167, τ.Α΄/30-9-1985), όπως έχει τροποποιηθεί, συμπληρωθεί και ισχύει
                </li>
                <li>
                    Τις διατάξεις του Π.Δ. 50/1996 (ΦΕΚ 45, τ.Α΄/08-3-1996), όπως έχει τροποποιηθεί, συμπληρωθεί και ισχύει
                </li>
                <li>
                    Τις διατάξεις  των εδαφ. 5 και 6, της παραγρ.1, του άρθρου 25, του Ν.4203/2013 (ΦΕΚ 235, τ.Α΄/01-11-2013) και της παραγρ. 5, του άρθρου 33, του Ν.4386/2016 (ΦΕΚ 83, τ΄.Α΄/11-5-2016), που αντικατέστησε το παραπάνω εδάφ. 6 και συμπληρώθηκε με το άρθρο 46, του Ν.4415/2016 (ΦΕΚ 159, τ.Α΄/06-9-2016)
                </li>
                <li>
                    Τις διατάξεις του Ν. 3528/2007 (Υ.Κ.) (Φ.Ε.Κ. 26, τ. Α΄/09-02-2007) με θέμα: «Κύρωση του Κώδικα Κατάστασης Δημοσίων Υπαλλήλων και Υπαλλήλων Ν.Π.Δ.Δ.», όπως έχει τροποποιηθεί, συμπληρωθεί και ισχύει
                </li>

                <li>
                    Τις διατάξεις του άρθρου 12, του Π.Δ. 1/2003 (Φ.Ε.Κ. 1, τ.Α΄/03-01-2003) με θέμα: «Σύνθεση, συγκρότηση και λειτουργία των υπηρεσιακών συμβουλίων πρωτοβάθμιας και δευτεροβάθμιας εκπαίδευσης…», όπως έχει τροποποιηθεί και ισχύει
                </li>
                <li>
                    Τις διατάξεις του Ν.3861/2010 (Φ.Ε.Κ. 112, τ. Α΄/13-7-2010) «Ενίσχυση της διαφάνειας με την υποχρεωτική ανάρτηση  νόμων και πράξεων των κυβερνητικών, διοικητικών και αυτοδιοικητικών οργάνων στο διαδίκτυο - «Πρόγραμμα Διαύγεια» και άλλες διατάξεις»
                </li>
                <li>
                    Την υπ’ αριθμ. πρωτ. Φ.353.1/4/4674/Ε3/16-1-2024 (ΑΔΑ: 9Λ1346ΝΚΠΔ-ΨΛ6) Υ.Α. Υ.ΠΑΙ.Θ.Α., με θέμα: «Τοποθέτηση Διευθυντή Δευτεροβάθμιας Εκπαίδευσης»
                </li>
                <li>
                    Την υπ’ αριθµ. 170405/ΓΓ1/28-12-2021 (ΑΔΑ: 6ΛΠΦ46ΜΤΛΗ-Τ5Ν) Υπουργική Απόφαση του Υ.ΠΑΙ.Θ. (ΦΕΚ 6273, τ.Β΄/28-12-2021) µε θέµα: «Καθορισµός των ειδικότερων καθηκόντων και αρµοδιοτήτων των Διευθυντών Εκπαίδευσης»
                </li>

                @if($myschool->organiki_id == 2000)
                    @foreach(config('requests.apospaseis_ypourgeiou') as $key=>$value)
                        @if($key == '2020-2021')
                            @foreach($value as $k=>$v)
                                <li>
                                    Την αριθμ. {{$k}} Υ.Α. &laquo;{{$v}}&raquo;
                                </li>
                            @endforeach
                        @endif
                    @endforeach
                @endif
                    <li>
                        Τις ανάγκες των Σχολείων σε διδακτικό προσωπικό
                    </li>
                <!-- @if($new_placements->first() != null)
                    @if(in_array($new_placements->first()->placements_type,[1,3]))
                        <li>
                            Την αίτηση @if($myschool->sex) του ενδιαφερόμενου @else της ενδιαφερόμενης @endif εκπαιδευτικού
                        </li>
                    @endif
                @endif -->
                <li>
                    Την υπ' αριθμ. <b>{!! $praxi->decision_number !!} / {!! $praxi->decision_date !!}</b> Πράξη του ΠΥΣΔΕ Χανίων
                </li>
            </ol>
        </div>
        <div id="wrapper">
            <div id="apofasi">αποφασίζουμε</div>

            <div class="line_apofasi">
                για τον/την εκπαιδευτικό <span class="span_bold">{{ $myschool->full_name }}</span> του  <span class="span_bold">{{ $myschool->middle_name }}</span>
                κλάδου <span class="span_bold">{{ $myschool->eidikotita }}</span> ({{ $myschool->new_eidikotita_name }})
                @if($myschool->organiki_id < 1000)
                    με οργανική στο  <span class="span_bold">{{ $myschool->organiki_name }}</span>
                @else
                    - <span class="span_bold">{{ $myschool->organiki_name }}</span>
                @endif
            </div>

            @if($base_placement != null)
                @if($praxi->decision_number == $base_placement->praxi->decision_number)
                    <?php
                        $reg = "/υπεράριθμος/i";
                        $isYperarithmos = preg_match($reg, $base_placement->description);
                    ?>
                    <p class="line_apofasi">
                        &#9775; την <span class="span_bold">
                            @if($isYperarithmos)
                                απόσπαση ως υπεράριθμος
                            @else
                                προσωρινή τοποθέτηση
                            @endif
                        </span> {!! $praxi->base_aitisis['text']  !!} 
                        @if($myschool->sex == 0)
                            της
                        @else
                            του
                        @endif
                        στο:
                        <span class="span_bold">
                            {!! $base_placement->to !!}
                        </span>

                        @if($base_placement->days >=1 && $base_placement->days < 5)
                            για
                            <span class="span_bold">
                                {!! $base_placement->hours !!} ώρες
                            </span>
                                / {!! $base_placement->days !!}
                                @if($base_placement->days == 1)
                                    ημέρα
                                @else
                                    ημέρες
                                @endif
                        @elseif($base_placement->days == 5)
                            εξ ολοκλήρου
                            <?php
                                $ex_oloklirou = true;
                            ?>
                        @endif
                        @if($base_placement->description != '')
                            [{!! $base_placement->description !!}]
                        @endif
                    </p>
                @else
                    @if(!str_contains('Οργανικά',$myschool->topothetisi))
                        <br>
                        με προσωρινή τοποθέτηση στο
                        <span class="span_bold">
                            {!! $base_placement->to !!}
                        </span>
                        {{--(Πράξη Δ/ντή ΔΔΕ Χανίων {!! $base_placement->praxi->decision_number .'/'.$base_placement->praxi->decision_date !!})--}}
                        <br>
                    @endif
                @endif
            @endif

            @if(!$new_placements->isEmpty())
                @if(!$new_placements->where('placements_type', 2)->isEmpty())
                    <p id="title">
                        αποσπαστήκατε
                    </p>
                    @if($praxi->aitisis['value'] != 'true&false')
                        {!! $praxi->aitisis['text'] !!} σας
                    @endif
                    στο: </p>

                                            <ol>
                                                @foreach($new_placements as $new_placement)
                                                    <li>
                                                        <span class="span_bold">
                                                            {!! $new_placement->to !!}
                                                        </span>
                                                        για
                                                        <span class="span_bold">
                                                            {!! $new_placement->hours !!} ώρες
                                                        </span>
                                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                                / {!! $new_placement->days !!}
                                                                @if($new_placement->days == 1)
                                                                    ημέρα
                                                                @else
                                                                    ημέρες
                                                                @endif
                                                        @endif
                                                        @if($praxi->aitisis['value'] == 'true&false')
                                                            @if($new_placement->me_aitisi)
                                                                με αίτηση
                                                            @else
                                                                χωρίς αίτηση
                                                            @endif
                                                        @endif
                                                        @if($new_placement->description != '')
                                                            [{!! $new_placement->description !!}]
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ol>

                @endif
                @if(!$new_placements->where('placements_type', 7)->isEmpty())
                    <p id="title">
                        αποσπαστήκατε
                    </p>
                    @if($praxi->aitisis['value'] != 'true&false')
                        {!! $praxi->aitisis['text'] !!} σας
                    @endif
                    στο: </p>

                                            <ol>
                                                @foreach($new_placements->where('placements_type', 7) as $new_placement)
                                                    <li>
                                                        <span class="span_bold">
                                                            {!! $new_placement->to !!}
                                                        </span>
                                                        για
                                                        <span class="span_bold">
                                                            {!! $new_placement->hours !!} ώρες
                                                        </span>
                                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                                / {!! $new_placement->days !!}
                                                                @if($new_placement->days == 1)
                                                                    ημέρα
                                                                @else
                                                                    ημέρες
                                                                @endif
                                                        @endif
                                                        @if($praxi->aitisis['value'] == 'true&false')
                                                            @if($new_placement->me_aitisi)
                                                                με αίτηση
                                                            @else
                                                                χωρίς αίτηση
                                                            @endif
                                                        @endif
                                                        @if($new_placement->description != '')
                                                            [{!! $new_placement->description !!}]
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ol>
                @endif
                @if(!$new_placements->where('placements_type', 9)->isEmpty())
                    <p id="title">
                        ανακαλείται
                    </p>
                    η τοποθέτησή σας στο: </p>
                        <ol>
                            @foreach($new_placements->where('placements_type', 9) as $new_placement)
                                <li>
                                    <span class="span_bold">
                                        {!! $new_placement->to !!}
                                    </span>
                                    για
                                    <span class="span_bold">
                                        {!! $new_placement->hours !!} ώρες
                                    </span>
                                    @if($new_placement->days >=1 && $new_placement->days <= 5)
                                            / {!! $new_placement->days !!}
                                            @if($new_placement->days == 1)
                                                ημέρα
                                            @else
                                                ημέρες
                                            @endif
                                    @endif
                                    @if($praxi->aitisis['value'] == 'true&false')
                                        @if($new_placement->me_aitisi)
                                            με αίτηση
                                        @else
                                            χωρίς αίτηση
                                        @endif
                                    @endif
                                    @if($new_placement->description != '')
                                        [{!! $new_placement->description !!}]
                                    @endif
                                </li>
                            @endforeach
                        </ol>
                @endif
                @if(!$new_placements->where('placements_type', 3)->isEmpty() || !$new_placements->where('placements_type', 5)->isEmpty() || !$new_placements->where('placements_type', 8)->isEmpty())
                    <p class="line_apofasi">
                        &#9775; τη <span class="span_bold">διάθεση </span>
                        @if($praxi->aitisis['value'] != 'true&false')
                            {!! $praxi->aitisis['text'] !!} 
                        @endif
                        για συμπλήρωση του υποχρεωτικού
                        @if($myschool->sex == 0)
                            της
                        @else
                            του
                        @endif
                        ωραρίου στο:
                    </p>

                    @if(!$new_placements->where('placements_type', 8)->isEmpty())
                            Πρωτοβάθμια Εκπαίδευση για
                        <span class="span_bold">
                            {!! $new_placements->first()->hours !!} ώρες
                        </span>
                        @if($new_placements->first()->days >=1 && $new_placements->first()->days <= 5)
                                / {!! $new_placements->first()->days !!}
                                @if($new_placements->first()->days == 1)
                                    ημέρα
                                @else
                                    ημέρες
                                @endif
                        @endif
                    @else
                        <p>
                            <ol>
                                @foreach($new_placements->where('placements_type', 3) as $new_placement)
                                    <li>
                                        <span class="span_bold">
                                            {!! $new_placement->to !!}
                                        </span>
                                        για
                                        <span class="span_bold">
                                            {!! $new_placement->hours !!} ώρες
                                        </span>
                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                / {!! $new_placement->days !!}
                                                @if($new_placement->days == 1)
                                                    ημέρα
                                                @else
                                                    ημέρες
                                                @endif
                                        @endif
                                        @if($praxi->aitisis['value'] == 'true&false')
                                            @if($new_placement->me_aitisi)
                                                με αίτηση
                                            @else
                                                χωρίς αίτηση
                                            @endif
                                        @endif
                                        @if($new_placement->description != '')
                                            [{!! $new_placement->description !!}]
                                        @endif
                                    </li>
                                @endforeach
                            </ol>
                            <ol>
                                @foreach($new_placements->where('placements_type', 5) as $new_placement)
                                    <li>
                                        <span class="span_bold">
                                            {!! $new_placement->to !!}
                                        </span>
                                        για
                                        <span class="span_bold">
                                            {!! $new_placement->hours !!} ώρες
                                        </span>
                                        @if($new_placement->days >=1 && $new_placement->days <= 5)
                                                / {!! $new_placement->days !!}
                                                @if($new_placement->days == 1)
                                                    ημέρα
                                                @else
                                                    ημέρες
                                                @endif
                                        @endif
                                        @if($praxi->aitisis['value'] == 'true&false')
                                            @if($new_placement->me_aitisi)
                                                με αίτηση
                                            @else
                                                χωρίς αίτηση
                                            @endif
                                        @endif
                                        @if($new_placement->description != '')
                                            [{!! $new_placement->description !!}]
                                        @endif
                                    </li>
                                @endforeach
                            </ol>
                        </p>

                    @endif

                @endif

            @endif

            @if($ex_oloklirou)
                    <div class="from_line_apofasi">
                        @if($new_placements->count() > 0)
                            από {!! $new_placements->first()->starts_at !!} μέχρι τις {!! $new_placements->first()->ends_at !!}.
                        @elseif($base_placement->count() > 0)
                            από {!! $base_placement->starts_at !!} μέχρι τις {!! $base_placement->ends_at !!}.
                        @endif
                    </div>
            @else
                <div class="from_line_apofasi">
                    @if($new_placements->count() > 0)
                        από {!! $new_placements->first()->starts_at !!} μέχρι τις {!! $new_placements->first()->ends_at !!}.
                    @elseif($base_placement->count() > 0)
                        από {!! $base_placement->starts_at !!} μέχρι τις {!! $base_placement->ends_at !!}.
                    @endif
                </div>
            @endif
        </div>
    </div>

<!-- ####################### OLD PLACEMENTS  START ############################### -->

 @if(config('requests.display_old_placements'))
            @if(!$old_placements->isEmpty())
                <table id="old_placements">
                    <thead>
                        <tr>
                            <th class="header" colspan="6">
                                Τοποθετήσεις στην Τρέχουσα Σχολική Χρονιά
                            </th>
                        </tr>
                        <tr>
                            <th class="header">Πράξη</th>
                            <th class="header">Σχολική Μονάδα</th>
                            <th class="header">Ώρες/Ημ</th>
                            <th class="header">Τοποθέτηση</th>
                            <th class="header">Με αίτηση</th>
                            <th class="header">Παρατηρήσεις</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if($base_placement != null)
                            @if($current_praxi_decision_number != $base_placement->praxi->decision_number)
                                <tr>
                                    <td class="content_centered"><a href="{{ $base_placement->praxi->ada }}">{{ $base_placement->praxi->decision_number . '/' . $base_placement->praxi->decision_date }}</a></td>
                                    <td class="content_left">{{ $base_placement->to }}</td>
                                    <td class="content_centered">{!! $base_placement->hours !!}Ωρ @if($base_placement->days > 0) /{{$base_placement->days}}Ημ @endif</td>
                                    <td class="content_centered">{{ config('requests.placements_type')[$base_placement->placements_type] }}</td>
                                    <td class="content_centered">@if($base_placement->me_aitisi)&#10003;@else&#10007;@endif</td>
                                    <td class="content_centered">από {{$base_placement->praxi->dde_protocol_date . '. '. $base_placement->description }}</td>
                                </tr>
                            @endif
                        @else
                            <tr>
                                <td class="content_centered"><a href="#"></a></td>
                                <td class="content_left">{{ $myschool->organiki_name }}</td>
                                <td class="content_centered">{{ ($myschool->ypoxreotiko - $sum_hours_of_placements) }}Ωρ @if($sum_days_of_placements > 0) /{{$sum_days_of_placements}}Ημ @endif</td>
                                <td class="content_centered">Οργανικά</td>
                                <td class="content_centered"></td>
                                <td class="content_centered"></td>
                            </tr>                        
                        @endif

                        @foreach($old_placements as $old_placement)
                            <tr>
                                <td class="content_centered"><a href="{{ $old_placement->praxi->ada }}">{{ $old_placement->praxi->decision_number. '/' .$old_placement->praxi->decision_date }}</a></td>
                                <td class="content_left">{{ $old_placement->to }}</td>
                                <td class="content_centered">{!! $old_placement->hours !!}Ωρ@if($old_placement->days >=1 && $old_placement->days <= 5)/{!! $old_placement->days !!} @if($old_placement->days == 1)Ημ @else Ημ @endif @endif</td>
                                <td class="content_centered">{{ config('requests.placements_type')[$old_placement->placements_type] }}</td>
                                <td class="content_centered">@if($old_placement->me_aitisi)&#10003;@else &#10007;@endif</td>
                                <td class="content_centered">από {{ $old_placement->praxi->dde_protocol_date . '. '. $old_placement->description }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
        @endif
<!-- ####################### OLD PLACEMENTS END ############################### -->
    <table id="footer">
        <tr>
            <td id="left_footer">
                <div>Κοινοποίηση:</div>
                <div> @if($myschool->sex)- Ενδιαφερόμενος @else - Ενδιαφερόμενη @endif</div>
                @if($myschool->organiki_id != 1000)
                    <div>- {!! $myschool->organiki_name !!} </div>
                @endif
                @foreach($new_placements as $placement)
                    <div>- {!! $placement->to !!}</div>
                @endforeach
            </td>
            <td id="center_footer"></td>
            <td id="right_footer">
                <div>Ε.Υ.</div>
                <div>Ο Διευθυντής της Δ.Δ.Ε. Χανίων</div>
                <div style="height: 25px"></div>
                <div></div>
                <div>{{ config('requests.administrator_name') }}</div>
                <div>{{ config('requests.administrator_job_title') }}</div>
            </td>
        </tr>
    </table>