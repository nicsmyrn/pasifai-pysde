
<div class="row" id="placementDetails" @if($creating)style="display: none"@endif>
    <div class="col-md-6">
        <!-- Klados Full Name -->
        <div class="form-group" id="divKlados">
            <span id="kladosName">Κλάδος: </span>
            {!! Form::label('klados_id', $placement->klados, ['class'=>'control-label']) !!}
            <input type="hidden" value="{!! $placement->klados_id !!}" name="klados_id" id="hiddenKlados"/>
        </div>

        <!-- Praxi_id  Form Input -->
        <div class="form-group" id="">
            {!! Form::label('praxi_id', 'Αρ. Πράξης:', ['class'=>'control-label']) !!}
            <select name="praxi_id">
                <option value="0">Επέλεξε Πράξη</option>
                @foreach($praxeis as $praxi)
                    <option @if($placement->praxi_id == $praxi->id) selected @endif value="{!! $praxi->id !!}">{!! Config::get('requests.praxi_type')[$praxi->praxi_type] !!}: {!! $praxi->decision_number !!}η / {!! $praxi->decision_date !!}</option>
                @endforeach
            </select>
        </div>

        <!-- From_id  Form Input -->
        <div class="form-group" id="">
            {!! Form::label('to_id', 'Προς:', ['class'=>'control-label']) !!}
{{--                    {!! Form::select('to_id',Config::get('requests.specieal_placements') + $schools, null,['class' => 'school_select']) !!}--}}
            <select name="to_id" class="school_select2 school_select">
                <option></option>
                @foreach(Config::get('requests.special_placements') as $k=>$v)
                    <option @if($placement->to_id == $k) selected @endif value="{{$k}}">{{$v}}</option>
                @endforeach
                @foreach($schools as $school)
                    <option @if($placement->to_id == $school->id) selected @endif value="{!! $school->id !!}">{!! $school->name !!}</option>
                @endforeach
            </select>
        </div>

        <!-- From_id  Form Input -->
        <div class="form-group" class="col-md-6 col-md-offset-3">
            {!! Form::label('me_aitisi', 'Έχει υποβάλλει αίτηση', ['class'=>'text-left control-label']) !!}
            {!! Form::checkbox('me_aitisi') !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Organiki -->
        <div class="form-group" id="divOrganiki">
            <span id="organikiName">Οργανική:</span>
            {!! Form::label('from_id', $placement->from , ['class'=>'control-label']) !!}
            <input type="hidden" name="from_id" id="hiddenOrganiki" value="{!! $placement->from_id !!}"/>
        </div>
        <!-- From_id  Form Input -->
        <div class="form-group" id="">
            {!! Form::label('from_id', 'Τύπος τοποθέτησης:', ['class'=>'control-label']) !!}
            {!! Form::select('placements_type', [0 => 'Επέλεξε τύπο'] + Config::get('requests.placements_type')) !!}
        </div>
        <!-- From_id  Form Input -->
        <div class="form-group" class="col-md-6 col-md-offset-3">
            {!! Form::label('hours', 'Ώρες τοποθέτησης:', ['class'=>'text-left control-label']) !!}
            {!! Form::text('hours',null, ['size'=> 2, 'maxlength'=>2, 'id' => 'hours']) !!}
        </div>

        <!-- From_id  Form Input -->
        <div class="form-group" class="col-md-6 col-md-offset-3">
            {!! Form::label('days', 'Ημέρες τοποθέτησης [προαιρετικό]:', ['class'=>'text-left control-label']) !!}
            {!! Form::select('days',[
                0 => '-',
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5
            ]) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        {!! Form::label('starts_at', 'Ισχύει από:', ['class'=>'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::text('starts_at',null, ['class'=>'form-control', 'id'=>'date_from']) !!}
        </div>
    </div>
    <div class="col-md-4">
        {!! Form::label('ends_at', 'μέχρι:', ['class'=>'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::text('ends_at',null, ['class'=>'form-control', 'id'=>'date_to']) !!}
        </div>
    </div>
    <div class="col-md-4">
        @if($placement->pending)
            <h5><label class="control-label"><span class="label label-danger">Σε εκκρεμότητα</span>
            {!! Form::checkbox('pending', 1, null, ['class'=>'checkbox-inline']) !!}
            </label></h5>
        @else
            <h5><label class="control-label"><span class="label label-info">Σε εκκρεμότητα</span>
            {!! Form::checkbox('pending', 1, null, ['class'=>'checkbox-inline']) !!}
            </label></h5>
        @endif
    </div>

    <!-- From_id  Form Input -->
    <div class="form-group" id="" class="col-md-12 text-center">
        {!! Form::label('description', 'Παρατηρήσεις:', ['class'=>'control-label']) !!}
        {!! Form::text('description',null, ['class'=>'form-control']) !!}
    </div>

    <!-- Αποθήκευση Form Input -->
    <div class="form-group col-md-4 col-md-offset-4">
        {!! Form::submit($submitButton, ['class'=>'btn btn-primary']) !!}
    </div>
</div>

