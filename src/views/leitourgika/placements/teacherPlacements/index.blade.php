@extends('app')

@section('title')
    Τοποθετήσεις Καθηγητή {!! $myschool->full_name !!}
@stop

@section('header.style')
<style>
    .table-user-information > tbody > tr {
        border-top: 1px solid rgb(221, 221, 221);
    }

    .table-user-information > tbody > tr:first-child {
        border-top: 0;
    }


    .table-user-information > tbody > tr > td {
        border-top: 0;
    }
    .pending{
        background-color: #ffff00 !important;
    }



img
{
	vertical-align: middle;
}
.img-responsive
{
	display: block;
	height: auto;
	max-width: 100%;
}
.img-rounded
{
	border-radius: 3px;
}
.img-thumbnail
{
	background-color: #fff;
	border: 1px solid #ededf0;
	border-radius: 3px;
	display: inline-block;
	height: auto;
	line-height: 1.428571429;
	max-width: 100%;
	moz-transition: all .2s ease-in-out;
	o-transition: all .2s ease-in-out;
	padding: 2px;
	transition: all .2s ease-in-out;
	webkit-transition: all .2s ease-in-out;
}
.img-circle
{
	border-radius: 50%;
}
.timeline-centered {
    position: relative;
    margin-bottom: 30px;
}

    .timeline-centered:before, .timeline-centered:after {
        content: " ";
        display: table;
    }

    .timeline-centered:after {
        clear: both;
    }

    .timeline-centered:before, .timeline-centered:after {
        content: " ";
        display: table;
    }

    .timeline-centered:after {
        clear: both;
    }

    .timeline-centered:before {
        content: '';
        position: absolute;
        display: block;
        width: 4px;
        background: #f5f5f6;
        left: 10%;
        top: 20px;
        bottom: 20px;
        margin-left: -4px;
    }

    .timeline-centered .timeline-entry {
        position: relative;
        width: 90%;
        float: right;
        margin-bottom: 25px;
        clear: both;
    }

        .timeline-centered .timeline-entry:before, .timeline-centered .timeline-entry:after {
            content: " ";
            display: table;
        }

        .timeline-centered .timeline-entry:after {
            clear: both;
        }

        .timeline-centered .timeline-entry:before, .timeline-centered .timeline-entry:after {
            content: " ";
            display: table;
        }

        .timeline-centered .timeline-entry:after {
            clear: both;
        }

        .timeline-centered .timeline-entry.begin {
            margin-bottom: 0;
        }

        .timeline-centered .timeline-entry.left-aligned {
            float: left;
        }

            .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner {
                margin-left: 0;
                margin-right: -18px;
            }

                .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner .timeline-time {
                    left: auto;
                    right: -100px;
                    text-align: left;
                }

                .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner .timeline-icon {
                    float: right;
                }

                .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner .timeline-label {
                    margin-left: 0;
                    margin-right: 25px;
                }

                    .timeline-centered .timeline-entry.left-aligned .timeline-entry-inner .timeline-label:after {
                        left: auto;
                        right: 0;
                        margin-left: 0;
                        margin-right: -9px;
                        -moz-transform: rotate(180deg);
                        -o-transform: rotate(180deg);
                        -webkit-transform: rotate(180deg);
                        -ms-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

        .timeline-centered .timeline-entry .timeline-entry-inner {
            position: relative;
            margin-left: -12px;
        }

            .timeline-centered .timeline-entry .timeline-entry-inner:before, .timeline-centered .timeline-entry .timeline-entry-inner:after {
                content: " ";
                display: table;
            }

            .timeline-centered .timeline-entry .timeline-entry-inner:after {
                clear: both;
            }

            .timeline-centered .timeline-entry .timeline-entry-inner:before, .timeline-centered .timeline-entry .timeline-entry-inner:after {
                content: " ";
                display: table;
            }

            .timeline-centered .timeline-entry .timeline-entry-inner:after {
                clear: both;
            }

            .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time {
                /* position: absolute;
                left: -100px; */
                /* text-align: right; */
                /* padding: 10px; */
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time > span {
                    display: block;
                }

                    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time > span:first-child {
                        font-size: 15px;
                        font-weight: bold;
                    }

                    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time > span:last-child {
                        font-size: 12px;
                    }

            .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon {
                background: #fff;
                color: #737881;
                display: block;
                width: 20px;
                height: 20px;
                -webkit-background-clip: padding-box;
                -moz-background-clip: padding;
                background-clip: padding-box;
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                text-align: center;
                -moz-box-shadow: 0 0 0 5px #f5f5f6;
                -webkit-box-shadow: 0 0 0 5px #f5f5f6;
                box-shadow: 0 0 0 5px #f5f5f6;
                line-height: 40px;
                font-size: 15px;
                float: left;
            }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-primary {
                    background-color: #303641;
                    color: #fff;
                }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-secondary {
                    background-color: #ee4749;
                    color: #fff;
                }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-success {
                    background-color: #00a651;
                    color: #fff;
                }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-info {
                    background-color: #D5D8DC;
                    color: #fff;
                }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-warning {
                    background-color: #fad839;
                    color: #fff;
                }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-icon.bg-danger {
                    background-color: #cc2424;
                    color: #fff;
                }

            .timeline-centered .timeline-entry .timeline-entry-inner .timeline-start {
                position: relative;
                background: #f5f5f6;
                padding: 0.4em;
                margin-left: 40px;
                -webkit-background-clip: padding-box;
                -moz-background-clip: padding;
                background-clip: padding-box;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
            }

            .timeline-centered .timeline-entry .timeline-entry-inner .timeline-start:after {
                    content: '';
                    display: block;
                    position: absolute;
                    width: 0;
                    height: 0;
                    border-style: solid;
                    border-width: 9px 9px 9px 0;
                    border-color: transparent #f5f5f6 transparent transparent;
                    left: 0;
                    top: 6px;
                    margin-left: -9px;
                }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-time-start > span {
                    display: block;
                    font-size: 12px;
                }

            .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label {
                position: relative;
                background: #f5f5f6;
                padding: 1em;
                margin-left: 40px;
                -webkit-background-clip: padding-box;
                -moz-background-clip: padding;
                background-clip: padding-box;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
            }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label:after {
                    content: '';
                    display: block;
                    position: absolute;
                    width: 0;
                    height: 0;
                    border-style: solid;
                    border-width: 9px 9px 9px 0;
                    border-color: transparent #f5f5f6 transparent transparent;
                    left: 0;
                    top: 6px;
                    margin-left: -9px;
                }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label h2, .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label p {
                    color: #737881;
                    font-family: "Noto Sans",sans-serif;
                    font-size: 12px;
                    margin: 0;
                    line-height: 1.428571429;
                }

                    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label p + p {
                        margin-top: 15px;
                    }

                .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label h2 {
                    font-size: 16px;
                    margin-bottom: 10px;
                }

                    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label h2 a {
                        color: #303641;
                    }

                    .timeline-centered .timeline-entry .timeline-entry-inner .timeline-label h2 span {
                        -webkit-opacity: .6;
                        -moz-opacity: .6;
                        opacity: .6;
                        -ms-filter: alpha(opacity=60);
                        filter: alpha(opacity=60);
                    }

                    .info-label{
                        color: green;
                        font-weight: bold;
                    }
                    .future-label{
                        color: red;
                    }

</style>
@endsection

@section('content')
    <h1 class="page-heading">
        <a href="{!! URL::previous() !!}" class="btn btn-primary btn-s">
            Επιστροφή
        </a>
        Τοποθετήσεις από το ΠΥΣΔΕ
        {{--@if(Auth::user()->userable_type == 'App\Teacher')--}}
            {{--<a target="_blank"--}}
                {{--href="{!! route('allTeacherPlacements') !!}"--}}
                {{--class="btn btn-success btn-lg"--}}
            {{-->--}}
        {{--@elseif(Auth::user()->userable_type == 'App\School')--}}
            {{--<a target="_blank"--}}
                {{--href="{!! action('SchoolPlacementsController@viewTeacherPlacementsPDF', ['teacher_id' => $myschool->afm]) !!}"--}}
                {{--class="btn btn-success btn-lg"--}}
            {{-->--}}
        {{--@else--}}
            {{--<a target="_blank"--}}
                {{--href="#"--}}
                {{--class="btn btn-success btn-lg"--}}
            {{-->--}}
        {{--@endif--}}
            {{--Όλα τα τοποθετήρια--}}
        {{--</a>--}}
    </h1>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h3 class="panel-title">{!! $myschool->full_name !!}</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>ΑΜ:</td>
                                    <td><b>{!! $myschool->am!!}</b></td>
                                </tr>
                                <tr>
                                    <td>Πατρώνυμο:</td>
                                    <td><b>{!! $myschool->middle_name!!}</b></td>
                                </tr>
                                <tr>
                                    <td>Κλάδος:</td>
                                    <td><b>{!! $myschool->eidikotita !!}</b></td>
                                </tr>

                                 <tr>
                                    <td>Οργανική</td>
                                    <td><b>{!! $myschool->organiki_name!!}</b></td>
                                </tr>
                                <tr>
                                    <td>Υποχρεωτικό Ωράριο:</td>
                                    <td><b>{!! $myschool->ypoxreotiko!!}</b></td>
                                </tr>

                            </tbody>
                        </table>

                        <div class="timeline-centered">

                            @foreach($sorted_wrario as $wrario)
                                @if($sorted_wrario->last() != $wrario)
                                    <article class="timeline-entry">  
                                        <div class="timeline-entry-inner">
                        
                                            @if($wrario['timeline'] == 'future')
                                                <div class="timeline-icon bg-danger">
                                                    <i class="entypo-feather"></i>
                                                </div>
                                            @elseif($wrario['timeline'] == 'current')
                                                <div class="timeline-icon bg-success">
                                                    <i class="entypo-feather"></i>
                                                </div>
                                            @else
                                                <div class="timeline-icon bg-info">
                                                    <i class="entypo-feather"></i>
                                                </div>
                                            @endif

                                            
                                            <div class="timeline-label">
                                                <p>
                                                    <time class="timeline-time">
                                                        <span>{{ $wrario['start_date'] }}</span>
                                                        <span>
                                                            @if($wrario['timeline'] == 'future')
                                                                <span class="future-label">
                                                                    μείωση
                                                                </span> 
                                                                ωραρίου σε 
                                                                <span class="future-label">{{ $wrario['hours'] }}</span> ώρες

                                                            @elseif($wrario['timeline'] == 'current')
                                                                <span class="info-label">
                                                                    έναρξη
                                                                </span> 
                                                                ωραρίου σε 
                                                                <span class="info-label">{{ $wrario['hours'] }}</span> ώρες                                                            
                                                            @else
                                                                <span>
                                                                    έναρξη
                                                                </span> 
                                                                ωραρίου σε 
                                                                <span>{{ $wrario['hours'] }}</span> ώρες    
                                                            @endif

                                                        </span>
                                                    </time>
                                                </p>
                                            </div>
                                        </div>
                                    </article>
                                @else
                                    <article class="timeline-entry begin">
                                        
                                        <div class="timeline-entry-inner">
                        
                                            <div class="timeline-icon">
                                                <i class="entypo-feather"></i>
                                            </div>
                                            
                                            <div class="timeline-start">
                                                <time class="timeline-time-start">
                                                    <span>
                                                        {{ $wrario['start_date'] }} | {{ $wrario['hours'] }} ώρες
                                                        Προϋπηρεσία: {{ $myschool['proip_years'] }} χρ | {{ $myschool['proip_months'] }}  μ | {{ $myschool['proip_days'] }}  ημ
                                                    </span>
                                                </time>
                                            </div>
                                        </div>
                                    </article>
                                @endif
                            @endforeach

                        </div>


                    </div>
                </div>
              </div>
            </div>
        </div>
        <div class="col-md-9">
            <p>
                <span style="background-color: #dff0d8;padding: 5px; border-radius: 5px;margin: 5px"> Προσωρινή τοποθέτηση</span>
                <span style="background-color: #f2dede;padding: 5px; border-radius: 5px;margin: 5px"> Ανάκληση τοποθέτησης</span>
                <span style="background-color: #f1f1f1;padding: 5px; border-radius: 5px;margin: 5px"> Συμπλήρωση ωραρίου</span>
                <span style="background-color: #d9edf7;padding: 5px; border-radius: 5px;margin: 5px"> Απόσπαση με αίτηση</span>
                <span style="background-color: #f8edba;padding: 5px; border-radius: 5px;margin: 5px"> Ολική Διάθεση</span>
                <span style="background-color: #ffff00;padding: 5px; border-radius: 5px;margin: 5px"> Σε εκκρεμότητα</span>
            </p>

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation">
                    <a href="#old_placements" aria-controls="old_placements" role="tab" data-toggle="tab" style="background-color: #f2f2f2;;">
                        Υπόλοιπα Έτη
                    </a>
                </li>
              <li role="presentation" class="active">
                    <a href="#current_placements" aria-controls="current_placements" role="tab" data-toggle="tab" style="background-color: #ccffcc">
                        {!! $current_year !!}
                    </a>
              </li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="old_placements" style="background-color:#f2f2f2">
                    @if($old_placements->isEmpty())
                        <br><br>
                        <div class="col-md-8 col-md-offset-2 alert alert-info text-center" role="alert">
                            <strong>ΔΕΝ</strong> υπάρχουν τοποθετήσεις για τα υπόλοιπα έτη
                        </div>
                        <br><br><br><br><br><br><br>
                    @else
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Α/Α</th>
                                    <th class="text-center">Πράξη ΠΥΣΔΕ</th>
                                    <th class="text-center">Στο</th>
                                    <th class="text-center">Τύπος</th>
                                    <th class="text-center">Ημέρες</th>
                                    <th class="text-center">Ώρες</th>
                                    <th class="text-center">Με αίτηση</th>
                                    <th class="text-center">Παρατηρήσεις</th>
                                    <th class="text-center">Ενέργειες</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1 ?>
                                @foreach($old_placements as $placement)
                                    @if($placement->deleted_at != null)
                                        <tr class="danger">
                                            <td class="text-center"><s>{{$index}}</s></td>
                                            <td class="text-center">
                                                @if($placement->praxi->praxi_type == 13)
                                                <s><a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        ΑΠΟΦΑΣΗ ΠΕΡΙΦΕΙΑΡΧΗ ΕΝΑΡΞΗ ΑΠΟ: {!! $placement->starts_at !!} εως {!! $placement->ends_at !!}
                                                </a></s>
                                                @else
                                                <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                    <s>{{$placement->praxi->decision_number}}η/{{$placement->praxi->decision_date}}</s>
                                                </a>
                                                @endif
                                            </td>
                                            <td><s>{{$placement->to}}</s></td>
                                            <td>{!! Config::get('requests.placements_type')[$placement->placements_type] !!}</td>
                                            <td class="text-center"><s>{!! $placement->days >0 ? $placement->days : '' !!}</s></td>
                                            <td class="text-center"><s>{!! $placement->hours >0 ? $placement->hours : '' !!}</s></td>
                                                @if($placement->me_aitisi)
                                                    <td class="text-center" style="color: #008000">
                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                    </td>
                                                @else
                                                    <td class="text-center" style="color: red">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </td>
                                                @endif
                                            <td class="text-center"><s>{{$placement->description}}</s></td>
                                                <td class="text-center">
                                                    {!! Form::open(['method'=>'POST', 'route'=>['Leitourgika::Placements::forceDeletePlacement']]) !!}

                                                       <a href="{!! route('Leitourgika::Placements::update', [$myschool->afm, $placement->id]) !!}" title="Επεξεργασία" class="btn btn-warning btn-xs">
                                                            <i class="glyphicon glyphicon-edit"></i>
                                                        </a>

                                                        <a href="{!! route('Leitourgika::Placements::restorePlacement', [$placement->id]) !!}" title="Ενεργοποίηση τοποθέτησης" class="btn btn-success btn-xs">
                                                           <i class="fa fa-undo" aria-hidden="true"></i>
                                                        </a>

                                                            <button type="button" title="Διαγραφή Τοποθέτησης" data-click="delete" class="btn btn-danger btn-xs">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                            {!!Form::hidden('placement_id', $placement->id)!!}
                                                        {!! Form::close() !!}
                                                </td>
                                        </tr>

                                    @else
                                        @if(isset($base_placement) && $base_placement->id == $placement->id)
                                            <tr class="success">
                                        @else
                                            @if(in_array($placement->placements_type, [3,5]))
                                                <tr class="active">
                                            @elseif(in_array($placement->placements_type, [2]))
                                                <tr class="info">
                                            @elseif(in_array($placement->placements_type, [4]))
                                                <tr class="warning">
                                            @else
                                                <tr>
                                            @endif
                                        @endif
                                                <td class="text-center">{{$index}}</td>
                                                <td class="text-center">
                                                    @if($placement->praxi->praxi_type == 13)
                                                    <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        ΑΠΟΦΑΣΗ ΠΕΡΙΦΕΙΑΡΧΗ ΕΝΑΡΞΗ ΑΠΟ: {!! $placement->starts_at !!} εως {!! $placement->ends_at !!}
                                                    </a>
                                                    @else
                                                    <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        {{$placement->praxi->decision_number}}η/{{$placement->praxi->decision_date}}
                                                    </a>
                                                    @endif
                                                </td>
                                                <td>{{$placement->to}}</td>
                                                <td>{!! Config::get('requests.placements_type')[$placement->placements_type] !!}</td>
                                                <td class="text-center">{!! $placement->days >0 ? $placement->days : '' !!}</td>
                                                <td class="text-center">{!! $placement->hours >0 ? $placement->hours : '' !!}</td>
                                                @if($placement->me_aitisi)
                                                    <td class="text-center" style="color: #008000">
                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                    </td>
                                                @else
                                                    <td class="text-center" style="color: red">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </td>
                                                @endif

                                                <td class="text-center">{{$placement->description}}</td>
                                                    <td class="text-center">
                                                        {!! Form::open(['method'=>'POST', 'route'=>['Leitourgika::Placements::forceDeletePlacement']]) !!}

                                                            <a href="{!! route('Leitourgika::Placements::update', [$myschool->afm, $placement->id]) !!}" title="Επεξεργασία" class="btn btn-warning btn-xs">
                                                                <i class="glyphicon glyphicon-edit"></i>
                                                            </a>

                                                            <a href="{!! route('Leitourgika::Placements::softDelete', [$placement->id]) !!}" title="Ανάκληση τοποθέτησης" class="btn btn-primary btn-xs">
                                                               <i class="fa fa-undo" aria-hidden="true"></i>
                                                            </a>

                                                            <button type="button" title="Διαγραφή Τοποθέτησης" data-click="delete" class="btn btn-danger btn-xs">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                            {!!Form::hidden('placement_id', $placement->id)!!}
                                                        {!! Form::close() !!}
                                                    </td>
                                        </tr>
                                    @endif
                                    <?php $index = $index + 1 ?>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8">
                                        @can('manage_placements')
                                                <a target="_blank"
                                                href="{!! route('Leitourgika::Placements::allPlacementsForYears', ['teacher_id' => $myschool->afm, 'year_name' => 'ΠΑΛΑΙΟΤΕΡΑ-ΕΤΗ']) !!}"
                                                    class="btn btn-warning btn-lg"
                                                >
                                                    Τοποθετήρια
                                                </a>
                                        @endcan
                                        @can('view_placements_by_dde')
                                                <a target="_blank"
                                                href="{!! route('Leitourgika::Placements::allPlacementsForYears', ['teacher_id' => $myschool->afm, 'year_name' => 'ΠΑΛΑΙΟΤΕΡΑ-ΕΤΗ']) !!}"
                                                    class="btn btn-warning btn-lg"
                                                >
                                                    Τοποθετήρια
                                                </a>
                                        @endcan
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane fade in active" id="current_placements" style="background-color:#ccffcc">
                    @if($current_placements->isEmpty())
                                <br><br>
                                <div class="col-md-8 col-md-offset-2 alert alert-warning text-center" role="alert">
                                    <strong>ΔΕΝ</strong> υπάρχουν τοποθετήσεις για το τρέχον έτος
                                </div>
                                <br><br><br><br><br><br><br>
                    @else
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Α/Α</th>
                                    <th class="text-center">Πράξη ΠΥΣΔΕ</th>
                                    <th class="text-center">Στο</th>
                                    <th class="text-center">Τύπος</th>
                                    <th class="text-center">Ημέρες</th>
                                    <th class="text-center">Ώρες</th>
                                    <th class="text-center">Με αίτηση</th>
                                    <th class="text-center">Παρατηρήσεις</th>
                                    @if(Auth::user()->isRole('employess_personnel_department'))
                                        <th class="text-center">Ενέργειες</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1 ?>
                                @foreach($current_placements as $placement)
                                    @if($placement->deleted_at != null)
                                        <tr class="danger">
                                            <td class="text-center"><s>{{$index}}</s></td>
                                            <td class="text-center">
                                                @if($placement->praxi->praxi_type == 13)
                                                <s><a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        ΑΠΟΦΑΣΗ ΠΕΡΙΦΕΙΑΡΧΗ ΕΝΑΡΞΗ ΑΠΟ: {!! $placement->starts_at !!} εως {!! $placement->ends_at !!}
                                                </a></s>
                                                @else
                                                <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                    <s>{{$placement->praxi->decision_number}}η/{{$placement->praxi->decision_date}}</s>
                                                </a>
                                                @endif
                                            </td>
                                            <td><s>{{$placement->to}}</s></td>
                                            <td>{!! Config::get('requests.placements_type')[$placement->placements_type] !!}</td>
                                            <td class="text-center"><s>{!! $placement->days >0 ? $placement->days : '' !!}</s></td>
                                            <td class="text-center"><s>{!! $placement->hours >0 ? $placement->hours : '' !!}</s></td>
                                                @if($placement->me_aitisi)
                                                    <td class="text-center" style="color: #008000">
                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                    </td>
                                                @else
                                                    <td class="text-center" style="color: red">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </td>
                                                @endif
                                            <td class="text-center"><s>{{$placement->description}}</s></td>
                                            @if(Auth::user()->isRole('employess_personnel_department'))

                                                <td class="text-center">
                                                    {!! Form::open(['method'=>'POST', 'route'=>['Leitourgika::Placements::forceDeletePlacement']]) !!}

                                                       <a href="{!! route('Leitourgika::Placements::update', [$myschool->afm, $placement->id]) !!}" title="Επεξεργασία" class="btn btn-warning btn-xs">
                                                            <i class="glyphicon glyphicon-edit"></i>
                                                        </a>

                                                        <a href="{!! route('Leitourgika::Placements::restorePlacement', [$placement->id]) !!}" title="Ενεργοποίηση τοποθέτησης" class="btn btn-success btn-xs">
                                                           <i class="fa fa-undo" aria-hidden="true"></i>
                                                        </a>

                                                        @if($placement->pending)
                                                            <a href="{!! route('Leitourgika::Placements::cancelPending', [$placement->id]) !!}" title="Ακύρωση Εκκρεμότητας" class="btn btn-info btn-xs">
                                                               <i class="fa fa-legal" aria-hidden="true"></i>
                                                            </a>
                                                        @else
                                                            <a href="{!! route('Leitourgika::Placements::enablePending', [$placement->id]) !!}" title="Ενεργοποίηση Εκκρεμότητας" class="btn btn-info btn-xs">
                                                               <i class="fa fa-legal" aria-hidden="true"></i>
                                                            </a>
                                                        @endif

                                                            <button type="button" title="Διαγραφή Τοποθέτησης" data-placement-id="{{$placement->id}}" data-click="delete" class="btn btn-danger btn-xs">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                            {!!Form::hidden('placement_id', $placement->id)!!}
                                                        {!! Form::close() !!}
                                                </td>

                                            @endif
                                        </tr>

                                    @else
                                        @if($placement->pending)
                                            <tr class="pending">
                                        @else

                                            @if(isset($base_placement) && $base_placement->id == $placement->id)
                                                <tr class="success">
                                            @else
                                                @if(in_array($placement->placements_type, [3,5]))
                                                    <tr class="active">
                                                @elseif(in_array($placement->placements_type, [2]))
                                                    <tr class="info">
                                                @elseif(in_array($placement->placements_type, [4]))
                                                    <tr class="warning">
                                                @else
                                                    <tr>
                                                @endif
                                            @endif
                                        @endif

                                                <td class="text-center">{{$index}}</td>
                                                <td class="text-center">
                                                    @if($placement->praxi->praxi_type == 13)
                                                    <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        ΑΠΟΦΑΣΗ ΠΕΡΙΦΕΙΑΡΧΗ ΕΝΑΡΞΗ ΑΠΟ: {!! $placement->starts_at !!} εως {!! $placement->ends_at !!}
                                                    </a>
                                                    @else
                                                    <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        {{$placement->praxi->decision_number}}η/{{$placement->praxi->decision_date}}
                                                    </a>
                                                    @endif
                                                </td>
                                                <td>{{$placement->to}}</td>
                                                <td>{!! Config::get('requests.placements_type')[$placement->placements_type] !!}</td>
                                                <td class="text-center">
                                                    @if($placement->placements_type != 9)
                                                        {!! $placement->days >0 ? $placement->days : '' !!}
                                                    @else

                                                    @endif
                                                </td>
                                                <td class="text-center">    
                                                    @if($placement->placements_type != 9)
                                                        <strong>
                                                            {!! $placement->hours > 0 ? $placement->hours  : '' !!}
                                                        </strong>
                                                    @else

                                                    @endif
                                                </td>
                                                @if($placement->me_aitisi)
                                                    <td class="text-center" style="color: #008000">
                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                    </td>
                                                @else
                                                    <td class="text-center" style="color: red">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </td>
                                                @endif

                                                <td class="text-center">{{$placement->description}}</td>

                                                @if(Auth::user()->isRole('employess_personnel_department'))

                                                    <td class="text-center">
                                                            <a href="{!! route('Leitourgika::Placements::update', [$myschool->afm, $placement->id]) !!}" title="Επεξεργασία" class="btn btn-warning btn-xs">
                                                                <i class="glyphicon glyphicon-edit"></i>
                                                            </a>

                                                            <a href="{!! route('Leitourgika::Placements::softDelete', [$placement->id]) !!}" title="Ανάκληση τοποθέτησης" class="btn btn-primary btn-xs">
                                                               <i class="fa fa-undo" aria-hidden="true"></i>
                                                            </a>

                                                            @if($placement->pending)
                                                                <a href="{!! route('Leitourgika::Placements::cancelPending', [$placement->id]) !!}" title="Ακύρωση Εκκρεμότητας" class="btn btn-info btn-xs">
                                                                   <i class="fa fa-legal" aria-hidden="true"></i>
                                                                </a>
                                                            @else
                                                                <a href="{!! route('Leitourgika::Placements::enablePending', [$placement->id]) !!}" title="Ενεργοποίηση Εκκρεμότητας" class="btn btn-info btn-xs">
                                                                   <i class="fa fa-legal" aria-hidden="true"></i>
                                                                </a>
                                                            @endif

                                                            <button type="button" title="Διαγραφή Τοποθέτησης" data-placement-id="{{$placement->id}}" data-click="delete" class="btn btn-danger btn-xs">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                    </td>
                                                @endif
                                        </tr>
                                    @endif
                                    <?php $index = $index + 1 ?>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" class="text-right"><h3 style="font-weight: bold;">Σύνολο Ωρών:</h3></td>
                                    <td colspan="2" class="text-left"><h3 style="padding-left: 15px;color: #0000ff;font-weight: bold">{!! $total !!}</h3></td>
                                    <td colspan="2">
                                        @if(Auth::user()->isRole('employess_personnel_department'))
                                                <a target="_blank"
                                                    href="{!! route('Leitourgika::Placements::teacherPDF', ['teacher_id' => $myschool->afm, 'year_name' => $current_year]) !!}"
                                                    class="btn btn-success btn-lg"
                                                >
                                                    Τοποθετήρια
                                                </a>
                                        @else
                                                <a target="_blank"
                                                    href="{!! route('Oikonomikou::Placements::teacherPDF', ['teacher_id' => $myschool->afm, 'year_name' => $current_year]) !!}"
                                                    class="btn btn-success btn-lg"
                                                >
                                                    Τοποθετήρια
                                                </a>
                                        @endif
                                    </td>
                                </tr>

                            </tfoot>
                        </table>
                    @endif
                </div>
            </div>

        </div>
    </div>
        
@stop

@section('scripts.footer')
        <script>
            $(document).ready(function() {
                $('button[data-click=delete]').on('click', swalAlertDelete);
            });

            function swalAlertDelete(e){
                e.preventDefault();

                var id = $(this).data("placement-id");

                alert("ID : "+ id)
                
                Swal.fire({
                    title: "Προσοχή!",
                    type: "danger",
                    text: "Η τοποθέτηση θα διαγραφεί οριστικά.",
                    showCancelButton: true,
                    cancelButtonText: 'Ακύρωση',
                    confirmButtonText:  "Διαγραφή",
                    confirmButtonColor: "#DD6B55",
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return fetch(`{{ url('/') }}/api/admin/placements/permanentDelete/${id}?api_token={{ Auth::user()->api_token }}`)
                        .then(response => {      
                            if (!response.ok) {        
                                throw new Error(response.statusText)
                            }                    
                            return response.json()
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Σφάλμα 432: ${error}`
                            )
                        })
                    },
                allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value) {
                        Swal.fire({
                            title : "",
                            text :  "Η τοποθέτηση διεγράφη.",
                            type : 'success'
                        });
                        setTimeout(function(){location.reload(true);}, 2000);

                    }
                });
            }
        </script>

@endsection


