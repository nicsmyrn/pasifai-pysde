@extends('app')

@section('title')
    Καθηγητές στη συγκεκριμένη πράξη
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" integrity="sha512-arEjGlJIdHpZzNfZD2IidQjDZ+QY9r4VFJIm2M/DhXLjvvPyXFj+cIotmo0DLgvL3/DOlIaEDwzEiClEPQaAFQ==" crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <style>
        .select-praxi{
            width: 100%;
        }
    </style>
@endsection

@section('content')

    <h1 class="text-center">
        Καθηγητές στην Πράξη
    </h1>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4">
            <label>
                Επέλεξε Πράξη από τη λίστα

                <select id="selectPraxi">
                    <option>Επέλεξε Πράξη</option>
                    @if(Auth::user()->isRole('pysde_secretary'))
                        @foreach($praxeis as $pr)
                            <option {!! Request::get('αριθμός') == $pr->decision_number?'selected':'' !!} value="{!! route('Leitourgika::Placements::placementsByPraxi', ['αριθμός'=>$pr->decision_number]) !!}">
                                {!! $pr->decision_number !!} / {!! $pr->decision_date !!}
                            </option>
                        @endforeach
                    @elseif(Auth::user()->isRole('oikonomika'))
                        @foreach($praxeis as $pr)
                            <option {!! Request::get('αριθμός') == $pr->decision_number?'selected':'' !!} value="{!! route('Oikonomikou::Placements::placementsByPraxi', ['αριθμός'=>$pr->decision_number]) !!}">
                                {!! $pr->decision_number !!} / {!! $pr->decision_date !!}
                            </option>
                        @endforeach
                    @elseif(Auth::user()->isRole('employess_personnel_department'))
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    @endif

                </select>
            </label>
        </div>
        <div class="col-md-6">
            @if($praxi != null)

                @include('errors.list')

                {!! Form::model($praxi, ['method'=>'PATCH', 'class'=>'form-horizontal', 'action'=>['\Pasifai\Pysde\controllers\PlacementsController@updatePraxi', $praxi->id]]) !!}
                    @include('pysde::leitourgika.placements._form_praxi',['submitButton' =>'Ενημέρωση'])
                {!! Form::close() !!}

            @endif  

        </div>
    </div>

    <div>

    </div>

        @if(Request::has('αριθμός'))
            @if(Auth::user()->isRole('pysde_secretary'))
                <a target="_blank" href="{!! route('Leitourgika::Placements::allByPraxi', Request::get('αριθμός')) !!}" class="btn btn-primary">
                    Εκτύπωση όλων των τοποθετήσεων
                </a>
            @elseif(Auth::user()->isRole('oikonomika'))
                <a target="_blank" href="{!! route('Oikonomikou::Placements::allByPraxi', Request::get('αριθμός')) !!}" class="btn btn-warning">
                    Εκτύπωση όλων των τοποθετήσεων
                </a>
            @elseif(Auth::user()->isRole('employess_personnel_department'))
                <h5>button...</h5>
            @endif

        @endif

    @include('pysde::leitourgika.placements.template_for_placements')

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js" integrity="sha512-fZy9udcMtCbrKvLIxWhOUaH6TZYddjizBEhESeTsv1lwzXgcR6ZalhWye+BlT/KQ0KIfyjiqwce7IKKtRH29hQ==" crossorigin="anonymous"></script>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
 
    <script type="text/javascript" charset="UTF-8">
        $.fn.datepicker.dates['gr'] = {
            days: ["Κυριακή", "Δευτέρα", "Τρίτη", "Τετάρτη", "Πέμπτη", "Παρασκευή", "Σάββατο", "Κυριακή"],
            daysShort: ["Κυρ", "Δευ", "Τρι", "Τετ", "Πεμ", "Παρ", "Σαβ", "Κυρ"],
            daysMin: ["Κυ", "Δε", "Τρ", "Τε", "Πε", "Πα", "Σα", "Κυ"],
            months: ["Ιανουάριος", "Φεβρούαριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"],
            monthsShort: ["Ιαν", "Φεβ", "Μαρ", "Απρ", "Μαι", "Ιου", "Ιουλ", "Αυγ", "Σεπ", "Οκτ", "Νοε", "Δεκ"],
            today: "Σήμερα",
            clear: "Καθάρισμα"
        };
    </script>

    <script>
        $(document).ready(function() {
            $('#selectPraxi').change(function(){
                var url = $(this).val();
                window.location = url;
            })
            .select2({
                theme: "classic",
                placeholder : 'Επέλεξε πράξη',
                language: {
                    noResults: function() {
                        return "Δεν υπάρχει αποτέλεσμα αναζήτησης";
                    }
                }
            });

            var table = $('#teachers').DataTable({
                    "order": [[ 0, "asc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [  ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
            
            var endDate = new Date();

            $('#dde_protocol_date').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                daysOfWeekDisabled: [0,6],
                language: 'gr'
            });  
            
        });
    </script>
@endsection


