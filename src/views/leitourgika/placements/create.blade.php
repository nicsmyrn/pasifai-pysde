@extends('app')

@section('header.style')
    <style>
        .loading{
            width: 80px;
            height: 80px;
        }
    </style>
@endsection

@section('loader')
    @include('vueloader')
@endsection

@section('content')
    <div id="placements">
        <h2 class="page-heading">
            ΝΕΕΣ Τοποθετήσεις
        </h2>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    
                    <div v-cloak v-if="loading" class="panel-body text-center">
                        <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                        <h5>παρακαλώ περιμένετε...</h5>
                    </div>
                    
                    <div v-cloak v-else class="panel-body">
                        <div class="form-group">
                            <div class="col-md-1">
                                <label for="decision_number" class="control-label">Πράξη Δντη<i class="fa fa-question"></i>
                                    <input for="dieuthidi" v-model="dieuthidi" type="checkbox"/>
                                </label>
                                <input v-model="praxi_number" type="text" class="form-control" placeholder="No"/>
                            </div>
                        </div>
 
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="decision_date" class="control-label">Ημερομηνία</label>
                                <date-picker v-model="praxi_date" 
                                            :clearable="datePickerClass.getClearable()" 
                                            :width="datePickerClass.getWidth()" 
                                            value-type="format" 
                                            :format="datePickerClass.getFormat()" 
                                            :lang="datePickerClass.getLang()" 
                                            :shortcuts="datePickerClass.getShortcuts()" 
                                            :first-day-of-week="datePickerClass.firstDayOfWeek()"
                                ></date-picker>     
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="klados" class="control-label">Κλάδος:</label>
                                <select @change="getTeachers" v-model="klados_name" class="form-control">
                                    <option></option>
                                    <option v-for="(klados, index) in kladoi" :value="index">@{{ index }} - @{{ klados }}</option>
                                </select>
                            </div>
                        </div>

                        <div v-show="displayTeachersList" class="form-group">
                            <div v-if="!teacherLoader" class="col-md-4">
                                <!-- <label for="teachers" class="control-label">
                                    Εκπαιδευτικοί:
                                </label> -->
                                <div class="checkbox">
                                    <label><input for="teachers" v-model="covid19" type="checkbox" true-value="yes" false-value="no"/>Υπολογίζονται στα Κενά - Πλεονάσματα</label>
                                </div>

                                <select @change="displayProfile" v-model="currentTeacher" class="form-control col-md-2">
                                        <option></option>
                                        <option v-for="teacher in teachers" v-bind:value="teacher">
                                            @{{ teacher.full_name }} [@{{ teacher.organiki }}]
                                        </option>
                                    </select>
                            </div>
                            <div v-else class="col-md-4">
                                <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
                            </div>
                        </div>

                        <div v-show="displaySubmitButton" class="form-group">
                            <div class="col-md-2">
                                <button @click="submitPlacements" class="btn btn-primary btn-lg">Αποθήκευση</button>
                            </div>
                        </div>

                    </div>
                </div>  
            </div>
            
        </div>

        <div class="row">
            <div v-cloak v-if="displayCurrentTeacherProfile" class="panel panel-default">
                <div class="panel-heading">
                    Καθηγητής: <a target="_blank" :href="currentTeacher.url"><b> @{{ currentTeacher.full_name }} του @{{ currentTeacher.middle_name }}</b></a>
                </div>
                <div class="panel-body">
                    <div class="col-md-3">
                        <teacher-profile-in-placement :current-teacher="currentTeacher"></teacher-profile-in-placement>
                    </div>
                    <div class="col-md-7">
                        <old-placements 
                            :dieuthidi="dieuthidi"
                            :topothetisis="currentTeacher.topothetisis"
                            :types="allPlacementsTypes"
                            :total-topothetisis="currentTeacher.totalTopothetisis"
                            :token="token"
                            :praxi="praxi_number"
                            :date="praxi_date"
                            v-on:recall-old-placement="oldPlacementRecalled"
                            v-on:delete-old-placement="oldPlacementDeleted"
                            v-on:delete-permanently-old-placement="oldPermanentPlacementDeleted"
                        ></old-placements>                        
                    </div>
                    <div class="col-md-2">
                        <new-placement
                            v-on:add-new-placement="addPlacement"
                            :types="allPlacementsTypes"
                            :token="token"
                            :nextday="current_data_plus_one"
                            :lastday="next_june_of_cuurent_year"
                        ></new-placement>
                    </div>
                </div>
            </div>
        </div>

        <div v-if="placements.length" class="row">
            <placements-for-insert-in-database 
                :placements="placements"
                :types="allPlacementsTypes"
                v-on:delete-placements-of-current-teacher="deleteCurrentTeacherPlacements"
            ></placements-for-insert-in-database>
        </div>


        <alert ref="alert"></alert>

    </div>
@stop

@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
        window.current_data_plus_one = "{{ \Carbon\Carbon::now()->addDay()->format('Y-m-d') }}"
        window.next_june_of_cuurent_year = "{{ \Carbon\Carbon::parse(($year). '-06-30')->format('Y-m-d') }}"
    </script>
    
    <script src="{{ mix('vendor/pysde/js/placements.js') }}"></script>
@endsection