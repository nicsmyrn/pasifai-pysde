<table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>A/A</th>
            <th>Ονοματεπώνυμο</th>
            <th>Πατρώνυμο</th>
            <th>Ειδικότητα</th>
            <th>Οργανική</th>
            <th>Ομάδα</th>
            <th>Υποχρεωτικό</th>
            <th>Μόρια/Σειρά</th>
            <th>Εντοπιότητα</th>
            <th>Συνυπηρέτηση</th>
            @if($key == 'Λειτ. Τοποθέτηση & Συμπλήρωση')
                <th>Λειτουργική Τοποθέτηση</th>
                <th>Διάθεση για Συμπλήρωση Ωραρίου</th>
            @elseif($key == 'Λειτ. Τοποθέτηση')
                <th>Λειτουργική Τοποθέτηση</th>
            @elseif($key == 'Συμπλήρωση Ωραρίου')
                <th>Διάθεση για Συμπλήρωση Ωραρίου</th>
            @elseif($key == 'Απόσπαση')
                <th>Απόσπαση</th>
            @elseif($key == 'Ανάκληση')
                <th>Ανάκληση</th>
            @elseif($key == 'Πθμια')

            @endif
            <th>Παρατηρήσεις</th>
        </tr>
    </thead>

    <tbody>
        @foreach($teachers as $index=>$teacher)
            <tr>
                <td>{!! ($index + 1) !!}</td>
                <td>{!! $teacher['full_name'] !!}</td>
                <td>{!! $teacher['middle_name'] !!}</td>
                <td>{!! $teacher['eidikotita'] !!}</td>
                <td>{!! $teacher['organiki'] !!}</td>
                <td>{!! $teacher['omada'] !!}</td>
                <td>{!! $teacher['ypoxreotiko'] !!}</td>
                <td>
                    @if($teacher['am'] == '')
                        {!! $teacher['position'] !!}
                    @else
                        @if($key == 'Απόσπαση' || $key == 'Λειτ. Τοποθέτηση')
                            {!! $teacher['moria_apospasis'] !!}
                        @else
                            {!! $teacher['moria'] !!}
                        @endif
                    @endif
                </td>
                <td>{!! $teacher['entopiotita'] !!}</td>
                <td>{!! $teacher['sinipiretisi'] !!}</td>
            </tr>
        @endforeach
    </tbody>
</table>