<table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>A/A</th>
            <th>AM</th>
            <th>Ονοματεπώνυμο</th>
            <th>Πατρώνυμο</th>
            <th>Αρ. Μητρώου</th>
            <th>Ειδικότητα</th>
            <th>Οργανική Θέση</th>
            <th>Μόρια/Σειρά</th>
            <th>Εντοπιότητα</th>
            <th>Συνυπηρέτηση</th>
            <th>ΝΕΑ ΟΡΓΑΝΙΚΗ ΘΕΣΗ</th>
            <th>Παρατηρήσεις</th>
        </tr>
    </thead>

    <tbody>
        @foreach($teachers as $index=>$teacher)
            <tr>
                <td>{!! ($index + 1) !!}</td>
                <td>{!! $teacher['am'] !!}</td>
                <td>{!! $teacher['full_name'] !!}</td>
                <td>{!! $teacher['middle_name'] !!}</td>
                <td>{!! $teacher['am'] !!}</td>
                <td>{!! $teacher['eidikotita'] !!}</td>
                <td>{!! $teacher['old_organiki'] !!}</td>
                <td>
                    {!! $teacher['moria'] !!}
                </td>
                <td>{!! $teacher['entopiotita'] !!}</td>
                <td>{!! $teacher['sinipiretisi'] !!}</td>
                <td>{!! $teacher['new_organiki'] !!}</td>
                <td>{!! $teacher['description'] !!}</td>
            </tr>
        @endforeach
    </tbody>
</table>