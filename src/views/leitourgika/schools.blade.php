@extends('app')

@section('header.style')
    <style>
        .loading{
            width: 80px;
            height: 80px;
        }
        .differentNumbers {
            background-color: #ff8566;
        }
        .buttons-margin{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .zero_div{
            color: red;
            text-align: center;
            padding-left: 10px;
        }

        .council-text{
            text-align: center;
            background-color: #787878;
            color: white;
            width: 50px;
        }

        .council-cell{
            text-align: center;
        }
        .pleonasma{
            color : #991f00;
            font-size: 12pt;
            text-align: center;
            font-weight: 800;
        }
        .elleima{
            color : #008000;
            font-size: 12pt;
            font-weight: 800;
            text-align: center;
        }
        .different-from-council{
            color: red;
        }

        .is_yperarithmos{
            background-color: #90EE90;
            font-weight: bold;
        }
        .checkbox-modal{
            
        }

        .transfer-all{
            color: green;
            cursor: pointer;
            font-size: 14pt;
        }

        .backward-global{
            font-size:15pt;
            color: #943126;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px;
        }

        .undo-global{
            color: #0407F9;
            font-size: 15pt;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px; 
        }

        .delete-button{
            color: #943126;
            font-size: 11pt;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px;
        }

        .transfer-to-school{
            color: red;
            font-size: 11pt;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px;
        }

        .style-undo{
            color: #0407F9;
            font-size: 11pt;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px;    
        }

        .checked{
            color: green;
            margin-left: 5px;
            margin-right: 5px;
        }       

        .failed{
            color: red;
            margin-left: 5px;
            margin-right: 5px; 
        } 

        .lock-button{
            color: #E59866;
            font-size: 11pt;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px;
        }
        .unlock-button{
            color: #48C9B0;
            font-size: 11pt;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px;
        }

        .teacher-types{
            width: 80px;
        }

        .teacher-types option{
            width: 120px;
        }

        .positive-cell {
            width : 100%;
            background: greenyellow;
            display: block;
            font-weight: bold;
        }

        .negative-cell{
            width : 100%;
            background: #ff4d4d;
            display: block;
            font-weight : bold;
        }
        .more-locked {
            font-weight : bold;
            color : yellow;
        }
        .more-unlocked {
            font-weight : bold;
            color : red;
        }

        .t-center{
            text-align: center;
        }

        .t-title{
            padding-left: 10px;
            padding-right: 10px;
        }
        .t-results{
            text-align: center;
            font-weight: bold;
            font-size : 14pt;
            color: blue;
        }

        .different-from-council{
            color: red;
        }
        .ok{
            color : green;
        }

        .a-row{
            background-color: #EAF2F8;
            padding: 4px;
        }
        .b-row{
            padding: 4px;
            background-color: #F5EEF8;
        }
        .c-row{
            padding: 4px;
            background-color: #FEF9E7;
        }
        .d-row{
            padding: 4px;
            background-color: #F2F4F4;
        }

        .main-table{
            border-collapse:collapse; 
            table-layout:fixed; 
            width:100%;
            margin:0 auto 0 auto;
        }
        .number-row{
            width: 68px;
            text-align: center;
        }

        .number-row2{
            width: 140px;
            text-align:center;
        }
        .actions{
            width: 100px;
            text-align: center;
        }
        .combo-box{
            font-weight: bold;
            color: #922B21;
            font-size: 20pt;
            height: 50px;
        }
        .select-style{
            width: 350px;
            margin-left: 20px;
            font-size: 12pt;
        }
        .new-style{
            background-color: #EAFAF1;
            color: #1E8449;
            font-weight: 800;
            text-align: center;
            font-size: 14pt;
        }

        tr.tr-council-school-difference:hover{
            font-weight: bold;
            background-color: #F2D7D5;
        }

        .td-school-council-diff{
            text-align: center;
        }

        .td-school-council-same{
            text-align: center;
        }
        tr.tr-council-school-difference:hover .td-school-council-diff{
            background-color: #F1C40F;
            font-weight: bold;
            font-size: 20pt;
        }
        tr.simple{
            padding-top: 15px;
            padding-bottom: 15px;
            font-size: 14pt;
        }
        tr.simple:hover{
            font-weight: bold;
        }
        .comments{
            color: black;
            font-size: 8pt;
            cursor: pointer;
        }

        .pe04-header{
            text-align: center;
            background-color: #D5D8DC;
            font-weight: bold;
            font-size: 15pt;
        }

        .t-results-success{
            background-color: #A9DFBF;
        }

        .t-results-failed{
            background-color: #F1948A;
        }
    </style>
@endsection

@section('loader')
    @include('vueloader')
@endsection

@section('content')
    <div id="leitourgika">
        <h2 class="page-heading">Λειτουργικά Κενά - Τοποθετήσεις Εκπαιδευτικών</h2>

        <div v-cloak class="row text-center">
            <div class="btn-group" role="group" aria-label="...">
                <button v-show="!updated" @click.prevent="createExcelforCouncil" type="submit" class="btn btn-success">Πίνακας Συμβουλίου</button>
                <button @click.prevent="createExcelforSchool" type="submit" class="btn btn-primary">Πίνακας Σχολικών Μονάδων</button>
            </div>
            <form class="form-inline">
                <div class="form-group">
                    <label>
                        Σχολείο: 
                    </label>
                    <input v-cloak type="hidden" v-model="school_id"/>
                    <select 
                        v-cloak 
                        :disabled="updated"
                        class="form-control combo-box" 
                        v-model="school_selected" 
                        @change="getSchoolTeachers(school_selected)"
                    >
                        <option v-cloak v-for="school in schools">
                            @{{ school.name }}
                        </option>
                    </select>
                </div>
                <!-- <div class="btn-group" role="group">
                    <button @click="confirmLoadingOrganika" class="btn btn-default">Μετάπτωση Κενά</button>
                    <button @click="confirmLoadingTeachers" class="btn btn-default">Μετάπτωση Καθηγητές</button>
                </div> -->
            </form>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    
                    <div v-cloak v-if="loading" class="panel-body text-center">
                        <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                        <h5>παρακαλώ περιμένετε...</h5>
                    </div>
                    
                    <div v-cloak v-else class="panel-body">
                            <div class="col-md-12">
                                <div v-cloak v-if="kena.length">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <table v-if="school_type === 'Γυμνάσιο'" class="table-pe04" border="1">
                                                <thead>
                                                    <tr>
                                                        <th class="pe04-header" colspan="5">
                                                            <span v-if="diffPe04 === diffPe04sch" class="checked"><i class="fa fa-check-circle"></i></span>
                                                            <span v-if="diffPe04 !== diffPe04sch" class="failed"><i class="fa fa-exclamation-triangle"></i></span>
                                                            Κλάδος ΠΕ04
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th class="t-center">Ανάγκες</th>
                                                        <th class="t-center">Διαθέσιμες</th>
                                                        <th class="t-center">R-Συμβουλίου</th>
                                                        <th class="t-center">R-Σχολείου</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="(eid04, index) in kladosPe04">
                                                        <td class="t-title">@{{ eid04.eidikotita_slug }}</td>
                                                        <td class="t-center">@{{ eid04.forseen }}</td>
                                                        <td class="t-center">@{{ eid04.available }}</td>
                                                        <td :class="{
                                                            't-results' : true,
                                                            't-results-success' : diffPe04 === diffPe04sch,
                                                            't-results-failed' : diffPe04 !== diffPe04sch,
                                                        }" v-if="index==0" v-bind:rowspan="kladosPe04.length">
                                                            @{{ diffPe04 }}
                                                        </td>
                                                        <td :class="{
                                                            't-results' : true,
                                                            't-results-success' : diffPe04 === diffPe04sch,
                                                            't-results-failed' : diffPe04 !== diffPe04sch,
                                                        }" v-if="index==0" v-bind:rowspan="kladosPe04.length">
                                                            @{{ diffPe04sch }} 
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-3">
                                            <table border="1">
                                                <thead>
                                                    <tr>
                                                        <th class="t-center">ΦΙΛΤΡΑ πίκακα</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td class="a-row"><label><input type="checkbox" v-model="acheck"> Εμφάνιση Αρ. Καθηγητών</label></td></tr>
                                                    <tr><td class="c-row"><label><input type="checkbox" v-model="bcheck"> Εμφάνιση Ωραρίου Εκπαιδευτικών</label></td></tr>
                                                    <!-- <tr><td class="c-row"><label><input type="checkbox" v-model="ccheck"> Εμφάνιση Λειτουργικού Κενού</label></td></tr> -->
                                                    <tr><td class="d-row"><label><input type="checkbox" v-model="dcheck"> Εμφάνιση Ενεργειών</label></td></tr>
                                                </tbody>
                                            </table>

                                            <div v-cloak v-if="updated" style="margin-top: 5px;" class="col-md-4">
                                                <button @click="saveChangesForEidikotites" class="btn btn-success btn-lg">Αποθήκευση αλλαγών</button>
                                            </div>
                
                                        </div>
                                        <div class="col-md-4">
                                            <table v-if="school_type === 'Γυμνάσιο'" class="table-pe04" border="1">
                                                <thead>
                                                    <tr>
                                                        <th class="pe04-header" colspan="5">
                                                            <span v-if="diffPe81 === diffPe81sch" class="checked"><i class="fa fa-check-circle"></i></span>
                                                            <span v-if="diffPe81 !== diffPe81sch" class="failed"><i class="fa fa-exclamation-triangle"></i></span>
                                                            ΤΕΧΝΟΛΟΓΙΑ
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th class="t-center">Ανάγκες</th>
                                                        <th class="t-center">Διαθέσιμες</th>
                                                        <th class="t-center">R-Συμβουλίου</th>
                                                        <th class="t-center">R-Σχολείου</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="(eid81, index) in kladosPe81">
                                                        <td class="t-title">@{{ eid81.eidikotita_slug }}</td>
                                                        <td class="t-center">@{{ eid81.forseen }}</td>
                                                        <td class="t-center">@{{ eid81.available }}</td>
                                                        <td :class="{
                                                            't-results' : true,
                                                            't-results-success' : diffPe81 === diffPe81sch,
                                                            't-results-failed' : diffPe81 !== diffPe81sch,
                                                        }" v-if="index==0" v-bind:rowspan="kladosPe81.length">
                                                            @{{ diffPe81 }}
                                                        </td>
                                                        <td :class="{
                                                            't-results' : true,
                                                            't-results-success' : diffPe81 === diffPe81sch,
                                                            't-results-failed' : diffPe81 !== diffPe81sch,
                                                        }" v-if="index==0" v-bind:rowspan="kladosPe81.length">
                                                            @{{ diffPe81sch }} 
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div style="margin-top: 5px;">
                                                <button @click="unLockAll" v-cloak v-if="locked_diff==='more_locked'" class="btn btn-primary">
                                                    <div>Ξεκλείδωμα όλων των Κενών</div>
                                                    <div>
                                                        <span class="more-locked" v-show="counter_unlocked > 0">Υπάρχουν @{{ counter_unlocked }} Ξεκλείδωτα</span>
                                                    </div>
                                                </button>
                                                <button @click="lockAll" v-cloak v-if="locked_diff==='more_unlocked'" class="btn btn-warning">
                                                    <div>Κλείδωμα όλων των Κενών</div>
                                                    <div>
                                                        <span class="more-unlocked" v-show="counter_locked > 0">Υπάρχουν @{{ counter_locked }} Κλειδωμένα</span>
                                                    </div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="main-table" border="1">
                                                <thead>
                                                    <tr>
                                                        <th class="t-center">
                                                            Ειδικότητες
                                                            <span v-if="notSynchronized > 0" @click="transferAll" :class="{
                                                                'transfer-all' : true
                                                            }">
                                                                <i class="fa fa-exchange"></i>
                                                            </span>
                                                        </th>
                                                        <th v-show="acheck" class="a-row number-row">Αρ. Καθηγ.</th>
                                                        <th class="number-row">Ώρες Διδασκαλ.</th>
                                                        <th v-show="bcheck" class="c-row number-row" align="center">Ωρ. Εκπαιδ.</th>
                                                        <th class="number-row">Διαφ.</th>
                                                        <!-- <th v-show="ccheck" class="c-row number-row" align="center">Λειτ. Κενό</th> -->
                                                        <th class="number-row">ΣΧΟΛΕΙΟ</th>
                                                        <th class="number-row2">Ημ. ενημέρωσης</th>
                                                        <th class="number-row">Από</th>
                                                        <th v-show="dcheck" class="d-row actions">
                                                            Ενέργειες
                                                            <i v-if="!allChecked" @click="globalTransfer()" class="fa fa-backward backward-global"></i>
                                                            <i v-if="allChecked"  @click="globalUndo()" class="fa fa-undo undo-global"></i>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-cloak v-for="eidikotita in kena" :class="{
                                                        'tr-council-school-difference' : eidikotita.sch_difference !== eidikotita.difference,
                                                        'simple' : true
                                                    }">
                                                        <td style="padding-left:3px">
                                                            <div v-cloak v-if="eidikotita.number_of_teachers > 0">
                                                                <a href="#" @click.prevent="modalTeachersByEidikotita(eidikotita.eidikotita_slug)">
                                                                    <span v-if="eidikotita.sch_difference === eidikotita.difference" class="checked"><i class="fa fa-check-circle"></i></span>
                                                                    <span v-if="eidikotita.sch_difference !== eidikotita.difference" class="failed"><i class="fa fa-exclamation-triangle"></i></span>
                                                                    @{{eidikotita.eidikotita_slug}} - @{{ eidikotita.eidikotita_name }}
                                                                </a>
                                                            </div>
                                                            <div v-cloak v-else>
                                                                <span v-if="eidikotita.sch_difference === eidikotita.difference" class="checked"><i class="fa fa-check-circle"></i></span>
                                                                <span v-if="eidikotita.sch_difference !== eidikotita.difference" class="failed"><i class="fa fa-exclamation-triangle"></i></span>                                                                @{{eidikotita.eidikotita_slug}} - @{{ eidikotita.eidikotita_name }}
                                                            </div>                                                
                                                        </td>
                                                        <td v-show="acheck" align="center">
                                                            <span v-show="eidikotita.number_of_teachers !== 0">
                                                                @{{ eidikotita.number_of_teachers }}
                                                            </span>
                                                        </td>
                                                        <td align="center">
                                                            <input @keyup="updateDifference(eidikotita)" type="text" v-model="eidikotita.forseen" maxlength="3" class="council-text"/>                                            
                                                        </td>
                                                        <td v-show="bcheck" align="center">
                                                            <span v-show="eidikotita.available !== 0">
                                                                @{{ eidikotita.available }}
                                                            </span>
                                                        </td>

                                                        <!-- <td v-cloak v-show="school_type !== 'Γυμνάσιο'" align="center">
                                                            <input @keyup="updateDifference(eidikotita)" type="text" v-model="eidikotita.project_hours" maxlength="3" class="council-text"/>                                            
                                                        </td> -->
                                                        <td align="center" :class="{
                                                            'td-school-council-diff' : eidikotita.sch_difference !== eidikotita.difference
                                                        }">
                                                            @{{ eidikotita.difference }}
                                                        </td>
                                                        <!-- <td v-show="ccheck" class="council-cell">
                                                            <div v-cloak v-if="eidikotita.difference > 11">
                                                                <span>@{{ eidikotita.difference }}</span>
                                                            </div>
                                                            <div v-cloak v-if="eidikotita.difference < -11">
                                                                <span>@{{ eidikotita.difference }}</span>
                                                            </div>
                                                        </td>   -->
                                                        <td :class="{
                                                            'pleonasma' : eidikotita.sch_difference > 0,
                                                            'elleima' : eidikotita.sch_difference < 0,
                                                            'td-school-council-diff' : eidikotita.sch_difference !== eidikotita.difference,
                                                            'td-school-council-same' : eidikotita.sch_difference === eidikotita.difference,
                                                        }">
                                                            <span v-show="eidikotita.sch_difference !== 0">
                                                                @{{ eidikotita.sch_difference }}
                                                            </span>
                                                            <span @click="showDescriptionModal(eidikotita.sch_description, `${school_selected} - ${eidikotita.eidikotita_name} ${eidikotita.eidikotita_slug}`)" v-if="eidikotita.sch_description !== null">
                                                                <i class="fa fa-list-alt comments"></i>
                                                            </span>
                                                        </td> 
                                                        <td> @{{ eidikotita.updated_at }}</td>
                                                        <td>@{{ eidikotita.user_id == 56 ? 'Πύσδε' : 'Σχολείο' }}</td>                                               
                                                        <td v-show="dcheck" align="center">
                                                            <span v-show="!eidikotita.alter_school">
                                                                <span v-if="eidikotita.sch_difference !== eidikotita.difference" @click="transferSchoolValue(eidikotita)">
                                                                    <i class="fa fa-backward delete-button"></i>
                                                                </span>
                                                                <span v-if="eidikotita.old_difference !== null && eidikotita.sch_difference === eidikotita.difference" @click="undoTransfer(eidikotita)">
                                                                    <i class="fa fa-undo style-undo"></i>
                                                                </span>                                                            
                                                            </span>

                                                            <span @click="transferCouncil(eidikotita)" v-show="eidikotita.alter_school">
                                                                <i class="fa fa-fast-forward transfer-to-school"></i>
                                                            </span>

                                                            <span v-if="eidikotita.sch_locked">
                                                                <span v-show="eidikotita.sch_difference !== eidikotita.difference">|</span>
                                                                <i @click="unlockEidikotita(eidikotita)" class="fa fa-lock lock-button"></i>
                                                            </span>
                                                            <span v-else>
                                                                <span v-show="eidikotita.sch_difference !== eidikotita.difference">|</span>
                                                                <i @click="lockEidikotita(eidikotita)" class="fa fa-unlock unlock-button"></i>
                                                            </span>
                                                            <span v-if="eidikotita.number_of_teachers <= 0">
                                                                |<i @click="deleleEidikotita(eidikotita)" class="fa fa-trash delete-button"></i>
                                                            </span>
                                                        </td> <!-- if teachers are null -->
                                                    </tr>
                                                </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td :colspan="colspan-3" class="new-style">
                                                                <label>
                                                                    Εισαγωγή Νέας Ειδικότητας:
                                                                    <select @change="focusHours" v-model="new_eidikotita" class="select-style">
                                                                        <option v-for="e in other_eidikotites" v-bind:value="e">
                                                                            @{{ e.eidikotita_slug }} @{{ e.eidikotita_name }}
                                                                        </option>
                                                                    </select>
                                                                </label>
                                                                <span v-if="new_forseen !== null && new_eidikotita !== null">
                                                                    <button @click="addNewEidikotita" class="btn btn-success">Προσθήκη...</button>
                                                                </span>
                                                            </td>
                                                            <td colspan="2" class="new-style">
                                                                <label>
                                                                    Διδακτικές Ώρες
                                                                    <input 
                                                                        @keyup="checkNewAvailable(new_forseen)" 
                                                                        v-model="new_forseen" 
                                                                        type="text" 
                                                                        size="4" 
                                                                        maxlength="3" 
                                                                        class="council-text"
                                                                        ref="neweidikotita"
                                                                        @keyup.enter="addNewEidikotita"
                                                                    />                                            
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div v-else>
                                    <div class="alert alert-warning">
                                        <strong>παρακαλώ</strong> επιλέξτε ένα Σχολείο από τη λίστα
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>  
            </div>
            
        </div>

        @include('pysde::leitourgika.modal_teachers')
        @include('pysde::leitourgika.modal_descriptions')

        <new-alert 
            ref="alert"
        ></new-alert>
    </div>
@stop

@section('scripts.footer')
    <script>
        window.school_name = "{{ $school->exists ? $school->name : 'null' }}";
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
        window.user_id = "{{ Auth::user()->id }} ";
    </script>
    
    <script src="{{ mix('vendor/pysde/js/leitourgika.js') }}"></script>
@endsection