@extends('app')

@section('header.style')
    <style>

        .custom-file-input.selected:lang(en)::after {
            content: "" !important;
        }
        .custom-file {
            overflow: hidden;
        }
        .custom-file-input {
            white-space: nowrap;
        }
        .loading{
            width: 80px;
            height: 80px;
        }
        .differentNumbers {
            background-color: #ff8566;
        }
        .buttons-margin{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .zero_div{
            color: red;
            text-align: center;
            padding-left: 10px;
        }

        .council-text{
            text-align: center;
            background-color: #787878;
            color: white;
        }

        .council-cell{
            text-align: center;
        }
        .pleonasma{
            color : #991f00;
            font-size: 12pt;
            font-weight: 800;
        }
        .elleima{
            color : #008000;
            font-size: 12pt;
            font-weight: 800;
        }
        .different-from-council{
            color: red;
        }
        .ok{
            color : green;
        }

        .ok-upload{
            color : green;
            font-size: 16pt;
        }

        .is_yperarithmos{
            background-color: #90EE90;
            font-weight: bold;
        }
        .checkbox-modal{
            
        }

        .delete-button{
            color: #943126;
            font-size: 11pt;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px;
        }

        .lock-button{
            color: #DF3A01;
            font-size: 11pt;
            margin-left: 5px;
            margin-right: 5px;
        }

        .positive-cell {
            width : 100%;
            background: greenyellow;
            display: block;
            font-weight: bold;
        }

        .negative-cell{
            width : 100%;
            background: #ff4d4d;
            display: block;
            font-weight : bold;
        }
        .more-locked {
            font-weight : bold;
            color : yellow;
        }
        .more-unlocked {
            font-weight : bold;
            color : red;
        }

        .not-belongs-to-school{
            background-color: #a6a6a6;
            color : white;
        }

    </style>
@endsection

@section('content')
    <div id="leitourgika_school">
        <h2 class="page-heading">Εκπαιδευτικοί τοποθετημένοι στη Σχολική Μονάδα</h2>

        <div class="row">
            <div class="col-md-12">
                <countdown 
                    v-if="set_countdown"
                    v-on:finished="countdown_finished"
                    :until="countdown_date">
                </countdown>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div 
                    class="alert alert-danger text-center" 
                    role="alert"
                >
                    <h4><strong>Προσοχή!</strong> </h4>
                    <p>
                        Για την αποθήκευση όλων των αλλαγών θα χρειαστεί να πατήσετε στο τέλος <strong>το κουμπί Επιβεβαίωση</strong>
                    </p>
                </div>

                <div v-cloak v-if="loading" class="panel-body text-center">
                    <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                    <h5>παρακαλώ περιμένετε...</h5>
                </div>
                
                <div v-cloak v-else class="panel-body">
                    <div v-cloak v-if="updated" class="row">
                        <div class="col-md-12 text-center">
                            <div v-if="loading_button">
                                <button class="btn btn-success btn-lg" disabled="disabled">
                                    <i class="fa fa-spinner fa-spin"></i>
                                    Επιβεβαίωση... 
                                </button>
                            </div>
                            <div v-else>
                                <button @click="saveTeachers" class="btn btn-success btn-lg">Επιβεβαίωση</button>
                            </div>
                        </div>
                    </div>

                    <div>
                        <!-- <button class="btn btn-default" v-if="global_file_upload == null" @click.prevent="clickTheFileElementGlobal()">Ανέβασμα Συγκεντρωτικής πράξης ανάληψης</button> -->
                        <input  class="custom-file-input" v-show="false" type="file" @change="prepareForAttachmentGlobal()" id="globalFile" ref="globalFile">
                        <button v-if="global_file_upload != null && !globalLoadingSpinner" @click.prevent="uploadGlobalAttach()">
                            <i class="fa fa-upload"></i>
                        </button>
                        <button v-if="globalLoadingSpinner">
                            <i class="fa fa-spinner fa-spin"></i>
                        </button>
                    </div>

                    <table slot="body" class="table table-bordered">
                        <thead>
                            <th align="center">A/A</th>
                            <th align="center">Επώνυμο</th>
                            <th align="center">Όνομα</th>
                            <th align="center">Ειδικότητα</th>
                            <th align="center">ΑΜ</th>
                            <th align="center">Υποχρ.</th>
                            <th align="center">Μείωση Ωραρίου</th>
                            <th align="center">Κατάσταση</th>
                            <th align="center">Διδ. (ώρες)</th>
                            <th align="center">Διάθεση αλλού</th>
                            <th align="center">Διαφορά</th>
                            <th align="center">Λήξη</th>
                            <th align="center">Πράξη ανάληψης</th>
                        </thead>
                        <tbody>
                            <tr v-for="(teacher, index) in teachers" v-bind:class="{
                                'not-belongs-to-school'      : !isEditable(teacher)
                            }">
                                <td>@{{ index + 1 }}</td>
                                <td>
                                    <span v-if="teacher.pivot.locked">
                                        <i class="fa fa-lock lock-button"></i>
                                    </span>
                                    <span v-if="teacher.pivot.url !== null">
                                        <a target="_blank" :href="teacher.pivot.url">
                                            @{{ teacher.last_name }}
                                        </a>
                                    </span>
                                    <span v-else>
                                        @{{ teacher.last_name }}
                                    </span>
                                </td>
                                <td>@{{ teacher.first_name }}</td>
                                <td>@{{ teacher.eidikotita }}</td>
                                <td align="center">@{{ teacher.am }}</td>
                                <td align="center">
                                    @{{ calculateSumHours(teacher) }}
                                </td>
                                <td align="center">
                                    <div v-if="teacher.pivot.locked">
                                        @{{ teacher.meiwsi }} 
                                    </div>
                                    <div v-else>
                                        <div v-if="isEditable(teacher)">
                                            <div v-if="isOrganika(teacher)">
                                                <input @keyup="updateMeiwsiHours(teacher)" type="text" v-model="teacher.meiwsi" size="3" maxlength="2" class="council-text"/>                                            
                                            </div>
                                            <div v-else>
                                                <div v-if="isSchoolAdmin(teacher)">
                                                    <input @keyup="updateMeiwsiHours(teacher)" type="text" v-model="teacher.meiwsi" size="3" maxlength="2" class="council-text"/>                                            
                                                </div>
                                            </div>
                                        </div>
                                        <div v-else>
                                            @{{ teacher.meiwsi }}
                                        </div>
                                    </div>
                                </td>
                                <td align="center">
                                    <div v-if="teacher.pivot.locked">
                                        @{{ all_types[teacher.pivot.teacher_type] }}
                                    </div>

                                    <div v-else>
                                        <div v-if="isEditable(teacher) && can_edit_hours">
                                            <div v-if="isSchoolAdmin(teacher)">
                                                @{{ all_types[teacher.pivot.teacher_type] }}
                                            </div>
                                            <div v-else>
                                                <select @change="updateTeacher(teacher)" class="teacher-types" v-model="teacher.pivot.teacher_type">
                                                    <option disabled value="">Επέλεξε:</option>
                                                    <option v-bind:value="index" v-for="(type, index) in editable_types">
                                                        @{{ type }}
                                                    </option>
                                                </select>
                                            </div>

                                        </div>
                                        <div v-else>
                                            <span>      
                                                @{{ all_types[teacher.pivot.teacher_type] }}
                                            </span> 
                                        </div>
                                    </div>
                                </td>
                                <td align="center">
                                    <div v-if="teacher.pivot.locked">
                                        @{{ teacher.pivot.hours }} 
                                    </div>
                                    <div v-else>
                                        <div v-if="isEditable(teacher)">
                                            <div v-if="isOrganika(teacher)">
                                                <input @keyup="updateDifferenceHours(teacher)" type="text" v-model="teacher.pivot.hours" size="3" maxlength="2" class="council-text"/>                                            
                                            </div>
                                            <div v-else>
                                                <div v-if="isSchoolAdmin(teacher)">
                                                    <input @keyup="updateDifferenceHours(teacher)" type="text" v-model="teacher.pivot.hours" size="3" maxlength="2" class="council-text"/>                                            
                                                </div>
                                            </div>
                                        </div>
                                        <div v-else>
                                            @{{ teacher.pivot.hours }}
                                        </div>
                                    </div>
                                </td>

                                <td align="center">
                                    <span v-if="teacher.sum_other_topothetisis !== 0">
                                        @{{ teacher.sum_other_topothetisis }}
                                    </span>
                                </td>

                                <td align="center">
                                    <div v-if="isOrganika(teacher) || isSchoolAdmin(teacher)">
                                        <span :class="{
                                            'ok' : calculateDifference(teacher) <= 0,
                                            'different-from-council' : calculateDifference(teacher) > 0
                                        }">
                                            <span v-if="calculateDifference(teacher) === 0">
                                                <i class="fa fa-check"></i>
                                            </span>

                                            <span v-else>
                                                <span v-if="calculateDifference(teacher) > 0">
                                                    @{{ calculateDifference(teacher) }} [πλεόνασμα]                                                    
                                                </span>

                                                <span v-else>
                                                    @{{ - calculateDifference(teacher) }} [υπερωρία]                                                    
                                                </span>
                                            </span>
                                        </span>
                                    </div>
                                </td>

                                <td>
                                    <div v-if="teacher.pivot.locked">
                                        @{{ teacher.pivot.date_ends }}
                                    </div>

                                    <div v-else>
                                        <div v-if="isEditable(teacher)">
                                            <div v-if="isOrganika(teacher)">
                                                
                                            </div>

                                            <div v-if="isSchoolAdmin(teacher)">
                                                @{{ teacher.pivot.date_ends }}
                                            </div>

                                            <div v-if="!isOrganika(teacher) && !isSchoolAdmin(teacher) && can_edit_hours">
                                                <date-picker v-model="teacher.pivot.date_ends" 
                                                    :default-value="datePickerClass.defaultValue()" 
                                                    :clearable="datePickerClass.getClearable()" 
                                                    :width="datePickerClass.getWidth()" 
                                                    value-type="format" 
                                                    :format="datePickerClass.getFormat()" 
                                                    :lang="datePickerClass.getLang()"
                                                    :shortcuts="datePickerClass.getShortcuts()" 
                                                    :first-day-of-week="datePickerClass.firstDayOfWeek()"
                                                    @change="isUpdated(teacher)"
                                                ></date-picker>    
                                            </div> 
                                        </div>      
                                        <div v-else>
                                            @{{ teacher.pivot.date_ends }}
                                        </div>
                                    </div>
                                </td>

                                <td align="center">
                                    <div v-if="teacher.praxi == null">
                                        <!-- THIS CODE IS THE SAME -->
                                        <div v-if="list_of_types.includes(teacher.pivot.teacher_type)">
                                            <div class="ok-upload" v-if="teacher.file_uploaded">
                                                <span>
                                                    <i class="fa fa-check-circle"></i>
                                                </span>
                                            </div>

                                            <div v-else>
                                                <form>
                                                    <button class="btn btn-default" v-if="teacher.file_upload == null" @click.prevent="clickTheFileElement(teacher)">Επιλογή Αρχείου</button>
                                                    <input  class="custom-file-input" v-show="false" type="file" @change="prepareForAttachment(teacher)" :id="'file_of_'+teacher.afm" :ref="teacher.afm">
                                                    <button v-if="teacher.file_upload != null && !teacher.loadingSpinner" @click.prevent="uploadTeacherAttach(teacher)">
                                                        <i class="fa fa-upload"></i>
                                                    </button>
                                                    <button v-if="teacher.file_upload != null && !teacher.loadingSpinner" @click.prevent="resetUpload(teacher)">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    <button v-if="teacher.loadingSpinner">
                                                        <i class="fa fa-spinner fa-spin"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>     
                                    </div>

                                    <div v-else>
                                        <div v-if="!teacher.praxi.pivot.uploaded">
                                            <!-- THIS CODE IS THE SAME -->
                                            <div v-if="list_of_types.includes(teacher.pivot.teacher_type)">
                                                <div class="ok-upload" v-if="teacher.file_uploaded">
                                                    <span>
                                                        <i class="fa fa-check-circle"></i>
                                                    </span>
                                                </div>

                                                <div v-else>
                                                    <form>
                                                        <button class="btn btn-default" v-if="teacher.file_upload == null" @click.prevent="clickTheFileElement(teacher)">Επιλογή Αρχείου</button>
                                                        <input  class="custom-file-input" v-show="false" type="file" @change="prepareForAttachment(teacher)" :id="'file_of_'+teacher.afm" :ref="teacher.afm">
                                                        <button v-if="teacher.file_upload != null && !teacher.loadingSpinner" @click.prevent="uploadTeacherAttach(teacher)">
                                                            <i class="fa fa-upload"></i>
                                                        </button>
                                                        <button v-if="teacher.file_upload != null && !teacher.loadingSpinner" @click.prevent="resetUpload(teacher)">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                        <button v-if="teacher.loadingSpinner">
                                                            <i class="fa fa-spinner fa-spin"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>     
                                        </div>

                                        <div v-else class="ok-upload">
                                            <span>
                                                <i class="fa fa-check-circle"></i>
                                            </span>
                                        </div>
                                    </div>
                      
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>

        <div v-cloak v-if="updated" class="row">
            <div class="col-md-12 text-center">
                <div v-if="loading_button">
                    <button class="btn btn-success btn-lg" disabled="disabled">
                        <i class="fa fa-spinner fa-spin"></i>
                        Επιβεβαίωση... 
                    </button>
                </div>
                <div v-else>
                    <button @click="saveTeachers" class="btn btn-success btn-lg">Επιβεβαίωση</button>
                </div>
            </div>
        </div>

        <alert ref="alert"></alert>

    </div>
@stop

@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
        window.countdown_date = "{{ config('requests.countdown.count_down_for_schools_leitourgika') }}";
        window.set_countdown = "{{ config('requests.countdown.set_countdown') }}";
        window.can_edit_hours = "{{ config('requests.can_edit_hours') }}";
    </script>
    
    <script src="{{ mix('vendor/pysde/js/leitourgika_for_school.js') }}"></script>
@endsection