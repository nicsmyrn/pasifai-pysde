@extends('app')

@section('header.style')
    <style>
        .loading{
            width: 80px;
            height: 80px;
        }
        .differentNumbers {
            background-color: #ff8566;
        }
        .buttons-margin{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .zero_div{
            color: red;
            text-align: center;
            padding-left: 10px;
        }

        .council-text{
            text-align: center;
            background-color: #787878;
            color: white;
        }

        .council-cell{
            text-align: center;
        }
        .pleonasma{
            color : #991f00;
            font-size: 12pt;
            font-weight: 800;
        }
        .elleima{
            color : #008000;
            font-size: 12pt;
            font-weight: 800;
        }
        .different-from-council{
            color: red;
        }
        .ok{
            color : green;
        }

        .different-from-council-school{
            color: #990000;
            background-color:#ff8566;
            font-weight: bold;
        }
        .ok-school{
            font-weight: bold;
            color : #004d00;
            background-color: #90EE90;
        }

        .is_yperarithmos{
            background-color: #90EE90;
            font-weight: bold;
        }
        .checkbox-modal{
            
        }

        .delete-button{
            color: #943126;
            font-size: 11pt;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px;
        }

        .lock-button{
            color: #DF3A01;
            font-size: 11pt;
            margin-left: 5px;
            margin-right: 5px;
        }

        .positive-cell {
            width : 100%;
            background: greenyellow;
            display: block;
            font-weight: bold;
        }

        .negative-cell{
            width : 100%;
            background: #ff4d4d;
            display: block;
            font-weight : bold;
        }
        .more-locked {
            font-weight : bold;
            color : yellow;
        }
        .more-unlocked {
            font-weight : bold;
            color : red;
        }

        .difference-school{
            background-color: #ffffcc;
        }

        .small-text-area {
            height: 3em;
            width: 100%;
            padding: 1px;
            resize: none;
        }

        .large-text-area {
            height: 14em;
            width: 100%;
            padding: 3px;   
        }

        .div-text-area{
            white-space: nowrap; 
            overflow: hidden;
            text-overflow: ellipsis;
            width: 50%;
            padding-left: 5px;
        }



    </style>
@endsection

@section('content')
    <div id="leitourgika_school">
        <h2 class="page-heading">Ειδικότητες Λειτουργικά Κενά - Πλεονάσματα</h2>

        <countdown 
            v-if="set_countdown_eidikotites"
            v-on:finished="countdown_finished_eidikotites"
            :until="countdown_date_eidikotites">
        </countdown>

        <div class="row">

            <div class="col-md-4"></div>
            <div class="col-md-2">
                <!-- <button class="btn btn-warning btn-lg" @click="printPage">Εκτύπωση</button> -->
            </div>
            <div class="col-md-2">
                <div v-cloak v-if="updated">
                    <div v-if="loading_button">
                            <button class="btn btn-success btn-lg" disabled="disabled">
                                <i class="fa fa-spinner fa-spin"></i>
                                Επιβεβαίωση...
                            </button>
                    </div>
                    <div v-else class="col-md-12 text-center">
                        <button @click="saveEidikotites" class="btn btn-success btn-lg">Επιβεβαίωση</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
  
            <br>

            <div 
                class="alert alert-danger text-center" 
                role="alert"
            >
                <h4><strong>Προσοχή!</strong> </h4>
                <p>
                    Για την αποθήκευση κενού/πλεονάσματος ή παρατήρησης <strong>σε κάθε ειδικότητα (γραμμή)</strong>
                    θα χρειαστεί να πατήσετε <strong>στο πληκτρολόγιο το πλήκτρο Enter ή  το πλήκτρο TAB</strong>.
                </p>
            </div>

            <div id="eidikotites_table" class="col-md-12">
                <div class="panel panel-primary">
                    
                    <div v-cloak class="panel-heading text-center">
                        @{{ school_name }} - τελευταία ενημέρωση: @{{ last_update }}
                    </div>

                    <div v-cloak v-if="loading" class="panel-body text-center">
                        <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                        <h5>παρακαλώ περιμένετε...</h5>
                    </div>
                    
                    <div v-cloak v-else class="panel-body">
                        <table slot="body" class="table table-bordered" id="table_eidikotites">                                
                            <thead>
                                <tr>
                                    <th align="center">Ειδικότητα</th>
                                    <!-- <th align="center">Διδακτικές Ώρες</th> -->
                                    <!-- <th align="center">Ωράριο Εκπαιδευτικών</th> -->
                                    <th align="center">Εικόνα Συμβουλίου</th>
                                    <th align="center">ΚΕΝΟ/ΠΛΕΟΝΑΣΜΑ</th>
                                    <th align="center">Παρατηρήσεις</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(eidikotita, index) in eidikotites">
                                    <td style="width: 30%;">
                                        <span v-if="eidikotita.pivot.locked">
                                            <i class="fa fa-lock lock-button"></i>
                                        </span>
                                        <span v-else>
                                            <span v-if="eidikotita.pivot.number_of_teachers === 0">
                                                <i @click="deleteEidikotita(eidikotita, index)" class="fa fa-trash delete-button"></i>
                                            </span>
                                        </span>
                                        @{{ eidikotita.eidikotita_slug }} @{{ eidikotita.eidikotita_name }}
                                    </td>
                                    <!-- <td align="center">
                                        <span v-if="eidikotita.pivot.locked">
                                            @{{ eidikotita.pivot.forseen }}
                                        </span>

                                        <span v-if="!eidikotita.pivot.locked && can_edit_hours_eidikotites">
                                            <input @keyup="updateDifferenceHours(eidikotita)" type="text" v-model="eidikotita.pivot.forseen" size="4" maxlength="3" class="council-text"/>                                            
                                        </span>
                                    </td> -->
                                    <!-- <td align="center">
                                        @{{ eidikotita.pivot.available }}
                                    </td> -->
                                    <td  style="width: 10%;" align="center">
                                        <span :class="{
                                            'different-from-council' : calculateDifference(eidikotita) > 0,
                                            'ok'                     : calculateDifference(eidikotita) <= 0
                                        }">
                                            @{{ calculateDifference(eidikotita) }}
                                        </span>
                                    </td>
                                    <td style="width: 10%;" align="center"  :class="{
                                            'different-from-council' : eidikotita.pivot.sch_difference > 0,
                                            'ok'                     : eidikotita.pivot.sch_difference <= 0,
                                            'difference-school'      : eidikotita.pivot.sch_difference != eidikotita.pivot.difference
                                        }">
                                        <span v-if="!eidikotita.pivot.sch_locked && school_can_edit_hours_eidikotites === 'can_edit'">
                                            <input 
                                                @keyup.enter.prevent="schoolUpdateDifferenceHours(eidikotita)" 
                                                @keydown.tab="schoolUpdateDifferenceHours(eidikotita)" 
                                                type="text" 
                                                v-model="eidikotita.pivot.sch_difference" 
                                                size="5" 
                                                maxlength="4" 
                                                :class="{
                                                    'council-text'                  : true,
                                                    'different-from-council-school' : eidikotita.pivot.sch_difference > 0,
                                                    'ok-school'                     : eidikotita.pivot.sch_difference < 0
                                                }"
                                            />                                            
                                        </span>
                                        <span v-else>
                                            @{{ eidikotita.pivot.sch_difference }} <i class="fa fa-lock lock-button"></i>    
                                        </span>
                                    </td>
                                    <td style="width: 50%;">
                                        <span v-if="!eidikotita.pivot.sch_locked && school_can_edit_hours_eidikotites === 'can_edit'">
                                            <textarea
                                                v-if="eidikotita.pivot.sch_difference != eidikotita.pivot.difference"
                                                v-model="eidikotita.pivot.sch_description"
                                                @keyup.enter.prevent="schoolUpdateDifferenceHours(eidikotita)" 
                                                @keydown.tab="schoolUpdateDifferenceHours(eidikotita)" 
                                                :class="{
                                                    'small-text-area'       : true   
                                                }"
                                            ></textarea>
                                        </span>
                                        <span v-else>
                                            @{{ substr(eidikotita.pivot.sch_description) }}
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot v-if="has_unlocked">
                                <tr>
                                    <td colspan="4" align="center">
                                        Εισαγωγή Νέας Ειδικότητας
                                        <span v-if="new_forseen !== null && new_eidikotita !== null">
                                            <button @click="addNewEidikotita" class="btn btn-success">Προσθήκη...</button>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <select v-model="new_eidikotita">
                                            <option v-for="e in other_eidikotites" v-bind:value="e">
                                                @{{ e.eidikotita_slug }} @{{ e.eidikotita_name }}
                                            </option>
                                        </select>
                                    </td>
                                    <td>
                                        Διδακτικές Ώρες
                                        <input @keyup="checkNewAvailable(new_forseen)" type="text" v-model="new_forseen" size="4" maxlength="3" class="council-text"/>                                            
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>  
            </div>
            
        </div>

        <div v-cloak v-if="updated" class="row">
            <div v-if="loading_button" class="col-md-12 text-center">
                    <button class="btn btn-success btn-lg" disabled="disabled">
                        <i class="fa fa-spinner fa-spin"></i>
                        Επιβεβαίωση...
                    </button>
            </div>
            <div v-else class="col-md-12 text-center">
                <button @click="saveEidikotites" class="btn btn-success btn-lg">Επιβεβαίωση</button>
            </div>
        </div>

        <!-- <alert ref="alert"></alert> -->

        <new-alert 
            ref="alert"
        ></new-alert>

    </div>
@stop

@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
        window.countdown_date_eidikotites = "{{ config('requests.countdown.count_down_for_schools_eidikotites') }}";
        window.set_countdown_eidikotites  = "{{ config('requests.countdown.set_countdown_eidikotites') }}";
        window.can_edit_hours_eidikotites = "{{ config('requests.can_edit_hours_eidikotites') }}";
        window.school_can_edit_hours_eidikotites = "{{ config('requests.school_can_edit_hours_eidikotites') }}";
    </script>
    
    <script src="{{ mix('vendor/pysde/js/leitourgika_for_school_eidikotites.js') }}"></script>
@endsection