@extends('app')

@section('header.style')
    <style>
        .loading{
            width: 80px;
            height: 80px;
        }
        .differentNumbers {
            background-color: #ff8566;
        }
        .buttons-margin{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .zero_div{
            color: red;
            text-align: center;
            padding-left: 10px;
        }

        .council-text{
            text-align: center;
            background-color: #787878;
            color: white;
            width: 100%;
        }

        .council-cell{
            text-align: center;
        }
        .pleonasma{
            color : #991f00;
            font-size: 12pt;
            font-weight: 800;
        }
        .elleima{
            color : #008000;
            font-size: 12pt;
            font-weight: 800;
        }
        .different-from-council{
            color: red;
        }

        .is_yperarithmos{
            background-color: #90EE90;
            font-weight: bold;
        }
        .checkbox-modal{
            
        }

        .table-topothetisis {
            width: 100%
        }

        .cell-topothetisis{

        }

        .zero-topothetisis{
            background : #ffb3b3;
            color : grey;
            font-size: 11pt;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
        }
        .sub-table-header{
            background: #E7E7E7;
            text-align: center;
            font-size: 8pt;
        }
        .sub-table-cell-text{
            font-weight: bold;
            text-align: left;
            padding-left: 10px;
        }
        .sub-table-cell-number{
            font-weight: bold;
            text-align: center;
            color: blue;
        }
        .table-cell-number{
            vertical-align: middle;
            text-align: center;
        }

        #teachers-table tr:hover{
            background: #FEED9C;
        }

        #teachers-table tr:hover .zero-topothetisis{
            background : #ff4d4d;
            color : yellow;
            font-size: 11pt;
            font-weight: bold;
            text-align: center;
            font-family: Arial, Helvetica, sans-serif;
        }

        #teachers-table tr:hover .sub-table-header{
            background: #DAD3B1;
            font-size:8pt;
            font-weight: bold;
        }
        #teachers-table tr:hover .table-cell-number{
            font-weight: bold;
        }
        .pleonasma-cell{
            word-wrap:break-word;
            color: red;
            font-size: 14pt;
            padding-left: 15px;
            padding-right: 15px;
        }
        
        .yperwria-cell{
            word-wrap:break-word;
            color: green;
            padding-left: 15px;
            padding-right: 15px;
            font-size: 14pt;
        }

        .yperwria-cell-description{
            font-size: 9pt !important;
        }

        .cell-header{
            text-align: center;
        }

        .ok{
            color : green;
            padding-left: 15px;
            padding-right: 15px;
            font-size: 14pt;
        }
    </style>
@endsection

@section('content')
    <div id="leitourgika">
        <h2 class="page-heading">Λειτουργικά Κενά - Τοποθετήσεις Εκπαιδευτικών ανά κλάδο.</h2>

        <div class="row">
            <div class="col-md-4 col-md-offset-1">
                <div v-if="!allEidikotites" class="form-group">
                    <label>
                        Ειδικότητα: 
                    </label>
                    <select @change="getTeachers" v-cloak class="form-control" v-model="eidikotita_slug">
                        <option v-cloak v-for="eidikotita in eidikotites" v-bind:value="eidikotita.eidikotita_slug">
                            @{{ eidikotita.eidikotita_slug }} - @{{ eidikotita.eidikotita_name }}
                        </option>
                    </select>
                </div>

                <div class="checkbox">
                    <label>
                        <input @change="getTeachersForAllEidikotites" type="checkbox" v-model="allEidikotites"> ΟΛΕΣ ΟΙ ΕΙΔΙΚΟΤΗΤΕΣ
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div v-if="eidikotita_slug !== null || allEidikotites">
                    <div class="checkbox">
                        <label>
                            Σχολείο: 
                        </label>
                        <select v-cloak class="form-control" v-model="school_id">
                            <option disabled> επέλεξε Σχολική Μονάδα</option>
                            <option value="null">ΟΛΑ τα Σχολεία</option>
                            <option v-cloak v-for="school in schools" v-bind:value="school.identifier">
                                @{{ school.name }}
                            </option>
                        </select>
                    </div>
                    <div v-show="school_id === null || school_id === 'null'">
                        <div class="checkbox">
                            <label><input type="checkbox" v-model="without_topothetisi">χωρίς τοποθέτηση</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" v-model="pleonazoun">πλεονάζουν</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" v-model="yperoria">Υπερωρίες</label>
                        </div>
                    </div>
 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">                    
                    <div v-cloak v-if="loading" class="panel-body text-center">
                        <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                        <h5>παρακαλώ περιμένετε...</h5>
                    </div>
                    
                    <div v-cloak v-else>
                        <div v-cloak v-if="teachers.length">
                            <table id="teachers-table" border="1">
                                <thead>
                                    <tr>
                                        <th width="5%" class="cell-header">A/A</th>
                                        <th width="15%" class="cell-header">Επώνυμο</th>
                                        <th width="10%" class="cell-header">Όνομα</th>
                                        <th v-if="allEidikotites" class="cell-header">Οργανική</th>
                                        <th width="5%" class="cell-header">ΑΜ</th>
                                        <th width="5%" class="cell-header">Υπ. Ωρ.</th>
                                        <th width="10%" class="cell-header">Οργανική</th>
                                        <th width="40%" class="cell-header">Τοποθετήσεις</th>
                                        <th width="5%" class="cell-header">Διαφορά</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-cloak v-for="(teacher,index) in filteredTeachers">
                                        <td style="text-align:center">
                                            @{{ (index + 1) }}
                                        </td>
                                        <td style="padding-left:3px">
                                            @{{ teacher.last_name }}
                                        </td>
                                        <td style="padding-left:3px">
                                            @{{ teacher.first_name }}
                                        </td>
                                        <td v-if="allEidikotites" text-align="center">
                                            @{{ teacher.eidikotita_name }}
                                        </td>
                                        <td align="center">
                                            @{{ teacher.am }}
                                        </td>
                                        <td align="center">
                                            @{{ teacher.ypoxreotiko }}
                                        </td>
                                        <td style="padding-left:5px; padding-right: 5px;">
                                            <a :href="teacher.url_organikis">@{{ teacher.organiki }}</a>
                                        </td>
                                        <td v-if="teacher.topothetisis.length || teacher.special_topothetisis.length">
                                                <table class="table-topothetisis" border=1>
                                                    <thead>
                                                        <tr>
                                                            <th class="sub-table-header">Είδος</th>
                                                            <th class="sub-table-header">Σχολική Μονάδα</th>
                                                            <th class="sub-table-header">Ωράριο</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="topothetisi in teacher.topothetisis">
                                                            <td class="sub-table-cell-text" width="20%">@{{ teacher_types[topothetisi.teacher_type] }} </td>
                                                            <td class="sub-table-cell-text" width="60%">
                                                                <span v-if="teacher_types[topothetisi.teacher_type] === 'Οργανικά'"></span>
                                                                <span v-else>
                                                                    <a :href="topothetisi.school_url">@{{ topothetisi.name }}</a>
                                                                </span>
                                                            </td>
                                                            <td class="sub-table-cell-number" width="10%">@{{ topothetisi.sum_hours }} </td>
                                                        </tr>
                                                        <tr v-for="special in teacher.special_topothetisis">
                                                            <td class="sub-table-cell-text" width="20%">@{{ teacher_types[special.pivot.teacher_type] }} </td>
                                                            <td class="sub-table-cell-text" width="60%">
                                                                <a :href="special.pivot.url">@{{ special.name }}</a>
                                                            </td>
                                                            <td class="sub-table-cell-number" width="10%">
                                                                @{{ special.pivot.hours }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                        </td>

                                        <td class="zero-topothetisis" v-else>
                                            δεν υπάρχουν τοποθετήσεις
                                        </td>

                                        </td>
                                        <td class="table-cell-number">
                                            <span class="ok" v-if="(teacher.ypoxreotiko - teacher.sum === 0)">
                                                <i class="fa fa-check"></i>
                                            </span>

                                            <span v-if="(teacher.ypoxreotiko - teacher.sum) > 0">
                                                <span class="pleonasma-cell">+@{{ (teacher.ypoxreotiko - teacher.sum) }}</span>
                                            </span>

                                            <span v-if="(teacher.ypoxreotiko - teacher.sum) < 0">
                                                <span class="yperwria-cell">@{{ (teacher.ypoxreotiko - teacher.sum) }}</span> <span class="yperwria-cell-description">Υπερωρία</span>
                                            </span>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div v-else>
                            <div class="col-md-6">
                                <div class="alert alert-warning text-center">
                                        <strong>παρακαλώ</strong> επιλέξτε μία Ειδικότητα από τη λίστα
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            
        </div>

    </div>
@stop

@section('scripts.footer')
    <script>
        window.school_name = "{{ $school->exists ? $school->name : 'null' }}";
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
        window.user_id = "{{ Auth::user()->id }} ";
    </script>
    
    <script src="{{ mix('vendor/pysde/js/leitourgika_teachers.js') }}"></script>
@endsection


