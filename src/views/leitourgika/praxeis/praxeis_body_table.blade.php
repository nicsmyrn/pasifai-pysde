    <h1 class="page-heading">
        Πράξεις Ανάληψης
    </h1>

    @if($list_of_teachers->isEmpty())
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει κανένας καθηγητής</div>
                </div>
            </div>
        </div>
    @else
        <table id="teachers" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr class="active">
                    <th class="text-center">Ονοματεπώνυμο</th>
                    <th class="text-center">ΑΦΜ</th>
                    <th class="text-center">Σχολική Μονάδα</th>
                    <th class="text-center">Κλάδος</th>
                    <th class="text-center">D</th>
                </tr>
            </thead>

            <tbody>
                @foreach($list_of_teachers as $teacher)
                    <tr>
                        <td>
                            {!! $teacher['full_name'] !!}
                        </td>
                        <td>{!! $teacher['afm'] !!}</td>
                        <td>{!! $teacher['school'] !!}</td>
                        <td class="text-center">
                            {!! $teacher['eidikotita'] !!}
                        </td>
                        <td class="text-center">
                            @if($teacher['praxi']['pivot']['downloaded'])
                                <span class="ok">
                                    <i class="fa fa-check-circle"></i>
                                </span>
                                <a href="{{ route('Oikonomikou::PraxeisAnalipsis::downloadPraxi', $teacher['encrypt_filename']) }}" class="btn btn-warning">
                                    <i class="fa fa-download"></i>
                                </a>
                            @else
                                <a href="{{ route('Oikonomikou::PraxeisAnalipsis::downloadPraxi', $teacher['encrypt_filename']) }}" class="btn btn-warning">
                                    <i class="fa fa-download"></i>
                                </a>
                            @endif

                            @if(Auth::user()->isRole('admin'))
                                <a href="{{ route('Oikonomikou::PraxeisAnalipsis::deletePraxi', [$teacher['school_identifier'], $teacher['afm']]) }}" class="btn btn-danger">
                                    Delete
                                </a>                            
                            @endif

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif