@extends('app')

@section('title')
    Εισαγωγή Πρωτοκόλλου
@stop

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('content')
    <h1 class="page-heading">Πρωτόκολλο ΠΥΣΔΕ Χανίων</h1>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Εισαγωγή Νέου Πρωτοκόλλου</h3>
                </div>
                <div class="panel-body">
                    @include('errors.list')
                    {!! Form::model($protocol = new \Pasifai\Pysde\models\Protocol(), ['method'=>'POST', 'class'=>'form-horizontal', 'action'=>'\Pasifai\Pysde\controllers\ProtocolController@manualStore']) !!}
                        {!! Form::hidden('next_protocol',$next_protocol,[]) !!}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::label('id', 'Id:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('id',null, ['class'=>'form-control', 'id'=>'id']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('type', 'Κατηγορία:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-9">
                                         {!! Form::select('type',[0=>'Εισερχόμενα', 1=>'Εξερχόμενα',9=>'κενό'],null, ['class'=>'selectpicker']) !!}
                                    </div>
                                </div>
                                <!-- Name Form Input -->
                                <div class="form-group">
                                    {!! Form::label('p_date', 'Ημερομηνία:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('p_date',$protocol->p_date, ['class'=>'form-control', 'id'=>'date']) !!}
                                    </div>
                                </div>
                        
                                <div class="form-group">
                                    {!! Form::label('from_to', 'Αποστολέας:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-9">
                                        {!! Form::text('from_to',null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('subject', 'Θέμα:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-9">
                                        {!! Form::text('subject',null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('f_id', 'Φ:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-4">
                                        <select name="f_id" class="selectpicker" data-live-search="true" data-size="6" title="Φάκελος Ταξινόμησης">
                                            @foreach($fs as $f)
                                                <option {!! Request::old('f_id') == $f->id?' selected ':'' !!} value="{!! $f->id !!}" data-subtext="{!! $f->description !!}">{!! $f->name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::label('description', 'Παρατηρήσεις:', ['class'=>'control-label']) !!}
                                {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 text-center">
                                <div class="btn-group">
                                    {!! Form::submit('Χειροκίνητη Δημιουργία', ['class'=>'btn btn-primary btn-lg']) !!}
                                    <a href="{!! action('\Pasifai\Pysde\controllers\ProtocolController@index') !!}" class="btn btn-default btn-lg">Επιστροφή</a>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="panel-footer">
                    Εισαγωγή Νέου Πρωτοκόλλου
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts.footer')
    @include('pysde::protocol.scripts')
@endsection


