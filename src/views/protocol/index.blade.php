@extends('app')

@section('title')
    Εισαγωγή Πρωτοκόλλου
@stop

@section('header.style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" integrity="sha256-FdatTf20PQr/rWg+cAKfl6j4/IY3oohFAJ7gVC3M34E=" crossorigin="anonymous" />
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        #protocols tfoot input {
            width: 100%;
            /*padding: 3px;*/
            box-sizing: border-box;
        }
    </style>
@endsection

@section('content')
    <h1 class="page-heading">
        Λίστα Πρωτοκόλλου
    </h1>

    <div class="btn-group col-md-2 col-md-offset-5">
        <select class="col-md-10">
            <option {!! Request::get('year') == \Carbon\Carbon::now()->format('Y')?'selected':'' !!} value="{!! route('Pysde::Secretary::Protocol::index', ['year'=>\Carbon\Carbon::now()->format('Y')]) !!}">{!! \Carbon\Carbon::now()->format('Y') !!}</option>
            @foreach($years as $year)
            <option {!! Request::get('year') == $year?'selected':'' !!} value="{!! route('Pysde::Secretary::Protocol::index', ['year'=>$year]) !!}">{!! $year !!}</option>
            @endforeach
        </select>

        @if(isset($type))
            @if($type == 'in')
               <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Εισερχόμενα
            @elseif($type=='out')
               <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Εξερχόμενα
            @elseif($type=='null')
               <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> ΚΕΝΑ
            @elseif($type=='pending')
               <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Σε εκκρεμότητα
            @else
               <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Όλη η Λίστα
            @endif
        @else
       <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Όλη η Λίστα
       @endif
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <li><a href="{!! route('Pysde::Secretary::Protocol::index') !!}">Όλη η Λίστα</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="{!! route('Pysde::Secretary::Protocol::index') !!}?type=in{!! Request::get('year')?'&year='.Request::get('year'):'' !!}"><span class="label label-success">Εισερχόμενα</span></a></li>
        <li><a href="{!! route('Pysde::Secretary::Protocol::index') !!}?type=out{!! Request::get('year')?'&year='.Request::get('year'):'' !!}"><span class="label label-warning">Εξερχόμενα</span> </a></li>
        <li><a href="{!! route('Pysde::Secretary::Protocol::index') !!}?type=null{!! Request::get('year')?'&year='.Request::get('year'):'' !!}"><span class="label label-danger">ΚΕΝΑ</span> </a></li>
        <li><a href="{!! route('Pysde::Secretary::Protocol::index') !!}?type=pending{!! Request::get('year')?'&year='.Request::get('year'):'' !!}"><span class="label label-info">Σε εκκρεμότητα</span> </a></li>
      </ul>
    </div>

    <table id="protocols" class="table table-bordered table-hover" cellspacing="0" width="100%">

            <thead>
                <tr class="active">
                    <th>Αρ.Π</th>
                    <th class="text-center">Ημ/νια</th>
                    <th class="text-center">Τύπος</th>
                    <th class="text-center">Αποστολέας/Παραλήπτες</th>
                    <th class="text-center">Θέμα</th>
                    <th class="text-center">Φ</th>
                    <th class="text-center">Παρατηρήσεις</th>
                </tr>
            </thead>
            <tfoot>
                <tr class="active">
                    <th>Αρ.Π</th>
                    <th class="text-center">Ημ/νια</th>
                    <th class="text-center">Τύπος</th>
                    <th class="text-center">Αποστολέας/Παραλήπτες</th>
                    <th class="text-center">Θέμα</th>
                    <th class="text-center">Φ</th>
                    <th class="text-center">Παρατηρήσεις</th>
                </tr>
            </tfoot>
    </table>
@stop

@section('scripts.footer')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js" integrity="sha256-d/edyIFneUo3SvmaFnf96hRcVBcyaOy96iMkPez1kaU=" crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {

            $('select').change(function(){
                var url = $(this).val();
                window.location = url;
            })
            .select2({
                theme: "classic",
                language: {
                    noResults: function() {
                        return "Δεν υπάρχει αποτέλεσμα αναζήτησης";
                    }
                }
            });

            $('#protocols tfoot th').each( function () {
                 var title = $('#protocols thead th').eq( $(this).index() ).text();
                 $(this).html( '<input type="text" placeholder="'+title+'" />' );
            });

            var table = $('#protocols').DataTable( {
                "ajax": {
                    url : "/aj/protocols",
                    "data" : {
                        'type' : '{!! $type !!}',
                        'year' : '{!! Request::get('year')? Request::get('year') : \Carbon\Carbon::now()->format('Y') !!}'
                    },
                    "dataSrc": function ( json ) {
                      for ( var i=0, ien=json.data.length ; i<ien ; i++ ) {
                        @if( Request::has('year') &&  Request::get('year') != \Carbon\Carbon::now()->format('Y'))
                            json.data[i]['id'] = "<a href='{!! url('ΟΡΓΑΝΙΚΑ-ΚΕΝΑ/ΓΡΑΜΜΑΤΕΙΑ-ΠΥΣΔΕ/Protocol/edit-archives/') !!}/"+json.data[i]['aa']+"'>"+json.data[i]['id']+"</a>";
                        @else
                            json.data[i]['id'] = "<a href='{!! url('ΟΡΓΑΝΙΚΑ-ΚΕΝΑ/ΓΡΑΜΜΑΤΕΙΑ-ΠΥΣΔΕ/Protocol/edit/') !!}/"+json.data[i]['id']+"'>"+json.data[i]['id']+"</a>";
                        @endif
                      }
                      return json.data;
                    }
                },
                "columns": [
                    { "data": "id" },
                    { "data": "p_date" },
                    { "data": "type" },
                    { "data": "from_to" },
                    { "data": "subject"},
                    { "data": "f.name" },
                    { "data": "description"}
                ],
                "order": [[ 0, "desc" ]],
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 1, 2, 3,4,6 ] } ],
                "language": {
                            "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                            "emptyTable": "Δεν υπάρχουν δεδομένα στον πίνακα",
                            "loadingRecords" : "Φόρτωση δεδομένων...",
                            "processing":     "Επεξεργασία",
                            "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                            "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                            "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                            "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                            "search": "Αναζήτηση:",
                            "paginate": {
                                  "previous": "Προηγούμενη",
                                  "next" : "Επόμενη"
                            }
                }
            } );

            table.columns().every( function () {
                 var that = this;

                 $( 'input', this.footer() ).on( 'keyup change', function () {
                     if ( that.search() !== this.value ) {
                         that
                         .search( this.value )
                         .draw();
                     }
                 });

            });

        } );
    </script>
@endsection


