<div class="row">
    <div class="col-md-8">
        <div class="form-group">
                <div class="col-md-3">
                    {!! Form::label('protocol_id', 'Αριθμός Πρωτοκόλλου:', ['class'=>'control-label']) !!}
                </div>
                <div class="col-md-9">
                    <h3 style="letter-spacing: 0.2em"><span class="label label-default">
                        <strong>
                            @if(isset($next_protocol))
                                {!! $next_protocol !!}
                            @elseif(isset($protocol))
                                {!! $protocol->id !!}
                            @endif
                        </strong></span>
                        @if(intval($protocol->year) != intval($protocol->now))
                            &#9755; <span class="label label-danger">Έτος {!! $protocol->year !!}</span>
                        @endif
                        </h3>
                </div>
        </div>
        <div class="form-group">
            {!! Form::label('type', 'Κατηγορία:', ['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                 {!! Form::select('type',[0=>'Εισερχόμενα', 1=>'Εξερχόμενα',9=>'κενό'],null, ['class'=>'selectpicker']) !!}
            </div>
        </div>
        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('p_date', 'Ημερομηνία:', ['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('p_date',$protocol->p_date, ['class'=>'form-control', 'id'=>'date']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('from_to', 'Αποστολέας:', ['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::text('from_to',null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('subject', 'Θέμα:', ['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::text('subject',null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('f_id', 'Φ:', ['class'=>'col-md-3 control-label']) !!}
            <div class="col-md-4">
                <select name="f_id" class="selectpicker" data-live-search="true" data-size="6" title="Φάκελος Ταξινόμησης">
                    @foreach($fs as $f)
                        <option {!! $protocol->f_id == $f->id?' selected ':'' !!} value="{!! $f->id !!}" data-subtext="{!! $f->description !!}">{!! $f->name !!}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        {!! Form::label('description', 'Παρατηρήσεις:', ['class'=>'control-label']) !!}
        {!! Form::textarea('description', '', ['class'=>'form-control']) !!}

        <br><br>
        <div class="form-group">
            <h4><label name="pending" class="control-label"><span class="label label-info">Σε εκκρεμότητα</span>
            {!! Form::checkbox('pending', 1, null, ['class'=>'checkbox-inline']) !!}
            </label></h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3 text-center">
        <div class="btn-group">
            {!! Form::submit($submitButton, ['class'=>'btn btn-primary btn-lg']) !!}
            <a href="{!! action('\Pasifai\Pysde\controllers\ProtocolController@index') !!}" class="btn btn-default btn-lg">Επιστροφή</a>
        </div>
    </div>
</div>
