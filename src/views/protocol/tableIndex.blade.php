<table id="protocols" class="table table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr class="active">
                <th class="text-center">Name</th>
                <th> class="text-center"Position</th>
                <th class="text-center">Office</th>
                <th class="text-center">Extn.</th>
                <th class="text-center">Start date</th>
                <th class="text-center">Salary</th>
            </tr>
        </thead>
        <tfoot>
            <tr class="active">
                <th class="text-center">Name</th>
                <th> class="text-center"Position</th>
                <th class="text-center">Office</th>
                <th class="text-center">Extn.</th>
                <th class="text-center">Start date</th>
                <th class="text-center">Salary</th>
            </tr>
        </tfoot>

        <tbody>

        </tbody>

    <tbody>

        @foreach($protocols as $protocol)
        <tr @if($protocol->type == 1) class="warning" @elseif($protocol->type == 9) class="danger" @endif >
            <td class="text-center">
                @can('edit_protocol')
                    <a href="{!! route('Pysde::Secretary::Protocol::edit', $protoloc->id) !!}">{!! $protocol->id !!}</a>
                @else
                    @if($protocol->type == 9)
                        <a href="{!! route('Pysde::Secretary::Protocol::edit', $protoloc->id) !!}">{!! $protocol->id !!}</a>
                    @else
                        {!! $protocol->id !!}
                    @endif
                @endcan
            </td>
            <td class="text-center">{!! $protocol->p_date !!}</td>
            @if($protocol->type == 0)
                <td class="text-center">Εισερχόμενο</td>
            @elseif($protocol->type == 1)
                <td class="text-center">Εξερχόμενο</td>
            @elseif($protocol->type == 9)
                <td class="text-center" style="letter-spacing:2px">ΚΕΝΟ</td>
            @endif
            <td>{!! $protocol->from_to !!}</td>
            <td>{!! $protocol->subject !!}</td>
            <td class="text-center">{!! $protocol->f_name !!}</td>
            <td>{!! $protocol->description !!}</td>
        </tr>
        @endforeach
    </tbody>
</table>