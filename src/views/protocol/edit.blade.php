@extends('app')

@section('title')
    Επεξεργασία Πρωτοκόλλου
@stop

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('content')
    <h1 class="page-heading">Πρωτόκολλο ΠΥΣΔΕ Χανίων</h1>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Επεξεργασία αρ. Πρωτοκόλλου: {!! $protocol->id !!} </h3>
                </div>
                <div class="panel-body">
                    @include('errors.list')
                    @if(isset($status) && $status == 'OLD')
                    {!! Form::model($protocol, ['method'=>'PATCH', 'class'=>'form-horizontal', 'action'=>['\Pasifai\Pysde\controllers\ProtocolController@updateArchives', $protocol->aa]]) !!}
                    @else
                    {!! Form::model($protocol, ['method'=>'PATCH', 'class'=>'form-horizontal', 'action'=>['\Pasifai\Pysde\controllers\ProtocolController@update', $protocol->id]]) !!}
                    @endif
                        @include('pysde::protocol._form',['submitButton' =>'Ενημέρωση'])
                    {!! Form::close() !!}
                </div>
                <div class="panel-footer">
                    Επεξεργασία αρ. Πρωτοκόλλου: {!! $protocol->id !!}
                    @if(intval($protocol->year) != intval($protocol->now))
                        <span class="label label-danger">Έτος {!! $protocol->year !!} </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts.footer')
    @include('pysde::protocol.scripts')
@endsection