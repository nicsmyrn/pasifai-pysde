    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
  
    <script type="text/javascript" src="/vendor/pysde/js/bootstrap-datepicker.gr.js" charset="UTF-8"></script>
  
  <script>
        $(document).ready(function() {

            $('#years').change(function(){
                var url = $(this).val();
                window.location = url;
            });
         
            $('button').on('click', function(){
                var url = $('#select_pdf').val();
              window.open(url); 
            });
            
            // $('#select_pdf').select2();
 
            $.fn.selectpicker.defaults = {
                noneSelectedText: 'Τίποτα δεν επιλέχθηκε',
                noneResultsText: 'Δεν υπάρχουν αποτελέσματα {0}',
                countSelectedText: function (numSelected, numTotal) {
                  return (numSelected == 1) ? "{0} στοιχείο επιλέχθηκε" : "{0} στοιχεία επιλέχθηκαν";
                },
                maxOptionsText: function (numAll, numGroup) {
                  return [
                    (numAll == 1) ? 'Limit reached ({n} item max)' : 'Limit reached ({n} items max)',
                    (numGroup == 1) ? 'Group limit reached ({n} item max)' : 'Group limit reached ({n} items max)'
                  ];
                },
                selectAllText: 'Επιλογή όλων',
                deselectAllText: 'αφαίρεση όλων',
                multipleSeparator: ', '
            };
            
            var endDate = new Date();
            $('#date').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                daysOfWeekDisabled: [0,6],
                @if(Request::is('*create'))
                    startDate: '01/01/'+endDate.getFullYear(),
                @endif
                endDate: endDate,
                language: 'gr'
            });

            var endDate = new Date();
            $('#date').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                daysOfWeekDisabled: [0,6],
                @if(Request::is('*create'))
                    startDate: '01/01/'+endDate.getFullYear(),
                @endif
                endDate: endDate,
                language: 'gr'
            });


            $('#type').on('change', function(e){
                if ($(this).val() == 0){
                    $('label[for=from_to]').html('Αποστολέας:');
                };
                if ($(this).val() == 1){
                    $('label[for=from_to]').html('Παραλήπτες:');
                }
                if ($(this).val() == 9){
                    $('input[name=from_to]').val('-').closest('.form-group').css('visibility','hidden');
                    $('input[name=subject]').val('-').closest('.form-group').css('visibility','hidden');
                    $('select[name=f_id]').val(30).closest('.form-group').css('visibility','hidden');
                }
            });
         });
    </script>