<table class="list" style="width:97%; margin-top:1em;">
        <tbody>
            <tr class="primary_head">
                <td class="center" rowspan="2" style="width:5%">Αρ. Πρωτ.</td>
                <td class="center" rowspan="2" style="width: 6%">Ημερομηνία</td>
                <td class="center type" colspan="2">ΕΙΣΕΡΧΟΜΕΝΑ</td>
                <td class="center" rowspan="2" style="width: 3%">Φ</td>
                <td class="center type" colspan="2">ΕΞΕΡΧΟΜΕΝΑ</td>
                <td class="center" rowspan="2" style="width: 7%">Παρατηρήσεις</td>
            </tr>
            <tr class="head">
                <td class="center" style="width: 15%">Αποστολέας</td>
                <td class="center" style="width: 15%">Θέμα</td>
                <td class="center" style="width: 15%">Παραλήπτες</td>
                <td class="center" style="width: 15%">Θέμα</td>
            </tr>
            @foreach($protocols as $protocol)
                <tr class="list_row">
                    <td class="center protocol">{!! $protocol->id !!}</td>
                    <td class="center date">{!! $protocol->p_date !!}</td>
                    <td class="left">@if($protocol->type == 'Εισερχόμενο'){!! str_limit($protocol->from_to, 17) !!}@endif</td>
                        @if($protocol->type == 'Εισερχόμενο')
                            <td class="left"> {!! str_limit($protocol->subject, 17) !!} </td>
                        @elseif($protocol->type == 'Εξερχόμενο')
                            <td class="center" style="letter-spacing: 0.5em"> ΕΞΕΡΧΟΜΕΝΟ</td>
                        @else
                            <td class="center"></td>
                        @endif
                    <td class="center">{!! $protocol->f_name !!}</td>
                    <td class="left">
                        @if($protocol->type == 'Εξερχόμενο')
                            {!! str_limit($protocol->from_to, 17) !!}
                        @endif
                    </td>
                    <td class="left">
                        @if($protocol->type == 'Εξερχόμενο')
                            {!! str_limit($protocol->subject, 17) !!}
                        @endif
                    </td>
                    <td class="left">{!! str_limit($protocol->description, 5) !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
