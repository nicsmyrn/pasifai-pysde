@extends('app')

@section('title')
    Καθηγητές για έλεγχο
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('content')

    <h1 class="page-heading">Διαχείριση  ΚΑΘΗΓΗΤΩΝ</h1>

    @if($teachers->isEmpty())
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει καθηγητής για έλεγχο</div>
                </div>
            </div>
        </div>
    @else

    <div class="row">
        <table id="requests" class="table table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th>Ονοματεπώνυμο</th>
                    <th>Πατρώνυμο</th>
                    <th>Ειδικότητα</th>
                    <th>Σχέση</th>
                    <th>Οργανική</th>
                </tr>
            </thead>

            <tbody>
                @foreach($teachers as $teacher)
                    <tr>
                        <td>
                            <a href="{!! route('Pysde::Teachers::check', [$teacher->id]) !!}">
                                {!! $teacher->user->full_name !!}
                            </a>
                        </td>
                        <td>{!! $teacher->middle_name !!}</td>
                        <td>{!! $teacher->klados_name !!}</td>
                        <td>{!! $teacher->teacherable_type == 'App\Monimos' ? 'Μόνιμος': 'Αναπληρωτής' !!}</td>
                        <td>{!! $teacher->teacherable->organiki_name !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    @endif

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#requests').DataTable({
                    "order": [[ 0, "asc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 3 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection


