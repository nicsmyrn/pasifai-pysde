@if(isset($myschool))
<h5>
    <small>My School:</small>
    <span id="fake_am" class="suggestion-fake">
        {!! $selectCollection[$myschool] !!}
    </span>
    <input type="hidden" class="suggestion-fake" value="{!! $myschool !!}"/>
    <button class="btn btn-default btn-xs button_select_box">
        <i class="fa fa-exchange button-change" aria-hidden="true"></i>
        αλλαγή
    </button>
</h5>
@endif