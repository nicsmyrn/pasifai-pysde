
<form method="post" class="form" action="{{route('Pysde::Teachers::updateTeacherProfile',[$teacher->id])}}">
    @csrf
    <div class="row">
        <div class="col-md-5 text-center">
            <div class="form-group form-inline">
                    {!! Form::label('ex_years', 'Χρόνια:', ['class'=>'control-label']) !!}
                    {!! Form::text('ex_years',$teacher->ex_years, ['size'=>2, 'maxlength'=>2, 'class'=>'form-control']) !!}
                    {!! Form::label('ex_months', 'Μήνες:') !!}
                    {!! Form::text('ex_months',$teacher->ex_months, ['size'=>2, 'maxlength'=>2, 'class'=>'form-control']) !!}
                    {!! Form::label('ex_days', 'Ημέρες:') !!}
                    {!! Form::text('ex_days',$teacher->ex_days, ['size'=>2, 'maxlength'=>2, 'class'=>'form-control']) !!}
            </div>
            <div class="form-group form-inline">
                @if($teacher->teacherable_type == 'App\Anaplirotis')
                    {!! Form::label('seira_topothetisis', 'Σειρά τοποθέτησης:', ['class'=>'control-label']) !!}
                    {!! Form::text('seira_topothetisis',$teacher->teacherable->seira_topothetisis, ['class'=>'form-control']) !!}
                @endif
            </div>
        </div>

        <div class="col-md-3 text-center">
            <div class="form-group form-inline">
                 {!! Form::label('moria', 'Μόρια:', ['class'=>'control-label']) !!}
                 {!! Form::text('moria',$teacher->teacherable->moria, ['size'=>6, 'maxlength'=>6,'class'=>'form-control']) !!}
            </div>
        </div>

        <div class="col-md-4 text-center">
            <div class="form-group form-inline">
                 {!! Form::label('moria_apospasis', 'Μόρια Απόσπασης:', ['class'=>'control-label']) !!}
                 {!! Form::text('moria_apospasis',$teacher->teacherable->moria_apospasis, ['size'=>7, 'maxlength'=>7,'class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                <h5 class="text-center">
                    Μόρια Απόσπασης:
                    <span style="font-size: 15pt;font-weight: bold;color: red">
                         {!! number_format($analytika->sum('value'), 3, ',', '') !!}
                    </span>
                    <a class="btn btn-success btn-xs" href="#" type="button" data-toggle="modal" data-target="#moriaApospasis">
                         Λεπτομέρειες...
                    </a>
                </h5>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if($teacher->profile_counter > 0)
                    <div class="col-md-12">
                        <div class="alert alert-info alert-dismissible text-center" role="alert">
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                             Ο εκπαιδευτικός έχει πραγματοποιήσει <strong> {!! $teacher->profile_counter  !!} {!! $teacher->profile_counter == 1 ?  ' φορά αλλαγή' : ' φορές αλλαγές' !!}  </strong> στα στοιχεία του.
                        </div>
                    </div>
            @else
                    <div class="col-md-12">
                        <div class="alert alert-info text-center" role="alert">
                             Ο εκπαιδευτικός δεν έχει πραγματοποιήσει <strong><u>καμία αλλαγή</u></strong> στα στοιχεία του.
                        </div>
                    </div>
            @endif
        </div>

        <div class="col-md-4 text-center">
            <button class="btn btn-primary">Αποθήκευση</button>
        </div>
        <div class="col-md-4 text-center">
            @if(!$teacher->is_checked)
                <button type="submit" class="btn btn-success" formaction="{{route('Pysde::Teachers::confirm', [$teacher->id])}}">
                    Αποθήκευση & Κλείδωμα
                </button>
            @endif
        </div>
        <div class="col-md-4 text-center">
            @if(!$teacher->is_checked)
                <a href="{{route('Pysde::Teachers::recheck', [$teacher->id])}}" class="btn btn-danger"> Καμία αλλαγή & κλείδωμα</a>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            @if($teacher->teacherable_type == 'App\Monimos')
                <h3><span style="letter-spacing: 0.4em" class="label label-default">ΜΟΝΙΜΟΣ</span></h3>
            @elseif($teacher->teacherable_type == 'App\Anaplirotis')
                <h3><span style="letter-spacing: 0.4em" class="label label-default">ΑΝΑΠΛΗΡΩΤΗΣ</span></h3>
            @endif
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <table class="table">
                <tbody>
                    <tr>
                        <td width="20%">
                            <strong>
                                <span class="glyphicon glyphicon-asterisk text-primary"></span>
                                Επώνυμο
                            </strong>
                        </td>
                        <td width="80%">
                            <strong>
                                {!! Form::text('last_name',$teacher->user->last_name, ['class'=>'text_real_value col-md-12']) !!}
                            </strong>
                             @if($teacher->user->last_name != $teacher->myschool->last_name)
                                 @include('pysde::check.form._MySchool_textbox',['my_school' => $teacher->myschool->last_name])
                             @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                Όνομα
                            </strong>
                        </td>
                        <td>
                            <strong>
                                {!! Form::text('first_name',$teacher->user->first_name, ['class'=>'text_real_value col-md-12']) !!}
                            </strong>
                            @if($teacher->user->first_name != $teacher->myschool->first_name)
                                @include('pysde::check.form._MySchool_textbox',['my_school' => $teacher->myschool->first_name])
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                Πατρώνυμο
                            </strong>
                        </td>
                        <td>
                            <strong>
                                {!! Form::text('middle_name',$teacher->middle_name, ['class'=>'text_real_value col-md-12']) !!}
                            </strong>
                            @if($teacher->middle_name != $teacher->myschool->middle_name)
                                @include('pysde::check.form._MySchool_textbox',['my_school' => $teacher->myschool->middle_name])
                            @endif
                            @if($hasFakeProfile)
                                @if($teacher->middle_name != $teacher->fake->middle_name)
                                    @include('pysde::check.form._Fake_textbox',['var'=> $teacher->fake->middle_name, 'my_school' => $teacher->myschool->middle_name])
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                Μητρώνυμο
                            </strong>
                        </td>
                        <td>
                            <strong>

                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                Φύλο
                            </strong>
                        </td>
                        <td>
                            <strong>
                                {!! Form::select('sex', ['Θήλυ','Άρρεν'], $teacher->user->sex, ['class'=>'text_real_value']) !!}
                            </strong>
                            @if($teacher->user->sex != $teacher->myschool->sex)
                                @include('pysde::check.form._MySchool_selectbox', ['myschool'=> $teacher->myschool->sex, 'selectCollection' => ['Θήλυ', 'Άρρεν']])
                            @endif
                            @if($hasFakeProfile)
                                @if($teacher->user->sex != $teacher->fake->sex)
                                     @include('pysde::check.form._Fake_selectbox',['var'=> $teacher->fake->sex, 'selectCollection'=>['Θήλυ', 'Άρρεν']])
                                @endif
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table">
                <tbody>
                    <tr>
                        <td width="20%">
                            <strong>
                                ΑΦΜ
                            </strong>
                        </td>
                        <td width="80%">
                            <strong>

                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                ΑΜ
                            </strong>
                        </td>
                        <td>
                            <strong>
                                {!! Form::text('am',$teacher->teacherable->am, ['class'=>'text_real_value', 'size' => 7]) !!}
                            </strong>
                            @if($teacher->teacherable->am != $teacher->myschool->am)
                                @include('pysde::check.form._MySchool_textbox',['my_school' => $teacher->myschool->am])
                            @endif
                            @if($hasFakeProfile)
                                @if($teacher->teacherable->am != $teacher->fake->teacherable->am)
                                    @include('pysde::check.form._Fake_textbox',['var'=> $teacher->fake->teacherable->am, 'my_school' => $teacher->myschool->am])
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                Υπ. Ωράριο:
                            </strong>
                        </td>
                        <td>
                            <strong>
                                {!! Form::text('orario',$teacher->teacherable->orario, ['class'=>'text_real_value', 'size' => 2]) !!}
                            </strong>
                            @if($teacher->teacherable->orario != $teacher->myschool->ypoxreotiko)
                                @include('pysde::check.form._MySchool_textbox',['my_school' => $teacher->myschool->ypoxreotiko])
                            @endif
                            @if($hasFakeProfile)
                                @if($teacher->teacherable->orario != $teacher->fake->teacherable->orario)
                                     @include('pysde::check.form._Fake_textbox',['var'=> $teacher->fake->teacherable->orario, 'my_school' => $teacher->myschool->ypoxreotiko])
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                Κλάδος:
                            </strong>
                        </td>
                        <td>
                            <select name="new_eidikotita" class="text_real_value col-md-6">
                                @foreach($new_kladoi as $klados)
                                    <option value="{{$klados->id}}" @if($teacher->myschool->new_eidikotita == $klados->id) selected @endif>
                                       {!! $klados->eidikotita_slug !!} - {!! $klados->eidikotita_name !!}
                                    </option>
                                @endforeach
                            </select>
                            @if($teacher->klados_id != ($myKlados = \App\Models\Eidikotita::where('slug_name', $teacher->myschool->eidikotita)->first()->id) )
                                @include('pysde::check.form._MySchool_selectbox', ['myschool'=> $myKlados, 'selectCollection' => $kladoi])
                            @endif
                            @if($hasFakeProfile)
                                @if($teacher->klados_id != $teacher->fake->klados_id)
                                     @include('pysde::check.form._Fake_selectbox',['var'=> $teacher->fake->klados_id, 'selectCollection'=>$kladoi])
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>
                                Οργανική:
                            </strong>
                        </td>
                        <td>
                            <strong>
                                {!! Form::select('organiki', $schools, $teacher->teacherable->organiki, ['class'=>'text_real_value']) !!}
                            </strong>
                            @if($teacher->teacherable->organiki != $myOrganiki)
                                @if(!str_contains($teacher->myschool->topothetisi, 'Οργανικά'))
                                    <h5>
                                        <small>My School:</small>
                                        <span class="suggestion-fake">
                                            {!! $teacher->myschool->topothetisi !!}
                                        </span>
                                    </h5>
                                @endif
                                    @include('pysde::check.form._MySchool_selectbox', ['myschool'=> $myOrganiki, 'selectCollection' => $schools])
                            @endif
                            @if($hasFakeProfile)
                                @if($teacher->teacherable->organiki != $teacher->fake->teacherable->organiki)
                                     @include('pysde::check.form._Fake_selectbox',['var'=> $teacher->fake->teacherable->organiki, 'selectCollection'=>$schools])
                                @endif
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

        <div class="row">

            <div class="col-md-6">
                <div class="panel panel-danger">
                  <div class="panel-heading">
                    <h3 class="panel-title">
                       <span>
                        <input id="logoi_ygeias_checkbox" type="checkbox" value="0"/>
                       </span>
                       {!! Form::label('show_logoi_ygeias', 'Σοβαροί Λόγοι Υγείας:') !!}
                    </h3>
                  </div>
                  <div class="panel-body">
                       <div id="content_logoi_ygeias" style="display: none;margin-top: 10px">
                            @foreach(Config::get('requests.logoi_ygeias') as $ygeia_type)
                                <div class="form-group">
                                   {!! Form::label($ygeia_type['name'], $ygeia_type['label']) !!}
                                   <strong>
                                       {!! Form::select($ygeia_type['name'],$ygeia_type['data'],$teacher[$ygeia_type['name']], ['id' => $ygeia_type['name'], 'class'=>'text_real_value']) !!}
                                   </strong>
                                   @if($hasFakeProfile)
                                       @if($teacher[$ygeia_type['name']] != $teacher->fake[$ygeia_type['name']])
                                            @include('pysde::check.form._Fake_selectbox',['var'=> $teacher->fake[$ygeia_type['name']], 'selectCollection'=> $ygeia_type['data']])
                                       @endif
                                   @endif
                                </div>
                            @endforeach
                       </div>
                  </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-warning">
                    <div class="panel-heading text-center">
                            Οικογενειακά Στοιχεία
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                                <small>Οικογενειακή κατάσταση: </small>
                                <strong>
                                    {!! Form::select('family_situation', Config::get('requests.family_situation'), $teacher->family_situation,['class'=>'text_real_value']) !!}
                                </strong>
                            @if($hasFakeProfile)
                                @if($teacher->family_situation != $teacher->fake->family_situation)
                                     @include('pysde::check.form._Fake_selectbox',['var'=> $teacher->fake->family_situation, 'selectCollection'=>Config::get('requests.family_situation')])
                                @endif
                            @endif
                        </div>
                        <br>
                        <div class="col-md-12">
                            <small>Αριθμός παιδιών:  </small> <strong> {!! Form::text('childs',$teacher->childs,['class'=>'text_real_value', 'size' => 2]) !!}</strong>
                            @if($hasFakeProfile)
                                @if($teacher->childs != $teacher->fake->childs)
                                     @include('pysde::check.form._Fake_textbox',['var'=> $teacher->fake->childs, 'my_school' => '-'])
                                @endif
                            @endif
                        </div>
                        <br>
                        <div class="col-md-12">
                                <small>Δήμος Εντοπιότητας:  </small>
                                <strong>
                                    {!! Form::select('dimos_entopiotitas', Config::get('requests.dimos'),$teacher->dimos_entopiotitas, ['class'=>'text_real_value']) !!}
                                </strong>
                            @if($hasFakeProfile)
                                @if($teacher->dimos_entopiotitas != $teacher->fake->dimos_entopiotitas)
                                     @include('pysde::check.form._Fake_selectbox',['var'=> $teacher->fake->dimos_entopiotitas, 'selectCollection'=>Config::get('requests.dimos')])
                                @endif
                            @endif
                        </div>
                        <br>
                        <div class="col-md-12">
                                <small>Δήμος Συνηπηρέτησης:  </small>
                                {!! Form::select('dimos_sinipiretisis', Config::get('requests.dimos'),$teacher->dimos_sinipiretisis,['class'=>'text_real_value']) !!}
                             @if($hasFakeProfile)
                                 @if($teacher->dimos_sinipiretisis != $teacher->fake->dimos_sinipiretisis)
                                      @include('pysde::check.form._Fake_selectbox',['var'=> $teacher->fake->dimos_sinipiretisis, 'selectCollection'=>Config::get('requests.dimos')])
                                 @endif
                             @endif
                        </div>
                        <br>
                        <div class="col-md-12">
                            <small>Ειδική κατηγορία:  </small>
                            {!! Form::select('special_situation',['ΟΧΙ','ΝΑΙ'],$teacher->special_situation) !!}
                             @if($hasFakeProfile)
                                 @if($teacher->special_situation != $teacher->fake->special_situation)
                                      @include('pysde::check.form._Fake_selectbox',['var'=> $teacher->fake->special_situation, 'selectCollection'=> ['ΟΧΙ', 'ΝΑΙ']])
                                 @endif
                             @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                            Στοιχεία Επικοινωνίας
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <h3><small>Σταθερό Τηλέφωνο:  </small> {!! $teacher->phone !!}</h3>
                        </div>
                        <div class="col-md-12">
                            <h3><small>Διεύθυνση:  </small> {!! $teacher->address !!}</h3>
                        </div>
                        <div class="col-md-12">
                            <h3><small>Πόλη:  </small> {!! $teacher->city !!}</h3>
                        </div>
                        <div class="col-md-12">
                            <h3><small>Κινητό Τηλέφωνο:  </small> {!! $teacher->mobile !!}</h3>
                        </div>
                        <div class="col-md-12">
                            <h3><small>ΤΚ:  </small> {!! $teacher->tk !!}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</form>