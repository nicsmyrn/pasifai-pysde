@extends('app')

@section('header.style')
    <style>
        .suggestion-fake{
            color: red;
            font-weight: bold;
        }
        .button-change{
            color : red;
        }
    </style>
@endsection

@section('content')
    @if ($teacher != null)

        <h3 class="page-heading">ΕΛΕΓΧΟΣ</h3>

        @include('errors.list')

        @if($teacher->is_checked)
            <div class="alert alert-success text-center" role="alert"><strong>Τα στοιχεία του καθηγητή είναι ελεγμένα από την υπηρεσία</strong></div>
        @endif
        @if($teacher->request_to_change)
             <div class="alert alert-danger text-center" role="alert">
                <strong>
                    Ο καθηγητής αιτήθηκε <u>ΑΛΛΑΓΗ</u> των στοιχείων του Προφίλ του. Πατήστε
                    <a href="{{route('Pysde::Teachers::unlockProfile', [$teacher->id])}}" class="btn btn-success btn-sm">ΞΕΚΛΕΙΔΩΜΑ</a>
                    για ξεκλείδωμα του δικαιώματος αλλαγής στοιχείων ή
                    <a href="{{route('Pysde::Teachers::recheck', [$teacher->id])}}" class="btn btn-danger btn-sm">ΚΑΜΙΑ ΑΛΛΑΓΗ και κλείδωμα</a>
                    για διατήρηση των στοιχείων ως έχουν
                </strong>
            </div>
         @endif

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Στοιχεία καθηγητή: {{ $teacher->user->full_name }} - ΑΦΜ : {{ $teacher->afm }}</h3>
                    </div>

                    <div class="panel-body">
                        @if($teacher->fid != null)
                            @if($teacher->teacherable_type == 'App\Anaplirotis' &&  $teacher->fake->teacherable_type == 'App\FakeAnaplirotis' || $teacher->teacherable_type == 'App\Monimos' &&  $teacher->fake->teacherable_type == 'App\FakeMonimos')
                                @include('pysde::check.form._checkDetails', ['hasFakeProfile' => true, 'kladoi' => $kladoi, 'schools'=> $schools])
                            @else
                                @include('pysde::check.form._differentRelationCheckProfile')
                            @endif
                        @else
                            @include('pysde::check.form._checkDetails', ['hasFakeProfile' => false, 'kladoi' => $kladoi, 'schools'=> $schools, 'myOrganiki' => $myOrganiki])
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @else
        Ο ΕΚΠΑΙΔΕΥΤΙΚΟΣ ΔΕΝ υπάρχει
    @endif

    {{--@include('teacher.aitisis.modal-moria-apospasis', ['teacher' => $teacher])--}}
@endsection

@section('scripts.footer')

    <script>
        $(document).ready(function() {
            $('button_sxesi').on('click', function(e){
                e.preventDefault();
            });

            $('.button_text_field').on('click', function(e){
                e.preventDefault();
                $(this).parent()
                    .fadeOut(500, function(){
                        $(this).remove();
                    });
                $(this).parent().prev().find('.text_real_value').val($(this).prev('.suggestion-fake').html());
            });

            $('.button_text_field_cancel').on('click', function(e){
                e.preventDefault();
                $(this).parent()
                    .fadeOut(500, function(){
                        $(this).remove();
                    });
            });

            $('.button_select_box_cancel').on('click', function(e){
                e.preventDefault();
                $(this).parent()
                    .fadeOut(500, function(){
                        $(this).remove();
                    });
            });

           $('.button_select_box').on('click', function(e){
               e.preventDefault();
               $(this).parent()
                   .fadeOut(500, function(){
                       $(this).remove();
                   });
               var selectBox = $(this).parent().prev().find('.text_real_value');
               var selectNewValue = $(this).prev('.suggestion-fake').val();

               selectBox.find('option:selected').removeAttr('selected');
               selectBox.val($(this).prev('.suggestion-fake').val());
               selectBox.find('option[value='+selectNewValue+']').attr('selected', 'selected');
           });

          $('#logoi_ygeias_checkbox').on('change', function(){
               if($(this).is(':checked')){
                  $('#content_logoi_ygeias').show();
               }else{
                  $('#content_logoi_ygeias').hide();
               }
          });
        })
    </script>
@endsection