@extends('app')

@section('header.style')
    	<style>
            #teacher-instructions{
                position: fixed;
                top: 260px;
                left:0px;
            }
    	</style>
@endsection

@section('content')

    <h1 class="page-heading"> ΝΕΑ ΑΙΤΗΣΗ - Δημιουργία</h1>

    <div class="alert alert-warning col-md-8 col-md-offset-2" role="alert">
        <ol>
            <li><a target="_blank" href="https://drive.google.com/file/d/1L5uXFRmytm-Dm6PUcWlcyZhxLYHEjAS3/view?usp=sharing">Μόρια Σχολείων</a></li>
            <li><a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViQUI2RUl0dElTUGc/view?usp=sharing">Ομάδες Σχολείων Ν. Χανίων</a></li>
        </ol>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table width="100%" cellpadding="10" cellspacing="10">
                @can('create_Οργαν_request')
                    @if(\Auth::user()->userable->teacherable_type == 'App\Monimos')
                        {{--
                            <tr>
                                <td>
                                    @can('create_Μοριοδ_request')
                                        @include('pysde::aitisis.organika.teacher._form._available_veltivseis', [
                                            'title' => 'Αίτηση μοριοδότησης μετάθεσης (συμπλήρωση ωραρίου)',
                                            'type'=>'Αίτηση-Μοριοδότησης-Μετάθεσης',
                                            'description'=>'Αίτηση επαναυπολογισμού μορίων μετάθεσης (για συμπλήρωση ωραρίου)',
                                            'file'=>'Μοριοδ'
                                        ])
                                    @else
                                        @include('pysde::aitisis.organika.teacher._form._not_available_veltiwseis', [
                                            'type'=>'Αίτηση-Μοριοδότησης-Μετάθεσης',
                                            'file'=>'Μοριοδ'
                                        ])
                                    @endcan
                                </td>
                                <td>
                                    @can('create_ΜοριοδΑ_request')
                                        @include('pysde::aitisis.organika.teacher._form._available_veltiwseis', [
                                            'title' => 'Αίτηση μοριοδότησης απόσπασης',
                                            'type'=>'Αίτηση-Μοριοδότησης-Απόσπασης',
                                            'description'=>'Αίτηση υπολογισμού μορίων απόσπασης (εντός ΠΥΣΔΕ)',
                                            'file'=>'ΜοριοδΑ'
                                        ])
                                    @else
                                        @include('pysde::aitisis.organika.teacher._form._not_available_veltiwseis', [
                                            'type'=>'Αίτηση-Μοριοδότησης-Απόσπασης',
                                            'file'=>'ΜοριοδΑ'
                                        ])
                                    @endcan
                                </td>
                            </tr>
                        --}}
                        <tr>
                            <td>
                                @can('create_Υπεραρ_request')
                                    @include('pysde::aitisis.organika.teacher._form._available_veltiwseis', [
                                        'title' => 'Αίτηση για Υπεραριθμία',
                                        'type'=>'Αίτηση-Υπεραριθμίας',
                                        'description'=>'Αίτηση επιθυμίας για χαρακτηρισμό ως ονομαστικά Υπεράριθμος',
                                        'file'=>'Υπεραρ'
                                    ])
                                @else
                                    @include('pysde::aitisis.organika.teacher._form._not_available_veltiwseis', [
                                        'type'=>'Αίτηση-Υπεραριθμίας',
                                        'file'=>'Υπεραρ'
                                    ])
                                @endcan

                            </td>
                            <td>
                                @can('create_ΟργανικήΥπ_request')
                                    @include('pysde::aitisis.organika.teacher._form._available_veltiwseis', [
                                        'title' => 'Αίτηση για Οργανική σε Υπεράριθμο',
                                        'type'=>'Αίτηση-Οργανικής-Υπεράριθμου',
                                        'description'=>'Αίτηση - Δήλωση προτίμησης Σχολείου - Σχολείων ΜΟΝΟ για Οργανικά Υπεράριθμους',
                                        'file'=>'ΟργανικήΥπ'
                                    ])
                                @else
                                    @include('pysde::aitisis.organika.teacher._form._not_available_veltiwseis', [
                                        'type'=>'Αίτηση-Οργανικής-Υπεράριθμου',
                                        'file'=>'ΟργανικήΥπ'
                                    ])
                                @endcan
                            </td>
                            <td>
                                @can('create_Βελτίωση_request')
                                    @include('pysde::aitisis.organika.teacher._form._available_veltiwseis', [
                                        'title' => 'Αίτηση για Βελτίωση - Οριστική Τοποθέτηση',
                                        'type'=>'Αίτηση-Βελτίωσης-Οριστικής-Τοποθέτησης',
                                        'description'=>'Αίτηση - Δήλωση πρότιμησης Σχολείου - Σχολείων για βελτίωση - οριστική τοποθέτηση',
                                        'file'=>'Βελτίωση'
                                    ])
                                @else
                                    @include('pysde::aitisis.organika.teacher._form._not_available_veltiwseis', [
                                        'type'=>'Αίτηση-Βελτίωσης-Οριστικής-Τοποθέτησης',
                                        'file'=>'Βελτίωση'
                                    ])
                                @endcan
                            </td>
                        </tr>
                    @endif
                @endcan
 
                    @if(\Auth::user()->userable->teacherable_type == 'App\Monimos')
                        <tr align="center">
                            <td>
                            @if(\Auth::user()->userable->teacherable->county == \Config::get('requests.default_county'))
                                @can('create_E1_request')
                                    @include('pysde::aitisis._form._available',['type'=>'E1', 'description'=> 'Αίτηση - Δήλωση Λειτουργικά υπεράριθμων, Διάθεση ΠΥΣΔΕ, μετάθεση'])
                                @else
                                    @include('pysde::aitisis._form._not_available',['type'=>'E1'])
                                @endcan
                            @endif
                            </td>

                            <td>
                                @can('create_E2_request')
                                    @include('pysde::aitisis._form._available',['type'=>'E2', 'description'=> 'Αίτηση - Δήλωση για συμπλήρωση ωραρίου'])
                                @else
                                    @include('pysde::aitisis._form._not_available',['type'=>'E2'])
                                @endcan
                            </td>


                            <td>
                                @can('create_E3_request')
                                    @include('pysde::aitisis._form._available',['type'=>'E3', 'description'=> 'Δήλωση απόσπασης εντός ΠΥΣΔΕ και αποσπασμένου από άλλο ΠΥΣΔΕ'])
                                @else
                                    @include('pysde::aitisis._form._not_available',['type'=>'E3'])
                                @endcan
                            </td>

                        </tr>
                        <tr align="center">
                            <td>
                                @can('create_E6_request')
                                    @include('pysde::aitisis._form._available',['type'=>'E6', 'description'=> 'για τους ΝΕΟΔΙΟΡΙΣΤΟΥΣ'])
                                @else
                                    @include('pysde::aitisis._form._not_available',['type'=>'E6'])
                                @endcan
                            </td>
                        </tr>
                    @else
                        <tr align="center">
                            <td>
                                @can('create_E2_request')
                                    @include('pysde::aitisis._form._available',['type'=>'E2', 'description'=> 'Αίτηση - Δήλωση για συμπλήρωση ωραρίου'])
                                @else
                                    @include('pysde::aitisis._form._not_available',['type'=>'E2'])
                                @endcan
                            </td>
                            <td>
                                @can('create_E4_request')
                                    @include('pysde::aitisis._form._available',['type'=>'E4', 'description'=> 'τοποθέτηση αναπληρωτή'])
                                @else
                                    @include('pysde::aitisis._form._not_available',['type'=>'E4'])
                                @endcan
                            </td>
                        </tr>
                        <tr align="center">
                            <td>
                                @can('create_E5_request')
                                    @include('pysde::aitisis._form._available',['type'=>'E5', 'description'=> 'τοποθέτηση αναπληρωτή COVID 19'])
                                @else
                                    @include('pysde::aitisis._form._not_available',['type'=>'E5'])
                                @endcan
                            </td>
                        </tr>
                    @endif

            </table>
        </div>
    </div>

@endsection