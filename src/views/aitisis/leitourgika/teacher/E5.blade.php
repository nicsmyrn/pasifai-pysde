@include('pysde::aitisis.leitourgika.teacher._foundRequest')

<div v-else class="col-md-9">

   <div v-if="showCovid19">
        <buttons-request
            ref="buttons"
            v-on:save-request="saveRequest"
            v-on:open-delete-modal="makeDeleteModalOpen"
            v-on:open-agreement-modal="makeAgreementModalOpen"
            :other-schools-selected="selectedSchoolsOther"
            :existed-request="requestExistsInDatabase"
        ></buttons-request>
        <hr>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>ΠΡΟΣΟΧΗ!</strong>
                <ul>
                    <li>
                        Το <i class="fa fa-star priority"></i> δηλώνει την κύρια τοποθέτηση (covid) και πρέπει να δηλωθεί υποχρεωτικά.
                    </li>
                    <li>
                        Πρέπει να δηλώσετε όλα τα Σχολεία που υπάρχουν στη λίστα με τη σειρά προτίμησης
                    </li>
                </ul>                
        </div>
        <teacher-module
            v-on:refresh-selected-schools="updateOtherSelectedSchools"
            v-on:refresh-description="updateOtherDescription"
            :selected-schools="selectedSchoolsOther"
            :max-selections="50"
            :all-schools="allSchoolsOther"
            :team-checked="true"
            label="apospasi_ektos"
            :description="description_other"
            :covid="isCovid"
        ></teacher-module>
   </div> <!-- v-if show showApospasi Ektos  module -->
</div>