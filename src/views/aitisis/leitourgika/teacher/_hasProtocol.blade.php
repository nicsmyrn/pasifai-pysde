    <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 20px;">
           <h3 class="text-center">
               Η αίτηση έχει υποβληθεί και δεν μπορεί να αλλάξει. Πηγαίνετε στο ιστορικό αρχείων για εμφάνιση της αίτησής σας.
               Εαν επιθυμείτε να υποβάλλετε εκ νέου πατήστε
               <a href="{{ route('Request::create', $type) }}" class="btn btn-success">
                    ΝΕΑ ΑΙΤΗΣΗ {{ $type }}
               </a>
           </h3>
    </div>