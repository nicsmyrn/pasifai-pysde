@extends('app')

@section('header.style')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
            [v-cloak] {
              display: none;
            }

            .tooltip {
              display: block !important;
              z-index: 10000;
            }

            .tooltip .tooltip-inner {
              background: black;
              color: white;
              border-radius: 16px;
              padding: 5px 10px 4px;
            }

            .tooltip .tooltip-arrow {
              width: 0;
              height: 0;
              border-style: solid;
              position: absolute;
              margin: 5px;
              border-color: black;
              z-index: 1;
            }

            .tooltip[x-placement^="top"] {
              margin-bottom: 5px;
            }

            .tooltip[x-placement^="top"] .tooltip-arrow {
              border-width: 5px 5px 0 5px;
              border-left-color: transparent !important;
              border-right-color: transparent !important;
              border-bottom-color: transparent !important;
              bottom: -5px;
              left: calc(50% - 5px);
              margin-top: 0;
              margin-bottom: 0;
            }

            .tooltip[x-placement^="bottom"] {
              margin-top: 5px;
            }

            .tooltip[x-placement^="bottom"] .tooltip-arrow {
              border-width: 0 5px 5px 5px;
              border-left-color: transparent !important;
              border-right-color: transparent !important;
              border-top-color: transparent !important;
              top: -5px;
              left: calc(50% - 5px);
              margin-top: 0;
              margin-bottom: 0;
            }

            .tooltip[x-placement^="right"] {
              margin-left: 5px;
            }

            .tooltip[x-placement^="right"] .tooltip-arrow {
              border-width: 5px 5px 5px 0;
              border-left-color: transparent !important;
              border-top-color: transparent !important;
              border-bottom-color: transparent !important;
              left: -5px;
              top: calc(50% - 5px);
              margin-left: 0;
              margin-right: 0;
            }

            .tooltip[x-placement^="left"] {
              margin-right: 5px;
            }

            .tooltip[x-placement^="left"] .tooltip-arrow {
              border-width: 5px 0 5px 5px;
              border-top-color: transparent !important;
              border-right-color: transparent !important;
              border-bottom-color: transparent !important;
              right: -5px;
              top: calc(50% - 5px);
              margin-left: 0;
              margin-right: 0;
            }

            .tooltip.popover .popover-inner {
              background: #f9f9f9;
              color: black;
              padding: 24px;
              border-radius: 5px;
              box-shadow: 0 5px 30px rgba(black, .1);
            }

            .tooltip.popover .popover-arrow {
              border-color: #f9f9f9;
            }

            .tooltip[aria-hidden='true'] {
              visibility: hidden;
              opacity: 0;
              transition: opacity .15s, visibility .15s;
            }

            .tooltip[aria-hidden='false'] {
              visibility: visible;
              opacity: 1;
              transition: opacity .15s;
            }
    </style>
@endsection

@section('loader')
    @include('vueloader')
@endsection

@section('content')
    @include('pysde::aitisis.leitourgika.teacher._notifications')

    <h2 id="request_header_h2" class="text-center">
        <span class="label label-default">Φόρμα Αίτησης {{$type}}</span>
    </h2>


    <div class="row">
        <div class="col-md-3">
            @include('pysde::aitisis._form._profile')
        </div>

            <div v-if="loader" class="col-md-9">

               <h1 class="text-center" id="loader">
                   <i class="fa fa-spinner fa-spin fa-2x"></i>
                   <p>
                       <b><i>Φορτώνει τα δεδομένα...</i></b>
                   </p>
               </h1>
            </div>

            <div v-cloak v-else>
                @if($type == 'E1')
                    @include('pysde::aitisis.leitourgika.teacher.E1')
                @elseif($type == 'E2')
                    @include('pysde::aitisis.leitourgika.teacher.E2')
                @elseif($type == 'E3')
                    @include('pysde::aitisis.leitourgika.teacher.E3')
                @elseif($type == 'E4')
                    @include('pysde::aitisis.leitourgika.teacher.E4')
                @elseif($type == 'E5')
                    @include('pysde::aitisis.leitourgika.teacher.E5')
                @elseif($type == 'E6')
                    @include('pysde::aitisis.leitourgika.teacher.E6')
                @endif
            </div> <!-- v-else loader -->

    </div>


    <modal
        v-on:cancel-the-request="cancelOperation"
        v-on:emit-the-request="deleteRequestOperation"
        v-if="showDeleteModal"
        :show="openDeleteModal"
        :loader="hideLoader"
        type="delete"
    >
        <h5 slot="title">ΠΡΟΣΟΧΗ!</h5>
        <p slot="body">
            Επιλέξατε να διαγράψετε Οριστικά την αίτησή σας. Είστε σίγουρος;
        </p>
        <span slot="button">ΝΑΙ, είμαι σίγουρος</span>
    </modal>


    <modal
        v-on:cancel-the-request="cancelOperation"
        v-on:emit-the-request="sentRequest"
        :show="openAgreementModal"
        :loader="hideLoader"
        type="agreement"
    >
        <h5 slot="title">Όροι και προϋποθέσεις</h5>
        <ol slot="body">
                <li>
                    Τα στοιχεία που δηλώνονται στην αίτηση  έχουν την έννοια της <strong>υπεύθυνης δήλωσης</strong>,
                    με βάση τα οριζόμενα στο άρθρο του Ν. 1599/86 και <u>ψευδής δήλωση συνεπάγεται κυρώσεις</u>
                    που προβλέπονται από την παράγραφο 6 του άρθρου 22 του ίδιου νόμου.
                </li>
                <li>
                    Με την αποστολή, η αίτηση καταχωρείται με αύξων αριθμό και στέλνεται αντίγραφο (E-mail) στη γραμματεία
                    του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο. Σε περίπτωση που δεν έχετε λάβει αύξων αριθμό ή E-mail επικοινωνήστε,
                    με τη Γραμματεία του ΠΥΣΔΕ.
                </li>
                <li>
                    Αν συμφωνείτε με τους παραπάνω όρους, πατήστε το κουμπί της Αποστολής
                </li>
        </ol>
        <span slot="button">Αποστολή</span>
    </modal>

    <alert ref="alert"></alert>

@endsection

@section('scripts.footer')
    <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script>

    <script>
        window.UniqueId = "{{ $unique_id }}";
        window.RequestType = "{{$type}}";
        window.urlArchives = "{{route('Request::archives')}}";
    </script>

    <script src="{{ mix('vendor/pysde/js/pysde.js') }}"></script>
@endsection