    @include('pysde::aitisis.leitourgika.teacher._foundRequest')

    <div v-else class="col-md-9">

       <div v-if="showApospasiEntosModule">
       <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Προσοχή!</strong>
                <p>Τα μόρια απόσπασης όσων έχουν Οργανική στο Νομό δεν έχουν υπολογιστεί.</p>  
                <p>
                    Τα μόρια των εκπαιδευτικών από άλλο 
                    Νομό υπολογίζονται στο Σύνολό τους (γι' αυτό το λόγο δεν είναι ενημερωμένο το Προφίλ τους). Η Εντοπιότητα και η συνυπηρέτηση
                    θα υπολογιστεί ξεχωριστά.
                </p>
            </div>
            <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Προσοχή!</strong> να δηλώσετε στις σημειώσεις, τους σοβαρούς λόγους για τους οποίους επιθυμείτε απόσπαση εντός ΠΥΣΔΕ.
            </div>
            <buttons-request
                ref="buttons"
                v-on:save-request="saveRequest"
                v-on:open-delete-modal="makeDeleteModalOpen"
                v-on:open-agreement-modal="makeAgreementModalOpen"
                :other-schools-selected="selectedSchoolsOther"
                :existed-request="requestExistsInDatabase"
            ></buttons-request>
            <hr>
            <div class="form-group">
                <h3>
                    <label>Σύζυγος Στρατιωτικού</label>
                    <select v-model="sizigos_stratiotikou" name="sizigos_stratiotikou">
                        <option v-for="k in listStay" v-bind:value="k">@{{ k == 0 ? 'ΟΧΙ' : 'ΝΑΙ' }}</option>
                    </select>
                </h3>
            </div>
            <teacher-module
                v-on:refresh-selected-schools="updateOtherSelectedSchools"
                v-on:refresh-description="updateOtherDescription"
                :selected-schools="selectedSchoolsOther"
                :all-schools="allSchoolsOther"
                :team-checked="true"
                label="apospasi_entos"
                :description="description_other"
            ></teacher-module>
       </div> <!-- v-if show showApospasi Entos Module module -->
    </div>