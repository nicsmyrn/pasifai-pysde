    @include('pysde::aitisis.leitourgika.teacher._foundRequest')


    <div v-else class="col-md-9">

       <div class="form-group">
            <label for="hours_for_request">
                Ώρες προς συμπλήρωση σε άλλο σχολείο:
                <input style="text-align: center" type="text" v-model="hours_for_request" maxlength="2" size="2" name="hours_for_request"/>
            </label>
       </div>

       <div v-if="showSiblirosiModule">
            <buttons-request
                ref="buttons"
                v-on:save-request="saveRequest"
                v-on:open-delete-modal="makeDeleteModalOpen"
                v-on:open-agreement-modal="makeAgreementModalOpen"
                :other-schools-selected="selectedSchoolsOther"
                :existed-request="requestExistsInDatabase"
            ></buttons-request>
            <hr>
            <teacher-module
                v-on:refresh-selected-schools="updateOtherSelectedSchools"
                v-on:refresh-description="updateOtherDescription"
                :selected-schools="selectedSchoolsOther"
                :all-schools="allSchoolsOther"
                :team-checked="true"
                label="siblirosi"
                :description="description_other"
            ></teacher-module>
       </div> <!-- v-if show showDiathesiModule module -->
    </div>