    <div v-if="weFoundOtherRequest" class="col-md-9">
         <div class="alert alert-danger" role="alert" style="margin-bottom: 400px; margin-top: 20px;">
                <h3 class="text-center">
                    Βρέθηκε στο αρχείο προηγούμενή σας αίτηση.
                </h3>
                <p>
                    <ol>
                        <li>
                            <button @click="goToArchives()" class="btn btn-info">
                                Άνοιγμα Αρχείου Αιτήσεων
                            </button>
                        </li>
                        <hr>
                        <li>
                            <button @click="goToUrl" class="btn btn-warning">
                                 Άνοιγμα τελευταίας χρονολογικά αποθηκευμένης αίτησης
                            </button>
                        </li>
                        <hr>
                        <li>
                            <button @click="canMakeNewRequest" class="btn btn-success">Νέα Αίτηση </button>
                            (από το Συμβούλιο του ΠΥΣΔΕ θα εξεταστεί η τελευταία πρωτοκολλημένη αίτηση)
                        </li>
                    </ol>
                </p>
         </div>
    </div>