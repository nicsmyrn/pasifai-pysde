<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._style')

</head>
<body class="page" marginwidth="0" marginheight="0">


    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._header')

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._general_information')

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._special_information')


        <p class="greekHeader3">Γ. ΔΗΛΩΣΗ ΠΡΟΤΙΜΗΣΕΩΝ</p>
        <div class="dilosi">
            Δηλώνω ότι επιθυμώ να τοποθετηθώ προσωρινά, κατά σειρά προτίμησης, σε μία από τις παρακάτω Σχολικές Μονάδες:
        </div>

       <?php
            $results_other = $request_teacher->where('aitisi_type', 'Διάθεση')->first();
        ?>

        @if($results_other != null)
            <table>
                <tr>
                    <td width="100%">
                        <table class="protimisis_table withBorders">
                            <thead>
                                <tr>
                                    <th width="5%">α/α</th>
                                    <th width="45%">Ονομασία Σχολείου</th>
                                    <th width="5%">α/α</th>
                                    <th width="45%">Ονομασία Σχολείου</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results_other->prefrences->chunk(2) as $chunk)
                                    <tr>
                                        @foreach($chunk as $prefrence)
                                            @if($chunk->count() == 1)
                                                <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                <td class="table3Label"></td>
                                                <td class="userTable3Data"></td>
                                            @else
                                                <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>

            @if($results_other->description != '')
                <table>
                    <tbody>
                        <tr><td valign="top"> Παρατηρήσεις:
                          {{  $results_other->description }}
                        </td></tr>
                    </tbody>
                </table>
            @endif


        @endif

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._footer')


</body>
</html>
