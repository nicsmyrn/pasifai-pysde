<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._style')

</head>
<body class="page" marginwidth="0" marginheight="0">


    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._header')

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._general_information')

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._special_information')

        <p class="greekHeader3">Γ. ΔΗΛΩΣΗ ΠΡΟΤΙΜΗΣΕΩΝ ΑΝΑ ΠΕΡΙΠΤΩΣΗ:</p>

        @if($request_teacher->first()->situation == 'E1')
            @if(Auth::user()->userable->teacherable->organiki_name != 'ΔΙΑΘΕΣΗ ΠΥΣΔΕ')
                <p class="greekHeader4"><span class="classBold">Περίπτωση 1 - Λειτουργικά υπάριθμοι:</span></p>
            @else
                <p class="greekHeader4"><span class="classBold">Περίπτωση 2 - Διάθεση ΠΥΣΔΕ, από μετάθεση και από μετάταξη:</span></p>
            @endif
        @endif

        <div class="dilosi">
            @if($request_teacher->first()->situation == 'E1')
                @if(Auth::user()->userable->teacherable->organiki_name != 'ΔΙΑΘΕΣΗ ΠΥΣΔΕ')
                    Παρακαλώ, σε περίπτωση που κριθώ λειτουργικά
                    @if(Auth::user()->userable->sex == 1)
                        υπεράριθμος,
                    @else
                        υπεράριθμη,
                    @endif
                    να με τοποθετήσετε προσωρινά από το <span>{{ Auth::user()->userable->teacherable->organiki_name }}</span>
                    όπου ανήκω οργανικά, σε ένα από τα παρακάτω Σχολεία ως εξής:
                @endif
            @endif

        </div>


        <?php
            $sistegazomeno = $request_teacher->first()->sistegazomeno;
        ?>

        @if($sistegazomeno)
            <p class="greekHeader4">
                α) Σε <span class="classBold">Συστεγαζόμενη Σχολική Μονάδα</span>  με το {{ Auth::user()->userable->teacherable->organiki_name }}
            </p>
        @endif

       <?php
            $results_idia_omada = $request_teacher->where('aitisi_type', 'ΥπεράριθμοςΊδιαΟμάδα')->first();
        ?>

        @if($results_idia_omada != null)


            <p class="greekHeader4">β) Σχολεία της <span class="classBold">ίδιας ομάδας</span></p>

                <table>
                    <tr>
                        <td width="100%">
                            <table class="protimisis_table withBorders">
                                <thead>
                                    <tr>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($results_idia_omada->prefrences->chunk(2) as $chunk)
                                        <tr>
                                            @foreach($chunk as $prefrence)
                                                @if($chunk->count() == 1)
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                    <td class="table3Label"></td>
                                                    <td class="userTable3Data"></td>
                                                @else
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </td>
                    </tr>
                </table>
                @if($results_idia_omada->description != '')
                    <table>
                        <tbody>
                            <tr><td valign="top"> Παρατηρήσεις:
                              {{  $results_idia_omada->description }}
                            </td></tr>
                        </tbody>
                    </table>
                @endif
        @endif

       <?php
            $results_omores = $request_teacher->where('aitisi_type', 'ΥπεράριθμοςΌμορες')->first();
        ?>


                @if($results_omores != null)


                    <p class="greekHeader4">γ)Σε περίπτωση που δεν καταστεί δυνατό να τοποθετηθώ σε ένα από την ίδια ομάδα με την οργανική μου  Σχολεία,
                     επιθυμώ την τοποθέτησή μου σε Σχολεία
                     <span class="classBold">όμορης ομάδας</span> σύμφωνα με τις παρακάτω προτιμήσεις:</p>

                        <table>
                            <tr>
                                <td width="100%">
                                    <table class="protimisis_table withBorders">
                                        <thead>
                                            <tr>
                                                <th width="5%">α/α</th>
                                                <th width="45%">Ονομασία Σχολείου</th>
                                                <th width="5%">α/α</th>
                                                <th width="45%">Ονομασία Σχολείου</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($results_omores->prefrences->chunk(2) as $chunk)
                                                <tr>
                                                    @foreach($chunk as $prefrence)
                                                        @if($chunk->count() == 1)
                                                            <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                            <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                            <td class="table3Label"></td>
                                                            <td class="userTable3Data"></td>
                                                        @else
                                                            <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                            <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                        @endif
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </td>
                            </tr>
                        </table>

                        @if($results_omores->description != '')
                            <table>
                                <tbody>
                                    <tr><td valign="top"> Παρατηρήσεις:
                                      {{  $results_omores->description }}
                                    </td></tr>
                                </tbody>
                            </table>
                        @endif

                @endif

       <?php
            $results_other = $request_teacher->where('aitisi_type', 'Διάθεση')->first();
        ?>

        @if($results_other != null)
        <p class="greekHeader4">

        @if($request_teacher->first()->situation == 'E1')
            @if(Auth::user()->userable->teacherable->organiki_name == 'ΔΙΑΘΕΣΗ ΠΥΣΔΕ')
                Δηλώνω ότι επιθυμώ να τοποθετηθώ σε ένα από τα παρακάτω, κατά σειρά προτίμησης, λειτουργικά κενά Σχολείων της Δ.Δ.Ε. Χανίων
            @else
                δ) Σε περίπτωση που δεν καταστεί δυνατό να τοποθετηθώ σε Σχολεία ίδιας ή
                όμορης ομάδας, επιθυμώ να <span class="classBold">συγκριθώ με τους υπόλοιπους εκπαιδευτικούς</span> , σύμφωνα με τις παρακάτω προτιμήσεις Σχολείων:
            @endif
        @endif


        </p>

            <table>
                <tr>
                    <td width="100%">
                        <table class="protimisis_table withBorders">
                            <thead>
                                <tr>
                                    <th width="5%">α/α</th>
                                    <th width="45%">Ονομασία Σχολείου</th>
                                    <th width="5%">α/α</th>
                                    <th width="45%">Ονομασία Σχολείου</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results_other->prefrences->chunk(2) as $chunk)
                                    <tr>
                                        @foreach($chunk as $prefrence)
                                            @if($chunk->count() == 1)
                                                <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                <td class="table3Label"></td>
                                                <td class="userTable3Data"></td>
                                            @else
                                                <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>

            @if($results_other->description != '')
                <table>
                    <tbody>
                        <tr><td valign="top"> Παρατηρήσεις:
                          {{  $results_other->description }}
                        </td></tr>
                    </tbody>
                </table>
            @endif

        @endif

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._footer')


</body>
</html>
