<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._style')

</head>
<body class="page" marginwidth="0" marginheight="0">


    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._header')

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._general_information')

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._special_information')

        <p class="greekHeader3">Γ. ΔΗΛΩΣΗ ΠΡΟΤΙΜΗΣΕΩΝ</p>
        <div class="dilosi">
            Δηλώνω ότι επιθυμώ να αποσπαστώ, κατά σειρά προτίμησης, σε μία από τις παρακάτω Σχολικές Μονάδες:
        </div>

       <?php
            $results_other = $request_teacher->where('aitisi_type', 'Διάθεση')->first();
        ?>

        @if($results_other != null)
            <table>
                <tr>
                    <td width="100%">
                        <table class="protimisis_table withBorders">
                            <thead>
                                <tr>
                                    <th width="5%">α/α</th>
                                    <th width="45%">Ονομασία Σχολείου</th>
                                    <th width="5%">α/α</th>
                                    <th width="45%">Ονομασία Σχολείου</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results_other->prefrences->chunk(2) as $chunk)
                                    <tr>
                                        @foreach($chunk as $prefrence)
                                            @if($chunk->count() == 1)
                                                <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                <td class="table3Label"></td>
                                                <td class="userTable3Data"></td>
                                            @else
                                                <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>

            @if($results_other->description != '')
                <table>
                    <tbody>
                        <tr><td valign="top"> Παρατηρήσεις:
                          {{  $results_other->description }}
                        </td></tr>
                    </tbody>
                </table>
            @endif

        @endif

        <p class="greekHeader3">Γ. ΜΟΡΙΑ ΓΙΑ ΑΠΟΣΠΑΣΗ</p>

        <table class="withBorders" id="moria_table">
            <tbody>
                <tr>
                    <td class="moria_cell">A/A</td>
                    <td class="moria_cell">ΚΡΙΤΗΡΙΑ ΓΙΑ ΑΠΟΣΠΑΣΗ</td>
                    <td class="moria_cell">ΜΟΡΙΑ</td>
                </tr>
                <?php $index = 1; ?>
                @foreach($analytika as $record)
                    @if($record['value'] > 0)
                        @if($record['name'] == 'proipiresia')
                            <tr>
                                <td class="moria_cell">{{ $index }}</td>
                                <td class="moria_cell">{{ $record['title'] }}</td>
                                <td class="moria_cell">{{ number_format($record['value'], 3, ',', '') }}</td>
                            </tr>
                        @else
                            <tr>
                                <td class="moria_cell">{{ $index }}</td>
                                <td class="moria_cell">{{ $record['title'] }}</td>
                                <td class="moria_cell">{{ $record['value'] }}</td>
                            </tr>
                        @endif
                        <?php $index = $index + 1; ?>
                    @endif
                @endforeach

                <tr>
                    <td class="moria_cell" colspan="2">ΣΥΝΟΛΟ ΜΟΡΙΩΝ</td>
                    <td class="moria_cell">
                        {{ number_format($analytika->sum('value'), 3, ',', '') }}
                    </td>
                </tr>
            </tbody>
        </table>

        @if($entopiotita > 0 || $sinipiretisi > 0)
                <ul>
                    @if($entopiotita > 0)
                        <li>Εντοπιότητα: +{!! $entopiotita !!} μόρια για τον Δήμο {!! Config::get('requests.dimos')[Auth::user()->userable->dimos_entopiotitas] !!}</li>
                    @endif
                    @if($sinipiretisi > 0)
                        <li>Συνυπηρέτηση: +{!! $sinipiretisi !!} μόρια για τον Δήμο {!! Config::get('requests.dimos')[Auth::user()->userable->dimos_sinipiretisis] !!}</li>
                    @endif
                </ul>
            </p>
        @endif

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._footer')


</body>
</html>
