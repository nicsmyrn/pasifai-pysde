        <table id="table_header">
            <tbody>
                <tr>
                    <td colspan="2">
                        <!-- <span id="aitisi_type">{{ $request_teacher->first()->situation }}</span> -->
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <div class="t1c1">ΑΙΤΗΣΗ - ΔΗΛΩΣΗ</div>
                        @if($request_teacher->first()->situation == 'E1')
                            <div class="t1c1">ΠΡΟΤΙΜΗΣΗΣ</div>
                        @elseif($request_teacher->first()->situation == 'E2')
                            <div class="t1c1">ΣΥΜΠΛΗΡΩΣΗΣ ΩΡΑΡΙΟΥ</div>
                        @elseif($request_teacher->first()->situation == 'E3')
                            <div class="t1c1">ΑΠΟΣΠΑΣΗΣ / ΤΟΠΟΘΕΤΗΣΗΣ</div>
                            <div class="t1c1">ΕΝΤΟΣ ΠΥΣΔΕ / ΑΠΟ ΑΛΛΟ ΠΥΣΔΕ</div>
                        @elseif($request_teacher->first()->situation == 'E4')
                            <div class="t1c1">ΠΡΟΣΩΡΙΝΗΣ ΤΟΠΟΘΕΤΗΣ</div>
                            <div class="t1c1">ΑΝΑΠΛΗΡΩΤΗ</div>
                        @elseif($request_teacher->first()->situation == 'E5')
                            <div class="t1c1">ΠΡΟΣΩΡΙΝΗΣ ΤΟΠΟΘΕΤΗΣ</div>
                            <div class="t1c1">ΑΝΑΠΛΗΡΩΤΗ - Codid 19</div>
                        @elseif($request_teacher->first()->situation == 'ΟργανικήΥπ')
                            <div class="t1c1">ΠΡΟΤΙΜΗΣΗΣ ΣΧΟΛΙΚΩΝ ΜΟΝΑΔΩΝ</div>
                            <div class="t1c2">ΟΡΓΑΝΙΚΑ ΥΠΕΡΑΡΙΘΜΟΥ</div>
                        @elseif($request_teacher->first()->situation == 'E6')
                            <div class="t1c1">ΝΕΟΔΙΟΡΙΣΤΟΥ</div>
                            <div class="t1c1">ΓΙΑ ΠΡΟΣΩΡΙΝΗ ΤΟΠΟΘΕΤΗΣΗ</div>
                        @endif

                    </td>

                    <td style="vertical-align: top"  width="50%">
                        <div>
                            <span id="t1c3s1">Αρ. Αίτησης:</span><span id="t1c3s2">{{ $protocol->protocol_name }}</span> 
                            <span id="t1c3s3">
                                {{ $recall_stamp }}
                            </span>
                        </div>
                        <div class="t1c4">Προς το</div>
                        <div class="t1c4">Π.Υ.Σ.Δ.Ε. Χανίων</div>
                    </td>
                </tr>
            </tbody>
        </table>