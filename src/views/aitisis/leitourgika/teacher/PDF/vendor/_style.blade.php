    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            margin: 0;
            font-size: 1em;
            text-align: justify;
        }

        table{
            width: 100%;
            margin-top: 1px;
            margin-bottom: 5px;
            border-collapse: collapse;
            font-size: 10pt;
        }

        #table_header {
            width: 100%;
            margin-top: 0px;
            margin-bottom: 0px;
            border-collapse: collapse;
            font-size: 10pt;
            text-align: center;
        }
        #moria_table{
            width: 60% !important;
            margin-top: 3px;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

        .protimisis_table{
            margin-top: 3px;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

        .withBorders td{
            border: 1px solid #000000;
        }

        .smallLabel{
            font-size: 7pt;
            text-align: left;
            font-weight: 100;
        }
        .t1c1{
            font-size: 14pt;
            font-weight: bold;
            text-align: center;
        }
        .t1c2{
            text-align: center;
            font-size: 9pt;
        }
        #t1c3s1{
            padding-left: 15px;
            font-size: 10pt;
        }

        #t1c3s2{
            font-size: 10pt;
            font-weight: bold;
        }

        #t1c3s3{
            font-size: 12pt;
            font-weight: bold;
            color: red;
        }

        .t1c4{
            text-align: center;
            font-size: 10pt;
        }

        #aitisi_type{
            font-size: 18pt;
            font-weight: bold;
            text-align: center;
            border: 2px solid #000000;
            border-radius: 10px;
            padding: 10px;
        }

        .greekHeader{
            font-size: 10pt;
            font-weight: bold;
            margin-left: 25px;
            border-bottom: 2px solid #000000;
            width: 200px;
        }

        .greekHeader3{
             font-size: 10pt;
             margin-left: 25px;
             border-bottom: 2px solid #000000;
             width: 380px;
        }
        .greekHeader4{
            font-size: 10pt;
            font-weight: bold;
            margin-left: 25px;
            margin-top: 25px;
        }

        .classBold{
            font-weight: bold;
            border-bottom: 1px solid #000000;
            margin-bottom: 1px;
        }

        .tableLabel{
            font-size: 10pt;
        }

        .userTableData{
            font-size: 10pt;
            font-weight: bold;
        }

        .table2Label{
            font-size: 10pt;
            text-align: center;
        }
        .userTable2Data{
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
        }

        .ypiresia{
            text-align: center;
            font-size: 9pt;
        }

        #moria{
            font-size: 15pt;
            font-weight: bold;
        }

        #organikiLabel{
            text-align: center;
            font-size: 10pt;
        }

        #organiki{
            font-size: 10pt;
            font-weight: bold;
            text-align: center;
        }

        #small{
            font-size: 9pt;
            font-weight: bold;
            text-align: center;   
        }

        .userDataNumbers{
            font-weight: bold;
        }

        .odigies{
            font-size: 10pt;
            padding-left: 15px;
        }

        .table3Label{
            text-align: center;
            font-size: 10pt;
        }
        .userTable3Data{
            padding-left: 15px;
            font-size: 10pt;
        }
        #dilosi{
            text-align: center;
            font-size: 10pt;
        }
        .date_signature{
            text-align: center;
        }
        #date_value{
            font-weight: bold;
            font-size: 10pt;
        }

        #descriptions{
            font-size: 10pt;
            font-weight: 400;
        }

        .dilosi{
            font-size: 10pt;
        }

        .moria_cell{
            text-align: center;
        }

        .label-small{
            text-align: center;
            font-size: 9pt;
            font-weight: 300;
        }
    </style>