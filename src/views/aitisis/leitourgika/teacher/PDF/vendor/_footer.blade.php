        <table>
            <tr>
                <td width="40%">
                </td>
                <td width="60%">
                    <div id="dilosi">Δηλώνω την ακρίβεια όλων των παραπάνω στοιχείων</div>
                    <div class="date_signature">Ημερομηνία: <span id="date_value">{{ \Carbon\Carbon::now()->format('d/m/Y H:i:s')}} </span></div>
                    <br>
                    <div class="date_signature">
                        @if($teacher->sex == 1)
                            Ο ΔΗΛΩΝ
                        @else
                            Η ΔΗΛΟΥΣΑ
                        @endif
                    </div>
                    <br>
                    <br>
                    <div class="date_signature">Υπογραφή</div>
                </td>
            </tr>
        </table>