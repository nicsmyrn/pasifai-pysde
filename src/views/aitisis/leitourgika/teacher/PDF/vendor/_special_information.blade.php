    <br>
    
<div class="greekHeader">Β. ΕΙΔΙΚΑ ΣΤΟΙΧΕΙΑ</div>

        <table cellpadding="8" class="withBorders">
            <tbody>
                @if($teacher->teacherable_type == 'App\Models\Anaplirotis')
                    <tr>
                        <td width="20%" class="table2Label"><div>ΣΕΙΡΑ ΠΙΝΑΚΑ</div></td>
                        <td width="30%" class="userTable2Data">{{ $monimos->seira_topothetisis }} </td>
                        <td width="20%" class="table2Label"><div>ΠΙΝΑΚΑΣ</div></td>
                        <td width="30%" class="userTable2Data">{{ $monimos->pinakas }}</td>
                    </tr>
                @else
                    <tr>
                        <td width="20%" class="table2Label">@if($request_teacher->first()->situation == 'E3') <div>ΜΟΡΙΑ ΑΠΟΣΠΑΣΗΣ</div><div id="small">(τα μόρια έπειτα από έλεγχο, ενδέχεται να αλλάξουν)</div> @else <div>ΜΟΡΙΑ ΜΕΤΑΘΕΣΗΣ</div>@endif</td>
                        <td width="30%" class="userTable2Data">@if($request_teacher->first()->situation == 'E3'){{ $monimos->moria_apospasis }} @else {{ $monimos->moria }} @endif</td>
                        <td width="20%" class="table2Label"><div>ΔΗΜΟΣ</div><div>ΣΥΝΥΠΗΡΕΤΗΣΗΣ * </div></td>
                        <td width="30%" class="userTable2Data">{{ \Config::get('requests.dimos')[$teacher->dimos_sinipiretisis] }}</td>
                    </tr>
                    <tr>
                        {{-- <td class="table2Label"><div>ΟΙΚΟΓΕΝΕΙΑΚΗ</div><div>ΚΑΤΑΣΤΑΣΗ * </div></td> --}}
                        {{-- <td class="userTable2Data">{{ \Config::get('requests.family_situation')[$teacher->family_situation] }}</td> --}}
                        <td class="table2Label"><div>ΔΗΜΟΣ</div><div>ΕΝΤΟΠΙΟΤΗΤΑΣ * </div><div>(τελευταία διετία)</div></td>
                        <td class="userTable2Data">{{ \Config::get('requests.dimos')[$teacher->dimos_entopiotitas] }}</td>
                        <td> <div class="table2Label">@if($request_teacher->first()->situation == 'E3')ΣΥΖΥΓΟΣ ΣΤΡΑΤΙΩΤΙΚΟΥ @else ΕΙΔΙΚΗ ΚΑΤΗΓΟΡΙΑ @endif </div></td>
                        <td class="userTable2Data">@if($request_teacher->first()->situation == 'E3'){{ $request_teacher->first()->sizigos_stratiotikou == 0 ? 'ΟΧΙ' : 'ΝΑΙ' }} @else {{ $edata->special_situation == 0?'ΟΧΙ':'ΝΑΙ' }} @endif</td>
                    </tr>
                    {{-- <tr> --}}
                        {{-- <td class="table2Label"><div>ΑΡΙΘΜΟΣ ΠΑΙΔΙΩΝ <= 18</div><div>ή ΣΠΟΥΔΑΣΤΕΣ</div><div>ΑΕΙ, ΤΕΙ, ΙΕΚ * </div></td>
                        <td class="userTable2Data">{{ $teacher->childs }}</td> --}}

                    {{-- </tr> --}}

                    @if($request_teacher->first()->situation == 'E1' || $request_teacher->first()->situation == 'E2')
                        <tr>
                            <td colspan="2" rowspan="3" class="table2Label">
                                @if($request_teacher->first()->situation == 'E1')
                                    καταθέτω την αίτηση - δήλωση προτίμησης γιατί
                                    <span class="organiki">
                                        @if($organiki_name != 'Διάθεση ΠΥΣΔΕ')
                                            είμαι ΛΕΙΤΟΥΡΓΙΚΑ ΥΠΕΡΑΡΙΘΜΟΣ
                                        @else
                                            είμαι στη ΔΙΑΘΕΣΗ του ΠΥΣΔΕ
                                        @endif
                                    </span>
                                @endif


                            </td>
                            <td colspan="2" class="odigies">
                                Υποχρεωτικό ωράριο εκπαιδευτικού: <span class="userDataNumbers">{{ $monimos->orario }}</span>
                            </td>
                        </tr>
                    @endif


                    @if($request_teacher->first()->situation == 'E1')
                        <tr>
                            <td colspan="2" class="odigies">
                                @if($organiki_name != 'Διάθεση ΠΥΣΔΕ')
                                    Ωράριο που θα έχω στην Οργανική: <span class="userDataNumbers">{{ $request_teacher->first()->hours_for_request }}</span>
                                @endif
                            </td>
                        </tr>
                    @elseif($request_teacher->first()->situation == 'E2')
                        <tr>
                            <td colspan="2" class="odigies">
                                Ώρες προς συμπλήρωση σε άλλο Σχολείο: <span class="userDataNumbers">{{ $request_teacher->first()->hours_for_request }}</span>
                            </td>
                        </tr>
                    @endif

                    @if($request_teacher->first()->situation == 'E1')
                        <tr>
                            <td colspan="2" class="odigies">
                                @if($organiki_name != 'Διάθεση ΠΥΣΔΕ')Επιθυμώ να διατηρήσω το ωράριο της οργανικής μου θέσης: <span class="userDataNumbers">{{ $request_teacher->first()->stay_to_organiki?'ΝΑΙ':'ΟΧΙ' }}</span>@endif
                            </td>
                        </tr>
                    @endif

                    @if($request_teacher->first()->situation == 'E2')
                        <tr>
                            <td colspan="4" class="odigies">
                                * Όσοι δεν κατέθεσαν Αίτηση Βελτίωσης τον Οκτώβριο 2023, να αποστείλουν τα σχετικά δικαιολογητικά (Πιστοποιητικό Οικογ. Κατάστασης, εντοπιότητας, Βεβαίωση συνυπηρέτησης και φοίτησης τέκνων) βάσει των οδηγιών που αναφέρεται στη σχετική πρόσκληση στο email: pysde@dide.chan.sch.gr
                            </td>
                        </tr>
                    @endif

                    @if($request_teacher->first()->situation == 'ΟργανικήΥπ')
                        <tr>
                            <td colspan="4" class="odigies">
                                * Όσοι δεν κατέθεσαν Αίτηση Βελτίωσης τον Οκτώβριο 2023, να αποστείλουν τα σχετικά δικαιολογητικά (Πιστοποιητικό Οικογ. Κατάστασης, εντοπιότητας, Βεβαίωση συνυπηρέτησης και φοίτησης τέκνων) βάσει των οδηγιών που αναφέρεται στη σχετική πρόσκληση στο email: pysde@dide.chan.sch.gr
                            </td>
                        </tr>
                    @endif
                @endif
            </tbody>
        </table>

