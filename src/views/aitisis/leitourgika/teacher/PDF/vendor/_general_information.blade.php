        <br>
        <div class="greekHeader">Α. ΓΕΝΙΚΑ ΣΤΟΙΧΕΙΑ</div>

        <table cellpadding="8" class="withBorders">
            <tbody>
                <tr>
                    <td width="20%" class="tableLabel">ΕΠΩΝΥΜΟ:</td>
                    <td width="30%" class="userTableData">{{ $user->last_name }}</td>
                    <td width="20%" class="tableLabel">ΟΡΓΑΝΙΚΗ ΘΕΣΗ:</td>
                    <td width="30%" class="userTableData">{{ $organiki_name }}</td>
                </tr>
                <tr>
                    <td class="tableLabel">ΟΝΟΜΑ:</td>
                    <td class="userTableData">{{ $user->first_name }}</td>
                    <td class="tableLabel">ΔΙΕΥΘΥΝΣΗ ΚΑΤΟΙΚΙΑΣ:</td>
                    <td class="userTableData">{{ $teacher->address }}</td>
                </tr>
                <tr>
                    <td class="tableLabel">ΠΑΤΡΩΝΥΜΟ:</td>
                    <td class="userTableData">{{ $teacher->middle_name }}</td>
                    <td class="tableLabel">ΠΟΛΗ:</td>
                    <td class="userTableData">{{ $teacher->city }}</td>
                </tr>
                <tr>
                    <td class="tableLabel">ΚΛΑΔΟΣ:</td>
                    <td class="userTableData">{{ $myschool->new_klados }}</td>
                    <td class="tableLabel">ΤΚ:</td>
                    <td class="userTableData">{{ $teacher->tk }}</td>
                </tr>
                <tr>
                    <td class="tableLabel">ΕΙΔΙΚΟΤΗΤΑ:</td>
                    <td class="userTableData">{{ $myschool->new_eidikotita_name }}</td>
                    <td class="tableLabel">ΤΗΛΕΦΩΝΟ:</td>
                    <td class="userTableData">{{ $teacher->phone }}</td>
                </tr>
                <tr>
                    @if($teacher->teacherable_type == 'App\Models\Anaplirotis')
                        <td class="tableLabel">ΑΦΜ</td>
                        <td class="userTableData">{{ $monimos->afm }}</td>
                    @else
                        <td class="tableLabel">ΑΡ. ΜΗΤΡΩΟΥ</td>
                        <td class="userTableData">{{ $monimos->am }}</td>
                    @endif
                    <td class="tableLabel">ΚΙΝΗΤΟ</td>
                    <td class="userTableData">{{ $teacher->mobile }}</td>
                </tr>
            </tbody>
        </table>