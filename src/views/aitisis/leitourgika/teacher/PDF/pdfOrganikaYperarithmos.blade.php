<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._style')

</head>
<body class="page" marginwidth="0" marginheight="0">

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._header')

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._general_information')

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._special_information')

    <p class="greekHeader3">Γ. ΔΗΛΩΣΗ ΠΡΟΤΙΜΗΣΕΩΝ:</p>


    <div class="dilosi">
        @if($request_teacher->first()->situation == 'ΟργανικήΥπ')
            @if($organiki_name != 'ΔΙΑΘΕΣΗ ΠΥΣΔΕ')
                Παρακαλώ,
                να με τοποθετήσετε  από το <span>{{ $organiki_name }}</span>
                όπου ανήκω οργανικά και κρίθηκα
                @if($teacher->sex == 1)
                    υπεράριθμος,
                @else
                    υπεράριθμη,
                @endif
                σε ένα από τα παρακάτω Σχολεία ως εξής:
            @endif            
        @endif

    </div>

    <?php
        $results_idia_omada = $request_teacher->where('aitisi_type', 'ΥπεράριθμοςΊδιαΟμάδα')->first();
    ?>

        @if($results_idia_omada != null)
            <p class="greekHeader4">α) Σχολεία της <span class="classBold">ίδιας ομάδας</span></p>

                <table>
                    <tr>
                        <td width="100%">
                            <table class="protimisis_table withBorders">
                                <thead>
                                    <tr>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($results_idia_omada->prefrences->chunk(2) as $chunk)
                                        <tr>
                                            @foreach($chunk as $prefrence)
                                                @if($chunk->count() == 1)
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                    <td class="table3Label"></td>
                                                    <td class="userTable3Data"></td>
                                                @else
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </td>
                    </tr>
                </table>
                @if($results_idia_omada->description != '')
                    <table>
                        <tbody>
                            <tr><td valign="top"> Παρατηρήσεις:
                              {{  $results_idia_omada->description }}
                            </td></tr>
                        </tbody>
                    </table>
                @endif
        @endif

       <?php
            $results_omores = $request_teacher->where('aitisi_type', 'ΥπεράριθμοςΌμορες')->first();
        ?>


        @if($results_omores != null)
            <p class="greekHeader4">β)Σε περίπτωση που δεν καταστεί δυνατό να τοποθετηθώ σε ένα από τα Σχολεία της ίδιας ομάδας με την οργανική μου,
                επιθυμώ την τοποθέτησή μου σε Σχολείο
                <span class="classBold">όμορης ομάδας</span> σύμφωνα με τις παρακάτω προτιμήσεις:</p>

                <table>
                    <tr>
                        <td width="100%">
                            <table class="protimisis_table withBorders">
                                <thead>
                                    <tr>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($results_omores->prefrences->chunk(2) as $chunk)
                                        <tr>
                                            @foreach($chunk as $prefrence)
                                                @if($chunk->count() == 1)
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                    <td class="table3Label"></td>
                                                    <td class="userTable3Data"></td>
                                                @else
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </td>
                    </tr>
                </table>

                @if($results_omores->description != '')
                    <table>
                        <tbody>
                            <tr><td valign="top"> Παρατηρήσεις:
                                {{  $results_omores->description }}
                            </td></tr>
                        </tbody>
                    </table>
                @endif

        @endif

        <?php
            $results_veltiwsi = $request_teacher->where('aitisi_type', 'Βελτίωση')->first();
        ?>

        @if($results_veltiwsi != null)
            <p class="greekHeader4">Δηλώνω ότι επιθυμώ να τοποθετηθώ σε ένα από τα παρακάτω, κατά σειρά προτίμησης, <span class="classBold">οργανικά κενά </span> Σχολικών Μονάδων: </p>
                <table>
                    <tr>
                        <td width="100%">
                            <table class="protimisis_table withBorders">
                                <thead>
                                    <tr>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($results_veltiwsi->prefrences->chunk(2) as $chunk)
                                        <tr>
                                            @foreach($chunk as $prefrence)
                                                @if($chunk->count() == 1)
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                    <td class="table3Label"></td>
                                                    <td class="userTable3Data"></td>
                                                @else
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </td>
                    </tr>
                </table>

                @if($results_veltiwsi->description != '')
                    <table>
                        <tbody>
                            <tr><td valign="top"> Παρατηρήσεις:
                                {{  $results_veltiwsi->description }}
                            </td></tr>
                        </tbody>
                    </table>
                @endif

        @endif

        <?php
            $results_others = $request_teacher->whereIn('aitisi_type', ['E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7'])->first();
            if($results_others != null){
                $aitisi_type = $results_others->aitisi_type;
            }else{
                $aitisi_type = 'unkown';
            }
        ?>

        @if($results_others != null)
            <p class="greekHeader4">
                @if($aitisi_type == 'E1')
                    @if($organiki_name != 'Διάθεση ΠΥΣΔΕ')
                        γ) Σε περίπτωση που δεν τοποθετηθώ ως υπεράριθμος σε κάποιο λειτουργικό κενό, θα συγκριθώ στην 2η φάση τοποθετήσεων, μαζί με όσους βρίσκονται στη Διάθεση του ΠΥΣΔΕ και με αυτούς που ήρθαν από μετάθεση. Για το λόγο αυτό επιθυμώ να τοποθετηθώ σε μία από τις παρακάτω, κατά σειρά προτίμησης, Σχολικές Μονάδες:
                    @else
                        Επιθυμώ να τοποθετηθώ σε μία από τις παρακάτω, κατά σειρά προτίμησης, Σχολικές Μονάδες:
                    @endif
                @elseif($aitisi_type == 'E2')
                    Παρακαλώ να με διαθέσετε για συμπλήρωση του υποχρεωτικού μου ωραρίου, με την αντίστοιχη σειρά προτίμησης, σε ένα από τα παρακάτω Σχολεία:
                @elseif($aitisi_type == 'E3')
                    Δηλώνω ότι επιθυμώ να τοποθετηθώ/αποσπαστώ σε ένα από τα παρακάτω, κατά σειρά προτίμησης, σχολεία της ΔΔΕ Χανίων:
                @elseif($aitisi_type == 'E4')
                    Παρακαλώ να με τοποθετήσετε, σύμφωνα με την αντίστοιχη σειρά προτίμησης και για το τρέχον Σχολικό έτος, σε ένα από τα παρακάτω Σχολεία:   
                @elseif($aitisi_type == 'E5')
                    Παρακαλώ να με τοποθετήσετε, σύμφωνα με την αντίστοιχη σειρά προτίμησης και για το τρέχον Σχολικό έτος, στα παρακάτω Σχολεία:                            
                @elseif($aitisi_type == 'E6')
                    Παρακαλώ να με τοποθετήσετε, σύμφωνα με την αντίστοιχη σειρά προτίμησης και για το τρέχον Σχολικό έτος, στα παρακάτω Σχολεία:                            
                @endif
            </p>
                <table>
                    <tr>
                        <td width="100%">
                            <table class="protimisis_table withBorders">
                                <thead>
                                    <tr>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                        <th width="5%">α/α</th>
                                        <th width="45%">Ονομασία Σχολείου</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($results_others->prefrences->chunk(2) as $chunk)
                                        <tr>
                                            @foreach($chunk as $prefrence)
                                                @if($chunk->count() == 1)
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                    <td class="table3Label"></td>
                                                    <td class="userTable3Data"></td>
                                                @else
                                                    <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                                    <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </td>
                    </tr>
                </table>

                @if($results_others->description != '')
                    <table>
                        <tbody>
                            <tr><td valign="top"> Παρατηρήσεις:
                                {{  $results_others->description }}
                            </td></tr>
                        </tbody>
                    </table>
                @endif

        @endif

    @include('pysde::aitisis.leitourgika.teacher.PDF.vendor._footer')


</body>
</html>
