    @include('pysde::aitisis.leitourgika.teacher._foundRequest')

    <div v-else class="col-md-9">

       <div v-if="showApospasiEktosModule">
            <buttons-request
                ref="buttons"
                v-on:save-request="saveRequest"
                v-on:open-delete-modal="makeDeleteModalOpen"
                v-on:open-agreement-modal="makeAgreementModalOpen"
                :other-schools-selected="selectedSchoolsOther"
                :existed-request="requestExistsInDatabase"
            ></buttons-request>
            <hr>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>ΣΗΜΕΙΩΣΗ!</strong>
                <p>
                    Εαν υπάρχει διαφωνία με το υποχρεωτικό σας ωράριο, <strong>αγνοήστε το</strong>. Ο έλεγχος του ωραρίου θα γίνει μέχρι την επόμενη συνεδρίαση του ΠΥΣΔΕ.
                </p>
            </div>
            <teacher-module
                v-on:refresh-selected-schools="updateOtherSelectedSchools"
                v-on:refresh-description="updateOtherDescription"
                :selected-schools="selectedSchoolsOther"
                :max-selections="50"
                :all-schools="allSchoolsOther"
                :team-checked="true"
                label="apospasi_ektos"
                :description="description_other"
            ></teacher-module>
       </div> <!-- v-if show showApospasi Ektos  module -->
    </div>