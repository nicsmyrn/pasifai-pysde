    @include('pysde::aitisis.leitourgika.teacher._foundRequest')
    <div v-else class="col-md-9">
        <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                    <label>Καταθέτω την αίτηση γιατί είμαι
                        <span style="color: red; font-size: medium; font-weight: bold">
                            @{{ currentReason == 'Διάθεση' ? ' στη ΔΙΑΘΕΣΗ του ΠΥΣΔΕ' : currentReason }}
                        </span>


               </div>
               <div v-if="requestExistsInDatabase" class="form-group">
                   <h5 class="text-danger text-center">
                       <strong>αριθμός αίτησης: {{$unique_id}}</strong>
                   </h5>
               </div>
            </div>
            <div class="col-md-12" v-if="false">
               <div class="form-group">
                    <label>Επιθυμώ να διατηρήσω το ωράριο της οργανικής μου:</label>
                    <select v-model="stay_to_organiki" @change="checkHours" name="stay_to_organiki">
                        <option value="null"></option>
                        <option v-for="k in listStay" v-bind:value="k">@{{ k == 0 ? 'ΟΧΙ' : 'ΝΑΙ' }}</option>
                    </select>
               </div>
               <div v-if="stay_to_organiki == 1" class="form-group">
                    <label for="hours_for_request">
                        Ωράριο που θα έχω στην Οργανική μου:
                        <input style="text-align: center" type="text" v-model="hours_for_request" maxlength="2" size="2" name="hours_for_request"/>
                        <span style="color: red">
                            [μέχρι 11 ώρες]
                        </span>
                    </label>
               </div>
            </div>
        </div>

       <div v-if="showYperarithmosModule">

                <div v-cloak v-if="hasMadeRequest">
                    @include('pysde::aitisis.leitourgika.teacher._hasProtocol')
                </div> <!-- v-if hasMadeRequest -->

                <div v-else>
                    <buttons-request
                        ref="buttons"
                        v-on:save-request="saveRequest"
                        v-on:open-delete-modal="makeDeleteModalOpen"
                        v-on:open-agreement-modal="makeAgreementModalOpen"
                        :selected-schools-idias-omadas="selectedSchoolsIdiasOmadas"
                        :selected-schools-omoron="selectedSchoolsOmores"
                        :other-schools-selected="selectedSchoolsOther"
                        aitisi-type="yperarithmos"
                        :existed-request="requestExistsInDatabase"
                    ></buttons-request>
                    <hr>
                    <teacher-module
                        v-on:refresh-selected-schools="updateIdiaOmadaSelectedSchools"
                        v-on:refresh-description="updateIdiaOmadaDescription"
                        :all-schools="allSchoolsIdiasOmadas"
                        :selected-schools="selectedSchoolsIdiasOmadas"
                        title="της ίδιας ομάδας"
                        label="idiaOmada"
                        :school-teams="sameTeam"
                        :team-checked="selectedSchoolsIdiasOmadas.length > 0"
                        :description="description_idia"
                    ></teacher-module>
                    <hr>
                    <teacher-module
                        v-on:refresh-selected-schools="updateOmoresSelectedSchools"
                        v-on:refresh-description="updateOmoresDescription"
                        :selected-schools="selectedSchoolsOmores"
                        :all-schools="allSchoolsOmoron"
                        title="όμορης ομάδας"
                        label="omores"
                        :school-teams="omoresTeams"
                        :team-checked="selectedSchoolsOmores.length > 0"
                        :description="description_omores"
                    ></teacher-module>
                    <hr>
                    <teacher-module
                        v-on:refresh-selected-schools="updateOtherSelectedSchools"
                        v-on:refresh-description="updateOtherDescription"
                        :selected-schools="selectedSchoolsOther"
                        :all-schools="allSchoolsOther"
                        title="όλα τα σχολεία του Νομού"
                        label="other"
                        :team-checked="selectedSchoolsOther.length > 0"
                        :description="description_other"
                    ></teacher-module>
                </div> <!-- v-else hasMadeRequest -->


       </div> <!-- v-if show Yperarithmos Module -->

       <div v-if="showDiathesiModule">
               <div v-cloak v-if="hasMadeRequest">
                    @include('pysde::aitisis.leitourgika.teacher._hasProtocol')
               </div> <!-- v-if hasMadeRequest -->

               <div v-else>
                   <buttons-request
                       ref="buttons"
                       v-on:save-request="saveRequest"
                       v-on:open-delete-modal="makeDeleteModalOpen"
                       v-on:open-agreement-modal="makeAgreementModalOpen"
                       :other-schools-selected="selectedSchoolsOther"
                       :existed-request="requestExistsInDatabase"
                   ></buttons-request>
                   <hr>
                   <teacher-module
                       v-on:refresh-selected-schools="updateOtherSelectedSchools"
                       v-on:refresh-description="updateOtherDescription"
                       :selected-schools="selectedSchoolsOther"
                       :all-schools="allSchoolsOther"
                       :team-checked="selectedSchoolsOther.length > 0"
                       title="στη Διάθεση"
                       label="diathesi"
                       :description="description_other"
                   ></teacher-module>
               </div>

       </div> <!-- v-if show showDiathesiModule module -->
    </div>