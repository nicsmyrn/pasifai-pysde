<table id="requests" class="table table-bordered table-hover" cellspacing="0" width="100%">
    <thead>
        <tr class="active">
            <th class="text-center">Κωδικός Αίτησης</th>
            <th class="text-center">Ημ/νια</th>
            <th class="text-center">Αρ. Αίτησης</th>
            <th class="text-center">Είδος</th>
            <th class="text-center">Παρατηρήσεις</th>
            <th class="text-center">Ενέργειες</th>
        </tr>
    </thead>

    <tbody>
        <tr v-for="request in requests">
            <td class="text-center">
                <span v-if="request.protocol_number == null">
                    <a :href="request.url"> @{{ request.unique_id }}</a>
                </span>
                <span v-else>
                    @{{ request.unique_id }}
                </span>
            </td>
            <td class="text-center">
                @{{ request.date }}
            </td>
            <td class="text-center">
                <span v-if="request.protocol_number != null">
                    @{{ request.protocol_name }}
                </span>
            </td>
            <td class="text-center">
                @{{ request.type }}
            </td>
            <td class="text-center">
                <span v-if="request.protocol_number == null">
                    Σε εκκρεμότητα
                </span>
                <span v-else>
                    Ολοκληρωμένη
                    <span v-show="request.recall_situation != null">
                        <span class="label label-danger" v-if="request.recall_situation === 'Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ'">
                            @{{ request.recall_situation }}
                        </span>
                        <span class="label label-success" v-if="request.recall_situation === 'Η ΑΙΤΗΣΗ ΑΝΑΚΛΗΘΗΚΕ'">
                             @{{ request.recall_situation }}
                        </span>
                        <span class="label label-warning" v-if="request.recall_situation === 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ'">
                             @{{ request.recall_situation }}
                        </span>
                    </span>
                </span>
            </td>
            <td class="text-center">
                <span v-if="request.protocol_number == null">
                    <button @click="destroy(request.unique_id)" type="button" title="Διαγραφή" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </span>

                <span v-else>
                    <a :href="request.downloadUrl"><img style="width: 22px; height: 22px" src="/img/download_icon.jpg"/> </a>

                    <span v-if="request.recall_date == null">
                        <button @click="recall(request.unique_id)" type="button" title="Ανάκληση Αίτησης" class="btn btn-warning btn-xs"><i class="fa fa-undo" aria-hidden="true"></i> </button>
                    </span>
                </span>
            </td>
        </tr>
    </tbody>
</table>