<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            margin: 0;
            font-size: 1em;
            text-align: justify;
        }

        table{
            width: 100%;
            margin-left: 20px;
            margin-right: 20px;
            margin-top: 3px;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

        .withBorders td{
            border: 1px solid #000000;
        }

        .t1c1{
            font-size: 22pt;
            font-weight: bold;
            text-align: center;
            letter-spacing: 5px;
        }


        #t1c3s1{
            padding-left: 15px;
            font-size: 16pt;
        }

        #t1c3s2{
            font-size: 18pt;
            font-weight: bold;
        }

        .t1c4{
            /*text-align: center;*/
            font-size: 16pt;
            font-weight: 800;
            padding-left: 30px;
        }

        #description{
            padding-left: 20px;
            line-height: 200%;
            text-align: justify;
            font-size: 14pt;
        }

        .line-data{
            font-size: 14pt;
            margin-bottom: 40px;
        }

        .data{
            font-size: 15pt;
            font-weight: bold;
        }

    </style>

</head>
<body class="page" marginwidth="0" marginheight="0">

    <table>
        <tbody>
            <tr>
                <td width="50%"></td>
                <td width="50%">
                    <div><span id="t1c3s1">Αρ. Πρωτ. :</span></div>
                    <div><span id="t1c3s2">{!! $protocol->protocol_name !!}</span> </div>
                </td>
            </tr>
            <tr>
                <td width="50%">
                    <div class="t1c1">ΑΙΤΗΣΗ</div>
                </td>
                <td width="50%">
                </td>
            </tr>
            <tr>
                <td width="50%" valign="top">
                    <br>
                    <br><br><br>
                    <div class="line-data">
                        <span class="label">ΕΠΩΝΥΜΟ:</span>
                        <span class="data">{!! $request_teacher->teacher->user->last_name !!}</span>
                    </div>
                    <div class="line-data">
                        <span class="label">ΟΝΟΜΑ:</span>
                        <span class="data">{!! $request_teacher->teacher->user->first_name !!}</span>
                    </div>
                    <div class="line-data">
                        <span class="label">ΟΝ. ΠΑΤΡΟΣ:</span>
                        <span class="data">{!! $request_teacher->teacher->middle_name !!}</span>
                    </div>
                    <div class="line-data">
                        <span class="label">ΔΙΕΥΘΥΝΣΗ:</span>
                        <span class="data">{!! $request_teacher->teacher->address !!}</span>
                    </div>
                    <div class="line-data">
                        <span class="label">ΤΗΛΕΦΩΝΟ:</span>
                        <span class="data">{!! $request_teacher->teacher->phone !!}</span>
                    </div>
                    <br><br><br>
                    <div class="line-data">
                        <span class="label">ΚΛΑΔΟΣ:</span>
                        <span class="data">{!! $request_teacher->teacher->klados_name !!} ({!! $request_teacher->teacher->eidikotita_name !!})</span>
                    </div>
                    @if($request_teacher->teacher->teacherable_type == 'App\Monimos')
                        <div class="line-data">
                            <span class="label">ΣΧΕΣΗ ΕΡΓΑΣΙΑΣ:</span>
                            <span class="data">ΜΟΝΙΜΟΣ</span>
                        </div>
                        <div class="line-data">
                            <span class="label">ΑΡΙΘΜΟΣ ΜΗΤΡΩΟΥ:</span>
                            <span class="data">{!! $request_teacher->teacher->teacherable->am !!}</span>
                        </div>
                        <div class="line-data">
                            <div><span class="label">ΟΡΓΑΝΙΚΗ ΘΕΣΗ:</span></div>
                            <div><span class="data">{!! $request_teacher->teacher->teacherable->organiki_name !!}</span></div>
                        </div>
                    @elseif($request_teacher->teacher->teacherable_type == 'App\Anaplirotis')
                        <div class="line-data">
                            <span class="label">ΣΧΕΣΗ ΕΡΓΑΣΙΑΣ:</span>
                            <span class="data">ΑΝΑΠΛΗΡΩΤΗΣ</span>
                        </div>
                        <div class="line-data">
                            <span class="label">ΑΦΜ:</span>
                            <span class="data">{!! $request_teacher->teacher->teacherable->afm !!}</span>
                        </div>
                    @endif
                    <div class="line-data">
                        <div><span class="label">ΣΧΟΛΕΙΟ ΠΟΥ ΥΠΗΡΕΤΕΙ:</span></div>
                        <div><span class="data">{!! $request_teacher->schools_that_is !!}</span></div>
                    </div>
                    <br><br><br>
                    <div class="line-data">
                        <div><span class="label">ΘΕΜΑ:</span></div>
                        <div><span class="data">{!! $request_teacher->subject !!}</span></div>
                    </div>
                    <br><br><br>
                </td>

                <td valign="top" width="50%">
                    <br><br>
                    <div class="t1c4">Προς το</div>
                    <div class="t1c4">Π.Υ.Σ.Δ.Ε. Χανίων</div>
                    <br><br>
                    <div id="description">
                        Παρακαλώ {!! $request_teacher->description !!}
                    </div>
                </td>
            </tr>

            <tr>
                <td width="50%">
                    <div class="line-data">
                        <span class="label">ΧΑΝΙΑ</span>
                        <span class="data">{!! $request_teacher->date_request !!}</span>
                    </div>
                </td>
                <td width="50%" align="center">
                    Ο ΑΙΤΩΝ
                </td>
            </tr>
        </tbody>
    </table>

</body>
</html>
