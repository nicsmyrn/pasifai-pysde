<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            margin: 0;
            font-size: 1em;
            text-align: justify;
        }

        table{
            width: 100%;
            margin-top: 3px;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

        .withBorders td{
            border: 1px solid #000000;
        }

        .t1c1{
            font-size: 20pt;
            font-weight: bold;
            text-align: center;
        }
        .t1c2{
            text-align: center;
        }

        #t1c3s1{
            padding-left: 15px;
            font-size: 12pt;
        }

        #t1c3s2{
            font-size: 14pt;
            font-weight: bold;
        }

        .t1c4{
            text-align: center;
            font-size: 16pt;
            font-weight: 800;
        }

        #aitisi_type{
            font-size: 28pt;
            font-weight: bold;
            border: 1px solid #000000;
            text-align: center;
        }

        .greekHeader{
            font-size: 13pt;
            font-weight: 800;
            margin-left: 25px;
            border-bottom: 2px solid #000000;
            width: 200px;
        }

        .greekHeader3{
            font-size: 13pt;
            font-weight: 800;
            margin-left: 25px;
            border-bottom: 2px solid #000000;
            width: 380px;
        }

        .tableLabel{

        }

        .userTableData{
            font-weight: bold;
        }

        .table2Label{
            text-align: center;
        }
        .userTable2Data{
            font-weight: bold;
            text-align: center;
        }

        #moria{
            font-size: 15pt;
            font-weight: bold;
        }

        #organikiLabel{
            text-align: center;
            font-size: 10pt;
        }

        #organiki{
            font-size: 15pt;
            font-weight: bold;
            text-align: center;
        }

        .userDataNumbers{
            font-weight: bold;
        }

        .odigies{
            padding-left: 15px;
        }

        .table3Label{
            text-align: center;
        }
        .userTable3Data{
            padding-left: 15px;
        }
        #dilosi{
            text-align: center;
            font-weight: bold;
            font-size: 13pt;
        }
        .date_signature{
            text-align: center;
        }
        #date_value{
            font-weight: bold;
            font-size: 14pt;
        }

        #descriptions{

        }
    </style>

</head>
<body class="page" marginwidth="0" marginheight="0">
    @if($request_teacher->aitisi_type == 'E1')
        @include('pysde::aitisis.type.E1')
    @elseif($request_teacher->aitisi_type == 'E2')
        @include('pysde::aitisis.type.E2')
    @elseif($request_teacher->aitisi_type == 'E3')
        @include('pysde::aitisis.type.E3')
    @elseif($request_teacher->aitisi_type == 'E4')
        @include('pysde::aitisis.type.E4')
    @elseif($request_teacher->aitisi_type == 'E5')
        @include('pysde::aitisis.type.E5')
    @elseif($request_teacher->aitisi_type == 'E6')
        @include('pysde::aitisis.type.E6')
    @elseif($request_teacher->aitisi_type == 'E7')
        @include('pysde::aitisis.type.E7')
    @endif
</body>
</html>
