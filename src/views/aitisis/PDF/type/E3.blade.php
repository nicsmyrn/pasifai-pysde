    <table>
        <tbody>
            <tr>
                <td width="45%">
                    <div class="t1c1">ΑΙΤΗΣΗ - ΔΗΛΩΣΗ</div>
                    <div class="t1c1">ΑΠΟΣΠΑΣΗΣ - ΤΟΠΟΘΕΤΗΣΗΣ</div>
                    <div class="t1c1">ΕΝΤΟΣ ΠΥΣΔΕ - ΑΠΟ ΑΛΛΟ ΠΥΣΔΕ</div>
                </td>
                <td width="10%">
                    <div id="aitisi_type">Ε3</div>
                </td>
                <td width="45%">
                    <div><span id="t1c3s1">Αρ. Αίτησης:</span><span id="t1c3s2">{!! $protocol->protocol_name !!}</span> </div>
                    <div class="t1c4">Προς το</div>
                    <div class="t1c4">Π.Υ.Σ.Δ.Ε. Χανίων</div>
                </td>
            </tr>
        </tbody>
    </table>

    <br>
    <div class="greekHeader">Α. ΓΕΝΙΚΑ ΣΤΟΙΧΕΙΑ</div>

    <table cellpadding="8" class="withBorders">
        <tbody>
            <tr>
                <td width="20%" class="tableLabel">ΕΠΩΝΥΜΟ:</td>
                <td width="30%" class="userTableData">{!! $request_teacher->teacher->user->last_name !!}</td>
                <td width="20%" class="tableLabel">ΟΡΓΑΝΙΚΗ ΘΕΣΗ:</td>
                <td width="30%" class="userTableData">{!! $request_teacher->school_organiki !!}</td>
            </tr>
            <tr>
                <td class="tableLabel">ΟΝΟΜΑ:</td>
                <td class="userTableData">{!! $request_teacher->teacher->user->first_name !!}</td>
                <td class="tableLabel">ΔΙΕΥΘΥΝΣΗ ΚΑΤΟΙΚΙΑΣ:</td>
                <td class="userTableData">{!! $request_teacher->teacher->address !!}</td>
            </tr>
            <tr>
                <td class="tableLabel">ΠΑΤΡΩΝΥΜΟ:</td>
                <td class="userTableData">{!! $request_teacher->teacher->middle_name !!}</td>
                <td class="tableLabel">ΠΟΛΗ:</td>
                <td class="userTableData">{!! $request_teacher->teacher->city !!}</td>
            </tr>
            <tr>
                <td class="tableLabel">ΚΛΑΔΟΣ:</td>
                <td class="userTableData">{!! $request_teacher->teacher->klados_name !!}</td>
                <td class="tableLabel">ΤΚ:</td>
                <td class="userTableData">{!! $request_teacher->teacher->tk !!}</td>
            </tr>
            <tr>
                <td class="tableLabel">ΕΙΔΙΚΟΤΗΤΑ:</td>
                <td class="userTableData">{!! $request_teacher->teacher->eidikotita_name  !!}</td>
                <td class="tableLabel">ΤΗΛΕΦΩΝΟ:</td>
                <td class="userTableData">{!! $request_teacher->teacher->phone !!}</td>
            </tr>
            <tr>
                <td class="tableLabel">ΑΡ. ΜΗΤΡΩΟΥ</td>
                <td class="userTableData">{!! $request_teacher->teacher->teacherable->am !!}</td>
                <td class="tableLabel">ΚΙΝΗΤΟ</td>
                <td class="userTableData">{!! $request_teacher->teacher->mobile !!}</td>
            </tr>
        </tbody>
    </table>

    <div class="greekHeader">Β. ΕΙΔΙΚΑ ΣΤΟΙΧΕΙΑ</div>

    <table class="withBorders">
        <tbody>
            <tr>
                <td width="20%" class="table2Label">ΜΟΡΙΑ ΑΠΟΣΠΑΣΗΣ:</td>
                <td width="20%" class="userTable2Data">
                    {!! 666 !!}
                </td>
                <td width="20%" class="table2Label">
                    <div>ΔΗΜΟΣ</div>
                    <div>ΣΥΝΥΠΗΡΕΤΗΣΗΣ</div>
                </td>
                <td width="40%" class="userTable2Data">{!! \Config::get('requests.dimos')[$request_teacher->teacher->dimos_sinipiretisis] !!}</td>
            </tr>
            <tr>
                <td class="table2Label">
                    <div>ΟΙΚΟΓΕΝΕΙΑΚΗ</div>
                    <div>ΚΑΤΑΣΤΑΣΗ</div>
                </td>
                <td class="userTable2Data">{!! \Config::get('requests.family_situation')[$request_teacher->teacher->family_situation] !!}</td>
                <td class="table2Label">
                    <div>ΔΗΜΟΣ</div>
                    <div>ΕΝΤΟΠΙΟΤΗΤΑΣ</div>
                    <div>(τελευταία διετία)</div>
                </td>
                <td class="userTable2Data">{!! \Config::get('requests.dimos')[$request_teacher->teacher->dimos_entopiotitas] !!}</td>
            </tr>
            <tr>
                <td class="table2Label">
                    <div>ΑΡΙΘΜΟΣ ΠΑΙΔΙΩΝ <=</div>
                    <div>18 ή ΣΠΟΥΔΑΣΤΕ</div>
                    <div>ΑΕΙ, ΤΕΙ, ΙΕΚ</div>
                </td>
                <td class="userTable2Data">{!! $request_teacher->teacher->childs !!}</td>
                <td>
                    <div class="table2Label">Υπ. Ωράριο</div>
                </td>
                <td class="userTable2Data">{!! 22 !!}</td>
            </tr>
            <tr>
                <td colspan="2" rowspan="3" class="table2Label">ΣΥΝΟΛΟ ΜΟΡΙΩΝ: <span id="moria">{!! $request_teacher->teacher->teacherable->moria !!}</span></td>
                <td colspan="2" class="odigies">Ωράριο εκπαιδευτικού: <span class="userDataNumbers">{!! $request_teacher->teacher->teacherable->orario !!}</span> </td>
            </tr>
        </tbody>
    </table>

    <p class="greekHeader3">Γ. ΔΗΛΩΣΗ ΠΡΟΤΙΜΗΣΕΩΝ</p>
    <div>Δηλώνω ότι επιθυμώ να τοποθετηθώ σε ένα από τα παρακάτω, κατά σειρά προτίμησης, λειτουργικά κενά σχολείων της Δ.Δ.Ε. Χανίων:</div>

    <table class="withBorders">
        <thead>
            <tr>
                <th width="5%">α/α</th>
                <th width="45%">Ονομασία Σχολείου</th>
                <th width="5%">α/α</th>
                <th width="45%">Ονομασία Σχολείου</th>
            </tr>
        </thead>
        <tbody>
            @foreach($request_teacher->prefrences->chunk(2) as $chunk)
                <tr>
                    @foreach($chunk as $prefrence)
                        @if($chunk->count() == 1)
                            <td class="table3Label">{!! $prefrence->order_number !!}</td>
                            <td class="userTable3Data">{!! $prefrence->school_name !!}</td>
                            <td class="table3Label"></td>
                            <td class="userTable3Data"></td>
                        @else
                            <td class="table3Label">{!! $prefrence->order_number !!}</td>
                            <td class="userTable3Data">{!! $prefrence->school_name !!}</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>

    @if($request_teacher->description != '')
        <p class="greekHeader">Δ. ΣΗΜΕΙΩΣΕΙΣ</p>
        <p id="descriptions">
           {!! $request_teacher->description !!}
        </p>
    @endif
    <table>
        <tr>
            <td width="40%">
                <div class="odigies">(Όσοι δεν κατέθεσαν τα σχετικά δικαιολογητικά</div>
                <div class="odigies">στις μεταθέσεις/βελτιώσεις του προηγούμενου σχολικού έτους, να τα προσκομίσουν στα γραφεία της Δ.Δ.Ε. Χανίων)</div>
            </td>
            <td width="60%">
                <div id="dilosi">Δηλώνω την ακρίβεια όλων των παραπάνω στοιχείων</div>
                <div class="date_signature">Ημερομηνία: <span id="date_value">{!! \Carbon\Carbon::now()->format('d/m/Y')  !!} </span></div>
                <br>
                <div class="date_signature">Ο/Η ΔΗΛΩΝ / ΔΗΛΟΥΣΑ</div>
                <br>
                <br>
                <div class="date_signature">Υπογραφή</div>
            </td>
        </tr>
    </table>