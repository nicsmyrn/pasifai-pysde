    @if(Auth::user()->userable->myschool->has_yperarithmia)
        <div id="requests">
                <div class="row">
                    <div class="alert alert-danger alert-dismissible col-md-6 col-md-offset-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <ul>
                            <li><h4>Για να σταλεί η Αίτηση στο ΠΥΣΔΕ θα πρέπει να: </h4>
                                <ol>
                                    <li>συμφωνείτε με τους όρους υποβολής αίτησης</li>
                                    <li>πάρετε αριθμό αίτησης ο οποίος εμφανίζεται με ένα μήνυμα επιτυχίας αποστολής</li>
                                    <li>λάβετε E-mail επιβεβαίωσης</li>
                                </ol>
                        </ul>
                    </div>
                </div>

            <h2 id="request_header_h2" class="text-center">
                <span class="label label-default">ΔΗΛΩΣΗ ΕΠΙΘΥΜΙΑΣ ΓΙΑ ΧΑΡΑΚΤΗΡΙΣΜΟ ΩΣ ΥΠΕΡΑΡΙΘΜΟΣ</span>
            </h2>

            <div class="row">
                <div class="col-md-4">
                    @include('pysde::aitisis._form._profile')
                </div>
                <h2 class="text-center">ΠΡΟΣ ΤΟ Π.Υ.Σ.Δ.Ε. Ν. ΧΑΝΙΩΝ</h2>
                <h3 style="padding: 5px; line-height: 40px;margin: 0 50px 0 50px;text-align: justify">

                    <input type="checkbox" id="checkYperarithmos" v-model="wants"/>
                    <label for="checkYperarithmos" v-if="wants" class="label label-success">ΕΠΙΘΥΜΩ</label>
                    <label for="checkYperarithmos" v-else class="label label-danger">ΔΕΝ ΕΠΙΘΥΜΩ</label>

                    <br>
                    να χαρακτηριστώ ως υπεράριθμος στο
                    <span style="font-weight: bold;font-size: 15pt">
                         {!! $organiki !!}
                    </span>
                    της περιοχής του Νομού Χανίων.
                </h3>
                <h1 class="text-center">
                <button @click="openModal" class="btn btn-primary btn-lg">
                    ΑΠΟΣΤΟΛΗ
                </button>
                </h1>
            </div>
        </div>

        <modal v-cloak
            v-on:cancel-the-request="cancelOperation"
            v-on:emit-the-request="sentRequest"
            :show="openAgreementModal"
            :loader="hideLoader"
            type="agreement"
        >
            <h5 slot="title">Όροι και προϋποθέσεις</h5>
            <ol slot="body">
                <li>
                    Τα στοιχεία που δηλώνονται στην αίτηση  έχουν την έννοια της <strong>υπεύθυνης δήλωσης</strong>,
                    με βάση τα οριζόμενα στο άρθρο του Ν. 1599/86 και <u>ψευδής δήλωση συνεπάγεται κυρώσεις</u>
                    που προβλέπονται από την παράγραφο 6 του άρθρου 22 του ίδιου νόμου.
                </li>
                <li>
                    Με την αποστολή η αίτηση καταχωρείται με αύξων αριθμό και στέλνεται αντίγραφο (E-mail) στη γραμματεία
                    του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.
                </li>
                <li>
                    Αν συμφωνείτε με τους παραπάνω όρους πατήστε <b>Συνέχεια</b>
                </li>
            </ol>
            <span slot="button">Αποστολή</span>
        </modal>


    @else
         <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 150px;">
                <h1 class="text-center">
                    Δεν έχει βγει υπεραριθμία στο Σχολείο σας για την ειδικότητά σας.
                </h1>
         </div>
    @endif