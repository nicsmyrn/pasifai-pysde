<div class="ih-item square effect15 left_to_right">
        <a href="{{route('Request::OrganikiType', $type)}}">
        <div class="img"><img src="{{ asset('/img/aitisis/'.$file.'.jpg') }}" alt="{!! $type !!}"></div>
        <div class="info">
            <h3>
                {{ $title }}
            </h3>
            <p>{{ $description }}</p>
        </div>
    </a>
</div>