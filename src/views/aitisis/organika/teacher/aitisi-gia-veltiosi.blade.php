@extends('app')

@section('title')
    Αίτηση - Δήλωση προτιμήσεων Σχολικών Μονάδων για Βελτίωση - Οριστική Τοποθέτηση
@endsection

@section('header.style')
    <style>
        [v-cloak] {
          display: none;
        }

        .listAvailableSchools{
            margin-bottom:3px;
            padding-left:10px;
            padding-right: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
            background-color: #D3D3D3;
            border-radius: 5px;
            color:#000000;
            cursor:pointer;
              -o-transition:color .1s ease-out, background 0.5s ease-in;
              -ms-transition:color .1s ease-out, background 0.5s ease-in;
              -moz-transition:color .1s ease-out, background 0.5s ease-in;
              -webkit-transition:color 1s ease-out, background 0.5s ease-in;
              transition:color .1s ease-out, background 0.5s ease-in;
        }
        .listAvailableSchools:hover{
            background-color: darkgrey;
            color: #0000ff;
            font-weight: 800;
        }

        .slideLabel{
            font-size: 12pt;
        }



        input[type=checkbox] {
        	visibility: hidden;
        }


        /* SLIDE THREE */
        .slideThree {
        	width: 80px;
        	height: 26px;
        	background: #cccccc;
        	margin-top: 10px;

        	-webkit-border-radius: 50px;
        	-moz-border-radius: 50px;
        	border-radius: 50px;
        	position: relative;


            -webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
            -moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
            box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
        }

        .slideThree:after {
        	content: 'ΌΧΙ';
        	font: 12px/26px Arial, sans-serif;
        	color: #FF0000;
        	position: absolute;
        	right: 10px;
        	z-index: 0;
        	font-weight: bold;
        	text-shadow: 1px 1px 0px rgba(255,255,255,.15);
        }

        .slideThree:before {
        	content: 'ΝΑΙ';
        	font: 14px/26px Arial, sans-serif;
        	color: #00bf00;
        	position: absolute;
        	left: 10px;
        	z-index: 0;
        	font-weight: bold;
        }

        .slideThree label {
        	display: block;
        	width: 34px;
        	height: 20px;

        	-webkit-border-radius: 50px;
        	-moz-border-radius: 50px;
        	border-radius: 50px;

        	-webkit-transition: all .4s ease;
        	-moz-transition: all .4s ease;
        	-o-transition: all .4s ease;
        	-ms-transition: all .4s ease;
        	transition: all .4s ease;
        	cursor: pointer;
        	position: absolute;
        	top: 3px;
        	left: 3px;
        	z-index: 1;



            -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
            -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
            box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
            background: #fcfff4;

        	background: -webkit-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	background: -moz-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	background: -o-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	background: -ms-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	background: linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfff4', endColorstr='#b3bead',GradientType=0 );
        }

        .slideThree input[type=checkbox]:checked + label {
        	left: 43px;
        }

    </style>
@endsection


@section('loader')
    @include('vueloader')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            @include('pysde::aitisis._form._profile')
        </div>

        <div v-if="loader" class="col-md-8">

            <h1 class="text-center" id="loader">
                <i class="fa fa-spinner fa-spin fa-2x"></i>
                <p>
                    <b><i>Φορτώνει τα δεδομένα...</i></b>
                </p>
            </h1>
        </div>

        <div v-cloak v-else class="col-md-8">

            <div v-cloak v-if="hasMadeRequest">
                <h2 class="text-center">ΑΙΤΗΣΗ ΒΕΛΤΙΩΣΗΣ - ΟΡΙΣΤΙΚΗΣ ΤΟΠΟΘΕΤΗΣΗΣ</h2>
                <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 20px;">
                        <h3 class="text-center">
                            Η αίτηση - δήλωση προτιμήσεων για βελτιώση - οριστική τοποθέτηση έχει πραγματοποιηθεί. Δεν μπορείτε
                            να υποβάλλετε εκ νέου αίτηση. Πηγαίνετε στο ιστορικό αρχείων για εμφάνιση της αίτησής σας.
                        </h3>
                </div>
            </div>

            <div v-else>
                <h4 class="text-center">
                    <u>
                        Αίτηση - Δήλωση προτιμήσεων Σχολικών Μονάδων για Βελτίωση - Οριστική Τοποθέτηση
                    </u>
                </h4>

                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p>
                        <strong>Σημείωση!</strong>
                        <ol>
                            <li>Επιλέξτε από την παρακάτω λίστα τα Σχολεία με τη σειρά που επιθυμείτε για την αίτησή σας.</li>
                            <li>Μπορείτε να κάνετε <b>ΜΟΝΟ μια</b>  φορά αίτηση.</li>
                            <li>Η αίτηση παραλαμβάνεται <b>ΜΟΝΟ</b> με το κουμπί <b>ΑΠΟΣΤΟΛΗ</b>.</li>
                        </ol>

                    </p>
                </div>

                <buttons-request
                        ref="buttons"
                        v-on:save-request="saveRequest"
                        v-on:open-delete-modal="makeDeleteModalOpen"
                        v-on:open-agreement-modal="makeAgreementModalOpen"
                        :selected-schools-idias-omadas="selectedSchoolsIdiasOmadas"
                        :selected-schools-omoron="selectedSchoolsOmores"
                        :other-schools-selected="selectedSchoolsOther"
                        aitisi-type="yperarithmos"
                        :existed-request="requestExistsInDatabase"
                    ></buttons-request>

                <hr>
                    <teacher-module
                        v-on:refresh-selected-schools="updateOtherSelectedSchools"
                        v-on:refresh-description="updateOtherDescription"
                        :all-schools="allSchoolsOther"
                        :selected-schools="selectedSchoolsOther"
                        title="ολα τα σχολεία"
                        label="veltiwsi"
                        :team-checked="true"
                        :description="description_other"
                    ></teacher-module>
            </div>
            
        </div>

    </div>
    
    @include('pysde::aitisis.organika.teacher.modals')
@endsection

@section('scripts.footer')
    <script>
        window.UniqueId = "{{ $unique_id }}";
        window.RequestType = "{{$new_type}}";
        window.urlArchives = "{{route('Request::archives')}}";
    </script>

    <script src="{{ mix('vendor/pysde/js/pysde.js') }}"></script>
@endsection