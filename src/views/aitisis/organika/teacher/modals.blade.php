<modal
        v-on:cancel-the-request="cancelOperation"
        v-on:emit-the-request="deleteRequestOperation"
        v-if="showDeleteModal"
        :show="openDeleteModal"
        :loader="hideLoader"
        type="delete"
    >
        <h5 slot="title">ΠΡΟΣΟΧΗ!</h5>
        <p slot="body">
            Επιλέξατε να διαγράψετε Οριστικά την αίτησή σας. Είστε σίγουρος;
        </p>
        <span slot="button">ΝΑΙ, είμαι σίγουρος</span>
    </modal>


    <modal
        v-on:cancel-the-request="cancelOperation"
        v-on:emit-the-request="sentRequest"
        :show="openAgreementModal"
        :loader="hideLoader"
        type="agreement"
    >
        <h5 slot="title">Όροι και προϋποθέσεις</h5>
        <ol slot="body">
                <li>
                    Τα στοιχεία που δηλώνονται στην αίτηση  έχουν την έννοια της <strong>υπεύθυνης δήλωσης</strong>,
                    με βάση τα οριζόμενα στο άρθρο του Ν. 1599/86 και <u>ψευδής δήλωση συνεπάγεται κυρώσεις</u>
                    που προβλέπονται από την παράγραφο 6 του άρθρου 22 του ίδιου νόμου.
                </li>
                <li>
                    Με την αποστολή, η αίτηση καταχωρείται με αύξων αριθμό και στέλνεται αντίγραφο (E-mail) στη γραμματεία
                    του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο. Σε περίπτωση που δεν έχετε λάβει αύξων αριθμό ή E-mail επικοινωνήστε,
                    με τη Γραμματεία του ΠΥΣΔΕ.
                </li>
                <li>
                    Αν συμφωνείτε με τους παραπάνω όρους, πατήστε το κουμπί της Αποστολής
                </li>
        </ol>
        <span slot="button">Αποστολή</span>
    </modal>

    <alert ref="alert"></alert>