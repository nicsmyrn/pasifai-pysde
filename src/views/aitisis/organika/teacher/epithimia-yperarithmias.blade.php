@extends('app')

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">
@endsection

@section('loader')
    @include('vueloader')
@endsection

@section('content')


<?php
    $current_year = \Carbon\Carbon::now()->year;
?>

    @if(Auth::user()->userable->yperarithmia_request != null)
        @if(Auth::user()->userable->yperarithmia_request->where('year', $current_year)->first() != null)
            <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 150px;">
                    <h2 class="text-center">
                        <p>
                            Έχετε στείλει αίτημα για να χαρακτηριστείτε ως Οργανικά Υπεράριθμος στο ΠΥΣΔΕ.
                        </p>
                        <p>

                        </p>
                            Μπορείτε να ανατρέξετε στο <a href="{{ route('Request::archives') }}">Ιστορικό Αιτήσεων</a>  για να δείτε το αίτημά σας.
                    </h2>
            </div>
        @else
            @include('pysde::aitisis.organika.teacher._form._yperarithmia_request')
        @endif
    @else
        @include('pysde::aitisis.organika.teacher._form._yperarithmia_request')
    @endif

@endsection

@section('scripts.footer')
    <script src="{{ mix('vendor/pysde/js/EpithimiaYperarithmias.js') }}"></script>

    <script type="text/javascript">
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>

@endsection