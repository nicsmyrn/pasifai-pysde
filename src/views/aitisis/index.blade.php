@extends('app')

@section('title')
    Όλες οι Αιτήσεις
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">

@endsection

@section('content')
    <h1 class="page-heading">Αρχείο Αιτήσεων</h1>

    <div class="row">
        <div class="col-md-12">
                <div v-cloak v-if="loading" class="panel-body text-center">
                    <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                    <h5>παρακαλώ περιμένετε...</h5>
                </div>

                <div v-cloak v-else>
                    <div v-if="requests.length">
                        @include('pysde::aitisis.tableIndex')
                    </div>
                    <div v-else>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <hr>
                                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει καμία Αίτηση</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#requests').DataTable({
                "order": [[ 1, "desc" ]],
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 4,5 ] } ],
                "language": {
                    "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                    "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                    "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                    "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                    "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                    "search": "Αναζήτηση:",
                    "paginate": {
                            "previous": "Προηγούμενη",
                            "next" : "Επόμενη"
                        }
                }
            });
        });
    </script>

    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
    </script>    
    <script src="{{ mix('vendor/pysde/js/indexTeacherRequests.js') }}"></script>

@endsection


