<div class="ih-item circle colored effect4 left_to_right">
    <a href="{{route('Request::create', $type)}}">
        <div class="img"><img src="{{ asset('/img/aitisis/'.$type.'.jpg') }}" alt="img"></div>
        <div class="info">
            <h3>{{ $type }} ΑΙΤΗΣΗ</h3>
            <p>{{ $description }}</p>
        </div>
    </a>
</div>