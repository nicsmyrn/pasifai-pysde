
<div class="ih-item circle  effect6 scale_up">
    <a href="#">
        <div class="img"><img src="{{ asset('/img/aitisis/'.$type.'.jpg') }}" alt="img"></div>
        <div class="info">
            <h3>Η {{ $type }} ΑΙΤΗΣΗ ΔΕΝ ΕΙΝΑΙ ΔΙΑΘΕΣΙΜΗ</h3>
        </div>
    </a>
</div>