@if(Auth::user()->isRole('teacher'))

    <div id="health" class="tab-pane">
    <div class="row">
            <div class="col-md-6">
                @if($real_teacher->teacherable_type == 'App\Monimos')
                    <table class="table table-user-information">
                        <tbody>
                            <tr>
                                <td class="left-col"><h3>Ειδική Κατηγορία:</h3></td>
                                <td class="right-col"><h3><b>
                                    @if($real_teacher->myschool->edata->special_situation)
                                        <span class="label label-success">
                                            ΝΑΙ
                                        </span>
                                    @else
                                        <span class="label label-danger">
                                            ΟΧΙ 
                                        </span>
                                    @endif
                                </b></h3></td>
                            </tr>


                        </tbody>
                    </table>                
                @else
                    <h4>Δεν αφορά τους Αναπληρωτές</h4>
                @endif

            </div>
            <div class="col-md-6">
                <div class="alert alert-danger text-center" role="alert">
                    <p>
                        <strong>Προσοχή!</strong>
                        Σε περίπτωση που υπάρχει διαφωνία με τα στοιχεία θα πρέπει να επικοινωνήστε
                        με το τμήμα Γ' Προσωπικού.
                    </p>
                </div>
            </div>
        </div>
    </div><!-- /#pysde -->

@endif