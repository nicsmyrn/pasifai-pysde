    <div id="pysde" class="tab-pane">

    @if($myschool->yadescription == 'ΝΕΟΔΙΟΡΙΣΤΟΣ')

        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <h5>Σημείωση!</h5>
                    <ul>
                        <li>
                            Δηλώνω υπεύθυνα ότι τα παρακάτω στοιχεία της φόρμας είναι αληθή.
                        </li>
                        <li>
                            Τα στοιχεία θα διασταυρωθούν από το τμήμα Προσωπικού.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
{{-- 
                                {{ Form::model($real_teacher,['method'=>'POST', 'action'=>'ProfileController@postNeodioristos']) }}

                                <div class="profile-feed row">
                                    <div class="col-sm-6">
                                        <div class="profile-activity clearfix">
                                            <div>
                                                <div class="form-group required" id="">
                                                    {{ Form::label('ypoxreotiko', 'Υποχρεωτικό Ωράριο:', ['class'=>'control-label']) }}
                                                    {!! Form::text('ypoxreotiko', $myschool->ypoxreotiko, ['class'=>'form-control', 'size'=> 2, 'maxlength'=> 2]) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->

                                <div class="profile-feed row">
                                    <div class="col-sm-6">
                                        <div class="profile-activity clearfix">
                                            <div>
                                                <div class="form-group required" id="">
                                                    {{ Form::label('family_situation', 'Οικογενειακή Κατάσταση:', ['class'=>'control-label']) }}
                                                    {!! Form::select('family_situation', Config::get('requests.family_situation'), $real_teacher->family_situation,['class'=>'text_real_value']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->

                                    <div class="col-sm-6">
                                        <div class="profile-activity clearfix">
                                            <div>
                                                <div class="form-group required" id="">
                                                    {!! Form::label('childs', 'Αριθμός Παιδιών (που μοριοδοτούνται):', ['class'=>'control-label']) !!}
                                                    {!! Form::text('childs',null, ['class'=>'form-control', 'size'=> 2, 'maxlength'=> 2]) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->

                                <div class="profile-feed row">
                                    <div class="col-sm-6">
                                        <div class="profile-activity clearfix">
                                            <div>
                                                <div class="form-group required" id="">
                                                    <div class="form-group required" id="">
                                                        {{ Form::label('dimos_entopiotitas', 'Δήμος Εντοπιότητας:', ['class'=>'control-label']) }}
                                                        {!! Form::select('dimos_entopiotitas', Config::get('requests.dimos'),$real_teacher->dimos_entopiotitas, ['class'=>'text_real_value']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->

                                    <div class="col-sm-6">
                                        <div class="profile-activity clearfix">
                                            <div>
                                                <div class="form-group required" id="">
                                                    {{ Form::label('dimos_sinipiretisis', 'Δήμος Συνυπηρέτησης:', ['class'=>'control-label']) }}
                                                    {!! Form::select('dimos_sinipiretisis', Config::get('requests.dimos'),$real_teacher->dimos_sinipiretisis, ['class'=>'text_real_value']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->

                                <div class="profile-feed row">
                                    <div class="col-md-12 text-center">
                                        {!! Form::submit('Αποθήκευση', ['class'=>'btn btn-success btn-submit']) !!}
                                    </div><!-- /.col -->
                                </div><!-- /.row -->

                                {{ Form::close() }}

                                <div class="space-12"></div>

                                <div class="center">

                                </div> --}}
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <table class="table table-user-information">
                    <tr>
                        <td><h3>Μόρια μετάθεσης:</h3></td>
                        <td><h3><b>
                            <span class="label label-primary">
                                {{ $real_teacher->teacherable->moria }}
                            </span>
                        </b></h3></td>
                    </tr>
                    @if($real_teacher->dimos_entopiotitas > 0)
                        <tr>
                            <td><h3>Μόρια Εντοπιότητας Δήμου {{ config('requests.dimos')[$real_teacher->dimos_entopiotitas] }}:</h3></td>
                            <td><h3><b>
                                <span class="label label-primary">
                                    {{ $real_teacher->dimos_entopiotitas > 0 ? 4 : 0}}
                                </span>
                            </b></h3></td>
                        </tr>
                    @endif

                    @if($real_teacher->dimos_sinipiretisis > 0)
                        <tr>
                            <td><h3>Μόρια Συνυπηρέτησης Δήμου {{ config('requests.dimos')[$real_teacher->dimos_sinipiretisis] }} :</h3></td>
                            <td><h3><b>
                                <span class="label label-primary">
                                    {{ $real_teacher->dimos_sinipiretisis > 0 ? 4 : 0 }}
                                </span>
                            </b></h3></td>
                        </tr>
                    @endif

                </table>
            </div>
        </div>
    @else

        <div class="row">
            <div class="col-md-6">
                @if($real_teacher->teacherable_type == 'App\Monimos')
                    <table class="table table-user-information">
                        <tbody>
                            {{-- <tr>
                                <td class="left-col">Χρόνια Προϋπηρεσίας:</td>
                                <td class="right-col"><b>
                                    {{ $real_teacher->ex_years }}
                                </b></td>
                            </tr> --}}

                            {{-- <tr>
                                <td>Μήνες Προϋπηρεσίας:</td>
                                <td><b>
                                    {{ $real_teacher->ex_months }}
                                </b></td>
                            </tr>

                            <tr>
                                <td>Ημέρες Προϋπηρεσίας:</td>
                                <td><b>
                                    {{ $real_teacher->ex_days }}
                                </b></td>
                            </tr>

                            <tr>
                                <td>Οικογενειακή Κατάσταση:</td>
                                <td><b>
                                    {{ Config::get('requests.family_situation')[$real_teacher->family_situation] }}
                                </b></td>
                            </tr>
                            <tr>
                                <td>Αριθμός Παιδιών (που μοριοδοτούνται):</td>
                                <td><b>
                                    {{ $real_teacher->childs }}
                                </b></td>
                            </tr> --}}
                            <tr>
                                <td>Δήμος Εντοπιότητας:</td>
                                <td><b>
                                    {{ Config::get('requests.dimos')[$real_teacher->dimos_entopiotitas] }}
                                </b></td>
                            </tr>
                            <tr>
                                <td>Δήμος Συνυπηρέτησης:</td>
                                <td><b>
                                    {{ Config::get('requests.dimos')[$real_teacher->dimos_sinipiretisis] }}
                                </b></td>
                            </tr>
                            <tr>
                                <td><h3>Σύνολο Μορίων (μετάθεσης) χωρίς εντοπιότητα - συνυπηρέτηση:</h3></td>
                                <td><h3><b>
                                    <span class="label label-primary">
                                        {{ $real_teacher->teacherable->moria }}
                                    </span>
                                </b></h3></td>
                            </tr>
                            <tr>
                                <td><h3>ΜΟΡΙΑ ΑΠΟΣΠΑΣΗΣ (χωρίς εντοπιότητα - συνυπηρέτηση):</h3></td>
                                <td><h3><b>
                                    <span class="label label-primary">
                                        {{ $real_teacher->teacherable->moria_apospasis }}
                                    </span>
                                </b></h3></td>
                            </tr>
                        </tbody>
                    </table>
                @elseif($real_teacher->teacherable_type == 'App\Models\Anaplirotis')
                    <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>
                                        <h3>Σειρά στον Πίνακα: <b>
                                            <span class="label label-primary">
                                                {{ $real_teacher->teacherable->seira_topothetisis }}
                                            </span>
                                        </b>
                                        </h3>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                @endif
            </div>
            <div class="col-md-6">
                <div class="alert alert-danger text-center" role="alert">
                    <p>
                        <strong>Προσοχή!</strong>
                        Σε περίπτωση που υπάρχει διαφωνία με τα στοιχεία θα πρέπει να επικοινωνήστε
                        με το τμήμα Γ' Προσωπικού.
                    </p>
                </div>
            </div>
        </div>

    @endif

    </div><!-- /#pysde -->
