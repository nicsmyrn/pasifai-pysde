@extends('app')

@section('content')

    <h1 class="text-center">Ονομαστικά Υπεράριθμοι</h1>

    @if($yperarithmoi->isEmpty())
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-danger alert-dismissible text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Προσοχή!</strong>
                    <p>
                            Δεν υπάρχουν Υπεράριθμοι για το Έτος {{ \Carbon\Carbon::now()->year }}
                    </p>
                </div>
            </div>
        </div>

    @else
        <div class="row">
            <div class="col-md-6 col-md-offset-5">
                <a class="btn btn-primary" href="{{ route('Pysde::Secretary::Teachers::exportYperarithmous') }}">
                    Εξαγωγή σε Excel
                </a>
            </div>
        </div>
        <hr>
        <div class="row">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>A/A</th>
                        <th>Ειδικότητα</th>
                        <th>ΑΜ</th>
                        <th>Ονοματεπώνυμο</th>
                        <th>Πατρώνυμο</th>
                        <th>Σχολείο</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($yperarithmoi as $key=>$yperarithmos)
                    <tr>
                        <td class="table-danger">{{ $key + 1 }}</td>
                        <td>{{ $yperarithmos->myschool->eidikotita}} </td>
                        <td>{{ $yperarithmos->myschool->am}}</td>
                        <td>{{ $yperarithmos->myschool->full_name }}</td>
                        <td>{{ $yperarithmos->myschool->middle_name}}</td>
                        <td>{{ $yperarithmos->myschool->organiki_name}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif

@endsection
