@extends('app')

@section('title')
    Αυτόματος Υπολογισμός Μορίων Αποσπάσεων
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('loader')
    @include('loader')
@endsection

@section('content')
    <h1 class="page-heading">Καθηγητές αποσπασμένοι από άλλο ΠΥΣΔΕ</h1>
    
    <div class="col-md-12 text-center">
        {!! Form::open(['id'=>'aitisiForm']) !!}
            {!! Form::submit('Υπολογισμός Μορίων',['class'=> 'btn btn-primary btn-lg' ,'id'=>'submitCalculate']) !!}
        {!! Form::close() !!}
    </div>

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {

        $('#submitCalculate').on('click', function(e){
            e.preventDefault();
            $('#loader').removeClass('invisible')
                .addClass('loading');
            setTimeout(function(){
                $('#aitisiForm').submit();
            }, 6000);
        });

            var table = $('#requests').DataTable({
                    "order": [[ 1, "asc" ]],
                    "pageLength": 50,
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 2 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection


