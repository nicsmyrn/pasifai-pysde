

@extends('app')

@section('title')
    Υπολογισμός Μορίων Απόσπασης
@stop

@section('header.style')

    <style>
            [v-cloak] {
              display: none;
            }
    </style>

@endsection

@section('content')

        <h1 class="page-heading">Υπολογισμός μορίων απόσπασης</h1>

        <div class="container" id="app">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <div class="form-group form-inline">
                        <label>Χρόνια:</label>
                        <input type="text" size="2" maxlength="2" v-model="years">
                        <label>Μήνες:</label>
                        <input type="text" size="2" maxlength="2" v-model="months">
                        <label>Ημέρες:</label>
                        <input type="text" size="2" maxlength="2" v-model="days">
                    </div>

                    <div class="form-group form-inline">
                        <label>Συνυπηρέτηση:</label>
                        <select v-model="sinipiretisi">
                            <option value="0">ΟΧΙ</option>
                            <option value="1">ΝΑΙ</option>
                        </select>

                        <label>Εντοπιότητα:</label>
                        <select v-model="entopiotita">
                            <option value="0">ΟΧΙ</option>
                            <option value="1">ΝΑΙ</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Οικογενειακοί Λόγοι:</label>
                        <select v-model="family">
                            @foreach(Config::get('requests.family_situation') as $k=>$v)
                                <option value="{!! $k !!}">
                                    {!! $v !!}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="kids">Αρ. Παιδιών:</label>
                        <input id="kids" type="text" size="2" maxlength="2" v-model="kids">
                    </div>

                    <div class="form-group checkbox">
                        <label for="sovaroi" class="control-label">
                            <input @click="changeSovaroi" type="checkbox" id="sovaroi" v-model="sovaroi">
                            Σοβαροί Λόγοι Υγείας;
                        </label>
                    </div>

                    <div v-cloak v-if="sovaroi">
                        <div class="form-group">
                            <label>Λόγοι Υγείας Ιδίου:</label>
                            <select v-model="idiou">
                                @foreach(Config::get('requests.logoi_ygeias.idiou.data') as $k=>$v)
                                    <option value="{!! $k !!}">
                                        {!! $v !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Λόγοι Υγείας Συζύγου:</label>
                            <select v-model="sizigou">
                                @foreach(Config::get('requests.logoi_ygeias.sizigou.data') as $k=>$v)
                                    <option value="{!! $k !!}">
                                        {!! $v !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Λόγοι Υγείας Παιδιών:</label>
                            <select v-model="paidion">
                                @foreach(Config::get('requests.logoi_ygeias.paidiou.data') as $k=>$v)
                                    <option value="{!! $k !!}">
                                        {!! $v !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Λόγοι Υγείας Γονέων:</label>
                            <select v-model="goneon">
                                @foreach(Config::get('requests.logoi_ygeias.goneon.data') as $k=>$v)
                                    <option value="{!! $k !!}">
                                        {!! $v !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Λόγοι Υγείας Αδερφών:</label>
                            <select v-model="aderfon">
                                @foreach(Config::get('requests.logoi_ygeias.aderfon.data') as $k=>$v)
                                    <option value="{!! $k !!}">
                                        {!! $v !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Εξωσωματική:</label>
                            <select  v-model="exosomatiki">
                                @foreach(Config::get('requests.logoi_ygeias.exosomatiki.data') as $k=>$v)
                                    <option value="{!! $k !!}">
                                        {!! $v !!}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                </div>
                <div class="col-md-4 text-center" style="vertical-align: middle">
                    <label>Σύνολο Μορίων : <span v-cloak style="color: blue"><h1>@{{ calculator }}</h1></span> </label>
                </div>
                <div class="col-md-2"></div>
            </div>

        </div>

@stop

@section('scripts.footer')
    <script src="{{ mix('vendor/pysde/js/moria.js') }}"></script>
@endsection


