@extends('app')

@section('header.style')
    <style>
        .not-belongs-to-school{
            background-color: #3cb371;
            color : white;
        }
        .loading{
            width: 80px;
            height: 80px;
        }
        .differentNumbers {
            background-color: #ff8566;
        }
        .buttons-margin{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .zero_div{
            color: red;
            text-align: center;
            padding-left: 10px;
        }

        .council-text{
            text-align: center;
            background-color: #787878;
            color: white;
        }

        .council-cell{
            text-align: center;
        }
        .pleonasma{
            color : #991f00;
            font-size: 12pt;
            font-weight: 800;
        }
        .elleima{
            color : #008000;
            font-size: 12pt;
            font-weight: 800;
        }
        .different-from-council{
            color: red;
        }
        .ok{
            color : green;
        }

        .different-from-council-school{
            color: #990000;
            background-color:#ff8566;
            font-weight: bold;
        }
        .ok-school{
            font-weight: bold;
            color : #004d00;
            background-color: #90EE90;
        }

        .is_yperarithmos{
            background-color: #90EE90;
            font-weight: bold;
        }
        .checkbox-modal{
            
        }

        .delete-button{
            color: #943126;
            font-size: 11pt;
            cursor: pointer;
            margin-left: 5px;
            margin-right: 5px;
        }

        .lock-button{
            color: #DF3A01;
            font-size: 11pt;
            margin-left: 5px;
            margin-right: 5px;
        }

        .positive-cell {
            width : 100%;
            background: greenyellow;
            display: block;
            font-weight: bold;
        }

        .negative-cell{
            width : 100%;
            background: #ff4d4d;
            display: block;
            font-weight : bold;
        }
        .more-locked {
            font-weight : bold;
            color : yellow;
        }
        .more-unlocked {
            font-weight : bold;
            color : red;
        }

        .difference-school{
            background-color: #ffffcc;
        }

        .small-text-area {
            height: 3em;
            width: 100%;
            padding: 1px;
            resize: none;
        }

        .large-text-area {
            height: 14em;
            width: 100%;
            padding: 3px;   
        }

        .div-text-area{
            white-space: nowrap; 
            overflow: hidden;
            text-overflow: ellipsis;
            width: 50%;
            padding-left: 5px;
        }



    </style>
@endsection

@section('content')
    <div id="organika_school_teachers">
        <h2 class="page-heading">
            Οργανικά Ανήκοντες στη Σχολική Μονάδα    
        <h2>

        <h5 class="text-center">
            Γενικής Παιδείας και Ειδικής Αγωγής
        </h5>

        <countdown 
            v-if="set_countdown_organika_anikontes"
            v-on:finished="countdown_finished"
            :until="countdown_for_organika_teachers">
        </countdown>

        <div class="row">

            <div id="eidikotites_table" class="col-md-12">
                <div class="panel panel-primary">

                    <div v-cloak v-if="loading" class="panel-body text-center">
                        <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                        <h5>παρακαλώ περιμένετε...</h5>
                    </div>
                    
                    <div v-cloak v-else class="panel-body">
                        <table slot="body" class="table table-bordered" id="table_eidikotites">                                
                            <thead>
                                <tr>
                                    <th align="center">Ειδικότητα</th>
                                    <th align="center">Επώνυμο</th>
                                    <th align="center">Όνομα</th>
                                    <th align="center">Πατρώνυμο</th>
                                    <th align="center">ΑΜ</th>
                                    <th align="center">Υπ. Ωράριο</th>
                                    <th align="center">Κατηγορία</th>
                                    <th align="center">Αίτηση Βελτίωσης</th>
                                    <th align="center">Επιβεβαίωση Ελέγχου</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(teacher, index) in organika_anikontes"
                                v-bind:class="{
                                    'not-belongs-to-school'      : teacher.checked
                                }"
                                >
                                    <td>
                                        @{{ teacher.eidikotita }}
                                    </td>

                                    <td>
                                        @{{ teacher.last_name }}
                                    </td>
                                    

                                    <td>
                                        @{{ teacher.first_name }}
                                    </td>

                                    <td>
                                        @{{ teacher.middle_name }}
                                    </td>

                                    <td>
                                        @{{ teacher.am }}
                                    </td>

                                    <td class="text-center">
                                        @{{ teacher.ypoxreotiko }}
                                    </td>

                                    <td>
                                        <span v-if="teacher.type === 'ΕΙΔΙΚΗΣ'" class="label label-info">
                                            @{{ teacher.type }}
                                        </span>
                                        <span v-else>
                                            @{{ teacher.type }}
                                        </span>
                                    </td>

                                    <td class="text-center">
                                        <h5>
                                            <span v-if="teacher.aitisi_veltiwsis === 'ΝΑΙ'" class="label label-danger">ΝΑΙ</span>
                                        </h5>
                                    </td>

                                    <td class="text-center">
                                        <input @change="updateTeacher(teacher)" type="checkbox" v-model="teacher.checked" />
                                    </td>

                                   
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>
            
        </div>

        <new-alert 
            ref="alert"
        ></new-alert>

    </div>
@stop

@section('scripts.footer')
    <script>
        window.base_url = "{{ url('/') }}";
        window.api_token = "{{ Auth::user()->api_token }}";
        window.countdown_for_organika_teachers = "{{ config('requests.countdown.countdown_for_organika_teachers') }}";
        window.set_countdown_organika_anikontes  = "{{ config('requests.countdown.set_countdown_organika_anikontes') }}";
    </script>
    
    <script src="{{ mix('vendor/pysde/js/organika_for_school.js') }}"></script>
@endsection