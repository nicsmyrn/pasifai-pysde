    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
    <script>

            $('.aitisis').on('change', ajaxChangeRequestPermission);

            function ajaxChangeRequestPermission(){
                $.ajax({
                    url : '/aj/at/chPrm',
                    type: 'post',
                    data : {
                        name : $(this).attr('name'),
                        checked : $(this).prop('checked'),
                        _token : $('input[name=_token]').val()
                    },
                    success : successChangeRequest,
                    error : errorChangeRequest
                })
            }

            function successChangeRequest(data){
                console.log(data);
                if (data['checked'] == 'true'){
                    $.msg(data['name'],'αίτηση ενεργοποιήθηκε', 'success', 'alert-message-'+data['name']);
                }else{
                    $.msg(data['name'],'αίτηση απενεργοποιήθηκε', 'danger', 'alert-message-'+data['name']);
                }
            }

            function errorChangeRequest(request){
                $('body').html(request.responseText);
            }
    </script>