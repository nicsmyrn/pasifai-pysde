@extends('app')

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection


@section('content')
    <h1 class="page-heading">Εξαγωγή σε Excel</h1>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @include('errors.list')
            <div class="panel panel-primary">
                <div class="panel-heading text-center">
                    <h3 class="panel-title">Δημιουργία  πίνακα Αιτήσεων - Καθηγητών</h3>
                </div>
                <div class="panel-body">
                    {{ Form::open(['route'=>'Pysde::Secretary::Teachers::downloadExcelTeachersRequest','method'=>'POST', 'class'=>'form-horizontal']) }}
                            <div class="form-group">
                                {!! Form::label('typeOfRequest', 'Αίτηση:', ['class'=>'col-md-3 control-label']) !!}
                                <div class="col-md-5">
                                    <select name="typeOfRequest" class="form-control">
                                        <option disabled selected value="Τύπος αίτησης">Επέλεξε τύπο...</option>
                                        @foreach($types as $type)
                                            <option value="{{$type}}">{{$type}}</option>    
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('excel_date_from', 'Από:', ['class'=>'col-md-3 control-label']) !!}
                                <div class="col-md-5">
                                    {!! Form::text('excel_date_from',\Carbon\Carbon::now()->format('d/m/Y'), ['class'=>'form-control text-center data_picker', 'id'=>'date_from']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('excel_date_to', 'Μέχρι:', ['class'=>'col-md-3 control-label']) !!}
                                <div class="col-md-5">
                                    {!! Form::text('excel_date_to',\Carbon\Carbon::now()->format('d/m/Y'), ['class'=>'form-control text-center data_picker', 'id'=>'date_to']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('description', 'Παρατηρήσεις', ['class'=>'col-md-3 control-label']) !!}
                                <div class="col-md-9">
                                    {!! Form::text('description', null, ['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                    {!! Form::submit('Δημιουργία Αρχείου Excel', ['class'=>'btn btn-primary']) !!}    
                            </div>
                        {{ Form::close() }}
                </div>
            </div>                
        </div>
    </div>

@stop

