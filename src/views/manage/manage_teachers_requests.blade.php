@extends('app')

@section('title')

@stop

@section('header.style')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
      .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
      .toggle.ios .toggle-handle { border-radius: 20px; }
    </style>
@endsection

@section('content')
    <h1 class="page-heading">Διαχείριση Αιτήσεων - Καθηγητών</h1>


    {{ Form::open() }}
        @foreach($teacher_permissions->chunk(3) as $chunk)
            <div class="row col-md-10 col-md-offset-1 text-center">
                @foreach($chunk as $permission)
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="checkbox-inline">
                                <img width="120px" height="120px" alt="{{ $permission->description }}" src="{{ asset('/img/aitisis/'.$permission->description.'.jpg') }}"/>
                                <span> &#8680; </span>
                                <label>
                                    {{ Form::checkbox($permission->description,1,$permission->roles->isEmpty()?false:true, [
                                            'class'=>'aitisis',
                                            'data-toggle'=> 'toggle',
                                            'data-size'=>'large',
                                            'data-style'=>'ios',
                                            'data-on'=>$permission->description,
                                            'data-off'=>$permission->description
                                        ]) }}
                                </label>
                            </div>
                            <div class="col-md-10 col-md-offset-1" id="alert-message-{{ $permission->description }}" style="position: absolute; left: 0px; top: 30px"></div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    {{ Form::close() }}
@stop


@section('scripts.footer')
    @include('pysde::manage.form._footer')
@endsection


