<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use Pasifai\Pysde\controllers\traits\VacuumTrait;

class SchoolKenaController extends Controller
{
    use VacuumTrait;

    public function __construct()
    {
        // \Auth::loginUsingId(50);        //todo MUST comment
        $this->middleware('isSchool');
    }

    public function getVacuumsForSchool()
    {
        return $this->getVacuums();
    }

    public function ajaxGetVacuumsOfSchool()
    {
        return $this->ajaxGetVacuumsForSchools();
    }

    public function getOrganikes()
    {     
        if(!config('requests.can_view_organika_anikontes'))
            abort(403);

        return view('pysde::organika.school.index_of_organika_teachers');
        
        // $teachers = collect();

        // $this->school = $this->getSchool();

        // $organikes =  $this->school->organikes;

        // if(count($organikes)){
        //     foreach($organikes as $key=>$organiki){

        //         $edata = $organiki->teacher->edata;
    
        //         if($edata != null){
        //             $aitisi_veltiwsis = $edata->aitisi_veltiwsis ? 'ΝΑΙ' : '';
        //         }else{
        //             $aitisi_veltiwsis = 'error';
        //         }
    
        //         $teachers->push([
        //             'last_name'     => $organiki->teacher->last_name,
        //             'first_name'    => $organiki->teacher->first_name,
        //             'middle_name'    => $organiki->teacher->middle_name,
        //             'am'    => $organiki->teacher->am,
        //             'eidikotita'    => $organiki->teacher->eidikotita,
        //             'topothetisi'    => $organiki->teacher->topothetisi,
        //             'ypoxreotiko'    => $organiki->teacher->ypoxreotiko,
        //             'aitisi_veltiwsis' => $aitisi_veltiwsis,
        //             'type'      => $organiki->teacher->type
        //         ]);
        //     }
        // }

        // $organikes_eidikis = $this->school->organikes_eidikis;


        // if(count($organikes_eidikis)){
        //     foreach($organikes_eidikis as $key=>$organiki){

        //         $edata = $organiki->teacher->edata;
    
        //         if($edata != null){
        //             $aitisi_veltiwsis = $edata->aitisi_veltiwsis ? 'ΝΑΙ' : '';
        //         }else{
        //             $aitisi_veltiwsis = 'error';
        //         }
    
        //         $teachers->push([
        //             'last_name'     => $organiki->teacher->last_name,
        //             'first_name'    => $organiki->teacher->first_name,
        //             'middle_name'    => $organiki->teacher->middle_name,
        //             'am'    => $organiki->teacher->am,
        //             'eidikotita'    => $organiki->teacher->eidikotita,
        //             'topothetisi'    => $organiki->teacher->topothetisi,
        //             'ypoxreotiko'    => $organiki->teacher->ypoxreotiko,
        //             'aitisi_veltiwsis' => $aitisi_veltiwsis,
        //             'type'      => $organiki->teacher->type
        //         ]);
        //     }
        // }

        
        // $teachers =  $teachers->sortBy('eidikotita')->values();

        // return view('pysde::school.organikes', compact('teachers'));
    }


}
