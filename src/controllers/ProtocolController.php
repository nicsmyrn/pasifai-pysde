<?php

namespace Pasifai\Pysde\controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Pasifai\Pysde\models\F;
use Pasifai\Pysde\models\Protocol;
use Pasifai\Pysde\models\ProtocolArchives;
use Pasifai\Pysde\repositories\ProtocolRepository;
use Pasifai\Pysde\requests\ProtocolRequest;
use Barryvdh\DomPDF\PDF;

class ProtocolController extends Controller
{
    private $pagination_number = 23;

    protected $repo;

    public function __construct(ProtocolRepository $protocolRepo)
    {
        $this->middleware('isPysdeHelper');
        $this->repo = $protocolRepo;
    }

    /**
     * Display a listing of the resource.
     *f
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->get('type');        

        $years = array_pluck(\DB::select('select distinct YEAR(p_date) as year from `protocol-archives`'), 'year');

        return view('pysde::protocol.index', compact('type', 'years'));
    }

    public function ajaxIndex(Request $request)
    {
        $type = $request->get('type');
        $year = $request->get('year');

        $protocols = $this->repo->filterProtocols($type, $year);

        return ['data'=> $protocols];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $next_protocol = (intval(Protocol::LastProtocol())+1);
        
        $fs = F::all();
        
        return view('pysde::protocol.create', compact('fs', 'next_protocol'));
    }

    public function manualCreate()
    {
        $next_protocol = (intval(Protocol::LastProtocol())+1);
        $fs = F::all();
        return view('pysde::protocol.manual', compact('fs', 'next_protocol'));
    }

    public function manualStore(ProtocolRequest $request)
    {
        $id = $request->get('id');
        $next_protocol = $request->get('next_protocol');
        $year = Carbon::createFromFormat('d/m/Y',$request->get('p_date'))->format('Y');

        if ($year == Carbon::now()->format('Y')){
            $exists = Protocol::find($id);
            if ($exists == null){
                if ($id > $next_protocol){
                    return redirect()->back()->withInput($request->all())->withErrors(['msg'=>'Το τελευταίο πρωτόκολλο που μπορείτε να χρησιμοποιήστε είναι το '. $next_protocol]);
                }
                $protocol = Protocol::create($request->all());
            }else{
                return redirect()->back()->withInput($request->all())->withErrors(['msg'=>'Το πρωτόκολλο με αριθμό: '. $id . ' υπάρχει ήδη για το έτος '. $year]);
            }
        }
        else{
            $exists = ProtocolArchives::whereYear('p_date','=', $year)->where('id', $id)->first();

            if ($exists == null){
                $protocol = ProtocolArchives::create($request->all());
            }else{
                return redirect()->back()->withInput($request->all())->withErrors(['msg'=>'Το πρωτόκολλο με αριθμό: '. $id . ' για το έτος: '.$year.' υπάρχει ήδη']);
            }
        }

        flash()->overlayS('Συγχαρητήρια', 'Ο αριθμός πρωτοκόλλου : <strong>'.$protocol->id.'</strong> με θέμα: <strong>&laquo;'.$protocol->subject.'&raquo;</strong> αποθηκεύτηκε με επιτυχία');
        return redirect()->back();
    }

    public function store(ProtocolRequest $request)
    {
        $protocol = Protocol::create($request->all());

        flash()->overlayS('Συγχαρητήρια', 'Ο αριθμός πρωτοκόλλου : <strong>'.$protocol->id.'</strong> με θέμα: <strong>&laquo;'.$protocol->subject.'&raquo;</strong> αποθηκεύτηκε με επιτυχία');
        return redirect()->back();
    }


    public function edit($id)
    {
        $protocol = Protocol::with('f')->findOrFail($id);
        $fs = F::all();
        
        return view('pysde::protocol.edit', compact('protocol', 'fs'));
    }

    public function editArchives($id)
    {        
        $protocol = ProtocolArchives::with('f')->findOrFail($id);
        $fs = F::all();
        $status = 'OLD';

        return view('pysde::protocol.edit', compact('protocol', 'fs','status'));
    }

    public function update(ProtocolRequest $request, $id)
    {        
        $this->updateProtocol($request, $id);

        return redirect()->action('\Pasifai\Pysde\controllers\ProtocolController@index');
    }


    public function updateArchives(ProtocolRequest $request, $id)
    {
        $this->updateProtocolArchives($request, $id);

        return redirect()->action('\Pasifai\Pysde\controllers\ProtocolController@index',['year'=>Carbon::createFromFormat('d/m/Y',$request->get('p_date'))->format('Y')]);
    }

    public function indexArchive(Request $request)
    {                
        $years = array_pluck(\DB::select('select distinct YEAR(p_date) as year from `protocol-archives`'), 'year');
        $year = $request->get('year');

        if($year == null || $year == Carbon::now()->format('Y')){

            $protocols = Protocol::orderBy('id')->get(['id']);

        }else{

            $protocols = ProtocolArchives::orderBy('id')->whereYear('p_date','=',$year)->get(['id']);
        }

        $array = array_pluck($protocols,'id');
        $group_array = array_chunk($array,23);

        return view('pysde::protocol.archive', compact('group_array', 'years'));
    }

    public function indexPDF()
    {
        $protocols = Protocol::with('f')->paginate($this->pagination_number);
        return view('pdf.test', compact('protocols'));
    }

    public function viewToPDF()
    {
        $protocols = Protocol::with('f')->paginate($this->pagination_number);
        // $pdf = \PDF::loadView('pdf.test',compact('protocols'));
        $pdf = \PDF::loadView('pysde::protocol.pdf.index_protocol_pdf',compact('protocols'));

        return $pdf->setPaper('A4', 'landscape')->stream();
    }

    public function viewArchivesToPDF(Request $request)
    {
        $year = $request->get('year');

        $protocols = ProtocolArchives::with('f')
            ->where('p_date', 'LIKE', $year.'%')
            ->orderBy('id')
            ->paginate($this->pagination_number);

        // return 'not set';     
            
       $pdf = \PDF::loadView('pysde::protocol.pdf.index_protocol_pdf',compact('protocols'));

       return $pdf->setPaper('A4', 'landscape')->stream();
    }

    private function updateProtocol($request, $id)
    {
        $protocol = Protocol::find($id);
        
        if (!$request->has('pending')){
            $request_all = $request->all() + ['pending'=>0];
            $protocol->update($request_all);
        }else{            
            $protocol->update($request->all());
        }

        flash()->info('Επιτυχής ενημέρωση', 'στον  αριθμό πρωτοκόλλου:'.$id. '.');

    }

    private function updateProtocolArchives($request, $id)
    {

        $protocol = ProtocolArchives::find($id);

        if (!$request->has('pending')){
            $request_all = $request->all() + ['pending'=>0];
            $protocol->update($request_all);
        }else{
            $protocol->update($request->all());

        }

        $protocol_id = $protocol->id;

        flash()->info('Επιτυχής ενημέρωση', 'στον  αριθμό πρωτοκόλλου:'.$protocol_id. '.');

    }

    private function denies($permision)
    {
        if (\Gate::denies($permision)){
            abort(403);
        }
    }
}
