<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use Pasifai\Pysde\controllers\traits\VacuumTrait;

class SchoolLeitourgikaController extends Controller
{
    use VacuumTrait;

    public function __construct()
    {
        $this->middleware('isSchool');
    }

    public function indexOfTeachers()
    {
        return view('pysde::leitourgika.school.teachers');
    }

    public function indexOfEidikotites()
    {
        return view('pysde::leitourgika.school.eidikotites');
    }

}
