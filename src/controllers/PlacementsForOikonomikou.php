<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use Auth;
use Pasifai\Pysde\repositories\PlacementRepositoryInterface;
use Pasifai\Pysde\controllers\traits\PlacementsTrait;
use Pasifai\Pysde\models\Placement;
use Illuminate\Http\Request;
use Pasifai\Pysde\models\Praxi;
use App\School;
use App\MySchoolTeacher;
use App\Models\Year;
use Artisan;
use Crypt;
use Log;
use Storage;

class PlacementsForOikonomikou extends Controller
{
    use PlacementsTrait;

    protected $user;
    protected $p;

    public function __construct(PlacementRepositoryInterface $p)
    {
        $this->middleware('isOikonomikou');
        $this->p = $p;
    }

    public function placementsDetailsOfTeacher($teacher_id)
    {        
        return $this->getIndexPlacementsOfTeacher($teacher_id);
    }

    public function deletePraxi($identifier, $afm){
        Artisan::call('file:delete',[
            'school_identifier'          => $identifier,
            'afm'    => $afm
        ]);

        return redirect()->back();
    }

    public function allPraxeisAnalipsis()
    {        
        $directory = "schools";

        $allFiles = Storage::allFiles($directory);

        $list_of_teachers = collect();

        foreach($allFiles as $file){
            $start = strrpos($file,"_") + 1;

            preg_match( '/\/\d{7}/', $file, $match);

            if(is_array($match)){
                if(array_key_exists(0, $match)){
                    $identifier = substr($match[0], 1, 7);

                    $school = School::where('identifier', $identifier)->first();
        
                    $afm = substr($file,$start,9);
        
                    $mySchoolModel = MySchoolTeacher::find($afm);
        
                    $praxi = $mySchoolModel->praxeis->where('identifier', $school->identifier)->first();
        
        
                    if($mySchoolModel != null && $school != null){
                        $list_of_teachers->push([
                            'afm'   => $afm,
                            'full_name' => $mySchoolModel->last_name . ' ' . $mySchoolModel->first_name,
                            'school'    => $school->name,
                            'school_identifier' => $school->identifier,
                            'encrypt_filename' => Crypt::encryptString($file),
                            'eidikotita'    => $mySchoolModel->eidikotita,
                            'praxi' => $praxi
                        ]);
                    }
                }
            }

        }
        
        return view('pysde::leitourgika.praxeis.all_teachers', compact('list_of_teachers'));

    }

    public function downloadPraxi($filename)
    {
        $descrypted_filename = Crypt::decryptString($filename);

        $path = config('filesystems.disks.local.root');

        $file_path = "$path/$descrypted_filename";

        $start = strrpos($descrypted_filename,"_") + 1;
        $afm = substr($descrypted_filename,$start,9);
        
        preg_match( '/\/\d{7}/', $descrypted_filename, $match);

        $identifier = substr($match[0], 1, 7);

        Log::warning("6666 Identifier : $identifier");

        if (file_exists($file_path)){

            $mySchoolModel = MySchoolTeacher::find($afm);

            $school = School::where('identifier', $identifier)->first();

            if($mySchoolModel != null && $school != null){
                $mySchoolModel->praxeis()->updateExistingPivot($school->id, [
                    'downloaded' => true
                ]);

                return response()->download($file_path, null , [
                    'Content-Length: '. filesize($file_path)
                ]);
            }
            exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');

        }else{
            exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');
        }
    }

    public function allTeachers()
    {        
        $message = 'Καθηγητές με τοποθετήρια';

        $teachers = MySchoolTeacher::whereHas('placements', function($query){
                $query->withTrashed();
        })->get();

        return view('pysde::leitourgika.placements.all_teachers', compact('teachers', 'message'));
    }

    public function byPraxi(Request $request)
    {
        $year_id =  Year::where('current', true)->first()->id;

        $praxi =  Praxi::where('year_id', $year_id)->where('decision_number', $request->get('αριθμός'))->first();
        
        $praxeis = Praxi::where('year_id', $year_id)->get();
        
        $message = '';
        $teachers = collect();

        if($praxi != null){
            $teachers = MySchoolTeacher::with(['placements'=> function($query) use($praxi){
                $query->withTrashed()->where('praxi_id', $praxi->id);
            }])
            ->get()
            ->filter(function($q){
                return count($q->placements) > 0;
            })
            ->values();
        }
        
        return view('pysde::leitourgika.placements.all_teachers_by_praxi', compact('teachers', 'message', 'praxeis', 'praxi'));
    }

    public function allByPraxi($number)
    {
        $year_id =  Year::where('current', true)->first()->id;

        $praxi = Praxi::where('decision_number', $number)
            ->where('year_id',  $year_id)
            ->first();

        return $this->allPlacementsPDFbyPraxi($praxi);
    }

    public function viewTeacherPlacementsPDF($teacher_id, $year_name)
    {
        $year_list = array();
        $yearModel = Year::where('name', $year_name)->first();


        if($yearModel != null){
            if($yearModel->current) $year_list[] = $yearModel->id;
        }else{
            $year_list = Year::where('current', false)->lists('id');
        }

        return $this->allPlacementsPDF($teacher_id, $year_list, true);    
    }

    protected function getUser()
    {
        $this->user = Auth::user();
    }

}
