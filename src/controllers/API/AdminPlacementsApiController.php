<?php

namespace Pasifai\Pysde\controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Year;
use App\School;
use Pasifai\Pysde\repositories\PlacementRepositoryInterface;
use Illuminate\Http\Request;
use App\MySchoolTeacher;
use Carbon\Carbon;
use DB;
use Log;
use Pasifai\Pysde\controllers\ExcelExports\Placements\PlacementsOfPraxi;
use Pasifai\Pysde\controllers\traits\PlacementsTrait;
use Pasifai\Pysde\models\Placement;
use Pasifai\Pysde\models\Praxi;
use Maatwebsite\Excel\Facades\Excel;
use Pasifai\Pysde\controllers\ExcelExports\Placements\OrganikesOfPraxi;

class AdminPlacementsApiController extends Controller 
{
    use PlacementsTrait;

    protected $placement;

    private $prosorini_topothetisi = false;
    private $diathesi = false;
    private  $apospasi = false;
    private  $anaklisi = false;
    private  $protovathimia = false;

    private  $placementsStringTypes = [
        'topothetisi' => [1,6],
        'diathesi'      => [3,5],
        'apospasi'      => [2,7],
        'anaklisi'      => [9],
        'pvthmia'       => [8]
    ];

    private $sort = [
        'eidikotita'    => 'asc',
        'seira'         => 'asc',
    ];

    public function __construct(PlacementRepositoryInterface $placement)
    {
        $this->middleware('auth:api'); 
        $this->placement = $placement;
    }

    public function fetchData()
    {
        $data = array();

        $data['kladoi'] = $this->placement->allNewKladoi();

        $data['allPlacementsTypes'] = config('requests.placements_type');

        return $data;
    }

    public function fetchDataForNewPlacement()
    {
        $data = array();

        $data['schools'] = $this->placement->allSchools();

        $data['special_placements'] = config('requests.special_placements');

        return $data;
    }


    public function getTeachers(Request $request)
    {
        $teacherCollection = MySchoolTeacher::with(['placements' => function($q){
            $q->with('praxi')->whereIn('praxi_id', $this->placement->praxeisList($this->placement->currentYear()));
        }])->whereIn('new_eidikotita', $this->placement->getEidikotitesList($request->get('klados_name')))
        ->get();

        $this->fetchedTeachers = collect();

        foreach($teacherCollection as $record){
            $this->createFetchedRecord($record);
        }

        return $this->fetchedTeachers->sortBy('full_name')->values()->all();
    }

    public function savePlacements(Request $request)
    {
        $data = array();

        $this->placement->createOrFindPraxi($request);

        $data['message'] = ['Οι αλλαγές αποθηκεύτηκαν με επιτυχία...'];
        
        return $data;
    }

    public function getExcelForPraxi($praxi_number, Request $request)
    {        
        $date = Carbon::now()->timestamp;
        
        $placementSheets = collect([
            'Λειτ. Τοποθέτηση & Συμπλήρωση' => collect(),
            'Λειτ. Τοποθέτηση' => collect(),
            'Συμπλήρωση Ωραρίου' => collect(),
            'Απόσπαση' => collect(),
            'Πθμια' => collect(),
            'Ανάκληση' => collect(),
            'Unknown' => collect()
        ]);

        $praxi = Praxi::with(['placements' => function($p){
            $p->where('pending', 0)  // ->where('from', '<>', 'ΑΝΑΠΛΗΡΩΤΗΣ')
                ->withTrashed();
        }])->where('decision_number', $praxi_number)
            ->where('year_id', Year::where('current', true)->first()->id)
            ->first();    

        if($praxi == null){
            return 'error';
        }
        
        $teachers =  $praxi->placements->sort($this->compareCollection())->groupBy('afm')->values(); // values() added... check if working...
        // $teachers =  $praxi->placements->sortBy('klados_id')->groupBy('afm')->values(); // values() added... check if working...

        foreach($teachers as $key=>$teacher_placements){ // $afm=>$teacher_placements
            $this->initializeTypes();

            $afm =  $teacher_placements[0]['afm'];

            $new_teacher = $this->teacherAttributes($afm);

            foreach($teacher_placements as $key=>$p){
                $this->checkTypeOfTeacher($p->placements_type);
            }
            $this->putTeacherWithPlacementToRightSheet($placementSheets, $new_teacher, $teacher_placements);
        }
        
        $file_name = 'ΠΡΑΞΗ_'.$praxi_number.'_ΤΟΠΟΘΕΤΗΣΕΙΣ_'.$date.'.xlsx';

        $praxi_title = $praxi->decision_number.'η/'. $praxi->decision_date;

        $filteredPlacementSheets = $placementSheets->filter(function($item, $key){
            return !$item->isEmpty();
        });
        
        // foreach($filteredPlacementSheets as $topothetisiType){
        //     $sorted = $topothetisiType->sortBy('eidikotita')->values();         // ????? NOT WORKING
        //     $topothetisiType = $sorted;
        // }
                
        return  Excel::download(new PlacementsOfPraxi($filteredPlacementSheets, $praxi_title), $file_name);
    }

    public function getExcelForPraxiOrganika($praxi_number, Request $request)
    {        
        $date = Carbon::now()->timestamp;
        
        $placementSheets = collect([
            'Οργανικά' => collect()
        ]);  

        $praxi = Praxi::with('organikes')->where('decision_number', $praxi_number)
            ->where('year_id', Year::where('current', true)->first()->id)
            ->first(); 

        if($praxi == null){
            return 'error';
        }
        
        // $teachers =  $praxi->organikes->sort($this->compareCollection())->groupBy('am')->values(); // values() added... check if working...

        $teachers = $praxi->organikes;

        foreach($teachers as $key=>$teacher){ // $afm=>$teacher_placements
            $new_teacher = $this->teacherAttributesOrganika($teacher);

            $placementSheets['Οργανικά']->push($new_teacher);
        }

        
        Log::warning($placementSheets['Οργανικά']);

        $file_name = 'ΠΡΑΞΗ_'.$praxi_number.'_ΟΡΓΑΝΙΚΕΣ_ΤΟΠΟΘΕΤΗΣΕΙΣ_'.$date.'.xlsx';

        $praxi_title = $praxi->decision_number.'η/'. $praxi->decision_date;

        $filteredOrganikes = $placementSheets->filter(function($item, $key){
            return !$item->isEmpty();
        });
 
        Log::warning($filteredOrganikes);

        return  Excel::download(new OrganikesOfPraxi($filteredOrganikes, $praxi_title), $file_name);
    }

    private function initializeTypes()
    {
        $this->prosorini_topothetisi = false;
        $this->diathesi = false;
        $this->apospasi = false;
        $this->anaklisi = false;
        $this->protovathimia = false;
    }

    private function checkPlacementTypeOfTeacher($type_list)
    {
        foreach ($type_list as $type) {
            if (in_array($type, $this->placementsStringTypes['topothetisi'])) {
                $this->prosorini_topothetisi = true;
            } elseif (in_array($type, $this->placementsStringTypes['diathesi'])) {
                $this->diathesi = true;
            } elseif (in_array($type, $this->placementsStringTypes['apospasi'])) {
                $this->apospasi = true;
            } elseif (in_array($type, $this->placementsStringTypes['anaklisi'])) {
                $this->anaklisi = true;
            } elseif (in_array($type, $this->placementsStringTypes['pvthmia'])) {
                $this->protovathimia = true;
            }
        }
    }

    private function checkTypeOfTeacher($type)
    {
        if (in_array($type, $this->placementsStringTypes['topothetisi'])) {
            $this->prosorini_topothetisi = true;
        } elseif (in_array($type, $this->placementsStringTypes['diathesi'])) {
            $this->diathesi = true;
        } elseif (in_array($type, $this->placementsStringTypes['apospasi'])) {
            $this->apospasi = true;
        } elseif (in_array($type, $this->placementsStringTypes['anaklisi'])) {
            $this->anaklisi = true;
        } elseif (in_array($type, $this->placementsStringTypes['pvthmia'])) {
            $this->protovathimia = true;
        } 
    }

    private function putTeacherToRightSheet($placementSheets , $afm)
    {
        $title = '';

        if ($this->prosorini_topothetisi && $this->diathesi) {
            $placementSheets['Λειτ. Τοποθέτηση & Συμπλήρωση']->push($this->teacherAttributes($afm));
            $title = 'Λειτ. Τοποθέτηση & Συμπλήρωση';
        } elseif ($this->prosorini_topothetisi && !$this->diathesi) {
            $placementSheets['Λειτ. Τοποθέτηση']->push($this->teacherAttributes($afm));
            $title = 'Λειτ. Τοποθέτηση';
        } elseif (!$this->prosorini_topothetisi && $this->diathesi) {
            $placementSheets['Συμπλήρωση Ωραρίου']->push($this->teacherAttributes($afm));
            $title = 'Συμπλήρωση Ωραρίου';
        } elseif ($this->apospasi) {
            $placementSheets['Απόσπαση']->push($this->teacherAttributes($afm));
            $title = 'Απόσπαση';
        } elseif ($this->anaklisi) {
            $placementSheets['Ανάκληση']->push($this->teacherAttributes($afm));
            $title = 'Ανάκληση';
        } elseif ($this->protovathimia) {
            $placementSheets['Πθμια']->push($this->teacherAttributes($afm));
            $title = 'Πθμια';
        } else {
            $placementSheets['Unknown']->push($this->teacherAttributes($afm));
            $title = 'Unknown';
        }

        return $title;
    }

    private function putTeacherWithPlacementToRightSheet($placementSheets , $new_teacher, $all_placements)
    {                    
        if ($this->prosorini_topothetisi && $this->diathesi) {
            $this->addTeacherToSheet($placementSheets, ['topothetisi', 'diathesi'], 'Λειτ. Τοποθέτηση & Συμπλήρωση', $all_placements, $new_teacher);
        }elseif ($this->prosorini_topothetisi && !$this->diathesi) {
            $this->addTeacherToSheet($placementSheets, 'topothetisi', 'Λειτ. Τοποθέτηση', $all_placements, $new_teacher);
        }elseif (!$this->prosorini_topothetisi && $this->diathesi) {
            $this->addTeacherToSheet($placementSheets, 'diathesi', 'Συμπλήρωση Ωραρίου', $all_placements, $new_teacher);
        }elseif ($this->apospasi) {
            $this->addTeacherToSheet($placementSheets, 'apospasi', 'Απόσπαση', $all_placements, $new_teacher);
        }
        if ($this->protovathimia) {
            $this->addTeacherToSheet($placementSheets, 'pvthmia', 'Διάθεση στη Πθμια', $all_placements, $new_teacher);
        }
        if ($this->anaklisi) {
            $this->addTeacherToSheet($placementSheets, 'anaklisi', 'Ανάκληση', $all_placements, $new_teacher);
        }

    }
    
    private function addTeacherToSheet($placementSheets, $data, $title, $all_placements, $new_teacher)
    {
        if(is_array($data)){
            $list_types =  array_merge($this->placementsStringTypes[$data[0]], $this->placementsStringTypes[$data[1]]);
        }else{
            $list_types =  $this->placementsStringTypes[$data];
        }   

        $temp = clone $new_teacher;

        $temp['placements'] = $all_placements->whereIn('placements_type', $list_types)->values();

        $placementSheets[$title]->push($temp);
    }

    private function teacherAttributes($afm)
    {
        $myschool = MySchoolTeacher::find($afm);

        return collect([
            'afm'       => $myschool->afm,
            'organiki'  => $myschool->organiki_name,
            'eidikotita'=> $myschool->eidikotita,
            'full_name' => $myschool->full_name,
            'moria'     => $myschool->edata != null ? $myschool->edata->sum_moria : 0,
            'moria_apospasis'     => $myschool->edata != null ? $myschool->edata->moria_apospasis : 0,
            'ypoxreotiko'   => $myschool->ypoxreotiko,
            'entopiotita'   => $myschool->edata != null ? \Config::get('requests.dimos')[$myschool->edata->dimos_entopiotitas] : 0,
            'sinipiretisi'  => $myschool->edata != null ? \Config::get('requests.dimos')[$myschool->edata->dimos_sinipiretisis] : 0,
            'special'       => $myschool->edata != null ? $myschool->edata->special_situation == 0 ? 'ΟΧΙ' : 'ΝΑΙ' : 'ΟΧΙ',
            'middle_name'   => $myschool->middle_name,
            'omada'         => $myschool->group_name != null ? $myschool->group_name : 'Χ',
            'placements'    => collect(), 
            'position'      => $myschool->position,
            'am'            => $myschool->am
        ]);
    }

    private function teacherAttributesOrganika($teacher)
    {
        $myschool = MySchoolTeacher::where('am', $teacher['am'])->first();

        $school = School::find($teacher['school_id']);

        if($school != null){
            $new_organiki = $school->name;
        }else{
            $new_organiki = '-';
        }
            
        if($myschool != null){
            return collect([
                'am'       => $myschool->am,
                'old_organiki'  => $myschool->organiki_name,
                'new_organiki'  => $new_organiki,
                'eidikotita'=> $myschool->eidikotita,
                'full_name' => $myschool->full_name,
                'moria'     => $myschool->edata != null ? $myschool->edata->sum_moria : 0,
                'moria_apospasis'     => $myschool->edata != null ? $myschool->edata->moria_apospasis : 0,
                'ypoxreotiko'   => $myschool->ypoxreotiko,
                'entopiotita'   => $myschool->edata != null ? \Config::get('requests.dimos')[$myschool->edata->dimos_entopiotitas] : 0,
                'sinipiretisi'  => $myschool->edata != null ? \Config::get('requests.dimos')[$myschool->edata->dimos_sinipiretisis] : 0,
                'special'       => $myschool->edata != null ? $myschool->edata->special_situation == 0 ? 'ΟΧΙ' : 'ΝΑΙ' : 'ΟΧΙ',
                'middle_name'   => $myschool->middle_name,
                'omada'         => $myschool->group_name != null ? $myschool->group_name : 'Χ',
                'position'      => $myschool->position,
                'description'   => $myschool->yadescription
            ]);
        }
    }

    public function deleteOldPlacement(Request $request)
    {
        $data = array();

        $topothetisi = Placement::find($request->get('topothetisi')['id']);

        $this->placement->updateLeitourgikaKena($topothetisi);

        $topothetisi->delete();
        
        $data['message'] = ['Η τοποθέτηση Διαγράφηκε με επιτυχία ...'];
            
        return $data;
    }

    public function permanentDeleteOldPlacement(Request $request)
    {
        $data = array();

        $topothetisi = Placement::find($request->get('topothetisi')['id']);

        $this->placement->updateLeitourgikaKena($topothetisi);

        $topothetisi->forceDelete();
        
        $data['message'] = ['Η τοποθέτηση Διαγράφηκε ΟΡΙΣΤΙΚΑ ...'];
            
        return $data;
    }

    public function permanentDelete($placement_id, Request $request)
    {
        Log::alert("Placement ID: $placement_id");

        Placement::withTrashed()->where('id', $placement_id)->forceDelete();

        return ['ok'];
    }

    public function recallOldPlacement(Request $request)
    {
        $data = array();

        $topothetisi = Placement::find($request->get('topothetisi')['id']);

        // DB::beginTransaction();

        $dieuthidi = $request->get('dieuthidi') ? 12 : 11; 

        Log::warning("DIEUTHIDI = $dieuthidi");

        $this->placement->createRecallPlacement($topothetisi, $request->get('date'), $request->get('praxi'), $dieuthidi);

        $this->placement->updateLeitourgikaKena($topothetisi);

        $topothetisi->delete();
        
        // DB::commit();

        $data['message'] = ['Η τοποθέτηση ανακλήθηκε με επιτυχία ...'];
            
        return $data;
    }

    private function getPlacementsBySheet($all_placements, $list_types)
    {
        return $all_placements->whereIn('placements_type', $list_types)->values();
    }

    public function compareCollection()
    {
        return function ($a, $b) {
            foreach($this->sort as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }

}
