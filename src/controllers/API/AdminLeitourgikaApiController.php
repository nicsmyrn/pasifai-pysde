<?php

namespace Pasifai\Pysde\controllers\API;

use App\Http\Controllers\Controller;
use App\NewEidikotita;
use App\School;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\MySchoolTeacher;
use App\User;
use Pasifai\Pysde\controllers\ExcelExports\LeitourgikaExport;
use Maatwebsite\Excel\Facades\Excel;
use Pasifai\Pysde\controllers\ExcelExports\LeitourgikaExportOfSchools;

class AdminLeitourgikaApiController extends Controller      //TODO must change permissions, E-mail and Notifications
{
    protected $data = [];

    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function getExcelForCouncil()
    {
        $date = Carbon::now()->timestamp;        

        return Excel::download(new LeitourgikaExport(), 'ΛΕΙΤΟΥΡΓΙΚΑ_ΚΕΝΑ_'.$date.'.xlsx');
    }

    public function getExcelForSchools()
    {
        $date = Carbon::now()->timestamp;        

        return Excel::download(new LeitourgikaExportOfSchools(), 'ΛΕΙΤΟΥΡΓΙΚΑ_ΚΕΝΑ_'.$date.'.xlsx');
    }

    public function fetchEidikotites()
    {
        $data = array();

        $data['eidikotites'] = NewEidikotita::all();

        $data['teacher_types_that_is_in_school'] = config('requests.teacher_types_that_is_in_school');

        $data['teacher_types'] =  config('requests.topothetisis_types');
        
        return $data;
    }

    public function fetchAllSchools()
    {
        $data = array();

        $data['schools'] = School::orderBy('name')->get();

        $data['teacher_types_that_is_in_school'] = config('requests.teacher_types_that_is_in_school');

        $data['counter_locked'] = DB::table('_kena_leitourgika_sum_hours')->where('sch_locked', true)->count();
        $data['counter_unlocked'] = DB::table('_kena_leitourgika_sum_hours')->where('sch_locked', false)->count();

        return $data;
    }

    public function fetchTeachersByEidikotita(Request $request)
    {
        $teachers_array = array();
        $data = array();

        if($request->get('eidikotita_slug') == null){
            $teachers = MySchoolTeacher::with(['topothetisis', 'special_topothetisis'])
            ->orderBy('last_name')
            ->get();
        }else{
            $teachers = MySchoolTeacher::with(['topothetisis', 'special_topothetisis'])
            ->where('eidikotita', $request->get('eidikotita_slug'))
            ->orderBy('last_name')
            ->get();
        }
                    
        foreach($teachers as $teacher){
            $topothetisis = array();

            // if( !((count($teacher->topothetisis->whereNotIn('pivot.teacher_type', [0,3,5,6,10])) > 0) && (count($teacher->special_topothetisis) == 0))){
            if(((count($teacher->topothetisis->whereIn('pivot.teacher_type', [0,5,6,10])) > 0) || (count($teacher->special_topothetisis) > 0))){

                $sum_hours = $teacher->topothetisis->sum('pivot.hours') + $teacher->special_topothetisis->sum('pivot.hours');
                $sum_project_hours = $teacher->topothetisis->sum('pivot.project_hours');
    
                $organiki_name = School::where('identifier', $teacher->organiki_prosorini)->first();
                if($organiki_name != null) $url_organikis = route('Leitourgika::pysdeSecretary::manageSchools', str_replace(" ", "-", $organiki_name->name));
                else $url_organikis = route('Leitourgika::pysdeSecretary::manageSchools');
    
                foreach($teacher->topothetisis as $topothetisi){
                    $topothetisis[] = [
                        'name'          => $topothetisi->name,
                        'identifier'    => $topothetisi->identifier,
                        'order'         => $topothetisi->order,
                        'sum_hours'     => ($topothetisi->pivot->hours + $topothetisi->pivot->project_hours),
                        'teacher_type'  => $topothetisi->pivot->teacher_type,
                        'date_ends'     => $topothetisi->pivot->date_ends,
                        'school_url'    => route('Leitourgika::pysdeSecretary::manageSchools', str_replace(" ", "-", $topothetisi->name))
                    ];
                }

                $teachers_array[] = [
                    'last_name'             => $teacher->last_name,
                    'first_name'            => $teacher->first_name,
                    'middle_name'           => $teacher->middle_name,
                    'am'                    => $teacher->am,
                    'afm'                   => $teacher->afm,
                    'ypoxreotiko'           => $teacher->ypoxreotiko - $teacher->meiwsi,
                    'organiki'              => $teacher->organiki_name,
                    'school_id'             => $teacher->organiki_prosorini,
                    'url_organikis'         => $url_organikis,
                    'eidikotita'            => $teacher->new_eidikotita,
                    'eidikotita_name'       => $teacher->eidikotita,
                    'topothetisis'          => $topothetisis,
                    'special_topothetisis'  => $teacher->special_topothetisis,
                    'sum'                   => ($sum_hours + $sum_project_hours)
                ];
            }
            
        }
        $data['teachers'] = $teachers_array;
        $data['schools'] = School::orderBy('name')->get();

        return $data;

    }

    public function getKenaOfSchool(School $school)
    {
        $email = env('MAIL_PYSDE');
        $pysde = User::where('email', $email)->first();

        $kena_leitourgika = array();

        foreach($school->leitourgika_kena as $keno){
            $kena_leitourgika[] = [
                'id'                => $keno['id'],
                'eidikotita_name'   => $keno['eidikotita_name'],
                'eidikotita_slug'   => $keno['eidikotita_slug'],
                'klados_name'       => $keno['klados_name'],
                'klados_slug'       => $keno['klados_slug'],
                'forseen'           => $keno['pivot']['forseen'],
                'available'         => $keno['pivot']['available'],
                'difference'        => $keno['pivot']['difference'],
                'locked'            => $keno['pivot']['locked'],
                'project_hours'     => $keno['pivot']['project_hours'],
                'school_id'         => $keno['pivot']['school_id'],
                'updated'           => false,
                'number_of_teachers' => $keno['pivot']['number_of_teachers'],
                'sch_difference'    => $keno['pivot']['sch_difference'],
                'sch_description'    => $keno['pivot']['sch_description'],
                'sch_locked'    => $keno['pivot']['sch_locked'],
                'old_difference'        => null,
                'alter_school'        => false,
                'updated_at'            => Carbon::parse($keno['pivot']['updated_at'])->format('d/m/Y h:i:s'),
                'user_id'                  => $keno['pivot']['user_id']
            ];
        }
        
        $this->data['school_id'] = $school->id;
        
        $this->data['kena'] = $kena_leitourgika;

        $this->data['school_type'] = $school->type;

        $this->data['teacher_types'] =  config('requests.topothetisis_types');

        $list = $school->leitourgika_kena->pluck('id');

        $this->data['other_eidikotites'] = NewEidikotita::where(function($q) use ($list){
            $q->whereNotIn('id', $list);
            $q->where('eidikotita_name', 'NOT LIKE', '%ΕΙΔΙΚΗΣ%');
        })->get();

        return $this->data;
    }

    public function loadOrganikaKena()
    {
        $email = env('MAIL_PYSDE');

        $schools = School::with('kena')->get();
        $pysde = User::where('email', $email)->first();

        foreach($schools as $school){
            $sync_array = array();

            foreach($school->kena as $record){
                $sync_array[$record->id] = [
                    'forseen'       => $record->pivot->council_forseen,
                    'available'     => $record->pivot->available,
                    'difference'    => $record->pivot->council_difference,
                    'user_id'                  =>$record->pivot->user_id,
                    'locked'        => false,
                    'project_hours' => $record->pivot->project_hours,
                    'number_of_teachers' => $record->pivot->number_of_teachers
                ];
            }

            $school->leitourgika_kena()->sync($sync_array);
        }

        return ['complete'];
    }

    public function loadTeachers()
    {
        $schools = School::with('organikes')->get();

        foreach($schools as $school){
            $sync_array = array();
            foreach($school->organikes as $organiki){
                $sync_array[$organiki->afm] = [
                    'teacher_type'      => 0,
                    'locked'            => false
                    // 'date_ends'         => '2020-08-31'
                ];
            }
            $school->topothetisis()->sync($sync_array);
        }

        return ['complete'];
    }

    public function lockUnlockAllKena(Request $request)
    {
        $this->toggleLockedAllKena($request->get('locked'));

        $text = $request->get('locked') ? 'Κλειδώθηκαν' : 'Ξεκλειδώθηκαν';

        flash()->html("Συγχαρητήρια", "ΟΛΑ τα κενά <strong>$text</strong>");

        return ['ok'];
    }

    protected function toggleLockedAllKena($value)
    {
        DB::table('_kena_leitourgika_sum_hours')->update(['sch_locked' => $value]);
    }

    public function deleteEidikotita(School $school, Request $request)
    {        
        $school->leitourgika_kena()->detach($request->get('eidikotita_id'));
        $email = env('MAIL_PYSDE');
        $pysde = User::where('email', $email)->first();
        $kena_leitourgika = array();

        foreach($school->leitourgika_kena as $keno){
            $kena_leitourgika[] = [
                'id'                => $keno['id'],
                'eidikotita_name'   => $keno['eidikotita_name'],
                'eidikotita_slug'   => $keno['eidikotita_slug'],
                'klados_name'       => $keno['klados_name'],
                'klados_slug'       => $keno['klados_slug'],
                'forseen'           => $keno['pivot']['forseen'],
                'available'         => $keno['pivot']['available'],
                'difference'        => $keno['pivot']['difference'],
                'locked'            => $keno['pivot']['locked'],
                'project_hours'     => $keno['pivot']['project_hours'],
                'user_id'           => $keno['pivot']['user_id'] ,
                'school_id'         => $keno['pivot']['school_id'],
                'updated'           => false,
                'number_of_teachers' => $keno['pivot']['number_of_teachers'],
                'sch_difference'    => $keno['pivot']['sch_difference'],
                'sch_description'   => $keno['pivot']['sch_description'],
                'sch_locked'        => $keno['pivot']['sch_locked'],
                'old_difference'        => null,
                'alter_school'        => false,
                'updated_at'            => Carbon::parse($keno_eidikotitas->pivot->updated_at)->format('d/m/Y h:i:s'),
            ];
        }

        return $kena_leitourgika;
    }

    public function saveEidikotita(School $school, Request $request)
    {        
        foreach($request->get('eidikotites') as $eidikotita){
            if($eidikotita['updated']){
                $difference = $eidikotita['available'] - $eidikotita['project_hours'] - $eidikotita['forseen'];

                $school->leitourgika_kena()->detach($eidikotita['id']);
                $school->leitourgika_kena()->attach($eidikotita['id'], [
                    'forseen'       => $eidikotita['forseen'],
                    'available'     => $eidikotita['available'],
                    'difference'    => $difference,
                    'user_id'       => Auth::id(),
                    'project_hours' => $eidikotita['project_hours'] != null ? $eidikotita['project_hours'] : 0,
                    'locked'        => $eidikotita['locked'],
                    'number_of_teachers'  => $eidikotita['number_of_teachers'],
                    'sch_difference'    => $eidikotita['sch_difference'],
                    'sch_description'    => $eidikotita['sch_description'],
                    'sch_locked'    => $eidikotita['sch_locked'],
                ]);
            }
        }

        $counter_locked = DB::table('_kena_leitourgika_sum_hours')->where('sch_locked', true)->count();
        $counter_unlocked  = DB::table('_kena_leitourgika_sum_hours')->where('sch_locked', false)->count();
        
        return [
            'message'=> 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία...',
            'type'      => 'success',
            'header'    => 'Συγχαρητήρια!',
            'counter_unlocked'  => $counter_unlocked,
            'counter_locked'    => $counter_locked
        ];
    }

    public function getTeachersOfSchool(School $school, Request $request)
    {        
        $eidikotita = $request->get('eidikotita');

        $teachers = array();
        $leitourgika = $school->topothetisis->where('eidikotita', $eidikotita)->sortBy('last_name')->values();
    
        foreach($leitourgika as $t){
            $special_topothetisis = $t->special_topothetisis;
            // $other_topothetisis = $t->topothetisis->where('identifier', '<>', $school->identifier);
            $other_topothetisis = $t->topothetisis->where('identifier', '<>', $school->identifier)->whereNotIn('pivot.teacher_type', [1,2,3,4,9,11]);


            $sum_special = $special_topothetisis == null ? 0 : $special_topothetisis->sum('pivot.hours');
            $sum_other = $other_topothetisis == null ? 0 : $other_topothetisis->sum('pivot.hours');

            $teachers[] = [
                'last_name'             => $t['last_name'],
                'first_name'            => $t['first_name'],
                'middle_name'           => $t['middle_name'],
                'am'                    => $t['am'],
                'afm'                   => $t['afm'],
                'teacher_type'           => $t['pivot']['teacher_type'],
                'hours'                 => $t['pivot']['hours'],
                'project_hours'         => $t['pivot']['project_hours'],
                'ypoxreotiko'           => $t['ypoxreotiko'],
                'meiwsi'                => $t['meiwsi'],
                'sum_ypoxreotiko'       => $t['ypoxreotiko'] - $t['meiwsi'],
                'difference'            => $t['ypoxreotiko'] - $t['meiwsi'] - ($t['pivot']['hours'] + $t['pivot']['project_hours'] + $sum_special + $sum_other),
                'end_date'              => $t['pivot']['date_ends'],
                'locked'                => $t['pivot']['locked'],
                'updated'               => false,
                'belongsToSchool'       => !! in_array($t['pivot']['teacher_type'], config('requests.teacher_types_that_is_in_school')),
                'sum_other_topothetisis'=> $sum_special + $sum_other
            ];
        }
        return $teachers;
    }

    public function saveTeachers(School $school, Request $request)
    {
        $eidikotita = NewEidikotita::where('eidikotita_slug', $request->get('eidikotitaName'))->first();

        $keno_eidikotitas = $school->leitourgika_kena()->where('eid_id', $eidikotita->id)->first();

        $sum_forseen = $keno_eidikotitas->pivot->forseen;
        $sum_project_hours = $keno_eidikotitas->pivot->project_hours;

        $sum_ypoxrewtiko = 0;
        $sum_other_topothetisis = 0;
        $teacher_types_that_is_in_school = $request->get('teacher_types_that_is_in_school');

        $number_of_teachers = count($request->get('teachers'));

        if($eidikotita != null){
            foreach($request->get('teachers') as $teacher){
                
                $myschoolProfile = MySchoolTeacher::find($teacher['afm']);

                if($myschoolProfile != null){
                    $myschoolProfile->ypoxreotiko = $teacher['ypoxreotiko'];
                    $myschoolProfile->meiwsi = $teacher['meiwsi'] == null ? 0 : $teacher['meiwsi'];
                    $myschoolProfile->save();
                }

                $hours = $teacher['hours'] == null ? 0 : $teacher['hours'];
                $hours = $hours < 0 ? 0 : $hours;
                $project_hours = $teacher['project_hours'] == null ? 0 : $teacher['project_hours'];
                $project_hours = $project_hours < 0 ? 0 : $project_hours;

                $sum_teacher_hours = $hours + $project_hours;
                
                if(in_array($teacher['teacher_type'], $teacher_types_that_is_in_school)){
                    if(in_array($teacher['teacher_type'], [5,6])){
                        $sum_ypoxrewtiko +=  $sum_teacher_hours;
                    }else{
                        if($teacher['difference'] < 0){
                            $sum_ypoxrewtiko +=  ($teacher['ypoxreotiko'] - $teacher['meiwsi'] - $teacher['difference']);
                        }else{
                            $sum_ypoxrewtiko +=  ($teacher['ypoxreotiko'] - $teacher['meiwsi']);
                        }
                        $sum_other_topothetisis += $teacher['sum_other_topothetisis'];   
                    }
                }
    
                if($teacher['updated']){    
                    $school->topothetisis()->updateExistingPivot($teacher['afm'], [
                        'teacher_type'      => $teacher['teacher_type'],
                        'date_ends'         => $teacher['end_date'],
                        'hours'             => $hours,
                        'project_hours'     => $project_hours,
                        'locked'            => $teacher['locked'],
                    ]);
                }
            }

            $available = $sum_ypoxrewtiko - $sum_other_topothetisis;

            $sum_difference = $available - $sum_project_hours - $sum_forseen;

            $school->leitourgika_kena()->updateExistingPivot($eidikotita->id, [
                'forseen'       => $sum_forseen,
                'available'     => $available,
                'difference'    => $sum_difference,
                'user_id'       => Auth::id(),
                'project_hours' => $sum_project_hours,
                'locked'        => true,
                'number_of_teachers' => $number_of_teachers,
                'sch_difference'    => $keno_eidikotitas->pivot->sch_difference,
                'sch_description'    => $keno_eidikotitas->pivot->sch_description,
                'sch_locked'    => $keno_eidikotitas->pivot->sch_locked,

            ]);
    
            return [
                'message'=> 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία...',
                'type'      => 'success',
                'header'    => 'Συγχαρητήρια!',
                'eidikotita'    => [
                    'id'                => $eidikotita->id,
                    'eidikotita_name'   => $eidikotita->eidikotita_name,
                    'eidikotita_slug'   => $eidikotita->eidikotita_slug,
                    'klados_name'       => $eidikotita->klados_name,
                    'klados_slug'       => $eidikotita->klados_slug,
                    'forseen'           => $sum_forseen,
                    'available'         => $available,
                    'difference'        => $sum_difference,
                    'locked'            => true,
                    'project_hours'     => $sum_project_hours,
                    'user_id'           => Auth::id(),
                    'school_id'         => $school->id,
                    'updated'           => false,
                    'number_of_teachers' => $number_of_teachers,
                    'sch_difference'    => $keno_eidikotitas->pivot->sch_difference,
                    'sch_description'    => $keno_eidikotitas->pivot->sch_description,
                    'sch_locked'    => $keno_eidikotitas->pivot->sch_locked,
                    'old_difference' => null,
                    'updated_at'     => Carbon::now()->format('d/m/Y h:i:s'),
                    'user'           => '999'
                ]
            ];
        }else{
            return [
                'message'=> 'Η ειδικότητα δεν υπάρχει',
                'type'      => 'danger',
                'header'    => 'Προσοχή!'
            ];   
        }
    }

}