<?php

namespace Pasifai\Pysde\controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Pasifai\Pysde\models\RequestTeacherYperarithmia;
use Pasifai\Pysde\models\OrganikiRequest;
use Carbon\Carbon;
use DB;
use Pasifai\Pysde\models\ProtocolTeacher;
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Notification;
use Log;
use Pasifai\Pysde\notifications\TeacherToPysdeNotification;
use Pasifai\Pysde\controllers\traits\RequestsTrait;
use Pasifai\Pysde\models\Protocol;
use Pasifai\Pysde\notifications\PysdeToTeacherNotification;

class AdminRequestApiController extends Controller      //TODO must change permissions, E-mail and Notifications
{
    use RequestsTrait;

    protected $all_requests;

    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function getAllRequests(Request $request)
    {
        $this->all_requests = collect();

        $this->getRequestsYperarithmias();
        $this->getAllOtherRequests();

        return $this->all_requests;
    }

    public function Recall($unique_id, $action)
    {        
        if($action == 'cancelRecall'){
            $recall_situation = 'Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ';
        }elseif($action == 'approvedRecall'){
            $recall_situation = 'Η ΑΙΤΗΣΗ ΑΝΑΚΛΗΘΗΚΕ';
        }else{
            $recall_situation = 'Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ';
        }

        $actionRequest = RequestTeacherYperarithmia::with('teacher')->where('unique_id', $unique_id)->first();

        if($actionRequest != null){
            $user = $actionRequest->teacher->user;
            $teacher_folder = str_slug($user->last_name) . md5($actionRequest->teacher->id);
            $file_path = $actionRequest->file_name;
            $myschool = $actionRequest->teacher->myschool;
            
            $actionRequest->recall_situation = $recall_situation;
            $actionRequest->save();

            $attributes = [
                'teacher_name'  => $user->full_name,
                'protocol_name' => $actionRequest->protocol_name,
                'myschool'           => $myschool,
                'new_klados'    => $myschool->new_klados,
                'new_eidikotita_name'   => $myschool->new_eidikotita_name,
                'want_yperarithmia' => $actionRequest->want_yperarithmia,
                'organiki_name' => $myschool->organiki_name,
                'date_request'  => $actionRequest->date_request,
                'recall_stamp'  => $actionRequest->recall_situation
            ];

            $view = 'pysde::aitisis.organika.teacher.pdf.YP_PDF';

            \Storage::delete('teachers/'. $teacher_folder . $file_path);

            $this->makePDFforRequests($attributes, $teacher_folder, $file_path, $view, $action, $user->sch_mail, $user->full_name);

            $email = $this->getTheCorrectMail($user);

            // $pysde_secretary = Role::where('slug', 'pysde_secretary')->first()->users->first();
            $pysde_secretary = User::where('email', config('requests.MAIL_PYSDE'))->first();

            if($this->send_notification){
                Notification::send($user, new PysdeToTeacherNotification($pysde_secretary,[
                    'title'         => 'Απάντηση στην αίτηση Ανάκλησης',
                    'description'   => "Η αίτηση με αριθμό {$actionRequest->protocol_name} <span class='notification_full_name'>&laquo;{$recall_situation}&raquo;</span>.",
                    'type'          => 'danger',
                    'url'           => route('Request::archives'),
                    'subject'       => 'Απάντηση στην ανάκληση',
                    'file'          => storage_path('app/teachers/'.$file_path),
                    'answer'        => $recall_situation,
                    'mail'          => $pysde_secretary->email
                ], 'pysde::email.teacher-answer-cancellation-request'));
            }
        }

        $organikaRequests = OrganikiRequest::with('teacher')->where('unique_id', $unique_id)->get();

        if(!$organikaRequests->isEmpty()){
            
            $protocol = Protocol::find($organikaRequests->first()->protocol_number);

            if($protocol != null){  

                $first_request =  $organikaRequests->first();
                $teacher = $first_request->teacher;
                $user = $teacher->user;

                $teacher_folder = str_slug($user->last_name) . md5($organikaRequests->first()->teacher->id);
                $file_path = $organikaRequests->first()->file_name;      
                $myschool = $organikaRequests->first()->teacher->myschool;

                foreach($organikaRequests as $request){
                    $request->recall_situation = $recall_situation;
                    $request->save();
                }

                $attr = [
                    'request_teacher'   => $organikaRequests,
                    'protocol'          => $protocol,
                    'user'              => $user,
                    'teacher'           => $teacher,
                    'monimos'           => $teacher->teacherable,
                    'organiki_name'     => $teacher->teacherable->organiki_name,
                    'myschool'          => $teacher->myschool,
                    'edata'             => $teacher->myschool->edata,
                    'recall_stamp'      => $recall_situation
                ];

                \Storage::delete('teachers/'. $teacher_folder . $file_path);

                $this->makePDFforRequests($attr, $teacher_folder, $file_path, 'pysde::aitisis.leitourgika.teacher.PDF.pdfOrganikaYperarithmos', $action, $user->sch_mail, $user->fullname);
    
                // $pysde_secretary = Role::where('slug', 'pysde_secretary')->first()->users->first(); 
                $pysde_secretary = User::where('email', config('requests.MAIL_PYSDE'))->first();
                
                $email = $this->getTheCorrectMail($user);

                if($this->send_notification){
                    Notification::send($user, new PysdeToTeacherNotification($pysde_secretary,[
                        'title'         => 'Απάντηση στην αίτηση Ανάκλησης',
                        'description'   => "Η αίτηση με αριθμό {$organikaRequests->first()->protocol_name} <span class='notification_full_name'>&laquo;{$recall_situation}&raquo;</span>.",
                        'type'          => 'danger',
                        'url'           => route('Request::archives'),
                        'subject'       => 'Απάντηση στην ανάκληση',
                        'file'          => storage_path('app/teachers/'.$file_path),
                        'answer'        => $recall_situation,
                        'mail'          => $pysde_secretary->email,
                    ], 'pysde::email.teacher-answer-cancellation-request'));
                }

            }else{
                return 'Major Error';
            }
        }
        return ['success'];
    }

    private function getRequestsYperarithmias()
    {
        $requestsYperarithmias = RequestTeacherYperarithmia::with('teacher')
        ->where('year', Carbon::now()->year)
        ->get();        

        foreach($requestsYperarithmias as $req){
            $user = $req->teacher->user;

            $this->all_requests->push([
                'full_name' => $user->full_name,
                'sch_mail'  => $user->sch_mail,
                'afm'       => $req->teacher->afm,
                'unique_id' => $req->unique_id,
                'req_date'  => $req->date_request,
                'protocol_name' => $req->protocol_name,
                'type'          => 'ΕΠΙΘΥΜΙΑ ΥΠΕΡΑΡΙΘΜΙΑΣ',
                'description'   => $req->recall_situation,
                'downloadUrl'   => route('PysdeAdmin::downloadPDF', $req->unique_id)
            ]);
        }

    }

    private function getAllOtherRequests()
    {
        $organikes = OrganikiRequest::with('teacher')
            ->where('date_request', '>=', Carbon::now()->year.'-01-01 00:00:00')
            ->where('protocol_number', '<>', null)
            ->get();

        $groups = $organikes->groupBy('unique_id');

        foreach($groups as $group){
            $req = $group->first();

            $user = $req->teacher->user;

            $this->all_requests->push([
                'full_name' => $user->full_name,
                'sch_mail'  => $user->sch_mail,
                'afm'       => $req->teacher->afm,
                'unique_id' => $req->unique_id,
                'req_date'  => $req->date_request,
                'protocol_name' => $req->protocol_name,
                'type'          => $req->groupType,
                'description'   => $req->recall_situation,
                'downloadUrl'   => route('PysdeAdmin::downloadPDF', $req->unique_id)
            ]);
        }  
    }
}