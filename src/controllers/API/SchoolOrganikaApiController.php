<?php

namespace Pasifai\Pysde\controllers\API;

use App\Http\Controllers\Controller;
use App\MySchoolTeacher;
use Illuminate\Http\Request;
use Pasifai\Pysde\controllers\traits\VacuumTrait;
use App\NewEidikotita;
use App\School;
use App\User;
use Artisan;
use App\Models\Role;
use Pasifai\Pysde\notifications\SchoolNotification;
use Carbon\Carbon;
use Pasifai\Pysde\models\Division;
use Illuminate\Support\Facades\Notification;
use Log;
use Pasifai\Pysde\models\Organiki;
use Pasifai\Pysde\notifications\PysdeNotification;
class SchoolOrganikaApiController extends Controller      //TODO must change permissions, E-mail and Notifications
{
    use VacuumTrait;

    protected $organikes;
    protected $eidikotites;

    protected $attach_array = [];
    protected $attach_project_array = [];
    protected $other_divisions = [];

    protected $data = [];

    protected $school_type = [
        'Γυμνάσιο'  => 'gym',
        'Λύκειο'    => 'lyk',
        'ΕΠΑΛ'      => 'epal',
        'Ειδικό'    => 'eidiko'
    ];

    protected $successSaveMessage = "<p>τα δεδομένα αποθηκεύτηκαν προσωρινά στην ΠΑΣΙΦΑΗ.</p><p>τα στοιχεία των πινάκων <strong style='color:red'>ΔΕΝ έχουν σταλεί</strong> στη ΔΔΕ Χανίων</p>";
    protected $successSentMessage = "<p>τα δεδομένα στάλθηκαν με E-mail</p><p><ul><li>στην Γραμματεία του ΠΥΣΔΕ</li><li>με κοινοποίηση στη Σχολική Μονάδα</li></ul></p><p>Όλοι οι πίνακες <span style='color:red;font-weight: bold;'>ΕΧΟΥΝ ΚΛΕΙΔΩΣΕΙ.</span></p>";
    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function updateTeacherOrganika(Request $request)
    {
        if($request->ajax()){

            $teacher = $request->get('teacher');

            $myschool = MySchoolTeacher::find($teacher['afm']);

            $myschool->realOrganiki()->update([
                'checked' => $teacher['checked'],
                'year_commit' => $teacher['year_commit']
            ]);

            if($teacher['checked']){
                return 'is_checked';
            }else{
                return 'not_checked';
            }

            return 'all_ok';
        }
    }

    public function getOrganikes(Request $request)
    {
        if($request->ajax()){
            $user = $request->user();

            $school = $user->userable;
            
            // #######################################################################
            $teachers = collect();
    
            $organikes =  $school->organikes;
    
            if(count($organikes)){
                foreach($organikes as $key=>$organiki){
    
                    $edata = $organiki->teacher->edata;
        
                    if($edata != null){
                        $aitisi_veltiwsis = $edata->aitisi_veltiwsis ? 'ΝΑΙ' : '';
                    }else{
                        $aitisi_veltiwsis = 'error';
                    }
        
                    $teachers->push([
                        'last_name'     => $organiki->teacher->last_name,
                        'first_name'    => $organiki->teacher->first_name,
                        'middle_name'    => $organiki->teacher->middle_name,
                        'am'    => $organiki->teacher->am,
                        'afm'       => $organiki->teacher->afm,
                        'eidikotita'    => $organiki->teacher->eidikotita,
                        'topothetisi'    => $organiki->teacher->topothetisi,
                        'ypoxreotiko'    => $organiki->teacher->ypoxreotiko,
                        'aitisi_veltiwsis' => $aitisi_veltiwsis,
                        'type'      => $organiki->teacher->type,
                        'checked'   => $organiki->checked,
                        'year_commit' => $organiki->year_commit
                    ]);
                }
            }
    
            $organikes_eidikis = $school->organikes_eidikis;
    
            if(count($organikes_eidikis)){
                foreach($organikes_eidikis as $key=>$organiki){
    
                    $edata = $organiki->teacher->edata;
        
                    if($edata != null){
                        $aitisi_veltiwsis = $edata->aitisi_veltiwsis ? 'ΝΑΙ' : '';
                    }else{
                        $aitisi_veltiwsis = 'error';
                    }
        
                    $teachers->push([
                        'last_name'     => $organiki->teacher->last_name,
                        'first_name'    => $organiki->teacher->first_name,
                        'middle_name'    => $organiki->teacher->middle_name,
                        'afm'       => $organiki->teacher->afm,
                        'am'    => $organiki->teacher->am,
                        'eidikotita'    => $organiki->teacher->eidikotita,
                        'topothetisi'    => $organiki->teacher->topothetisi,
                        'ypoxreotiko'    => $organiki->teacher->ypoxreotiko,
                        'aitisi_veltiwsis' => $aitisi_veltiwsis,
                        'type'      => $organiki->teacher->type,
                        'checked'   => $organiki->checked,
                        'year_commit' => $organiki->year_commit
                    ]);
                }
            }
            
            $teachers =  $teachers->sortBy('eidikotita')->values();
            // #######################################################################
            return $teachers;
        }

    }

    public function saveGeneralDivs(Request $request)
    {
        $schools = $request->get('schools');

        foreach($schools as $school){

            $s = School::find($school['id']);

            if($s != null){
                $array_sync = array();
     
                foreach($school['divisions'] as $division){
                    if(intval($division['number']) > 0 && intval($division['students']) > 0){
                        $array_sync[$division['id']] = [
                            'number'                => $division['number'],
                            'students'              => $division['students'],
                            'numberSchoolProvide'   => $division['number'],
                            'locked'                => false
                        ];
                    }
                }
    
                $s->divisions()->syncWithoutDetaching($array_sync);
            }
        }

        return 'ok';
    }

    public function getAllSchools(Request $request)
    {
        $data = collect();

        $schools = School::with(['divisions' => function($q){
            $q->where('name', 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ');
        }])
        ->whereIn('type', ['gym', 'lyk', 'epal'])
        ->orderBy('order')
        ->get();

        foreach($schools as $school){
            $divs = collect();

            $list = Division::where('school_type', $this->schoolType[$school->type])->where('name', 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ')->get();

            if($school->divisions->isEmpty()){
                foreach($list as $l){
                    $divs->push([
                        'id'    => $l->id,
                        'class' => $l->class,
                        'students'  => 0,
                        'number' => 0
                    ]);                    
                }
            }else{
                foreach($list as $l){
                    $number = $school->divisions->where('pivot.div_id', $l->id)->first()->pivot->number;
                    $students = $school->divisions->where('pivot.div_id', $l->id)->first()->pivot->students;
                    

                    $divs->push([
                        'id'    => $l->id,
                        'class' => $l->class,
                        'students'  => $students,
                        'number' => $number
                    ]);                    
                }
            }

            $data->push([
                'name'  => $school->name,
                'id'    => $school->id,
                'divisions' => $divs,
                'type'      => $this->schoolType[$school->type]
            ]);
        }
        return $data;
    }

    public function saveDivisions(Request $request)
    {
        if($request->ajax()){     
            
            $this->user = $request->user();
            $this->school = $this->user->userable;

            $this->synchronizeDivisions($request);

            $user = Role::where('slug', 'pysde_secretary')->first()->users->first();

           Notification::send($user, new SchoolNotification($this->user,[
               'title'         => 'Αριθμός Τμημάτων '. $this->school->name,
               'description'   => "Κενά - Πλεονάσματα",
               'type'          => 'primary',
               'url'           => route('Pysde::Secretary::assignment::vacuumPerSchool', str_replace(' ', '-', $this->school->name))
           ]));

            return $this->successSaveMessage;
        }
    }

    public function saveGymnasiumDivisions(Request $request)
    {
        if($request->ajax()){
            $this->user = $request->user();
            $this->school = $this->user->userable;

            $this->synchronizeDivisions($request);

        //     $user = Role::where('slug', 'pysde_secretary')->first()->users->first();

        //    Notification::send($user, new SchoolNotification($this->user,[
        //        'title'         => 'Αριθμός Τμημάτων '. $this->school->name,
        //        'description'   => "Κενά - Πλεονάσματα",
        //        'type'          => 'primary',
        //        'url'           => route('Pysde::Secretary::assignment::vacuumPerSchool', str_replace(' ', '-', $this->school->name))
        //    ]));

            return $this->successSaveMessage;
        }    
    }

    public function sentDivisions(Request $request)
    {
        if($request->ajax()){
            $lock = true;
            $this->user = $request->user();
            $this->school = $this->user->userable;

            $file_name1 = 'pinakas-kenwn-pleonasmatwn.pdf';
            $file_name2 = 'pinakas-tmimatwn.pdf';

            $this->synchronizeDivisions($request, $lock);

            $this->runAlgorithmAnathesewn(); // RUN ALGORITH A

            $this->makePDFforSchool($this->loadPdfData($request, 'για βελτιώσεις - οριστικές τοποθετήσεις'),'Organika', $file_name1, 'pysde::school.organika.pdf.array-divisions');
            $this->makePDFforSchool($this->loadPdfData($request, 'για βελτιώσεις - οριστικές τοποθετήσεις'),'Organika', $file_name2, 'pysde::school.organika.pdf.array-vacuum');
 
            $this->notifyPysde($file_name1, $file_name2);
            
            return $this->successSentMessage;
        }
    }

    public function sentGymnasiumDivisions(Request $request)
    {
        
        $lock = true;
        $this->user = $request->user();
        $this->school = $this->user->userable;

        $file_name1 = 'pinakas-kenwn-pleonasmatwn.pdf';
        $file_name2 = 'pinakas-tmimatwn.pdf';

        $this->synchronizeDivisions($request, $lock);

        $this->runAlgorithmAnathesewn(); // RUN ALGORITH A

        $this->makePDFforSchool($this->loadPdfData($request, 'για βελτιώσεις - οριστικές τοποθετήσεις'),'Organika', $file_name1, 'pysde::school.organika.pdf.array-divisions');
        $this->makePDFforSchool($this->loadPdfData($request, 'για βελτιώσεις - οριστικές τοποθετήσεις'),'Organika', $file_name2, 'pysde::school.organika.pdf.array-vacuum');

        $this->notifyPysde($file_name1, $file_name2);

        return $this->successSentMessage;
    }

    public function getDivisionsOfSchoolLyceum(Request $request)
    {
        $this->getSchoolInformation($request);

        // $projects = $this->getProjects();

        $projects = $this->getErgastiria();

        $this->createFetchingData($projects);

        return $this->data;
    }

    public function getDivisionsOfSchoolOther(Request $request)
    {
        $this->getSchoolInformation($request);

        $this->createFetchingDataOtherSchool();

        $this->data['school'] = $this->school;
        $this->data['organikes'] = $this->organikes;
        $kena = $this->school->kena;

        $eidikotites = collect();

        $kena_sum_hours = collect();

        $existing_eidikotites = [];

        if(count($kena) == 0){
            foreach($this->organikes as $key=>$organiki){
                $mySchoolTeacher = $organiki->teacher;
                $eidikotites->push([
                    'id'    => $mySchoolTeacher->new_eidikotita_id,
                    'eid_name'  => $mySchoolTeacher->new_eidikotita_name,
                    'eid_slug'  => $mySchoolTeacher->new_klados,
                    'school_id' => $this->school->id,
                    'ypoxreotiko' => $mySchoolTeacher->ypoxreotiko
                ]);
            }

            $grouped = $eidikotites->groupBy('id');


            foreach($grouped as $k=>$eid){

                $number_of_teachers = 0;
                $available = 0;

                $existing_eidikotites[] = $k;

                foreach($eid as $k2=>$rec){
                    $number_of_teachers++;
                    $available = $available + $rec['ypoxreotiko'];
                }

                $kena_sum_hours->push([
                    'id'    => $k,
                    'name'  => $eid[0]['eid_name'],
                    'slug'  => $eid[0]['eid_slug'],
                    'school_id' => $this->school->id,
                    'forseen'   => 0,
                    'available'   => $available,
                    'difference'   => $available,
                    'number_of_teachers' => $number_of_teachers,
                    'project_hours'   => 0,
                    'council_forseen' => 0,
                    'council_difference' => $available,
                    'locked' => false
                ]);
            }

            $this->data['locked'] = false;

        }else{
            foreach($kena as $key=>$keno){
                $kena_sum_hours->push([
                    'id'    => $keno->id,
                    'name'  => $keno->eidikotita_name,
                    'slug'  => $keno->eidikotita_slug,
                    'school_id' => $this->school->id,
                    'forseen'   => $keno->pivot->forseen,
                    'available'   => $keno->pivot->available,
                    'difference'   => $keno->pivot->difference,
                    'number_of_teachers' => $keno->pivot->number_of_teachers,
                    'project_hours'   => $keno->pivot->project_hours,
                    'council_forseen' => $keno->pivot->council_forseen,
                    'council_difference' => $keno->pivot->council_difference,
                    'locked' => $keno->pivot->locked
                ]);

                $existing_eidikotites[] = $keno->id;
            }

            if($kena[0]['pivot']['locked']){
                $this->data['locked'] = true;
            }else{
                $this->data['locked'] = false;
            }

        }

        $sorted_kena = $kena_sum_hours->sortBy('id')->values()->all();

        $this->data['eidikotites'] = NewEidikotita::whereNotIn('id', $existing_eidikotites)->get();
        
        $this->data['kena_sum_hours'] = $sorted_kena;
        $this->data['existing_eidikotites'] = $existing_eidikotites;
        $this->data['school'] = $this->school;

        return $this->data; 
    }

    public function getDivisionsOfSchoolGymnasium(Request $request)
    {
        $this->getSchoolInformation($request);

        $projects = $this->getErgastiria();

        $this->createFetchingDataGymnasium($projects);
        
        return $this->data;
    }

    public function saveOtherDivisions(Request $request, $lock = false)
    {
            $this->school = $request->user()->userable;

            $eidikotites = $request->get('eidikotites');

            $this->synchronizeDivisions($request, $lock);
    
            $sync_array = array();
    
            foreach($eidikotites as $key=>$eidikotita){
                
                $sync_array[$eidikotita['id']] = [
                    'forseen'               => $eidikotita['forseen'],
                    'council_forseen'       => $eidikotita['forseen'],
                    'available'             => $eidikotita['available'],
                    'difference'            => (int) $eidikotita['available'] - (int) $eidikotita['project_hours'] - (int) $eidikotita['forseen'],
                    'council_difference'    => (int) $eidikotita['available'] - (int) $eidikotita['project_hours'] - (int) $eidikotita['forseen'],
                    'project_hours'         => $eidikotita['project_hours'],
                    'number_of_teachers'    => $eidikotita['number_of_teachers'],
                    'sum_pe04'              => 0,
                    'locked'                => $lock
                ];
            }

            $this->school->kena()->sync($sync_array);
    
            return 'Οι αλλαγές <strong>αποθηκεύτηκαν</strong> με επιτυχία!!!';
    }

    public function sentOtherDivisions(Request $request)
    {
        Log::alert("888 outside");

            Log::alert("888a inside");
        
            $lock = true;
            $this->user = $request->user();
            $this->school = $this->user->userable;

            $file_name1 = 'pinakas-kenwn-pleonasmatwn.pdf';

            $this->saveOtherDivisions($request, $lock);

            $this->makePDFforSchool($this->loadPdfData($request, 'για βελτιώσεις - οριστικές τοποθετήσεις'),'Organika', $file_name1, 'pysde::school.organika.pdf.array-divisions');
 
            \Artisan::call('kena:pinakas_a',[
                'type'          => $this->school_type[$this->school->type],
                'one_school'    => 'true',
                'sch_id'        => $this->school->id
            ]);

            $this->notifyPysde($file_name1);
            
            return $this->successSentMessage;
    }
}