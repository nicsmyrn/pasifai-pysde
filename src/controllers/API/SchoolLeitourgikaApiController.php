<?php

namespace Pasifai\Pysde\controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\PraxiAnalipsisRequest;
use App\MySchoolTeacher;
use App\NewEidikotita;
use App\Rules\PraxiRule;
use App\School;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Log;
use Pasifai\Pysde\controllers\traits\LeitourgikaTrait;
use Validator;

class SchoolLeitourgikaApiController extends Controller      //TODO must change permissions, E-mail and Notifications
{
    use LeitourgikaTrait;

    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function uploadTeacherFile(PraxiAnalipsisRequest $request)
    {
        $myschoolTeacher = MySchoolTeacher::find($request->get('afm'));

        if($myschoolTeacher != null){

            
            return  $this->insertFile($request, $myschoolTeacher);


        }else{
            return ['Teacher does not exist'];
        }

    }

    public function uploadGlobalFile(Request $request)
    {
        if($request->hasFile('uploadFile')){

            $file=  $request->file('uploadFile')->store('temp3');

            // $file_extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

            // $file_name = 'uploadedExcel.' . $file_extension;

            // $file_path = "app/public/Deliverables/$file_name";

            // $file->storeAs("public/Deliverables/", $file_name);

            return $file;
            // $this->company[$property] = Storage::url($file_path);
        }else{
            return 'error23';
        }

        $file_path = $this->insertFile($request);
    }

    public function getTeachers(Request $request)
    {
        if($request->ajax()){
            $user = $request->user();

            $data = array();
            $school_types = array();
    
            $all_types =  config('requests.topothetisis_types');
    
            $available_types = config('requests.teacher_types_available_for_school');
    
            foreach($available_types as $index){
                $school_types[$index] = $all_types[$index];
            }
    
            $topothetisis = $user->userable->topothetisis->sortBy('eidikotita')->values()->all();


            foreach($topothetisis as $teacher){

                $special_topothetisis = $teacher->special_topothetisis;

                $teacher->praxi = $teacher->praxeis->where('identifier', $user->userable->identifier)->first();

                $other_topothetisis = $teacher->topothetisis->where('identifier', '<>', $user->userable->identifier)->whereNotIn('pivot.teacher_type', [1,2,3,4,9,11]);
                
                $sum_special = $special_topothetisis == null ? 0 : $special_topothetisis->sum('pivot.hours');
                $sum_other = $other_topothetisis == null ? 0 : $other_topothetisis->sum('pivot.hours');

                $teacher->sum_other_topothetisis =  $sum_special + $sum_other;
                
                $teacher->file_upload = null;
                $teacher->loadingSpinner = false;
                $teacher->file_uploaded = false;
            }

            Log::warning("666");
            Log::warning($available_types);

            $data['topothetisis'] = $topothetisis;
            $data['all_types'] = $all_types;
            $data['editable_types'] = $school_types;
            $data['editable_types_list'] = $available_types;
            $data['school_type'] = $user->userable->type;
    
            return $data;
        }
    }

    public function getEidikotites(Request $request)
    {
        if($request->ajax()){
            $user = $request->user();

            $data = array();
        
    
            $data['leitourgika_kena'] = $user->userable->leitourgika_kena;

            $data['has_unlocked'] =  $data['leitourgika_kena']->search(function($item, $key){
                return !$item->pivot->sch_locked;
            });

            $list = $data['leitourgika_kena']->pluck('id');

            $data['other_eidikotites'] = NewEidikotita::where(function($q) use ($list){
                $q->whereNotIn('id', $list);
                $q->where('eidikotita_name', 'NOT LIKE', '%ΕΙΔΙΚΗΣ%');
            })->get();

            $data['school_type'] = $user->userable->type;
            $data['school_name'] = $user->userable->name;
            $last_updated = $data['leitourgika_kena']->sortByDesc('pivot.updated_at')->values()->first();
    
            $data['last_update'] = Carbon::createFromFormat('Y-m-d H:i:s', $last_updated->pivot->updated_at)->format('d/m/Y και ώρα H:i:s');
            
            return $data;
        }
    }


    public function saveTeachers(Request $request)
    {
        if($request->ajax()){
            $data = array();

            // $teacher_type_that_are_in_school = config('requests.teacher_types_that_is_in_school');

            $user = $request->user();

            $school_id =  $user->userable->id;

            $teachers = $request->get('teachers');

            // $list_of_eidikotites = array();

            $apousies = collect();


            foreach($teachers as $teacher){
                $myschool = MySchoolTeacher::find($teacher['afm']);
                if($myschool != null){
                    if(array_key_exists('rowUpdated', $teacher['pivot'])){
                        if($teacher['pivot']['rowUpdated']){
                            $date_ends = ($teacher['pivot']['date_ends'] == NULL || $teacher['pivot']['date_ends'] == '' || $teacher['pivot']['date_ends'] == 'NULL')  ? config('requests.date_ends') : $teacher['pivot']['date_ends'];

                            $myschool->meiwsi = $teacher['meiwsi'] == null ? 0 : $teacher['meiwsi'];
                            $myschool->save();

                            $myschool->topothetisis()->updateExistingPivot($teacher['pivot']['sch_id'], [
                                'teacher_type'      => $teacher['pivot']['teacher_type'],
                                'date_ends'         => $teacher['pivot']['teacher_type'] == 0 ? null : $date_ends,
                                'hours'             => $teacher['pivot']['hours'] == null ? 0 : $teacher['pivot']['hours'],
                                'project_hours'     => $teacher['pivot']['project_hours'] == null ? 0 : $teacher['pivot']['project_hours'],
                            ]);

                            $apousies->push([
                                'myschool'          => $myschool,
                                'teacher_type'      => $teacher['pivot']['teacher_type'],
                                'date_ends'         => $teacher['pivot']['teacher_type'] == 0 ? null : $date_ends,
                                'hours'             => $teacher['pivot']['hours'] == null ? 0 : $teacher['pivot']['hours'],
                                'project_hours'     => $teacher['pivot']['project_hours'] == null ? 0 : $teacher['pivot']['project_hours'],
                            ]);
                        }
                    }
                }
            }

            if(!$apousies->isEmpty()){
                Log::warning($apousies);
                
                $this->notifyPysde($apousies);
            }

            // $t = collect($teachers);
            
            // $groupByEidikotita = $t->groupBy('eidikotita');

            // foreach($groupByEidikotita as $key=>$eidikotita){

            //     $count_teachers = $eidikotita->count();

            //     $teachers_in_school = $eidikotita->whereIn('pivot.teacher_type', $teacher_type_that_are_in_school);

            //     $current_eidikotita = NewEidikotita::where('eidikotita_slug', $key)->first();

            //     $available = $teachers_in_school->sum('ypoxreotiko') - $teachers_in_school->sum('meiwsi') - $teachers_in_school->sum('sum_other_topothetisis');

            //     if($current_eidikotita != null){
            //         $list_of_eidikotites[] = $current_eidikotita->id;
                    
            //         $record = $user->userable->leitourgika_kena->where('id', $current_eidikotita->id)->first();
                    
            //         if($record != null){
            //             $user->userable->leitourgika_kena()->updateExistingPivot($current_eidikotita->id,[
            //                 'available'             => $available,
            //                 'difference'            => $record != null ? $available + $record->pivot->project_hours  - $record->pivot->forseen : 0,
            //                 'user_id'               => $user->id,
            //                 'number_of_teachers'    => $count_teachers
            //             ]);                    
            //         }else{
            //             $user->userable->leitourgika_kena()->attach($current_eidikotita->id, [
            //                 'available'             => $available,
            //                 'difference'            => $record != null ? $available + $record->pivot->project_hours  - $record->pivot->forseen : 0,
            //                 'user_id'               => $user->id,
            //                 'number_of_teachers'    => $count_teachers,
            //                 'locked'                => false,
            //                 'forseen'               => $available
            //             ]);
            //         }

            //     }
            // }

            // foreach($user->userable->leitourgika_kena as $keno_eidikotitas){
            //     if(!in_array($keno_eidikotitas->id, $list_of_eidikotites)){
            //         $user->userable->leitourgika_kena()->updateExistingPivot($keno_eidikotitas->id, [
            //             'available'             => 0,
            //             'forseen'               => $keno_eidikotitas->pivot->forseen,
            //             'difference'            => - $keno_eidikotitas->pivot->forseen,
            //             'number_of_teachers'    => 0
            //         ]);
            //     }
            // }
            // ########################################
            // TODO ADD leitourgika:update by school ID
            // ########################################
    
            Artisan::call('leitourgika:update', ['to' => $school_id]);

            $data['header'] = 'Συγχαρητήρια!';
            $data['message'] = ['οι αλλαγές αποθηκεύτηκαν με επιτυχία...'];
            $data['type'] = 'success';
    
            return $data;
        }
    }

    public function saveEidikotitaForSchool(Request $request)
    {
        if($request->ajax()){
            $data = [];
            $user = $request->user();
            $eidikotita = $request->get('eidikotita');

            $user->userable->leitourgika_kena()->updateExistingPivot($eidikotita['id'], [
                'sch_difference'    => $eidikotita['pivot']['sch_difference'],
                'sch_description'   => $eidikotita['pivot']['sch_description']
            ]); 
            $data['message'] = 'all_ok';
            $data['difference'] = $eidikotita['pivot']['sch_difference'];

            return $data;
        }  
    }

    public function attachNewEidikotita(Request $request)
    {
        if($request->ajax()){
            $user = $request->user();
            $new_eidikotita = $request->get('new_eidikotita');

            $user->userable->leitourgika_kena()->attach($new_eidikotita['id'], [
                'available'             => 0,
                'difference'            => 0,
                'user_id'               => $user->id,
                'number_of_teachers'    => 0,
                'locked'                => false,
                'forseen'               => 0,
                'sch_difference'        => - $request->get('new_forseen')
            ]);

            return 'all_ok';
        }  
    }

    public function detachEidikotita(Request $request)
    {
        if($request->ajax()){
            $user = $request->user();
            $eidikotita_id = $request->get('eidikotita_id');

            $user->userable->leitourgika_kena()->detach($eidikotita_id);

            return 'all_ok';
        }  
    }

    public function saveEidikotites(Request $request)
    {
        if($request->ajax()){
            $data = array();
            $sync_array = array();

            $user = $request->user();

            foreach($request->get('eidikotites') as $eidikotita){
                $sync_array[$eidikotita['id']] = [
                    'forseen'               => $eidikotita['pivot']['forseen'],
                    'available'             => $eidikotita['pivot']['available'],
                    'project_hours'         => $eidikotita['pivot']['project_hours'],
                    'locked'                => $eidikotita['pivot']['locked'],
                    'number_of_teachers'    => $eidikotita['pivot']['number_of_teachers'],
                    'user_id'               => $user->id,
                    'difference'            => $eidikotita['pivot']['available'] - $eidikotita['pivot']['project_hours'] - $eidikotita['pivot']['forseen'],
                ];
            };
            
            $user->userable->leitourgika_kena()->sync($sync_array);

            $data['header'] = 'Συγχαρητήρια!';
            $data['message'] = ['οι αλλαγές αποθηκεύτηκαν με επιτυχία...'];
            $data['type'] = 'success';
    
            return $data;
        }

    }

    protected function insertFile($request, $myschoolTeacher)
    {
        if($request->hasFile('uploadFile')){

            $file=  $request->file('uploadFile');

            $file_extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

            $file_name = snake_case($myschoolTeacher->last_name . '-' . $myschoolTeacher->first_name . '_' . $myschoolTeacher->afm) . '.'. $file_extension;

            $school = School::find($request->get('sch_id')); 

            if($school != null){
                $file->storeAs("schools/{$school->identifier}", $file_name);
            
                $school->praxeis()->attach($myschoolTeacher['afm'], [
                    'filename' => "schools/{$school->identifier}/$file_name",
                    'uploaded' => true
                ]);

                return $file;

            }else{
                return 'school does not exist';
            }
        }else{
            return 'no file';
        }
    }
}