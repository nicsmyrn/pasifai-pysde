<?php

namespace Pasifai\Pysde\controllers\API;


use App\MySchoolTeacher;
use App\Http\Controllers\Controller;
use App\NewEidikotita;
use Pasifai\Pysde\models\OrganikiRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
class Auto extends Controller
{
    private $sort_veiltiwseis = [
        'special_situation'  => 'asc',
        'moria' => 'desc',
        'last_name' => 'asc'
    ];

    public function __construct()
    {
        
        // $this->middleware('auth:api'); 
    }

    public function getKena()
    {
        $kladoi_list =  NewEidikotita::with(['kena' => function($q){
            $q->where('council_difference', '<=', -12);
        }])->get()->filter(function($value, $key){
            return count($value['kena']) > 0;
        })->pluck('klados_slug');

        $kena_pleonasmata =  NewEidikotita::with(['kena' => function($q){
            $q->where('council_difference', '<>', 0);
        }])->get()->filter(function($value, $key){
            return count($value['kena']) > 0;
        });

        return [
            'kladoi_list' => $kladoi_list->all(),
            'kena_pleonasmata' => $kena_pleonasmata->values()
        ];
    }

    public function saveOrganikes(Request $request)
    {
        return 'hi';
    }

    public function getPleonasmata()
    {

        $kena =  NewEidikotita::with(['kena' => function($q){
            $q->where('council_difference', '>=', 12);
        }])->get()->filter(function($value, $key){
            return count($value['kena']) > 0;
        });

        return $kena->values();
    }
 
    public function getRequests()
    {
        $from = '2023-02-01 00:29:06';
        $to = '2023-04-26 11:29:06';
        $type = 'Βελτίωση';
        $order = 'date_request';
        $orderByMoria = true;      

        $requests = OrganikiRequest::with('prefrences')
                ->where('groupType', $type)
                ->whereNotNull('protocol_number')
                ->whereBetween('date_request', [$from, $to])
                ->where(function($q){
                    $q->whereNull('recall_situation')
                    ->orWhereIn('recall_situation', ['Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ', 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ']);
                })
                ->orderBy($order)
                ->get();

        $requests = $requests->each(function ($item, $key) use ($type) {
            $item->klados = $item->teacher->myschool->new_klados;
            $item->myschool_organiki_identifier = $item->teacher->myschool->organiki_prosorini;
            $item->myschool_organiki = $item->teacher->myschool->organiki_name;
            $item->myschool_organiki_id = $item->teacher->myschool->organiki_id;
            $item->klados_united = $item->teacher->myschool->new_klados_name;
            $item->full_name = $item->teacher->user->full_name;
            $item->moria = $item->teacher->teacherable->moria;
            $item->group_name = $item->teacher->teacherable->group_name;
            $item->middle_name = $item->teacher->middle_name;
            $item->special_situation = $item->teacher->special_situation ? 'ΝΑΙ' : 'ΟΧΙ';
        });

        if ($orderByMoria) {
            $requests = $requests->sortByDesc('moria');
        }

        $sorted = $requests->sort($this->compareCollectionVeltiwseis())->values();

        return $sorted;

        /*
        return $request->wantsJson()
                    ? new JsonResponse([], 204)
                    : redirect()->intended($this->redirectPath());
        */
    }

    private function compareCollectionVeltiwseis()
    {
        return function ($a, $b) {
            foreach($this->sort_veiltiwseis as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }
}