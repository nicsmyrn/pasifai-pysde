<?php

namespace Pasifai\Pysde\controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Pasifai\Pysde\models\RequestTeacherYperarithmia;
use Pasifai\Pysde\models\OrganikiRequest;
use Carbon\Carbon;
use DB;
use Pasifai\Pysde\models\ProtocolTeacher;
use App\Models\Role;
use App\User;
use Illuminate\Support\Facades\Notification;
use Pasifai\Pysde\notifications\TeacherToPysdeNotification;
use Pasifai\Pysde\controllers\traits\RequestsTrait;
use Pasifai\Pysde\models\Protocol;

class RequestApiController extends Controller      //TODO must change permissions, E-mail and Notifications
{
    use RequestsTrait;

    protected $all_requests;

    private $types = [
        'Αίτηση-Υπεραριθμίας'   => 'Υπεραρ',
        'Αίτηση-Οργανικής-Υπεράριθμου'  => 'ΟργανικήΥπ',
        'Αίτηση-Βελτίωσης-Οριστικής-Τοποθέτησης'    => 'Βελτίωση',
        'Αίτηση-Μοριοδότησης-Μετάθεσης' => 'Μοριοδ',
        'Αίτηση-Μοριοδότησης-Απόσπασης' => 'ΜοριοδΑ',
        'bla-1'                         => 'E1',
        'bla-2'                         => 'E2',
        'bla-3'                         => 'E3'
    ];

    private $groupTypeTitle = [
        'Βελτίωση'                  => 'ΒΕΛΤΙΩΣΗ - ΟΡΙΣΤΙΚΗ ΤΟΠΟΘΕΤΗΣΗ',
        'ΟργανικήΥπεράριθμου'       => 'ΟΡΓΑΝΙΚΑ ΥΠΕΡΑΡΙΘΜΟΣ',
        'Λειτουργικά Υπεράριθμος'   => 'Ε1',
        'Διάθεση'                   => 'Ε1',
        'Συμπλήρωση'                => 'Ε2',
        'Απόσπαση Εντός'            => 'Ε3',
        'Αναπληρωτής'               => 'Ε4',
        'Αναπληρωτής - Covid 19'      => 'Ε5',
        'Διάθεση ΠΥΣΔΕ - ΝΕΟΔΙΟΡΙΣΤΟΣ' => 'E6'
    ];

    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function destroy($unique_id, Request $request)
    {
        $requestTeacher = RequestTeacherYperarithmia::where('unique_id', $unique_id)->first();

        if($requestTeacher!=null){
            $requestTeacher->delete();
        }else{
            $requestTeacher = OrganikiRequest::where('unique_id', $unique_id)->delete();
        }

        return ['destroyed'];
    }

    public function getAllRequests(Request $request)
    {
        $this->all_requests = collect();

        $this->user = $request->user();

        $this->getRequestsYperarithmias();
        $this->getAllOtherRequests();

        return $this->all_requests;
    }

    public function recallRequest($unique_id, Request $request)
    {
        $requestTeacher = RequestTeacherYperarithmia::where('unique_id', $unique_id)->first();

        if($requestTeacher==null){
            $requestTeacher = OrganikiRequest::where('unique_id', $unique_id)->first();
            if($requestTeacher == null){
                return ['Error'];
            }
        }

        $requestTeacher->recall_date = Carbon::now();
        $requestTeacher->recall_situation = 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ';
        $requestTeacher->save();

        $this->ajaxCreateRecallRequest($requestTeacher, $request->user());

        return ['recalled'];
    }

    private function getRequestsYperarithmias()
    {
        $requestsYperarithmias = RequestTeacherYperarithmia::where('teacher_id', $this->user->userable->id)->get();

        if(!$requestsYperarithmias->isEmpty()){
            foreach($requestsYperarithmias as $yp){
                $this->all_requests->push([
                    'unique_id'         => $yp->unique_id,
                    'protocol_number'   => $yp->protocol_number,
                    'type'              => 'ΕΠΙΘΥΜΙΑ ΥΠΕΡΑΡΙΘΜΙΑΣ',
                    'date'              => Carbon::parse($yp->date_request)->format('d/m/Y'),
                    'protocol_name'     => $yp->protocol_number != null ? $yp->protocol_name : null,
                    'recall_situation'  => $yp->recall_situation,
                    'recall_date'       => $yp->recall_date,
                    'url'               => route('Request::OrganikiType', ['Αίτηση-Υπεραριθμίας', $yp->unique_id]),
                    'downloadUrl'       => route('Request::downloadPDF', $yp->unique_id)
                ]);
            }
        }
    }

    private function getAllOtherRequests()
    {
        $requestsOrganikaYperarithmos = OrganikiRequest::where('teacher_id', $this->user->userable->id)->get();

        if(!$requestsOrganikaYperarithmos->isEmpty()){
            $groupRequests = $requestsOrganikaYperarithmos->groupBy('unique_id');

            foreach($groupRequests as $group){
                $yp = $group->first();

                if(array_key_exists($yp->groupType, $this->groupTypeTitle)){
                    $type = $this->groupTypeTitle[$yp->groupType];
                }else{
                    $type = 'Error 8475';
                }

                if(in_array($yp->situation, ['E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7'])){
                    $url = route('Request::create', [$yp->situation, $yp->unique_id]);
                }else{
                    $url = route('Request::OrganikiType', [array_search($yp->situation, $this->types), $yp->unique_id]);
                }

                $this->all_requests->push([
                    'unique_id'         => $yp->unique_id,
                    'protocol_number'   => $yp->protocol_number,
                    'type'              => $type,
                    'date'              => Carbon::parse($yp->date_request)->format('d/m/Y'),
                    'protocol_name'     => $yp->protocol_number != null ? $yp->protocol_name : null,
                    'recall_situation'  => $yp->recall_situation,
                    'recall_date'       => $yp->recall_date,
                    'url'               => $url,
                    'downloadUrl'       => route('Request::downloadPDF', $yp->unique_id)
                ]);
            }
        }
    }

    private function ajaxCreateRecallRequest($request, $user)
    {
        $unique_id = uniqid($user->id);

            DB::beginTransaction();

            $teacher_folder = str_slug($user->last_name) . md5($user->userable->id);
            $file_path = $teacher_folder . '/' . $unique_id . '.pdf';

            $protocol = Protocol::create([
                'p_date'    =>  date('d/m/Y'),
                'from_to'      => $user->full_name . ' του '. $user->userable->middle_name,
                'type'      => 0,
                'teacher_type'      => 'ΑΝΑΚΛΗΣΗ',
                'subject'   => 'ΑΝΑΚΛΗΣΗ ΑΙΤΗΣΗΣ',
                'f_id'      => 2,   // Φ.2.1
                'description'   => 'με αριθμό πρωτοκόλλου: '. $request->unique_id
            ]);

            $myschool = $user->userable->myschool;

            $attributes = [
                'teacher_name'              => $user->full_name,
                'protocol_name'             => $protocol->protocol_name,
                'myschool'                  => $myschool,
                'new_klados'                => $myschool->new_klados,
                'new_eidikotita_name'       => $myschool->new_eidikotita_name,
                'organiki_name'             => $myschool->organiki_name,
                'date_request'              => $protocol->created_at,
                'prototype_unique_id'       => $request->unique_id,
                'prototype_protocol_name'   => $request->protocol_name,
                'recall_stamp'              => null
            ];

            $this->makePDFforRequests($attributes, $teacher_folder, $file_path, 'pysde::aitisis.organika.teacher.pdf.RECALL', 'recallRequest',$user->sch_mail, $user->full_name);

            // $pysde_secretary = Role::where('slug', 'pysde_secretary')->first()->users->first();
            $pysde_secretary = User::where('email', config('requests.master_mail'))->first();
            
            $email = $this->getTheCorrectMail($user);

            Notification::send($pysde_secretary, new TeacherToPysdeNotification($user,[
                'title'         => 'Ανάκληση Αίτησης',
                'description'   => "αίτημα ανάκλησης αίτησης με κωδικό $unique_id",
                'type'          => 'danger',
                'url'           => route('PysdeAdmin::indexRequests'),
                'subject'       => 'ΑΝΑΚΛΗΣΗ αίτησης',
                'file'          => storage_path('app/teachers/'.$file_path),
                'mail'          => $email
            ],'pysde::email.teacher-cancelation-request'));

            DB::commit();
    }


}
