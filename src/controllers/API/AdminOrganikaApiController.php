<?php

namespace Pasifai\Pysde\controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;
use Pasifai\Pysde\models\Division;
use Pasifai\Pysde\models\WrologioLesson;

class AdminOrganikaApiController extends Controller 
{
    protected $data = [];

    public function __construct()
    {
        $this->middleware('auth:api'); 
    }

    public function getDivisions()
    {
        $divisions = Division::all();

        foreach($divisions as $k=>$division){
            $wrologio = $division->wrologio;

            foreach($wrologio as $key=>$lesson){
                $lesson->pivot->eidikotites;
            }
        }

        return $divisions->groupBy(['school_group', 'class', 'name']);
    }


    public function updateLessonHours(Request $request)
    {
        $wrologio = WrologioLesson::find($request->get('id_kena_div'));

        if($wrologio != null){
            $wrologio->hours = $request->get('newHours');
            $wrologio->save();
            return 'saved...';
        }

        return 'error...';

    }

    public function deletePermanentLessonDiv(Request $request)
    {
        $id = $request->get('id');

        Log::alert("ID: $id");

        $wrologio = WrologioLesson::find($id);

        if($wrologio != null){
            $wrologio->delete();
            return 'ok...';
        }

        return 'not deleted...';

    }
}