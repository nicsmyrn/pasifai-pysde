<?php

namespace Pasifai\Pysde\controllers\traits;

trait Calculator
{

    private $sort = [
        'group'            => 'asc',
        'school_order'     => 'asc',
        'last_name'        => 'asc'
    ];


    private $sort_veltiwseis = [
        'eidiki_katigoria'  => 'asc',
        'moria'             => 'desc',
        'last_name'         => 'asc'
    ];

    private function compareCollectionVeltiwseis()
    {
        return function ($a, $b) {
            foreach($this->sort_veltiwseis as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }


    private function compareCollection()
    {
        return function ($a, $b) {
            foreach($this->sort as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }
    
}