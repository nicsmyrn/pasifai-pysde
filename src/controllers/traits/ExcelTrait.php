<?php

namespace Pasifai\Pysde\controllers\traits;

use App\School;
use Auth;
use Carbon\Carbon;
use Log;

trait ExcelTrait
{
    private $divisor = 20;
    private $limit = 12;
    private $limit_organika = 7;
    private $days_after = 90;

    protected function getTeachersLeaveOfAbsence($list_group, $last_row, $event)
    {
        $now = Carbon::now();
        $afterDays = Carbon::now()->addDays(config('requests.after_days_for_adeies_leitourgika'));
        $column = 'B';
        $columnOFFset = 'Q';
        $offset = $last_row + 2;
               
        $schools = School::with('topothetisis')->whereIn('group', $list_group)->get();

        foreach($schools as $school){
            foreach($school->topothetisis as $topothetisi){
                if($topothetisi->pivot->date_ends != null){
                    $school = School::find($topothetisi->pivot->sch_id);

                    $date_ends = Carbon::createFromFormat('d/m/Y', $topothetisi->pivot->date_ends);
                        
                    if($date_ends->between($now, $afterDays)){
                        $cell_value = "[$topothetisi->eidikotita] : {$school->name}  το κενό ισχύει εως {$date_ends->format('d/m/Y')}";

                        $event->sheet->getDelegate()->mergeCells("$column$offset:$columnOFFset$offset");
                        $event->sheet->getDelegate()->setCellValue("$column$offset", $cell_value);   
                        
                        $event->sheet->getDelegate()->getStyle("$column$offset")->applyFromArray($this->getAbsenceCellStyles());

                        $offset++;
                    }
                }
            }
        }
    }

    protected function summary($eidikotites, $event, $offset, $last_column){
        $grouped = $eidikotites->groupBy('eidikotita_slug');
        
        $row = $offset;

        foreach($grouped as $key=>$value){
            $sum = 0;

            $k = $value[0]['leitourgika_kena'];

            $sum = $k->sum(function($e){
                if($e->type <> 'Ειδικό'){
                    return $e->pivot->difference;
                }
            });

            $event->sheet->getDelegate()->setCellValue($last_column. '1', 'ΣΥΝΟΛΟ');    

            $event->sheet->getDelegate()->setCellValue("$last_column$row", $this->cellLeitourgika($sum));    
            
            $row++;
        }

    }

    protected function summarySchool($eidikotites, $event, $offset, $last_column){
        $grouped = $eidikotites->groupBy('eidikotita_slug');
        
        $row = $offset;

        foreach($grouped as $key=>$value){
            $sum = 0;

            $k = $value[0]['leitourgika_kena'];

            $sum = $k->sum(function($e){
                if($e->type <> 'eidiko'){
                    return $e->pivot->sch_difference;
                }
            });


            $event->sheet->getDelegate()->setCellValue($last_column. '1', 'ΣΥΝΟΛΟ');    

            $event->sheet->getDelegate()->setCellValue("$last_column$row", $this->cellLeitourgika($sum));    
            
            $row++;
        }

    }


    protected function pe04CalculateForLeitourgika($eidikotites, $header, $event, $offset)
    {
        foreach($eidikotites as $eidikotitaIndex=>$e){
            if($e->klados_slug != 'ΠΕ04'){
                foreach($header as $index=>$school){
                    $comment = $school->cellLeitourgikaComment($e->id);
                    if($comment !== ''){
                        $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);
                        $row = $eidikotitaIndex + $offset;
                        $headerComment = $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($school->name. ':');
                        $headerComment->getFont()->setBold(true);
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun("\r\n");
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($comment);
                    }
                }                    
            }
        }

        $pe04 =  $eidikotites->where('klados_slug', 'ΠΕ04');
        $pe04_start = 3;

        $count = 1;
        foreach($pe04 as $key=>$value){
            if($count == 1){
                $pe04_start = $key + $offset;
            }

            $count++;
        }


        $temp_array = array();
        $sum_temp_array = array();

        foreach($pe04 as $key=>$value){
            foreach($value->leitourgika_kena as $school){
                $temp_array[$school->identifier][$value->eidikotita_slug] = $school->pivot->difference; 
            }
        }

        foreach($temp_array as $key=>$school){
            $sum_temp_array[$key] = array_sum($school);
        }

        $pe04_length = $pe04->count() + $pe04_start - 1;

        foreach($header as $index=>$school){

            $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);

            if(array_key_exists($school->identifier, $temp_array)){
                
                if($school->type == 'Λύκειο' || $school->type == 'ΕΠΑΛ'){
                    if(count($temp_array[$school->identifier]) > 0){
                        $counter = 0;
                        foreach($temp_array[$school->identifier] as $key=>$val){
                            // dd($val);
                            $row = $pe04_start + $counter;

                            for($i = $pe04_start; $i <= $pe04_length; $i++){
                                $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                                if($eidikotita_name == $key){
                                    $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellLeitourgika($val));            
                                }
                            }

                            $counter++;    
                        }
                    }

                }else{
                    $sum_kladou_pe04 = $sum_temp_array[$school->identifier];

                    if($sum_kladou_pe04 < 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe04_start:$column$pe04_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe04_start", $sum_kladou_pe04);                        
                    }elseif ($sum_kladou_pe04 > 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe04_start:$column$pe04_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe04_start", $sum_kladou_pe04);    
                        // if(count($temp_array[$school->identifier]) > 0){
                        //     $counter = 0;
                        //     foreach($temp_array[$school->identifier] as $key=>$val){
                        //         // dd($val);
                        //         $row = $pe04_start + $counter;
    
                        //         for($i = $pe04_start; $i <= $pe04_length; $i++){
                        //             $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                        //             if($eidikotita_name == $key){
                        //                 $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellLeitourgika($val));            
                        //             }
                        //         }
    
                        //         $counter++;    
                        //     }
                        // }
                    }

                }

            }else{
                // dd($temp_array);
                // dd($school->identifier);
                // dd('out of range 666');
                Log::warning("ExcelTrait.php pe04CalculateForLeitourgika --> School ID: {$school->identifier}");
            }

        }

    }

    protected function pe04CalculateForLeitourgikaSchools($eidikotites, $header, $event, $offset)
    {
        foreach($eidikotites as $eidikotitaIndex=>$e){
            if($e->klados_slug != 'ΠΕ04'){
                foreach($header as $index=>$school){
                    $comment = $school->cellLeitourgikaComment($e->id);
                    if($comment !== ''){
                        $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);
                        $row = $eidikotitaIndex + $offset;
                        $headerComment = $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($school->name. ':');
                        $headerComment->getFont()->setBold(true);
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun("\r\n");
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($comment);
                    }
                }                    
            }
        }

        $pe04 =  $eidikotites->where('klados_slug', 'ΠΕ04');
        $pe04_start = 3;

        $count = 1;
        foreach($pe04 as $key=>$value){
            if($count == 1){
                $pe04_start = $key + $offset;
            }

            $count++;
        }


        $temp_array = array();
        $sum_temp_array = array();

        foreach($pe04 as $key=>$value){
            foreach($value->leitourgika_kena as $school){
                $temp_array[$school->identifier][$value->eidikotita_slug] = $school->pivot->sch_difference; 
            }
        }

        foreach($temp_array as $key=>$school){
            $sum_temp_array[$key] = array_sum($school);
        }

        $pe04_length = $pe04->count() + $pe04_start - 1;

        foreach($header as $index=>$school){

            $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);

            if(array_key_exists($school->identifier, $temp_array)){
                
                if($school->type == 'Λύκειο' || $school->type == 'ΕΠΑΛ'){
                    if(count($temp_array[$school->identifier]) > 0){
                        $counter = 0;
                        foreach($temp_array[$school->identifier] as $key=>$val){
                            // dd($val);
                            $row = $pe04_start + $counter;

                            for($i = $pe04_start; $i <= $pe04_length; $i++){
                                $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                                if($eidikotita_name == $key){
                                    $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellLeitourgika($val));            
                                }
                            }

                            $counter++;    
                        }
                    }

                }else{
                    $sum_kladou_pe04 = $sum_temp_array[$school->identifier];

                    if($sum_kladou_pe04 < 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe04_start:$column$pe04_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe04_start", $sum_kladou_pe04);                        
                    }elseif ($sum_kladou_pe04 > 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe04_start:$column$pe04_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe04_start", $sum_kladou_pe04);    
                        // if(count($temp_array[$school->identifier]) > 0){
                        //     $counter = 0;
                        //     foreach($temp_array[$school->identifier] as $key=>$val){
                        //         // dd($val);
                        //         $row = $pe04_start + $counter;
    
                        //         for($i = $pe04_start; $i <= $pe04_length; $i++){
                        //             $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                        //             if($eidikotita_name == $key){
                        //                 $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellLeitourgika($val));            
                        //             }
                        //         }
    
                        //         $counter++;    
                        //     }
                        // }
                    }

                }

            }else{
                // dd($temp_array);
                // dd($school->identifier);
                // dd('out of range 666');
                Log::warning("ExcelTrait.php pe04CalculateForLeitourgikaSchools --> School ID: {$school->identifier}");
            }

        }

    }

    protected function pe81CalculateForLeitourgika($eidikotites, $header, $event, $offset)
    {
        foreach($eidikotites as $eidikotitaIndex=>$e){
            if($e->klados_slug != 'ΠΕ81tech'){
                foreach($header as $index=>$school){
                    $comment = $school->cellLeitourgikaComment($e->id);
                    if($comment !== ''){
                        $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);
                        $row = $eidikotitaIndex + $offset;
                        $headerComment = $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($school->name. ':');
                        $headerComment->getFont()->setBold(true);
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun("\r\n");
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($comment);
                    }
                }                    
            }
        }

        $pe81 =  $eidikotites->where('klados_slug', 'ΠΕ81tech');
        $pe81_start = 17;

        $count = 1;
        foreach($pe81 as $key=>$value){
            if($count == 1){
                $pe81_start = $key + $offset;
            }

            $count++;
        }

        $temp_array = array();
        $sum_temp_array = array();

        foreach($pe81 as $key=>$value){
            foreach($value->leitourgika_kena as $school){
                $temp_array[$school->identifier][$value->eidikotita_slug] = $school->pivot->difference; 
            }
        }

        foreach($temp_array as $key=>$school){
            $sum_temp_array[$key] = array_sum($school);
        }

        $pe81_length = $pe81->count() + $pe81_start - 1;

        foreach($header as $index=>$school){
            $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);

            if(array_key_exists($school->identifier, $temp_array)){
                
                if($school->type == 'Λύκειο' || $school->type == 'ΕΠΑΛ'){
                    if(count($temp_array[$school->identifier]) > 0){
                        $counter = 0;
                        foreach($temp_array[$school->identifier] as $key=>$val){
                            // dd($val);
                            $row = $pe81_start + $counter;

                            for($i = $pe81_start; $i <= $pe81_length; $i++){
                                $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                                if($eidikotita_name == $key){
                                    $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellLeitourgika($val));            
                                }
                            }

                            $counter++;    
                        }
                    }

                }else{
                    $sum_kladou_pe81 = $sum_temp_array[$school->identifier];

                    if($sum_kladou_pe81 < 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe81_start", $sum_kladou_pe81);                        
                    }elseif($sum_kladou_pe81 > 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe81_start", $sum_kladou_pe81);    
                        // if(count($temp_array[$school->identifier]) > 0){
                        //     $counter = 0;
                        //     foreach($temp_array[$school->identifier] as $key=>$val){
                        //         // dd($val);
                        //         $row = $pe04_start + $counter;
    
                        //         for($i = $pe04_start; $i <= $pe04_length; $i++){
                        //             $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                        //             if($eidikotita_name == $key){
                        //                 $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellLeitourgika($val));            
                        //             }
                        //         }
    
                        //         $counter++;    
                        //     }
                        // }
                    }else{
                        $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe81_start", "");   
                    }


                }

            }else{
                $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                $event->sheet->getDelegate()->setCellValue("$column$pe81_start", "");    
                // dd($school->name);
                // dd($school->identifier);
                // dd($temp_array);
                // dd('out of range');
            }

        }

    }

    protected function pe81CalculateForOrganika($eidikotites, $header, $event, $offset)
    {
        foreach($eidikotites as $eidikotitaIndex=>$e){
            if($e->klados_slug != 'ΠΕ81tech'){
                foreach($header as $index=>$school){
                    $comment = $school->cellLeitourgikaComment($e->id);
                    if($comment !== ''){
                        $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);
                        $row = $eidikotitaIndex + $offset;
                        $headerComment = $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($school->name. ':');
                        $headerComment->getFont()->setBold(true);
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun("\r\n");
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($comment);
                    }
                }                    
            }
        }

        $pe81 =  $eidikotites->where('klados_slug', 'ΠΕ81tech');
        $pe81_start = 17;

        $count = 1;
        foreach($pe81 as $key=>$value){
            if($count == 1){
                $pe81_start = $key + $offset;
            }

            $count++;
        }

        $temp_array = array();
        $sum_temp_array = array();

        foreach($pe81 as $key=>$value){
            foreach($value->kena as $school){
                $temp_array[$school->identifier][$value->eidikotita_slug] = $school->pivot->difference; 
            }
        }

        foreach($temp_array as $key=>$school){
            $sum_temp_array[$key] = array_sum($school);
        }

        $pe81_length = $pe81->count() + $pe81_start - 1;

        foreach($header as $index=>$school){
            $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);

            if(array_key_exists($school->identifier, $temp_array)){
                
                if($school->type == 'Λύκειο' || $school->type == 'ΕΠΑΛ'){
                    if(count($temp_array[$school->identifier]) > 0){
                        $counter = 0;
                        foreach($temp_array[$school->identifier] as $key=>$val){
                            // dd($val);
                            $row = $pe81_start + $counter;

                            for($i = $pe81_start; $i <= $pe81_length; $i++){
                                $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                                if($eidikotita_name == $key){
                                    $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellVacuum($val, true));            
                                }
                            }

                            $counter++;    
                        }
                    }

                }else{
                    $sum_kladou_pe81 = $sum_temp_array[$school->identifier];

                    Log::warning("School {$school->name} | SUM = $sum_kladou_pe81 | KENO = {$this->cellVacuum($sum_kladou_pe81)}");
                    if($sum_kladou_pe81 < 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe81_start", $this->cellVacuum($sum_kladou_pe81, true));                        
                    }elseif($sum_kladou_pe81 > 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe81_start", $this->cellVacuum($sum_kladou_pe81, true));    
                        // if(count($temp_array[$school->identifier]) > 0){
                        //     $counter = 0;
                        //     foreach($temp_array[$school->identifier] as $key=>$val){
                        //         // dd($val);
                        //         $row = $pe04_start + $counter;
    
                        //         for($i = $pe04_start; $i <= $pe04_length; $i++){
                        //             $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                        //             if($eidikotita_name == $key){
                        //                 $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellLeitourgika($val));            
                        //             }
                        //         }
    
                        //         $counter++;    
                        //     }
                        // }
                    }else{
                        $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe81_start", "");   
                    }


                }

            }else{
                $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                $event->sheet->getDelegate()->setCellValue("$column$pe81_start", "");    
                // dd($school->name);
                // dd($school->identifier);
                // dd($temp_array);
                // dd('out of range');
            }

        }

    }

    protected function pe81CalculateForLeitourgikaSchools($eidikotites, $header, $event, $offset)
    {
        foreach($eidikotites as $eidikotitaIndex=>$e){
            if($e->klados_slug != 'ΠΕ81tech'){
                foreach($header as $index=>$school){
                    $comment = $school->cellLeitourgikaComment($e->id);
                    if($comment !== ''){
                        $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);
                        $row = $eidikotitaIndex + $offset;
                        $headerComment = $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($school->name. ':');
                        $headerComment->getFont()->setBold(true);
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun("\r\n");
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($comment);
                    }
                }                    
            }
        }

        $pe81 =  $eidikotites->where('klados_slug', 'ΠΕ81tech');
        $pe81_start = 17;

        $count = 1;
        foreach($pe81 as $key=>$value){
            if($count == 1){
                $pe81_start = $key + $offset;
            }

            $count++;
        }

        $temp_array = array();
        $sum_temp_array = array();

        foreach($pe81 as $key=>$value){
            foreach($value->leitourgika_kena as $school){
                $temp_array[$school->identifier][$value->eidikotita_slug] = $school->pivot->sch_difference; 
            }
        }

        foreach($temp_array as $key=>$school){
            $sum_temp_array[$key] = array_sum($school);
        }

        $pe81_length = $pe81->count() + $pe81_start - 1;

        foreach($header as $index=>$school){
            $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);

            if(array_key_exists($school->identifier, $temp_array)){
                
                if($school->type == 'Λύκειο' || $school->type == 'ΕΠΑΛ'){
                    if(count($temp_array[$school->identifier]) > 0){
                        $counter = 0;
                        foreach($temp_array[$school->identifier] as $key=>$val){
                            // dd($val);
                            $row = $pe81_start + $counter;

                            for($i = $pe81_start; $i <= $pe81_length; $i++){
                                $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                                if($eidikotita_name == $key){
                                    $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellLeitourgika($val));            
                                }
                            }

                            $counter++;    
                        }
                    }

                }else{
                    $sum_kladou_pe81 = $sum_temp_array[$school->identifier];

                    if($sum_kladou_pe81 < 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe81_start", $sum_kladou_pe81);                        
                    }elseif($sum_kladou_pe81 > 0){
                        $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe81_start", $sum_kladou_pe81);    
                        // if(count($temp_array[$school->identifier]) > 0){
                        //     $counter = 0;
                        //     foreach($temp_array[$school->identifier] as $key=>$val){
                        //         // dd($val);
                        //         $row = $pe04_start + $counter;
    
                        //         for($i = $pe04_start; $i <= $pe04_length; $i++){
                        //             $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                        //             if($eidikotita_name == $key){
                        //                 $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellLeitourgika($val));            
                        //             }
                        //         }
    
                        //         $counter++;    
                        //     }
                        // }
                    }else{
                        $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                        $event->sheet->getDelegate()->setCellValue("$column$pe81_start", "");   
                    }


                }

            }else{
                $event->sheet->getDelegate()->mergeCells("$column$pe81_start:$column$pe81_length");
                $event->sheet->getDelegate()->setCellValue("$column$pe81_start", "");    
                // dd($school->name);
                // dd($school->identifier);
                // dd($temp_array);
                // dd('out of range');
            }

        }

    }


    protected function pe04Calculate($eidikotites, $header, $event, $offset)
    {
        foreach($eidikotites as $eidikotitaIndex=>$e){
            if($e->klados_slug != 'ΠΕ04'){
                foreach($header as $index=>$school){
                    $comment = $school->cellVacuumComment($e->id);
                    if($comment !== ''){
                        $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);
                        $row = $eidikotitaIndex + $offset;
                        $headerComment = $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($school->name. ':');
                        $headerComment->getFont()->setBold(true);
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun("\r\n");
                        $event->sheet->getDelegate()->getComment($column.$row)->getText()->createTextRun($comment);
                    }
                }                    
            }
        }

        $pe04 =  $eidikotites->where('klados_slug', 'ΠΕ04');
        $pe04_start = 3;
        $count = 1;
        foreach($pe04 as $key=>$value){
            if($count == 1){
                $pe04_start = $key + $offset;
            }

            $count++;
        }


        $temp_array = array();

        foreach($pe04 as $key=>$value){
            foreach($value->kena as $school){
                $temp_array[$school->identifier][$value->eidikotita_slug] = $school->pivot->sum_pe04; 
            }
        }

        
        $pe04_length = $pe04->count() + $pe04_start - 2; // TODO check this
        
        Log::warning("666-->");
        Log::warning($pe04_length);
        Log::warning($pe04->count());
        Log::warning($pe04_start);
        Log::warning("666 <-- ");

        foreach($header as $index=>$school){
            $column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($index + 3);

            $cell_value = '';
            $all_null = true;

            if(array_key_exists($school->identifier, $temp_array)){
                
                $temp = array();
                foreach($temp_array[$school->identifier] as $key=>$value){
                    if($value != null){
                        $temp[$key] = $value;
                        $all_null = false;
                    }
                }

                if($all_null){
                    $cell_value = '';
                }else{
                    if(count($temp) == 0){
                        $cell_value =  '';
                    }elseif(count($temp) == count($temp_array[$school->identifier])){
                        foreach($temp as $key=>$value){
                            $cell_value = $value;
                            break;
                        }
                    }else{
                        $cell_value = 'lykeio_organiko_keno';
                    }
                }

                if($school->type == 'Λύκειο' || $school->type == 'ΕΠΑΛ'){
                    if($cell_value == 'lykeio_organiko_keno'){
                        // if($school->identifier == '5051050'){
                        //     dd($temp);
                        // }
                        if(count($temp) > 0){
                            $counter = 0;
                            foreach($temp as $key=>$val){
                                $row = $pe04_start + $counter;

                                if($val >= 7){
                                    $event->sheet->getDelegate()->mergeCells("$column$pe04_start:$column$pe04_length");
                                    $event->sheet->getDelegate()->setCellValue("$column$row", $this->cellVacuum($val));
                                    break;  
                                }elseif($val <= -12){
                                    for($i = $pe04_start; $i <= $pe04_length; $i++){
                                        $eidikotita_name = $event->sheet->getDelegate()->getCell("A$i")->getValue();
                                        if($eidikotita_name == $key){
                                            $event->sheet->getDelegate()->setCellValue("$column$i", $this->cellVacuum($val));            
                                        }
                                    }
                                }
                                $counter++;   
 
                            }
                        }
                    }else{
                        if($all_null){
                            $cell_value = '';
                            $event->sheet->getDelegate()->mergeCells("$column$pe04_start:$column$pe04_length");
                            $event->sheet->getDelegate()->setCellValue("$column$pe04_start", $this->cellVacuum($cell_value));   
                        }else{
                            $event->sheet->getDelegate()->mergeCells("$column$pe04_start:$column$pe04_length");
                            $event->sheet->getDelegate()->setCellValue("$column$pe04_start", $this->cellVacuum($cell_value));
                        }
                    }

                }else{
                    $event->sheet->getDelegate()->mergeCells("$column$pe04_start:$column$pe04_length");
                    $event->sheet->getDelegate()->setCellValue("$column$pe04_start", $this->cellVacuum($cell_value));
                }

            }else{
                $event->sheet->getDelegate()->mergeCells("$column$pe04_start:$column$pe04_length");
                $event->sheet->getDelegate()->setCellValue("$column$pe04_start", $this->cellVacuum($cell_value));
            }

        }

    }

    protected function cellVacuum($sum_pe04, $pe81_organika = false)
    {
            $value = abs($sum_pe04);
            $sign = $sum_pe04 < 0 ? -1 : 1;
            $plus = 0;

            $division = intdiv($value, $this->divisor);
            $mod = $value % $this->divisor;
            
            Log::warning("SUM 333 : $sum_pe04");

            if($mod == 0){
                if($division != 0){
                    return $sign * $division;
                }else{
                    return '';
                }
            }else{
                if($pe81_organika){
                    if($sign == 1){
                        if($mod >= $this->limit_organika){
                            $plus++;
                        }
                        Log::warning("Limit Organika $plus");
                    }else{
                        if($mod >= $this->limit){
                            $plus++;
                        }
                        Log::warning("Limit 81 $plus");
                    }
                }else{
                    // if($mod >= $this->limit){
                    //     $plus++;
                    // }
                    if($sign == 1){
                        if($mod >= $this->limit_organika){
                            $plus++;
                        }
                        Log::warning("Limit NOT 81 $plus");
                    }else{
                        if($mod >= $this->limit){
                            $plus++;
                        }
                        Log::warning("Limit NOT 81 $plus");

                    }
                }

                $result = $sign * ($division + $plus);

                return $result != 0 ? $result : '';
            }
        
    }

    public function cellLeitourgika($value)
    {
        if($value != 0){
            return $value;
        }
        
        return '';        
    }

    protected function getAbsenceCellStyles()
    {
        return  [
            'font' => [
                'bold' => true,
                'name' => 'Arial',
                'size'  => 18
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'startColor' => [
                    'argb' => 'FF0000',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
    }

}