<?php

namespace Pasifai\Pysde\controllers\traits;

use App\MySchoolTeacher;
use App\School;
use App\User;
use Artisan;
use Auth;
use Carbon\Carbon;
use Illuminate\Queue\Events\Looping;
use Illuminate\Support\Facades\Log;
use Notification;
use Pasifai\Pysde\models\SpecialPlacement;
use Pasifai\Pysde\notifications\LeitourgikaPysdeNotification;
use Pasifai\Pysde\notifications\PysdeNotification;

trait LeitourgikaTrait
{
    private $headersCounter = 0;
    private $teachers = [];
    private $newTeacher = 0;
    private $updatedTeacher = 0;
    private $untouchedTeacher = 0;
    // this is for commit only

    private $apousiaType = [
        'ΑΠΟΣΠΑΣΗ ΣΕ ΑΛΛΟ ΠΥΣΠΕ / ΠΥΣΔΕ' => 2,
        'ΘΕΣΗ ΕΚΠΑΙΔΕΥΣΗΣ ΕΠΙ ΘΗΤΕΙΑ'    => 9,
        'ΑΠΟΣΠΑΣΗ ΣΕ ΦΟΡΕΑ ΥΠ. ΠΑΙΔΕΙΑΣ' => 1,
        'ΑΠΟΣΠΑΣΗ ΣΕ ΦΟΡΕΑ ΕΚΤΟΣ ΥΠ. ΠΑΙΔΕΙΑΣ' => 1,
        'ΑΠΕΥΘΕΙΑΣ ΑΠΟΣΠΑΣΗ ΣΕ ΣΧΟΛΙΚΗ ΜΟΝΑΔΑ'  => 3,
        'ΟΛΙΚΗ ΔΙΑΘΕΣΗ ΣΕ ΑΠΟΚΕΝΤΡΩΜΕΝΕΣ ΥΠΗΡΕΣΙΕΣ ΥΠ. ΠΑΙΔΕΙΑΣ' => 1,
        'ΟΛΙΚΗ ΔΙΑΘΕΣΗ ΣΕ ΑΛΛΗ ΣΧΟΛΙΚΗ ΜΟΝΑΔΑ' => 3,
        'ΑΠΟΣΠΑΣΗ ΣΤΟ ΕΞΩΤΕΡΙΚΟ' => 1,
        'ΜΑΚΡΟΧΡΟΝΙΑ ΑΔΕΙΑ (>10 ημέρες)' => 8,
    ];

    private $meiwseis = [
        "dieyoynths-proistamenos"   => 0,
        "ypodieyoynths"             => 0,
        "ypeyoynos-tomea-ek-epal"   => 0,
        "ypeyoynos-ek-gia-spoydastes-diek" => 0,
        "ypeyoynos-ergasthrioy-plhn-sepehy-sefe" => 0,
        "ypeyoynos-sepehy" => 0,
        "ypeyoynos-sefe" => 0,
        "goneas-anhlikoy-2-etwn" => 0,
        "airetos-ekpaideyshs" => 0,
        "syndikalisths" => 0,
        "airetos-ota" => 0,
        "epopteia-maohteias-ston-ergasiako-xwro" => 0,
        // "wres-ypox-didaktikoy-wrarioy-yphrethshs-sto-forea" => 0

    ];

    protected function notifyPysde($apousies)
    {        
        $user_pysde = User::where('email', config('requests.MAIL_PYSDE'))->first();

        if($user_pysde != null){
            $user = Auth::user(); 

            $name = $user->userable->name;

            $ul = "";
            foreach($apousies as $k=>$apousia){
                $type_apousia = config('requests.topothetisis_types')[$apousia['teacher_type']];
                Log::warning("444444");
                Log::warning("Type apousia : $type_apousia");
                Log::warning("{$apousia['myschool']['last_name']}");
                Log::warning("{$apousia['myschool']['first_name']}");
                Log::warning("{$apousia['date_ends']}");
                Log::warning("{$apousia['hours']}");
                Log::warning("<li>{$apousia['myschool']['last_name']} {$apousia['myschool']['first_name']} | $type_apousia | {$apousia['date_ends']} | {$apousia['hours']}</li>");


                Log::warning($ul);

                $ul .= "<li>{$apousia['myschool']['last_name']} {$apousia['myschool']['first_name']} | $type_apousia | {$apousia['date_ends']} | {$apousia['hours']}</li>";
            
                Log::warning($ul);
            }

            $description = "<ul>$ul</ul>";

            Notification::send($user_pysde, new LeitourgikaPysdeNotification($user,[
                'name'          => $name,
                'title'         => "ΑΠΟΥΣΙΕΣ εκπαιδευτικών",
                'description'   => $description,
                'type'          => 'primary',
                'url'           => route('Leitourgika::pysdeSecretary::manageSchools', str_replace('-', ' ', $name)),
                'subject'       => "[Λειτουργικά]-[$name] ## ΑΠΟΥΣΙΕΣ ΕΚΠΑΙΔΕΥΤΙΚΩΝ",
                'apousies'      => $apousies,
                ], 'pysde::email.school-leitourgika-teachers'));

        }
    }

    public function executeUpdateMeiwsi($path)
    {
        $teachers = array();
        $columns_names = array();

        $count_restored = 0;

        if ( ($handle = fopen($path,'r')) !== FALSE) {
            $columns_names = $this->getHeaders($handle, $columns_names);
            
            while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER

                $this->getTeacher($data, $columns_names);
            }

            // \DB::beginTransaction();

                foreach($this->teachers as $key=>$teacher){
                    $sum = 0;

                    // dd($teacher);
                    
                    foreach($this->meiwseis as $index=>$value){
                        $new_value = intval($teacher[$index]);

                        $this->meiwseis[$index] = $new_value;                        
                    }

                    foreach($this->meiwseis as $index=>$value){
                        $sum = $sum + $value;
                    }

                    $modelTeacher = MySchoolTeacher::withTrashed()->where('afm', $teacher['afm'])
                        ->first();

                    if($modelTeacher != null){
                        $modelTeacher->meiwsi = $sum;
                        $modelTeacher->save();

                        $school = School::where('id', $modelTeacher->organiki_id)->first();

                        if($school != null){
                            $modelTeacher->topothetisis()->updateExistingPivot($school->id , [
                                'hours'         => $modelTeacher->ypoxreotiko - $modelTeacher->meiwsi,
                            ]);
                        }

                    }
                }
                Artisan::call('leitourgika:update');

            // \DB::commit();

            flash()->overlayS('', "Ενημερώθηκαν: $this->updatedTeacher - Δημιουργήθηκαν: $this->newTeacher- Καμία ενέργεια σε: $this->untouchedTeacher εκπαιδευτικούς");

        }else{
            flash()->error('Error');

            \Log::alert('Error Opening File');
        }
    }

    public function executeUpdateLeitourgikaDieuthides($path){
        $teachers = array();
        $columns_names = array();

        $count_restored = 0;

        if ( ($handle = fopen($path,'r')) !== FALSE) {
            $columns_names = $this->getHeaders($handle, $columns_names);

            while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER

                $this->getTeacher($data, $columns_names);
            }

            // \DB::beginTransaction();

            // dd($this->teachers);

                foreach($this->teachers as $key=>$teacher){
                    //kwd-ypepo-sxoleioy
                    
                    $next_year = Carbon::now()->year + 1;

                    $school_identifier = intval(preg_replace('/[^0-9]/','', $teacher['kwd-ypepo-sxoleioy']));
                    
                    $school = School::where('identifier', $school_identifier)->first();

                    $modelTeacher = MySchoolTeacher::withTrashed()->where('afm', $teacher['afm'])
                        ->first();

           
                    if($modelTeacher != null){
                        if(!$modelTeacher->trashed()){
                        
                            // if($teacher['am'] == '168566'){
                            //     dd($school);
                            // }

                            if($school_identifier == $modelTeacher->organiki_prosorini){
                                if($school != null){
                                    $modelTeacher->topothetisis()->updateExistingPivot($school->id , [
                                        'teacher_type'  => 9,
                                        'hours'         => 0,
                                        'date_ends'     => "31/08/$next_year",
                                    ]);
                                }
                            }else{                        
                                if($school != null){
                                    $modelTeacher->topothetisis()->detach($school->id);
                                    $modelTeacher->topothetisis()->attach([$school->id => [
                                        'teacher_type'  => 10,
                                        'date_ends'     => "31/08/$next_year",
                                        'hours'         => $modelTeacher->ypoxreotiko - $modelTeacher->meiwsi,
                                        'project_hours' => 0,
                                        'locked'        => true,
                                        'url'           => ''
                                    ]]);

                                    // if($teacher['am'] == '168566'){
                                    //     dd($modelTeacher->organiki_id);
                                    // }
                                    
                                    if($modelTeacher->organiki_id < 100){
                                        $modelTeacher->topothetisis()->updateExistingPivot($modelTeacher->organiki_id , [
                                            'teacher_type'  => 9,
                                            'locked'        => true,
                                            'project_hours' => 0,
                                            'url'           => '',
                                            'hours'         => 0,
                                            'date_ends'     => "31/08/$next_year",
                                        ]);
                                    }   
                                
                                }
                            }
                        }
                    }
                }

                Artisan::call('leitourgika:update');

            // \DB::commit();

            flash()->overlayS('', "Ενημερώθηκαν: $this->updatedTeacher - Δημιουργήθηκαν: $this->newTeacher- Καμία ενέργεια σε: $this->untouchedTeacher εκπαιδευτικούς");

        }else{
            flash()->error('Error');

            \Log::alert('Error Opening File');
        }
    }

    public function executeUpdateLeitourgikaApousies($path)
    {        
        $teachers = array();
        $columns_names = array();

        $count_restored = 0;

        if ( ($handle = fopen($path,'r')) !== FALSE) {
            $columns_names = $this->getHeaders($handle, $columns_names);


            while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER

                $this->getTeacher($data, $columns_names);
            }

            // \DB::beginTransaction();

                foreach($this->teachers as $key=>$teacher){
                    if(!array_key_exists($teacher['aitiologhsh-apoysias'], $this->apousiaType)){
                        Log::warning("{$teacher['aitiologhsh-apoysias']} - Apousia {$teacher['epwnymo']} - ΑΜ {$teacher['am']} - {$teacher['exeid-aitiologhshs']}");
                    }

                    $modelTeacher = MySchoolTeacher::withTrashed()->where('afm', $teacher['afm'])
                        ->first();

                    if($modelTeacher != null){
                        if(!$modelTeacher->trashed()){
                            $type = 1;

                            
                            if($modelTeacher->organiki_id < 1000){
                                // Με οργανική
                                if(array_key_exists($teacher['aitiologhsh-apoysias'], $this->apousiaType)){
                                    $type = $this->apousiaType[$teacher['aitiologhsh-apoysias']];

                                    $modelTeacher->topothetisis()->sync([$modelTeacher->organiki_id => [
                                        'teacher_type'  => $type,
                                        'date_ends'     => $teacher['ews'],
                                        'hours'         => $modelTeacher->ypoxreotiko,
                                        'project_hours' => 0,
                                        'locked'        => true,
                                        'url'           => ''
                                    ]]);
                                }else{
                                    // Log::error("{$teacher['aitiologhsh-apoysias']}");
                                    // dd($teacher);
                                }

                                // Artisan::call('leitourgika:update', [
                                //     'to' => $modelTeacher->organiki_id,
                                // ]);

                            }else{

                                if(array_key_exists($teacher['aitiologhsh-apoysias'], $this->apousiaType)){
                                    $type = $this->apousiaType[$teacher['aitiologhsh-apoysias']];

                                    $modelTeacher->special_topothetisis()->sync([10001 => [
                                        'teacher_type'  => $type,
                                        'date_ends'     => $teacher['ews'],
                                        'hours'         => $modelTeacher->ypoxreotiko,
                                        'locked'        => true,
                                        'url'           => ''
                                    ]]);

                                }else{
                                    // Log::error("R{$teacher['aitiologhsh-apoysias']}");
                                }

                                // Διάθεση ή κάτι άλλο
                            }

                            // $this->updateOrNothing($modelTeacher, $teacher);
                        }

                        // $this->updateUserProfile($modelTeacher);
                        //update User Profile function
                    }
                }



            // \DB::commit();

            flash()->overlayS('', "Ενημερώθηκαν: $this->updatedTeacher - Δημιουργήθηκαν: $this->newTeacher- Καμία ενέργεια σε: $this->untouchedTeacher εκπαιδευτικούς");

        }else{
            flash()->error('Error');

            \Log::alert('Error Opening File');
        }

    }

    private function convert($text)
    {
        return iconv("greek", "UTF-8", $text);
    }
    
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($this->convert($column));
        }
        return $columns_names;
    }

    private function getTeacher($data, $columns_names)
    {
        $record = array();
        $i = 0;

        foreach ($columns_names as $key) {
            if($key == "afm"){
                $record[$key] = preg_replace('/[^0-9]/','',$this->convert($data[$i++]));
            }else{
                $record[$key] = $this->convert($data[$i++]);
            }
        }
        $this->teachers[] = $record;
    }
}