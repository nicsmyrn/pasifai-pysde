<?php

namespace Pasifai\Pysde\controllers\traits;

use App\MySchoolTeacher;
use Pasifai\Pysde\models\Placement;
use App\School;
use App\Models\Eidikotita;
use App\Models\Year;
use Pasifai\Pysde\models\Praxi;
use Carbon\Carbon;
use Log;

trait PlacementsTrait
{
    protected $fetchedTeachers;

    public function allPlacementsPDF($teacher_id = null, $year_list, $beta = false)
    {
        $myschool = MySchoolTeacher::find($teacher_id);
        
        $pr = Praxi::with(['placements' => function($query) use ($myschool){
            $query->where('afm', $myschool->afm)
                ->where('pending', 0)
                ->withTrashed();
        }])->whereIn('praxi_type', [11,12])
            ->whereIn('year_id', $year_list)
            ->get();
            
        $temp = array();
                
        foreach($pr as $praxi){
            if (!$praxi->placements->isEmpty()){
                $last_BASE_placement = Placement::with(['praxi' => function($q) use ($praxi, $year_list){
                    $q->where('id', '<=', $praxi->id)
                        ->whereIn('year_id', $year_list);   // *******<<<<<<<<<<<<<<  $praxi->decision_number
                }])
                    ->whereIn('placements_type', [1,6])     //ΠΡΟΣΩΡΙΝΕΣ ΤΟΠΟΘΕΤΗΣΕΙΣ
                    ->where('afm', $myschool->afm)
                    ->withTrashed()
                    ->orderBy('created_at','desc')          // ΑΥΤΟ ΕΔΩ ΠΡΕΠΕΙ ΝΑ ΤΟ ΞΑΝΑΔΩ ....<<<<<<<<<<<<<<<<<<<<<<<<
                    ->get();

                $new_placements = Placement::with(['praxi' => function($q) use ($praxi, $year_list){
                    $q->where('decision_number', $praxi->decision_number)->whereIn('year_id', $year_list);                      // ****************CHANGES from id to decision_numbers green words
                }])
                    ->whereNotIn('placements_type', [1,6])
                    ->where('afm', $myschool->afm)
                    ->withTrashed()
                    ->get();

                $temp0 = collect();
                $temp_new_placements = array();

                foreach($last_BASE_placement as $base){
                    if($base->praxi != null){
                        $temp0->push($base);
                    }
                }


                    $max = $temp0->max('praxi_id');
                    $praxi->last_BASE_placement = $temp0->where('praxi_id', $max)->first();


                foreach($new_placements as $new_placement){
                    if($new_placement->praxi != null){
                        if($praxi->last_BASE_placement != null){
                            if($new_placement->id != $praxi->last_BASE_placement->id){
                                $temp_new_placements[] = $new_placement;
                            }
                        }else{
                            $temp_new_placements[] = $new_placement;
                        }
                    }
                }
                $praxi->new_placements = collect($temp_new_placements);


                $old_placements = Placement::with(['praxi' => function($q) use($praxi, $year_list){
                    $q->whereIn('year_id', $year_list)->where('decision_date', '<', Carbon::parse($praxi->decision_date)->format('Y-m-d'));
                }])->where('afm', $myschool->afm)
                    ->whereNotIn('placements_type', [1,6]) //ΠΡΟΣΩΡΙΝΕΣ ΤΟΠΟΘΕΤΗΣΕΙΣ
                    ->get();
                $temp2 = array();
                foreach($old_placements as $old){
                    if ($old->praxi != null){
                        $temp2[] = $old;
                    }
                }
                $praxi->old_placements = collect($temp2);
                if($praxi->praxi_type == 11){
                    $temp[] = $praxi;
                }

                if($praxi->last_BASE_placement != null){
                    $hours_BASE_placement = $praxi->last_BASE_placement->hours;
                    $days_BASE_placement = $praxi->last_BASE_placement->days;
                }else{
                    $hours_BASE_placement = 0;
                    $days_BASE_placement = 0;
                }

                $praxi->sum_hours_of_placements = $praxi->old_placements->sum('hours') + $praxi->new_placements->sum('hours') + $hours_BASE_placement;
                $praxi->sum_days_of_placements = 5 - ($praxi->old_placements->sum('days') + $praxi->new_placements->sum('days') + $days_BASE_placement);


                $praxi->calculated_ypoxreotiko = $this->calculateYpoxretorikoOnDate($myschool, $praxi->decision_date);
            }
        }

        $praxeis = collect($temp);

        foreach($praxeis as $praxi){
            $this->aitisiTypeF($praxi);
            $this->oldAitisiTypeF($praxi);
            $this->createTitle($praxi, $myschool);
            $this->baseAitisiTypeF($praxi);
            $this->calculateYpoxreotiko($praxi, $myschool);
        }

        $ex_oloklirou = false;

        // return compact('praxeis', 'myschool', 'ex_oloklirou', 'beta');
                
        $pdf = \PDF::loadView('pysde::leitourgika.placements.teacherPlacements.PDF.all_teacher_topothetiria',compact('praxeis', 'myschool', 'ex_oloklirou', 'beta'));

        return $pdf->setPaper('A4', 'portrait')->stream();
    }

    public function allPlacementsPDFbyPraxi($praxi, $beta = false)
    {        
        $teachers = MySchoolTeacher::with(['placements'=> function($query) use($praxi){
            $query->where('pending', 0)
                ->where('from', '<>', 'ΑΝΑΠΛΗΡΩΤΗΣ')
                ->withTrashed()->where('praxi_id', $praxi->id);
        }])
            ->orderBy('last_name')
            ->get()
            ->filter(function($p){
                if(count($p->placements) != 0) return true;
            })
            ->values();     

        $pages = collect();

        foreach($teachers as $teacher){
            if (!$teacher->placements->isEmpty()){
                $page = null;
                $page = Praxi::find($praxi->id);

                $page->teacher = $teacher;
                $page->placements = $teacher->placements;

                $last_BASE_placement = $teacher->placements->filter(function($p) use ($praxi){
                    if(in_array($p->placements_type, [1,6])) return true;
                })->sortByDesc('praxi_id')->first();

                $new_placements = $teacher->placements->filter(function($p) use ($praxi){
                    if(!in_array($p->placements_type, [1,6])) return true;
                })->sortByDesc('praxi_id')->values();
                $page->last_BASE_placement = $last_BASE_placement;

/*
                $last_BASE_placement = Placement::with(['praxi' => function($q) use ($praxi, $year_list){
                    $q->where('id', '<=', $praxi->id)
                        ->whereIn('year_id', $year_list);   // *******<<<<<<<<<<<<<<  $praxi->decision_number
                }])
                    ->whereIn('placements_type', [1,6])     //ΠΡΟΣΩΡΙΝΕΣ ΤΟΠΟΘΕΤΗΣΕΙΣ
                    ->where('afm', $myschool->afm)
                    ->withTrashed()
                    ->orderBy('created_at','desc')          // ΑΥΤΟ ΕΔΩ ΠΡΕΠΕΙ ΝΑ ΤΟ ΞΑΝΑΔΩ ....<<<<<<<<<<<<<<<<<<<<<<<<
                    ->get();

                $new_placements = Placement::with(['praxi' => function($q) use ($praxi, $year_list){
                    $q->where('decision_number', $praxi->decision_number)->whereIn('year_id', $year_list);                      // ****************CHANGES from id to decision_numbers green words
                }])
                    ->whereNotIn('placements_type', [1,6])
                    ->where('afm', $myschool->afm)
                    ->withTrashed()
                    ->get();
*/


                $page->new_placements = $new_placements;

                $this->aitisiTypeF($page);
                $this->createTitle_for_leter($page, $page->teacher);
                $this->baseAitisiTypeF_for_letter($page);

                $pages->push($page);
            }
        }

        $ex_oloklirou = false;

        set_time_limit(300); // Extends to 5 minutes.

        $pdf = \PDF::loadView('pysde::leitourgika.placements.teacherPlacements.PDF.all_topothetiria_by_letter',compact('pages', 'ex_oloklirou', 'beta'));

        return $pdf->setPaper('A4', 'portrait')->stream();

        // $pdf = \PDF::loadView('teacher.placements.all_topothetiria_by_letter',compact('pages', 'ex_oloklirou'));
        // return $pdf->setOrientation('portrait')->stream();
    }

    public function getIndexPlacementsOfTeacher($afm = null)
    {        
        $myschool = MySchoolTeacher::findOrFail($afm);

        $placements = Placement::withTrashed()
            ->with('praxi')
            ->where('afm', $myschool->afm)
            ->get();

        $old_placements = $placements->where('praxi.current_year', 0);
        $current_placements = $placements->where('praxi.current_year', 1);

        $total = Placement::with(['praxi'=> function($q){
            $q->where('year_id', Year::where('current', true)->first()->id);
        }])->where('afm', $myschool->afm)
            ->get()
            ->filter(function($p){
                if($p->praxi != null && !$p->pending && !in_array($p->placements_type,[9])) return true;    // 9 : ανάκληση
            })
            ->sum('hours');

        $base_placement = Placement::whereIn('placements_type', [1,6])     //ΠΡΟΣΩΡΙΝΕΣ ΤΟΠΟΘΕΤΗΣΕΙΣ
        ->where('afm', $myschool->afm)
            ->orderBy('created_at','desc')          // ΑΥΤΟ ΕΔΩ ΠΡΕΠΕΙ ΝΑ ΤΟ ΞΑΝΑΔΩ ....<<<<<<<<<<<<<<<<<<<<<<<<
            ->first();
//
        $schools = School::pluck('name', 'id');
        $kladoi = Eidikotita::pluck('name', 'id');

        $current_year = Year::where('current', true)->first()->name;

        $start_ipiresia = $myschool->start_ipiresia;
        $proip_years    = $myschool->proip_years;
        $proip_months   = $myschool->proip_months;
        $proip_days     = $myschool->proip_days;

        $wrario_collection = $this->calculateHistoryProipiresia($start_ipiresia, $proip_years, $proip_months, $proip_days, $myschool['ypoxreotiko']);

        $sorted_wrario = $wrario_collection->sortByDesc('order')->values();

        // return compact(
        //     'old_placements', 
        //     'current_placements', 
        //     'total', 
        //     'base_placement', 
        //     'myschool', 
        //     'schools',
        //     'kladoi', 
        //     'current_year', 
        //     'sorted_wrario');
        
        return view('pysde::leitourgika.placements.teacherPlacements.index', compact(
            'old_placements', 
            'current_placements', 
            'total', 
            'base_placement', 
            'myschool', 
            'schools',
            'kladoi', 
            'current_year', 
            'sorted_wrario'));
    }
    
    protected function calculateYpoxretorikoOnDate($myschool, $date)
    {
        $sum_days_proipiresia = ($myschool['proip_years'] * 365) + ($myschool['proip_months'] * 30) + $myschool['proip_days'];

        $current_ypoxrewtiko = 0;   

        $temp_date = Carbon::createFromFormat('d-m-Y', $date);
        $changed_date = Carbon::createFromFormat('Y-m-d', config('requests.old_wrario_date'));

        $sum_days_ipiresia = $temp_date->diffInDays($myschool['start_ipiresia']);

        $sumDays = $sum_days_proipiresia + $sum_days_ipiresia;
        
        foreach(config('requests.wrario') as $days=>$wrario){
    
            if($sumDays >= $days){
                $current_ypoxrewtiko = $wrario;
                
                if($temp_date->lessThan($changed_date)){
                    $current_ypoxrewtiko = $current_ypoxrewtiko - 2;
                }                    
                break;
            }
        }; // end for each

        return $current_ypoxrewtiko;

    }

    protected function calculateHistoryProipiresia($start, $years, $months, $days, $ypoxreotiko){
        $sum_days_proipiresia = ($years * 365) + ($months * 30) + $days;

        $current_ypoxrewtiko = 0;
        $diff = 0;

        $col = collect();

        $now = Carbon::now();
        
        $temp_date = $start;
        // $temp_date = Carbon::createFromFormat('Y-m-d', $start);
        $changed_date = Carbon::createFromFormat('Y-m-d', config('requests.old_wrario_date'));
                
        $count = 1;

        if($temp_date != null){
            while($temp_date->lessThan($now)){
                $previous_days = 0;
                $previous_hours = 0;
    
                foreach(config('requests.wrario') as $days=>$wrario){
    
                    if($temp_date->format('d/m/Y') == '25/10/2011'){
                        // dd($sum_days_proipiresia);
                        
                        // dd('in');
                    }

                    if($sum_days_proipiresia >= $days){
                        $current_ypoxrewtiko = $wrario;
                        if($temp_date->lessThan($changed_date)){
                            $current_ypoxrewtiko = $current_ypoxrewtiko - 2;
                        }                    
    
                        break;
                    }
    
                    $previous_days = $days;   
                    $previous_hours = $wrario;
    
                    $diff = $previous_days - $sum_days_proipiresia;   
                }; // end for each
    
                
                $timeline = $ypoxreotiko == $current_ypoxrewtiko ? 'current' : 'old';
    

                $col->push([
                    'start_date'    => $temp_date->format('d/m/Y'),
                    'hours'         => $current_ypoxrewtiko,
                    'timeline'      => $timeline,
                    'order'         => $count
                ]);
                $count++;
    
                if($current_ypoxrewtiko == config('requests.max_wrario')){
                    break;
                }
    
                $temp_date = $temp_date->addDays($diff);
                $sum_days_proipiresia = $sum_days_proipiresia + $diff;
            }
                    // if($previous_days != 0){
            if($current_ypoxrewtiko != 18){
                $col->push([
                    'start_date'    => $temp_date->format('d/m/Y'),
                    'hours'         => $previous_hours,
                    'timeline'      => 'future',
                    'order'         => $count
                ]);
            }
        } // end of if 

        return $col;

    }

    protected function createFetchedRecord($teacher){
        $am = $teacher->am;
        $edata = $teacher->edata;

        if($teacher->organiki_id < 999){
            $myschool = MySchoolTeacher::with('topothetisis')->where('afm', $teacher->afm)->first();

            if($myschool != null){
                $topothetisi_organikis = $myschool->topothetisis->where('pivot.teacher_type', 0)->first();
                if($topothetisi_organikis != null){
                    $wrario_organikis = $topothetisi_organikis->pivot->hours + $topothetisi_organikis->pivot->project_hours;
                }else{
                    $wrario_organikis = 0;
                }
            }else{
                $wrario_organikis = 0;
            }
        }else{
            $wrario_organikis = 0;
        }

        $topothetisis = collect();

        foreach($teacher->placements as $placement){
            $topothetisis->push([
                'id'                => $placement->id,
                'afm'               => $placement->afm,
                'praxi_id'          => $placement->praxi_id,
                'hours'             => $placement->hours,
                'from'              => $placement->from,
                'to'                => $placement->to,
                'description'       => $placement->description,
                'from_id'           => $placement->from_id,
                'to_id'             => $placement->to_id,
                'placements_type'   => $placement->placements_type,
                'days'              => $placement->days,
                'me_aitisi'         => $placement->me_aitisi,
                'starts_at'         => $placement->starts_at,
                'ends_at'           => $placement->ends_at,
                'pending'           => $placement->pending,
                'deleted_at'        => $placement->deleted_at,
                'praxi'             => $placement->praxi,
                'url'               => route('Leitourgika::Placements::update', [$placement->afm, $placement->id])
            ]);
        }

        $this->fetchedTeachers->push([
            'sxesi'         => $am != '' ? 'ΜΟΝΙΜΟΣ' : 'ΑΝΑΠΛΗΡΩΤΗΣ',
            'am'            => $am,
            'full_name'     => $teacher->last_name . ' ' . $teacher->first_name,
            'middle_name'   => $teacher->middle_name,
            'moria'         => $edata != null ? $edata->sum_moria : '-',
            'organiki'      => $teacher->organiki_name,
            'organiki_id'   => $teacher->organiki_id,
            'klados'        => $teacher->new_klados . ' ('. $teacher->new_eidikotita_name .')',
            'klados_id'     => $teacher->new_eidikotita,
            'entopiotita'   => $edata != null ? \Config::get('requests.dimos')[$edata->dimos_entopiotitas]: 'Χωρίς στοιχεία',
            'sinipiretisi'  => $edata != null ? \Config::get('requests.dimos')[$edata->dimos_sinipiretisis]: 'Χωρίς στοιχεία',
            'special'       => $edata != null ? $edata->special_situation ? 'NAI' :'OXI' : 'Χωρίς στοιχεία',
            'ypoxreotiko'   => $teacher->ypoxreotiko,
            'topothetisis'  => $topothetisis,
            'afm'           => $teacher->afm,
            'placements'    => collect(),
            'totalTopothetisis' => $teacher->placements->filter(function($v, $k){
                return $v->placements_type != 9; // όχι την ανάκληση
            })->sum('hours'),
            'orario_organikis'  => $wrario_organikis,
            'url'               => route('Leitourgika::Placements::placementsDetails', $teacher->afm)
        ]);
    }



    //###########################

    private  function baseAitisiTypeF($praxi)
    {
        if ($praxi->last_BASE_placement != null){
            if($praxi->last_BASE_placement->me_aitisi){
                $aitisis = collect([
                    'value' => 'x-true',
                    'text'  => 'με αίτησή'
                ]);
            }else{
                $aitisis = collect([
                    'value' => 'x-false',
                    'text' => 'χωρίς αίτησή'
                ]);
            }
            return $praxi->base_aitisis = $aitisis;
        }
        return $praxi->base_aitisis = null;
    }

    private  function baseAitisiTypeF_for_letter($praxi)
    {

        if($praxi->last_BASE_placement != null){
            if($praxi->last_BASE_placement['me_aitisi'] == 1){
                $aitisis = collect([
                    'value' => 'x-true',
                    'text'  => 'με αίτησή'
                ]);
            }else{
                $aitisis = collect([
                    'value' => 'x-false',
                    'text' => 'χωρίς αίτησή'
                ]);
            }
            return $praxi->base_aitisis = $aitisis;
        }        
        return $praxi->base_aitisis = null;
    }

    private function oldAitisiTypeF($praxi)
    {
        if (!$praxi->old_placements->isEmpty()){
            if (!$praxi->old_placements->contains('me_aitisi', false)) {
                $aitisis = collect([
                    'value' => 'x-true',
                    'text' => 'με αίτησή'
                ]);
            } elseif (!$praxi->old_placements->contains('me_aitisi', true)) {
                $aitisis = collect([
                    'value' => 'x-false',
                    'text' => 'χωρίς αίτησή'
                ]);
            } else {
                $aitisis = collect([
                    'value' => 'true&false',
                    'text' => ''
                ]);
            }

            $praxi->old_aitisis =  $aitisis;
        }else{
            $praxi->old_aitisis = null;
        }
    }

    private function calculateYpoxreotiko($praxi, $myschool){
        if($myschool['start_ipiresia'] == null){
            $praxi->ypoxreotiko = $myschool['ypoxreotiko'];
        }else{
            $sum_days_proipiresia = ($myschool['proip_years'] * 365) + ($myschool['proip_months'] * 30) + $myschool['proip_days'];
            $current_date = Carbon::createFromFormat('d-m-Y', $praxi['decision_date']);
            $changed_date = Carbon::createFromFormat('Y-m-d', config('requests.old_wrario_date'));
    
            $sum_days_ipiresia = $current_date->diffInDays($myschool['start_ipiresia']);
    
            $sum = $sum_days_proipiresia + $sum_days_ipiresia;
    
            $current_ypoxrewtiko = 0;
          
            foreach(config('requests.wrario') as $days=>$wrario){
                if($sum >= $days){
                    $current_ypoxrewtiko = $wrario;
                    if($current_date->lessThan($changed_date)){
                        $current_ypoxrewtiko = $current_ypoxrewtiko - 2;
                    }                    
                    break;
                } 
            }; // end for each
    
            $praxi->ypoxreotiko = $current_ypoxrewtiko;
        }
    }

    private function aitisiTypeF($praxi)
    {
        if (!$praxi->placements->contains('me_aitisi', false)) {
            $aitisis = collect([
                'value' => 'x-true',
                'text' => 'με αίτησή'
            ]);
        } elseif (!$praxi->placements->contains('me_aitisi', true)) {
            $aitisis = collect([
                'value' => 'x-false',
                'text' => 'χωρίς αίτησή'
            ]);
        } else {
            $aitisis = collect([
                'value' => 'true&false',
                'text' => ''
            ]);
        }

        $praxi->aitisis =  $aitisis;
    }

    private function createTitle($praxi, $teacher)
    {
//        if($teacher->teacherable->organiki == 2000){
//            $teacher_title = ' αποσπασμένου εκπαιδευτικού από άλλο ΠΥΣΔΕ ';
//        }else{
//            $teacher_title = ' εκπαιδευτικού ';
//        }

        if($teacher->dieuthinsi == \Config::get('requests.my_dieuthinsi')){
            $teacher_title = ' εκπαιδευτικού ';
        }else{
            $teacher_title = ' αποσπασμένου εκπαιδευτικού από άλλο ΠΥΣΔΕ ';
        }

        if(!$praxi->placements->where('placements_type', 2)->isEmpty()){
            $placement_title = 'Aπόσπαση';
        }elseif(!$praxi->placements->where('placements_type', 7)->isEmpty()) {
            $placement_title = 'Tροποποίηση απόσπαση';
        }elseif(!$praxi->placements->where('placements_type', 9)->isEmpty()){
            $placement_title = 'Ανάκληση τοποθέτησης';
        }else{
            $placement_title = '';
        }

        if (!$praxi->placements->where('placements_type', 4)->isEmpty()) {
            $pvthmia = 'ολικής διάθεσης';
            $pvthmia2 = 'στην Π/θμια Εκπαίδευση';
        }elseif(!$praxi->placements->where('placements_type', 8)->isEmpty()){
            $pvthmia = 'διάθεσης';
            $pvthmia2 = 'στην Π/θμια Εκπαίδευση';

        }else{
            $pvthmia = '';
            $pvthmia2 = '';
        }
        if($praxi->last_BASE_placement != null){
            if($praxi->last_BASE_placement->placements_type == 6 ){
                $base_placement_tile = 'Τροποποίηση προσωρινής τοποθέτησης ';
            }elseif($praxi->last_BASE_placement->placements_type == 1   && $praxi->last_BASE_placement->praxi->decision_number == $praxi->decision_number){
                $base_placement_tile = 'Προσωρινή τοποθέτηση ';
            }else{
                $base_placement_tile = '';
            }
        }else{
            $base_placement_tile = '';
        }
        if(!$praxi->placements->where('placements_type', 3)->isEmpty()){
            $new_placement_operator = $base_placement_tile == '' ? '' : ' και ';
            $new_placements_title = 'Διάθεση';
            $new_placements_title2 = 'για συμπλήρωση του υποχρεωτικού του ωραρίου';
        }elseif(!$praxi->placements->where('placements_type', 5)->isEmpty()){
            $new_placement_operator = $base_placement_tile == '' ? '' : ' και ';
            $new_placements_title = 'Τροποποίησης διάθεσης';
            $new_placements_title2 = 'για συμπλήρωση του υποχρεωτικού του ωραρίου';
        }else{
            $new_placement_operator = '';
            $new_placements_title = '';
            $new_placements_title2 = '';
        }

        $praxi->title =   $base_placement_tile .$new_placement_operator. $new_placements_title . $placement_title . $pvthmia . $teacher_title . $new_placements_title2 . $pvthmia2;

    }

    private function createTitle_for_leter($praxi, $teacher)
    {
        $praxi->placements = $praxi->placements->filter(function($p) use($praxi){
            if ($p->praxi_id == $praxi->id) return true;;
        });

        if($teacher->dieuthinsi == \Config::get('requests.my_dieuthinsi')){
            $teacher_title = ' εκπαιδευτικού ';
        }else{
            $teacher_title = ' αποσπασμένου εκπαιδευτικού από άλλο ΠΥΣΔΕ ';
        }

        if(!$praxi->placements->where('placements_type', 2)->isEmpty()){
            $placement_title = 'Aπόσπαση';
        }elseif(!$praxi->placements->where('placements_type', 7)->isEmpty()) {
            $placement_title = 'Tροποποίηση απόσπαση';
        }elseif(!$praxi->placements->where('placements_type', 9)->isEmpty()){
            $placement_title = 'Ανάκληση τοποθέτησης';
        }else{
            $placement_title = '';
        }

        if (!$praxi->placements->where('placements_type', 4)->isEmpty()) {
            $pvthmia = 'Oλική διάθεση';
            $pvthmia2 = 'στην Π/θμια Εκπαίδευση';
        }elseif(!$praxi->placements->where('placements_type', 8)->isEmpty()){
            $pvthmia = 'Διάθεση';
            $pvthmia2 = 'στην Π/θμια Εκπαίδευση';

        }else{
            $pvthmia = '';
            $pvthmia2 = '';
        }

        $base_placement_tile = '';
        $new_placements_title2 = '';

        if($praxi->last_BASE_placement != null){
            if($praxi->last_BASE_placement['placements_type'] == 6 ){
                $base_placement_tile = 'Τροποποίηση προσωρινής τοποθέτησης ';
            }elseif($praxi->last_BASE_placement['placements_type'] == 1   && $praxi->last_BASE_placement['praxi_id'] == $praxi->id){
                $base_placement_tile = 'Προσωρινή τοποθέτηση ';
            }
        }
        
        if(!$praxi->placements->where('placements_type', 3)->isEmpty()){
            $new_placement_operator = $base_placement_tile == '' ? '' : ' και ';
            $new_placements_title = 'Διάθεση';
            $new_placements_title2 = 'για συμπλήρωση του υποχρεωτικού του ωραρίου';
        }elseif(!$praxi->placements->where('placements_type', 5)->isEmpty()){
            $new_placement_operator = $base_placement_tile == '' ? '' : ' και ';
            $new_placements_title = 'Τροποποίηση διάθεσης';
            $new_placements_title2 = 'για συμπλήρωση του υποχρεωτικού του ωραρίου';
        }else{
            $new_placement_operator = '';
            $new_placements_title = '';
            $new_placements_title2 = '';
        }

        $praxi->title =   $base_placement_tile .$new_placement_operator. $new_placements_title . $placement_title . $pvthmia . $teacher_title . $new_placements_title2 . $pvthmia2;

    }
}