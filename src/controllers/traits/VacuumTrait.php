<?php

namespace Pasifai\Pysde\controllers\traits;

use Auth;
use Pasifai\Pysde\models\Division;
use App\NewEidikotita;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\Year;
use App\User;
use Artisan;
use Carbon\Carbon;
use Log;
use Notification;
use Pasifai\Pysde\notifications\PysdeNotification;
use Storage;

trait VacuumTrait
{
    protected $school;
    protected $students = [];
    protected $user;
    protected $maxStudents = 27;
    protected $maxComputer = 11;

    protected $classKeys = [
        'Α' => 0,
        'Β' => 1,
        'Γ' => 2
    ];

    protected $classes = ['Α', 'Β', 'Γ'];

    protected $statusDivs = [
        // 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'  => 'unlocked',    // κλειδώνει τα κελιά στις οργανικές της Γενικής Παιδείας. Για να γίνει πρέπει να μπει σε ΣΧΟΛΙΟ
        'ΓΑΛΛΙΚΑ'          => 'unlocked',
        'ΓΕΡΜΑΝΙΚΑ'        => 'unlocked'
    ];

    protected $schoolType = [
        'Γυμνάσιο'  => 'gym',
        'Λύκειο'    => 'lyk',
        'ΕΠΑΛ'      => 'epal'
    ];

    protected function runAlgorithmAnathesewn()
    {
        Artisan::call('kena:pinakas_a', [               // run algorithm anathesewn for school
            'type'          => $this->school->type,
            'one_school'    => 'true',
            'sch_id'        => $this->school->id
        ]);
    }

    protected function calculateSumPe04($pe04Collection)
    {
        return ($pe04Collection->sum('pivot.available') + $pe04Collection->sum('pivot.project_hours')) - $pe04Collection->sum('pivot.forseen');
    }

    public function makePDFforSchool($attr, $subfolder,  $file_path, $view)
    {
        $current_year = Year::where('current', true)->first()->name;

        $folder_path = 'schools/'.str_slug($this->school->name).'/'.$current_year. '/'. $subfolder . '/';

        \Storage::makeDirectory($folder_path);

        $pdf = PDF::loadView($view, $attr);

        $pdf->setPaper('a4', 'portrait')->save(storage_path('app/'. $folder_path . $file_path));        
    }

    public function deletePDFfile($subfolder)
    {
        $current_year = Year::where('current', true)->first()->name;

        $folder_path = 'schools/'.str_slug($this->school->name).'/'.$current_year. '/'. $subfolder . '/';

        Storage::deleteDirectory($folder_path);
    }

    public function getVacuums()
    {
        return view('pysde::school.kena_pleonasmata');
    }

    public function ajaxGetVacuumsForSchools()
    {
        $data = array();

        $school = $this->getSchool();

        $data['kena'] = $school->kena;
        $data['name'] = $school->name;

        return $data;
    }


    protected function getUser()
    {
        $this->user = Auth::user();
    }

    protected function getSchool()
    {
        return auth()->guard('web')->user()->userable;
    }

    protected function generalDivisionRealNumber($classStudents)
    {
        if(($classStudents % $this->maxStudents) == 0){
            return intdiv($classStudents, $this->maxStudents);
        }else{
            return intdiv($classStudents, $this->maxStudents) + 1;
        }
    }

    protected function getStudentsFromDivision($name, $class, $students)
    {
        if($name == 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'){
            if($class == 'Α'){
                $this->students['Α'] = $students;
            }elseif($class == 'Β'){
                $this->students['Β'] = $students;
            }elseif($class == 'Γ'){
                $this->students['Γ'] = $students;
            }
        }
    }

        /**
     * @param $divisions_general
     * @param $c
     * @param $status
     */
    private function pushToCollections($divisions, $c, $status)
    {
        $divisions->push([
            'div_id' => $c->id,
            'name' => $c->name,
            'class' => $c->class,
            'numberSchoolProvide' => $c->pivot->numberSchoolProvide,
            'oligomeles'    => $c->pivot->oligomeles,
            'pedio2' => $c->pivot->pedio2,
            'pedio3' => $c->pivot->pedio3,
            'number' => $c->pivot->number,
            'status' => $status,
            'students' => $c->pivot->students,
            'locked'    => $c->pivot->locked
        ]);
    }

    private function manualPushTollection($divisions, $c, $number, $students, $status = 'locked', $locked = true)
    {
        $divisions->push([
            'div_id' => $c->id,
            'name' => $c->name,
            'class' => $c->class,
            'numberSchoolProvide' => $number,
            'oligomeles'    => false,
            'number' => $number,
            'status' => $status,
            'students' => $students,
            'locked'    => $locked,
            'pedio2' => false,
            'pedio3' => false
        ]);
    }

    /**
     * @param $div
     * @param $attach_array
     * @return mixed
     */
    private function pushToArrayForAttach($div, $attach_array, $all_lock)
    {
        if($all_lock){
            $locked = true;
        }else{
             if(array_key_exists('locked', $div)){
                $locked = $div['locked'];
            }else{
                $locked = false;
            }
        }

        if(in_array($div['name'], ['ΑΝΘΡΩΠΙΣΤΙΚΩΝ ΣΠΟΥΔΩΝ', 'ΘΕΤΙΚΩΝ ΣΠΟΥΔΩΝ', 'ΟΙΚΟΝΟΜΙΑΣ & ΠΛΗΡΟΦ.', 'ΣΠΟΥΔΩΝ ΥΓΕΙΑΣ'])){
            $attach_array[$div['div_id']] = [
                'number' => $div['numberSchoolProvide'],
                'numberSchoolProvide' => $div['numberSchoolProvide'],
                'oligomeles'    => $div['oligomeles'],
                'pedio2'    => $div['pedio2'],
                'pedio3'    => $div['pedio3'],
                'students' => $div['students'],
                'locked'    => $locked
            ];
        }else{
            $attach_array[$div['div_id']] = [
                // 'number' => $this->generalDivisionRealNumber($div['students']),
                'number' => $div['numberSchoolProvide'],
                'numberSchoolProvide' => $div['numberSchoolProvide'],
                'oligomeles'    => $div['oligomeles'],
                'pedio2'    => $div['pedio2'],
                'pedio3'    => $div['pedio3'],
                'students' => $div['students'],
                'locked'    => $locked
            ];
        }

        return $attach_array;
    }

    private function pushToArrayProjectsForAttach($div, $attach_projects_array)
    {
//        $attach_projects_array
        $attach_projects_array[$div['eid_id']] = [
            'hours' => $div['hours']
        ];
        return $attach_projects_array;
    }

    private function pushToProjects($divisions, $c)
    {
        $divisions->push([
            'eid_id' => $c->id,
            'name' => $c->eidikotita_name,
            'slug' => $c->eidikotita_slug,
            'hours' => $c->pivot->hours
        ]);
    }


    protected function getSchoolInformation($request)
    {
        $this->school = $request->user()->userable;
        $this->organikes = $this->school->organikes;
        $this->eidikotites = NewEidikotita::all();
    }

    protected function getErgastiria()
    {
        $ergastiria = collect();

        $eidikotites = NewEidikotita::whereIn('eidikotita_slug', [
            'ΠΕ08',
            'ΠΕ11',
            'ΠΕ78',
            'ΠΕ79.01',
            'ΠΕ79.02',
            'ΠΕ80',
            'ΠΕ81',
            'ΠΕ82',
            'ΠΕ83',
            'ΠΕ84',
            'ΠΕ85',
            'ΠΕ88.01',
            'ΠΕ88.02',
            'ΠΕ88.03',
            'ΠΕ88.04',
            'ΠΕ88.05',
            'ΠΕ86',
            'ΠΕ89.01',
            'ΠΕ89.02',
            'ΠΕ91.01',
            'ΠΕ91.02'
        ])->get();

        foreach($eidikotites as $key=>$eidikotita){
            $this->pushToProjectsCollection($ergastiria, $eidikotita);
        }

        return $ergastiria;
    }

    protected function getProjects()
    {
        $projects = collect();

        foreach($this->organikes as $organiki){
            $teacher = $organiki->teacher;

            if(!$projects->contains('id', $teacher->new_eidikotita)){
                $eidikotita =  $this->eidikotites->where('id', $teacher->new_eidikotita)->first();

                if(!in_array($eidikotita->klados_slug, ['ΠΕ02', 'ΠΕ03', 'ΠΕ04'])){
                    $this->pushToProjectsCollection($projects, $eidikotita);
                }
            }
        }

        return $projects;
    }

    protected function pushToProjectsCollection($projects, $eidikotita)
    {
        $projects->push([
            'id' => $eidikotita->id,
            'slug' => $eidikotita->eidikotita_slug,
            'name' => $eidikotita->eidikotita_name
        ]);
    }

    protected function createFetchingDataGymnasium($projects)
    {
        $divisions = $this->school->divisions;
        
        $divisions_general = collect();
        $divisions_projects = collect();

        $this->createFetchingDataProject($divisions_projects);

        $list_projects = $projects->whereNotIn('id', $this->school->projects->pluck('id'))->values();
        
        $this->data['list_projects'] = $list_projects->sortBy('id')->values();

        $this->data['divisions_projects'] = $divisions_projects;

        $this->createFetchingDataOther($divisions_general);

        $this->data['list_divisions'] = Division::where('school_type', 'gym')->whereNotIn('id',$divisions->pluck('id'))->get();

        $this->data['divisions_general'] =  $divisions_general->sortBy('div_id')->values()->groupBy('name');
        $this->data['students'] = $this->students;

        // $this->data['locked'] =  !!$divisions_general[0]['locked'];

        if($divisions->isEmpty()){
            $this->data['locked'] = false;
        }else{
            if($divisions->count() == 3){
                $this->data['locked'] =  !! false;
            }else{
                $this->data['locked'] =  !! $divisions->firstWhere('pivot.locked', 0) == null; 
            }
        }

    }

    protected function createFetchingDataOtherSchool()
    {
        $divisions_general = collect();
        // $this->data['locked'] =  !!$divisions_general[0]['locked'];

        $this->createFetchingDataOther($divisions_general);

        $this->data['divisions_general'] =  $divisions_general->sortBy('class')->values();

        Log::error("9999");
        Log::warning($this->data['divisions_general']);
    }
    
    protected function createFetchingData($projects)
    {
        
        $divisions_general = collect();
        $divisions_foreign = collect();
        $divisions_direction = collect();
        $divisions_choice = collect();
        $divisions_projects = collect();
        
        $this->createFetchingDataProject($divisions_projects);

        $this->createFetchingDataOther($divisions_general, $divisions_foreign, $divisions_direction, $divisions_choice);

        $list_projects = $projects->whereNotIn('id', $this->school->projects->pluck('id'))->values();

        $school_divisions = $this->school->divisions;

        $this->data['canSentForm'] = !$school_divisions->isEmpty();

        $this->data['list_divisions'] = Division::where('school_type', 'lyk')->whereNotIn('id', $school_divisions->pluck('id'))->get();
        $this->data['list_projects'] = $list_projects->sortBy('id')->values();

        $this->data['divisions_general'] =  $divisions_general->sortBy('class')->values();
        $this->data['divisions_foreign'] =  $divisions_foreign->sortBy('div_id')->values()->groupBy('class');
        $this->data['divisions_direction'] =  $divisions_direction->sortBy('div_id')->values()->groupBy('class');
        $this->data['divisions_choice'] =  $divisions_choice->sortBy('div_id')->values()->groupBy('class');
        $this->data['students'] = $this->students;
        $this->data['divisions_projects'] = $divisions_projects;

        if($school_divisions->firstWhere('pivot.locked', 0) == null){
            if($school_divisions->count() == 3 || $school_divisions->count() == 0){    // ΜΟΝΟ ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ ή άδειες εγγραφές
                $this->data['locked'] = false;
            }else{
                $this->data['locked'] = true;
            }
        }else{
            $this->data['locked'] = false;
        }

    }

    protected function createFetchingDataProject($divisions_projects)
    {
        foreach($this->school->projects as $c){
            $this->pushToProjects($divisions_projects, $c);
        }

    }

    protected function createFetchingDataOther($divisions_general, $divisions_foreign = null, $divisions_direction = null, $divisions_choice = null)
    {
        $school_divisions = $this->school->divisions; // ΤΑ ΑΠΟΘΗΚΕΥΜΕΝΑ ΓΙΑ ΤΟ ΣΧΟΛΕΙΑ ΤΜΗΜΑΤΑ 

        $divs = Division::where('type', 'general')
            ->where('name', 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ')
            ->where('school_type', $this->schoolType[$this->school->type])->get();
        
        if($school_divisions->isEmpty()){

            foreach($divs as $div){
                $divisions_general->push([
                    'div_id' => $div->id,
                    'name' => $div->name,
                    'class' => $div->class,
                    'numberSchoolProvide' => 0,
                    'oligomeles' => false,
                    'pedio2' => false,
                    'pedio3' => false,
                    'locked'    => false,
                    'number' => 0,
                    'status' => 'locked',
                    'students' => 0
                ]);
                $this->getStudentsFromDivision($div->name, $div->class, 0);
            }            
        }else{
            // if($this->school->type == 'Γυμνάσιο'){
            //     foreach($divs as $div){
            //         $exists =  $school_divisions->firstWhere('id', $div->id);

                    
            //         if($exists != null){

            //             if(array_key_exists($exists->name, $this->statusDivs)){                            
            //                 $status = $exists->pivot->locked ? 'locked' : 'unlocked';
            //             }else{
            //                 $status = 'locked';
            //             }
                        
            //             $this->getStudentsFromDivision($exists->name, $exists->class, $exists->pivot->students);
        
            //             if($exists->type == 'general'){
            //                 $this->pushToCollections($divisions_general, $exists, $status);
            //             }elseif($exists->type == 'foreign'){
            //                 $this->pushToCollections($divisions_foreign, $exists, $status);
            //             }elseif($exists->type == 'direction'){
            //                 $this->pushToCollections($divisions_direction, $exists, $status);
            //             }elseif($exists->type == 'choice'){
            //                 $this->pushToCollections($divisions_choice, $exists, $status);
            //             }
            //         }else{
            //             $general = $school_divisions->firstWhere('class', $div->class);

            //             $students = $general->pivot->students;
            //             $general_number = $general->pivot->number;
    
            //             if($div->name == 'ΑΓΓΛΙΚΑ'){
            //                 $this->manualPushTollection($divisions_general, $div, $general->pivot->number, $general->pivot->students);
            //             }else if($div->name == 'ΓΑΛΛΙΚΑ'){
            //                 $french = $this->getFrench($general_number);
            //                 $this->manualPushTollection($divisions_general, $div, $french, $students, 'unlocked', false);
            //             }else if($div->name == 'ΓΕΡΜΑΝΙΚΑ'){
            //                 $german = $this->getGerman($general_number);
            //                 $this->manualPushTollection($divisions_general, $div, $german, $students, 'unlocked', false);
            //             }else if($div->name == 'ΠΛΗΡΟΦΟΡΙΚΗΣ' || $div->name == 'ΤΕΧΝΟΛΟΓΙΑΣ' || $div->name == 'ΟΙΚΟΝΟΜΙΑΣ'){
            //                 $computers = $this->getComputerNumber($students, $general_number);
            //                 $this->manualPushTollection($divisions_general, $div, $computers, $students);
            //             }
    
            //             $this->getStudentsFromDivision($div->name, $div->class, 0);
            //         }
            //     }
            // }else{
                foreach($school_divisions as $c){

                    if(array_key_exists($c->name, $this->statusDivs)){
                        $status = $c->pivot->locked ? 'locked' : 'unlocked';
                    }else{
                        $status = 'locked';
                    }
        
                    $this->getStudentsFromDivision($c->name, $c->class, $c->pivot->students);
        
                    if($c->type == 'general'){
                        $this->pushToCollections($divisions_general, $c, $status);
                    }
                    // elseif($c->type == 'foreign'){
                    //     $this->pushToCollections($divisions_foreign, $c, $status);
                    // }elseif($c->type == 'direction'){
                    //     $this->pushToCollections($divisions_direction, $c, $status);
                    // }elseif($c->type == 'choice'){
                    //     $this->pushToCollections($divisions_choice, $c, $status);
                    // }
                }
            // }

        }
    }

    protected function loadPdfData($request, $description = '')
    {
        $pe04Collection = $this->school->kena->where('klados_slug', 'ΠΕ04')->values();

        $sumPe04 = $this->calculateSumPe04($pe04Collection);
        
        return [
            'general_divisions' => $request->get('general_divisions'),
            'divisions'         => $request->get('divisions'),
            'foreign_languages' => $request->get('foreign_languages'),
            'division_direction' => $request->get('division_direction'),
            'division_choice' => $request->get('division_choice'),
            'divisions_projects' => $request->get('divisions_projects'),
            'now'   => Carbon::now()->format('d-m-Y H:i:s'),
            'school'    => $this->school->name,
            'type'      => $this->schoolType[$this->school->type],
            'students'  => $request->get('students'),
            'admin'     => $this->user->full_name,
            'description'   => $description,

            'kena'  => $this->school->kena,
            'kenaPe04'  => $pe04Collection,
            'sumPe04'   => $sumPe04,
            'sex'       => $this->user->sex
        ];
    }

    protected function synchronizeDivisions($request, $all_lock = false)
    {
        $this->pushGeneralDivisions($request, $all_lock);

        // $this->pushOtherDivisions($request, $all_lock);

        // $this->pushProjectDivisions($request);

        Log::warning($this->school);

        $this->school->divisions()->sync($this->attach_array);
        // $this->school->projects()->sync($this->attach_project_array);
    
    }

    protected function pushGeneralDivisions($request, $all_lock)
    {
        $general_divisions = $request->get('general_divisions');

        // $this->pushProjectDivisions($request);

        foreach($general_divisions as $div){
            $this->attach_array = $this->pushToArrayForAttach($div, $this->attach_array, $all_lock);
        }
        
    }

    protected function pushOtherDivisions($request, $all_lock)
    {
        $div_titles = ['foreign_languages', 'division_direction', 'division_choice'];
        $other_divisions = [];

        foreach($div_titles as $title){
            if($request->get($title) != null){
                $other_divisions[] = $request->get($title);
            }
        }

        if(!empty($other_divisions)){
            foreach($other_divisions as $group_div){
                foreach($group_div as $class_div){
                    if(!empty($class_div)){
                        foreach($class_div as $div){
                            $this->attach_array = $this->pushToArrayForAttach($div, $this->attach_array, $all_lock);
                        }
                    }
                }
            }
        }
    }

    protected function pushProjectDivisions($request)
    {
        $divisions_projects = $request->get('divisions_projects');

        if(!empty($divisions_projects)){
            foreach($divisions_projects as $div){
                $this->attach_project_array = $this->pushToArrayProjectsForAttach($div, $this->attach_project_array);
            }
        }
    }

    protected function notifyPysde($file_name1, $file_name2 = null)
    {
        $user_pysde = User::where('email', config('requests.MAIL_PYSDE'))->first();

        if($user_pysde != null){
            $name = $this->user->userable->name;

            $data = [
                'name'          => $name,
                'title'         => "ΟΡΓΑΝΙΚΑ Κενά - Πλεονάσματα",
                'description'   => 'description for notify',
                'type'          => 'primary',
                'url'           => route('Pysde::School::getKena'),
                'filename1'      => $file_name1,
                'subfolder'     => 'Organika',
                'subject'       => "[$name] Κενά - Πλεονάσματα για Οργανικές",
            ];

            if($file_name2 != null){
                $data['filename2'] = $file_name2;
            }

            Notification::send($user_pysde, new PysdeNotification($this->user,$data, 'pysde::email.school-organika-divisions'));

            $this->deletePDFfile('Organika');
        }
    }

    protected function getComputerNumber($students, $number)
    {
        $realNumber = $this->calculateRealNumber($students);

        $minComputerStudents = 22;

        $x = intdiv($students, $realNumber);
        $mod = intval($students) % intval($realNumber);

        if($x >= $minComputerStudents){
            return  2 * $realNumber;
        }else if($x < ($minComputerStudents - 1)){
            return $realNumber;
        }else if($x == ($minComputerStudents - 1)){
            return (2 * $mod) + ($realNumber - $mod);
        }
    }

    protected function calculateRealNumber($students){
        return intdiv($students, $this->maxStudents) + 1;
    }

    protected function getFrench($number)
    {
        return intdiv($number, 2) + ($number % 2);
    }

    protected function getGerman($number)
    {
        return $number - $this->getFrench($number);
    }

}