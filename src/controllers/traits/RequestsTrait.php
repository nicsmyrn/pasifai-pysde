<?php

namespace Pasifai\Pysde\controllers\traits;

use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Pasifai\Pysde\models\RequestTeacherYperarithmia;
use Illuminate\Support\Facades\Mail;
use Pasifai\Pysde\mail\EpithimiaYperarithmias;
use Pasifai\Pysde\mail\RecallRequest;
use Pasifai\Pysde\mail\recallAnswer;
use Pasifai\Pysde\mail\OrganikaYperarithmos;

trait RequestsTrait
{
    protected $send_notification = false;

    public function makePDFforRequests($attr, $teacher_folder, $file_path, $view)
    {

        \Storage::makeDirectory('teachers/'.$teacher_folder);

        $pdf = PDF::loadView($view,$attr);

        $pdf->setPaper('a4', 'portrait')->save(storage_path('app/teachers/'. $file_path));        

    }

    public function getTheCorrectMail($user)
    {
        $has_word = 'not_used';
        $email = config('requests.MAIL_PYSDE');

        if(strpos($user->sch_mail, $has_word) === false){
            if($user->sch_mail !== ''){
                $this->send_notification = true;
                $email = $user->sch_mail;
            }
        }else{
            if(strpos($user->email, $has_word) === false){
                if($user->email !== ''){
                    $this->send_notification = true;
                    $email = $user->email;
                }
            }
        }

        return $email;
    }
}