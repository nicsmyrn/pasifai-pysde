<?php

namespace Pasifai\Pysde\controllers\traits;
use App\Teacher;

trait MoriaCalculator
{
    public function getMoriaApospasis(Teacher $teacher)
    {
        $sum = 0;

        if(is_null($teacher)){
            $teacher = $this->user->userable;
        }

        $years = $teacher->ex_years;
        $months = $teacher->ex_months;
        $days = $teacher->ex_days;
        $kids = $teacher->childs;

        $ygeia_idiou = $this->calculateIdionSizigouTeknon($teacher->logoi_ygeias_idiou);

        $ygeia_sizigou = $this->calculateIdionSizigouTeknon($teacher->logoi_ygeias_sizigou);
        $ygeia_teknon = $this->calculateIdionSizigouTeknon($teacher->logoi_ygeias_paidiwn);

        $ygeia_goneon = $this->calculateGoneon($teacher->logoi_ygeias_goneon);
        $ygeia_aderfon = $this->calculateAderfon($teacher->logoi_ygeias_aderfon);
        $ygeia_exosomatiki = $this->calculateExosomatiki($teacher->logoi_ygeias_exosomatiki);


        $family = $teacher->family_situation;

        $kids = $this->calculateKids($kids);

        $family = $this->calculateFamilyMoria($family, $kids);

        $proipiresia = $this->calculateProipiresia($years, $days, $months);

        $extra = $this->getEntopiotita($teacher) + $this->getSinipiretisiMoria($teacher);
        
        $sum = $proipiresia + $family + $kids  + $ygeia_idiou + $ygeia_teknon + $ygeia_sizigou + $ygeia_aderfon + $ygeia_goneon + $ygeia_exosomatiki;

        return $sum;
    }

    protected function getMoriaDetails($teacher = null)
    {
        if(is_null($teacher)){
            $teacher = $this->user->userable;
        }

        $years = $teacher->ex_years;
        $months = $teacher->ex_months;
        $days = $teacher->ex_days;
        $kids = $teacher->childs;

        $ygeia_idiou = $this->calculateIdionSizigouTeknon($teacher->logoi_ygeias_idiou);

        $ygeia_sizigou = $this->calculateIdionSizigouTeknon($teacher->logoi_ygeias_sizigou);
        $ygeia_teknon = $this->calculateIdionSizigouTeknon($teacher->logoi_ygeias_paidiwn);

        $ygeia_goneon = $this->calculateGoneon($teacher->logoi_ygeias_goneon);
        $ygeia_aderfon = $this->calculateAderfon($teacher->logoi_ygeias_aderfon);
        $ygeia_exosomatiki = $this->calculateExosomatiki($teacher->logoi_ygeias_exosomatiki);


        $family = $teacher->family_situation;

        $kids = $this->calculateKids($kids);

        $family = $this->calculateFamilyMoria($family, $kids);

        $proipiresia = $this->calculateProipiresia($years, $days, $months);

        return collect([
            ['name'=> 'proipiresia','title' => 'Μόρια Εκπαιδευτικής Προϋπηρεσίας', 'value' => $proipiresia],
            ['name'=> 'family','title' => 'Μόρια Οικογενειακής Κατάστασης', 'value' => $family],
            ['name'=> 'kids','title' => 'Μόρια Τέκνων', 'value' => $kids],
            ['name'=> 'ygeia_idiou','title' => 'Μόρια Υγείας Ιδίου', 'value' => $ygeia_idiou],
            ['name'=> 'ygeia_sizigou','title' => 'Μόρια Υγείας Συζύγου', 'value' => $ygeia_sizigou],
            ['name'=> 'ygeia_teknon','title' => 'Μόρια Υγείας Τέκνων', 'value' => $ygeia_teknon],
            ['name'=> 'ygeia_goneon','title' => 'Μόρια Υγείας Γονέων', 'value' => $ygeia_goneon],
            ['name'=> 'ygeia_aderfon','title' => 'Μόρια Υγείας Αδερφών', 'value' => $ygeia_aderfon],
            ['name'=> 'exosomatiki','title' => 'Θεραπεία Εξωσωματικής Γονιμοποίησης', 'value' => $ygeia_exosomatiki]
        ]);
    }

    protected function calculateGoneon($value)
    {
        switch ($value){
            case 0:
                $calculatedValue = 0;
                break;
            case 1:
                $calculatedValue = 1;
                break;
            case 2:
                $calculatedValue = 3;
                break;
            default:
                $calculatedValue = 0;
        }

        return $calculatedValue;
    }

    protected function calculateAderfon($value)
    {
        switch ($value){
            case 0:
                $calculatedValue = 0;
                break;
            case 1:
                $calculatedValue = 5;
                break;
            default:
                $calculatedValue = 0;
        }

        return $calculatedValue;
    }

    protected function calculateExosomatiki($value)
    {
        switch ($value){
            case 0:
                $calculatedValue = 0;
                break;
            case 1:
                $calculatedValue = 3;
                break;
            default:
                $calculatedValue = 0;
        }

        return $calculatedValue;
    }

    protected function calculateIdionSizigouTeknon($value){
        switch ($value){
            case 0:
                $calculatedValue = 0;
                break;
            case 1:
                $calculatedValue = 5;
                break;
            case 2:
                $calculatedValue = 20;
                break;
            case 3:
                $calculatedValue = 30;
                break;
            default:
                $calculatedValue = 0;
        }

        return $calculatedValue;
    }

    /**
     * @param $family
     * @param $kids
     */
    protected function calculateFamilyMoria($family, $kids)
    {
        if ($family == 1 || $family == 2 || $family == 4 || $family == 6) $family = 4;

        if ($family == 3 && $kids > 0) $family = 12;
        elseif ($family == 3 && $kids == 0) $family = 4;

        if ($family == 5 && $kids > 0) $family = 6;

        return $family;
    }


    /**
     * @param $kids
     */
    protected function calculateKids($kids)
    {
        if ($kids == 1) $kids = 5;
        else if ($kids == 2) $kids = 5 + 6;
        else if ($kids == 3) $kids = 5 + 6 + 8;
        else if ($kids > 3) $kids = 5 + 6 + 8 + ($kids - 3) * 10;
        else $kids = 0;

        return $kids;
    }

    /**
     * @param $years
     * @param $days
     * @param $months
     */
    protected function calculateProipiresia($years, $days, $months)
    {
        $sintelestis = 1;
        if ($years > 10 && $years <= 20) {
            $sintelestis = 1.5;
            $years_s = 10 + ($years - 10) * $sintelestis;
        } else if ($years > 20) {
            $sintelestis = 2;
            $years_s = 25 + ($years - 20) * $sintelestis;
        } else if ($years >= 1 && $years <= 10) {
            $years_s = $years;
        } else $years_s = 0;

        if ($days >= 15 && $days <= 31) $months = $months + 1;

        if ($years == 10) $sintelestis = 1.5;
        if ($years == 20) $sintelestis = 2;

        if ($months >= 1 && $months <= 12) {
            $months = ($months / 12) * $sintelestis;
        } else  $months = 0;

        return  $years_s + $months;
    }

    protected function getSinipiretisiMoria($teacher)
    {
        $dimos_sinipiretisis = $teacher->dimos_sinipiretisis;
        $sinipiretisi = 0;
        if ($dimos_sinipiretisis > 0 && $dimos_sinipiretisis <= 6) $sinipiretisi = 10;

        return $sinipiretisi;
    }

    protected function getEntopiotita($teacher)
    {
        $entopiotita = 0;
        $dimos_entopiotitas = $teacher->dimos_entopiotitas;

        if ($dimos_entopiotitas  > 0 && $dimos_entopiotitas <= 6) $entopiotita = 4;

        return $entopiotita;
    }
}