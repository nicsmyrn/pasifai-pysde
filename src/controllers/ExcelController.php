<?php

namespace Pasifai\Pysde\controllers;

use App\Http\Controllers\Controller;
use App\School;
use Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Pasifai\Pysde\models\OrganikiRequest;
use Pasifai\Pysde\controllers\ExcelExports\OrganikesYperarithmoi;
use Maatwebsite\Excel\Facades\Excel;
use Pasifai\Pysde\controllers\ExcelExports\AnaplirotesE4;
use Pasifai\Pysde\controllers\ExcelExports\ApospasiE3;
use Pasifai\Pysde\controllers\ExcelExports\DiathesiE1;
use Pasifai\Pysde\controllers\ExcelExports\E6Diathesi;
use Pasifai\Pysde\controllers\ExcelExports\LeitourgikaYperarithmoi;
use Pasifai\Pysde\controllers\ExcelExports\SiblirosiE2;

class ExcelController extends Controller
{    
    private $sort = [
        'group_name'     => 'asc',
        'special_situation'  => 'asc',
        'moria' => 'desc'
    ];

    private $sort_apospasis = [
        'moria_apospasis' => 'desc',
        'last_name'       => 'asc'
    ];

    private $sort_anaplirotes = [
        // 'type_pinakas'      => 'asc',
        'position'          => 'asc',
        'last_name'       => 'asc'
    ];

    private $sort_leitourgika = [
        'group_name'        => 'desc',
        'moria'             => 'desc',
        'last_name'         => 'asc'
    ];

    private $sort_veiltiwseis = [
        'special_situation'  => 'asc',
        'moria' => 'desc',
        'last_name' => 'asc'
    ];

    private $sort_neodioristoi = [
        'type' => 'asc',
        'special_situation'  => 'asc',
        'moria' => 'desc',
        'seira_pinaka' => 'asc'
    ];

    private $sort_yperarithmous = [
        'aitisi_type'     => 'asc',
        'group_name'  => 'asc',
        'moria' => 'desc'
    ];

    protected $user;
    protected $requests_types = [
        'Ε1 Υπεράριθμοι',
        'Ε1 Διάθεση/μετάθεση',
        'Ε2 συμπλήρωση',
        'Ε3 (απόσπαση)',
        'Ε4 (αναπληρωτές)',
        'Ε5 (αναπληρωτές Covid)',
        'E6 - Νεοδιόριστοι',
        'Ονομαστικά Υπεράριθμοι',
        'Βελτιώση - Οριστική Τοποθέτηση'
    ];

    protected $groupNames = [
        'Ονομαστικά Υπεράριθμοι' => 'ΟργανικήΥπεράριθμου',
        'Βελτιώση - Οριστική Τοποθέτηση'    => 'Βελτίωση',
        'Ε1 Υπεράριθμοι' => 'Λειτουργικά Υπεράριθμος',
        'Ε1 Διάθεση/μετάθεση' => 'Διάθεση',
        'Ε2 συμπλήρωση' => 'Συμπλήρωση',
        'Ε3 (απόσπαση)' => 'Απόσπαση Εντός',
        'Ε4 (αναπληρωτές)' => 'Αναπληρωτής',
        'Ε5 (αναπληρωτές Covid)' => 'Αναπληρωτής - Covid 19',
        'E6 - Νεοδιόριστοι' => 'Διάθεση ΠΥΣΔΕ - ΝΕΟΔΙΟΡΙΣΤΟΣ'
    ];

    /**
     * This Controller is under Pysde Role 
     */
    public function __construct()
    {
        // Auth::loginUsingId(56);         //todo not forget to comment this line
        $this->middleware('isPysde');
    }

    public function getPrepareExcel()
    {
        $types = $this->requests_types;
        
        return view('pysde::manage.excelPreparationRequests', compact('types'));
    }

    public function excelTeachersRequests(Request $request)
    {
        $type = $request->get('typeOfRequest');
        $file_name = $request->get('file_name');
        $from = Carbon::createFromFormat('d/m/Y', $request->get('excel_date_from'))->format('Y-m-d 00:00:00');
        $to = Carbon::createFromFormat('d/m/Y', $request->get('excel_date_to'))->format('Y-m-d 23:59:59');
        $order = 'date_request';
        $orderByMoria = true;      
        
        return $this->createExcelByType($type, $from, $to, $order, $orderByMoria, $file_name);
    }

    protected function createExcelByType($type, $from, $to, $order, $orderByMoria, $file_name)
    {
        if($type == 'Ε1 Υπεράριθμοι'){
            return $this->createE1RequestLeitourgikaKena($type, $from, $to, $order, $orderByMoria, $file_name);
        }elseif($type == 'Ε1 Διάθεση/μετάθεση'){
            return $this->createE1diathesiRequestLeitourgikaKena($type, $from, $to, $order, $orderByMoria, $file_name);
        }elseif($type == 'Ε2 συμπλήρωση'){
            return $this->createE234diathesiRequestLeitourgikaKena($type, $from, $to, $order, $orderByMoria, $file_name);
        }elseif($type == 'Ε3 (απόσπαση)'){
            return $this->createE3apospasiLeitourgika($type, $from, $to, $order, $orderByMoria, $file_name);
        }elseif($type == 'Ε4 (αναπληρωτές)'){
            return $this->createE4Anaplirotes($type, $from, $to, $order, $orderByMoria, $file_name);
        }elseif($type == 'Ε5 (αναπληρωτές Covid)'){
            return $this->createE4Anaplirotes($type, $from, $to, $order, $orderByMoria, $file_name);
        }elseif($type == 'E6 - Νεοδιόριστοι'){
            return $this->createE6diathesiRequestLeitourgikaKena($type, $from, $to, $order, $orderByMoria, $file_name);
        }elseif($type == 'Ονομαστικά Υπεράριθμοι'){
            return $this->createRequestsOrganikaKena($type, $from, $to, $order, $orderByMoria, $file_name);
        }elseif($type == 'Βελτιώση - Οριστική Τοποθέτηση'){
            return $this->createRequestsOrganikaKena($type, $from, $to, $order, $orderByMoria, $file_name);
        }
    }

    private function createE4Anaplirotes($type, $from, $to, $order, $orderByMoria, $file_name)
    {
        
        $requests = OrganikiRequest::with('prefrences')
            ->where('groupType', $this->groupNames[$type])
            ->whereNotNull('protocol_number')
            ->whereBetween('date_request', [$from, $to])
            ->where(function($q){
                $q->whereNull('recall_situation')
                ->orWhereIn('recall_situation', ['Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ', 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ']);
            })
            ->orderBy($order)
            ->get();
                        
        if ($requests->isEmpty()) {
            return redirect()->back()->withErrors('Δεν υπάρχουν αιτήσεις με τα συγκεκριμένα κριτήρια');
        }
    
        $requests = $requests->each(function ($item, $key) use ($type) {
            $item->klados = $item->teacher->myschool->new_klados;
            $item->klados_united = $item->teacher->myschool->new_klados_name;
            $item->full_name = $item->teacher->user->full_name;
            $item->organiki = $item->teacher->teacherable->organiki_name;
            $item->organiki_id = $item->teacher->teacherable->organiki_id;
            $item->middle_name = $item->teacher->middle_name;
            $item->ypoxreotiko = $item->teacher->myschool->ypoxreotiko;
            $item->type_pinakas = $item->teacher->myschool->type_pinakas;
            $item->position = $item->teacher->myschool->position;
        });
        
        $sorted = $requests->sort($this->compareCollectionAnaplirotes())->values();

        $kladoiArray = $sorted->groupBy('klados_united');
                
        return Excel::download(new AnaplirotesE4($kladoiArray), Carbon::now()->format('Y_m_d_His').'_'.str_slug($type).'.xlsx');
    }

    private function createE3apospasiLeitourgika($type, $from, $to, $order, $orderByMoria, $file_name)
    {                
        $requests = OrganikiRequest::with('prefrences')
            ->where('groupType', $this->groupNames[$type])
            ->whereNotNull('protocol_number')
            ->whereBetween('date_request', [$from, $to])
            ->where(function($q){
                $q->whereNull('recall_situation')
                ->orWhereIn('recall_situation', ['Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ', 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ']);
            })
            ->orderBy($order)
            ->get();
                        
        if ($requests->isEmpty()) {
            return redirect()->back()->withErrors('Δεν υπάρχουν αιτήσεις με τα συγκεκριμένα κριτήρια');
        }
    
        $requests = $requests->each(function ($item, $key) use ($type) {
            $item->klados = $item->teacher->myschool->new_klados;
            $item->klados_united = $item->teacher->myschool->new_klados_name;
            $item->full_name = $item->teacher->myschool->full_name;
            $item->am = $item->teacher->myschool->am;
            $item->moria_apospasis = $item->teacher->myschool->edata->moria_apospasis;
            $item->organiki = $item->teacher->myschool->organiki_name;
            $item->organiki_id = $item->teacher->myschool->organiki_id;
            $item->middle_name = $item->teacher->middle_name;
            $item->dimos_entopiotitas = \Config::get('requests.dimos')[$item->teacher->myschool->edata->dimos_entopiotitas];
            $item->dimos_sinipiretisis = \Config::get('requests.dimos')[$item->teacher->myschool->edata->dimos_sinipiretisis];
            $item->ypoxreotiko = $item->teacher->myschool->ypoxreotiko;
            $item->stratiwtikos = $item->teacher->myschool->sizigos_stratiwtikou ? 'ΝΑΙ' : '-';  
            $item->type = $item->teacher->myschool->type;
        });
        
        $sorted = $requests->sort($this->compareCollectionLeitourgikaApospasis())->values();

        $kladoiArray = $sorted->groupBy('klados_united');
                      
        return Excel::download(new ApospasiE3($kladoiArray), Carbon::now()->format('Y_m_d_His').'_'.str_slug($type).'.xlsx');
    }

    private function createE234diathesiRequestLeitourgikaKena($type, $from, $to, $order, $orderByMoria, $file_name)
    {
        
        $requests = OrganikiRequest::with('prefrences')
            ->where('groupType', $this->groupNames[$type])
            ->whereNotNull('protocol_number')
            ->whereBetween('date_request', [$from, $to])
            ->where(function($q){
                $q->whereNull('recall_situation')
                ->orWhereIn('recall_situation', ['Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ', 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ']);
            })
            ->orderBy($order)
            ->get();
                
        if ($requests->isEmpty()) {
            return redirect()->back()->withErrors('Δεν υπάρχουν αιτήσεις με τα συγκεκριμένα κριτήρια');
        }
    
        $requests = $requests->each(function ($item, $key) use ($type) {
            $item->klados = $item->teacher->myschool->new_klados;
            $item->klados_united = $item->teacher->myschool->new_klados_name;
            $item->full_name = $item->teacher->user->full_name;
            $item->moria = $item->teacher->teacherable->moria;
            $item->group_name = $item->teacher->teacherable->group_name != null ? $item->teacher->teacherable->group_name : 'Χ';
            $item->organiki = $item->teacher->teacherable->organiki_name;
            $item->organiki_id = $item->teacher->teacherable->organiki_id;
            $item->middle_name = $item->teacher->middle_name;
            $item->dimos_entopiotitas = \Config::get('requests.dimos')[$item->teacher->dimos_entopiotitas];
            $item->dimos_sinipiretisis = \Config::get('requests.dimos')[$item->teacher->dimos_sinipiretisis];
            $item->special_situation = $item->teacher->special_situation ? 'ΝΑΙ' : 'ΟΧΙ';
            $item->ypoxreotiko = $item->teacher->myschool->ypoxreotiko;

            $sistegazomena = [];

            if($item->teacher->teacherable->organiki_id != 667){
                foreach(config('requests.sistegazomena') as $key=>$value){
                    if(in_array($item->teacher->teacherable->organiki_id, $value)){
                        $sistegazomena_ids = array_filter($value, function($v) use($item){
                            return ($v != $item->teacher->teacherable->organiki_id);
                        });

                        foreach($sistegazomena_ids as $k=>$v){
                            $sistegazomena[] = School::find($v)->name;
                        }
                        // dd($value);
                    }else{
                        // dd('out');
                    }
                }
            }

            $item->sistegazomena = $sistegazomena;
        });

        $sorted = $requests->sort($this->compareCollection())->values();

        $kladoiArray = $sorted->groupBy('klados_united');
                
        return Excel::download(new SiblirosiE2($kladoiArray), Carbon::now()->format('Y_m_d_His').'_'.str_slug($type).'.xlsx');
    }

    private function createE6diathesiRequestLeitourgikaKena($type, $from, $to, $order, $orderByMoria, $file_name)
    {
        $requests = OrganikiRequest::with('prefrences')
            // ->where('groupType', $this->groupNames[$type])
            ->where('aitisi_type', 'E6')
            ->whereNotNull('protocol_number')
            ->whereBetween('date_request', [$from, $to])
            ->where(function($q){
                $q->whereNull('recall_situation')
                ->orWhereIn('recall_situation', ['Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ', 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ']);
            })
            ->orderBy($order)
            ->get();
        
        if ($requests->isEmpty()) {
            return redirect()->back()->withErrors('Δεν υπάρχουν αιτήσεις με τα συγκεκριμένα κριτήρια');
        }
        
        $requests = $requests->each(function ($item, $key) use ($type) {
            $item->klados = $item->teacher->myschool->new_klados;
            $item->klados_united = $item->teacher->myschool->new_klados_name;
            $item->full_name = $item->teacher->user->full_name;
            $item->type = $item->teacher->myschool->type;
            $item->moria = $item->teacher->teacherable->moria;
            $item->seira_pinaka = $item->teacher->myschool->position;
            $item->group_name = $item->teacher->teacherable->group_name;
            $item->organiki = $item->teacher->teacherable->organiki_name;
            $item->middle_name = $item->teacher->middle_name;
            $item->dimos_entopiotitas = \Config::get('requests.dimos')[$item->teacher->dimos_entopiotitas];
            $item->dimos_sinipiretisis = \Config::get('requests.dimos')[$item->teacher->dimos_sinipiretisis];
            $item->special_situation = $item->teacher->special_situation ? 'ΝΑΙ' : 'ΟΧΙ';
        });

        $sorted = $requests->sort($this->compareCollectionNeodioristoi())->values();

        // if ($orderByMoria) {
        //     $requests = $requests->sortByDesc('moria');
        // }

        $kladoiArray = $sorted->groupBy('klados_united');          
        return Excel::download(new E6Diathesi($kladoiArray), str_slug($type).'_'.Carbon::now()->format('YmdHis').'.xlsx');
    }

    private function createE1diathesiRequestLeitourgikaKena($type, $from, $to, $order, $orderByMoria, $file_name)
    {
        $requests = OrganikiRequest::with('prefrences')
            // ->where('groupType', $this->groupNames[$type])
            ->where('aitisi_type', 'E1')
            ->whereNotNull('protocol_number')
            ->whereBetween('date_request', [$from, $to])
            ->where(function($q){
                $q->whereNull('recall_situation')
                ->orWhereIn('recall_situation', ['Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ', 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ']);
            })
            ->orderBy($order)
            ->get();
        
        if ($requests->isEmpty()) {
            return redirect()->back()->withErrors('Δεν υπάρχουν αιτήσεις με τα συγκεκριμένα κριτήρια');
        }
        
        $requests = $requests->each(function ($item, $key) use ($type) {
            $item->klados = $item->teacher->myschool->new_klados;
            $item->klados_united = $item->teacher->myschool->new_klados_name;
            $item->full_name = $item->teacher->user->full_name;
            $item->moria = $item->teacher->teacherable->moria;
            $item->group_name = $item->teacher->teacherable->group_name;
            $item->organiki = $item->teacher->teacherable->organiki_name;
            $item->middle_name = $item->teacher->middle_name;
            $item->dimos_entopiotitas = \Config::get('requests.dimos')[$item->teacher->dimos_entopiotitas];
            $item->dimos_sinipiretisis = \Config::get('requests.dimos')[$item->teacher->dimos_sinipiretisis];
            $item->special_situation = $item->teacher->special_situation ? 'ΝΑΙ' : 'ΟΧΙ';
        });

        if ($orderByMoria) {
            $requests = $requests->sortByDesc('moria');
        }

        $kladoiArray = $requests->groupBy('klados_united');
        
        return Excel::download(new DiathesiE1($kladoiArray), str_slug($type).'_'.Carbon::now()->format('YmdHis').'.xlsx');
    }

    private function createE1RequestLeitourgikaKena($type, $from, $to, $order, $orderByMoria, $file_name)
    {
        $requests = OrganikiRequest::with('prefrences')
            ->where('groupType', $this->groupNames[$type])
            ->whereIn('aitisi_type', ['ΥπεράριθμοςΊδιαΟμάδα', 'ΥπεράριθμοςΌμορες'])
            ->whereNotNull('protocol_number')
            ->whereBetween('date_request', [$from, $to])
            ->where(function($q){
                $q->whereNull('recall_situation')
                    ->orWhereIn('recall_situation', ['Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ', 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ']);
            })
            ->orderBy($order)
            ->get();
        
        if ($requests->isEmpty()) {
            return redirect()->back()->withErrors('Δεν υπάρχουν αιτήσεις με τα συγκεκριμένα κριτήρια');
        }
        
        $r = $requests->each(function ($item, $key) use ($type) {
            $item->klados = $item->teacher->myschool->new_klados;
            $item->klados_united = $item->teacher->myschool->new_klados_name;
            $item->full_name = $item->teacher->user->full_name;
            $item->moria = $item->teacher->teacherable->moria;
            $item->group_name = $item->teacher->teacherable->group_name;
            $item->organiki = $item->teacher->teacherable->organiki_name;
            $item->middle_name = $item->teacher->middle_name;
            $item->dimos_entopiotitas = \Config::get('requests.dimos')[$item->teacher->dimos_entopiotitas];
            $item->dimos_sinipiretisis = \Config::get('requests.dimos')[$item->teacher->dimos_sinipiretisis];
            $item->special_situation = $item->teacher->special_situation ? 'ΝΑΙ' : 'ΟΧΙ';
        });

        // if ($orderByMoria) {
        //     $requests = $requests->sortByDesc('moria');
        // }


        $sorted = $requests->sort($this->compareCollectionLeitourgika())->values();

        $kladoiArray = $sorted->groupBy(function($item, $key){
            return $item['klados_united'] . '-' . mb_substr($item['aitisi_type'], 11);
        });
                
        return Excel::download(new LeitourgikaYperarithmoi($kladoiArray), str_slug($type).'_'.Carbon::now()->format('YmdHis').'.xlsx');
    }

    private function createRequestsOrganikaKena($type, $from, $to, $order, $orderByMoria, $file_name)
    {
            $requests = OrganikiRequest::with('prefrences')
                ->where('groupType', $this->groupNames[$type])
                ->whereNotNull('protocol_number')
                ->whereBetween('date_request', [$from, $to])
                ->where(function($q){
                    $q->whereNull('recall_situation')
                    ->orWhereIn('recall_situation', ['Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ', 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ']);
                })
                ->orderBy($order)
                ->get();
             
            if ($requests->isEmpty()) {
                return redirect()->back()->withErrors('Δεν υπάρχουν αιτήσεις με τα συγκεκριμένα κριτήρια');
            }
            
            $requests = $requests->each(function ($item, $key) use ($type) {
                $item->klados = $item->teacher->myschool->new_klados;
                $item->klados_united = $item->teacher->myschool->new_klados_name;
                $item->full_name = $item->teacher->user->full_name;
                $item->moria = $item->teacher->teacherable->moria;
                $item->group_name = $item->teacher->myschool->group_name;
                $item->organiki = $item->teacher->myschool->organiki_name;
                $item->middle_name = $item->teacher->middle_name;
                $item->dimos_entopiotitas = \Config::get('requests.dimos')[$item->teacher->dimos_entopiotitas];
                $item->dimos_sinipiretisis = \Config::get('requests.dimos')[$item->teacher->dimos_sinipiretisis];
                $item->special_situation = $item->teacher->special_situation ? 'ΝΑΙ' : 'ΟΧΙ';
            });

            if ($orderByMoria) {
                $requests = $requests->sortByDesc('moria');
            }

            if($type == 'Βελτιώση - Οριστική Τοποθέτηση'){
                $sorted = $requests->sort($this->compareCollectionVeltiwseis())->values();
            }else{
                $sorted = $requests->sort($this->compareCollectionYperarithmoi())->values();
            }

            $kladoiArray = $sorted->groupBy('klados_united');            

            return Excel::download(new OrganikesYperarithmoi($kladoiArray, $type), str_slug($type).'_timestamp_'.Carbon::now()->format('YmdHis').'.xlsx');
    }

    protected function getUser()
    {
        $this->user = Auth::user();
    }

    private function compareCollection()
    {
        return function ($a, $b) {
            foreach($this->sort as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }

    private function compareCollectionVeltiwseis()
    {
        return function ($a, $b) {
            foreach($this->sort_veiltiwseis as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }

    private function compareCollectionNeodioristoi()
    {
        return function ($a, $b) {
            foreach($this->sort_neodioristoi as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }
    

    private function compareCollectionYperarithmoi()
    {
        return function ($a, $b) {
            foreach($this->sort_yperarithmous as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }

    private function compareCollectionLeitourgika()
    {
        return function ($a, $b) {
            foreach($this->sort_leitourgika as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };   
    }

    private function compareCollectionLeitourgikaApospasis()
    {
        return function ($a, $b) {
            foreach($this->sort_apospasis as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };   
    }

    private function compareCollectionAnaplirotes()
    {
        return function ($a, $b) {
            foreach($this->sort_anaplirotes as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };   
    }
}
