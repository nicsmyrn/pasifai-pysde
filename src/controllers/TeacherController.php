<?php

namespace Pasifai\Pysde\controllers;

use App\Http\Controllers\Traits\MoriaCalculator;
use App\Models\Anaplirotis;
use App\Models\Eidikotita;
use App\Models\FakeAnaplirotis;
use App\Models\FakeMonimos;
use App\Models\FakeTeacher;
use App\Models\Permission;
use App\Monimos;
use App\MySchoolTeacher;
use App\NewEidikotita;
use App\School;
use App\Teacher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pasifai\Pysde\models\Protocol;
use Pasifai\Pysde\models\RequestTeacher;
use Pasifai\Pysde\models\TeacherRights;
use Illuminate\Support\Facades\Auth;
use App\User;
use Pasifai\Pysde\requests\TeacherIpiresiaRequest;
use Pasifai\Pysde\events\TeacherRequestGrandDenyPermission;
use Pasifai\Pysde\models\RequestTeacherYperarithmia;
use Pasifai\Pysde\controllers\traits\RequestsTrait;
use Pasifai\Pysde\models\OrganikiRequest;
use Pasifai\Pysde\models\ProtocolTeacher;
use Maatwebsite\Excel\Facades\Excel;
use App\Notifications\AdminNotifyTeacher;

use Illuminate\Support\Facades\Notification;
use Pasifai\Pysde\notifications\PysdeToTeacherNotification;

use Pasifai\Pysde\controllers\ExcelExports\YperarithmoiPerSchool;
use Pasifai\Pysde\controllers\traits\Calculator;
use App\Http\Controllers\Traits\TraitNotification;
use Log;

class TeacherController extends Controller
{
    use MoriaCalculator, RequestsTrait, Calculator, TraitNotification;

    public function __construct()
    {

        $this->middleware('isPysde');
    }

    public function indexRequestsOfTeachers(Request $request)
    {        
        $notification_id = $request->get('id');

        $this->checkIfRead($notification_id);

        return view('pysde::admin.index-requests');
    }

    public function getEditTeacherIpiresiaka(User $userTeacher, Request $request)
    {
        $notification_id = $request->get('id');

        $this->checkIfRead($notification_id);

        return view('pysde::admin.teacher-ipiresia', compact('userTeacher'));
    }

    public function postEditTeacherIpiresiaka(User $userTeacher, TeacherIpiresiaRequest $request)
    {
        $this->getUser();

        $teacher = $userTeacher->userable;
        $teacherable_instance= $teacher->teacherable;
        $myschool_instance = $userTeacher->userable->myschool;
        $edata = $myschool_instance->edata;

        \DB::beginTransaction();

            $teacher->update($request->all());

            $teacherable_instance->update($request->all());

            if($edata != null){
                $edata->update([
                    'dimos_entopiotitas'        => $request->get('dimos_entopiotitas'),
                    'dimos_sinipiretisis'       => $request->get('dimos_sinipiretisis'),
                    'ex_years'                  => $request->get('ex_years'),
                    'ex_months'                 => $request->get('ex_months'),
                    'ex_days'                   => $request->get('ex_days'),
                    'sum_tekna'                 => $request->get('childs'),
                    'family_situation'          => $request->get('family_situation'),
                    'sum_moria'                 => $request->get('moria')
                ]);
            }

            $middle_name = $teacher->middle_name;

            Notification::send($userTeacher, new AdminNotifyTeacher($this->user, $userTeacher->sch_mail, $middle_name, "Ο διαχειριστής τους συστήματος της ΠΑΣΙΦΑΗ έχει αλλάξει και <span class='notification_full_name'>κλειδώσει</span> τα  <span class='notification_full_name'>στοιχεία για το ΠΥΣΔΕ</span> στο Προφίλ σας", "warning", "Κλείδωμα Προφίλ"));

        \DB::commit();

        flash()->success('Συγχαρητήρια!','Οι αλλαγές στα στοιχεία γιατ το ΠΥΣΔΕ του εκπαιδευτικού ' . $userTeacher->full_name . ' έγιναν με επιτυχία.');

    return redirect()->back();
    }
    
    public function getEditTeacherHealth(User $userTeacher, Request $request)
    {
        $notification_id = $request->get('id');

        $this->checkIfRead($notification_id);
        
        return view('pysde::admin.teacher-health', compact('userTeacher'));
    }

    public function getEditTeacherHealthApospasis(User $userTeacher, Request $request)
    {
        $notification_id = $request->get('id');

        $this->checkIfRead($notification_id);
        
        return view('pysde::admin.teacher-health-apospasis', compact('userTeacher'));
    }

    public function postEditTeacherHealth(User $userTeacher, Request $request)
    {        
        $this->getUser();

        $teacher = $userTeacher->userable;

        $edata = $teacher->myschool->edata;

        $teacher->update($request->all());

        if($edata != null) $edata->update($request->all());
    
        $middle_name = $teacher->middle_name;

        Notification::send($userTeacher, new AdminNotifyTeacher($this->user, $userTeacher->sch_mail, $middle_name, "Ο διαχειριστής τους συστήματος της ΠΑΣΙΦΑΗ έχει αλλάξει και <span class='notification_full_name'>κλειδώσει</span> τα  <span class='notification_full_name'>Στοιχεία Υγείας</span> στο Προφίλ σας", "warning", "Κλείδωμα Προφίλ"));

        flash()->success('Συγχαρητήρια!','Οι αλλαγές στα στοιχεία Υγείας του εκπαιδευτικού ' . $userTeacher->full_name . ' έγιναν με επιτυχία.');

        return redirect()->back();
    }

    public function postEditTeacherHealthApospasis(User $userTeacher, Request $request)
    {        
        $this->getUser();

        $teacher = $userTeacher->userable;

        $sum = ['moria_apospasis' => round($this->calculateApospasiForOnePearson($teacher),3)];

        $merged_array = array_merge($sum, $request->all());
        
        return $merged_array;
        
        $teacher->update($merged_array);

        $middle_name = $teacher->middle_name;

        Notification::send($userTeacher, new AdminNotifyTeacher($this->user, $userTeacher->sch_mail, $middle_name, "Ο διαχειριστής τους συστήματος της ΠΑΣΙΦΑΗ έχει αλλάξει και <span class='notification_full_name'>κλειδώσει</span> τα  <span class='notification_full_name'>Στοιχεία Υγείας που αφορούν τις αποσπάσεις</span>", "warning", "Κλείδωμα Προφίλ"));

        flash()->success('Συγχαρητήρια!','Οι αλλαγές στα στοιχεία Υγείας για την ΑΠΟΣΠΑΣΗ του εκπαιδευτικού ' . $userTeacher->full_name . ' έγιναν με επιτυχία.');

        return redirect()->back();
    }

    public function manageOnomastikaYperarithmous()
    {
        $yperarithmoi = TeacherRights::with('myschool')
            ->where('onomastika_yperarithmos', true)
            ->where('year', Carbon::now()->year)
            ->get()->sortBy('myschool.eidikotita')->values();

        return view('pysde::yperarithmoi.manage-onomastika-yperarithmous', compact('yperarithmoi'));
    }

    public function extractYperarithmousToExcel()
    {
        $yperarithmoi = TeacherRights::with('myschool')
            ->where('onomastika_yperarithmos', true)
            ->where('year', Carbon::now()->year)
            ->get()->sortBy('myschool.last_name')->values();

        $collection = collect();

        foreach($yperarithmoi as $y){
            $school = School::where('identifier', $y->myschool->organiki_prosorini)->first();

            if($school!= null){
                $order = $school->order;
                $group = $school->group;
            }else{
                $order = 100;
                $group = 7;
            }

            $teacher_rights = $y->myschool->rights;

            if($teacher_rights != null){
                $description = $teacher_rights->description;
            }else{
                $description = '';
            }

            $collection->push([
                'last_name'  => $y->myschool->last_name,
                'first_name'    => $y->myschool->first_name,
                'middle_name'   => $y->myschool->middle_name,
                'eidikotita'    => $y->myschool->eidikotita,
                'school_name'   => $y->myschool->organiki_name,
                'school_order'  => $order,
                'group'         => $group,
                'description'    => $description,
                'moria'         => $y->myschool->edata->sum_moria
            ]);
        }
        $ordered_collection = $collection->sort($this->compareCollection());

        $yperarithmoiForExcel =  $ordered_collection->groupBy(['group', 'school_name']);

        return Excel::download(new YperarithmoiPerSchool($yperarithmoiForExcel), 'ΟΝΟΜΑΣΤΙΚΑ_ΥΠΕΡΑΡΙΘΜΟΙ_'.Carbon::now()->year.'.xlsx');
    }

    public function ajaxToggleYperarithmiaValueOfTeacher(Request $request)
    {
        if($request->ajax()){
            $myschoolTeacher = MySchoolTeacher::find($request->get('afm'));
            $hasOnomastikiYperarithmia = $request->get('onomastika_yperarithmos');

            if($myschoolTeacher->rights == null){
                TeacherRights::create([
                    'afm'   => $request->get('afm'),
                    'onomastika_yperarithmos'   => true,
                    'year'                      => Carbon::now()->format('Y')
                ]);
            }else{
                $myschoolTeacher->rights->update([
                    'onomastika_yperarithmos'   => !$myschoolTeacher->rights->onomastika_yperarithmos,
                    'year'                      => Carbon::now()->format('Y')
                ]);
            }

            return 'ok';
        }
    }

    public function ajaxFetchTeachersFromMySchool(Request $request)
    {
        if ($request->ajax())
        {
            $school  = School::find($request->get('schoolId'));
            $teachers = array();

            foreach ($school->organika as $t){
                $teachers[] = [
                    'full_name'         => $t['last_name'] . ' ' . $t['first_name'],
                    'middle_name'       => $t->middle_name,
                    'eidikotita_klados' => $t->new_eidikotita_name . '('.$t->new_klados.')',
                    'sxesi'             => $t->topothetisi,
                    'afm'               => $t->afm,
                    'onomastika_yperarithmos'   => $t->rights == null ? false :  $t->rights->onomastika_yperarithmos
                ];
            }

            return $teachers;
        }
    }

    public function allRequests(Request $request)
    {
//        $this->denies('allRequests');

        $uniqueAction = $request->get('action');

        if($uniqueAction != null){
//            Notification::where('uniqueAction', $uniqueAction)
//                ->update([
//                    'unread' => 'is_false'
//                ]);
        }

        $requests = RequestTeacher::where('protocol_number', null)
            ->where('file_name',null)
            ->get();

        return view('pysde::aitisis.index', compact('requests'));
    }

    public function archivesTeachersRequests()
    {
//        $this->denies('archives_protocoled_requests');

        $requests = RequestTeacher::where('protocol_number','<>', 'null')
            ->where('file_name','<>','null')
            ->get();

        return view('pysde::aitisis.archives', compact('requests'));
    }

    public function prepareCalculateApospasis()
    {
        return 'hello';
        
        $requests = RequestTeacher::where('protocol_number','<>', 'null')
            ->where('file_name','<>','null')
            ->where('aitisi_type', 'E3')
            ->orWhere('aitisi_type', 'E4')
            ->orderBy('aitisi_type')
            ->orderBy('protocol_number', 'desc')
            ->groupBy('teacher_id')
            ->get();

        return view('pysde::moria.autocalculate', compact('requests'));
    }

    public function calculateApospasis()
    {
        return 'hello';

//        $teacher = Teacher::find(490);
//        $sum = $this->getMoriaApospasis($teacher);
//        $teacher->teacherable->moria_apospasis = round($sum,3);
//        $teacher->teacherable->save();
//
//        flash()->success('Επιτυχία', 'Ο υπολογισμός των μορίων απόσπασης πραγματοποιήθηκε');
//
//        return redirect()->back();


//         $requests = RequestTeacher::where('protocol_number','<>', 'null')
//             ->where('file_name','<>','null')
//             ->where('aitisi_type', 'E3')
// //            ->orWhere('aitisi_type', 'E4')
//             ->orderBy('aitisi_type')
//             ->orderBy('protocol_number', 'desc')
//             ->groupBy('teacher_id')
//             ->get();
        $all_teachers = Teacher::all();

        foreach($all_teachers as $teacher){
            if($teacher->teacherable_type == 'App\Monimos'){
                $sum = $this->getMoriaApospasis($teacher);

                $teacher->teacherable->moria_apospasis = round($sum,3);
                $teacher->teacherable->save();
            }
        }

        flash()->success('Επιτυχία', 'Ο υπολογισμός των μορίων απόσπασης πραγματοποιήθηκε');

        return redirect()->back();

    }



    public function downloadPDF($unique_id)
    {
        $request_teacher = RequestTeacherYperarithmia::where('unique_id', $unique_id)->first();
        
        if($request_teacher != null){
            $teacher_folder = str_slug($request_teacher->teacher->user->last_name).  md5($request_teacher->teacher->id);

            $file = $unique_id.'.pdf';
            $file_path = storage_path('app/teachers/'.$teacher_folder).'/'.$file;
    
            if (file_exists($file_path)){
                return response()->download($file_path, $file, [
                    'Content-Length: '. filesize($file_path)
                ]);
            }else{
                exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');
            }
        }else{
            $request_teacher = OrganikiRequest::where('unique_id', $unique_id)->first();

            if($request_teacher != null){
                $teacher_folder = str_slug($request_teacher->teacher->user->last_name).  md5($request_teacher->teacher->id);
                
                $file = $unique_id.'.pdf';
                $file_path = storage_path('app/teachers/'.$teacher_folder).'/'.$file;
        
                if (file_exists($file_path)){
                    return response()->download($file_path, $file, [
                        'Content-Length: '. filesize($file_path)
                    ]);
                }else{
                    exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');
                }               
            }else{
                exit('Λάθος 666');
            }
        }
    }

    public function checkProfile($teacherID, Request $request)
    {
//        $this->denies('h');

        $action = $request->get('action');
        if ($action != null){
//            Notification::where('uniqueAction', $action)
//                ->update([
//                    'unread' => 'is_false'
//                ]);
        }
        $teacher =  Teacher::find($teacherID);


        $analytika = $this->getMoriaDetails($teacher);
        $sinipiretisi = $this->getSinipiretisiMoria($teacher);
        $entopiotita = $this->getEntopiotita($teacher);

        $kladoi = Eidikotita::pluck('name', 'id');
        $new_kladoi = NewEidikotita::all();
        $schools = School::pluck('name', 'id');

        $schools->put('1000','Διάθεση ΠΥΣΔΕ');
        $schools->put('2000','Απόσπαση από άλλο ΠΥΣΔΕ');

        foreach(\Config::get('requests.teacher_types') as $k=>$v){
            $schools->put($k, $v);
        }

        if(School::where('identifier', $teacher->myschool->organiki_prosorini)->first() != null){
            $myOrganiki = School::where('identifier', $teacher->myschool->organiki_prosorini)->first()->id;
        }else{
            $myOrganiki = 1000;
        }

        return view('pysde::check.checkTeacherProfile', compact('teacher', 'kladoi', 'new_kladoi', 'schools', 'myOrganiki', 'analytika', 'sinipiretisi', 'entopiotita'));
    }

    public function getTeachersNotConfirmed()
    {
//        $this->denies('h');
        $teachers = Teacher::where('is_checked', false)->get();

        $count = $teachers->count();

        return view('pysde::check.teachers_not_confirmed', compact('teachers', 'kladoi', 'count'));
    }

    public function getAllTeachersForConfirmation()
    {
        $teachers = Teacher::all();

        return view('pysde::check.teachers_not_confirmed', compact('teachers'));
    }

    public function confirmTeacherProfile($teacherID, Request $request)
    {
//        $this->denies('h');

        $teacher = Teacher::find($teacherID);

        $validator = \Validator::make($request->all(), $this->teacherRules($teacher->teacherable_type),$this->teacherMessages());

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }else{
            $this->updateTeacherProfile($request, $teacher);
        }

        if ($teacherID != null){
            if($teacher->fid != null){
                $teacher->fake->teacherable->delete();
                $teacher->fake->delete();
            }
            $teacher->update([
                'is_checked'=>true,
                'fid'   => null,
                'request_to_change_profile' => false,
                'profile_new_request' => false,
                'profile_counter'   => 0
            ]);

            flash()->success('Ελέγχθηκε', 'Ο έλεγχος των στοιχείων ολοκληρώθηκε');
            if(\Config::get('requests.sent_mail')) {
//                Smail::sentTeacherProfile($teacher, 'Ορθά στοιχεία Προφίλ', [     //todo must change this
//                    'type' => 'confirm',
//                    'full_name' => $teacher->user->full_name
//                ]);
            }
//            event(new PysdeSendNotificationToTeacher($teacher,[
//                'url' => 'ProfileController@getProfile',  //todo must change this
//                'title' => 'Ορθά Στοιχεία',
//                'description' => "Έπειτα από έλεγχο της υπηρεσίας <span class='notification_full_name'>διορθώθηκαν</span> τα στοιχεία του Προφίλ σου",
//                'type' => 'success'
//            ]));

            return redirect()->route('Teachers::allTeachers');
        }
        abort(403);
    }

    public function wrongTeacherProfile($teacherID)
    {
//        $this->denies('h');

        $teacher = Teacher::find($teacherID);

        if ($teacherID != null){
            if($teacher->fid != null){
                $teacher->fake->teacherable->delete();
                $teacher->fake->delete();
            }
            $teacher->update([
                'is_checked'=>true,
                'request_to_change' => false,
                'fid'   => null,
                'request_to_change_profile' => false,
                'profile_new_request' => false,
                'profile_counter'   => 0
            ]);

            flash()->info('', 'Κλείδωμα χωρίς αλλαγές');

            if(\Config::get('requests.sent_mail')) {
//                Smail::sentTeacherProfile($teacher, 'Καμία αλλαγή στο Προφίλ', [ //todo must change this
//                    'type' => 'recheck',
//                    'full_name' => $teacher->user->full_name
//                ]);
            }
//            event(new PysdeSendNotificationToTeacher($teacher,[
//                'url' => 'ProfileController@getProfile',
//                'title' => 'Καμία αλλαγή στοιχείων Προφίλ',
//                'description' => "Η υπηρεσία <span class='notification_full_name'>ΔΕΝ</span> πραγματοποίησε καμία αλλαγή στα στοιχεία σας διότι είναι ορθά.",
//                'type' => 'danger'
//            ])); //todo must change this

            return redirect()->back();
        }
        abort(403);
    }

    public function updateProfileFields($teacherID, Request $request)
    {
//        $this->denies('h');
        $teacher = Teacher::find($teacherID);

        $validator = \Validator::make($request->all(), $this->teacherRules($teacher->teacherable_type),$this->teacherMessages());

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }else{
            $this->updateTeacherProfile($request, $teacher);

//            event(new PysdeSendNotificationToTeacher($teacher,[
//                'url' => 'ProfileController@getProfile',
//                'title' => 'Αλλαγή Στοιχείων',
//                'description' => "Έπειτα από έλεγχο της υπηρεσίας <span class='notification_full_name'>διαπιστώθηκαν αλλαγές</span> στα στοιχεία του Προφίλ σου",
//                'type' => 'success' //todo must change this
//            ]));

            flash()->info('Οι αλλαγές ενημερώθηκαν');

            return redirect()->back();

        }
    }

    public function giveProtocol($requestUniqueId)
    {
//        $this->denies('h');

        $request_teacher = RequestTeacher::where('unique_id', $requestUniqueId)->first();

        if ($request_teacher == null){
            abort(404);
        }

        $user = $request_teacher->teacher->user;
        $teacher = $request_teacher->teacher;

        if ($request_teacher->teacher->is_checked && $request_teacher->protocol_number == null && $request_teacher->file_name == null){
            $type = $request_teacher->aitisi_type;
            switch($type){
                case 'ΑΠΛΗ':
                    $f_id = 1;      // Φ1
                    break;
                case 'E1':
                    $f_id = 6;      // Φ4
                    break;
                case 'E2':
                    $f_id = 6;      // Φ4
                    break;
                case 'E3':
                    $f_id = 6;      // Φ4
                    break;

                case 'E4':
                    $f_id = 7;      // Φ5
                    break;
                case 'E5':
                    $f_id = 6;      // Φ4
                    break;
                case 'E6':
                    $f_id = 6;      // Φ4
                    break;
                case 'E7':
                    $f_id = 6;      // Φ4
                    break;
                default:
                    $f_id = 1;
            }

            $protocolRequest = array(
                'type' => 0,            //ΕΙΣΕΡΧΟΜΕΝΟ
                'p_date' => date('d/m/Y'),
                'teacher_type' => $type,
                'from_to'   => $request_teacher->teacher->user->full_name,
                'subject'   => 'Αίτηση '. $type,
                'f_id'      => $f_id
            );

            $teacher_folder = md5($request_teacher->teacher->id);
            $file_path = $teacher_folder . '/' . $requestUniqueId . '.pdf';
            \Storage::makeDirectory('teachers/'.$teacher_folder);

            \DB::beginTransaction();
            $protocol = Protocol::create($protocolRequest);

            $request_teacher->update([
                'protocol_number'   => $protocol->id,
                'file_name'         => $file_path
            ]);

            if($type == 'ΑΠΛΗ'){
                $pdf = \PDF::loadView('teacher.simpleAitisiPDF', compact('request_teacher'));
            }else{
                $pdf = \PDF::loadView('teacher.aitisiPDF', compact('request_teacher','type'));
            }

            $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

//            event(new PysdeSendNotificationToTeacher($teacher, [
//                'url' => 'RequestController@index',
//                'title' => 'Πρωτοκόλληση Αίτησης '.$type,
//                'description' => 'Η Αίτηση '.$type. ' πρωτοκολλήθηκε από την Γραμματεία του ΠΥΣΔΕ με αριθμό πρωτοκόλλου: <span class="notification_full_name">'.$protocol->id. '-'.$protocol->p_date.'</span>',
//                'type' => 'primary'
//            ])); //todo must change this

            flash()->overlayS('Συγχαρητήρια', 'Ο αριθμός πρωτοκόλλου : <strong>'.$protocol->id.'</strong> με θέμα: <strong>&laquo;'.$protocol->subject.'&raquo;</strong> αποθηκεύτηκε με επιτυχία και στάλθηκε E-mail στον καθηγητή');

            \DB::commit();

//            \Mail::send('emails.sentTeacherRequest',[
//                'protocol_name'=> $protocol->protocol_name,
//                'type'=>$type,
//                'date'=>$request_teacher->date_request
//            ], function($m) use ($file_path, $user){
//                $m->from('mail3didechanion@gmail.com', 'Δ.Δ.Ε. Χανίων')
//                    ->to($user->email, $user->full_name)
//                    ->subject('Αίτηση προς ΠΥΣΔΕ')
//                    ->attach(storage_path('app/teachers/'.$file_path));
//            }); //todo must change this

        }else{
            flash()->overlayE('Σφάλμα!!!', 'Δε δόθηκε κανένα πρωτόκολλο στην αίτηση');
        }
        return redirect()->back();

    }

    public function cancelRequest($id)
    {
        $teacher = Teacher::find($id);
        $teacher->fake->teacherable->delete();
        $teacher->fake->delete();
        $teacher->fid = null;
        $teacher->is_checked = true;
        $teacher->save();

        flash()->success('', 'Τα στοιχεία του καθηγητή επέστρεψαν στην αρχική τους κατάσταση');
        return redirect()->back();
        /*
         * notification to teacher
         * mail to teacher
         */
    }

    public function confirmRequest($id)
    {
        \DB::beginTransaction();
        $teacher = Teacher::find($id);

        $teacher->teacherable->delete();
        if($teacher->teacherable_type == 'App\Monimos'){
            $teacher->teacherable_type = 'App\Anaplirotis';
            $t_able = Anaplirotis::create($teacher->fake->teacherable->toArray());
            $teacher->teacherable_id = $t_able->id;
        }else{
            $teacher->teacherable_type = 'App\Monimos';
            $t_able = Monimos::create($teacher->fake->teacherable->toArray());
            $teacher->teacherable_id = $t_able->id;
        }
        $teacher->save();

        \DB::commit();
        flash()->success('', 'Η Σχέση του καθηγητή άλλαξε με επιτυχία');

        return redirect()->back();
    }

    public function getManage()
    {
//        $this->denies('h');
        $teacher_permissions = Permission::where('slug','like', '%request')->get();

        return view('pysde::manage.manage_teachers_requests', compact('teacher_permissions'));
    }

    public function changeTeacherRequestAccess(Request $request)
    {
//        $this->denies('h');

        if ($request->ajax()){
            $data = array();
            $permission = 'create_'.$request->get('name').'_request';
            
            Log::warning($permission);

            event(new TeacherRequestGrandDenyPermission($permission)); //todo must go to package

            $data['name'] = $request->get('name');
            $data['checked'] = $request->get('checked');

            return $data;
        }else{
            abort(404);
        }
//
//        return redirect()->back();
    }

    public function unlock($teacherId)
    {
        $teacher = Teacher::find($teacherId);

        $this->unlockTeacherProfile($teacher);

        $this->createFakeProfile($teacher);
        if(\Config::get('requests.sent_mail')) {
//            Smail::sentTeacherProfile($teacher, 'ΞΕΚΛΕΙΔΩΜΑ Προφίλ', [
//                'type' => 'unlockProfile',
//                'full_name' => $teacher->user->full_name
//            ]);//todo must change this
        }
//        event(new PysdeSendNotificationToTeacher($teacher,[
//            'url' => 'ProfileController@getProfile',
//            'title' => 'ΞΕΚΛΕΙΔΩΜΑ ΠΡΟΦΙΛ',
//            'description' => "Τα στοιχεία στο Προφίλ σου <span class='notification_full_name'>είναι επεξεργάσιμα</span>. Μπορείς να πατήσεις εδώ για να πραγματοποιήσεις τις αλλαγές",
//            'type' => 'success'
//        ]));

        flash()->success('', 'OK');

        return redirect()->back();
    }

    private function denies($permision)
    {
        if (\Gate::denies($permision)){
            abort(403);
        }
    }

    private function autoCalculateApospasis($teacher)
    {
        return $this->getMoriaApospasis($teacher);
//        $sum = 0;
//        $years = $teacher->ex_years;
//        $months = $teacher->ex_months;
//        $days = $teacher->ex_days;
//        $kids = $teacher->childs;
//        $family = $teacher->family_situation;
//        $years_s = 0;
//
//        if ($family == 0) $family = 0;
//        elseif ($family == 1 || $family == 2 || $family == 4) $family = 4;
//        elseif ($family == 3 && $kids > 0 && $kids < 10) $family = 12;
//        elseif ($family == 3 && $kids = 0) $family = 4;
//        elseif ($family == 5 && $kids > 0 && $kids < 10) $family = 6;
//
//        if ($kids == 1) $kids = 5;
//        elseif ($kids == 2) $kids = 5 + 6;
//        elseif ($kids == 3) $kids = 5 + 6 + 8;
//        elseif ($kids > 3) $kids = 5 + 6 + 8 + ($kids - 3) * 10;
//        else $kids = 0;
//
//        $sintelestis = 1;
//
//        if ($years > 10 && $years <= 20) {
//            $sintelestis = 1.5;
//            $years_s = 10 + ($years - 10) * $sintelestis;
//        } elseif ($years > 20) {
//            $sintelestis = 2;
//            $years_s = 25 + ($years - 20) * $sintelestis;
//        } elseif ($years >= 1 && $years <= 10) {
//            $years_s = $years;
//        } else {
//            $years_s = 0;
//        }
//
//        if ($days >= 15 && $days <= 31) $months = $months + 1;
//
//        if ($years == 10) $sintelestis = 1.5;
//        if ($years == 20) $sintelestis = 2;
//
//        if ($months >= 1 && $months <= 12) {
//            $months = ($months / 12) * $sintelestis;
//        } else {
//            $months = 0;
//        }
//
//        $sum = $years_s + $months + $kids + $family;
//        return $sum;
    }

    protected function teacherRules($teacherable)
    {
        if ($teacherable == 'App\Anaplirotis'){
            $extra_rules = [
                'afm'   => 'required|digits:9'
            ];
        }elseif($teacherable == 'App\Monimos'){
            $extra_rules = [
                'am'          => 'required|digits:6'
            ];
        }else{
            abort(403);
        }
        return $extra_rules +  $rules = [
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'middle_name' => 'required|min:3',
            'orario'      => 'required|int|between:4,30',
            'childs'      => 'integer|between:0,12',

            'moria' => 'numeric|between:0,400',
            'moria_apospasis' => 'numeric|between:0,400',
            'ex_years'      => 'integer|between:0,40',
            'ex_months'      => 'integer|between:0,11',
            'ex_days'      => 'integer|between:0,30',
        ];
    }

    protected function teacherMessages()
    {
        return         $messages = [
            'childs.int'    => 'Το πεδίο αρ. παιδιών πρέπει να είναι ακέραιος',
            'childs.between' => 'Τα παιδιά μπορεί να είναι μεταξύ 0 και 12',
            'orario.required' => 'Το υποχρεωτικό ωράριο είναι υποχρεωτικό',
            'orario.int'    => 'Το υποχρεωτικό ωράριο πρέπει να είναι ακέραιος',
            'orario.between' => 'Το υποχρεωτικό ωράριο πρέπει να είναι μεταξύ 4 και 30',
            'middle_name.required' => 'Το πατρώνυμο είναι υποχρεωτικό',
            'middle_name.min'   => 'Το Πατρώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'am.required'   => 'Ο Αριθμός Μητρώου είναι υποχρεωτικός',
            'am.digits'        => 'Ο Αριθμός Μητρώου πρέπει να είναι αριθμός 6 ψηφίων',
            'am.unique'     => 'Ο Αριθμός Μητρώου υπάρχει ήδη σε άλλον καθηγητή',
            'afm.required'   => 'Το ΑΦΜ είναι υποχρεωτικό',
            'afm.digits'        => 'Το ΑΦΜ πρέπει να είναι αριθμός 9 ψηφίων',
            'afm.unique'     => 'Το ΑΦΜ υπάρχει ήδη σε άλλον καθηγητή',
            'first_name.required' => 'Το Όνομα είναι υποχρεωτικό',
            'first_name.min'   => 'Το Όνομα πρέπει να είναι τουλάχιστον 3 χαρακτήρες',

            'last_name.required' => 'Το Επώνυμο είναι υποχρεωτικό',
            'last_name.min'   => 'Το Επώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',

            'moria.numeric'    => 'Τα μόρια πρέπει να είναι αριθμός',
            'moria.between'    => 'Τα μόρια πρέπει να είναι μεταξύ 0 και 400',
            'moria_apospasis.numeric'    => 'Τα μόρια πρέπει να είναι αριθμός',
            'moria_apospasis.between'    => 'Τα μόρια πρέπει να είναι μεταξύ 0 και 400',
            'ex_years.int'    => 'Τα χρόνια προϋπηρεσίας πρέπει να είναι ακέραιος',
            'ex_months.int'    => 'Οι μήνες προϋπηρεσίας πρέπει να είναι ακέραιος',
            'ex_days.int'    => 'Οι ημέρες προϋπηρεσίας πρέπει να είναι ακέραιος',
            'ex_years.between'    => 'Τα χρόνια προϋπηρεσίας πρέπει να είναι μεταξύ 0 και 40',
            'ex_months.between'    => 'Οι μήνες προϋπηρεσίας πρέπει να είναι μεταξύ 0 και 11',
            'ex_days.between'    => 'Οι ημέρες προϋπηρεσίας πρέπει να είναι μεταξύ 0 και 30',

        ];
    }

    protected function unlockTeacherProfile($teacher)
    {
        $teacher->request_to_change = false;

        $teacher->is_checked = false;

        $teacher->save();
    }

    protected function lockTeacherProfile($teacher)
    {
        $teacher->request_to_change = false;

        $teacher->is_checked = true;

        $teacher->profile_counter = 0;

        $teacher->save();
    }

    private function createFakeProfile($teacher){

        $fakeTeacher = FakeTeacher::create($teacher->toArray());

        $teacher->fid = $fakeTeacher->fid;
        $teacher->save();

        if($fakeTeacher->teacherable_type == 'App\Monimos'){
            $fakeMonimos = FakeMonimos::create($teacher->teacherable->toArray());
            $fakeTeacher->teacherable_id = $fakeMonimos->id;
            $fakeTeacher->teacherable_type = 'App\FakeMonimos';
        }elseif($fakeTeacher->teacherable_type == 'App\Anaplirotis'){
            $fakeAnaplirotis = FakeAnaplirotis::create($teacher->teacherable->toArray());
            $fakeTeacher->teacherable_id = $fakeAnaplirotis->id;
            $fakeTeacher->teacherable_type = 'App\FakeAnaplirotis';
        }
        $fakeTeacher->save();
    }

    /**
     * @param Request $request
     * @param $teacher
     */
    private function updateTeacherProfile(Request $request,Teacher $teacher)
    {
        $teacher->user->update([
            'last_name' => $request->get('last_name'),
            'first_name' => $request->get('first_name'),
            'sex'        => $request->get('sex')
        ]);

        $teacher->myschool->update([
            'new_eidikotita' => $request->get('new_eidikotita')
        ]);

        $teacher->update([
            'ex_years' => $request->get('ex_years'),
            'ex_months' => $request->get('ex_months'),
            'ex_days' => $request->get('ex_days'),
            'middle_name' => $request->get('middle_name'),
//            'klados_id' => $request->get('klados_id'),
            'family_situation' => $request->get('family_situation'),
            'childs' => $request->get('childs'),
            'special_situation' => $request->get('special_situation'),
            'dimos_sinipiretisis' => $request->get('dimos_sinipiretisis'),
            'dimos_entopiotitas' => $request->get('dimos_entopiotitas'),
            'sex' => $request->get('sex'),
            'logoi_ygeias_idiou'    => $request->get('logoi_ygeias_idiou'),
            'logoi_ygeias_sizigou'    => $request->get('logoi_ygeias_sizigou'),
            'logoi_ygeias_paidiwn'    => $request->get('logoi_ygeias_paidiwn'),
            'logoi_ygeias_goneon'    => $request->get('logoi_ygeias_goneon'),
            'logoi_ygeias_aderfon'    => $request->get('logoi_ygeias_aderfon'),
            'logoi_ygeias_exosomatiki'    => $request->get('logoi_ygeias_exosomatiki')
        ]);


        if ($teacher->teacherable_type == 'App\Anaplirotis') {
            $teacher->teacherable->update([
                'moria' => $request->get('moria'),
                'moria_apospasis' => $request->get('moria_apospasis'),
                'seira_topothetisis' => $request->get('seira_topothetisis'),
                'orario' => $request->get('orario'),
                'afm' => $request->get('afm')
            ]);
        } else {
            $teacher->teacherable->update([
                'am' => $request->get('am'),
                'organiki' => $request->get('organiki'),
                'orario' => $request->get('orario'),
                'moria' => $request->get('moria'),
                'moria_apospasis' => $request->get('moria_apospasis')
            ]);
        }

    }

    protected function calculateApospasiForOnePearson($teacher)
    {
        if($teacher->teacherable_type == 'App\Monimos'){
            $sum = $this->getMoriaApospasis($teacher);
            return $sum;
        }
        return 0;
    }

    protected function getUser()
    {
        // Auth::loginUsingId(56);
        $this->user = Auth::user();
    }
}
