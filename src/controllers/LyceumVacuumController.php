<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use App\Models\Role;
use App\NewEidikotita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\Traits\TraitNotification;
use Pasifai\Pysde\controllers\traits\VacuumTrait;
use Pasifai\Pysde\models\Division;
use Pasifai\Pysde\notifications\SchoolNotification;

class LyceumVacuumController extends Controller
{
    use VacuumTrait;

    public function __construct()
    {
        $this->middleware('isLykeio');
    }

    public function getVacuumsForLyceum()
    {
        return $this->getVacuums();
    }

    public function ajaxGetVacuumsOfSchool()
    {
        return $this->ajaxGetVacuumsForSchools();
    }

    public function getDivisionsForLyceum()
    {       
        if(!config('requests.can_view_organika_kena'))
            abort(403);
            
        return view('pysde::school.lyceum_divisions');
    }

}
