<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\Traits\TraitNotification;
use Pasifai\Pysde\controllers\traits\VacuumTrait;
use Pasifai\Pysde\notifications\SchoolNotification;

class SchoolVacuumController extends Controller
{
    use VacuumTrait;

    public function __construct()
    {
        $this->middleware('isPrimary');
    }

    public function getVacuumsForGymnasium()
    {
        return $this->getVacuums();
    }

    public function ajaxGetVacuumsOfSchool()
    {
        return $this->ajaxGetVacuumsForSchools();
    }

    public function getDivisions()
    {        
        if(!config('requests.can_view_organika_kena'))
            abort(403);

        return view('pysde::school.gymnasium_divisions');
    }

    // public function saveDivisions(Request $request)
    // {
    //     if($request->ajax()){
    //         $this->school = $this->getSchool();
    //         $this->getUser();

    //         $attach_array = array();

    //         $divisions = $request->get('divisions');
    //         $students = $request->get('students');

    //         foreach($divisions as $group_div){
    //             foreach($group_div as $div){
    //                 $makeSubtraction = false;

    //                 if($divisions['ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'][$this->classKeys[$div['class']]]['number'] >= $divisions['ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'][$this->classKeys[$div['class']]]['numberSchoolProvide']){
    //                     $makeSubtraction = true;
    //                 }

    //                 if($div['name'] == 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'){
    //                     $numberOfStudents = $students[$div['class']];
    //                 }elseif($div['name'] == 'ΑΓΓΛΙΚΑ'){
    //                     $numberOfStudents = $students[$div['class']];
    //                 }elseif($div['name'] == 'ΓΑΛΛΙΚΑ'){
    //                     if($makeSubtraction){
    //                         $numberOfStudents = intdiv(($students[$div['class']] * $div['numberSchoolProvide']), $this->generalDivisionRealNumber($students[$div['class']]));
    //                     }else{
    //                         $numberOfStudents = intdiv(($students[$div['class']] * $div['number']), $this->generalDivisionRealNumber($students[$div['class']]));
    //                     }
    //                 }elseif($div['name'] == 'ΓΕΡΜΑΝΙΚΑ'){
    //                     if($makeSubtraction){
    //                         $numberOfStudents = intdiv(($students[$div['class']] * $div['numberSchoolProvide']), $this->generalDivisionRealNumber($students[$div['class']]));
    //                     }else{
    //                         $numberOfStudents = intdiv(($students[$div['class']] * $div['number']), $this->generalDivisionRealNumber($students[$div['class']]));
    //                     }                    }elseif($div['name'] == 'ΠΛΗΡΟΦΟΡΙΚΗΣ'){
    //                 }elseif($div['name'] == 'ΤΕΧΝΟΛΟΓΙΑΣ'){
    //                     $numberOfStudents = $students[$div['class']];
    //                 }else{
    //                     $numberOfStudents = 0;
    //                 }
    //                 $attach_array[$div['div_id']] = [
    //                     'number'    => $div['number'],
    //                     'numberSchoolProvide'   => $div['numberSchoolProvide'],
    //                     'students'              => $numberOfStudents
    //                 ];
    //             }
    //         }

    //         $this->school->divisions()->sync($attach_array);

    //         $user = Role::where('slug', 'pysde_secretary')->first()->users->first();

    //        Notification::send($user, new SchoolNotification($this->user,[
    //            'title'         => 'Αριθμός Τμημάτων '. $this->school->name,
    //            'description'   => "Κενά - Πλεονάσματα",
    //            'type'          => 'primary',
    //            'url'           => route('Pysde::Secretary::assignment::vacuumPerSchool', str_replace(' ', '-', $this->school->name))
    //        ]));

    //         return 'τα δεδομένα στάλθηκαν με επιτυχία στην Γραμματεία του ΠΥΣΔΕ';
    //     }
    // }

    // public function ajaxGetDivisionsOfSchool()
    // {
    //     $data = array();

    //     $this->school = $this->getSchool();

    //     $databaseCollection =  $this->school->divisions;

    //     $divisions = collect();

    //     foreach($databaseCollection as $c){
    //         if(array_key_exists($c->name, $this->statusDivs)){
    //             $status = $this->statusDivs[$c->name];
    //         }else{
    //             $status = 'locked';
    //         }

    //         $this->getStudentsFromDivision($c->name, $c->class, $c->pivot->students);

    //         $divisions->push([
    //             'div_id'    => $c->id,
    //             'name'      => $c->name,
    //             'class'     => $c->class,
    //             'numberSchoolProvide'   => $c->pivot->numberSchoolProvide,
    //             'number'    => $c->pivot->number,
    //             'status'    => $status,
    //             'students'  => $c->pivot->students
    //         ]);

    //     }

    //     $data['divisions'] =  $divisions->sortBy('div_id')->values()->groupBy('name');
    //     $data['students'] = $this->students;

    //     return $data;
    // }

}
