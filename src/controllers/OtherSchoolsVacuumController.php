<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use App\Models\Role;
use App\NewEidikotita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\Traits\TraitNotification;
use Pasifai\Pysde\controllers\traits\VacuumTrait;
use Pasifai\Pysde\models\Division;
use Pasifai\Pysde\notifications\SchoolNotification;
use Auth;

class OtherSchoolsVacuumController extends Controller
{
    use VacuumTrait;

    public function __construct()
    {
        $this->middleware('isOther');
    }

    public function getVacuumsForLyceum()
    {
        return $this->getVacuums();
    }

    public function ajaxGetVacuumsOfSchool()
    {
        return $this->ajaxGetVacuumsForSchools();
    }

    public function getDivisionsForOtherSchool()
    {       
        $not_all_checked = false;

        $user = Auth::user();
        $organikes = $user->userable->organikes;

        foreach($organikes as $organiki){
            if(!$organiki->checked){
                $not_all_checked = true;
            }
        }

        if(!config('requests.can_view_organika_kena'))
            abort(403);

        if($not_all_checked)
            abort(403);
            
        return view('pysde::school.other_divisions');
    }

}
