<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use Pasifai\Pysde\models\Lesson;
use App\School;

class AssignmentsController extends Controller
{

    /**
     * This Controller is under Pysde Role
     */
    public function __construct()
    {
        $this->middleware('pysde');
    }

    public function getAssignmentsForGymnasiums()
    {
        $school = School::find(24);

        $divisions = $school->organikes->first()->teacher;
        
        $lessons = Lesson::with('wrologio')->get();

        return view('pysde::assignments.gymnasium.index', compact('lessons'));
    }

}
