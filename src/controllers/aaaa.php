<?php
class A {
    public function approveRecall($unique_id)
    {
        $request = RequestTeacherYperarithmia::with('teacher')->where('unique_id', $unique_id)->first();
    
        if($request != null){
            $user = $request->teacher->user;
            $teacher_folder = str_slug($user->last_name) . md5($request->teacher->id);
            $file_path = $request->file_name;
            $myschool = $request->teacher->myschool;
    
            $request->recall_situation = 'Η ΑΙΤΗΣΗ ΑΝΑΚΛΗΘΗΚΕ';
            $request->save();
    
            $attributes = [
                'teacher_name'  => $user->full_name,
                'protocol_name' => $request->protocol_name,
                'myschool'           => $myschool,
                'new_klados'    => $myschool->new_klados,
                'new_eidikotita_name'   => $myschool->new_eidikotita_name,
                'want_yperarithmia' => $request->want_yperarithmia,
                'organiki_name' => $myschool->organiki_name,
                'date_request'  => $request->date_request,
                'recall_stamp'  => $request->recall_situation
            ];
    
            \Storage::delete('teachers/'. $teacher_folder . $file_path);
    
            $this->makePDFforRequests($attributes, $teacher_folder, $file_path, 'pysde::aitisis.organika.teacher.pdf.YP_PDF', 'approvedRecall',$user->sch_mail, $user->full_name);
    
            flash()->overlayS('Συγχαρητήρια!', 'Η αίτηση με αριθμό : <strong>'.$request->protocol_name.'</strong>.  ΑΝΑΚΛΗΘΗΚΕ με επιτυχία');
    
        }
    
        $organikaRequests = OrganikiRequest::with('teacher')->where('unique_id', $unique_id)->get();
    
        if(!$organikaRequests->isEmpty()){
            
            $protocol = ProtocolTeacher::find($organikaRequests->first()->protocol_number);
    
            if($protocol != null){  
                $first_request =  $organikaRequests->first();
                $teacher = $first_request->teacher;
                $user = $teacher->user;
    
                $teacher_folder = str_slug($user->last_name) . md5($organikaRequests->first()->teacher->id);
                $file_path = $organikaRequests->first()->file_name;      
                $myschool = $organikaRequests->first()->teacher->myschool;
    
                $recall_situation = 'Η ΑΙΤΗΣΗ ΑΝΑΚΛΗΘΗΚΕ';
    
                foreach($organikaRequests as $request){
                    $request->recall_situation = $recall_situation;
                    $request->save();
                }
    
                $attr = [
                    'request_teacher'   => $organikaRequests,
                    'protocol'          => $protocol,
                    'user'              => $user,
                    'teacher'           => $teacher,
                    'monimos'           => $teacher->teacherable,
                    'organiki_name'     => $teacher->teacherable->organiki_name,
                    'myschool'          => $teacher->myschool,
                    'recall_stamp'      => $recall_situation
                ];
    
                \Storage::delete('teachers/'. $teacher_folder . $file_path);
    
                $this->makePDFforRequests($attr, $teacher_folder, $file_path, 'pysde::aitisis.leitourgika.teacher.PDF.pdfOrganikaYperarithmos','organikaYperarithmos', $user->sch_mail, $user->fullname);
    
                flash()->error('', 'Η αίτηση με αριθμό :'.$first_request->protocol_name.' ΔΕΝ ανακλήθηκε');
            }else{
                flash()->error('Σφάλμα', 'με αριθμό 356');
            }
        }
    
        return $unique_id;
    }
}
