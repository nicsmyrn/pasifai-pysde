<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use App\NewEidikotita;
use Auth;
use App\School;
use Illuminate\Support\Facades\Route;
use Pasifai\Pysde\controllers\ExcelExports\LeitourgikaExport;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\MySchoolTeacher;
use Illuminate\Http\Request;

use Pasifai\Pysde\controllers\traits\LeitourgikaTrait;
use App\Http\Controllers\Traits\TraitNotification;


class LeitourgikaController extends Controller
{

    use LeitourgikaTrait, TraitNotification;
    
    protected $user;

    protected $organikes;
    protected $eidikotites;
    protected $databaseProjects;

    /**
     * This Controller is under Pysde Role
     */
    public function __construct()
    {
        $this->middleware('isPysde');
    }

    public function getMeiwsi()
    {
        return view('pysde::leitourgika.meiwsi');
    }

    public function postMeiwsi(Request $request)
    {
        if($request->hasFile('csv_file')){
            $real_path =  $request->file('csv_file')->getRealPath();
            $mime =  $request->file('csv_file')->getMimeType();
//            Storage::put('schools/'.str_slug(\Auth::user()->userable->name).'/myschool.csv', file_get_contents($real_path));

            $this->executeUpdateMeiwsi($real_path);
        }else{
            flash()->error('Δεν έχετε επιλέξει το σωστό csv αρχείο');
        }
        return redirect()->back();
    }


    public function getUpdateDieuthides()
    {
        return view('pysde::leitourgika.update-dieuthides');
    }

    public function postUpdateDieuthides(Request $request)
    {
        if($request->hasFile('csv_file')){
            $real_path =  $request->file('csv_file')->getRealPath();
            $mime =  $request->file('csv_file')->getMimeType();
//            Storage::put('schools/'.str_slug(\Auth::user()->userable->name).'/myschool.csv', file_get_contents($real_path));

            $this->executeUpdateLeitourgikaDieuthides($real_path);
        }else{
            flash()->error('Δεν έχετε επιλέξει το σωστό csv αρχείο');
        }
        return redirect()->back();
    }

    public function getUpdateLeitourgika()
    {
        return view('pysde::leitourgika.update');
    }

    public function postUpdateLeitourgika(Request $request)
    {
        if($request->hasFile('csv_file')){
            $real_path =  $request->file('csv_file')->getRealPath();
            $mime =  $request->file('csv_file')->getMimeType();
//            Storage::put('schools/'.str_slug(\Auth::user()->userable->name).'/myschool.csv', file_get_contents($real_path));

            $this->executeUpdateLeitourgikaApousies($real_path);
        }else{
            flash()->error('Δεν έχετε επιλέξει το σωστό csv αρχείο');
        }
        return redirect()->back();

    }

    public function getDiathesi()
    {
        return MySchoolTeacher::with(['newOrganiki' => function($q){
            $q->where('school_id', null);
        }])
            ->where('am', '<>', '')
            ->where('topothetisi', 'like', '%Διάθεση%')
            ->where('yadescription', '<>', 'ΕΙΔΙΚΗΣ')
            ->get(['last_name', 'first_name', 'am', 'eidikotita'])
            ->filter(function($m){
                return $m->newOrganiki != null;
        });
    }

    public function getManageSchools(Request $request, School $school)
    {             
        $notification_id = $request->get('id');

        $this->checkIfRead($notification_id);

        return view('pysde::leitourgika.schools', compact('school'));
    }

    public function getManageTeachers(School $school)
    {
        return view('pysde::leitourgika.teachers', compact('school'));
    }

    public function getIndexTables()
    {
        return 'Hello World!!';
    }

    protected function getUser()
    {
        $this->user = Auth::user();
    }

    public function getExcel()
    {
        $date = Carbon::now()->timestamp;        

        return Excel::download(new LeitourgikaExport(), 'ΛΕΙΤΟΥΡΓΙΚΑ_ΚΕΝΑ_'.$date.'.xlsx');

    }
}
