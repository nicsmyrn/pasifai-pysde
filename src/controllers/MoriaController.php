<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use Auth;
use Pasifai\Pysde\models\OrganikiRequest;
use App\Teacher;
use App\Http\Controllers\Traits\MoriaCalculator;
use Illuminate\Support\Facades\DB;

class MoriaController extends Controller
{

    use MoriaCalculator;

    protected $user;

    public function __construct()
    {

    }

    public function calculateMoria()
    {        
        return view('pysde::moria.manual');
    }

    public function prepareCalculateApospasis()
    {        
        $requests = OrganikiRequest::where('protocol_number', '<>', 'null')
            ->whereIn('situation', ['E3', 'E4'])
            // ->groupBy('teacher_id')
            ->get();

        return view('pysde::moria.autocalculate', compact('requests'));
    }

    public function calculateApospasis()
    {
        $all_teachers = Teacher::all();
        
        foreach($all_teachers as $teacher){
            
            if($teacher->teacherable_type == 'App\Monimos'){
                $monimos = $teacher->teacherable;

                if($monimos->organiki == 2000){
                    $edata = $teacher->myschool->edata;
                    $monimos->moria_apospasis = $edata->moria_apospasis;
                    $monimos->save();

                    $teacher->dimos_entopiotitas = $edata->dimos_entopiotitas;
                    $teacher->dimos_sinipiretisis = $edata->dimos_sinipiretisis;
                    $teacher->save();
                }

                // $sum = $this->getMoriaApospasis($teacher);

                // $teacher->teacherable->moria_apospasis = round($sum,3);
                // $teacher->teacherable->save();
            }
        }

        flash()->success('Επιτυχία', 'Ο υπολογισμός των μορίων απόσπασης πραγματοποιήθηκε');

        return redirect()->back();

    }

    protected function getUser()
    {
        $this->user = Auth::user();
    }

}
