<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Pasifai\Pysde\controllers\ExcelExports\ApospasiE3byEidikotita;

class ApospasiE3 implements WithMultipleSheets
{
    protected $eidikotites;

    public function __construct($eidikotites)
    {
        $this->eidikotites = $eidikotites;
    }

    public function sheets(): array
    {
        $sheets = [];

        foreach($this->eidikotites as $key=>$eidikotita){
            $sheets[$key] = new ApospasiE3byEidikotita($eidikotita, $key);
        }
        
        return $sheets;
    }
}