<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithTitle;
use Log;

class SheetByEidikotita implements FromView, ShouldAutoSize, WithEvents, WithTitle
{
    protected $collector;
    protected $eidikotita_name;
    protected $type;
    protected $title;

    public function __construct($collection, $key, $type)
    {
        $this->type = $type;
        $this->collector = $collection;    
        $this->eidikotita_name = $key;
    }

    public function view(): View
    {
        $requests = $this->collector;

        $type = $this->type;
        return view('pysde::vacuum.excel.organikes_yperarithmoi', compact('requests', 'type'));
    }

    public function title(): string
    {
        return $this->eidikotita_name;
    }

    public function registerEvents(): array
    {        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                
                $this->createCellPrefrence($this->collector, $event);

                $lastColumnLetter = $event->sheet->getDelegate()->getHighestColumn();

                $event->sheet->getDelegate()->getStyle('A2:'.$lastColumnLetter.($this->collector->count()+1))->applyFromArray($this->getGeneralStyles());
                        
                $event->sheet->getDelegate()->getStyle("A1:{$lastColumnLetter}1")->applyFromArray($this->getHeaderStyle());

                $event->sheet->getDelegate()->getStyle('A2:B'.($this->collector->count()+1))->applyFromArray($this->getMetaDataStyles());
                $event->sheet->getDelegate()->getStyle('D2:N'.($this->collector->count()+1))->applyFromArray($this->getMetaDataStyles());
                $event->sheet->getDelegate()->getStyle('C2:D'.($this->collector->count()+1))->applyFromArray($this->getTeacherInformation());
                $event->sheet->getDelegate()->getStyle('F2:F'.($this->collector->count()+1))->applyFromArray($this->getSchoolInformation());

                foreach(range('A', $lastColumnLetter) as $letter){
                    if(in_array($letter, ["D", "F"])){
                        $event->sheet->getDelegate()->getColumnDimension($letter)->setWidth(20);

                    }elseif($letter == "C"){
                        $event->sheet->getDelegate()->getColumnDimension($letter)->setWidth(25);
                        // a
                    }else{
                        $event->sheet->getDelegate()->getColumnDimension($letter)->setAutoSize(true);
                    }
                }

                if($this->type == 'Βελτιώση - Οριστική Τοποθέτηση'){
                    $range_letter = 'K';
                }else{
                    $range_letter = 'L';
                }

                $event->sheet->getDelegate()->getStyle("{$range_letter}2:".$lastColumnLetter.($this->collector->count()+1))->applyFromArray($this->getProtimisisStyle());

                if($this->type == 'Βελτιώση - Οριστική Τοποθέτηση'){
                    $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('&C&H&25 ΑΙΤΗΣΕΙΣ ΓΙΑ ΒΕΛΤΙΩΣΗ - ΟΡΙΣΤΙΚΗ ΤΟΠΟΘΕΤΗΣΗ ΚΛΑΔΟΣ:['. $this->eidikotita_name .']   ' .Carbon::now()->year);
                }else{
                    $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('ΑΙΤΗΣΕΙΣ ΟΝΟΜΑΣΤΙΚΑ ΥΠΕΡΑΡΙΘΜΩΝ ΚΛΑΔΟΣ: ['. $this->eidikotita_name . '] '. Carbon::now()->year);
                }

                $event->sheet->getDelegate()->getHeaderFooter()->setOddFooter('&L &C&H&18 Συντάχθηκε από τη Γραμματεία του ΠΥΣΔΕ στις '. Carbon::now()->format('d-m-Y στις H:i:s'). '&RΣελίδα &P από &N');
                $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->freezePane('D2');

                //something good

                $event->sheet->getDelegate()->getPageMargins()->setTop(1);
                $event->sheet->getDelegate()->getPageMargins()->setRight(0);
                $event->sheet->getDelegate()->getPageMargins()->setLeft(0);
                $event->sheet->getDelegate()->getPageMargins()->setBottom(0);

                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getPageSetup()->setFitToHeight(0);
            }
        ];
    }

    private function createCellPrefrence($requests, $event)
    {

        $i = 2;
        $columnNumber = 0;

        $prefrences_offset = config('requests.prefrences_offset');
        
        foreach($requests as $request){
            if($this->type == 'Βελτιώση - Οριστική Τοποθέτηση'){
                $columnNumber = 11;            
            }else{
                $columnNumber = 12;            
            }
            foreach($request["prefrences"]->chunk($prefrences_offset) as $k=>$prefCell){                

                if($k == 0){
                    $max_value = $prefrences_offset;
                    $min_value = $max_value - ($prefrences_offset - 1);
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, 1, "Προτιμήσεις $min_value-$max_value");
                }elseif($k == 1){
                    $max_value = $prefrences_offset * 2;
                    $min_value = $max_value - ($prefrences_offset - 1);
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, 1, "Προτιμήσεις $min_value-$max_value");
                }elseif($k == 2){
                    $max_value = $prefrences_offset;
                    $min_value = $max_value - ($prefrences_offset - 1);
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, 1, "Προτιμήσεις $min_value-$max_value");
                }

                $cell = '';
                foreach($prefCell as $prefrence){
                    $moria_entopiotitas = 0;
                    $moria_sinipiretisis = 0;

                    if ($prefrence->OrganikiGroup == $request->teacher->dimos_entopiotitas && $request->teacher->dimos_entopiotitas > 0){
                        $moria_entopiotitas = 4;
                    }


                    if ($prefrence->OrganikiGroup == $request->teacher->dimos_sinipiretisis && $request->teacher->dimos_sinipiretisis > 0){
                        $moria_sinipiretisis = 4;
                    }

                    $total_moria = floatval($request['moria']) + $moria_entopiotitas + $moria_sinipiretisis;
      

                    $cell = $cell . "($prefrence->order_number). [{$total_moria}] $prefrence->OrganikiName\n";
                }

                $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, $i, $cell);

                $lastColumnLetter = $event->sheet->getDelegate()->getHighestColumn();


                if($this->type == 'Βελτιώση - Οριστική Τοποθέτηση'){
                    $range_letter = 'K';
                }else{
                    $range_letter = 'L';
                }

                foreach(range($range_letter, $lastColumnLetter) as $letter){
                    $event->sheet->getDelegate()->getStyle("$letter$i")->getAlignment()->setWrapText(true);
                    $event->sheet->getDelegate()->getStyle("$letter$i")->getFont()->setSize(14);
                }
                $columnNumber = $columnNumber + 1;

            }

            $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, $i, $request['description']);
            
            $i = $i + 1;
        }
        $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, 1, 'Παρατηρήσεις');

    }

    protected function getMetaDataStyles() : array
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]           
        ];
    }

    protected function getTeacherInformation() : array
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'indent'    => 1,
                'wrapText'  => true
            ]               
        ];
    }

    protected function getSchoolInformation() : array 
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'indent'    => 1,
                'wrapText'  => true
            ]               
        ];
    }

    protected function getGeneralStyles() : array
    {
        return [
            'font' =>[
                'name' => 'Calibri',
                'size'  => 16, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                // 'wrapText'  => true,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ]              
        ];
    }

    protected function getProtimisisStyle() : array
    {
        return [
            'font' =>[
                'name' => 'Calibri',
                'size'  => 16, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'indent'    => 1,
                'wrapText'  => true
            ]              
        ];
    }

    protected function getNameSchoolStyle() : array
    {
        return [
            'font' =>[
                'name' => 'Calibri',
                'size'  => 16, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'indent'    => 1,
                'wrapText'  => true
            ]              
        ];
    }

    protected function getHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],   
            'font' =>[
                'name' => 'Calibri',
                'size'  => 16, 
                'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ] 
            ],
            'alignment' => [
                'wrapText'  => true,
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ]
            ],      
        ];
    }

}