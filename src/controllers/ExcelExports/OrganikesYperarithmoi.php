<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OrganikesYperarithmoi implements WithMultipleSheets
{
    protected $eidikotites;
    protected $type;

    public function __construct($eidikotites, $type = null)
    {
        $this->eidikotites = $eidikotites;
        $this->type = $type;
    }

    public function sheets(): array
    {
        $sheets = [];

        foreach($this->eidikotites as $key=>$eidikotita){
            $sheets[$key] = new SheetByEidikotita($eidikotita, $key, $this->type);
        }
        
        return $sheets;
    }
}