<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class LeitourgikaExport implements WithMultipleSheets
{

    public function sheets(): array
    {        
        return [
            '1' => new LeitourgikaSheetGroup1(),
            '2' => new LeitourgikaSheetGroup2()
        ];
    }
}