<?php

namespace Pasifai\Pysde\controllers\ExcelExports\Placements;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;
use Log;
use Maatwebsite\Excel\Concerns\WithTitle;

class SheetsByOrganikes implements FromView, ShouldAutoSize, WithEvents, WithTitle
{
    protected $collector;
    protected $title;
    protected $title_praxi;

    public function __construct($collection, $key, $title_praxi)
    {
        $this->collector = $collection;    
        $this->title = $key;
        $this->title_praxi = $title_praxi;
    }

    public function title(): string
    {                
        return $this->title;
    }

    public function view(): View
    {
        $teachers = $this->collector;
        $key = $this->title;

        Log::warning('Teachers');
        Log::warning($teachers);

        return view('pysde::leitourgika.excel.organikes', compact('teachers', 'key'));
    }

    public function registerEvents(): array
    {        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                         
                $lastColumnLetter = $event->sheet->getDelegate()->getHighestColumn();

                $event->sheet->getDelegate()->getStyle("A1:{$lastColumnLetter}1")->applyFromArray($this->getHeaderStyle());
                $event->sheet->getDelegate()->getStyle('A2:'.$lastColumnLetter.($this->collector->count()+1))->applyFromArray($this->getGeneralStyles());

                $event->sheet->getDelegate()->getStyle('A2:A'.($this->collector->count()+1))->applyFromArray($this->getMetaDataStyles());
                $event->sheet->getDelegate()->getStyle('C2:N'.($this->collector->count()+1))->applyFromArray($this->getMetaDataStyles());
                $event->sheet->getDelegate()->getStyle('A2:B'.($this->collector->count()+1))->applyFromArray($this->getTeacherInformation());

                for($i=2; $i<=$this->collector->count()+1; $i++){
                    $event->sheet->getDelegate()->getRowDimension($i)->setRowHeight(28);
                }

                $this->createCellTopothetisis($this->collector, $event, $this->title);

                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('ΤΟΠΟΘΕΤΗΣΕΙΣ ΕΚΠΑΙΔΕΥΤΙΚΩΝ '. $this->title . ' ΠΡΑΞΗ: '. $this->title_praxi);
                $event->sheet->getDelegate()->getHeaderFooter()->setOddFooter('&L Συντάχθηκε από τη Γραμματεία του ΠΥΣΔΕ στις '. Carbon::now()->format('d/m/Y H:i:s'));

                $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->freezePane('C2');

                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getPageSetup()->setFitToHeight(0);

                $event->sheet->getDelegate()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1,1);
            }
        ];
    }

    protected function getMetaDataStyles() : array
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]           
        ];
    }

    protected function getTeacherInformation() : array
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'indent'    => 1
            ]               
        ];
    }

    protected function getTopothetisisInformation() : array
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'indent'    => 1
            ]               
        ];
    }

    protected function getGeneralStyles() : array
    {
        return [
            'font' =>[
                'name' => 'Calibri',
                'size'  => 15, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'wrapText'  => true,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ]              
        ];
    }

    protected function getHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],   
            'font' =>[
                'name' => 'Calibri',
                'size'  => 14, 
                'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ] 
            ],
            'alignment' => [
                'wrapText'  => true,
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ]
            ],      
        ];
    }


    private function createCellTopothetisis($teachers, $event, $key)
    {
        switch($key){
            case 'Λειτ. Τοποθέτηση & Συμπλήρωση':
                $topothetisiColumn = "I";
                $diathesiColumn = "J";
                $descriptionColumn = "K";

                $i = 2;
                
                foreach($teachers as $teacher){
                    $cellDiathesis = '';
                    $cellTopothetisis = '';
                    $cellDescription = '';

                    foreach($teacher['placements'] as $index=>$placement){
                        $days = $placement->days > 0 ? '/'.$placement->days.'Ημ' : '';
                        $days = $placement->days == 5 ? ' εξ ολοκλήρου': $days;
                        if(in_array($placement->placements_type, $this->placementsStringTypes['topothetisi'])){
                            $withOutRequest = $placement->me_aitisi ? '' : ' [χωρίς αίτηση]';
                            if($placement->days < 5){
                                $cellTopothetisis = $placement->to . ' για '. $placement->hours . 'Ωρ' . $days . $withOutRequest;
                            }else{
                                $cellTopothetisis = $placement->to . ' εξ ολοκλήρου' . $withOutRequest;
                            }
                            $cellDescription = $cellDescription . "$placement->description\n";
                        }elseif(in_array($placement->placements_type, $this->placementsStringTypes['diathesi'])){
                            if($teacher['placements']->last()->id == $placement->id){
                                $withOutRequest = $placement->me_aitisi ? '' : ' [χωρίς αίτηση]';
                                $cellDiathesis = $cellDiathesis . "- $placement->to για $placement->hours Ωρ $days $withOutRequest";
                                $cellDescription = $cellDescription . "$placement->description";
                            }else{
                                $withOutRequest = $placement->me_aitisi ? '' : ' [χωρίς αίτηση]';
                                $cellDiathesis = $cellDiathesis . "- $placement->to για $placement->hours Ωρ $days $withOutRequest\n";
                                $cellDescription = $cellDescription . "$placement->description\n";
                            }
                        }
                    }
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($topothetisiColumn), $i, $cellTopothetisis);
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($diathesiColumn), $i, $cellDiathesis);
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($descriptionColumn), $i, $cellDescription);

                    $i++;
                }
                $event->sheet->getDelegate()->getStyle("{$topothetisiColumn}2:$descriptionColumn$i".($this->collector->count()+1))->applyFromArray($this->getTopothetisisInformation());

                break;
            case 'Λειτ. Τοποθέτηση':
                $topothetisiColumn = "I";
                $descriptionColumn = "J";
                
                $i = 2;

                foreach($teachers as $teacher){
                    $cellTopothetisis = '';
                    $cellDescription = '';

                    foreach($teacher['placements'] as $index=>$placement){
                        $days = $placement->days > 0 ? '/'.$placement->days.'Ημ' : '';
                        if(in_array($placement->placements_type, $this->placementsStringTypes['topothetisi'])){
                            $withOutRequest = $placement->me_aitisi ? '' : ' [χωρίς αίτηση]';
                            if($placement->days < 5){
                                $cellTopothetisis = $placement->to . ' για '. $placement->hours . 'Ωρ' . $days . $withOutRequest;
                            }else{
                                $cellTopothetisis = $placement->to . ' εξ ολοκλήρου' . $withOutRequest;
                            }
                            $cellDescription = $cellDescription . "$placement->description";
                        }
                    }
                    
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($topothetisiColumn), $i, $cellTopothetisis);
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($descriptionColumn), $i, $cellDescription);

                    $i = $i + 1;
                }
                $event->sheet->getDelegate()->getStyle("{$topothetisiColumn}2:$descriptionColumn$i".($this->collector->count()+1))->applyFromArray($this->getTopothetisisInformation());

                break;
            case 'Συμπλήρωση Ωραρίου':
                $diathesiColumn = "I";
                $descriptionColumn = "J";

                $i = 2;      

                foreach($teachers as $teacher){
                    $cellDiathesis = '';
                    $cellDescription = '';

                    $insideRows = 1;

                    foreach($teacher['placements'] as $index=>$placement){                                            
                        $days = $placement->days > 0 ? '/'.$placement->days.'Ημ' : '';
                        if(in_array($placement->placements_type, $this->placementsStringTypes['diathesi'])){                            
                            if($teacher['placements']->last()->id == $placement->id){
                                $withOutRequest = $placement->me_aitisi ? '' : ' [χωρίς αίτηση]';
                                $cellDiathesis = $cellDiathesis . "- $placement->to για $placement->hours Ωρ $days $withOutRequest";
                                $cellDescription = $cellDescription . "$placement->description";
                            }else{
                                $withOutRequest = $placement->me_aitisi ? '' : ' [χωρίς αίτηση]';
                                $cellDiathesis = $cellDiathesis . "- $placement->to για $placement->hours Ωρ $days $withOutRequest\n";
                                $cellDescription = $cellDescription . "$placement->description\n";
                            }
                            $insideRows++;
                        }
                    }

                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($diathesiColumn), $i, $cellDiathesis);
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($descriptionColumn), $i, $cellDescription);

                    $event->sheet->getDelegate()->getRowDimension($i)->setRowHeight(28*$insideRows);
                    $i++;
                }

                // $event->sheet->getDelegate()->getStyle("{$diathesiColumn}2:$descriptionColumn$i".($this->collector->count()+1))->applyFromArray($this->getTopothetisisInformation());

                break;
            case 'Απόσπαση':
                $apospasiColumn = "I";
                $descriptionColumn = "J";

                $i = 2;
                
                foreach($teachers as $teacher){
                    $cellApospasis = '';
                    $cellDescription = '';

                    foreach($teacher['placements'] as $index=>$placement){
                        $days = $placement->days > 0 ? '/'.$placement->days.'Ημ' : '';
                        if(in_array($placement->placements_type, $this->placementsStringTypes['apospasi'])){
                            $withOutRequest = $placement->me_aitisi ? '' : ' [χωρίς αίτηση]';
                            if($placement->days < 5){
                                $cellApospasis = $placement->to . ' για '. $placement->hours . 'Ωρ' . $days . $withOutRequest;
                            }else{
                                $cellApospasis = $placement->to . ' εξ ολοκλήρου' . $withOutRequest;
                            }
                            $cellDescription = $cellDescription . "$placement->description";
                        }
                    }

                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($apospasiColumn), $i, $cellApospasis);
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($descriptionColumn), $i, $cellDescription);

                    $i = $i + 1;
                }

                $event->sheet->getDelegate()->getStyle("{$apospasiColumn}2:$descriptionColumn$i".($this->collector->count()+1))->applyFromArray($this->getTopothetisisInformation());

                break;
            case 'Ανάκληση':
                $anaklisiColumn = "I";
                $descriptionColumn = "J";

                $i = 2;
                
                foreach($teachers as $teacher){
                    $cellAnaklisis = '';
                    $cellDescription = '';

                    foreach($teacher['placements'] as $index=>$placement){
                        if(in_array($placement->placements_type, $this->placementsStringTypes['anaklisi'])){
                            $withOutRequest = $placement->me_aitisi ? '' : ' [χωρίς αίτηση]';
                            $cellAnaklisis = 'ανάκληση από ' . $placement->to . $withOutRequest;
                            $cellDescription = "$placement->description";
                        }
                    }

                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($anaklisiColumn), $i, $cellAnaklisis);
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($this->getColumnNumber($descriptionColumn), $i, $cellDescription);

                    $i = $i + 1;
                }
                $event->sheet->getDelegate()->getStyle("{$anaklisiColumn}2:$descriptionColumn$i".($this->collector->count()+1))->applyFromArray($this->getTopothetisisInformation());

                break;
            case 'Πθμια':
                //commands
                break;
        }                
    }

    protected function getColumnNumber($columnLetter){
        return  \PHPOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($columnLetter);
    }


}