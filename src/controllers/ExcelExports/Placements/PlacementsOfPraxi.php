<?php

namespace Pasifai\Pysde\controllers\ExcelExports\Placements;

use Log;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

use Pasifai\Pysde\controllers\ExcelExports\Placements\SheetsByPlacementType;


class PlacementsOfPraxi implements WithMultipleSheets
{
    protected $placement_sheets;
    protected $title_praxi;

    public function __construct($placementSheets, $title_praxi)
    {
        $this->placement_sheets = $placementSheets;
        $this->title_praxi = $title_praxi;
    }

    public function sheets(): array
    {                
        $sheets = [];

        foreach($this->placement_sheets as $key=>$sheet){   
            $sheets[] = new SheetsByPlacementType($sheet, $key, $this->title_praxi);
        }
        return $sheets;
    }
}