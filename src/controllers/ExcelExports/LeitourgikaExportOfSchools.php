<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class LeitourgikaExportOfSchools implements WithMultipleSheets
{

    public function sheets(): array
    {        
        return [
            '1' => new LeitourgikaSheetGroup1schools(),
            '2' => new LeitourgikaSheetGroup2schools()
        ];
    }
}