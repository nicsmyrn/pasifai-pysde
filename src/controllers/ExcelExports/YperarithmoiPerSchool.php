<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;

class YperarithmoiPerSchool implements FromView, WithEvents, ShouldAutoSize
{
    protected $collector;

    public function __construct($collection)
    {
        $this->collector = $collection;    
    }

    public function view(): View
    {
        return view('pysde::vacuum.excel.yperarithmoi', [
            'groups' => $this->collector
        ]);
    }

    public function registerEvents(): array
    {        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $group_start = 2;
                $school_start = 2;
                $counter = 2;

                foreach($this->collector as $key=>$group){
                    foreach($group as $k=>$school){
                        $counter = $counter + $school->count();

                        $school_end = $counter - 1;
                        $event->sheet->getDelegate()->mergeCells("B$school_start:B$school_end");
                        $event->sheet->getDelegate()->getStyle("B$school_start:B$school_end")->applyFromArray($this->getSchoolsStyle());

                        $school_start = $counter;
                    }
                    $group_end = $counter - 1; 
                    $event->sheet->getDelegate()->mergeCells("A$group_start:A$group_end");
                    $event->sheet->getDelegate()->getStyle("A$group_start:A$group_end")->applyFromArray($this->getGroupStyle());

                    $event->sheet->getDelegate()->getStyle("A$group_start:F$group_end")->applyFromArray($this->getGroupBordersStyle());

                    $group_start = $counter;
                }           
                $event->sheet->getDelegate()->getStyle('C2:F'.($counter-1))->applyFromArray($this->getTeachersStyle());
                $event->sheet->getDelegate()->getStyle('A1:F1')->applyFromArray($this->getHeaderStyle());
    
                $event->sheet->getDelegate()->setAutoFilter('A1:F1');

                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('ΟΝΟΜΑΣΤΙΚΑ ΥΠΕΡΑΡΙΘΜΟΙ '.Carbon::now()->year);
                $event->sheet->getDelegate()->getHeaderFooter()->setOddFooter('&L Συντάχθηκε από τη Γραμματεία του ΠΥΣΔΕ στις &D &T');
                $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->freezePane('A2');

                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getPageSetup()->setFitToHeight(0);
            }
        ];
    }

    protected function getGroupStyle() : array
    {
        return [
            'font' =>[
                'name' => 'Calibri',
                'size'  => 22, 
                'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'wrapText'  => true,
                'textRotation' => 90, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]              
        ];
    }

    protected function getGroupBordersStyle() : array
    {
        return [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                ],
                'inside' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ]         
        ];
    }

    protected function getSchoolsStyle() : array
    {
        return [
            'font' =>[
                'name' => 'Calibri',
                'size'  => 14, 
                'bold' => FALSE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
            ]      
        ];
    }

    protected function getTeachersStyle() : array
    {
        return [
            'font' =>[
                'name' => 'Calibri',
                'size'  => 14, 
                'bold' => FALSE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'indent'    => 1
            ]             
        ];
    }

    protected function getHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],   
            'font' =>[
                'name' => 'Calibri',
                'size'  => 18, 
                'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]             
        ];
    }

}