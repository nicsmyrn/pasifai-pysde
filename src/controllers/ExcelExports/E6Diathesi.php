<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Pasifai\Pysde\controllers\ExcelExports\LeitourgikaSheetByEidikotita;

class E6Diathesi implements WithMultipleSheets
{
    protected $eidikotites;

    public function __construct($eidikotites)
    {
        $this->eidikotites = $eidikotites;
    }

    public function sheets(): array
    {
        $sheets = [];

        foreach($this->eidikotites as $key=>$eidikotita){            
            $sheets[$key] = new E6NeodioristoiSheetByEidikotita($eidikotita, $key);
        }
        
        return $sheets;
    }
}