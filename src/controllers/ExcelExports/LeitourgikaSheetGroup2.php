<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use App\NewEidikotita;
use App\School;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Pasifai\Pysde\controllers\traits\ExcelTrait;

class LeitourgikaSheetGroup2 implements FromView, WithEvents
{
    use ExcelTrait;

    protected $header;
    protected $last_cell_row;
    protected $eidikotites;


    public function view(): View
    {
        $this->getHeaders();
        $this->getEidikotites();
        $this->getLastCellNumber();

        return view('pysde::vacuum.excel.group2_leitourgika', [
            'header'   => $this->header,
            'eidikotites'   => $this->eidikotites
        ]);
    }
    
    protected function getHeaders()
    {
        $this->header = School::with('leitourgika_kena')->where('group', '<>',1)->where('type','<>', 'eidiko')->orderBy('order')->get();

    }

    protected function getEidikotites()
    {
        $this->eidikotites = NewEidikotita::with('leitourgika_kena')->has('leitourgika_kena')->orderBy('order')->get();

    }

    protected function getLastCellNumber()
    {
        $this->last_cell_row = $this->eidikotites->count() + 2;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $date = Carbon::now();
                
                $last_column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($this->header->count() + 2);
                
                $masterHeaderRange = 'C1:X1';
                $headerRange = 'C2:'.$last_column.'2'; 
                $slugRange = 'A3:A'. $this->last_cell_row;
                $nameRange = 'B3:B'. $this->last_cell_row;
                $bodyRange = 'C3:'.$last_column . $this->last_cell_row;

                $event->sheet->getDelegate()->getStyle($masterHeaderRange)->applyFromArray($this->getMasterHeaderStyle());
                $event->sheet->getDelegate()->getStyle($headerRange)->applyFromArray($this->getHeaderStyle());
                $event->sheet->getDelegate()->getStyle($slugRange)->applyFromArray($this->getRowSlugStyle());
                $event->sheet->getDelegate()->getStyle($nameRange)->applyFromArray($this->getRowNameStyle());

                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(50);
                $event->sheet->getDelegate()->getRowDimension(2)->setRowHeight(150);

                for($i=3; $i<=$this->last_cell_row; $i++){
                    $event->sheet->getDelegate()->getRowDimension($i)->setRowHeight(30);
                }

                $event->sheet->getDelegate()->getColumnDimension('A')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('B')->setAutoSize(true);

                $event->sheet->getDelegate()->getStyle('A1:B1')->getFont()->setSize(16);
                $event->sheet->getDelegate()->getStyle('A1:B1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('A1:B1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $event->sheet->getDelegate()->getStyle($bodyRange)->applyFromArray($this->getBodyStyle());

                $event->sheet->getDelegate()->getStyle($bodyRange)->setConditionalStyles($this->setConditionalStylesArray());

                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('&C&H&25 ΟΜΑΔΕΣ 2,3,4,5,6 -  ΠΙΝΑΚΑΣ ΛΕΙΤΟΥΡΓΙΚΩΝ ΚΕΝΩΝ: ');
                $event->sheet->getDelegate()->getHeaderFooter()->setOddFooter('&L &C&H&18 Συντάχθηκε από τη Γραμματεία του ΠΥΣΔΕ στις '. Carbon::now()->format('d-m-Y στις H:i:s'). '&RΣελίδα &P από &N');
                
                $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->freezePane('B3');

                $event->sheet->getDelegate()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1,2);

                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getPageSetup()->setFitToHeight(0);
                $event->sheet->getDelegate()->setTitle('Ομάδες 2,3,4,5,6');
               
                $this->pe04CalculateForLeitourgika($this->eidikotites, $this->header, $event, 3);
                $this->pe81CalculateForLeitourgika($this->eidikotites, $this->header, $event, 3);

                $this->getTeachersLeaveOfAbsence([2,3,4,5,6], $this->last_cell_row, $event);
            },
        ];
    }

    protected function getMasterHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 20, 
                'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
            ]          
        ];       
    }

    protected function getHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 16, 
                // 'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'wrapText'  => true,
                'textRotation' => 90, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
            ]          
        ];
    }

    protected function getBodyStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 18, 
                // 'bold' => TRUE, 
                // 'color' => [ 'rgb' => '000000' ]   
            ],            
        ];
    }

    protected function getRowSlugStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 14, 
                // 'bold' => TRUE, 
                // 'color' => [ 'rgb' => '000000' ]   
            ],  
        ];
    }

    protected function getRowNameStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ]
            ],
            'font' =>[
                'name' => 'Arial',
                'size'  => 9, 
                // 'bold' => TRUE, 
                // 'color' => [ 'rgb' => '000000' ]   
            ],  
        ];
    }

    protected function setConditionalStylesArray() : array
    {
        $conditionalStyles = array();

        $conditional1 = new Conditional();

        $conditional1->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
        $conditional1->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_LESSTHAN);
        $conditional1->addCondition('0');
        $conditional1->getStyle()->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_DARKGREEN);
        $conditional1->getStyle()->getFont()->setBold(true);

        $conditional2 = new Conditional();
        $conditional2->setConditionType(\PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CELLIS);
        $conditional2->setOperatorType(\PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_GREATERTHANOREQUAL);
        $conditional2->addCondition('0');
        $conditional2->getStyle()->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $conditional2->getStyle()->getFont()->setBold(true);

        $conditionalStyles[] = $conditional1;
        $conditionalStyles[] = $conditional2;

        return $conditionalStyles;
    }

}

/*

