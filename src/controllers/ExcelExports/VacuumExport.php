<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class VacuumExport implements WithMultipleSheets
{

    public function sheets(): array
    {
        return [
            '1' => new SheetGroup1(),
            '2' => new SheetGroup2()
        ];
    }
}