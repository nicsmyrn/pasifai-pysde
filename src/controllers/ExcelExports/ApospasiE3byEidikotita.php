<?php

namespace Pasifai\Pysde\controllers\ExcelExports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithTitle;

class ApospasiE3byEidikotita implements FromView, ShouldAutoSize, WithEvents, WithTitle
{
    protected $collector;
    protected $title;
    // protected $column_number = 12;
    protected $protimisisColumnName = 'K';

    public function __construct($collection, $key)
    {
        $this->collector = $collection;    
        $this->title = $key;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function view(): View
    {
        $requests = $this->collector;

        return view('pysde::vacuum.excel.apospasi', compact('requests'));
    }

    public function registerEvents(): array
    {        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                
                $this->createCellPrefrence($this->collector, $event);
         
                $lastColumnLetter = $event->sheet->getDelegate()->getHighestColumn();

                $event->sheet->getDelegate()->getStyle('A2:'.$lastColumnLetter.($this->collector->count()+1))->applyFromArray($this->getGeneralStyles());

                $event->sheet->getDelegate()->getStyle("A1:{$lastColumnLetter}1")->applyFromArray($this->getHeaderStyle());

                // $event->sheet->getDelegate()->getRowDimension('1')->setRowHeight(60);

                $event->sheet->getDelegate()->getStyle('A2:A'.($this->collector->count()+1))->applyFromArray($this->getMetaDataStyles());
                $event->sheet->getDelegate()->getStyle('D2:Q'.($this->collector->count()+1))->applyFromArray($this->getMetaDataStyles());
                $event->sheet->getDelegate()->getStyle('B2:C'.($this->collector->count()+1))->applyFromArray($this->getTeacherInformation());
                $event->sheet->getDelegate()->getStyle('F2:F'.($this->collector->count()+1))->applyFromArray($this->getSchoolInformation());
                                
                // foreach(range($this->protimisisColumnName, $lastColumnLetter) as $letter){
                //     $event->sheet->getDelegate()->getColumnDimension($letter)->setAutoSize(true);
                //     // $event->sheet->getDelegate()->getColumnDimension($letter)->setWidth(40);
                // }


                foreach(range('A', $lastColumnLetter) as $letter){
                    if(in_array($letter, ["D", "F"])){
                        $event->sheet->getDelegate()->getColumnDimension($letter)->setWidth(20);

                    }elseif($letter == "C"){
                        $event->sheet->getDelegate()->getColumnDimension($letter)->setWidth(25);
                        // a
                    }elseif($letter == $lastColumnLetter){
                        $event->sheet->getDelegate()->getColumnDimension($letter)->setWidth(100);
                        // a
                    }else{
                        $event->sheet->getDelegate()->getColumnDimension($letter)->setAutoSize(true);
                    }
                }


                $event->sheet->getDelegate()->getStyle($this->protimisisColumnName.'2:'.$lastColumnLetter.($this->collector->count()+1))->applyFromArray($this->getProtimisisStyle());

                // $event->sheet->getDelegate()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1,1);

                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('&C&H&25 ΑΙΤΗΣΕΙΣ E3 '. $this->title .'   ' .Carbon::now()->year);
                $event->sheet->getDelegate()->getHeaderFooter()->setOddFooter('&L &C&H&18 Συντάχθηκε από τη Γραμματεία του ΠΥΣΔΕ στις '. Carbon::now()->format('d-m-Y στις H:i:s'). '&RΣελίδα &P από &N');
                $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                // $event->sheet->getDelegate()->freezePane('C2');
                $event->sheet->getDelegate()->freezePane('D2');

                $event->sheet->getDelegate()->getPageMargins()->setTop(1);
                $event->sheet->getDelegate()->getPageMargins()->setRight(0);
                $event->sheet->getDelegate()->getPageMargins()->setLeft(0);
                $event->sheet->getDelegate()->getPageMargins()->setBottom(0);

                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getPageSetup()->setFitToHeight(0);
            }
        ];
    }

    private function createCellPrefrence($requests, $event)
    {

        $i = 2;

        foreach($requests as $request){
            // $columnNumber = $this->column_number;
            $columnNumber = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($this->protimisisColumnName);

            foreach($request["prefrences"]->chunk(10) as $k=>$prefCell){                

                if($k == 0){
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, 1, 'Προτιμήσεις 1-10');
                }elseif($k == 1){
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, 1, 'Προτιμήσεις 11-20');
                }elseif($k == 2){
                    $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, 1, 'Προτιμήσεις 21-30');
                }

                $cell = '';
                foreach($prefCell as $prefrence){
                    $moria_entopiotitas = 0;
                    $moria_sinipiretisis = 0;


                    if ($prefrence->OrganikiGroup == $request->teacher->dimos_entopiotitas && $request->teacher->dimos_entopiotitas > 0){
                        $moria_entopiotitas = 4;
                    }


                    if ($prefrence->OrganikiGroup == $request->teacher->dimos_sinipiretisis && $request->teacher->dimos_sinipiretisis > 0){
                        $moria_sinipiretisis = 10;
                    }

                    $total_moria = floatval($request['moria_apospasis']) + $moria_entopiotitas + $moria_sinipiretisis;
      

                    $cell = $cell . "($prefrence->order_number). [{$total_moria}] $prefrence->OrganikiName\n";

                    // $cell = $cell . "($prefrence->order_number). $prefrence->OrganikiName\n";
                }

                $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, $i, $cell);

                $lastColumnLetter = $event->sheet->getDelegate()->getHighestColumn();

                foreach(range($this->protimisisColumnName, $lastColumnLetter) as $letter){
                    $event->sheet->getDelegate()->getStyle("$letter$i")->getAlignment()->setWrapText(true);
                    $event->sheet->getDelegate()->getStyle("$letter$i")->getFont()->setSize(14);
                }
                $columnNumber = $columnNumber + 1;

            }

            //FIX THIS
            $columnNumber = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString('M');

            $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, $i, $request['description']);

            // $last_column = \PHPOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($columnNumber);

            // $event->sheet->getDelegate()->getStyle("$last_column$i")->getAlignment()->setWrapText(true);

            $i = $i + 1;
        }
        $event->sheet->getDelegate()->setCellValueByColumnAndRow($columnNumber, 1, 'Παρατηρήσεις');

    }

    protected function getMetaDataStyles() : array
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]           
        ];
    }

    protected function getTeacherInformation() : array
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'indent'    => 1,
                'wrapText'  => true
            ]               
        ];
    }

    protected function getGeneralStyles() : array
    {
        return [
            'font' =>[
                'name' => 'Calibri',
                'size'  => 24, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                // 'wrapText'  => true,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ]              
        ];
    }

    protected function getProtimisisStyle() : array
    {
        return [
            'font' =>[
                'name' => 'Calibri',
                'size'  => 22, 
                'color' => [ 'rgb' => '000000' ]   
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'indent'    => 1,
                'wrapText'  => true
            ]              
        ];
    }

    protected function getSchoolInformation() : array 
    {
        return [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT, 
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 
                'indent'    => 1,
                'wrapText'  => true
            ]               
        ];
    }

    protected function getHeaderStyle() : array
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],   
            'font' =>[
                'name' => 'Calibri',
                'size'  => 20, 
                'bold' => TRUE, 
                'color' => [ 'rgb' => '000000' ] 
            ],
            'alignment' => [
                'wrapText'  => true,
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'   => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ]
            ],      
        ];
    }

}