<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use App\Models\Role;
use App\NewEidikotita;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Pasifai\Pysde\controllers\ExcelExports\UsersExport;
use Pasifai\Pysde\controllers\ExcelExports\VacuumExport;
use Pasifai\Pysde\models\Division;
use Pasifai\Pysde\models\Lesson;
use App\School;
use App\Sch_gr\Facade\Sch;
use Illuminate\Support\Facades\Notification;
use Pasifai\Pysde\notifications\PysdeNotification;
use Auth;
use ExportController;
use Carbon\Carbon;
use Pasifai\Pysde\models\TeacherRights;
use App\MySchoolTeacher;

class VacuumController extends Controller
{
    protected $students = [];
    protected $school_type = [
        'Γυμνάσιο'  => 'gym',
        'Λύκειο'    => 'lyk',
        'ΕΠΑΛ'      => 'epal',
        'Ειδικό'    => 'eidiko'
    ];
    protected $classes = ['Α', 'Β', 'Γ'];

    protected $organikes;
    protected $eidikotites;
    protected $databaseProjects;
    protected $data = [];

    protected $user;
    /**
     * This Controller is under Pysde Role
     */
    public function __construct()
    {
        // Auth::loginUsingId(56);         //todo not forget to comment this line
        $this->middleware('isPysde');
    }

    public function createGeneralDivsData()
    {
        return view('pysde::vacuum.gymnasium.create-general-divs');
    }

    public function ajaxToggleTeacherRights(Request $request)
    {
        if($request->ajax()){
            $myschoolTeacher = MySchoolTeacher::find($request->get('afm'));

            if($myschoolTeacher->rights == null){
                TeacherRights::create([
                    'afm'   => $request->get('afm'),
                    'onomastika_yperarithmos'   => true,
                    'dikaiwma_veltiwsis'        => false,
                    'year'                      => Carbon::now()->format('Y')
                ]);
            }else{
                $myschoolTeacher->rights->update([
                    'onomastika_yperarithmos'   => !$myschoolTeacher->rights->onomastika_yperarithmos,
                    'year'                      => Carbon::now()->format('Y'),
                    'dikaiwma_veltiwsis'        => false
                ]);
            }

            return 'ok';
        }
    }

    public function ajaxSaveDescriptionOfTeacher(Request $request)
    {
        if($request->ajax()){
            $myschoolTeacher = MySchoolTeacher::find($request->get('afm'));
            $new_description = $request->get('description');

            if($myschoolTeacher->rights != null){
                $myschoolTeacher->rights->update([
                    'description'   => $new_description
                ]);
                return 'αποθηκεύτηκε με επιτυχία';
            }
            return 'κάτι πήγε λάθος...';
        }
    }

    public function getExcelOrganika()
    {
        return view('pysde::organika.excel');   
    }

    public function getExcel()
    {
        $date = Carbon::now()->timestamp;

        return Excel::download(new VacuumExport(), 'ΟΡΓΑΝΙΚΑ_ΚΕΝΑ_'.$date.'.xlsx');
    }

    public function anatheseis()
    {
        return view('pysde::vacuum.gymnasium.anatheseis');
    }

    public function getHoursForGymnasiums(School $school)
    {
        return view('pysde::vacuum.gymnasium.index', compact('school'));
    }

    public function fetchAllSchools()
    {
        $schools = School::orderBy('name')->get();

        return $schools;
    }

    public function saveCouncil(School $school, Request $request)
    {
        if($request->ajax()){
            
            $eidikotites = $request->get('eidikotites');
            $pe04diff = $request->get('diffPe04');

            if($school->type == 'Λύκειο' || $school->type == 'ΕΠΑΛ'){
                foreach($eidikotites as $eidikotita){
                    $council_difference = $eidikotita['pivot']['available'] - $eidikotita['pivot']['council_forseen'] - $eidikotita['pivot']['project_hours'];
                    if($eidikotita['klados_slug'] == 'ΠΕ04'){
                        if($pe04diff <= -12){
                            if($council_difference <= -12){
                                $school->kena()->updateExistingPivot($eidikotita['id'], [
                                    'council_forseen'    => $eidikotita['pivot']['council_forseen'],
                                    'council_difference'    => $council_difference,
                                    'sum_pe04'    => $pe04diff
                                ]);
                            }else{
                                $school->kena()->updateExistingPivot($eidikotita['id'], [
                                    'council_forseen'    => $eidikotita['pivot']['council_forseen'],
                                    'council_difference'    => $council_difference,
                                    'sum_pe04'      => null
                                ]);
                            }
                        }elseif($pe04diff >= 12){
                            $school->kena()->updateExistingPivot($eidikotita['id'], [
                                'council_forseen'    => $eidikotita['pivot']['council_forseen'],
                                'council_difference'    => $council_difference,
                                'sum_pe04'    => $pe04diff
                            ]);                           
                        }else{
                            $school->kena()->updateExistingPivot($eidikotita['id'], [
                                'council_forseen'    => $eidikotita['pivot']['council_forseen'],
                                'council_difference'    => $council_difference,
                                'sum_pe04'    => null
                            ]);
                        }


                    }else{
                        $school->kena()->updateExistingPivot($eidikotita['id'], [
                            'council_forseen'    => $eidikotita['pivot']['council_forseen'],
                            'council_difference'    => $council_difference
                        ]);
                    }
                }
            }else{
                foreach($eidikotites as $eidikotita){
                    $council_difference = $eidikotita['pivot']['available'] - $eidikotita['pivot']['council_forseen'] - $eidikotita['pivot']['project_hours'];
                    if($eidikotita['klados_slug'] == 'ΠΕ04'){
                        $school->kena()->updateExistingPivot($eidikotita['id'], [
                            'council_forseen'    => $eidikotita['pivot']['council_forseen'],
                            'council_difference'    => $council_difference,
                            'sum_pe04'    => $pe04diff
                        ]);
                    }else{
                        $school->kena()->updateExistingPivot($eidikotita['id'], [
                            'council_forseen'    => $eidikotita['pivot']['council_forseen'],
                            'council_difference'    => $council_difference
                        ]);
                    }
                }
            }

            return 'ok';
        }
    }

    public function runAlgorithm(School $school, Request $request)
    {
        if($request->ajax()){
            $this->getUser();

            $divisions = $request->get('divisions');

            $sync_array = array();

            if($school->type == 'Γυμνάσιο'){
                foreach($divisions as $group_div){
                    foreach($group_div as $div){
//                        if($div['students'] > 0 && $div['number'] > 0){
                            $sync_array[$div['div_id']] = [
                                'number'    => $div['number'],
                                'numberSchoolProvide'   => $div['numberSchoolProvide'],
                                'students'              => $div['students']
                            ];
//                        }

                    }
                }
            }elseif($school->type == 'Λύκειο') {
                foreach ($divisions as $group_type) {
                    foreach ($group_type as $group_div) {
                        foreach ($group_div as $div) {
                            if ($div['students'] > 0 && $div['number'] > 0) {
                                $sync_array[$div['div_id']] = [
                                    'number' => $div['number'],
                                    'numberSchoolProvide' => $div['numberSchoolProvide'],
                                    'students' => $div['students'],
                                    'pedio2' => $div['pedio2'],
                                    'pedio3' => $div['pedio3'],
                                    'oligomeles' => $div['oligomeles']
                                ];
                            }
                        }
                    }
                }

                $projects = $request->get('projects');
                $sync_projects = array();
                foreach($projects as $project){
                    $sync_projects[$project['eid_id']] = [
                        'hours' => $project['hours']
                    ];
                }
                $school->projects()->sync($sync_projects);
            }else{
                foreach ($divisions as $group_type) {
                    foreach ($group_type as $group_div) {
                        foreach ($group_div as $div) {
                            if ($div['students'] > 0 && $div['number'] > 0) {
                                $sync_array[$div['div_id']] = [
                                    'number' => $div['number'],
                                    'numberSchoolProvide' => $div['numberSchoolProvide'],
                                    'students' => $div['students']
                                ];
                            }
                        }
                    }
                }            
            }

            $school->divisions()->sync($sync_array);

            \Artisan::call('kena:pinakas_a',[
                'type'          => $this->school_type[$school->type],
                'one_school'    => 'true',
                'sch_id'        => $school->id
            ]);

            $user = $school->user;

        //    Notification::send($user, new PysdeNotification($this->user,[
        //        'title'         => 'Προβολή πίνακα κενών πλεονασμάτων',
        //        'description'   => "Κενά - Πλεονάσματα",
        //        'type'          => 'primary',
        //        'url'           => route('Pysde::School::getKena')
        //    ]));

            return 'all ok';
        }
    }

    public function initializeAlgorithm(School $school, Request $request)
    {
        if($request->ajax()){
            $students = $request->get('students');
            $divisions = $request->get('divisions');

            return 'hello';
        }
    }

    public function getVaccumOfSchool(School $school)
    {
        $this->data['kena'] = $school->kena;

        $allDivisionsList = Division::where('school_type', $this->school_type[$school->type])->distinct()->get(['name', 'type']);

        $databaseCollection =  $school->divisions;

        $divisions = collect();

        foreach($allDivisionsList as $divisionObject){
            foreach($this->classes as $key=>$class){
                $databaseDiv = $databaseCollection->where('class', $class)->where('name', $divisionObject->name)->first();
                if($databaseDiv == null){
                    $nullDivision = Division::where('name', $divisionObject->name)->where('class', $class)->first();

                    $nullDivisionId = $nullDivision != null ? $nullDivision->id : 0;

                    $divisions->push([
                        'div_id'    => $nullDivisionId,
                        'name'      => $divisionObject->name,
                        'class'     => $class,
                        'numberSchoolProvide'   => 0,
                        'number'    => 0,
                        'students'  => 0,
                        'type'      => $divisionObject->type,
                        'order'     => $key,
                        'oligomeles' => false,
                        'pedio2' => false,
                        'pedio3' => false,
                    ]);
                }else{
                    $divisions->push([
                        'div_id'    => $databaseDiv->id,
                        'name'      => $databaseDiv->name,
                        'class'     => $databaseDiv->class,
                        'numberSchoolProvide'   => $databaseDiv->pivot->numberSchoolProvide,
                        'number'    => $databaseDiv->pivot->number,
                        'students'  => $databaseDiv->pivot->students,
                        'type'      => $databaseDiv->type,
                        'order'     => $key,
                        'oligomeles' => $databaseDiv->pivot->oligomeles,
                        'pedio2' => $databaseDiv->pivot->pedio2,
                        'pedio3' => $databaseDiv->pivot->pedio3,
                    ]);
                }
            }
        }

        if($school->type == 'Γυμνάσιο'){
            $this->data['divisions'] =  $divisions->sortBy('div_id')->values()->groupBy('name');
        }elseif($school->type == 'Λύκειο'){
            $this->data['divisions'] =  $divisions->sortBy('order')->values()->groupBy(['type', 'name']);
            $this->createProjects($school);
        }else{
            $this->data['divisions'] =  $divisions->sortBy('order')->values()->groupBy(['type', 'name']);
        }
        $this->data['students'] = $this->students;
        $this->data['school_type'] = $this->school_type[$school->type];

        return $this->data;
    }

    public function getTeachersOfSchool(School $school, Request $request)
    {
        if($request->ajax()){
            $eidikotita = $request->get('eidikotita');

            $teachers = array();
            $organika = $school->organika->where('eidikotita', $eidikotita)->sortBy('last_name')->values();
        
            foreach($organika as $t){
                $teachers[] = [
                    'last_name'             => $t['last_name'],
                    'first_name'            => $t['first_name'],
                    'middle_name'           => $t['middle_name'],
                    'am'                    => $t['am'],
                    'afm'                   => $t['afm'],
                    'ypoxreotiko'           => $t['ypoxreotiko'],
                    'yperarithmos'          => $t->rights == null ? false :  $t->rights->onomastika_yperarithmos,
                    'description'           => $t->rights == null ? null :  $t->rights->description
                ];
            }
            return $teachers;
        }
    }

    protected function getStudentsFromDivision($name, $class, $students)
    {
        if($name == 'ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ'){
            if($class == 'Α'){
                $this->students['Α'] = $students;
            }elseif($class == 'Β'){
                $this->students['Β'] = $students;
            }elseif($class == 'Γ'){
                $this->students['Γ'] = $students;
            }
        }
    }

    private function pushToProjects($divisions, $c)
    {
        $divisions->push([
            'eid_id' => $c->id,
            'name' => $c->eidikotita_name,
            'slug' => $c->eidikotita_slug,
            'hours' => $c->pivot->hours
        ]);
    }

    /**
     * @param School $school
     */
    private function createProjects(School $school)
    {
        $projects = collect();
        $divisions_projects = collect();

        $this->organikes = $school->organikes;

        $this->eidikotites = NewEidikotita::all();

        foreach ($this->organikes as $organiki) {
            $teacher = $organiki->teacher;
            if (!$projects->contains('id', $teacher->new_eidikotita)) {
                $eidikotita = $this->eidikotites->where('id', $teacher->new_eidikotita)->first();

                $projects->push([
                    'id' => $eidikotita->id,
                    'slug' => $eidikotita->eidikotita_slug,
                    'name' => $eidikotita->eidikotita_name
                ]);
            }
        }
        $this->databaseProjects = $school->projects;

        foreach ($this->databaseProjects as $c) {
            $this->pushToProjects($divisions_projects, $c);
        }

        $list_projects = $projects->whereNotIn('id', $this->databaseProjects->pluck('id'))->values();

        $this->data['projects'] = $divisions_projects;
        $this->data['list_projects'] = $list_projects->sortBy('id')->values();
    }

    protected function getUser()
    {
        $this->user = Auth::user();
    }
}
