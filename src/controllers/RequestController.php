<?php

namespace Pasifai\Pysde\controllers;

use Pasifai\Pysde\models\ProtocolTeacher;
use Pasifai\Pysde\models\Protocol;

use App\NewEidikotita;
use App\School;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Pasifai\Pysde\models\OrganikiPrefrences;
use Pasifai\Pysde\models\OrganikiRequest;
use DB;
use Pasifai\Pysde\models\Prefrences;
use Pasifai\Pysde\models\RequestTeacher;
use Pasifai\Pysde\models\RequestTeacherYperarithmia;
use Barryvdh\DomPDF\Facade as PDF;
use Pasifai\Pysde\controllers\traits\RequestsTrait;
use App\Http\Controllers\Traits\TraitNotification;
use App\Models\Dieuthinsi;
use App\User;

use Gate;
use Pasifai\Pysde\mail\EpithimiaYperarithmias;

use Pasifai\Pysde\controllers\traits\MoriaCalculator;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Notification;
use Pasifai\Pysde\notifications\TeacherToPysdeNotification;
use App\Models\Role;

class RequestController extends Controller      //TODO must change permissions, E-mail and Notifications
{
    use MoriaCalculator, RequestsTrait, TraitNotification;

    private $user;
    private $unique_id = '';
    private $date_of_requests = "2019-09-01 00:00:00";
    private $analytika;
    private $entopiotita;
    private $sinipiretisi;

    /*
        Ονομαστικά Υπεράριθμος
        Λειτουργικά Υπεράριθμος
    */

    private $request_type = [
        'E1'    => ['Διάθεση', 'Λειτουργικά Υπεράριθμος'],
        'E2'    => 'Συμπλήρωση',
        'E3'    => 'Απόσπαση Εντός',
        'E4'    => 'Αναπληρωτής',
        'E5'    => 'Αναπληρωτής - Covid 19',
        'E6'    => 'Διάθεση ΠΥΣΔΕ - ΝΕΟΔΙΟΡΙΣΤΟΣ',
        'ΟργανικήΥπ'    => 'ΟργανικήΥπεράριθμου',
        'Βελτίωση'      => 'Βελτίωση'
    ];
    private $types = [
        'Αίτηση-Υπεραριθμίας'   => 'Υπεραρ',
        'Αίτηση-Οργανικής-Υπεράριθμου'  => 'ΟργανικήΥπ',
        'Αίτηση-Βελτίωσης-Οριστικής-Τοποθέτησης'    => 'Βελτίωση',
        'Αίτηση-Μοριοδότησης-Μετάθεσης' => 'Μοριοδ',
        'Αίτηση-Μοριοδότησης-Απόσπασης' => 'ΜοριοδΑ',
    ];

    private $moriodotisi = [
        'Αίτηση-Μοριοδότησης-Μετάθεσης' => 'ΜΕΤΑΘΕΣΗΣ',
        'Αίτηση-Μοριοδότησης-Απόσπασης' => 'ΑΠΟΣΠΑΣΗΣ'
    ];

    public function __construct()
    {
        // Auth::loginUsingId(123);
        $this->middleware('isTeacher'); //TODO   MUST CHANGE THIS!!!
    }

    public function ajaxPermanentlyDeleteRequestOrganika(Request $request)
    {
        $this->getUser();

        if ($request->ajax()){
            $type = $request->get('type');
            $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                ->where('aitisi_type', $type)
                ->where('protocol_number', null)
                ->first();
            $request->delete();

            flash()->overlayE('Προσοχή!!!', 'Η Αίτηση διαγράφηκε Οριστικά και ΔΕΝ θα ληφθεί υπόψη');

            return route('pysde::Aitisi::Prepare');

        }
    }


    public function ajaxFetchSchoolsVeltiwsi(Request $request)
    {
        if($request->ajax()){
            $this->getUser();

            $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
//                ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
                ->where(DB::raw('TIMEDIFF("2018-05-24 18:00:00", date_request)'), '<', 0)   //TODO  MUST CHANGE THIS
                ->where('aitisi_type', 'Βελτιώση - Οριστική τοποθέτηση')
                ->first();

            $selectedSchoolsList = [];
            $selectedSchools = array();

            if($request != null){
                if(is_integer($request->protocol_number)){
                    return 'is_made';
                }
                foreach($request->prefrences->sortBy('order_number') as $prefrence){
                    $selectedSchoolsList[] = $prefrence->school_id;
                    $selectedSchools[] = [
                        'id'    => $prefrence->school_id,
                        'name'  => $prefrence->OrganikiName,
                        'order' => $prefrence->OrganikiOrder
                    ];
                }
            }

            $allSchools =  School::where('municipality_id', 0)
                ->whereNotIn('id', $selectedSchoolsList)
                ->orderBy('order')
                ->get(['name', 'id', 'order']);

            return compact('allSchools', 'selectedSchools');
        }else{
            abort(404);
        }
    }

    public function ajaxSaveOrganikaRequest(Request $request)
    {
        if ($request->ajax()){
            $message = $this->saveOrganikiRequest($request);

            return $message;
        }
    }

    public function ajaxSaveOrganikaRequestVeltiwsi(Request $request)
    {
        if ($request->ajax()){
            $message = $this->saveOrganikiRequestVeltiwsi($request);

            return $message;
        }
    }

    public function ajaxSentOrganikaRequestForProtocol(Request $request)
    {
        $message = $this->saveOrganikiRequest($request);

        $this->giveOrganikiProtocol($request);

        return 'all ok for Organika Yperarithmous';
    }

    public function ajaxSentOrganikaRequestForProtocolVeltiwsi(Request $request)
    {
        $message = $this->saveOrganikiRequestVeltiwsi($request);

        $this->giveOrganikiProtocolVeltiwsi($request);

        return 'all ok';
    }

    public function ajaxCheckIfYperarithmiaExists(Request $request)
    {
        return 'ok';
    }

    //############################### NEW VUE 2  start ###############################

    public function ajaxFetchSchools(Request $request)
    {
        if($request->ajax()){
            
            $this->getUser();

            $requestExistsInDatabase = false;
            $sistegazomeno = 0;
            $sizigos_stratiotikou = 0;

            $unique_id = $request->get('unique_id');
            $request_type = $request->get('request_type'); // E1, E2, E3 etc..., ΟργανικήΥπ, Βελτίωση
            
            $organiki = School::find($this->user->userable->teacherable->organiki_id);
            

            if(array_key_exists($request_type, $this->request_type)){
                $currentReason =  $this->request_type[$request_type];
                if(is_array($currentReason)){
                    if($organiki != null){
                        $currentReason = $currentReason[1];
                    }else{
                        $currentReason = $currentReason[0];
                    }
                }
            }else{
                $currentReason = $request_type;
            }            
                              
            $records = OrganikiRequest::with(['prefrences' => function($query){
                $query->orderBy('order_number');
            }])
                ->where('teacher_id', $this->user->userable->id)
            //    ->where(DB::raw('TIMEDIFF($this->date_for_requests, date_request)'), '<', 0)           //TODO MUST CHANGE THIS
                ->where('groupType', $currentReason)
                ->where('unique_id', $unique_id)
                ->get();

            $selectedSchoolsListOther = [];
            $selectedSchoolsListIdiaOmada = [];
            $stay_to_organiki = -1;
            $hours_for_request = 0;
            $selectedSchoolsListOmores = [];

            $selectedSchoolsOther = array();
            $selectedSchoolsIdiaOmada = array();
            $selectedSchoolsOmores = array();

            $description_idia = '';
            $description_omores = '';
            $description_other = '';

            $isMadeValue = 'not_protocol_given';

            $old_unique_id = '';
            $weFoundOtherRequest = false;
            $url = '';
            
            if(!$records->isEmpty()){
                $requestExistsInDatabase = true;
                $currentReason = $records->first()->groupType;
                $sistegazomeno = $records->first()->sistegazomeno;
                $sizigos_stratiotikou = $records->first()->sizigos_stratiotikou;

                if(is_integer($records->first()->protocol_number)){
                    $isMadeValue = 'protocol_given';
                }

                foreach($records as $r){
                    if($r->aitisi_type == 'ΥπεράριθμοςΊδιαΟμάδα'){
                        $stay_to_organiki = $r->stay_to_organiki;
                        $hours_for_request = $r->hours_for_request;
                        foreach($r->prefrences as $prefrence){
                            $selectedSchoolsListIdiaOmada[] = $prefrence->school_id;
                            $selectedSchoolsIdiaOmada[] = [
                                'id'    => $prefrence->school_id,
                                'name'  => $prefrence->OrganikiName,
                                'order' => $prefrence->OrganikiOrder
                            ];
                        }
                        $description_idia = $r->description;
                    }
                    if($r->aitisi_type == 'ΥπεράριθμοςΌμορες'){
                        $stay_to_organiki = $r->stay_to_organiki;
                        $hours_for_request = $r->hours_for_request;
                        foreach($r->prefrences as $prefrence){
                            $selectedSchoolsListOmores[] = $prefrence->school_id;
                            $selectedSchoolsOmores[] = [
                                'id'    => $prefrence->school_id,
                                'name'  => $prefrence->OrganikiName,
                                'order' => $prefrence->OrganikiOrder
                            ];
                        }
                        $description_omores = $r->description;
                    }
                    if($r->aitisi_type == 'Διάθεση'){
                        $hours_for_request = $r->hours_for_request;
                        foreach($r->prefrences as $prefrence){
                            $selectedSchoolsListOther[] = $prefrence->school_id;
                            $selectedSchoolsOther[] = [
                                'id'    => $prefrence->school_id,
                                'name'  => $prefrence->OrganikiName,
                                'order' => $prefrence->OrganikiOrder
                            ];
                        }
                        $description_other = $r->description;
                    }
                    if($r->aitisi_type == 'Βελτίωση'){
                        $hours_for_request = $r->hours_for_request;
                        foreach($r->prefrences as $prefrence){
                            $selectedSchoolsListOther[] = $prefrence->school_id;
                            $selectedSchoolsOther[] = [
                                'id'    => $prefrence->school_id,
                                'name'  => $prefrence->OrganikiName,
                                'order' => $prefrence->OrganikiOrder
                            ];
                        }
                        $description_other = $r->description;
                    }
                    if(in_array($r->aitisi_type, ['E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7'])){
                        $hours_for_request = $r->hours_for_request;
                        foreach($r->prefrences as $prefrence){
                            $selectedSchoolsListOther[] = $prefrence->school_id;
                            $selectedSchoolsOther[] = [
                                'id'    => $prefrence->school_id,
                                'name'  => $prefrence->OrganikiName,
                                'order' => $prefrence->OrganikiOrder
                            ];
                        }
                        $description_other = $r->description;
                    }
                }

            }else{
                $requestFirst = OrganikiRequest::with(['prefrences' => function($query){
                    $query->orderBy('order_number');
                }])
                    ->where('teacher_id', $this->user->userable->id)
                    ->where(DB::raw('TIMEDIFF("2023-09-01 00:00:00", date_request)'), '<', 0)           //TODO MUST CHANGE THIS
                    ->where('groupType', $currentReason)
                    ->where('protocol_number', null)
                    ->orderBy('updated_at', 'desc')
                    ->first();

                if($requestFirst != null){
                    $requestExistsInDatabase = true;

                    $old_unique_id = $requestFirst->unique_id;
                    $weFoundOtherRequest = true;

                    $url = route('Request::OrganikiType', [$request_type, $old_unique_id]);
                }
            }

            $allSchools = School::where('type', '<>', 'private')->orderBy('order')->get();

            
            if($organiki == ''){
                $hasOrganiki = false;

                $allSchoolsIdiasOmadas = [];
                $allSchoolsOmoron = [];
                $sameTeam = [];
                $omoresTeams =[];
                

                if($requestExistsInDatabase){
                    $allSchools = $allSchools->reject(function($value) use($selectedSchoolsListOther){
                        return in_array($value->id, $selectedSchoolsListOther);
                    })->sortBy('order')->values();
                }
            }else{
                $hasOrganiki = true;

                $sameTeam = [$organiki->stinomada];
                $omoresTeams = $organiki->stinomada->omores;

                $allSchoolsIdiasOmadas = $organiki->stinomada->idia_omada->pluck('schools')->collapse();
                $allSchoolsOmoron = $organiki->stinomada->omores->pluck('schools')->collapse();

                if($requestExistsInDatabase){
                    $allSchoolsIdiasOmadas = $allSchoolsIdiasOmadas->reject(function($value) use($selectedSchoolsListIdiaOmada){
                        return in_array($value->id, $selectedSchoolsListIdiaOmada);
                    })->sortBy('order')->values();

                    $allSchoolsOmoron = $allSchoolsOmoron->reject(function($value) use($selectedSchoolsListOmores){
                        return in_array($value->id, $selectedSchoolsListOmores);
                    })->sortBy('order')->values();

                    $allSchools = $allSchools->reject(function($value) use($selectedSchoolsListOther){
                        return in_array($value->id, $selectedSchoolsListOther);
                    })->sortBy('order')->values();
                }
            }

            return compact(
                'organiki',
                'allSchoolsIdiasOmadas',
                'allSchoolsOmoron',
                'allSchools',
                'selectedSchoolsIdiaOmada',
                'selectedSchoolsOmores',
                'selectedSchoolsOther',
                'isMadeValue',
                'sameTeam',
                'omoresTeams',
                'hasOrganiki',
                'requestExistsInDatabase',
                'currentReason',
                'stay_to_organiki',
                'hours_for_request',
                'old_unique_id',
                'weFoundOtherRequest',
                'url',
                'sistegazomeno',
                'description_idia',
                'description_omores',
                'description_other',
                'sizigos_stratiotikou'
            );
        }

    }

    public function ajaxFetchSchoolsCovid19(Request $request)
    {        
        if($request->ajax()){
            $this->getUser();

            $new_eidikotita =  $this->user->userable->myschool->new_eidikotita;
            
            $requestExistsInDatabase = false;
            $hasOrganiki = false;
            $sizigos_stratiotikou = false;

            $unique_id = $request->get('unique_id');
            $request_type = $request->get('request_type'); // E5 for covid 19   
            $hours_for_request = 0;         


            if(array_key_exists($request_type, $this->request_type)){
                $currentReason =  $this->request_type[$request_type];
            }else{
                $currentReason = $request_type;
            }
      
            $records = OrganikiRequest::with(['prefrences' => function($query){
                $query->orderBy('order_number');
            }])
                ->where('teacher_id', $this->user->userable->id)
            //    ->where(DB::raw('TIMEDIFF($this->date_for_requests, date_request)'), '<', 0)           //TODO MUST CHANGE THIS
                ->where('groupType', $currentReason)
                ->where('unique_id', $unique_id)
                ->get();

            $selectedSchoolsListOther = [];
            $selectedSchoolsOther = array();
            $description_other = '';

            $isMadeValue = 'not_protocol_given';

            $old_unique_id = '';
            $weFoundOtherRequest = false;
            $url = '';

            $allSchools = DB::table('_kena_leitourgika_covid19')
                            ->join('schools', 'schools.id', '=', '_kena_leitourgika_covid19.sch_id')
                            ->where('eid_id', $new_eidikotita)
                            // ->where('hours', '>', 0)
                            ->orderBy('priority', 'desc')
                            ->get(['sch_id', 'eid_id', 'hours', 'name', 'type', 'date_ends', 'id', 'locked', 'priority']);

            if(!$records->isEmpty()){
                $requestExistsInDatabase = true;
                $currentReason = $records->first()->groupType;

                if(is_integer($records->first()->protocol_number)){
                    $isMadeValue = 'protocol_given';
                }

                foreach($records as $r){
                    if(in_array($r->aitisi_type, ['E5'])){
                        $hours_for_request = $r->hours_for_request;
                        foreach($r->prefrences as $prefrence){
                            $selectedSchoolsListOther[] = $prefrence->school_id;

                            // 666 test this...
                            // $covid_record = $allSchools->where('sch_id', $prefrence->school_id)->where('sch_id', $prefrence->school_id)->first();
                            $covid_record = $allSchools->where('sch_id', $prefrence->school_id)->where('eid_id', $new_eidikotita)->first();

                            $selectedSchoolsOther[] = [
                                'id'    => $prefrence->school_id,
                                'name'  => $prefrence->OrganikiName,
                                'order' => $prefrence->OrganikiOrder,
                                'eid_id' => $new_eidikotita,
                                'hours' => $covid_record->hours,
                                'priority' => $covid_record->priority,
                                'sch_id' => $prefrence->school_id
                            ];
                        }
                        $description_other = $r->description;
                    }
                }

            }else{
                $requestFirst = OrganikiRequest::with(['prefrences' => function($query){
                    $query->orderBy('order_number');
                }])
                    ->where('teacher_id', $this->user->userable->id)
                    ->where(DB::raw('TIMEDIFF("2019-09-01 00:00:00", date_request)'), '<', 0)           //TODO MUST CHANGE THIS
                    ->where('groupType', $currentReason)
                    ->where('protocol_number', null)
                    ->orderBy('updated_at', 'desc')
                    ->first();

                if($requestFirst != null){
                    $requestExistsInDatabase = true;

                    $old_unique_id = $requestFirst->unique_id;
                    $weFoundOtherRequest = true;

                    $url = route('Request::OrganikiType', [$request_type, $old_unique_id]);
                }
            }

            if($requestExistsInDatabase){
                $allSchools = $allSchools->reject(function($value) use($selectedSchoolsListOther){
                    return in_array($value->id, $selectedSchoolsListOther);
                })->sortBy('order')->values();
            }
            
            return compact(
                'allSchools', 
                'selectedSchoolsOther',
                'isMadeValue',
                'requestExistsInDatabase',
                'currentReason',
                'hours_for_request',
                'old_unique_id',
                'weFoundOtherRequest',
                'url',
                'description_other',
                'hasOrganiki',
                'sizigos_stratiotikou'
            );
        }
    }
        //ajaxFetchSchoolsYperarithmou
        public function ajaxFetchSchoolsLeitourgika(Request $request)
        {
            if($request->ajax()){
                $this->getUser();

                // return $this->user;

                $requestExistsInDatabase = false;
                $sistegazomeno = 0;
                $sizigos_stratiotikou = 0;

                $unique_id = $request->get('unique_id');
                $request_type = $request->get('request_type'); // E1, E2, E3 etc...

                $organiki = School::find($this->user->userable->teacherable->organiki_id);


                if($request_type == 'E1'){
                    if($organiki == ''){
                        $currentReason = 'Διάθεση';
                    }else{
                        $currentReason = 'Λειτουργικά Υπεράριθμος';
                    }
                }elseif($request_type == 'E2'){
                    $currentReason = 'Συμπλήρωση';
                }elseif($request_type == 'E3'){
                    $currentReason = 'Απόσπαση Εντός';
                }elseif($request_type == 'E4'){
                    $currentReason = 'Αναπληρωτής';

                }elseif($request_type == 'E5'){
                    $currentReason = 'Συμπλήρωση';
                }else{
                    $currentReason = 'Συμπλήρωση';
                }

                $records = OrganikiRequest::with(['prefrences' => function($query){
                    $query->orderBy('order_number');
                }])
                    ->where('teacher_id', $this->user->userable->id)
    //                ->where(DB::raw('TIMEDIFF($this->date_for_requests, date_request)'), '<', 0)           //TODO MUST CHANGE THIS
                    ->where('groupType', $currentReason)
                    ->where('unique_id', $unique_id)
                    ->get();

                $selectedSchoolsListOther = [];
                $selectedSchoolsListIdiaOmada = [];
                $stay_to_organiki = -1;
                $hours_for_request = 0;
                $selectedSchoolsListOmores = [];

                $selectedSchoolsOther = array();
                $selectedSchoolsIdiaOmada = array();
                $selectedSchoolsOmores = array();

                $description_idia = '';
                $description_omores = '';
                $description_other = '';

                $isMadeValue = 'not_protocol_given';

                if(!$records->isEmpty()){
                    $requestExistsInDatabase = true;
                    $currentReason = $records->first()->groupType;
                    $sistegazomeno = $records->first()->sistegazomeno;
                    $sizigos_stratiotikou = $records->first()->sizigos_stratiotikou;

                    if(is_integer($records->first()->protocol_number)){
                        $isMadeValue = 'protocol_given';
                    }

                    foreach($records as $r){
                        if($r->aitisi_type == 'ΥπεράριθμοςΊδιαΟμάδα'){
                            $stay_to_organiki = $r->stay_to_organiki;
                            $hours_for_request = $r->hours_for_request;
                            foreach($r->prefrences as $prefrence){
                                $selectedSchoolsListIdiaOmada[] = $prefrence->school_id;
                                $selectedSchoolsIdiaOmada[] = [
                                    'id'    => $prefrence->school_id,
                                    'name'  => $prefrence->OrganikiName,
                                    'order' => $prefrence->OrganikiOrder
                                ];
                            }
                            $description_idia = $r->description;
                        }
                        if($r->aitisi_type == 'ΥπεράριθμοςΌμορες'){
                            $stay_to_organiki = $r->stay_to_organiki;
                            $hours_for_request = $r->hours_for_request;
                            foreach($r->prefrences as $prefrence){
                                $selectedSchoolsListOmores[] = $prefrence->school_id;
                                $selectedSchoolsOmores[] = [
                                    'id'    => $prefrence->school_id,
                                    'name'  => $prefrence->OrganikiName,
                                    'order' => $prefrence->OrganikiOrder
                                ];
                            }
                            $description_omores = $r->description;
                        }
                        if($r->aitisi_type == 'Διάθεση'){
                            $hours_for_request = $r->hours_for_request;
                            foreach($r->prefrences as $prefrence){
                                $selectedSchoolsListOther[] = $prefrence->school_id;
                                $selectedSchoolsOther[] = [
                                    'id'    => $prefrence->school_id,
                                    'name'  => $prefrence->OrganikiName,
                                    'order' => $prefrence->OrganikiOrder
                                ];
                            }
                            $description_other = $r->description;
                        }
                    }

                }else{
                    $requestFirst = OrganikiRequest::with(['prefrences' => function($query){
                        $query->orderBy('order_number');
                    }])
                        ->where('teacher_id', $this->user->userable->id)
                        ->where(DB::raw('TIMEDIFF("2018-09-01 00:00:00", date_request)'), '<', 0)           //TODO MUST CHANGE THIS
                        ->where('groupType', $currentReason)
                        ->where('protocol_number', null)
                        ->orderBy('updated_at', 'desc')
                        ->first();

                    if($requestFirst != null){
                        $requestExistsInDatabase = true;

                        $old_unique_id = $requestFirst->unique_id;
                        $weFoundOtherRequest = true;

                        $url = route('Request::create', [$request_type, $old_unique_id]);
                    }
                }


                $allSchools = School::orderBy('order')->get();

                if($organiki == ''){
                    $hasOrganiki = false;
                    if($requestExistsInDatabase){
                        $allSchools = $allSchools->reject(function($value) use($selectedSchoolsListOther){
                            return in_array($value->id, $selectedSchoolsListOther);
                        })->sortBy('order')->values();
                    }
                }else{
                    $hasOrganiki = true;

                    $sameTeam = [$organiki->stinomada];
                    $omoresTeams = $organiki->stinomada->omores;

                    $allSchoolsIdiasOmadas = $organiki->stinomada->idia_omada->pluck('schools')->collapse();
                    $allSchoolsOmoron = $organiki->stinomada->omores->pluck('schools')->collapse();

                    if($requestExistsInDatabase){
                        $allSchoolsIdiasOmadas = $allSchoolsIdiasOmadas->reject(function($value) use($selectedSchoolsListIdiaOmada){
                            return in_array($value->id, $selectedSchoolsListIdiaOmada);
                        })->sortBy('order')->values();

                        $allSchoolsOmoron = $allSchoolsOmoron->reject(function($value) use($selectedSchoolsListOmores){
                            return in_array($value->id, $selectedSchoolsListOmores);
                        })->sortBy('order')->values();

                        $allSchools = $allSchools->reject(function($value) use($selectedSchoolsListOther){
                            return in_array($value->id, $selectedSchoolsListOther);
                        })->sortBy('order')->values();
                    }
                }

                return compact(
                    'organiki',
                    'allSchoolsIdiasOmadas',
                    'allSchoolsOmoron',
                    'allSchools',
                    'selectedSchoolsIdiaOmada',
                    'selectedSchoolsOmores',
                    'selectedSchoolsOther',
                    'isMadeValue',
                    'sameTeam',
                    'omoresTeams',
                    'hasOrganiki',
                    'requestExistsInDatabase',
                    'currentReason',
                    'stay_to_organiki',
                    'hours_for_request',
                    'old_unique_id',
                    'weFoundOtherRequest',
                    'url',
                    'sistegazomeno',
                    'description_idia',
                    'description_omores',
                    'description_other',
                    'sizigos_stratiotikou'
                );
            }else{
                abort(404);
            }
        }
        //saveRequestYperarithmosLeitourgika
        public function saveRequestLeitourgika(Request $request)
        {
            if ($request->ajax()){
                $message = [];         
                
                // DB::beginTransaction();
                $temp = $request->get('unique_id');

                if($temp != ""){
                    $this->unique_id = $temp;
                }else{
                    $this->unique_id = uniqid(\Auth::id());
                }

                
                $msg1 = $this->createOrUpdateIdiaOmadaToDatabaseLeitourgika($request);
                $msg2 = $this->createOrUpdateOmoresToDatabaseLeitourgika($request);

                $msg3 = $this->saveOrganikiRequestDiathesiLeitourgika($request);

                if($msg1 != '')
                    $message[] = $msg1;
                if($msg2 != '')
                    $message[] = $msg2;
                if($msg3 != '')
                    $message[] = $msg3;
                $unique_id = $this->unique_id;

                // DB::commit();
                return compact('message', 'unique_id');
            }
        }
        //createOrUpdateIdiaOmadaToDatabase
        private function createOrUpdateIdiaOmadaToDatabaseLeitourgika(Request $request)
        {
            $this->getUser();

            $requestIdiaOmada = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                ->where('aitisi_type', 'ΥπεράριθμοςΊδιαΟμάδα')
                ->where('unique_id', $this->unique_id)
                ->first();

            if ($requestIdiaOmada == null) {
                if(!empty($request->get('selectedSchoolsIdiasOmadas'))){

                    $new_request_Idia_Omada = OrganikiRequest::create([
                        'unique_id' => $this->unique_id,
                        'teacher_id' => $this->user->userable->id,
                        'school_organiki' => $this->user->userable->teacherable->organiki_name,
                        'aitisi_type' => 'ΥπεράριθμοςΊδιαΟμάδα',
                        'date_request' => Carbon::now(),
                        'situation' => $request->get('aitisi_type'),
                        'stay_to_organiki' => $request->get('stay_to_organiki'),
                        'hours_for_request'    => $request->get('hours_for_request'),
                        'groupType' => $request->get('currentReason'),
                        'description' => $request->get('description_idia'),
                        'sistegazomeno' => $request->get('sistegazomeno')
                    ]);

                    $this->createOrganikesPrefrences($request->get('selectedSchoolsIdiasOmadas'), $new_request_Idia_Omada);
                    $message = 'Οι προτιμήσεις σας για την ίδια ομάδα σχολείων αποθηκεύτηκαν.';
                    return $message;
                }else{
                    return '';
                }
            } else {
                if(empty($request->get('selectedSchoolsIdiasOmadas'))){
                    $requestIdiaOmada->delete();
                    return $message = 'Οι προτιμήσεις σας για την ίδια ομάδα σχολείων ΔΙΑΓΡΑΦΗΚΑΝ.';
                }else{
                    OrganikiPrefrences::where('request_id', $requestIdiaOmada->id)->delete();
                    $this->createOrganikesPrefrences($request->get('selectedSchoolsIdiasOmadas'), $requestIdiaOmada);
                    $requestIdiaOmada->updated_at = Carbon::now();
                    $requestIdiaOmada->stay_to_organiki = $request->get('stay_to_organiki');
                    $requestIdiaOmada->hours_for_request = $request->get('hours_for_request');
                    $requestIdiaOmada->description = $request->get('description_idia');
                    $requestIdiaOmada->sistegazomeno = $request->get('sistegazomeno');
                    $requestIdiaOmada->save();
                    $message = 'Οι προτιμήσεις σας για την ίδια ομάδα σχολείων ΕΝΗΜΕΡΩΘΗΚΑΝ.';
                    return $message;
                }
            }
        }
        //createOrUpdateOmoresToDatabase
        private function createOrUpdateOmoresToDatabaseLeitourgika(Request $request)
        {
            $requestOmores = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                ->where('aitisi_type', 'ΥπεράριθμοςΌμορες')
                ->where('unique_id', $this->unique_id)
                ->first();

            if ($requestOmores == null) {
                if(!empty($request->get('selectedSchoolsOmores'))){
                    $new_request_Omores = OrganikiRequest::create([
                        'unique_id' => $this->unique_id,
                        'teacher_id' => $this->user->userable->id,
                        'school_organiki' => $this->user->userable->teacherable->organiki_name,
                        'aitisi_type' => 'ΥπεράριθμοςΌμορες',
                        'date_request' => Carbon::now(),
                        'situation' => $request->get('aitisi_type'),
                        'stay_to_organiki' => $request->get('stay_to_organiki'),
                        'hours_for_request'    => $request->get('hours_for_request'),
                        'groupType' => $request->get('currentReason'),
                        'description' => $request->get('description_omores'),
                        'sistegazomeno' => $request->get('sistegazomeno')
                    ]);

                    $this->createOrganikesPrefrences($request->get('selectedSchoolsOmores'), $new_request_Omores);
                    $message = 'Οι προτιμήσεις σας για τις ΟΜΟΡΕΣ Ομάδες σχολείων αποθηκεύτηκαν.';
                    return $message;
                }else{
                    return '';
                }
            } else {
                if(empty($request->get('selectedSchoolsOmores'))){
                    $requestOmores->delete();
                    return $message = 'Οι προτιμήσεις σας για τις ΟΜΟΡΕΣ Ομάδες σχολείων ΔΙΑΓΡΑΦΗΚΑΝ.';
                }else{
                    OrganikiPrefrences::where('request_id', $requestOmores->id)->delete();
                    $this->createOrganikesPrefrences($request->get('selectedSchoolsOmores'), $requestOmores);
                    $requestOmores->updated_at = Carbon::now();
                    $requestOmores->stay_to_organiki = $request->get('stay_to_organiki');
                    $requestOmores->hours_for_request = $request->get('hours_for_request');
                    $requestOmores->description = $request->get('description_omores');
                    $requestOmores->sistegazomeno = $request->get('sistegazomeno');
                    $requestOmores->save();
                    $message = 'Οι προτιμήσεις σας για τις ΟΜΟΡΕΣ Ομάδες σχολείων ΕΝΗΜΕΡΩΘΗΚΑΝ.';
                    return $message;
                }
            }
        }
        //saveOrganikiRequestDiathesi
        private function saveOrganikiRequestDiathesiLeitourgika(Request $request)
        {
            $this->getUser();

            $selectedSchools = $request->get('selectedSchoolsOther');
            $description = $request->get('description_other');
            $stay_to_organiki = $request->get('stay_to_organiki');
            $hours_for_request = $request->get('hours_for_request');
            $currentReason = $request->get('currentReason');
            $aitisi_type = $request->get('aitisi_type');
            $sistegazomeno = $request->get('sistegazomeno');
            $sizigos_stratiotikou = $request->get('sizigos_stratiotikou');

            $message = '';
            
            DB::beginTransaction();

                $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
        //            ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
                    ->where('aitisi_type', $aitisi_type)
                    ->where('unique_id', $this->unique_id)
                    ->first();

                $dataForE3 = [];
                if($aitisi_type == 'E3'){
                    $this->analytika = $this->getMoriaDetails();
                    $this->sinipiretisi = $this->getSinipiretisiMoria($this->user->userable);
                    $this->entopiotita = $this->getEntopiotita($this->user->userable);

                    $dataForE3 = [
                        'proipiresia'       => $this->analytika->where('name','proipiresia')->first()['value'],
                        'family'       => $this->analytika->where('name','family')->first()['value'],
                        'kids'       => $this->analytika->where('name','kids')->first()['value'],
                        'ygeia_idiou'       => $this->analytika->where('name','ygeia_idiou')->first()['value'],
                        'ygeia_sizigou'       => $this->analytika->where('name','ygeia_sizigou')->first()['value'],
                        'ygeia_teknon'       => $this->analytika->where('name','ygeia_teknon')->first()['value'],
                        'ygeia_goneon'       => $this->analytika->where('name','ygeia_goneon')->first()['value'],
                        'ygeia_aderfon'       => $this->analytika->where('name','ygeia_aderfon')->first()['value'],
                        'exosomatiki'       => $this->analytika->where('name','exosomatiki')->first()['value'],
                        'entopiotita'       => $this->entopiotita,
                        'sinipiretisi'       => $this->sinipiretisi,
                        'sum'       => $this->analytika->sum('value'),
                    ];
                }

                if ($request == null) {
                    if($this->unique_id == ''){
                        $this->unique_id = uniqid($this->user->id);
                    }

                    if(!empty($selectedSchools)){
                        $new_request = OrganikiRequest::create([
                            'unique_id' => $this->unique_id,
                            'teacher_id' => $this->user->userable->id,
                            'school_organiki' => $this->user->userable->myschool->organiki_name,
                            'aitisi_type' => $aitisi_type,
                            'date_request' => Carbon::now(),
                            'situation' => $aitisi_type,
                            'stay_to_organiki'  => $stay_to_organiki,
                            'hours_for_request' => $hours_for_request,

                            'groupType' => $currentReason == ''? 'Διάθεση' : $currentReason,
                            'description' => $description,
                            'sistegazomeno' => $sistegazomeno,
                            'sizigos_stratiotikou' => $sizigos_stratiotikou
                        ] + $dataForE3);
                        
                        $this->createOrganikesPrefrences($selectedSchools, $new_request);
                        $message = $this->createMessage($aitisi_type, $currentReason);
                    }

                } else {
                    OrganikiPrefrences::where('request_id', $request->id)->delete();
                    $this->createOrganikesPrefrences($selectedSchools, $request);
                    $stay_to_organiki = 0;
                    $request->updated_at = Carbon::now();
                    $request->stay_to_organiki = $stay_to_organiki;
                    $request->hours_for_request = $hours_for_request;
                    $request->description = $description;
                    $request->sistegazomeno = $sistegazomeno;
                    $request->sizigos_stratiotikou = $sizigos_stratiotikou;
                    $request->save();

                    $message = $this->createMessage($aitisi_type, $currentReason);
                }

            DB::commit();
            return $message;
        }

        private function createMessage($aitisi_type, $currentReason)
        {
            if($aitisi_type == 'E1'){
                $message = 'τα Σχολεία με τα οποία θα συγκριθείτε στη 2η Φάση των τοποθετήσεων, αποθηκεύτηκαν με επιτυχία...';
            }else{
                $message = 'Η Αίτηση σας, τύπου '. $aitisi_type . ' για ' . $currentReason . ', αποθηκεύτηκε με επιτυχία.';
            }
            return $message;
        }
        //ajaxPermanentlyDeleteRequest
        public function ajaxPermanentlyDeleteRequest(Request $request)
        {
            $this->getUser();

            if ($request->ajax()){
                $unique_id = $request->get('unique_id');

                OrganikiRequest::where('teacher_id', $this->user->userable->id)
                    ->where('unique_id', $unique_id)
                    ->where('protocol_number', null)
                    ->delete();

                flash()->overlayE('Προσοχή!!!', 'Η Αίτηση διαγράφηκε Οριστικά και ΔΕΝ θα ληφθεί υπόψη');

                return route('welcome');

            }
        }
        //ajaxSentLeitourgikaRequestForProtocolYperarithmos
        public function ajaxSentLeitourgikaRequestForProtocol(Request $request)
        {            
            $message = $this->saveRequestLeitourgika($request);

            $this->giveLeitourgikaProtocol($request);

            return route('Request::archives');
        }
        //giveLeitourgikaProtocolYperarithmos
        private function giveLeitourgikaProtocol(Request $request)
        {            
            if($request->ajax()){
                $this->getUser();

                $temp = $request->get('unique_id');
                $currentReason = $request->get('currentReason');
                $aitisi_type = $request->get('aitisi_type');
                $sistegazomeno = $request->get('sistegazomeno');

                if($temp != ""){
                    $this->unique_id = $temp;
                }else{
                    $this->unique_id = uniqid(\Auth::id());
                }

                $request_teacher = OrganikiRequest::with('prefrences')
                    ->where('teacher_id', $this->user->userable->id)
                    ->where('groupType', $currentReason)
                    ->where('unique_id', $this->unique_id)
                    ->get();

                if (!$request_teacher->isEmpty()){
                        DB::beginTransaction();

                        $teacher = $this->user->userable;

                        $protocolRequest = array(
                            'p_date'    => date('d/m/Y'),
                            'from_to'      => $this->user->full_name . ' του '. $teacher->middle_name,
                            // 'type'      => $currentReason,
                            'teacher_type' => $currentReason,
                            'type'      => 0, // Εισερχόμενο
                            'subject'   => 'Αίτηση για '. $currentReason,
                            'f_id'      => 3,
                            'description' => ''
                        );

                        $protocol = Protocol::create($protocolRequest);                        

                        $teacher_folder = str_slug($this->user->last_name) . md5($teacher->id);

                        $file_path = $teacher_folder . '/'. $request_teacher->first()->unique_id . '.pdf';

                        \Storage::makeDirectory('teachers/'.$teacher_folder);

                        foreach($request_teacher as $record){
                            if($record->protocol_number == null){
                                $record->update([
                                    'protocol_number'   => $protocol->id,
                                    'file_name'         => $file_path,
                                ]);
                            }
                        }

                        flash()->html('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.');
                
                        $attr = [
                            'aitisi_type'       => $aitisi_type,
                            'sistegazomeno'     => $sistegazomeno,
                            'request_teacher'   => $request_teacher,
                            'protocol'          => $protocol,
                            'user'              => $this->user,
                            'teacher'           => $teacher,
                            'monimos'           => $teacher->teacherable,
                            'organiki_name'     => $teacher->myschool->organiki_name,
                            'myschool'          => $teacher->myschool,
                            'edata'             => $teacher->myschool->edata,
                            'recall_stamp'  => null
                        ];

                        $this->makePDFforRequests($attr, $teacher_folder, $file_path, 'pysde::aitisis.leitourgika.teacher.PDF.pdfOrganikaYperarithmos');

                        // $user = Role::where('slug', 'pysde_secretary')->first()->users->first();

                        $user = User::where('email', config('requests.MAIL_PYSDE'))->first();

                        $email = $this->getTheCorrectMail($this->user);

                        Notification::send($user, new TeacherToPysdeNotification($this->user,[
                            'title'         => 'Δήλωση προτιμήσεων για '. $currentReason,
                            'description'   => "ο Εκπαιδευτικός {$this->user->full_name} έστειλε αίτηση προτιμήσεων για $currentReason",
                            'type'          => 'warning',
                            'url'           => route('PysdeAdmin::indexRequests'),
                            'subject'       => "[$currentReason] Προτιμήσεις Σχολείων",
                            'file'          => storage_path('app/teachers/'.$file_path),
                            'mail'          => $email
                        ], 'pysde::email.teacher-organika'));

                    DB::commit();
                }
            }

        }
        //createPdfFile
        private function createPdfFile($request_teacher, $protocol, $file_path)
        {
            if($request_teacher->first()->situation == 'E3'){
                $analytika = $this->analytika;
                $entopiotita = $this->entopiotita;
                $sinipiretisi = $this->sinipiretisi;

                $pdf = \PDF::loadView('pysde::aitisis.leitourgika.teacher.PDF.pdf'. $request_teacher->first()->situation, compact('request_teacher','protocol', 'analytika', 'entopiotita', 'sinipiretisi'));
            }else{
                $pdf = \PDF::loadView('pysde::aitisis.leitourgika.teacher.PDF.pdf'. $request_teacher->first()->situation, compact('request_teacher','protocol'));
            }
//            $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));
            $pdf->setPaper('A4', 'portrait')->save(storage_path('app/teachers/'.$file_path));
        }
        //sentAitisiToTeacherAndToPysde
        private function sentAitisiToTeacherAndToPysde($protocol, $date, $file_path, $user)
        {
            $viewForPysde = 'pysde::email.sentTeacherRequestToPysde';
            $subjectForPysde = 'Αίτηση ' . $protocol->type . ' από τον εκπαιδευτικό '. $user->first_name . ' ' . $user->last_name;
            $toMailPysde = 'nicsmyrn@gmail.com'; //pysde@dide.chan.sch.gr
            $toNamePysde = 'ΠΥΣΔΕ ΧΑΝΙΩΝ';
            $teacher_name = $user->full_name;

            $viewForTeacher = 'pysde::email.sentTeacherRequest';
            $subjectForTeacher = 'Αίτηση ' . $protocol->type . ' προς ΠΥΣΔΕ';
            $toMailTeacher = $user->email;
            $toNameTeacher = $user->full_name;

            $data = array(
                'protocol_name' => $protocol->protocol_name,
                'type'          => $protocol->type,
                'date'          => $date,
                'fullname'      => $teacher_name
            );

            \Mail::send($viewForPysde, $data, function($message) use ($subjectForPysde, $file_path, $toMailPysde, $toNamePysde){
                $message->to($toMailPysde, $toNamePysde)
                    ->subject($subjectForPysde)
                    ->attach(storage_path('app/teachers/'.$file_path));
            });
            \Mail::send($viewForTeacher, $data, function($message) use ($subjectForTeacher, $file_path, $toMailTeacher, $toNameTeacher){
                $message->to($toMailTeacher, $toNameTeacher)
                    ->subject($subjectForTeacher)
                    ->attach(storage_path('app/teachers/'.$file_path));
            });
        }
    //############################### NEW VUE 2  END ###############################


    public function ajaxCreateYperarithmia(Request $request)
    {
        $this->getUser();

        $unique_id = uniqid($this->user->id);

        if ($request->ajax()) {
            $teacher_folder = str_slug($this->user->last_name) . md5($this->user->userable->id);
            $file_path = $teacher_folder . '/' . $unique_id . '.pdf';

            DB::beginTransaction();
                $protocol = Protocol::create([
                    'p_date'    =>  date('d/m/Y'),
                    'from_to'      => $this->user->full_name . ' του '. $this->user->userable->middle_name,
                    // 'type'      => 'Επιθυμία Υπεραριθμίας',
                    'teacher_type' => 'Επιθυμία Υπεραριθμίας',
                    'type'      => 0, // Εισερχόμενο
                    'subject'   => 'Αίτηση Επιθυμίας Υπεραριθμίας',
                    'f_id'      => 2,   // Φ.2.1
                    'description'   => ''
                ]);

                $request_teacher = RequestTeacherYperarithmia::create([
                    'protocol_number'       => $protocol->id,
                    'year'                  => date('Y'),
                    'unique_id'             => $unique_id,
                    'teacher_id'            => $this->user->userable->id,
                    'want_yperarithmia'     => $request->get('want_yperarithmia'),
                    'school_organiki'       => $this->user->userable->myschool->organiki_name,
                    'file_name'             => $file_path,
                    'date_request'          => Carbon::now(),
                    'name'                  => $this->user->full_name
                ]);

            $myschool = $this->user->userable->myschool;

            $attributes = [
                'teacher_name'  => $this->user->full_name,
                'protocol_name' => $protocol->protocol_name,
                'myschool'           => $myschool,
                'new_klados'    => $myschool->new_klados,
                'new_eidikotita_name'   => $myschool->new_eidikotita_name,
                'want_yperarithmia' => $request_teacher->want_yperarithmia,
                'organiki_name' => $myschool->organiki_name,
                'date_request'  => $request_teacher->date_request,
                'recall_stamp'  => null
            ];
            
            $this->makePDFforRequests($attributes, $teacher_folder, $file_path, 'pysde::aitisis.organika.teacher.pdf.YP_PDF');

            // $user = Role::where('slug', 'pysde_secretary')->first()->users->first();
            $user = User::where('email', config('requests.MAIL_PYSDE'))->first();

            $email = $this->getTheCorrectMail($this->user);


            Notification::send($user, new TeacherToPysdeNotification($this->user,[
                'title'         => 'Αίτηση Χαρακτηρισμού ΥΠΕΡΑΡΙΘΜΟΥ',
                'description'   => "Υπεράριθμοι",
                'type'          => 'primary',
                'url'           => route('PysdeAdmin::indexRequests'),
                'subject'       => 'Αίτηση για χαρακτηρισμό ως Υπεράριθμος',
                'file'          => storage_path('app/teachers/'.$file_path),
                'mail'          => $email
            ],'pysde::email.teacher-yperarithmia'));

            flash()->html('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.','success');
            
            DB::commit();
        }
    }

    public function ajaxCreateRecallRequest($request)
    {
        $this->getUser();

        $unique_id = uniqid($this->user->id);

            DB::beginTransaction();

            $teacher_folder = str_slug($this->user->last_name) . md5($this->user->userable->id);
            $file_path = $teacher_folder . '/' . $unique_id . '.pdf';

            $protocol = Protocol::create([
                'p_date'    =>  date('d/m/Y'),
                'from_to'      => $this->user->full_name . ' του '. $this->user->userable->middle_name,
                // 'type'      => 'ΑΝΑΚΛΗΣΗ',
                'type'      => 0, // Εισερχόμενο
                'teacher_type' => 'ΑΝΑΚΛΗΣΗ',
                'subject'   => 'ΑΝΑΚΛΗΣΗ ΑΙΤΗΣΗΣ',
                'f_id'      => 2,   // Φ.2.1
                'description'   => 'με αριθμό πρωτοκόλλου: '. $request->unique_id
            ]);

            $myschool = $this->user->userable->myschool;

            $attributes = [
                'teacher_name'  => $this->user->full_name,
                'protocol_name' => $protocol->protocol_name,
                'myschool'           => $myschool,
                'new_klados'    => $myschool->new_klados,
                'new_eidikotita_name'   => $myschool->new_eidikotita_name,
                'organiki_name' => $myschool->organiki_name,
                'date_request'  => $protocol->created_at,
                'prototype_unique_id' => $request->unique_id,
                'prototype_protocol_name'  => $request->protocol_name,
                'recall_stamp'  => null
            ];

            $this->makePDFforRequests($attributes, $teacher_folder, $file_path, 'pysde::aitisis.organika.teacher.pdf.RECALL');


            flash()->html('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.', 'success');

            // $user = Role::where('slug', 'pysde_secretary')->first()->users->first();
            $user = User::where('email', config('requests.MAIL_PYSDE'))->first();

            $email = $this->getTheCorrectMail($this->user);

            Notification::send($user, new TeacherToPysdeNotification($this->user,[
                'title'         => 'Ανάκληση Αίτησης',
                'description'   => "αίτημα ανάκλησης αίτησης με κωδικό $unique_id",
                'type'          => 'danger',
                'url'           => route('PysdeAdmin::indexRequests'),
                'subject'       => 'ΑΝΑΚΛΗΣΗ αίτησης',
                'file'          => storage_path('app/teachers/'.$file_path),
                'mail'          => $email
            ],'pysde::email.teacher-cancelation-request'));

            DB::commit();
    }

    public function createPreparation()
    {
        $this->getUser();        

        return view('pysde::aitisis.prepare_create');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pysde::aitisis.index');
    }

    public function indexOrganiki()
    {
        $requests = OrganikiRequest::where('teacher_id', $this->user->userable->id)->get();

        $title = 'Αρχείο Αιτήσεων για Οργανική Θέση';
        $type = 'organiki';

        return view('teacher.index', compact('requests','title', 'type'));
    }

    public function indexYperarithmia()
    {
        $this->getUser();

        $requests = RequestTeacherYperarithmia::where('teacher_id', $this->user->userable->id)->get();

        $title = 'Αρχείο Αιτήσεων Επιθυμίας Υπεραριθμίας';
        $type = 'yperarithmia';

        return view('pysde::aitisis.index', compact('requests','title','type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type, $unique_id = null)
    {        
        $this->getUser();
                
        $can_make_E1 = false;

        if ($unique_id == null){
            $unique_id = uniqid($this->user->id);
        }

        $this->checkAitisisAvailability($type);

        $this->denies('create_'.$type.'_request');

        $eidikotita = $this->findEidikotita();
        $organiki = $this->findOrganiki();   
        
        if($type == 'E5'){
            if($this->user->userable->myschool->yadescription != 'covid19'){
                flash()->overlayE('Προσοχή!!!', "Δεν είστε Αναπληρωτής Covid 19.");

                return redirect()->route('Request::Prepare');
            }
        }
        
        if($type == 'E1'){            
            $monimos =  $this->user->userable->teacherable;
            if($monimos != null){
                $organiki_id = $monimos->organiki_id;
            }else{
                $organiki_id = 0;
            }            
            $teacher_school = School::find($organiki_id);

            if($teacher_school != null){
                $teacher_eidikotita =  $teacher_school->leitourgika_kena->where('eidikotita_slug', $this->user->userable->myschool->eidikotita)->first();
                if($teacher_eidikotita != null){                    
                    $difference = $teacher_eidikotita->pivot->difference;
                    
                    if($difference >= 7){
                        $can_make_E1 = true;
                    }
                }
            }else{
                $can_make_E1 = true;
            }

            if(!$can_make_E1){
                $url = route('Request::archives');
                flash()->html('Προσοχή!!!', "<strong>ΔΕΝ υπάρχει</strong> Λειτουργική Υπεραριθμία στο Σχολείο σας.");

                return redirect()->route('Request::Prepare');
            }
        }
            if(array_key_exists($type, $this->request_type)){
                $value = $this->request_type[$type];
                if(is_array($value)){
                    if($this->user->userable->myschool->organiki_id < 1000){
                        $groupType =  $value[1];
                    }else{
                        $groupType =  $value[0];
                    }
                }else{
                    $groupType =  $value;
                }
            }else{
                $groupType =  $type;
            }            

            // $old_requests = OrganikiRequest::where('teacher_id', $this->user->userable->id)->where('groupType', $groupType)->first();

            $old_requests = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                                ->where('groupType', $groupType)
                                ->where(function($q){
                                    $q->whereNull('recall_situation')
                                        ->orWhereIn('recall_situation', ['ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ', 'NULL', '', 'Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ']);
                                })
                                ->where('date_request', '>', config('requests.aitisis_E2'))
                                ->first();

            if($old_requests == null){
                return view('pysde::aitisis.leitourgika.teacher.create', compact('type', 'eidikotita', 'organiki', 'unique_id'));
            }else{       
                if($unique_id == $old_requests->unique_id){
                    return view('pysde::aitisis.leitourgika.teacher.create', compact('type', 'eidikotita', 'organiki', 'unique_id'));
                }else{
                    $url = route('Request::archives');
                    flash()->html('Προσοχή!!!', "υπάρχει αίτηση  με κωδικό: <strong>$old_requests->unique_id</strong> Ανατρέξτε στο <a href='$url'>Ιστορικό Αιτήσεων</a> για προβολή της.");
                }
            }

        return redirect()->route('Request::Prepare');

    }

    public function organikiCreate($type, $unique_id = null)
    {        
        $new_type = $this->types[$type];
        
        $this->checkAitisisAvailability($new_type);
        
        $this->denies('create_'.$new_type.'_request');
        
        if ($unique_id == null){
            $unique_id = uniqid($this->user->id);
        }        

        $eidikotita = $this->findEidikotita();
        $organiki = $this->findOrganiki();
        
        if($new_type == 'Υπεραρ'){
            return view('pysde::aitisis.organika.teacher.epithimia-yperarithmias', compact('new_type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'ΟργανικήΥπ'){
            $rights =  $this->user->userable->myschool->rights;

            if($rights != null){
                if($rights->year == Carbon::now()->year && $rights->onomastika_yperarithmos){
                    /*
                                        ->where(function($q){
                                            $q->whereNull('recall_situation')
                                                ->orWhereIn('recall_situation', ['ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ', 'NULL', '', 'Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ']);
                                        })

                                        για κάποιο λόγο κάθε φορά το αλλάζω....
                    */
                    $organikiYperarithmiaRequest = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                                                    ->where('groupType', 'ΟργανικήΥπεράριθμου')
                                                    ->where(function($q){
                                                        $q->whereNull('recall_situation')
                                                            ->orWhereIn('recall_situation', ['ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ', 'NULL', '', 'Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ']);
                                                    })
                                                    ->first();
                    if($organikiYperarithmiaRequest == null){
                        return view('pysde::aitisis.organika.teacher.aitisi-yperarithmou', compact('new_type', 'eidikotita', 'organiki', 'unique_id'));                        
                    }else{   
                        if($unique_id == $organikiYperarithmiaRequest->unique_id){
                            return view('pysde::aitisis.organika.teacher.aitisi-yperarithmou', compact('new_type', 'eidikotita', 'organiki', 'unique_id'));                        
                        }else{
                            $url = route('Request::archives');
                            flash()->html('Προσοχή!!!', "υπάρχει αίτηση Οργανικής Υπεραριθμίας με κωδικό: <strong>$organikiYperarithmiaRequest->unique_id</strong> Ανατρέξτε στο <a href='$url'>Ιστορικό Αιτήσεων</a> για προβολή της.");
                        }
                    }
                }else{
                    flash()->overlayE('Προσοχή!!!', 'Δεν έχετε κριθεί ΟΝΟΜΑΣΤΙΚΑ ΥΠΕΡΑΡΙΘΜΟΣ για το τρέχον έτος');
                }
            }else{
                flash()->overlayE('Προσοχή!!!', 'Δεν έχετε κριθεί ΟΝΟΜΑΣΤΙΚΑ ΥΠΕΡΑΡΙΘΜΟΣ');
            }
            return redirect()->route('Request::Prepare');
        }elseif($new_type == 'Βελτίωση'){
            $myschool = $this->user->userable->myschool;
            $edata = $myschool->edata;
            $rights = $myschool->rights;
            $rights_flag = false;
            $current_year = Carbon::now()->year;

            $start_date = Carbon::parse(config('requests.start_aitisis_veltiwsis'));


            // if($rights != null){
            //     if($rights->year == Carbon::now()->year && $rights->onomastika_yperarithmos){
            //         $rights_flag = true;
            //     }
            // }
            
            if($edata != null ){ // || $rights_flag
                if($edata->aitisi_veltiwsis){ // || rights_flag
                    $aitisiVeltiwsis = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                                        ->where('groupType', 'Βελτίωση')
                                        // ->whereYear('date_request', '=' , $current_year)
                                        ->where('date_request', '>', $start_date)
                                        ->where(function($q){
                                            $q->whereNull('recall_situation')
                                                ->orWhereIn('recall_situation', ['ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ', 'NULL', '', 'Η ΑΙΤΗΣΗ ΔΕΝ ΑΝΑΚΛΗΘΗΚΕ']);
                                        })
                                        // ->toSql();
                                        ->first();

                    $aitiseis_diathesis = Carbon::parse(config('requests.aitisis_diathesis'));

                    $now = Carbon::now();
                    $url = route('Request::archives');

                    if($aitisiVeltiwsis == null){                        
                        if($now->gt($aitiseis_diathesis)){
                            if($myschool->organiki_name == 'Διάθεση ΠΥΣΔΕ'){
                                return view('pysde::aitisis.organika.teacher.aitisi-gia-veltiosi', compact('new_type', 'eidikotita', 'organiki', 'unique_id'));                        
                            }else{
                                flash()->html('Προσοχή!!!', "<strong>ΔΕΝ</strong> είστε στη ΔΙΑΘΕΣΗ του ΠΥΣΔΕ",'success');
                                return redirect()->route('Request::Prepare'); 
                            }
                        }

                        return view('pysde::aitisis.organika.teacher.aitisi-gia-veltiosi', compact('new_type', 'eidikotita', 'organiki', 'unique_id'));                        
                    }else{  
                        if($unique_id == $aitisiVeltiwsis->unique_id){
                            return view('pysde::aitisis.organika.teacher.aitisi-gia-veltiosi', compact('new_type', 'eidikotita', 'organiki', 'unique_id'));                        
                        }else{

                            if($now->gt($aitiseis_diathesis)){
                                if($myschool->organiki_name == 'Διάθεση ΠΥΣΔΕ'){
                                    return view('pysde::aitisis.organika.teacher.aitisi-gia-veltiosi', compact('new_type', 'eidikotita', 'organiki', 'unique_id'));                        
                                }else{
                                    flash()->html('Προσοχή!!!', "<strong>ΔΕΝ</strong> είστε στη ΔΙΑΘΕΣΗ του ΠΥΣΔΕ",'success');
                                    return redirect()->route('Request::Prepare');
                                }
                            }
                            flash()->html('Προσοχή!!!', "υπάρχει αίτηση Βελτίωσης - Οριστικής Τοποθέτησης με κωδικό: <strong>$aitisiVeltiwsis->unique_id</strong> Ανατρέξτε στο <a href='$url'>Ιστορικό Αιτήσεων</a> για προβολή της.",'success');
                        }
                    }
                }else{
                    flash()->html("Προσοχή!", "<strong>ΔΕΝ</strong> έχετε κάνει αίτηση Βελτίωσης/Οριστικής Τοποθέτησης <strong>(το Νοέμβρη) ή </strong> <span style='color:red'>δεν έχετε δικαίωμα στην παρούσα φάση να χρησιμοποιήσετε την αίτηση αυτή.</span>","error");
                }
            }else{
                flash()->html("Προσοχή!", "Λάθος 73847. Επικοινωνήστε με τον διαχειριστή της Πασιφάη","error");
            }
            return redirect()->route('Request::Prepare');
        }elseif($new_type == 'Μοριοδ'){
            return view('teacher.aitisis.moriodotisis', compact('new_type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'ΜοριοδΑ'){
            return view('teacher.aitisis.moriodotisis', compact('new_type', 'eidikotita', 'organiki'));
        }else{
            return 'wrong view';
        }
    }

    // public function create($type, $unique_id = null)
    // {
    //     $this->getUser();

    //     if ($unique_id == null){
    //         $unique_id = uniqid($this->user->id);
    //     }

    //     $this->checkAitisisAvailability($type);

    //     $this->denies('create_'.$type.'_request');

    //     $eidikotita = $this->findEidikotita();
    //     $organiki = $this->findOrganiki();

    //     return view('pysde::aitisis.leitourgika.teacher.create', compact('type', 'eidikotita', 'organiki', 'unique_id'));
    // }


    public function organikiCreateTest($type)
    {        
        $new_type = $this->types[$type];
//        $this->denies('create_'.$new_type.'_request');

        $eidikotita = $this->findEidikotita();
        $organiki = $this->findOrganiki();


        if($new_type == 'Υπεραρ'){
            return view('teacher.aitisis.organikes.epithimia-yperarithmias', compact('type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'ΟργανικήΥπ'){
            return view('teacher.aitisis.organikes.aitisi-yperarithmou', compact('type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'Βελτίωση'){
            return view('teacher.aitisis.organikes.aitisi-gia-veltiosi', compact('type', 'eidikotita', 'organiki'));
        }else{
            return 'wrong view';
        }
    }

    public function sentAttachementsMoriodotisi(Request $request)
    {
        $this->getUser();

        $unique_id = uniqid($this->user->id);



        $teacher_folder = md5($this->user->userable->id);
        $file_path = $teacher_folder . '/'. $unique_id . '.pdf';
        \Storage::makeDirectory('teachers/'.$teacher_folder);

        DB::beginTransaction();
        $protocol = Protocol::create([
            'p_date'    =>  date('d/m/Y'),
            'from_to'      => $this->user->full_name . ' του '. $this->user->userable->middle_name,
            'type'      => 0, // Εισερχόμενο
            'teacher_type'      => 'Μοριοδότηση '. $this->moriodotisi[$request->get('type')],
            'subject'   => 'Αίτηση Μοριοδότησης '. $this->moriodotisi[$request->get('type')],
            'f_id'      => 1,   // Φ.2.1
        ]);

        $description = '';

        if($this->moriodotisi[$request->get('type')] == 'ΜΕΤΑΘΕΣΗΣ'){
            $description = 'όπως υπολογιστούν τα μόρια μετάθεσης για τις λειτουργικές τοποθετήσεις τον Σεπτέμβρη του 2018';
        }elseif($this->moriodotisi[$request->get('type')] == 'ΑΠΟΣΠΑΣΗΣ'){
            $description = 'όπως υπολογιστούν τα μόρια απόσπασης';
        }

        $request_teacher = RequestTeacher::create([
            'unique_id'             => $unique_id,
            'protocol_number'       => $protocol->id,
            'teacher_id'            => $this->user->userable->id,
            'file_name'             => $file_path,
            'aitisi_type'           => 'ΜΟΡΙΑ '. $this->moriodotisi[$request->get('type')],
            'date_request'          => Carbon::now(),
            'subject'               => 'ΑΙΤΗΣΗ ΜΟΡΙΟΔΟΤΗΣΗΣ '. $this->moriodotisi[$request->get('type')],
            'description'           => $description
        ]);
        DB::commit();

        $pdf = PDF::loadView('teacher.aitisis.moriodotisi', compact('request_teacher','protocol', 'type'));

        $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

        $attachments = array();

        $attachments[] = [
            'real_path' => storage_path('app/teachers/'.$file_path),
            'options' =>
                [
                    'as'  => 'ΑΙΤΗΣΗ ΜΟΡΙΟΔΟΤΗΣΗΣ '.$this->moriodotisi[$request->get('type')],
                    'mime' => 'application/pdf'
                ]
        ];

        for($i=1; $i<=3; $i++){
            if($request->hasFile('attachment'.$i)){
                $attachments[] = [
                    'real_path' => $request->file('attachment'.$i)->getRealPath(),
                    'options' =>
                        [
                            'as'  => 'Δικαιολογητικό_No'.$i,
                            'mime' => $request->file('attachment'.$i)->getMimeType()
                        ]
                ];
            }
        }


        if(\Config::get('requests.sent_mail') && count($attachments) > 0) {
//            Smail::sentTeacherToProswpikoForMoriodotisi($attachments, $this->user->full_name, $this->user->userable->myschool->afm, $this->user->userable->myschool->am, $this->user->userable->id, $this->moriodotisi[$request->get('type')]);
//            Smail::sentAitisiToTeacher($protocol, $request_teacher, $file_path, $this->user);
        }

//        event(new TeacherRequestForMoriodotisi($this->user, [
//            'title' => 'Αίτημα Μοριοδότησης',
//            'description' => "Ο Εκπαιδευτικός <span class='notification_full_name'>&laquo;{$this->user->full_name}&raquo;</span> του {$this->user->userable->middle_name} έχει αιτηθεί να υπολογιστούν τα <span class='notification_full_name'> μόρια {$this->moriodotisi[$request->get('type')]} </span>",
//            'type' => 'danger',
//            'teacherID' => $this->user->userable->fid != null ? $this->user->userable->fid : $this->user->userable->id
//        ]));

        flash()->html('Συγχαρητήρια '.$this->user->first_name,'Η Αίτηση <strong>'.$this->moriodotisi[$request->get('type')].'</strong> / Δικαιολογητικά στάλθηκε στο τμήμα Γ Προσωπικού. Ο αριθμός της Αίτησης είναι: <strong>' . $protocol->protocol_name . '</strong>', 'success');

        return redirect()->route('welcome');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aitisi = $this->validateTeacherRequest($request);

        $this->denies('create_'.$aitisi['aitisi_type'].'_request');

        if ($request->ajax()){      //if not a simple aitisi then
            DB::beginTransaction();
            $new_request = RequestTeacher::create($aitisi);

            $i = 1;
            foreach($aitisi['prefrences'] as $prefrence){
                Prefrences::create([
                    'request_id'    => $new_request->id,
                    'order_number'  => $i,
                    'school_name'   => $prefrence['school_name']
                ]);
                $i = $i + 1;
            }
            $this->giveProtocol($request, $new_request->unique_id);
            DB::commit();
//            return $request->all();
        }else{  //if simple aitisi
            DB::beginTransaction();
            $new_request = RequestTeacher::create($aitisi);

            $this->giveProtocol($request, $new_request->unique_id);
            DB::commit();

            return redirect()->route('Aitisi::archives');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($unique_id)
    {
        $request = RequestTeacher::where('unique_id', $unique_id)->first();
        $eidikotita = $this->findEidikotita();
        $organiki = $this->findOrganiki();

        if($request->protocol_number == null){
            return view('teacher.edit', compact('unique_id', 'request', 'eidikotita', 'organiki'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aitisi = $this->validateTeacherRequest($request);

        $request_teacher = RequestTeacher::where('unique_id', $id)->first();

        if ($request->ajax()){ // if not a simple aitisi
            DB::beginTransaction();
            $request_teacher->description = $aitisi['description'];
            $request_teacher->date_request = $aitisi['date_request'];
            $request_teacher->hours_for_request = $aitisi['hours_for_request'];
            $request_teacher->reason = $aitisi['reason'];
            $request_teacher->stay_to_organiki = $aitisi['stay_to_organiki'];
            $request_teacher->save();

            $delete_rows = Prefrences::where('request_id', $request_teacher->id)->delete();

            $i = 1;
            foreach($aitisi['prefrences'] as $prefrence){
                Prefrences::create([
                    'request_id'    => $request_teacher->id,
                    'order_number'  => $i,
                    'school_name'   => $prefrence['school_name']
                ]);
                $i = $i + 1;
            }
            $this->giveProtocol($request, $id);
            DB::commit();

        }else{  //if simple aitisi
            DB::beginTransaction();
            $request_teacher->update([
                'subject' => $aitisi['subject'],
                'schools_that_is' => $aitisi['schools_that_is'],
                'description' => $aitisi['description'],
                'date_request' => $aitisi['date_request'],
            ]);

            $this->giveProtocol($request,$request_teacher->unique_id);
            DB::commit();

            return redirect()->route('Aitisi::archives');
        }

    }

    private function giveOrganikiProtocol(Request $request)
    {
        if($request->ajax()){
            $request_teacher = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                ->where('aitisi_type', 'Ονομαστικά Υπεράριθμος')
                ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
                ->first();

            if ($request_teacher != null){
                $user = $request_teacher->teacher->user;
                $teacher = $request_teacher->teacher;

                if ($request_teacher->protocol_number == null && $request_teacher->file_name == null){
                    $protocolRequest = array(
                        'p_date'    => date('d/m/Y'),
                        'from_to'      => $user->full_name . ' του '. $teacher->middle_name,
                        'type'      => 0, // Εισερχόμενο
                        'teacher_type' => $request_teacher->aitisi_type,
                        // 'type'      => $request_teacher->aitisi_type,
                        'subject'   => 'Αίτηση Ονομαστικά Υπεράριθμου για Οργανική',
                        'f_id'      => 2
                    );
                    $teacher_folder = md5($teacher->id);
                    $file_path = $teacher_folder . '/check_' . $request_teacher->unique_id . '.pdf';
                    \Storage::makeDirectory('teachers/'.$teacher_folder);

                    DB::beginTransaction();
                    $protocol = Protocol::create($protocolRequest);
                    $request_teacher->update([
                        'protocol_number'   => $protocol->id,
                        'file_name'         => $file_path
                    ]);
                    DB::commit();

                    $pdf = PDF::loadView('teacher.aitisis.organikes.PDF-Organika-Yperarithmos', compact('request_teacher','protocol'));

                    $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

//                    event(new TeacherSendNotificationToPysde(\Auth::user(), $request['a_type'], [
//                        'title' => 'Αίτηση για Οργανική',
//                        'description' => "Ο <span class='notification_full_name'>&laquo;{$user->full_name}&raquo;</span> έχει στείλει στο ΠΥΣΔΕ αίτηση ως ονομαστικά Υπεράριθμος για Οργανική <strong>{$request_teacher->aitisi_type}</strong> με αριθμό αίτησης: {$protocol->protocol_name}",
//                        'type' => 'primary'
//                    ]));
                    flash()->html('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλθεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.');
                    if(\Config::get('requests.sent_mail')) {
//                        Smail::sentAitisiToTeacher($protocol, $request_teacher, $file_path, $user);
//                        Smail::sentAitisiToPysde($protocol, $request_teacher, $file_path, $user);
                    }
                }else{
                    flash()->overlayE('Σφάλμα!!!', 'Δε δόθηκε κανένας αριθμός  στην αίτηση');
                }
            }else{
                flash()->overlayE('Σφάλμα!!!', 'Error code : 1141');

            }
        }

        return redirect()->back();


    }

    private function giveOrganikiProtocolVeltiwsi(Request $request)
    {
        if($request->ajax()){
            $request_teacher = OrganikiRequest::where('teacher_id', $this->user->userable->id)
//                ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
                ->where(DB::raw('TIMEDIFF("2018-05-24 18:00:00", date_request)'), '<', 0)       //TODO MUST GHANGE THIS
                ->where('aitisi_type', 'Βελτιώση - Οριστική τοποθέτηση')
                ->first();

            if ($request_teacher != null){
                $user = $request_teacher->teacher->user;
                $teacher = $request_teacher->teacher;

                if ($request_teacher->protocol_number == null && $request_teacher->file_name == null){
                    $protocolRequest = array(
                        'p_date'    => date('d/m/Y'),
                        'from_to'      => $user->full_name . ' του '. $teacher->middle_name,
                        'type'      => 0, // Εισερχόμενο
                        'teacher_type'      => $request_teacher->aitisi_type,
                        'subject'   => 'Αίτηση Δήλωση για Βελτίωση - Οριστική τοποθέτηση',
                        'f_id'      => 3
                    );
                    $teacher_folder = md5($teacher->id);
                    $file_path = $teacher_folder . '/check_' . $request_teacher->unique_id . '.pdf';
                    \Storage::makeDirectory('teachers/'.$teacher_folder);

                    DB::beginTransaction();
                    $protocol = Protocol::create($protocolRequest);
                    $request_teacher->update([
                        'protocol_number'   => $protocol->id,
                        'file_name'         => $file_path
                    ]);
                    DB::commit();

                    $pdf = PDF::loadView('teacher.aitisis.organikes.PDF-Veltiwsi', compact('request_teacher','protocol'));

                    $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

//                    event(new TeacherSendNotificationToPysde(\Auth::user(), $request['a_type'], [
//                        'title' => 'Αίτηση για Οργανική',
//                        'description' => "Ο <span class='notification_full_name'>&laquo;$user->full_name&raquo;</span> έχει στείλει στο ΠΥΣΔΕ αίτηση Βελτίωσης - Οριστικής Τοποθέτησης <strong>{$request_teacher->aitisi_type}</strong> με αριθμό αίτησης: {$protocol->protocol_name}",
//                        'type' => 'primary'
//                    ]));
                    flash()->html('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλθεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.');
                    if(\Config::get('requests.sent_mail')) {
//                        Smail::sentAitisiToTeacher($protocol, $request_teacher, $file_path, $user);
//                        Smail::sentAitisiToPysde($protocol, $request_teacher, $file_path, $user);
                    }
                }else{
                    flash()->overlayE('Σφάλμα!!!', 'Δε δόθηκε κανένας αριθμός  στην αίτηση');
                }
            }
        }



        return redirect()->back();


    }

    private function giveProtocol(Request $request, $uid = null)
    {
        if($request->has('sent_request')){
            $request_teacher = RequestTeacher::where('unique_id', $uid)->first();
            if ($request_teacher == null){
                abort(404);
            }
            $user = $request_teacher->teacher->user;
            $teacher = $request_teacher->teacher;

            if ($request_teacher->protocol_number == null && $request_teacher->file_name == null){
                $protocolRequest = array(
                    'p_date'    => date('d/m/Y'),
                    'from_to'      => $user->full_name,
                    'type'      => 0, // Εισερχόμενο
                    'teacher_type'      => $request_teacher->aitisi_type,
                    'subject'   => 'Αίτηση '. $request_teacher->aitisi_type,
                    'f_id'      => $request_teacher->type
                );
//3#####################################################33###########################
                $teacher_folder = md5($teacher->id);
                $file_path = $teacher_folder . '/' . $uid . '.pdf';
                \Storage::makeDirectory('teachers/'.$teacher_folder);

                DB::beginTransaction();
                $protocol = Protocol::create($protocolRequest);
                $request_teacher->update([
                    'protocol_number'   => $protocol->id,
                    'file_name'         => $file_path
                ]);
                DB::commit();

                if($request_teacher->aitisi_type == 'ΑΠΛΗ'){
                    $pdf = PDF::loadView('teacher.simpleAitisiPDF', compact('request_teacher', 'protocol'));
                }else{
                    $pdf = PDF::loadView('teacher.aitisiPDF', compact('request_teacher','protocol'));
                }

                $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

//                event(new TeacherSendNotificationToPysde(\Auth::user(), $request['a_type'], [
//                    'title' => 'Αίτηση Εκπαιδευτικού',
//                    'description' => "Ο <span class='notification_full_name'>&laquo;{$user->full_name}&raquo;</span> έχει στείλει στο ΠΥΣΔΕ αίτηση <strong>{$request_teacher->aitisi_type}</strong> με αριθμό αίτησης: {$protocol->protocol_name}",
//                    'type' => 'primary'
//                ]));
                flash()->html('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλθεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.');
                if(\Config::get('requests.sent_mail')) {
//                    Smail::sentAitisiToTeacher($protocol, $request_teacher, $file_path, $user);
//                    Smail::sentAitisiToPysde($protocol, $request_teacher, $file_path, $user);
                }
            }else{
                flash()->overlayE('Σφάλμα!!!', 'Δε δόθηκε κανένας αριθμός  στην αίτηση');
            }
            return redirect()->back();

        }elseif($request->has('save_request')){
            flash()->success('Συγχαρητήρια','Η αίτηση αποθηκεύτηκε με επιτυχία');
        }elseif($request->has('update_request')){
            flash()->success('', 'Η αίτησή ενημερώθηκε με επιτυχία');
        }else{
            flash()->overlayE('Σφάλμα22!!!', 'Δε δόθηκε κανένας αριθμός  στην αίτηση');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return 'deleted';
    }

    public function ajaxEdit($unique_id){
        $requestTeacher = RequestTeacher::with('prefrences')->where('unique_id', $unique_id)->first();

        return $requestTeacher;
    }

    public function deleteRowRequest($unique_id, $type)
    {
        if($type == 'organiki'){
            $requestTeacher = OrganikiRequest::where('unique_id', $unique_id)->first();
        }elseif($type == 'simple'){
            $requestTeacher = RequestTeacher::where('unique_id', $unique_id)->first();
        }elseif($type == 'yperarithmia'){
            $requestTeacher = RequestTeacherYperarithmia::where('unique_id', $unique_id)->first();
        }else{
            abort(403);
        }

        if($requestTeacher->protocol_number == null){
            $requestTeacher->delete();

            flash()->success('','Η αίτηση διεγράφη με επιτυχία');

            return 'success';
        }else{
            flash()->overlayE('Προσοχή!','Η ενέργεια που κάνατε δεν είναι αποδεκτή. Ο λογαριασμός σας θα είναι υπό επιτήρηση');

            return 'error';
        }

    }


    public function downloadPDF($unique_id)
    {
        $this->getUser();

        $teacher_folder = str_slug($this->user->last_name) . md5($this->user->userable->id);

        $file = $unique_id.'.pdf';
        $file_path = storage_path('app/teachers/'.$teacher_folder).'/'.$file;


        if (file_exists($file_path)){
            return response()->download($file_path, $file, [
                'Content-Length: '. filesize($file_path)
            ]);
        }else{
            $file = 'check_' . $file;
            $organiki_file = storage_path('app/teachers/'.$teacher_folder).'/'.$file;

            if(file_exists($organiki_file)){
                return response()->download($organiki_file, $file, [
                    'Content-Length: '. filesize($organiki_file)
                ]);
            }

            exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');
        }
    }

    public function recallRequest($unique_id)
    {
        $request = RequestTeacherYperarithmia::where('unique_id', $unique_id)->first();

        if($request==null){
            $request = OrganikiRequest::where('unique_id', $unique_id)->first();
            if($request == null){
                return 'Error';
            }
        }

        $request->recall_date = Carbon::now();
        $request->recall_situation = 'ΥΠΟ ΕΛΕΓΧΟ για ΑΝΑΚΛΗΣΗ';
        $request->save();

        $this->ajaxCreateRecallRequest($request);

        return $request;
    }

    public function temptoHTML()
    {
        return view('teacher.simpleAitisiPDF');
    }

    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(403);
        }
    }

    /**
     * @param Request $request
     */
    private function validateTeacherRequest(Request $request)
    {
        if ($request->ajax()) {

        } else { // if simple request - ΑΠΛΗ Αίτηση
            $this->getUser();

            $this->validate($request, [
                'subject' => 'required',
                'schools_that_is' => 'required',
                'description' => 'required'
            ], [
                'subject.required' => 'Το Θέμα είναι υποχρεωτικό',
                'schools_that_is.required' => 'Το/τα σχολείο/α που υπηρετείς είναι υποχρεωτικό/ά',
                'description.required' => 'Η περιγραφή της αίτησης είναι υποχρεωτική'
            ]);
        }

        return $aitisi = [
            'subject'           => $request->get('subject'),
            'aitisi_type'       => $request->get('a_type'),
            'description'       => $request->get('description'),
            'schools_that_is'   => $request->get('schools_that_is'),
            'prefrences'        => $request->get('prefrences'),
            'hours_for_request' => $request->get('hours_for_request'),
            'stay_to_organiki'  => $request->get('stay_to_organiki'),
            'reason'            => $request->get('reason'),
            'unique_id'         => uniqid($this->user->id),
            'teacher_id'        => $this->user->userable->id,
            'date_request'      => Carbon::now()
        ];
    }

    /**
     * @param $type
     */
    private function checkAitisisAvailability($type)
    {
        if (!in_array($type, \Config::get('requests.aitisis_type'))) {
            abort(404);
        } else {
            $this->getUser();

            if ($this->user->userable->teacherable_type == 'App\Monimos' && $type == 'E5') {
                abort(404);
            } elseif ($this->user->userable->teacherable_type == 'App\Anaplirotis' && in_array($type, ['E1', 'E3', 'E4', 'E6', 'E7', 'Οργανικές', 'Βελτιώσεις'])) {
                abort(404);
            }
        }
    }

    /**
     * @return string
     */
    private function findOrganiki()
    {

        $organiki = School::find($this->user->userable->teacherable->organiki_id);
        
        if ($organiki == null) {
            if ($this->user->userable->teacherable->organiki == 4000) {
                $dieuthinsi = Dieuthinsi::where('identifier', $this->user->userable->teacherable->dieuthinsi)->first();
                if($dieuthinsi != null){
                    $organiki = $dieuthinsi->title;
                }else{
                    $organiki = 'Απόσπαση από άλλο ΠΥΣΔΕ';
                }
                return $organiki;
            } elseif ($this->user->userable->teacherable->organiki == 1000) {
                $organiki = 'Διάθεση';
                return $organiki;
            }
            return $organiki;
        } else {
            $organiki = $organiki->name;

            return $organiki;
        }
    }

    /**
     * @return mixed
     */
    private function findEidikotita()
    {
        $this->getUser();

        $new = NewEidikotita::find($this->user->userable->myschool->new_eidikotita);

        return $new->eidikotita_slug . ' ('. $new->eidikotita_name . ')';
    }

    /**
     * @param $selectedSchools
     * @param $new_request
     */
    private function createOrganikesPrefrences($selectedSchools, $new_request)
    {
        $order_number = 1;
        foreach ($selectedSchools as $school) {
            $prefrences = OrganikiPrefrences::create([
                'request_id' => $new_request->id,
                'school_id' => $school['id'],
                'order_number' => $order_number
            ]);
            $order_number = $order_number + 1;
        }
    }


    private function createPrefrences($selectedSchools, $new_request)
    {
        $order_number = 1;
        foreach ($selectedSchools as $school) {
            $prefrences = Prefrences::create([
                'request_id' => $new_request->id,
                'school_id' => $school['id'],
                'order_number' => $order_number
            ]);
            $order_number = $order_number + 1;
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    private function saveOrganikiRequest(Request $request)
    {
        $selectedSchools = $request->get('selectedSchools');

        DB::beginTransaction();

        $this->getUser();

        $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
            ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
            ->where('aitisi_type', 'Ονομαστικά Υπεράριθμος')
            ->first();

        if ($request == null) {
            $unique_id = uniqid(Auth::id());

            $new_request = OrganikiRequest::create([
                'unique_id' => $unique_id,
                'teacher_id' => $this->user->userable->id,
                'school_organiki' => \Auth::user()->userable->myschool->organiki_name,
                'aitisi_type' => 'Ονομαστικά Υπεράριθμος',
                'date_request' => Carbon::now(),
                'situation' => 'saved'
            ]);

            $this->createOrganikesPrefrences($selectedSchools, $new_request);
            $message = 'Η Αίτηση σας αποθηκεύτηκε με επιτυχία.';
        } else {
            OrganikiPrefrences::where('request_id', $request->id)->delete();
            $this->createOrganikesPrefrences($selectedSchools, $request);
            $request->updated_at = Carbon::now();
            $request->save();
            $message = 'Η Αίτηση σας ενημερώθηκε με επιτυχία.';
        }

        DB::commit();
        return $message;
    }


    private function saveOrganikiRequestVeltiwsi(Request $request)
    {
        $this->getUser();

        $selectedSchools = $request->get('selectedSchools');

        DB::beginTransaction();

        $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
//            ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
            ->where(DB::raw('TIMEDIFF("2018-05-24 18:00:00", date_request)'), '<', 0)           //TODO MUST CHANGE THIS
            ->where('aitisi_type', 'Βελτιώση - Οριστική τοποθέτηση')
            ->first();

        if ($request == null) {
            $unique_id = uniqid($this->user->id);

            $new_request = OrganikiRequest::create([
                'unique_id' => $unique_id,
                'teacher_id' => $this->user->userable->id,
                'school_organiki' => $this->user->userable->myschool->organiki_name,
                'aitisi_type' => 'Βελτιώση - Οριστική τοποθέτηση',
                'date_request' => Carbon::now(),
                'situation' => 'saved'
            ]);

            $this->createOrganikesPrefrences($selectedSchools, $new_request);
            $message = 'Η Αίτηση σας αποθηκεύτηκε με επιτυχία.';
        } else {
            OrganikiPrefrences::where('request_id', $request->id)->delete();
            $this->createOrganikesPrefrences($selectedSchools, $request);
            $request->updated_at = Carbon::now();
            $request->save();
            $message = 'Η Αίτηση σας ενημερώθηκε με επιτυχία.';
        }

        DB::commit();
        return $message;
    }

    protected function getUser()
    {        
        $this->user =  Auth::user();
    }
}
