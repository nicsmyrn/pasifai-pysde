<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use App\Models\Year;
use App\Teacher;
use Pasifai\Pysde\models\OrganikiRequest;

class StatisticsController extends Controller
{
    public function getLeitourgika()
    {
        $lists = [
            ['E1', 'ΥπεράριθμοςΊδιαΟμάδα', 'ΥπεράριθμοςΌμορες'],
            ['E2'],
            ['E3']
        ];

        $teachers = [];

        $current_year_id = Year::where('current', true)->first()->id;

        $types = collect();

        foreach($lists as $list){

            $satisfied = 0;

            $not_satisfied = 0;

            $total = 0;

            $teacher_list = [];

            // $total = OrganikiRequest::with('prefrences')
            //     ->where('recall_situation', null)
            //     ->whereIn('aitisi_type', $list)
            //     ->count(); 

            $requests = OrganikiRequest::with('prefrences')
                ->whereIn('aitisi_type', $list)
                ->where('recall_situation', null)
                ->get();


            foreach($requests as $key=>$request){

                $teacher = Teacher::with(['placements.praxi'])
                            ->where('id', $request['teacher_id'])
                            ->first();


                $is_satisfied = false;

                $filtered_teacher_placements = $teacher->placements->filter(function($value, $key) use($current_year_id){
                    
                    return $value['praxi']['year_id'] == $current_year_id;

                })->all();

                foreach($filtered_teacher_placements as $key=>$pl){

                    if(in_array($pl->to_id, $request->prefrences->pluck('school_id')->toArray())){
                        $is_satisfied = true;
                    }else{
                        // return $teacher;
                    }
                }

                if(!in_array($teacher->id, $teachers)){
                    $total++;
                    $teachers[] = $teacher->id;

                    if($is_satisfied)
                        $satisfied++;
                    else{
                        $not_satisfied++;

                        $teacher_list[] = $teacher->myschool->full_name;

                        if(in_array('E2', $list)){
                            // return $request->teacher->myschool;
                        }
                    }
                }else{
                    if(in_array('E2', $list)){
                        // return $request->teacher->myschool;
                    }
                }
            }

            $types->push([
                'total' =>  $total,
                'list'  => $list,
                'satisfied' => $satisfied,
                'not_satisfied' => $not_satisfied,
                'teachers_not_satisfied' => $teacher_list
            ]);

        } // end foreach list


        return $types; 

    }

    protected function getUser()
    {
        // $this->user = Auth::user();
    }

}