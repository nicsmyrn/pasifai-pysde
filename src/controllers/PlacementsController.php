<?php

namespace Pasifai\Pysde\controllers;


use App\Http\Controllers\Controller;
use Auth;
use Pasifai\Pysde\repositories\PlacementRepositoryInterface;
use Pasifai\Pysde\controllers\traits\PlacementsTrait;
use Pasifai\Pysde\models\Placement;
use Illuminate\Http\Request;
use Pasifai\Pysde\models\Praxi;
use App\School;
use App\MySchoolTeacher;
use App\Models\Year;
use Carbon\Carbon;
use Log;

class PlacementsController extends Controller
{
    use PlacementsTrait;

    protected $user;
    protected $p;

    public function __construct(PlacementRepositoryInterface $p)
    {
        $this->middleware('isProsopikou');
        $this->p = $p;
    }

    public function placementsDetailsOfTeacher($teacher_id)
    {        
        return $this->getIndexPlacementsOfTeacher($teacher_id);
    }

    public function createPlacements()
    {   
        $current_year = Carbon::now()->year;
        
        $current = Year::where('current', 1)->first();
        $sec = explode("-", $current->name);

        $current_school_year = intval($sec[1]);

        if($current_year == $current_school_year){
            $year = $current_year;
        }else{
            $year = $current_year + 1;
        }

        return view('pysde::leitourgika.placements.create', ['year' => $year]);
    }

    public function createExcel()
    {
        return view('pysde::leitourgika.placements.excel');   
    }

    public function allTeachers()
    {        
        $message = 'Καθηγητές με τοποθετήρια';

        $teachers = MySchoolTeacher::whereHas('placements', function($query){
                $query->withTrashed();
        })->get();

        return view('pysde::leitourgika.placements.all_teachers', compact('teachers', 'message'));
    }

    public function byPraxi(Request $request)
    {
        $year_id =  Year::where('current', true)->first()->id;

        $praxi =  Praxi::where('year_id', $year_id)->where('decision_number', $request->get('αριθμός'))->first();

        $praxeis = Praxi::where('year_id', $year_id)->get();
        
        $message = '';
        $teachers = collect();

        if($praxi != null){
            $teachers = MySchoolTeacher::with(['placements'=> function($query) use($praxi){
                $query->withTrashed()->where('praxi_id', $praxi->id);
            }])
            ->get()
            ->filter(function($q){
                return count($q->placements) > 0;
            })
            ->values();
        }
        return view('pysde::leitourgika.placements.all_teachers_by_praxi', compact('teachers', 'message', 'praxeis', 'praxi'));
    }

    public function updatePraxi(Request $request, Praxi $praxi)
    {
        $date =  Carbon::createFromFormat('d-m-Y',$request->dde_protocol_date)->format('Y-m-d');
        
        $praxi->update([
            'dde_protocol' => $request->dde_protocol,
            'dde_protocol_date' => $date
        ]);

        flash()->info('Επιτυχής ενημέρωση', 'στην Πράξη:' . $praxi->decision_number . '/' . $praxi->decision_date);

        return redirect()->back();

    }

    public function allByPraxi($number)
    {
        $year_id =  Year::where('current', true)->first()->id;

        $praxi = Praxi::where('decision_number', $number)
            ->where('year_id',  $year_id)
            ->first();

        return $this->allPlacementsPDFbyPraxi($praxi, true);
    }

    public function updatePlacement($teacher_id, $placement_id)
    {
        $placement = Placement::where('afm', $teacher_id)->whereId($placement_id)->withTrashed()->firstOrFail();
//        $praxeis = Praxi::all();
        // $praxeis = Praxi::where('praxi_type', 11)->get();
        $praxeis = Praxi::all();
        $schools = School::all();
        $teacher = MySchoolTeacher::find($teacher_id);

        return view('pysde::leitourgika.placements.teacherPlacements.edit-teacher-placement', compact('placement','praxeis','schools', 'teacher'));    }

    public function storePlacement(Request $request, $teacher_afm, $placement_id)
    {        
        Placement::withTrashed()->find($placement_id)->update($this->p->initializeDataCreate($request,$teacher_afm));

        flash()->overlayS('Συγχαρητήρια', 'Η τοποθέτηση αποθηκεύτηκε με επιτυχία...');
 
        return redirect()->route('Leitourgika::Placements::placementsDetails', $teacher_afm);    }

    public function softDelete($placement_id)
    {
        Placement::find($placement_id)->delete();

        // ενημέρωση πίνακα

        flash()->error('Προσοχή', 'Η τοποθέτηση ανακλήθηκε');

        return redirect()->back();
    }

    public function restorePlacement($placement_id)
    {
        Placement::withTrashed()->find($placement_id)->restore();

        flash()->info('Συγχαρητήρια', 'Η τοποθέτηση ενεργοποιήθηκε');

        return redirect()->back();
    }

    public function postPermanentDelete(Request $request)
    {
        if($request->ajax()){
            Placement::withTrashed()->find($request->get('placement_id'))->forceDelete();

            flash()->success('','Η τοποθέτηση '.$request->get('placement_id').' διεγράφη με επιτυχία');

            return 'success';
        }
            flash()->error('Προσοχή!', 'αποτυχία διαγραφής...');
        abort(403);
    }

    public function enablePending($placement_id)
    {
        $placement = Placement::find($placement_id);

        $placement->pending = true;
        $placement->save();

        flash()->error('Προσοχή', 'Η τοποθέτηση είναι σε εκκρεμότητα');

        return redirect()->back();
    }

    public function cancelPending($placement_id)
    {
        $placement = Placement::find($placement_id);

        $placement->pending = false;
        $placement->save();

        flash()->success('Προσοχή', 'Η τοποθέτηση ενεργοποιήθηκε');

        return redirect()->back();
    }

    public function viewTeacherPlacementsPDF($teacher_id, $year_name)
    {
        $year_list = array();
        $yearModel = Year::where('name', $year_name)->first();

        if($yearModel != null){
            if($yearModel->current) $year_list[] = $yearModel->id;
            else    
                $year_list[] = $yearModel->id;
        }else{            
            $year_list = Year::where('current', false)->lists('id');
        }   
        return $this->allPlacementsPDF($teacher_id, $year_list, true);    
    }

    public function viewTeacherPlacementsPDFbeta($teacher_id, $year_name)
    {
        $year_list = array();
        $yearModel = Year::where('name', $year_name)->first();


        if($yearModel != null){
            if($yearModel->current) $year_list[] = $yearModel->id;
        }else{
            $year_list = Year::where('current', false)->lists('id');
        }

        return $this->allPlacementsPDF($teacher_id, $year_list, true);    
    }

    protected function getUser()
    {
        $this->user = Auth::user();
    }

}
