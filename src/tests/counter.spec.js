import Vue from 'vue'
import { shallowMount } from '@vue/test-utils';


describe('Counter', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(Counter);
    });

    it('defaults to a count of 0', () => {        
        expect(wrapper.vm.count).toBe(0);
    });

    it('increments the count when the increment button is clicked', () => {        
        expect(wrapper.vm.count).toBe(0);

        wrapper.find('.increment').trigger('click');
    
        expect(wrapper.vm.count).toBe(1);
    });

    it('decrements the count when the decrement button is clicked', () => {        
        expect(wrapper.vm.count).toBe(0);

        wrapper.setData({
            count : 5
        });

        wrapper.find('.decrement').trigger('click');

        expect(wrapper.vm.count).toBe(4);
    });

    it('never goes below 0', async () => {
        expect(wrapper.vm.count).toBe(0);

        expect(wrapper.find('.decrement').isVisible()).toBe(false);
        
        wrapper.find('.increment').trigger('click');

        await Vue.nextTick();
        // expect(true).toBe(false)
        
        expect(wrapper.find('.decrement').isVisible()).toBe(true);
    });

    it('presents the current count', async () => {   
        expect(wrapper.find('.count').html()).toContain(0);

        wrapper.find('.increment').trigger('click');

        await Vue.nextTick();
        // expect(true).toBe(false)

        expect(wrapper.find('.count').html()).toContain(1);
    })

});