<?php

namespace Pasifai\Pysde\requests;

use Illuminate\Foundation\Http\FormRequest;

class TeacherIpiresiaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        return [
            'ex_years' => 'required|integer',
            'ex_months' => 'required|integer',
            'ex_days' => 'required|integer',
            'ex_days' => 'required|integer',
            'family_situation' => 'required|integer',
            'childs' => 'required|integer',
            'dimos_entopiotitas' => 'required|integer',
            'dimos_sinipiretisis' => 'required|integer',
            'moria' => 'required|numeric',
            'moria_apospasis' => 'required|numeric',
        ];
    }

    // public function messages()
    // {
    //     return [
    //         'type.required' => 'Επέλεξε Εισερχόμενο ή Εξερχόμενο',
    //         'type.in' => 'Δεν έχεις επιλέξει Εισερχόμενο ή Εξερχόμενο',
    //         'p_date.required' => 'Η ημερομηνία είναι υποχρεωτική',
    //         'p_date.date_format' => 'Η ημερομηνία δεν είναι σωστή',
    //         'from_to.required' => 'Ο Αποστολέας / Παραλήπτης  είναι υποχρεωτικός',
    //         'subject.required' => 'Το Θέμα είναι υποχρεωτικό',
    //         'f_id.required' => 'Ο Φάκελος ταξινόμησης είναι υποχρεωτικός',
    //         'f_id.exists' => 'Δεν υπάρχει ο συγκεκριμένος Φάκελος Ταξινόμησης'
    //     ];
    // }
}
