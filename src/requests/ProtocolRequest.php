<?php

namespace Pasifai\Pysde\requests;

use Illuminate\Foundation\Http\FormRequest;

class ProtocolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'type' => 'required|in:0,1,9',
            'p_date' => 'required|date_format:d/m/Y',
            'from_to' => 'required',
            'subject' => 'required',
            'f_id' => 'required|exists:f,id'
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'Επέλεξε Εισερχόμενο ή Εξερχόμενο',
            'type.in' => 'Δεν έχεις επιλέξει Εισερχόμενο ή Εξερχόμενο',
            'p_date.required' => 'Η ημερομηνία είναι υποχρεωτική',
            'p_date.date_format' => 'Η ημερομηνία δεν είναι σωστή',
            'from_to.required' => 'Ο Αποστολέας / Παραλήπτης  είναι υποχρεωτικός',
            'subject.required' => 'Το Θέμα είναι υποχρεωτικό',
            'f_id.required' => 'Ο Φάκελος ταξινόμησης είναι υποχρεωτικός',
            'f_id.exists' => 'Δεν υπάρχει ο συγκεκριμένος Φάκελος Ταξινόμησης'
        ];
    }
}
