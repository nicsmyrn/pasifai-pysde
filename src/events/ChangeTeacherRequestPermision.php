<?php

namespace  Pasifai\Pysde\events;

use Pasifai\Pysde\events\TeacherRequestGrandDenyPermission;
use App\Models\Role;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class ChangeTeacherRequestPermision
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeacherRequestGrandDenyPermission  $event
     * @return void
     */
    public function handle(TeacherRequestGrandDenyPermission $event)
    {
        Log::warning("INSIDE 555");
        
        $role_teacher = Role::where('slug', 'teacher')->first();
        if ($role_teacher->hasPermission($event->permission)){ //create_Απόσπαση_request
            $role_teacher->takePermission($event->permission);
        }else{
            $role_teacher->givePermission($event->permission);
        }
    }
}
