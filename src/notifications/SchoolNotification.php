<?php

namespace Pasifai\Pysde\notifications;

use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SchoolNotification extends Notification
{
    use Queueable;

    private $user;
    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line($this->data['description'])
            ->action('Προβολή Τμημάτων', $this->data['url'])
            ->greeting($this->data['title'])
            ->subject($this->user->full_name);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'from' => [
                'user_id'       => $this->user->id,
                'last_name'     => $this->user->last_name,
                'first_name'    => $this->user->first_name,
                'middle_name'   => $this->user->userable->middle_name
            ],
            'title' => $this->data['title'],
            'description'   => $this->data['description'],
            'type'          => $this->data['type'],
            'url'           => $this->data['url']
        ];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage([
            'data' => [
                'from' => [
                    'user_id'       => $this->user->id,
                    'last_name'     => $this->user->last_name,
                    'first_name'    => $this->user->first_name,
                    'middle_name'   => $this->user->userable->middle_name
                ],
                'title' => $this->data['title'],
                'description'   => $this->data['description'],
                'type'          => $this->data['type'],
                'url'           => $this->data['url']
            ],
            'created_at' => Carbon::now()->format('Y-m-d h:i:s'),
            'type' => 'App\Notifications\SchoolNotification'
        ]);
    }
}
