<?php


Route::prefix('api')->group(function () {

    Route::group(['prefix' => 'requests', 'as' => 'Request::'], function () {
        Route::get('allRequests', 'Pasifai\Pysde\controllers\API\RequestApiController@getAllRequests')->name('getAllRequests');
        Route::get('recall/{id}', 'Pasifai\Pysde\controllers\API\RequestApiController@recallRequest')->name('recall');       
        Route::get('destroy/{id}', 'Pasifai\Pysde\controllers\API\RequestApiController@destroy')->name('delete');
    });

    Route::group(['prefix' => 'admin', 'as' => 'Admin::'], function () {
        Route::group(['prefix' => 'requests', 'as' => 'Requests::'], function () {
            Route::get('allRequests', 'Pasifai\Pysde\controllers\API\AdminRequestApiController@getAllRequests')->name('adminGetAllRequests');  
            Route::get('recall/{unique_id}/{action}', 'Pasifai\Pysde\controllers\API\AdminRequestApiController@Recall')->name('Recall');
        });

        Route::group(['prefix' => 'organika', 'as' => 'Organika::'], function(){
            Route::group(['prefix' => 'auto', 'as' => 'Auto::'], function () {
                Route::get('getRequests', 'Pasifai\Pysde\controllers\API\Auto@getRequests');                
                Route::get('getKena', 'Pasifai\Pysde\controllers\API\Auto@getKena');                
                Route::get('getPleonasmata', 'Pasifai\Pysde\controllers\API\Auto@getPleonasmata');   
                Route::post('saveNewOrganikes', 'Pasifai\Pysde\controllers\API\Auto@saveOrganikes');             
            });
            Route::get('getDivisions', 'Pasifai\Pysde\controllers\API\AdminOrganikaApiController@getDivisions');
            Route::post('updateLessonHours', 'Pasifai\Pysde\controllers\API\AdminOrganikaApiController@updateLessonHours');
            Route::post('deletePermanentLessonDiv', 'Pasifai\Pysde\controllers\API\AdminOrganikaApiController@deletePermanentLessonDiv');
        });

        Route::group(['prefix' => 'leitourgika', 'as' => 'Leitourgika::'], function () {
            Route::get('fetchSchools', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@fetchAllSchools')->name('leitourgikaFetchSchools');  
            Route::get('getKenaOfSchool/{forSchPysde}', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@getKenaOfSchool')
            ->middleware('bindings')
            ->name('getKenaOfSchool');  

            Route::get('loadOrganika', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@loadOrganikaKena')->name('loadOrganikaKena');  
            Route::get('loadTeachers', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@loadTeachers')->name('loadTeachers');  

            Route::post('getTeachersOfSchool/{forSchPysde}', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@getTeachersOfSchool')
            ->middleware('bindings')            
            ->name('getTeachersOfSchool');      
            
            Route::post('saveEidikotita/{forSchPysde}', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@saveEidikotita')
            ->middleware('bindings')            
            ->name('saveEidikotita');   

            Route::post('deleteEidikotita/{forSchPysde}', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@deleteEidikotita')
            ->middleware('bindings')            
            ->name('deleteEidikotita');   

            Route::post('saveTeachersOfEidikotita/{forSchPysde}', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@saveTeachers')
            ->middleware('bindings')            
            ->name('saveTeachers');   

            Route::get('lockUnlockAllKena', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@lockUnlockAllKena')
            ->name('lockUnlockAllKena');  
            
            Route::post('fetchTeachersByEidikotita', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@fetchTeachersByEidikotita')->name('fetchTeachersByEidikotita');  
            Route::get('fetchEidikotites', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@fetchEidikotites')->name('fetchEidikotites');

            Route::get('getExcelForCouncil', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@getExcelForCouncil')->name('getExcelForCouncil'); 
            Route::get('getExcelForSchools', 'Pasifai\Pysde\controllers\API\AdminLeitourgikaApiController@getExcelForSchools')->name('getExcelForSchools'); 

         });

        Route::group(['prefix' => 'placements', 'as' => 'Placements::'], function () {
            Route::get('fetchData', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@fetchData')->name('fetchData');              
            Route::get('fetchDataForNewPlacement', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@fetchDataForNewPlacement')->name('fetchDataForNewPlacement');              
            Route::post('getTeachers', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@getTeachers')->name('getTeachers');              
            Route::post('savePlacements', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@savePlacements')->name('savePlacements');              
            Route::post('deleteOldPlacement', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@deleteOldPlacement')->name('deleteOldPlacement');              
            Route::post('deletePermanentlyPlacement', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@permanentDeleteOldPlacement')->name('permanentDeleteOldPlacement');              
            Route::post('recallOldPlacement', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@recallOldPlacement')->name('recallOldPlacement');              
            Route::get('createExcel/{praxi}', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@getExcelForPraxi')->name('getExcelForPraxi'); 
            Route::get('createExcelOrganika/{praxi}', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@getExcelForPraxiOrganika')->name('getExcelForPraxiOrganika'); 
            
            Route::get('permanentDelete/{id}', 'Pasifai\Pysde\controllers\API\AdminPlacementsApiController@permanentDelete')->name('permanentDelete');                          
        });
    });

    Route::group(['prefix' => 'school', 'as' => 'School::'], function () {
        Route::group(['prefix' => 'leitourgika', 'as' => 'Leitourgika::'], function () {
            Route::get('getTeachers', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@getTeachers')->name('getTeachers');           
            Route::post('saveTeachers', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@saveTeachers')->name('saveTeachers');           
            Route::get('getEidikotites', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@getEidikotites')->name('getEidikotites');           
            Route::post('saveEidikotites', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@saveEidikotites')->name('saveEidikotites');
            Route::post('saveEidikotita', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@saveEidikotitaForSchool')->name('saveEidikotitaForSchool');
            Route::post('attachNewEidikotita', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@attachNewEidikotita')->name('attachNewEidikotita');
            Route::post('detachEidikotita', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@detachEidikotita')->name('detachEidikotita');
            Route::get('lockTeachers', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@lockTeachers')->name('lockTeachers');     
            
            Route::post('uploadTeacherFile', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@uploadTeacherFile')->name('uploadTeacherFile');      
            Route::post('uploadGlobalFile', 'Pasifai\Pysde\controllers\API\SchoolLeitourgikaApiController@uploadGlobalFile')->name('uploadGlobalFile');      
            
        });

        Route::group(['prefix' => 'organika', 'as' => 'Organika::'], function(){
            Route::get('getOrganikaTeachers', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@getOrganikes')->name('getOrganikes');
            Route::post('updateTeacherOrganika', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@updateTeacherOrganika')->name('updateTeacherOrganika');
            
            Route::group(['prefix' => 'lyceum', 'as' => 'Lyceum::'], function(){
                Route::get('fetchDivisions', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@getDivisionsOfSchoolLyceum')->name('getDivisionsOfSchoolLyceum');            
                Route::post('saveDivisions', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@saveDivisions')->name('saveDivisions');            
                Route::post('sentDivisions', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@sentDivisions')->name('sentDivisions');            
            });
            Route::group(['prefix' => 'gymnasium', 'as' => 'Gymnasium::'], function(){
                Route::get('fetchDivisions', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@getDivisionsOfSchoolGymnasium')->name('getDivisionsOfSchoolGymnasium');            
                Route::post('saveDivisions', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@saveGymnasiumDivisions')->name('saveGymnasiumDivisions');            
                Route::post('sentDivisions', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@sentGymnasiumDivisions')->name('sentGymnasiumDivisions');   
            });

            Route::group(['prefix' => 'other', 'as' => 'Other::'], function(){
                Route::get('fetchDivisions', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@getDivisionsOfSchoolOther')->name('getDivisionsOfSchoolOther');            
                Route::post('saveDivisions', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@saveOtherDivisions')->name('saveOtherDivisions');            
                Route::post('sentDivisions', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@sentOtherDivisions')->name('sentOtherDivisions');   
            });

            Route::group(['prefix' => 'admin', 'as' => 'Admin::'], function(){
                Route::get('fetchSchools', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@getAllSchools')->name('getAllSchools'); 
                Route::post('saveGeneralDivs', 'Pasifai\Pysde\controllers\API\SchoolOrganikaApiController@saveGeneralDivs')->name('saveGeneralDivs');                 
            });

        });
    });
});