<?php

use App\School;
use App\User;

Route::group(['middleware' => ['web']], function () {

    Route::group(['prefix' => 'statistics', 'as' => 'Statistics::'], function () {
        Route::get('leitourgika', 'Pasifai\Pysde\controllers\StatisticsController@getLeitourgika')->name('getLeitourgika');
    });
    
    Route::group(['prefix' => 'Αποσπάσεις', 'as' => 'Apospaseis::'], function () {
        Route::get('Υπολογισμός-Μορίων', 'Pasifai\Pysde\controllers\MoriaController@calculateMoria')->name('calculateMoria');
        Route::get('Αυτόματος-Υπολογισμός-Μορίων', 'Pasifai\Pysde\controllers\MoriaController@prepareCalculateApospasis')->name('prepareCalculate');
        Route::post('Αυτόματος-Υπολογισμός-Μορίων', 'Pasifai\Pysde\controllers\MoriaController@calculateApospasis')->name('autoCalculate');
    });

    Route::group(['prefix' => 'ΑΙΤΗΣΕΙΣ', 'as' => 'Request::'], function () {

        Route::get('Δημιουργία', 'Pasifai\Pysde\controllers\RequestController@createPreparation')->name('Prepare');
        Route::get('Οργανικές/{type}/{unique_id?}', 'Pasifai\Pysde\controllers\RequestController@organikiCreate')->name('OrganikiType');
        Route::get('Δημιουργία/{aitisi}/{unique_id?}', 'Pasifai\Pysde\controllers\RequestController@create')->name('create');
        // Route::get('{id}/Επεξεργασία', 'Pasifai\Pysde\controllers\RequestController@edit')->name('edit');
        // Route::patch('{id}/Επεξεργασία', 'Pasifai\Pysde\controllers\RequestController@update')->name('patch');
        // Route::get('ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ', 'Pasifai\Pysde\controllers\RequestController@index')->name('archives');
        Route::get('ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ', 'Pasifai\Pysde\controllers\RequestController@index')->name('archives');
        Route::get('ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ-ΥΠΕΡΑΡΙΘΜΙΕΣ', 'Pasifai\Pysde\controllers\RequestController@indexYperarithmia')->name('archivesYperarithmia');
        // Route::get('{id}/Διαγραφή', 'Pasifai\Pysde\controllers\RequestController@destroy')->name('delete');

        Route::get('download/{file}', 'Pasifai\Pysde\controllers\RequestController@downloadPDF')->name('downloadPDF');
    });


    Route::group(['prefix' => 'ΔΙΑΧΕΙΡΙΣΗ', 'as' => 'PysdeAdmin::'], function () {
        Route::get('Αιτήσεων', 'Pasifai\Pysde\controllers\TeacherController@indexRequestsOfTeachers')->name('indexRequests');
        Route::get('Αιτήσεων/{id}/download', 'Pasifai\Pysde\controllers\TeacherController@downloadPDF')->name('downloadPDF');

        Route::get('Επεξεργασία-Καθηγητή/{userTeacher}/Υπηρεσιακά', 'Pasifai\Pysde\controllers\TeacherController@getEditTeacherIpiresiaka')->name('getEditTeacherIpiresiaka');
        Route::post('Επεξεργασία-Καθηγητή/{userTeacher}/Υπηρεσιακά', 'Pasifai\Pysde\controllers\TeacherController@postEditTeacherIpiresiaka')->name('postEditTeacherIpiresiaka');
        Route::get('Επεξεργασία-Καθηγητή/{userTeacher}/Λόγοι-Υγείας', 'Pasifai\Pysde\controllers\TeacherController@getEditTeacherHealth')->name('getEditTeacherHealth');
        Route::post('Επεξεργασία-Καθηγητή/{userTeacher}/Λόγοι-Υγείας', 'Pasifai\Pysde\controllers\TeacherController@postEditTeacherHealth')->name('postEditTeacherHealth');
        Route::get('Επεξεργασία-Καθηγητή/{userTeacher}/Λόγοι-Υγείας-Απόσπασης', 'Pasifai\Pysde\controllers\TeacherController@getEditTeacherHealthApospasis')->name('getEditTeacherHealthApospasis');
        Route::post('Επεξεργασία-Καθηγητή/{userTeacher}/Λόγοι-Υγείας-Απόσπασης', 'Pasifai\Pysde\controllers\TeacherController@postEditTeacherHealthApospasis')->name('postEditTeacherHealthApospasis');
    });

    // Route::group(['prefix'=> 'ΤΜΗΜΑ-Γ-ΠΡΟΣΩΠΙΚΟΥ', 'as' => 'Proswpikou::'], function(){

    // });

    Route::group(['prefix'=> 'ΤΜΗΜΑ-Γ-ΠΡΟΣΩΠΙΚΟΥ', 'as' => 'Oikonomikou::'], function(){
        Route::group(['prefix' => 'ΠΡΑΞΕΙΣ-ΑΝΑΛΗΨΗΣ', 'as' => 'PraxeisAnalipsis::'], function(){
            Route::get('ΟΛΟΙ-ΟΙ-ΚΑΘΗΓΗΤΕΣ', 'Pasifai\Pysde\controllers\PlacementsForOikonomikou@allPraxeisAnalipsis')->name('allPraxeisAnalipsis');
            Route::get('downloadPraxi/{filename}', 'Pasifai\Pysde\controllers\PlacementsForOikonomikou@downloadPraxi')->name('downloadPraxi');
            Route::get('deletePraxi/{identifier}/{afm}', 'Pasifai\Pysde\controllers\PlacementsForOikonomikou@deletePraxi')->name('deletePraxi');
        });

        Route::group(['prefix' => 'ΤΟΠΟΘΕΤΗΣΕΙΣ', 'as' => 'Placements::'], function(){
            Route::get('ΟΛΟΙ-ΟΙ-ΚΑΘΗΓΗΤΕΣ', 'Pasifai\Pysde\controllers\PlacementsForOikonomikou@allTeachers')->name('allTeachers');
            Route::get('ΠΡΑΞΗ', 'Pasifai\Pysde\controllers\PlacementsForOikonomikou@byPraxi')->name('placementsByPraxi');
            Route::get('ΠΡΑΞΗ/{number}/ΟΛΕΣ-ΟΙ-ΤΟΠΟΘΕΤΗΣΕΙΣ', 'Pasifai\Pysde\controllers\PlacementsForOikonomikou@allByPraxi')->name('allByPraxi');

            Route::get('ΚΑΘΗΓΗΤΗΣ/{afm}', 'Pasifai\Pysde\controllers\PlacementsForOikonomikou@placementsDetailsOfTeacher')->name('placementsDetails');

            Route::get('/teacher/{afm}/ΤΟΠΟΘΕΤΗΡΙΑ-ΚΑΘΗΓΗΤΗ/{year_name}', 'Pasifai\Pysde\controllers\PlacementsForOikonomikou@viewTeacherPlacementsPDF')->name('teacherPDF');
        });

    });
    
    Route::group(['prefix' => 'ΛΕΙΤΟΥΡΓΙΚΑ-ΚΕΝΑ', 'as' => 'Leitourgika::'], function () {
        Route::group(['prefix' => 'ΔΙΑΧΕΙΡΙΣΗ', 'as' => 'pysdeSecretary::'], function () {
            Route::get('ΣΧΟΛΙΚΕΣ-ΜΟΝΑΔΕΣ/{forSchPysde?}', 'Pasifai\Pysde\controllers\LeitourgikaController@getManageSchools')->name('manageSchools');
            Route::get('ΕΚΠΑΙΔΕΥΤΙΚΟΙ-ΑΝΑ-ΣΧΟΛΕΙΟ', 'Pasifai\Pysde\controllers\LeitourgikaController@getManageTeachers')->name('manageTeachers');
            Route::get('ΠΙΝΑΚΕΣ-ΚΕΝΩΝ-ΠΛΕΟΝΑΣΜΑΤΩΝ', 'Pasifai\Pysde\controllers\LeitourgikaController@getIndexTables')->name('getIndexTables');
            Route::get('downloadExcel', 'Pasifai\Pysde\controllers\LeitourgikaController@getExcel')->name('getExcel');
            Route::get('getDiathesi', 'Pasifai\Pysde\controllers\LeitourgikaController@getDiathesi')->name('getDiathesi');

            Route::get('Ενημέρωση-Λειτουργικών-Κενών-Με-Εκπαιδευτικους-Που-Απουσιάζουν', 'Pasifai\Pysde\controllers\LeitourgikaController@getUpdateLeitourgika')->name('getUpdateLeitourgika');
            Route::post('Ενημέρωση-Λειτουργικών-Κενών-Με-Εκπαιδευτικους-Που-Απουσιάζουν', 'Pasifai\Pysde\controllers\LeitourgikaController@postUpdateLeitourgika')->name('postUpdateLeitourgika');
          
            Route::get('Ενημέρωση-Λειτουργικών-Κενών-Διεθυντές', 'Pasifai\Pysde\controllers\LeitourgikaController@getUpdateDieuthides')->name('getUpdateDieuthides');
            Route::post('Ενημέρωση-Λειτουργικών-Κενών-Διεθυντές', 'Pasifai\Pysde\controllers\LeitourgikaController@postUpdateDieuthides')->name('postUpdateDieuthides');
          
            Route::get('Μείωση-Ωραρίου', 'Pasifai\Pysde\controllers\LeitourgikaController@getMeiwsi')->name('getMeiwsi');
            Route::post('Μείωση-Ωραρίου', 'Pasifai\Pysde\controllers\LeitourgikaController@postMeiwsi')->name('postMeiwsi');
        });

        Route::group(['prefix' => 'ΤΟΠΟΘΕΤΗΣΕΙΣ', 'as' => 'Placements::'], function(){
            Route::get('Δημιουργία', 'Pasifai\Pysde\controllers\PlacementsController@createPlacements')->name('index');
            Route::get('Excel', 'Pasifai\Pysde\controllers\PlacementsController@createExcel')->name('getExcelForPraxi');

            Route::get('ΚΑΘΗΓΗΤΗΣ/{afm}', 'Pasifai\Pysde\controllers\PlacementsController@placementsDetailsOfTeacher')->name('placementsDetails');
            Route::get('ΑΝΑΚΛΗΣΗ-ΤΟΠΟΘΕΤΗΣΗΣ/{placement_id}', 'Pasifai\Pysde\controllers\PlacementsController@softDelete')->name('softDelete');
            Route::get('ΕΠΑΝΑΦΟΡΑ-ΤΟΠΟΘΕΤΗΣΗΣ/{placement_id}', 'Pasifai\Pysde\controllers\PlacementsController@restorePlacement')->name('restorePlacement');
            Route::get('ΕΝΕΡΓΟΠΟΙΗΣΗ-ΕΚΚΡΕΜΟΤΗΤΑΣ/{placement_id}', 'Pasifai\Pysde\controllers\PlacementsController@enablePending')->name('enablePending');
            Route::get('ΑΚΥΡΩΣΗ-ΕΚΚΡΕΜΟΤΗΤΑΣ/{placement_id}', 'Pasifai\Pysde\controllers\PlacementsController@cancelPending')->name('cancelPending');
            Route::post('ΟΡΙΣΤΙΚΗ-ΔΙΑΓΡΑΦΗ', 'Pasifai\Pysde\controllers\PlacementsController@postPermanentDelete')->name('forceDeletePlacement');

            Route::get('ΟΛΟΙ-ΟΙ-ΚΑΘΗΓΗΤΕΣ', 'Pasifai\Pysde\controllers\PlacementsController@allTeachers')->name('allTeachers');

            Route::get('ΠΡΑΞΗ', 'Pasifai\Pysde\controllers\PlacementsController@byPraxi')->name('placementsByPraxi');
            Route::patch('update-praxi/{praxi}', 'Pasifai\Pysde\controllers\PlacementsController@updatePraxi')->name('updatePraxi');
            

            Route::get('ΠΡΑΞΗ/{number}/ΟΛΕΣ-ΟΙ-ΤΟΠΟΘΕΤΗΣΕΙΣ', 'Pasifai\Pysde\controllers\PlacementsController@allByPraxi')->name('allByPraxi');

            Route::get('/teacher/{afm}/τοποθέτηση/{placement_id}', 'Pasifai\Pysde\controllers\PlacementsController@updatePlacement')->name('update');
            Route::patch('/teacher/{afm}/τοποθέτηση/{placement_id}', 'Pasifai\Pysde\controllers\PlacementsController@storePlacement')->name('store');

            Route::get('/teacher/{afm}/ΤΟΠΟΘΕΤΗΡΙΑ-ΚΑΘΗΓΗΤΗ/{year_name}', 'Pasifai\Pysde\controllers\PlacementsController@viewTeacherPlacementsPDF')->name('teacherPDF');
            Route::get('/teacher/{afm}/ΤΟΠΟΘΕΤΗΡΙΑ-ΚΑΘΗΓΗΤΗ-beta/{year_name}', 'Pasifai\Pysde\controllers\PlacementsController@viewTeacherPlacementsPDFbeta')->name('teacherPDFbeta');
        });

//hi
        Route::group(['prefix' => 'ΣΧΟΛΙΚΗ-ΜΟΝΑΔΑ', 'as' => 'school::'], function () {
        
        });
    });

    Route::group(['prefix' => 'ΟΡΓΑΝΙΚΑ-ΚΕΝΑ', 'as' => 'Pysde::'], function () {

            Route::group(['prefix' => 'ΣΧΟΛΕΙΟ', 'as' => 'School::'], function () {
                if(config('requests.offline_organika')){
                    //
                }else{
                    Route::get('Δήλωση-Τμημάτων-Γυμνασίου', 'Pasifai\Pysde\controllers\SchoolVacuumController@getDivisions')->name('divisions');
                    Route::get('Δήλωση-Τμημάτων-Λυκείου', 'Pasifai\Pysde\controllers\LyceumVacuumController@getDivisionsForLyceum')->name('LyceumDivisions');
                    Route::get('Δήλωση-Κενών-Πλεονασμάτων', 'Pasifai\Pysde\controllers\OtherSchoolsVacuumController@getDivisionsForOtherSchool')->name('OtherDivisions');
        
                    Route::get('Οργανικές-Θέσεις', 'Pasifai\Pysde\controllers\SchoolKenaController@getOrganikes')->name('organikes');
                    Route::get('Κενα-Πλεονάσματα', 'Pasifai\Pysde\controllers\SchoolKenaController@getVacuumsForSchool')->name('getKena');
                }    
                if(config('requests.offline_leitourgika')){
                    //
                }else{
                    Route::group(['prefix' => 'ΛΕΙΤΟΥΡΓΙΚΑ', 'as' => 'Leitourgika::'], function () {
                        if(!config('requests.offline_leitourgika_teachers')){
                            Route::get('ΕΚΠΑΙΔΕΥΤΙΚΟΙ', 'Pasifai\Pysde\controllers\SchoolLeitourgikaController@indexOfTeachers')->name('indexOfTeachers');                              
                        }
                        if(!config('requests.offline_leitourgika_eidikotites')){
                            Route::get('ΕΙΔΙΚΟΤΗΤΕΣ', 'Pasifai\Pysde\controllers\SchoolLeitourgikaController@indexOfEidikotites')->name('indexOfEidikotites');            
                        }          
                    });
                }
            });        

        Route::group(['prefix' => 'ΓΡΑΜΜΑΤΕΙΑ-ΠΥΣΔΕ', 'as' => 'Secretary::'], function () {

            Route::group(['prefix' => 'ΚΕΝΑ', 'as' => 'assignment::'], function () {
                Route::get('ΑΝΑ-ΣΧΟΛΕΙΟ/{forSchPysde?}', 'Pasifai\Pysde\controllers\VacuumController@getHoursForGymnasiums')->name('vacuumPerSchool');
                Route::get('Εισαγωγή-Δεδομένων-Γενική-Παιδείας', 'Pasifai\Pysde\controllers\VacuumController@createGeneralDivsData')->name('createGeneralDivsData');
                Route::get('Excel', 'Pasifai\Pysde\controllers\VacuumController@getExcel')->name('getExcel');
                Route::get('ExcelOrganika', 'Pasifai\Pysde\controllers\VacuumController@getExcelOrganika')->name('getExcelOrganika');
                Route::get('Αναθέσεις-Μαθημάτων', 'Pasifai\Pysde\controllers\VacuumController@anatheseis')->name('anatheseis');
            });

            Route::group(['prefix' => 'ΑΝΑΘΕΣΕΙΣ', 'as' => 'EE'], function () {
                Route::get('ΓΥΜΝΑΣΙΟΥ', 'Pasifai\Pysde\controllers\AssignmentsController@getAssignmentsForGymnasiums')->name('assignGym');
            });

            Route::group(['prefix' => 'Protocol', 'as' => 'Protocol::'], function () {
                Route::get('create', 'Pasifai\Pysde\controllers\ProtocolController@create')->name('create');
                Route::post('create', 'Pasifai\Pysde\controllers\ProtocolController@store')->name('create');

                Route::get('manual', 'Pasifai\Pysde\controllers\ProtocolController@manualCreate')->name('manual');
                Route::post('manual', 'Pasifai\Pysde\controllers\ProtocolController@manualStore')->name('postManual');

                Route::get('edit/{id}', 'Pasifai\Pysde\controllers\ProtocolController@edit')->name('edit');
                Route::patch('edit/{id}', 'Pasifai\Pysde\controllers\ProtocolController@update')->name('patch');

                Route::get('edit-archives/{id}', 'Pasifai\Pysde\controllers\ProtocolController@editArchives')->name('editArchives');
                Route::patch('edit-archives/{id}', 'Pasifai\Pysde\controllers\ProtocolController@updateArchives')->name('editArchives');


                Route::get('convertPDF', 'Pasifai\Pysde\controllers\ProtocolController@indexPDF')->name('convert');
                Route::get('protocolToPDF', 'Pasifai\Pysde\controllers\ProtocolController@viewToPDF')->name('toPDF');
                Route::get('protocolArchivesToPDF', 'Pasifai\Pysde\controllers\ProtocolController@viewArchivesToPDF')->name('archivesToPDF');
                Route::get('archives', 'Pasifai\Pysde\controllers\ProtocolController@indexArchive')->name('archives');
                Route::get('/', 'Pasifai\Pysde\controllers\ProtocolController@index')->name('index');
            });


            Route::group(['prefix' => 'teachers', 'as' => 'Teachers::'], function () {

                Route::get('Ονομαστικά-Υπεράριθμοι', 'Pasifai\Pysde\controllers\TeacherController@manageOnomastikaYperarithmous')->name('makeOnomastikaYperarithmous');
                Route::get('exportYperarithmousPerSchool', 'Pasifai\Pysde\controllers\TeacherController@extractYperarithmousToExcel')->name('exportYperarithmous');
                
                // Route::get('check', 'Pasifai\Pysde\controllers\TeacherController@getTeachersNotConfirmed')->name('allTeachers');
                // Route::get('allForChecking', 'Pasifai\Pysde\controllers\TeacherController@getAllTeachersForConfirmation')->name('allTeachersForChecking');

                // Route::get('requests', 'Pasifai\Pysde\controllers\TeacherController@allRequests')->name('allRequests');
                // Route::get('archives', 'Pasifai\Pysde\controllers\TeacherController@archivesTeachersRequests')->name('archives');
                // Route::get('checkProfile/teacher/{id}', 'Pasifai\Pysde\controllers\TeacherController@checkProfile')->name('check');
                // Route::patch('confirmProfile/{id}', 'Pasifai\Pysde\controllers\TeacherController@confirmTeacherProfile')->name('confirm');
                // Route::get('reCheckProfile/{id}', 'Pasifai\Pysde\controllers\TeacherController@wrongTeacherProfile')->name('recheck');
                // Route::patch('checkProfile/{id}', 'Pasifai\Pysde\controllers\TeacherController@updateProfileFields')->name('updateTeacherProfile');
                // Route::get('{id}/action/giveProtocol', 'Pasifai\Pysde\controllers\TeacherController@giveProtocol')->name('giveProtocol');
                Route::get('Διαχείριση-Αιτήσεων', 'Pasifai\Pysde\controllers\TeacherController@getManage')->name('manageRequests');
                // Route::get('ΞΕΚΛΕΙΔΩΜΑ-ΠΡΟΦΙΛ/{id}', 'Pasifai\Pysde\controllers\TeacherController@unlock')->name('unlockProfile');
                // Route::get('ΚΑΜΙΑ-ΑΛΛΑΓΗ-ΣΤΟ-ΠΡΟΦΙΛ/{id}', 'Pasifai\Pysde\controllers\TeacherController@lock')->name('lockProfile');

                // Route::get('{id}/ΑΚΥΡΩΣΗ-ΑΙΤΗΜΑΤΟΣ', 'Pasifai\Pysde\controllers\TeacherController@cancelRequest')->name('cancelRequest');
                // Route::get('{id}/ΕΠΙΒΕΒΑΙΩΣΗ-ΑΙΤΗΜΑΤΟΣ', 'Pasifai\Pysde\controllers\TeacherController@confirmRequest')->name('confirmRequest');

               Route::get('Μεταμόρφωση-Αιτήσεων', 'Pasifai\Pysde\controllers\ExcelController@getPrepareExcel')->name('prepareExcel');
               Route::post('downloadExcel', 'Pasifai\Pysde\controllers\ExcelController@excelTeachersRequests')->name('downloadExcelTeachersRequest');
//                Route::get('displayExcel', 'Pasifai\Pysde\controllers\ExcelController@displayExcelTeachersRequests')->name('displayExcelTeachersRequest');
            });

        });
    });

    Route::get('aj/protocols', 'Pasifai\Pysde\controllers\ProtocolController@ajaxIndex')->name('ajaxIndex');


    Route::post('aj/yperarithmia/save', 'Pasifai\Pysde\controllers\RequestController@ajaxCreateYperarithmia')->name('storeYperarithmia');

    Route::post('aj/at/chPrm', 'Pasifai\Pysde\controllers\TeacherController@changeTeacherRequestAccess')->name('changePermission');

    Route::post('aj/requests/ajaxFetchSchools', 'Pasifai\Pysde\controllers\RequestController@ajaxFetchSchools')->name('ajaxFetchSchools');
    Route::post('aj/requests/ajaxFetchSchoolsCovid19', 'Pasifai\Pysde\controllers\RequestController@ajaxFetchSchoolsCovid19')->name('ajaxFetchSchoolsCovid19');

    Route::post('aj/requests/saveRequest', 'Pasifai\Pysde\controllers\RequestController@saveRequest')->name('saveRequestYperarithmos');
    Route::post('aj/requests/permanentlyDeleteRequest', 'Pasifai\Pysde\controllers\RequestController@ajaxPermanentlyDeleteRequest')->name('permanentlyDeleteRequest');


    Route::post('aj/leitourgika/fetchSelectedSchools', 'Pasifai\Pysde\controllers\RequestController@ajaxFetchSchoolsLeitourgika')->name('fetchSelectedSchoolsYperarithmon');
    Route::post('aj/leitourgika/saveRequestLeitourgika', 'Pasifai\Pysde\controllers\RequestController@saveRequestLeitourgika')->name('saveRequestYperarithmosLeitourgika');
    Route::post('aj/leitourgika/permanentlyDeleteRequest', 'Pasifai\Pysde\controllers\RequestController@ajaxPermanentlyDeleteRequest')->name('permanentlyDeleteRequest');


    Route::post('aj/leitourgika/sentRequestForProtocolLeitourgika', 'Pasifai\Pysde\controllers\RequestController@ajaxSentLeitourgikaRequestForProtocol')->name('sentRequestLeitourgikaYperarithmos');

    Route::get('aj/vacuum/fetchSchools', 'Pasifai\Pysde\controllers\VacuumController@fetchAllSchools')->name('fetchSchools');
    Route::get('aj/vacuum/getKenaOfSchool/{forSchPysde}', 'Pasifai\Pysde\controllers\VacuumController@getVaccumOfSchool')
        ->middleware('bindings')
        ->name('getVacuum');
    Route::post('aj/vacuum/getTeachersOfSchool/{forSchPysde}', 'Pasifai\Pysde\controllers\VacuumController@getTeachersOfSchool')
        ->middleware('bindings')
        ->name('getVacuum');

    Route::post('aj/vacuum/runAlgorithm/{forSchPysde}', 'Pasifai\Pysde\controllers\VacuumController@runAlgorithm')
        ->middleware('bindings')
        ->name('runAlgorithm');

    Route::post('aj/vacuum/initializeAlgorithm/{forSchPysde}', 'Pasifai\Pysde\controllers\VacuumController@initializeAlgorithm')
        ->middleware('bindings')
        ->name('initializeAlgorithm');

    Route::post('aj/vacuum/saveCouncilChanges/{forSchPysde}', 'Pasifai\Pysde\controllers\VacuumController@saveCouncil')
    ->middleware('bindings')
    ->name('saveCouncil');
    
    Route::post('aj/vacuum/toggleYperarithmiaValueOfTeacher', 'Pasifai\Pysde\controllers\VacuumController@ajaxToggleTeacherRights')
    ->name('ajaxToggleTeacherRights');
    Route::post('aj/vacuum/saveDescriptionOfTeacher', 'Pasifai\Pysde\controllers\VacuumController@ajaxSaveDescriptionOfTeacher')
    ->name('ajaxSaveDescriptionOfTeacher');


    Route::get('aj/divisions/fetchDivisions', 'Pasifai\Pysde\controllers\SchoolVacuumController@ajaxGetDivisionsOfSchool')->name('fetchDivisions');
    Route::post('aj/divisions/saveDivisions', 'Pasifai\Pysde\controllers\SchoolVacuumController@saveDivisions')->name('saveDivisions');

    // Route::get('aj/lyceum/divisions/fetchDivisions', 'Pasifai\Pysde\controllers\LyceumVacuumController@ajaxGetDivisionsOfSchool')->name('fetchDivisionsLyceum');
    // Route::post('aj/lyceum/divisions/saveDivisions', 'Pasifai\Pysde\controllers\LyceumVacuumController@saveDivisions')->name('saveDivisionsLyceum');

    Route::get('aj/divisions/fetchVacuums', 'Pasifai\Pysde\controllers\SchoolKenaController@ajaxGetVacuumsOfSchool')->name('fetchVacuums');

});