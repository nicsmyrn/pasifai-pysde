<?php

namespace Pasifai\Pysde\repositories;

interface PlacementRepositoryInterface
{
    public function allNewKladoi();

    public function nextDay();

    public function untilDate();

    public function allSchools();

    public function getEidikotitesList($klados);

    public function currentYear();

    public function praxeisList($year);

    public function yearPeriod($year);

    public function createOrFindPraxi($request);

    public function createRecallPlacement($topothetisi, $date , $praxi, $dieuthidi);

    public function updateLeitourgikaKena($topothetisi);
}
