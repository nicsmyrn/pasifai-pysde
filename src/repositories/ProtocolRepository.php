<?php

namespace Pasifai\Pysde\repositories;

use Carbon\Carbon;
use Pasifai\Pysde\models\Protocol;
use Pasifai\Pysde\models\ProtocolArchives;

class ProtocolRepository{

    private $current_year;

    public function __construct()
    {
        $this->current_year = Carbon::now()->format('Y');
    }

    public function filterProtocols($type, $year = null)
    {
        if ( ($year == null) || ($year == $this->current_year) ){
            return Protocol::with('f')
                ->where(function($query) use ($type){
                    if ($type) {
                        if($type == 'pending'){
                            $query->where('pending',1);
                        }else{
                            if ($type == 'in') $t = 0;
                            if ($type == 'out') $t = 1;
                            if ($type == 'null') $t = 9;
                            $query->where('type', $t);
                        }
                    }
                })
                ->get();
        }else{
            return ProtocolArchives::with('f')
                ->whereYear('p_date','=', $year)
                ->where(function($query) use ($type){
                    if ($type) {
                        if($type == 'pending'){
                            $query->where('pending',1);
                        }else{
                            if ($type == 'in') $t = 0;
                            if ($type == 'out') $t = 1;
                            if ($type == 'null') $t = 9;
                            $query->where('type', $t);
                        }
                    }
                })
                ->get();
        }
    }
}