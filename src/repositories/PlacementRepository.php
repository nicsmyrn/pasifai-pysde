<?php

namespace Pasifai\Pysde\repositories;

use App\Models\Year;
use App\MySchoolTeacher;
use App\NewEidikotita;
use App\School;
use Illuminate\Support\Carbon;
use Pasifai\Pysde\models\Praxi;
use Pasifai\Pysde\models\Praxi as PasifaiPraxi;
use DB;
use Hamcrest\Type\IsString;
use Illuminate\Support\Facades\Artisan;
use Pasifai\Pysde\models\Placement;
use Pasifai\Pysde\models\SpecialPlacement;
use Illuminate\Http\Request;
use Log;
use Pasifai\Pysde\models\Organiki;

class PlacementRepository implements PlacementRepositoryInterface{

    public function allNewKladoi()
    {
        return NewEidikotita::all()->pluck('klados_name','klados_slug')->all();
    }
    
    public function allSchools()
    {
        return School::orderBy('name')->get();
    }

    public function nextDay()
    {
        return 'next day';
    }

    public function untilDate()
    {
        return 'until date';
    }

    public function currentYear()
    {
        return Carbon::now()->year;
    }

    public function getEidikotitesList($klados)
    {
        return NewEidikotita::where('klados_slug', $klados)->pluck('id');
    }

    public function praxeisList($year)
    {
        $year_placement = Year::where('name', $this->yearPeriod($year))->first();
        
        return Praxi::where('year_id', $year_placement->id)->pluck('id');
    }

    public function yearPeriod($year)
    {
        $year = $this->currentYear();

        if(in_array(Carbon::now()->month, [1,2,3,4,5,6])){
            return ($year-1).'-'.$year;
        }else{
            return $year.'-'.($year+1);
        }
    }

    public function initializeDataCreate(Request $request, $teacher_id = null)
    {
        if ($teacher_id != null){
            $teacher = MySchoolTeacher::find($teacher_id);
        }else{
            $teacher = MySchoolTeacher::find($request->get('teachers'));
        }

        $new_klados = NewEidikotita::find($request->get('klados_id'));

        $data = array(
            'klados' => $new_klados->eidikotita_slug . ' ('. $new_klados->eidikotita_name . ')',
            'praxi_id'  => $request->get('praxi_id'),
            'teacher_name'  => $teacher->last_name . ' ' . $teacher->first_name,
            'hours'         => $request->get('hours'),
            'from'          => School::find($request->get('from_id')) != null? School::find($request->get('from_id'))->name : \Config::get('requests.teacher_types')[$request->get('from_id')],
            'to'            => School::find($request->get('to_id')) != null ? School::find($request->get('to_id'))->name : \Config::get('requests.special_placements')[$request->get('to_id')],
            'description'   => $request->get('description'),
            'from_id'       => $request->get('from_id'),
            'to_id'         => School::find($request->get('to_id')) != null ? School::find($request->get('to_id'))->id : $request->get('to_id'),
            'klados_id'     => $new_klados->id,
            'afm'               => $teacher->afm,
            'placements_type'   => $request->get('placements_type'),
            'days'              => $request->get('days'),
            'me_aitisi'         => $request->has('me_aitisi') ? true : false,
            'starts_at'         => $request->get('starts_at'),
            'ends_at'           => $request->get('ends_at'),
            'pending'           => !!$request->has('pending')
        );

        return $data;
    }
    

    public function createOrFindPraxi($request)
    {
        $decision_date = $request->get('praxi_date');
        $decision_number = $request->get('praxi_number');
        $placements = $request->get('placements');
        $covid19 = $request->get("covid19");
        $dieuthidi = $request->get('dieuthidi') ? 12 : 11; 

        Log::warning("Create Or Find Praxi Method Dieuthidi value: $dieuthidi");
        
        foreach ($placements as $teacher){            
            foreach($teacher['placements'] as $placement) {

                if(is_string($placement['to'])){
                    $to_name = config('requests.special_placements')[$placement['to']];
                    $to_id = $placement['to'];
                }else{
                    $to_name = $placement['to']['name'];
                    $to_id = $placement['to']['id'];
                }
                
                // find old same placements and delete them...

                Placement::where('afm', $teacher['afm'])->where('to_id', $to_id)->delete();

                $new_placement = Placement::create([
                    'klados' => $teacher['klados'],
                    'praxi_id' => $this->getPraxiIdOrCreate($decision_number, $decision_date, $dieuthidi),
                    'teacher_name' => $teacher['full_name'],  //$placement['name'],
                    'hours' => $placement['hours'],
                    'from' => $teacher['organiki'],
                    'to' => $to_name,
                    'description' => $placement['description'],
                    'from_id' => $teacher['organiki_id'],
                    'to_id' => $to_id,
                    'klados_id' => $teacher['klados_id'],
                    'placements_type'   => $placement['placementType'],
                    'days'  => $placement['days'],
                    'me_aitisi' => $placement['aitisi'],
                    'pending' => $placement['pending'],
                    'afm'   => $teacher['afm'],
                    'starts_at' => $placement['starts_at'],
                    'ends_at' => $placement['ends_at']
                ]);
        
                //## ΑΛΛΑΓΗ ΠΙΝΑΚΑ ΛΕΙΤΟΥΡΓΙΚΩΝ ΚΕΝΩΝ - ΠΛΕΟΝΑΣΜΑΤΩΝ - START
                if($covid19 == "yes"){
                    $this->createTopothetisis($teacher, $placement, $to_id, $teacher['organiki_id'], $new_placement->praxi_id);

                     #############################
                        $myschoolTeacher = MySchoolTeacher::where('afm', $teacher['afm'])->first();

                        if($myschoolTeacher != null){
                            Log::warning("Looks that is works fine");
                            
                            // $schoolResult = $myschoolTeacher->topothetisis()
                            //                 ->whereIn('teacher_type', [0,5])
                            //                 ->where('hours', '>', 0)
                            //                 ->where('praxi_id', '<>', $new_placement->praxi_id)
                            //                 ->whereNull('praxi_id')
                            //                 ->first(); 
                                            // ψάχνει αν έχει οργανική ή προσωρινή τοποθέτηση
                            
                            $schoolResult = $myschoolTeacher->topothetisis->first(
                                function($v, $k) use($new_placement)
                                { 
                                    return in_array($v['pivot']['teacher_type'], [0,5]) && 
                                        $v['pivot']['hours'] > 0 && 
                                        $v['pivot']['praxi_id'] != $new_placement->praxi_id;
                                });

                            if($schoolResult != null){
                                Log::alert("INside 999");
                                $myschoolTeacher->topothetisis()->updateExistingPivot($schoolResult->id, [
                                    'hours'               => $schoolResult->pivot->hours - $placement['hours'],
                                    'project_hours'       => 0,
                                    'locked'              => true
                                ]);

                            }else{
                                Log::alert("out 666");
                            }
                        }

                    ##############################

                    if($to_id < 1000){
                        Artisan::call('leitourgika:update', $this->getParameters($to_id, $teacher['organiki_id']));
                    }else{
                        Artisan::call('leitourgika:update', $this->getParameters($teacher['organiki_id'], $to_id));
                    }
                }
                //## ΑΛΛΑΓΗ ΠΙΝΑΚΑ ΛΕΙΤΟΥΡΓΙΚΩΝ ΚΕΝΩΝ - ΠΛΕΟΝΑΣΜΑΤΩΝ - END
            }
        }

        return 'ok 2';

    }

    protected function getParameters($to, $from)
    {
        $parameters = [];

        $from_school = School::find($from);
        $to_school = School::find($to);

        if($from_school != null){      
            $parameters['from'] = $from_school->id;
        }

        if($to_school != null){
            $parameters['to'] = $to_school->id;
        }

        return $parameters;
    }

    public function createRecallPlacement($topothetisi, $date , $praxi, $dieuthidi)
    {
        Placement::create([
            'klados' => $topothetisi->klados,
            'praxi_id' => $this->getPraxiIdOrCreate($praxi, $date, $dieuthidi),
            'teacher_name' => $topothetisi->teacher_name,  //$placement['name'],
            'hours' => $topothetisi->hours,
            'from' => $topothetisi->from,
            'to' => $topothetisi->to,
            'description' => "ανάκληση {$topothetisi->praxi->decision_number}/{$topothetisi->praxi->decision_date}} πράξης προς το {$topothetisi->to}",
            'from_id' => $topothetisi->from_id,
            'to_id' => $topothetisi->to_id,
            'klados_id' => $topothetisi->klados_id,
            'placements_type'   => 9,  // number of anaklisi in config file
            'days'  => $topothetisi->days,
            'me_aitisi' => true, 
            'pending' => false,
            'afm'   => $topothetisi->afm,
            'starts_at' => '09/09/2020',
            'ends_at' => '30/06/2020'
        ]);
    }

    public function updateLeitourgikaKena($topothetisi)
    {
        Log::warning("DELETING...");
        Log::warning("Topothetisi:");
        Log::warning($topothetisi);


        $parameters = [];

        $from_school = School::find($topothetisi->from_id);

        $myschoolTeacher = MySchoolTeacher::find($topothetisi->afm);
        
        $baseTopothetisi = $myschoolTeacher->topothetisis->whereIn('pivot.teacher_type', [0,5])->first();
        // ψάχνει την κύρια τοποθέτηση του εκπαιδευτικού ή μόνιμου ή αναπληρωτή

        Log::error("BASE TOPOTHETISI");
        Log::error($baseTopothetisi);

            $sum = $baseTopothetisi->pivot->hours + $topothetisi->hours;

            Log::warning("SUM : $sum");

            $myschoolTeacher->topothetisis()->updateExistingPivot($baseTopothetisi->pivot->sch_id, [
                'hours'               => $sum
            ]); 
            // ενημέρωση του Σχολείου βάσης

        if($from_school != null){       
            $parameters['from'] = $from_school->id;
        }else{
            $parameters['from'] = $baseTopothetisi->pivot->sch_id;
        }

        $to_school = School::find($topothetisi->to_id);

        if($to_school != null){
            $to_school->topothetisis()->detach($topothetisi->afm);
            $parameters['to'] = $to_school->id;
        }

        Artisan::call('leitourgika:update', $parameters);

    }

    // ################### CHECK CODE LEITOURGIKA  S T A R T #########################
    private function createTopothetisis($teacher, $placement, $to_id, $organiki_id, $praxi_id)
    {        

        if(array_key_exists($placement['placementType'], config('requests.link_topothetisis_with_placements_from'))){
            $topothetisi_type_from = config('requests.link_topothetisis_with_placements_from')[$placement['placementType']];
        }else{
            $topothetisi_type_from =  null;
        }
        Log::warning("Teacher : {$teacher['full_name']}");
        Log::warning("From :");
        Log::warning($topothetisi_type_from);

        if(array_key_exists($placement['placementType'], config('requests.link_topothetisis_with_placements_to'))){
            $topothetisi_type_to = config('requests.link_topothetisis_with_placements_to')[$placement['placementType']];
        }else{
            $topothetisi_type_to =  null;
        }
        Log::warning("To Type : $topothetisi_type_to");
        
        $organiki = School::find($teacher['organiki_id']);

        if($organiki != null && $topothetisi_type_from != null){
            $organiki->topothetisis()->updateExistingPivot($teacher['afm'], [
                'teacher_type'        => $topothetisi_type_from,
                'date_ends'           => $placement['ends_at'],
                'hours'               => 0,
                'project_hours'       => 0,
                'locked'              => true
            ]);

        }else{
            //nothing...
        }
        $to_School = School::find($to_id);

        Log::warning("To School : $to_id");

        if($to_School != null && $topothetisi_type_to != null){
            $exists = $to_School->topothetisis->firstWhere('afm', $teacher['afm']);

            if($exists != null){
                Log::alert('Already exists '. $teacher['afm'] . ' to ' . $to_School->name);
                $to_School->topothetisis()->updateExistingPivot($teacher['afm'], [
                    'teacher_type'        => $topothetisi_type_to,
                    'date_ends'           => $placement['ends_at'],
                    'hours'               => $placement['hours'],
                    'project_hours'       => 0,
                    'locked'              => true
                ]);
            }else{
                $to_School->topothetisis()->attach($teacher['afm'], [
                    'teacher_type'        => $topothetisi_type_to,
                    'date_ends'           => $placement['ends_at'],
                    'hours'               => $placement['hours'],
                    'project_hours'       => 0,
                    'locked'              => true,
                    'url'                   => '',
                    'praxi_id'            => $praxi_id
                ]);
            }
        }else{
            $special =  SpecialPlacement::find($to_id);
            $special->special_topothetisis()->attach($teacher['afm'], [
                'teacher_type'  => $topothetisi_type_to,
                'date_ends'     => $placement['ends_at'],
                'hours'         => $placement['hours'],
                'locked'        => true,
                'url'           => '',
                'praxi_id'            => $praxi_id
            ]);
            //nothing
        }
    }
    // ################### CHECK CODE LEITOURGIKA  E N D #########################


    private function getPraxiIdOrCreate($number, $date, $dieuthidi)
    {
        $year_id = Year::where('current', true)->first()->id;

        Log::warning("DIEUTHIDI 2 = $dieuthidi");

        Log::warning("Decision Number = $number");

        $p = Praxi::where('decision_number', $number)
            ->where('year_id', $year_id)
            ->where('praxi_type', $dieuthidi)
            ->first();


        if ($p == null){
            $praxi = Praxi::create([
                'decision_number' => $number,
                'decision_date' => Carbon::createFromFormat('d/m/Y', $date),
                'praxi_type'    => $dieuthidi,
                'year_id'       => $year_id,
                'description'   => ''
            ]);
            $praxi_id = $praxi->id;
        }else{
            $praxi_id = $p->id;
        }

        return $praxi_id;
    }

}