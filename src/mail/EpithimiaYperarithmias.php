<?php

namespace Pasifai\Pysde\mail;

use App\School;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EpithimiaYperarithmias extends Mailable
{
    use Queueable, SerializesModels;

    public $attachment;
    public $userEmail;
    public $full_name;

    public function __construct($attachement, $userEmail, $full_name)
    {
        $this->attachment = $attachement;
        $this->userEmail = $userEmail;
        $this->full_name = $full_name;
    }   

    public function build()
    {
        return $this->attach($this->attachment)
            ->from($this->userEmail)
            ->to(config()->get('requests.MAIL_PYSDE'), config()->get('requests.NAME_PYSDE'))
            ->cc($this->userEmail)
            ->subject('Αίτηση για χαρακτηρισμό ως ΥΠΕΡΑΡΙΘΜΟΣ')
            ->markdown('pysde::email.teacher-yperarithmia')
            ->with([
                'teacher_name' => $this->full_name
            ]);
    }
}
