<?php

namespace Pasifai\Pysde\mail;

use App\School;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class recallAnswer extends Mailable
{
    use Queueable, SerializesModels;

    public $attachment;
    public $userEmail;
    public $full_name;
    public $answer;

    public function __construct($attachement, $userEmail, $full_name, $answer)
    {
        $this->attachment = $attachement;
        $this->userEmail = $userEmail;
        $this->full_name = $full_name;
        $this->answer = $answer;
    }   

    public function build()
    {
        return $this->attach($this->attachment)
            ->from(config()->get('requests.MAIL_PYSDE'), config()->get('requests.NAME_PYSDE'))
            ->to($this->userEmail)
            ->subject('Απάντηση αιτήματος ανάκλησης αίτησης')
            ->markdown('pysde::email.teacher-answer-cancellation-request')
            ->with([
                'teacher_name' => $this->full_name,
                'answer'    => $this->answer
            ]);
    }
}
