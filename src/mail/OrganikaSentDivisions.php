<?php

namespace Pasifai\Pysde\mail;

use App\School;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrganikaSentDivisions extends Mailable
{
    use Queueable, SerializesModels;

    public $attachment;
    public $userEmail;
    public $full_name;

    public function __construct($attachement, $userEmail, $full_name)
    {
        $this->attachment = $attachement;
        $this->userEmail = $userEmail;
        $this->full_name = $full_name;
    }   

    public function build()
    {
        return $this//->attach($this->attachment)
            ->from($this->userEmail, $this->full_name)
            ->to(config()->get('requests.MAIL_PYSDE'), config()->get('requests.NAME_PYSDE'))
            ->cc($this->userEmail)
            ->subject('Πίνακες δηλώσεων τμημάτων - Πίνακας Οργανικών Κενών Πλεονασμάτων')
            ->markdown('pysde::email.school-organika-divisions')
            ->with([
                'school_name' => $this->full_name
            ]);
    }
}
