<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KenaPivotTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_kena_assignment', function(Blueprint $table){
            $table->integer('wrologio_id')->unsigned();
            $table->foreign('wrologio_id')->references('id')->on('_kena_lesson_div')->onDelete('cascade');
        
            $table->integer('eid_id')->unsigned();
            $table->foreign('eid_id')->references('id')->on('eidikotites_new')->onDelete('cascade');

            $table->string('type'); // Ανάθεση Μαθημάτων Α', Β', Γ'

            $table->primary(['wrologio_id', 'eid_id']);
        });

        Schema::create('_kena_sch_divs', function(Blueprint $table){
            $table->integer('sch_id')->unsigned();
            $table->foreign('sch_id')->references('id')->on('schools')->onDelete('cascade');

            $table->integer('div_id')->unsigned();
            $table->foreign('div_id')->references('id')->on('_kena_divisions')->onDelete('cascade');

            $table->integer('number');

            $table->timestamps();
        });

        Schema::create('_kena_sum_hours', function(Blueprint $table){
            $table->integer('eid_id')->unsigned();
            $table->foreign('eid_id')->references('id')->on('eidikotites_new')->onDelete('cascade');

            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');

            $table->primary(['eid_id', 'school_id']);

            $table->integer('forseen')->nullable();
            $table->integer('available')->nullable();
            $table->integer('difference')->nullable();

            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_kena_sum_hours');
        Schema::dropIfExists('_kena_sch_divs');
        Schema::dropIfExists('_kena_assignment');
    }
}
