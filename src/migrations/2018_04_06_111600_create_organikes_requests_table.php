<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganikesRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organikes_requests', function (Blueprint $table) {
            $table->increments('id');

            $table->string('unique_id')->unique();

            $table->integer('protocol_number')->nullable();

            $table->integer('teacher_id')->unsigned();
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');

            $table->string('school_organiki')->nullable();
            $table->string('file_name')->nullable();

            $table->string('aitisi_type');
            $table->timestamp('date_request');

            $table->string('situation');
            $table->text('description')->nullable();

            $table->float('proipiresia', 6, 3)->default(0)->unsigned();
            $table->integer('family')->default(0)->unsigned();
            $table->integer('kids')->default(0)->unsigned();
            $table->integer('ygeia_idiou')->default(0)->unsigned();
            $table->integer('ygeia_sizigou')->default(0)->unsigned();
            $table->integer('ygeia_teknon')->default(0)->unsigned();
            $table->integer('ygeia_goneon')->default(0)->unsigned();
            $table->integer('ygeia_aderfon')->default(0)->unsigned();
            $table->integer('exosomatiki')->default(0)->unsigned();
            $table->integer('entopiotita')->default(0)->unsigned();
            $table->integer('sinipiretisi')->default(0)->unsigned();
            $table->float('sum', 6, 3)->default(0)->unsigned();
            $table->float('moria_dieuthidi', 6,3);
            $table->string('groupType')->nullable();
            $table->boolean('stay_to_organiki')->default(false);
            $table->boolean('return_to_organiki')->default(false);

            $table->unique(['teacher_id', 'aitisi_type']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organikes_requests');
    }
}
