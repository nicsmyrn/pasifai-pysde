<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeitourgikaProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_kena_leitourgika_sum_hours', function(Blueprint $table){
            $table->integer('eid_id')->unsigned();
            $table->foreign('eid_id')->references('id')->on('eidikotites_new')->onDelete('cascade');

            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');

            $table->primary(['eid_id', 'school_id']);

            $table->integer('forseen')->nullable();
            $table->integer('available')->nullable();
            $table->integer('difference')->nullable();

            $table->integer('user_id')->unsigned();;
            $table->foreign('user_id')->references('id')->on('users');

            $table->boolean('locked')->default(true);

            $table->timestamps();
        }); 

        Schema::create('_kena_leitourgika', function(Blueprint $table){
            $table->integer('sch_id')->unsigned();
            $table->foreign('sch_id')->references('id')->on('schools')->onDelete('cascade');

            $table->string('afm',9);
            $table->foreign('afm')->references('afm')->on('myschool_teachers')->onDelete('cascade');

            $table->primary(['sch_id', 'afm']);

            $table->integer('teacher_type')->nullable();
            $table->date('date_ends')->nullable();

            $table->integer('hours')->default(0);
            $table->integer('project_hours')->default(0);

            $table->boolean('locked')->default(true);

            $table->timestamps();
        });     
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_kena_leitourgika_sum_hours');
        Schema::dropIfExists('_kena_leitourgika');    

    }
}
