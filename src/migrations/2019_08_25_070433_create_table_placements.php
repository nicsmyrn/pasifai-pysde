<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlacements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create praxeis table
        Schema::create('praxeis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('decision_number')->unique();
            $table->date('decision_date');
            $table->text('description');
            $table->timestamps();
        });

        //Create placements table
        Schema::create('placements', function (Blueprint $table) {
            $table->increments('id');

            $table->string('afm',9);
            $table->foreign('afm')->references('afm')->on('myschool_teachers');

            $table->integer('praxi_id')->unsigned();
            $table->foreign('praxi_id')->references('id')->on('praxeis')->onDelete('cascade');


            $table->string('teacher_name');
            $table->integer('hours')->unisgned()->nullable();
            $table->string('from');
            $table->string('to');
            $table->text('description')->nullable();


            $table->integer('from_id')->unsigned();
            $table->integer('to_id')->unsigned();
            
            $table->string('klados');
            $table->integer('klados_id')->unsigned();
            $table->foreign('klados_id')->references('id')->on('eidikotites_new');

            $table->integer('placements_type')->unsigned();
            $table->integer('days')->unsigned();
            $table->boolean('me_aitisi');
            $table->date('starts_at')->nullable();
            $table->date('ends_at')->nullable();
            $table->boolean('pending')->default(false);


            $table->softDeletes();
            $table->timestamps();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('placements');
        Schema::drop('praxeis');    
    }
}
