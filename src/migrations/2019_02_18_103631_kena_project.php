<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KenaProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_kena_lessons', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('group');
        });  

        Schema::create('_kena_divisions', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('class', 10);
            $table->string('school_type');
            $table->string('school_group');
        });

        Schema::create('_kena_lesson_div', function(Blueprint $table){
            $table->increments('id');

            $table->integer('l_id')->unsigned();
            $table->foreign('l_id')->references('id')->on('_kena_lessons')->onDelete('cascade');

            $table->integer('d_id')->unsigned();
            $table->foreign('d_id')->references('id')->on('_kena_divisions')->onDelete('cascade');

            $table->unique(['l_id', 'd_id']);

            $table->integer('hours');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_kena_lesson_div');        
        Schema::dropIfExists('_kena_lessons');
        Schema::dropIfExists('_kena_divisions');        
    }
}
