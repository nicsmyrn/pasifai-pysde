<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKenaProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create _kena_projects table
        Schema::create('_kena_projects', function (Blueprint $table) {
            $table->integer('eid_id')->unsigned();
            $table->foreign('eid_id')->references('id')->on('eidikotites_new')->onDelete('cascade');

            $table->integer('sch_id')->unsigned();
            $table->foreign('sch_id')->references('id')->on('schools')->onDelete('cascade');

            $table->primary(['eid_id', 'sch_id']);

            $table->integer('hours');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_kena_projects');
    }
}
