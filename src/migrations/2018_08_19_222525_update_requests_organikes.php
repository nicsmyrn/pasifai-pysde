<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequestsOrganikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organikes_requests', function(Blueprint $table){
            $table->removeColumn('moria_dieuthidi');
            $table->removeColumn('return_to_organiki');

            $table->integer('hours_for_request')->nullable();
            $table->string('subject')->nullable();
            $table->boolean('sizigos_stratiotikou')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
