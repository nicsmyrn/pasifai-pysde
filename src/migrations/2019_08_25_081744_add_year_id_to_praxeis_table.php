<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYearIdToPraxeisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('praxeis', function (Blueprint $table) {
            $table->integer('year_id')->unsigned();
            $table->foreign('year_id')->references('id')->on('years_placements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('praxeis', function (Blueprint $table) {
            $table->dropForeign('year_id');
            $table->dropIfExists('year_id');
            
        });
    }
}
