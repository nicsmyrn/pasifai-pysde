<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKenaLeitourgikaCovid19 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_kena_leitourgika_covid19', function(Blueprint $table){
            $table->integer('sch_id')->unsigned();
            $table->foreign('sch_id')->references('id')->on('schools')->onDelete('cascade');

            $table->integer('eid_id')->unsigned();
            $table->foreign('eid_id')->references('id')->on('eidikotites_new')->onDelete('cascade');


            $table->primary(['sch_id', 'eid_id']);

            $table->date('date_ends')->nullable();

            $table->integer('hours')->default(0);

            $table->boolean('locked')->default(true);

            $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_kena_leitourgika_covid19');    
    }
}
