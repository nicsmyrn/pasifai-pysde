<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPraxiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('praxeis', function (Blueprint $table) {
            $table->integer('dde_protocol')->unsigned()->nullable();
            $table->date('dde_protocol_date')->nullable();
            $table->integer('praxi_type');
            $table->string('ada')->nullable();
            $table->string('url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('praxeis', function (Blueprint $table) {
            $table->dropColumn('praxi_type');
            $table->dropColumn('ada');
            $table->dropColumn('url');
            $table->dropColumn('dde_protocol');
            $table->dropColumn('dde_protocol_date');
        });
    }
}
