<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToYperarithmiesRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests_yperarithmias', function(Blueprint $table){
            $table->timestamp('recall_date')->nullable();
            $table->string('recall_situation')->nullable();
        });
        Schema::table('organikes_requests', function(Blueprint $table){
            $table->timestamp('recall_date')->nullable();
            $table->string('recall_situation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests_yperarithmias', function(Blueprint $table){
            $table->dropColumn('recall_date');
            $table->dropColumn('recall_situation');
        });
        Schema::table('organikes_requests', function(Blueprint $table){
            $table->dropColumn('recall_date');
            $table->dropColumn('recall_situation');
        });
    }
}
