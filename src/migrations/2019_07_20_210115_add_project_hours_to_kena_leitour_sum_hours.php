<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectHoursToKenaLeitourSumHours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_kena_leitourgika_sum_hours', function (Blueprint $table) {
            $table->integer('project_hours')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_kena_leitourgika_sum_hours', function (Blueprint $table) {
            $table->dropColumn('project_hours');
        });
    }
}
