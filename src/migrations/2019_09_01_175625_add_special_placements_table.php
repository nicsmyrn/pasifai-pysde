<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpecialPlacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_kena_leitourgika_special', function(Blueprint $table){
            $table->integer('id')->unsigned();
            $table->primary('id');

            $table->string('name');

            $table->timestamps();
        });

        Schema::create('_kena_leitourgika_special_pivot', function(Blueprint $table){
            $table->integer('special_id')->unsigned();
            $table->foreign('special_id')->references('id')->on('_kena_leitourgika_special')->onDelete('cascade');

            $table->string('afm', 9);
            $table->foreign('afm')->references('afm')->on('myschool_teachers')->onDelete('cascade');

            $table->integer('teacher_type')->default(0);

            $table->date('date_ends')->nullable();

            $table->integer('hours')->unsigned()->default(0);
            $table->boolean('locked')->default(0);
            $table->string('url')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_kena_leitourgika_special_pivot');
        Schema::dropIfExists('_kena_leitourgika_special');
    }
}
