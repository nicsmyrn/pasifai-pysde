<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsYperarithmiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create requests_yperarithmias table
        Schema::create('requests_yperarithmias', function (Blueprint $table) {

            $table->integer('protocol_number')->unsigned();
            $table->integer('year')->unsigned();

            $table->string('unique_id')->unique();

            $table->integer('teacher_id')->unsigned();
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');

            $table->boolean('want_yperarithmia')->default(true);

            $table->string('school_organiki')->nullable();
            $table->string('file_name')->nullable();

            $table->timestamp('date_request');
            $table->text('description')->nullable();

            $table->string('name');
            $table->timestamps();

            $table->primary(['protocol_number', 'year']);
            $table->unique(['teacher_id', 'year']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests_yperarithmias');
    }
}
