<?php

use App\Sch_gr\Facade\Sch;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNumberOfTeachersToLeitourgika extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_kena_leitourgika_sum_hours', function(Blueprint $table){
            $table->integer('number_of_teachers')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_kena_leitourgika_sum_hours', function(Blueprint $table){
            $table->dropColumn('number_of_teachers');
        });
    }
}
