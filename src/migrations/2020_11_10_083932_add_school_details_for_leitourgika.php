<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchoolDetailsForLeitourgika extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_kena_leitourgika_sum_hours', function(Blueprint $table){
            $table->integer('sch_difference')->default(0);
            $table->longText('sch_description')->nullable();
            $table->boolean('sch_locked')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_kena_leitourgika_sum_hours', function(Blueprint $table){
            $table->removeColumn('sch_difference');
            $table->removeColumn('sch_description');
            $table->removeColumn('sch_locked');
        });    }
}
