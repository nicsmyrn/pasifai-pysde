<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProtocolTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create f_list table
        Schema::create('f', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
        });

        //Create protocols table
        Schema::create('protocols', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('type');
            $table->date('p_date');
            $table->string('from_to');
            $table->string('subject');

            $table->integer('f_id')->unsigned();
            $table->foreign('f_id')->references('id')->on('f');


            $table->longText('description');
            $table->boolean('printed')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('protocols');
        Schema::dropIfExists('f');
    }
}
