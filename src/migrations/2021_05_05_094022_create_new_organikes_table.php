<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewOrganikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_organikes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('am');
            $table->foreign('am')->references('am')->on('edata')->onUpdate('cascade');

            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('pref_number')->unsigned();

            $table->unique(['am', 'school_id']);

            $table->integer('praxi_id')->unsigned()->nullable();
            $table->foreign('praxi_id')->references('id')->on('praxeis');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_organikes');
    }
}
