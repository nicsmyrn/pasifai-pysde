<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYperarithmiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create yperarithmies table
        Schema::create('yperarithmies', function (Blueprint $table) {
            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools');

            $table->integer('eidikotita_id')->unsigned();
            $table->foreign('eidikotita_id')->references('id')->on('eidikotites_new');

            $table->primary(['school_id', 'eidikotita_id']);

            $table->integer('number')->unsigned();
            $table->string('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yperarithmies');
    }
}
