<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNumberSchoolProvideToPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_kena_sch_divs', function (Blueprint $table) {
            $table->integer('numberSchoolProvide')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_kena_sch_divs', function (Blueprint $table) {
            $table->dropColumn('numberSchoolProvide');
        });
    }
}
