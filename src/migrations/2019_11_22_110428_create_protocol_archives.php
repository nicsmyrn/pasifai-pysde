<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProtocolArchives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('protocol-archives', function (Blueprint $table) {
            $table->increments('aa');

            $table->integer('id')->unsigned();
            $table->binary('type');
            $table->date('p_date');
            $table->string('from_to');
            $table->string('subject');

            $table->integer('f_id')->unsigned();
            $table->foreign('f_id')->references('id')->on('f');


            $table->longText('description');
            $table->boolean('printed')->default(false);

            $table->boolean('pending');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('protocol-archives');
    }
}
