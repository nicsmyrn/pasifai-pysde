<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganikesForSchools extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_kena_organikes', function(Blueprint $table){
            $table->integer('sch_id')->unsigned();
            $table->foreign('sch_id')->references('id')->on('schools')->onDelete('cascade');

            $table->string('afm',9);
            $table->foreign('afm')->references('afm')->on('myschool_teachers')->onDelete('cascade');

            $table->primary('afm');

            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organikes');    
    }
}
