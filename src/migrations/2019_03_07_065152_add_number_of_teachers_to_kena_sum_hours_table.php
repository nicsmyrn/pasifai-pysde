<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNumberOfTeachersToKenaSumHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_kena_sum_hours', function (Blueprint $table) {
            $table->integer('number_of_teachers')->unsigned()->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_kena_sum_hours', function (Blueprint $table) {
            $table->dropColumn('number_of_teachers');
        });
    }
}
