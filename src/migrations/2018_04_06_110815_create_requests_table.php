<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create requests table
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');

            $table->string('unique_id')->unique();

            $table->integer('protocol_number')->nullable();

            $table->integer('teacher_id')->unsigned();
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');

            $table->string('schools_that_is')->nullable();
            $table->string('file_name')->nullable();


            $table->string('aitisi_type');
            $table->timestamp('date_request');

            $table->boolean('stay_to_organiki')->nullable();
            $table->integer('hours_for_request')->nullable();

            $table->string('subject')->nullable();
            $table->text('description')->nullable();

            $table->smallInteger('reason')->nullable();

            $table->boolean('sizigos_stratiotikou')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
