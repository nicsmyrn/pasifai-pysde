<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToKenaOrganika extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('_kena_sch_divs', function(Blueprint $table){
            $table->boolean('pedio2')->default(false);
            $table->boolean('pedio3')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('_kena_sch_divs', function(Blueprint $table){
            $table->dropColumn('pedio2');
            $table->dropColumn('pedio3');
        });
    }
}
