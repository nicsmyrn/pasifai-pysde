<?php

namespace Pasifai\Pysde\models;

use Illuminate\Database\Eloquent\Model;
use App\NewEidikotita;

class Lesson extends Model
{
    protected $table = '_kena_lessons';

    protected $fillable = [
        'name',
        'group'
    ];

    public function wrologio()
    {
        return $this->belongsToMany(Division::class, '_kena_lesson_div', 'l_id', 'd_id')
                    ->using(WrologioLesson::class)
                    ->withPivot('id','hours');
    }
}
