<?php

namespace Pasifai\Pysde\models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Protocol extends Model
{
    protected $table = 'protocols';

    protected $fillable = [
        'id',
        'type',
        'p_date',
        'from_to',
        'subject',
        'f_id',
        'description',
        'printed',
        'pending'
    ];

    public function f()
    {
        return $this->belongsTo(F::class, 'f_id', 'id');
    }

    public function setPDateAttribute($date){
        $this->attributes['p_date'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function getPDateAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }

    public static  function LastProtocol()
    {
        $last_protocol = Protocol::orderBy('id', 'desc')->first();
        return ($last_protocol != null)? $last_protocol->id: 0;
    }

    public function getTypeAttribute($type){
        if ($type == 0){
            return 'Εισερχόμενο';
        }elseif($type == 1){
            return 'Εξερχόμενο';
        }elseif($type == 9){
            return 'Κενό';
        }
    }

    public function getFNameAttribute()
    {
        return $this->f->name;
    }

    public function getProtocolNameAttribute()
    {
        return $this->f_name . ' / ' . $this->id . ' / ' . str_replace('/','-',$this->p_date);
    }

    public function getIsNullAttribute()
    {
        if ($this->type == 9){
            return true;
        }else{
            return false;
        }
    }

    public function getYearAttribute()
    {
        return Carbon::createFromFormat('d/m/Y',$this->p_date)->format('Y');
    }

    public function getNowAttribute()
    {
        return Carbon::now()->format('Y');
    }
}
