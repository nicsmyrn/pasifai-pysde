<?php

namespace Pasifai\Pysde\models;

use Pasifai\Pysde\models\Protocol;
use Pasifai\Pysde\models\ProtocolArchives;
//ok
use App\Teacher;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OrganikiRequest extends Model
{
    protected $table = 'organikes_requests';

    protected $fillable = [
        'unique_id',
        'protocol_number',
        'teacher_id',
        'school_organiki',
        'file_name',
        'date_request',
        'aitisi_type',
        'situation',
        'description',

        'proipiresia',
        'family',
        'kids',
        'ygeia_idiou',
        'ygeia_sizigou',
        'ygeia_teknon',
        'ygeia_goneon',
        'ygeia_aderfon',
        'exosomatiki',
        'entopiotita',
        'sinipiretisi',
        'sum',

        'groupType',
        'stay_to_organiki',

        'hours_for_request',
        'subject',
        'sizigos_stratiotikou',
        'sistegazomeno',

        'recall_date',
        'recall_situation'
    ];

    public function protocol()
    {
        return $this->belongsTo(ProtocolTeacher::class, 'protocol_number', 'id');
    }

    public function prefrences()
    {
        return $this->hasMany(OrganikiPrefrences::class, 'request_id','id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }

    public function getDateReqAttribute($date)
    {
        return Carbon::parse($this->date_request)->format('d/m/Y -  H:i:s');   // W H Y ?????
    }

    public function getProtocolDateAttribute()
    {
        $protocol = Protocol::find($this->protocol_number);

        if ($protocol != null){
            return str_replace('/','-',$protocol->p_date);
        }else{
            return '';
        }
    }

    public function getFNameAttribute()
    {
        $protocol = Protocol::find($this->protocol_number);

        return $protocol->f_name;
    }

    public function getProtocolNameAttribute()
    {
        $year =  Carbon::parse($this->date_request)->format('Y');

        if ($year == Carbon::now()->format('Y')){
            $protocol = Protocol::find($this->protocol_number);
        }else{
            $protocol = ProtocolArchives::where('id',$this->protocol_number)->first();
        }

        if($protocol != null){
            return $protocol->protocol_name;
        }else{
            return 'keno';
        }

    }
}
