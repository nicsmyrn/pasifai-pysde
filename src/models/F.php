<?php

namespace Pasifai\Pysde\models;

use Illuminate\Database\Eloquent\Model;

class F extends Model
{
    protected $table = 'f';

    protected $fillable = [
        'name',
        'description'
    ];

    public $timestamps = false;

    public function protocols()
    {
        return $this->hasMany(Protocol::class,'f_id', 'id');
    }

    public function setFListAttribute()
    {
        return "$this->name - $this->description";
    }
}
