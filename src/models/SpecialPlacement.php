<?php

namespace Pasifai\Pysde\models;

use App\MySchoolTeacher;
use Illuminate\Database\Eloquent\Model;
use Pasifai\Pysde\models\Pivot\TopothetisisPivot;

class SpecialPlacement extends Model
{
    protected $table = '_kena_leitourgika_special';

    protected $fillable = [
        'id',
        'name'
    ];

    public function special_topothetisis()
    {
        return $this->belongsToMany(MySchoolTeacher::class, '_kena_leitourgika_special_pivot','special_id', 'afm')
            ->using(TopothetisisPivot::class)
            ->withPivot('teacher_type', 'date_ends', 'hours', 'locked', 'url')
            ->withTimestamps();
    }

}
