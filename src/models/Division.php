<?php

namespace Pasifai\Pysde\models;

use Illuminate\Database\Eloquent\Model;
use App\School;

class Division extends Model
{
    protected $table = '_kena_divisions';

    protected $fillable = [
        'name',
        'class',
        'school_type',
        'school_group'
    ];

    public function wrologio()
    {
        return $this->belongsToMany(Lesson::class, '_kena_lesson_div', 'd_id', 'l_id')
                    ->using(WrologioLesson::class)
                    ->withPivot('id','hours');
    }

    public function schools()
    {
        return $this->belongsToMany(School::class, '_kena_sch_divs', 'div_id', 'sch_id')
        ->withPivot('number', 'students', 'numberSchoolProvide', 'oligomeles', 'locked', 'pedio2', 'pedio3')
        ->withTimestamps();
    }
}
