<?php

namespace Pasifai\Pysde\models;

use App\Models\Edata;
use App\School;
use Illuminate\Database\Eloquent\Model;

class NewOrganiki extends Model
{
    protected $table = 'new_organikes';

    protected $fillable = [
        'id',
        'am',
        'school_id',
        'pref_number',
        'praxi_id'
    ];

    // just do it ...

    public function teacher()
    {
        return $this->hasOne(Edata::class, 'am', 'am');
    }

    public function organiki()
    {
        return $this->hasOne(School::class, 'id', 'school_id');
    }
}
