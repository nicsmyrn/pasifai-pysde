<?php

namespace Pasifai\Pysde\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use App\School;
use App\NewEidikotita;

// class WrologioLesson extends Model
class WrologioLesson extends Pivot
{
    protected $table = '_kena_lesson_div';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'l_id',
        'd_id',
        'hours'
    ];

    public function eidikotites()
    {
        return $this->belongsToMany(NewEidikotita::class, '_kena_assignment', 'wrologio_id', 'eid_id')
                    ->withPivot('type');
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class, 'l_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'd_id', 'id');
    }
    
    public function getLessonNameAttribute()
    {
        return $this->lesson->name;
    }

}
