<?php

namespace Pasifai\Pysde\models;

use App\Models\ProtocolTeacher;
use App\Models\ProtocolTeacherArchives;
use App\Teacher;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RequestTeacher extends Model
{
    protected $table = 'requests';

    protected $fillable = [
        'unique_id',
        'protocol_number',
        'teacher_id',
        'aitisi_type',
        'date_request',
        'stay_to_organiki',
        'hours_for_request',
        'subject',
        'schools_that_is',
        'description',
        'file_name',
        'reason',

        'school_organiki',
        'situation',
        'proipiresia',
        'family',
        'kids',
        'ygeia_idiou',
        'ygeia_sizigou',
        'ygeia_teknon',
        'ygeia_goneon',
        'ygeia_aderfon',
        'exosomatiki',
        'entopiotita',
        'sinipiretisi',
        'sum',
        'groupType'
    ];



    public function prefrences()
    {
        return $this->hasMany(Prefrences::class, 'request_id','id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }

    public function getDateReqAttribute($date)
    {
        return Carbon::parse($this->date_request)->format('d/m/Y -  H:i:s');   // W H Y ?????
    }

    public function getProtocolDateAttribute()
    {
        $protocol = ProtocolTeacher::find($this->protocol_number);

        if ($protocol != null){
            return str_replace('/','-',$protocol->p_date);
        }else{
            return '';
        }
    }

    public function getFNameAttribute()
    {
        $protocol = ProtocolTeacher::find($this->protocol_number);

        return $protocol->f_name;
    }

    public function getProtocolNameAttribute()
    {
        $year =  Carbon::parse($this->date_request)->format('Y');


        if ($year == Carbon::now()->format('Y')){
            $protocol = ProtocolTeacher::find($this->protocol_number);
        }else{
            $protocol = ProtocolTeacherArchives::where('id',$this->protocol_number)->first();
        }

        return $protocol->protocol_name;
    }

    public function getTypeAttribute()
    {
        switch($this->aitisi_type){
            case 'ΑΠΛΗ':
                $f_id = 1;      // Φ1
                break;
            case 'E1':
                $f_id = 6;      // Φ4
                break;
            case 'E2':
                $f_id = 6;      // Φ4
                break;
            case 'E3':
                $f_id = 6;      // Φ4
                break;

            case 'E4':
                $f_id = 7;      // Φ5
                break;
            case 'E5':
                $f_id = 6;      // Φ4
                break;
            case 'E6':
                $f_id = 6;      // Φ4
                break;
            case 'E7':
                $f_id = 6;      // Φ4
                break;
            default:
                $f_id = 1;
        }
        return $f_id;
    }
}
