<?php

namespace Pasifai\Pysde\models;

use Illuminate\Database\Eloquent\Model;
use App\NewEidikotita;
use App\School;
use App\MySchoolTeacher;
use Illuminate\Database\Eloquent\Builder;


class Topothetisi extends Model
{
    protected $table = '_kena_leitourgika';

    protected $fillable = [
        'sch_id',
        'afm',
        'teacher_type',
        'date_ends',
        'hours',
        'project_hours',
        'locked'
    ];
    
    // protected $primary_key = ['sch_id', 'afm'];

    // protected $dates = ['date_ends'];


    public function teacher()
    {
        return $this->hasOne(MySchoolTeacher::class, 'afm', 'afm');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'sch_id', 'id');
    }

    public function setDateEndsAttribute($date){
        if($date == null){
            $this->attributes['date_ends'] = null;
        }else{
            $this->attributes['date_ends'] = Carbon::createFromFormat('d/m/Y', $date);
        }
    }

    public function getDateEndsAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where('sch_id', '=', $this->getAttribute('sch_id'))
            ->where('afm', '=', $this->getAttribute('afm'));
        return $query;
    }
}
