<?php

namespace Pasifai\Pysde\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MySchoolTeacher;
use Pasifai\Pysde\models\Praxi;
use Carbon\Carbon;

class Placement extends Model
{
    use SoftDeletes;

    protected $table = 'placements';

    protected $fillable = [
        'klados',
        'praxi_id',
        'teacher_name',
        'hours',
        'from',
        'to',
        'description',
        'from_id',
        'to_id',
        'klados_id',
        'placements_type',
        'days',
        'me_aitisi',
        'starts_at',
        'ends_at',
        'afm',
        'pending'
    ];

    public function teacher()
    {
        return $this->belongsTo(MySchoolTeacher::class,'afm','afm');
    }

    protected $dates = ['deleted_at', 'starts_at','ends_at'];

    public function praxi()
    {
        return $this->belongsTo(Praxi::class);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d-m-Y');
    }

    public function setStartsAtAttribute($date){
        $this->attributes['starts_at'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function getStartsAtAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }

    public function setEndsAtAttribute($date){
        $this->attributes['ends_at'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function getEndsAtAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }
}
