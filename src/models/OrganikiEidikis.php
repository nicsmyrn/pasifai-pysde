<?php

namespace Pasifai\Pysde\models;

use Illuminate\Database\Eloquent\Model;
use App\NewEidikotita;
use App\School;
use App\MySchoolTeacher;

class OrganikiEidikis extends Model
{
    protected $table = '_kena_organikes_eidikis';

    protected $fillable = [
        'sch_id',
        'afm',
        'checked',
        'year_commit'
    ];

    public function teacher()
    {
        return $this->hasOne(MySchoolTeacher::class, 'afm', 'afm');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'sch_id', 'id');
    }
}
