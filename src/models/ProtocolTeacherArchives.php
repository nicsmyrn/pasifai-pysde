<?php

namespace Pasifai\Pysde\models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Pasifai\Pysde\models\F;

class ProtocolTeacherArchives extends Model
{
    protected $table = 'teacher-protocol-archives';

    protected $primaryKey = 'aa';

    protected $fillable = [
        'aa',
        'id',
        'type',
        'p_date',
        'from',
        'subject',
        'f_id',
        'description'
    ];

    public function f()
    {
        return $this->belongsTo(F::class, 'f_id', 'id');
    }

    public function setPDateAttribute($date){
        $this->attributes['p_date'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function getPDateAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }


    public function getFNameAttribute()
    {
        return $this->f->name;
    }

    public function getProtocolNameAttribute()
    {
        return $this->f_name . ' / ' . $this->id . ' / ' . str_replace('/','-',$this->p_date);
    }

    public function getIsNullAttribute()
    {
        if ($this->type == 9){
            return true;
        }else{
            return false;
        }
    }

    public function getYearAttribute()
    {
        return Carbon::createFromFormat('d/m/Y',$this->p_date)->format('Y');
    }

    public function getNowAttribute()
    {
        return Carbon::now()->format('Y');
    }
}
