<?php

namespace Pasifai\Pysde\models;

use App\School;
use Illuminate\Database\Eloquent\Model;

class OrganikiPrefrences extends Model
{
    protected $table = 'organikes_prefrences';

    public  $timestamps = false;

    protected $fillable = [
        'request_id',
        'order_number',
        'school_id'
    ];


    protected $appends = [
        'OrganikiName',
        'OrganikiOrder',
        'OrganikiGroup'
    ];

    public function request()
    {
        return $this->belongsTo(OrganikiRequest::class,'request_id','id');
    }

    public function getOrganikiNameAttribute()
    {
        return School::find($this->school_id)->name;
    }

    public function getOrganikiOrderAttribute()
    {
        return School::find($this->school_id)->order;
    }

    public function getOrganikiGroupAttribute()
    {
        return School::find($this->school_id)->group_id;
    }
}
