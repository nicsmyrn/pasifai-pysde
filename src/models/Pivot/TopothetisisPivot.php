<?php

namespace Pasifai\Pysde\models\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Carbon\Carbon;

class TopothetisisPivot extends Pivot
{
    public function setDateEndsAttribute($date){
        if($date == null){
            $this->attributes['date_ends'] = null;
        }else{
            $this->attributes['date_ends'] = Carbon::createFromFormat('d/m/Y', $date);
        }
    }

    public function getDateEndsAttribute($date)
    {
        if($date == null){
            return null;
        }
        return Carbon::parse($date)->format('d/m/Y');
    }

}
