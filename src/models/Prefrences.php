<?php

namespace Pasifai\Pysde\models;

use Illuminate\Database\Eloquent\Model;

class Prefrences extends Model
{
    protected $table = 'prefrences';

    protected $fillable = [
        'request_id',
        'order_number',
        'school_name'
    ];

    public function request()
    {
        return $this->belongsTo(RequestTeacher::class,'request_id','id');
    }
}
