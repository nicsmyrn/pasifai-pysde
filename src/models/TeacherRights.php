<?php

namespace Pasifai\Pysde\models;

use App\MySchoolTeacher;
use Illuminate\Database\Eloquent\Model;

class TeacherRights extends Model
{
    protected $table = 'teacher_rights';

    protected $primaryKey = 'afm';
    
    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'year',
        'afm',
        'onomastika_yperarithmos',
        'dikaiwma_veltiwsis',
        'description'
    ];

    public function myschool()
    {
        return $this->belongsTo(MySchoolTeacher::class, 'afm', 'afm');
    }


}
