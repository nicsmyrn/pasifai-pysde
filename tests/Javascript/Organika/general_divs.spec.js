import Vue from 'vue';
import { mount } from '@vue/test-utils';

import Lyceum from './../../../src/assets/components/Organika/TableGeneralDivision.vue';
import expectExport from 'expect';

describe('ΠΙΝΑΚΑΣ ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ', () => {
    let wrapper;

    beforeEach(() => {

        wrapper = mount(Lyceum, {
            propsData : {
                generalDivisionData : [
                    {"div_id":19,"name":"ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ","class":"Α","numberSchoolProvide":2,"number":2,"status":"unlocked","students":27},
                    {"div_id":20,"name":"ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ","class":"Β","numberSchoolProvide":2,"number":3,"status":"unlocked","students":29},
                    {"div_id":21,"name":"ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ","class":"Γ","numberSchoolProvide":2,"number":2,"status":"unlocked","students":55}
                ],
                max : 27
            }
        });
    });

    it('display table', () => {
        see('ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ');
    });

    it('check if number of students in table has integer numbers', () => {
        inputIsInteger('studentsΑ');
        inputIsInteger('studentsΒ');
        inputIsInteger('studentsΓ');

        inputIsInteger('divΑ');
        inputIsInteger('divΒ');
        inputIsInteger('divΓ');

    });

    it('students must be an integer', async() => {

        type(23, 'input[name=studentsΑ]');

        await Vue.nextTick();

        keyUp('input[name=studentsΑ]');

        expectToBe('students', 23);
    });

    it('students cannot be any other symbol except integer', async() => {

        type("a23", 'input[name=studentsΑ]');

        await Vue.nextTick();

        keyUp('input[name=studentsΑ]');

        expectToBe('students', null);
    });

    it('checks if message error NOT displayd when putting number', async() => {
        type(23, 'input[name=studentsΑ]');

        await Vue.nextTick();

        keyUp('input[name=studentsΑ]');

        notTriggerEvent('display-alert');
    });

    it('checks if message error displayed when putting symbols in input', async() => {
        type("a23", 'input[name=studentsΑ]');

        await Vue.nextTick();

        keyUp('input[name=studentsΑ]');

        expectEvent('display-alert');
    });

    it('divisions must be 1 when students are less than 27', async() => {
        type(23, 'input[name=studentsΑ]');

        await Vue.nextTick();

        keyUp('input[name=studentsΑ]');

        expectToBe('numberSchoolProvide', 1);
    });

    it('divisions must be 2 when students are 28', async() => {
        type(28, 'input[name=studentsΑ]');

        await Vue.nextTick();

        keyUp('input[name=studentsΑ]');

        expectToBe('numberSchoolProvide', 2);
    });

    it('divisions must be null when students input is symbols', async() => {
        type("adf", 'input[name=studentsΑ]');

        await Vue.nextTick();

        keyUp('input[name=studentsΑ]');

        expectToBe('numberSchoolProvide', null);
    });

    it('divisions must be an integer', async() => {
        type(2, 'input[name=divΑ]');

        await Vue.nextTick();

        keyUp('input[name=divΑ]');

        expectToBe('numberSchoolProvide', 2);
    });

    it('divisions must be null when inserting symbols', async() => {
        type("adsfa", 'input[name=divΑ]');

        await Vue.nextTick();

        keyUp('input[name=divΑ]');

        expectToBe('numberSchoolProvide', null);
    });

    it('checks if message error displayed when putting symbols in divisions input field', async() => {
        type("a23", 'input[name=divΑ]');

        await Vue.nextTick();

        keyUp('input[name=divΑ]');

        expectEvent('display-alert');
    });

    it('checks if message error NOT displayed when putting number in divisions input field', async() => {
        type(2, 'input[name=divΑ]');

        await Vue.nextTick();

        keyUp('input[name=divΑ]');

        notTriggerEvent('display-alert');
    });

    let expectToBe = (data, value) => {
        expect(wrapper.vm['generalDivisionData'][0][data]).toBe(value);
    }

    let inputIsInteger = (name) => {
        expect(wrapper.find(`input[name=${name}]`).element.value).toMatch(/^[1-9][0-9]*$/);
    }

    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;
    
        expect(wrap.html()).toContain(text);
    }

    let type = (value, selector) => {
        let node = wrapper.find(selector);
        
        node.element.value = value;
        node.trigger('input');
    }

    let keyUp = selector => {
        wrapper.find(selector).trigger('keyup');
    }

    let expectEvent = (event) => {
        expect(wrapper.emitted()[event]).toBeTruthy();
    }


    let notTriggerEvent = (event) => {
        expect(wrapper.emitted()[event]).toBeFalsy();
    }
});

