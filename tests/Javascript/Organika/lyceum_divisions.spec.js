import { mount } from '@vue/test-utils';
import Vue from 'vue';

import Lyceum from './../../../src/assets/OrganikaLyceum.vue';

import moxios from 'moxios';

describe('Lyceum Organika', () => {
    let wrapper;

    beforeEach(() => {
        moxios.install();

        wrapper = mount(Lyceum, {
            propsData : {
                hideLoader : false,
                token : 'sdgasdghasdj'
            }
        });
    });

    afterEach(() => {
        moxios.uninstall();
    });

    it('loader display before fetching data', () => {
        see('παρακαλώ περιμένετε...');
    });

    it('display all tables vacuum for lyceum organika', async () => {
        moxios.stubRequest(`/api/school/organika/lyceum/fetchDivisions?api_token=${wrapper.vm.token}`, {
            status : 200,
            response : {"list_divisions":[{"id":32,"name":"\u0393\u0391\u039b\u039b\u0399\u039a\u0391","class":"\u0391","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"foreign"},{"id":33,"name":"\u0393\u0391\u039b\u039b\u0399\u039a\u0391","class":"\u0392","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"foreign"},{"id":35,"name":"\u0393\u0395\u03a1\u039c\u0391\u039d\u0399\u039a\u0391","class":"\u0391","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"foreign"},{"id":36,"name":"\u0393\u0395\u03a1\u039c\u0391\u039d\u0399\u039a\u0391","class":"\u0392","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"foreign"},{"id":39,"name":"[\u0395\u03a0\u0399\u039b.] \u0395\u03a5\u03a1\u03a9\u03a0\u0391\u0399\u039a\u039f\u03a3 \u03a0\u039f\u039b.","class":"\u0391","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"choice"},{"id":40,"name":"[\u0395\u03a0\u0399\u039b.] \u0395\u0399\u039a\u0391\u03a3\u03a4\u0399\u039a\u0391","class":"\u0391","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"choice"},{"id":42,"name":"[\u0395\u03a0\u0399\u039b.] \u0398\u0395\u0391\u03a4\u03a1\u039f\u039b\u039f\u0393\u0399\u0391","class":"\u0391","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"choice"},{"id":43,"name":"[\u0395\u03a0\u0399\u039b.] \u0393\u039b\u03a9\u03a3\u03a3\u0391 \u0393\u0391\u039b\u039b\u0399\u039a\u0391","class":"\u0393","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"choice"},{"id":44,"name":"[\u0395\u03a0\u0399\u039b.] \u0393\u039b\u03a9\u03a3\u03a3\u0391 \u0393\u0395\u03a1\u039c\u0391\u039d\u0399\u039a\u0391","class":"\u0393","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"choice"},{"id":46,"name":"[\u0395\u03a0\u0399\u039b.] \u0393\u03a1\u0391\u039c\u039c\u0399\u039a\u039f \u03a3\u03a7\u0395\u0394\u0399\u039f","class":"\u0393","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"choice"},{"id":53,"name":"[\u0395\u03a0\u0399\u039b.] \u039b\u0391\u03a4\u0399\u039d\u0399\u039a\u0391","class":"\u0393","school_type":"lyk","school_group":"\u0397\u039c\u0395\u03a1\u0399\u03a3\u0399\u039f \u039b\u03a5\u039a\u0395\u0399\u039f","type":"choice"}],"list_projects":[{"id":2,"slug":"\u03a0\u039502","name":"\u03a6\u0399\u039b\u039f\u039b\u039f\u0393\u039f\u0399"},{"id":3,"slug":"\u03a0\u039503","name":"\u039c\u0391\u0398\u0397\u039c\u0391\u03a4\u0399\u039a\u039f\u0399"},{"id":4,"slug":"\u03a0\u039504.01","name":"\u03a6\u03a5\u03a3\u0399\u039a\u039f\u0399"},{"id":7,"slug":"\u03a0\u039504.04","name":"\u0392\u0399\u039f\u039b\u039f\u0393\u039f\u0399"},{"id":22,"slug":"\u03a0\u039578","name":"\u039a\u039f\u0399\u039d\u03a9\u039d\u0399\u039a\u03a9\u039d \u0395\u03a0\u0399\u03a3\u03a4\u0397\u039c\u03a9\u039d"}],"divisions_general":[{"div_id":19,"name":"\u0393\u0395\u039d\u0399\u039a\u0397\u03a3 \u03a0\u0391\u0399\u0394\u0395\u0399\u0391\u03a3","class":"\u0391","numberSchoolProvide":2,"number":1,"status":"unlocked","students":27},{"div_id":20,"name":"\u0393\u0395\u039d\u0399\u039a\u0397\u03a3 \u03a0\u0391\u0399\u0394\u0395\u0399\u0391\u03a3","class":"\u0392","numberSchoolProvide":2,"number":2,"status":"unlocked","students":29},{"div_id":21,"name":"\u0393\u0395\u039d\u0399\u039a\u0397\u03a3 \u03a0\u0391\u0399\u0394\u0395\u0399\u0391\u03a3","class":"\u0393","numberSchoolProvide":2,"number":2,"status":"unlocked","students":30}],"divisions_foreign":{"\u0391":[{"div_id":29,"name":"\u0391\u0393\u0393\u039b\u0399\u039a\u0391","class":"\u0391","numberSchoolProvide":2,"number":1,"status":"locked","students":27}],"\u0392":[{"div_id":30,"name":"\u0391\u0393\u0393\u039b\u0399\u039a\u0391","class":"\u0392","numberSchoolProvide":1,"number":2,"status":"locked","students":29}]},"divisions_direction":{"\u0392":[{"div_id":24,"name":"\u0391\u039d\u0398\u03a1\u03a9\u03a0\u0399\u03a3\u03a4\u0399\u039a\u03a9\u039d \u03a3\u03a0\u039f\u03a5\u0394\u03a9\u039d","class":"\u0392","numberSchoolProvide":1,"number":1,"status":"locked","students":16},{"div_id":25,"name":"\u0398\u0395\u03a4\u0399\u039a\u03a9\u039d \u03a3\u03a0\u039f\u03a5\u0394\u03a9\u039d","class":"\u0392","numberSchoolProvide":1,"number":1,"status":"locked","students":13}],"\u0393":[{"div_id":26,"name":"\u0391\u039d\u0398\u03a1\u03a9\u03a0\u0399\u03a3\u03a4\u0399\u039a\u03a9\u039d \u03a3\u03a0\u039f\u03a5\u0394\u03a9\u039d","class":"\u0393","numberSchoolProvide":1,"number":1,"status":"locked","students":10},{"div_id":27,"name":"\u0398\u0395\u03a4\u0399\u039a\u03a9\u039d \u03a3\u03a0\u039f\u03a5\u0394\u03a9\u039d","class":"\u0393","numberSchoolProvide":1,"number":1,"status":"locked","students":3},{"div_id":28,"name":"\u039f\u0399\u039a\u039f\u039d\u039f\u039c\u0399\u0391\u03a3 & \u03a0\u039b\u0397\u03a1\u039f\u03a6.","class":"\u0393","numberSchoolProvide":1,"number":1,"status":"locked","students":12},{"div_id":52,"name":"\u03a3\u03a0\u039f\u03a5\u0394\u03a9\u039d \u03a5\u0393\u0395\u0399\u0391\u03a3","class":"\u0393","numberSchoolProvide":1,"number":1,"status":"locked","students":5}]},"divisions_choice":{"\u0391":[{"div_id":22,"name":"[\u0395\u03a0\u0399\u039b.] \u0395\u03a6\u0391\u03a1\u039c. \u03a0\u039b\u0397\u03a1\u039f\u03a6.","class":"\u0391","numberSchoolProvide":1,"number":1,"status":"locked","students":16},{"div_id":38,"name":"[\u0395\u03a0\u0399\u039b.] \u0394\u0399\u0391\u03a7\u0395\u0399\u03a1\u0399\u03a3\u0397 \u03a6\u03a5\u03a3. \u03a0\u039f\u03a1\u03a9\u039d","class":"\u0391","numberSchoolProvide":1,"number":1,"status":"locked","students":7},{"div_id":41,"name":"[\u0395\u03a0\u0399\u039b.] \u039c\u039f\u03a5\u03a3\u0399\u039a\u0397","class":"\u0391","numberSchoolProvide":1,"number":1,"status":"locked","students":4}],"\u0393":[{"div_id":23,"name":"[\u0395\u03a0\u0399\u039b.] \u0393\u039b\u03a9\u03a3\u03a3\u0391 \u0391\u0393\u0393\u039b\u0399\u039a\u0391","class":"\u0393","numberSchoolProvide":1,"number":1,"status":"locked","students":24},{"div_id":45,"name":"[\u0395\u03a0\u0399\u039b.] \u0395\u039b\u0395\u03a5\u0398\u0395\u03a1\u039f \u03a3\u03a7\u0395\u0394\u0399\u039f","class":"\u0393","numberSchoolProvide":1,"number":1,"status":"locked","students":6}]},"students":{"\u0391":27,"\u0392":29,"\u0393":30},"divisions_projects":[{"eid_id":10,"name":"\u0391\u0393\u0393\u039b\u0399\u039a\u0397\u03a3","slug":"\u03a0\u039506","hours":2},{"eid_id":26,"name":"\u03a0\u039f\u039b. \u039c\u0397\u03a7\u0391\u039d\u0399\u039a\u03a9\u039d - \u0391\u03a1\u03a7\u0399\u03a4\u0395\u039a\u03a4\u039f\u039d\u03a9\u039d","slug":"\u03a0\u039581","hours":4}]}
        });

        moxios.wait(()=> {
            see('ΤΜΗΜΑΤΑ ΓΕΝΙΚΗΣ ΠΑΙΔΕΙΑΣ');
            see("ΠΡΟΣΑΝΑΤΟΛΙΣΜΟΣ Β' ΛΥΚΕΙΟΥ");
            see("ΠΡΟΣΑΝΑΤΟΛΙΣΜΟΣ Γ' ΛΥΚΕΙΟΥ");
            see("ΠΡΟΣΑΝΑΤΟΛΙΣΜΟΣ Γ' ΛΥΚΕΙΟΥ");
            see("ΞΕΝΕΣ ΓΛΩΣΣΕΣ Α' ΛΥΚΕΙΟΥ");
            see("ΞΕΝΕΣ ΓΛΩΣΣΕΣ Α' ΛΥΚΕΙΟΥ");
            see("ΞΕΝΕΣ ΓΛΩΣΣΕΣ Γ' ΛΥΚΕΙΟΥ");
            see("ΕΠΙΛΟΓΗΣ Α' ΛΥΚΕΙΟΥ");
            see("ΕΠΙΛΟΓΗΣ Γ' ΛΥΚΕΙΟΥ");
            see("Δήλωση Projects");
        });
    });

    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;
    
        expect(wrap.html()).toContain(text);
    }
});

