import Vue from 'vue';
import { shallowMount } from '@vue/test-utils';

import Question from '../../src/assets/components/Tests/Question.vue';
import moxios from 'moxios';

describe('Question', () => {
    let wrapper;

    beforeEach(() => {
        moxios.install();

        wrapper = shallowMount(Question, {
            propsData : {
                dataQuestion : {
                    title : 'The title',
                    body : 'The body'
                }
            }
        });
    });

    afterEach(() => {
        moxios.uninstall();
    });

    it('presents the title and the body', () => {  
        see('The body');
        see('The title');      
    });

    it('can be edited', async() => {
        wrapper.find('#edit').trigger('click');

        await Vue.nextTick();

        expect(wrapper.find('input[name=title]').element.value).toBe('The title');
        expect(wrapper.find('textarea[name=body]').element.value).toBe('The body');
    });

    it('hides the edit button during editing', async()=> {
        wrapper.find('#edit').trigger('click');

        await Vue.nextTick();

        expect(wrapper.contains('#edit')).toBe(false);
    });

    it('updates the question after editing', async() => {

        click('#edit');

        await Vue.nextTick();

        type('Changed title', 'input[name=title]');
        type('Changed body', 'textarea[name=body]');

        moxios.stubRequest(/questions\/\d+/, {
            status : 200,
            response : {
                title : 'Changed title',
                body : 'Changed body'
            }
        });

        click('#update');

        moxios.wait(()=> {
            see('Your question has been updated.');
            // done();
        });

        await Vue.nextTick();

        see('Changed title');
        see('Changed body');
    });

    it('can cancel out of edit mode', async () => {
        click('#edit');

        await Vue.nextTick();

        type('Changed title', 'input[name=title]');
        type('Changed body', 'textarea[name=body]');

        click('#cancel');

        await Vue.nextTick();
        
        see('The title');
        see('The body');
    });

    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;

        expect(wrap.html()).toContain(text);
    }

    let type = (value, selector) => {
        let node = wrapper.find(selector);
        
        node.element.value = value;
        node.trigger('input');
    }

    let click = selector => {
        wrapper.find(selector).trigger('click');
    }



});