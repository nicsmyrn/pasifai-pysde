import Vue from 'vue';
import { mount } from '@vue/test-utils';

import Countdown from '../../src/assets/components/Tests/Countdown.vue';
import moxios from 'moxios';
import moment from 'moment';
import sinon from 'sinon';


describe('Countdown', () => {
    let wrapper, clock;

    beforeEach(() => {
        // moxios.install();

        clock = sinon.useFakeTimers({
            now : 1483228800000
        });

        wrapper = mount(Countdown, {
            propsData : {
                until : moment().add(10, 'seconds'),
                expiredText : 'Contest is over'
            }
        });
    });

    afterEach(() => {
        moxios.uninstall();
        clock.restore();
    });

    it('renders a countdown timer', () => {

        see('10 Seconds');
    });

    it('reduces the countdown every second', async () => {

        see('10 Seconds');

        clock.tick(1000);

        await Vue.nextTick();

        see('9 Seconds');
    });

    it('shows an expired message when the countdown has compledted', async () => {
        see('10 Seconds');

        clock.tick(10000);

        await Vue.nextTick();

        see('Contest is over');
    });

    it('it broadcasts when the countdown is finishded', async () => {
        clock.tick(10000);

        await Vue.nextTick();

        expectEvent('finished');
        see('Contest is over');
 
    });

    it('clears the interval onces completed', async() => {
        clock.tick(10000);

        expect(wrapper.vm.now.getSeconds()).toBe(10);

        await Vue.nextTick();

        clock.tick(5000);
           
        expect(wrapper.vm.now.getSeconds()).toBe(10);
    });


    let expectEvent = (event) => {
        expect(wrapper.emitted()[event]).toBeTruthy();
    }

    let see = (text, selector) => {
        let wrap = selector ? wrapper.find(selector) : wrapper;
    
        expect(wrap.html()).toContain(text);
    }
    
    let type = (value, selector) => {
        let node = wrapper.find(selector);
        
        node.element.value = value;
        node.trigger('input');
    }
    
    let click = selector => {
        wrapper.find(selector).trigger('click');
    }
});

