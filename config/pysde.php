<?php

return array(
    'middleware' => ['auth'],

    'aitisis_E2' => env('AITISEIS_E2', '2020-11-16 00:00:00'),

    'date_ends' => env('LEITOURGIKA_DATE_ENDS', '30/06/2024'),

    'after_days_for_adeies_leitourgika' => env('LEITOURGIKA_AFTER_DAYS_ADEIES', 90),

    'offline_organika' => env('ORGANIKA_OFFLINE', true),
    'offline_leitourgika' => env('LEITOURGIKA_OFFLINE', true),
    'offline_leitourgika_eidikotites' => env('LEITOURGIKA_OFFLINE_EIDIKOTITES', true),
    'offline_leitourgika_teachers' => env('LEITOURGIKA_OFFLINE_TEACHERS', true),

    'aitisis_diathesis' => env('AITISEIS_DIATHESIS', '2040-09-02'),
    'start_aitisis_veltiwsis' => env('AITISIS_VELTIWSIS', '2020-07-06'),

    'organiki_hours' => env('organiki_hours', 20),
    'manual_organiki_hours' => env('manual_organiki_hours', false),

    'prefrences_offset' => env('PREFRENCES_OFFSET', 10),

    'guard' => 'web',

    'apospaseis_ypourgeiou' => [
        "2020-2021" => [
            '93856/Ε2 16-7-2020'  => 'Αποσπάσεις εκπαιδευτικών Δ.Ε. από ΠΥΣΔΕ σε ΠΥΣΔΕ για το διδακτικό έτος 2020-2021',
            '117488/Ε2 8-9-2020'  => 'Τροποποιήσεις αποσπάσεων και αποσπάσεις από ΠΥΣΔΕ σε ΠΥΣΔΕ εκπαιδευτικών Δευτεροβάθμιας Εκπαίδευσης για το διδακτικό έτος 2020-2021',
            '131535/Ε2 1-10-2020'  => 'Τροποποιήσεις αποσπάσεων και αποσπάσεις από ΠΥΣΔΕ σε ΠΥΣΔΕ εκπαιδευτικών Δευτεροβάθμιας Εκπαίδευσης για το διδακτικό έτος 2020-2021'
        ]
    ],

    'information_name'  =>  env('INFORMATION_NAME', 'Νικόλαος Σμυρναίος'),
    'administrator_name'    => env('ADMINISTRATOR_NAME', 'Ζερβάκης Γ. Στυλιανός'),
    'administrator_job_title' => env('ADMINISTRATOR_JOB_TITLE', 'Διοίκησης Επιχειρήσεων'),

    'MAIL_PYSDE' => env('MAIL_PYSDE', 'nicsmyrn@gmail.com'),
    'NAME_PYSDE' => env('NAME_PYSDE', 'Γραμματεία ΠΥΣΔΕ Χανίων'),

    'wrario'    => [
        7300    => 18,
        4380    => 20,
        2190    => 21,
        0       => 23
    ],

    'display_old_placements' => env('OLD_PLACEMENTS', true),

    'max_wrario' => 18,

    'old_wrario_date' => '2013-09-01',

    'date_of_new_placements' => '2018-04-19',

    'praxi_type' => array(
        11 => 'Πράξη ΠΥΣΔΕ',
        12 => 'Πράξη Διευθυντή',
        13 => 'Απόφαση Περιφερειακού Δ/ντή'
    ),

    'topothetisis_types'  => array(
        0 => 'Οργανικά',
        1 => 'Απόσπαση σε Φορείς',
        2 => 'Απόσπαση σε άλλο ΠΥΣΔΕ/ΠΥΣΠΕ',
        3 => 'Απόσπαση σε Σχολική Μονάδα εντός Νομού/Μουσικό',
        4 => 'Διάθεση στην πρωτοβάθμια',
        5 => 'Προσωρινή τοποθέτηση',
        6 => 'συμπλήρωση',
        7 => 'Άδεια ανευ αποδοχών',
        8 => 'Άδεια μακροχρόνια (>10 ημέρες)',
        9 => 'Επί Θητεία',
        10 => 'Διευθυντής στη Σχολική Μονάδα',
        11 => 'Απόσπαση στη ΔΔΕ Χανίων',
        12 => 'εξ ολοκλήρου γραμματειακή Υποστήριξη',
        13 => 'Άδεια ανατροφής'
    ),


    'can_edit_hours' => env('SCHOOL_CAN_EDIT_TEACHER_HOURS', false),
    'can_edit_hours_eidikotites' => false,
    'school_can_edit_hours_eidikotites' => env('SCHOOL_CAN_EDIT_EIDIKOTITES', 'can_edit'),

    'can_view_organika_anikontes' => env('VIEW_ORGANIKA_ANIKONTES', false),
    'can_view_organika_kena' => env('VIEW_ORGANIKA_KENA', false),

    'countdown' => [
        'count_down_for_schools_leitourgika' => env('COUNTDOWN_SCHOOLS_LEITOURGIKA', 'September 10 2020 12:29'),
        'set_countdown' => env('COUNTDOWN_IS_ON', false),

        'countdown_for_organika_teachers' => env('COUNTDOWN_ORGANIKA_DATE', 'May 2 2024 23:00'),
        'set_countdown_organika_anikontes' => env('COUNTDOWN_ORGANIKA_ANIKONTES', true),
        
        'count_down_for_schools_eidikotites' => env('COUNTDOWN_SCHOOLS_EIDIKOTITES', 'September 10 2020 12:29'),
        'set_countdown_eidikotites' => env('COUNTDOWN_IS_ON_EIDIKOTITES', false)
    ],

    'placements_type' => array(
        1  => 'προσωρινή τοποθέτηση',      // ΔΕΝ ΑΛΛΑΖΕΙ ΤΟ ΝΟΥΜΕΡΟ
        2  => 'απόσπαση (με αίτηση)',
        3  => 'συμπλήρωση ωραρίου',
        4  => 'ολική διάθεση',
        5 =>  'τροποποίηση συμπλήρωσης ωραρίου',
        6  => 'τροποποίηση προσωρινής τοποθέτησης', // ΔΕΝ ΑΛΛΑΖΕΙ ΤΟ ΝΟΥΜΕΡΟ
        7  => 'τροποποίηση απόσπασης',
        8  => 'διάθεση στην Π/θμια',
        9  => 'ανάκληση τοποθέτησης'
    ),

    'link_topothetisis_with_placements_from' => [
        1 => 3, // προσωρινή τοποθέτηση
        2 => 3, // απόσπαση
        4 => 3, 
        6 => 3,
        7 => 3,
        8 => 4
    ],

    'link_topothetisis_with_placements_to' => [
        1 => 5, // προσωρινή τοποθέτηση
        2 => 5, // απόσπαση
        3 => 6, // συμπλήρωση
        4 => 5,
        5 => 6,
        6 => 5,
        7 => 5
    ],

    'teacher_types_available_for_school' => [0,5,6,1,2,7,8,10,13],  // 10 dieuthidis sti sxoliki monada

    'teacher_types_that_is_in_school' => [0,5,6,10],

    'omores_omades' => [
        1 => [8,4,5],
        2 => [8,4,5],
        3 => [8,4,5],
        4 => [10,3, 12],
        5 => [10,3, 12],
        6 => [10,3, 12],
        7 => [10,3, 12],
        8 => [14, 2],
        9 => [14, 2],
        10 => [12, 13, 6, 7],
        11 => [9,4],
        12 => [10, 6],
        13 => [10, 6],
        14 => [8,9]
    ],

    'sistegazomena' => [
        [25, 27, 49],  // 3 Γ/σιο, 4 Γ/σιο , 3 ΓΕΛ
        [5, 46], //εσπερινό γυμσιο , 6ο
        [16, 14], // εσπερινο γελ , 4ο γελ
        [48, 9],  // εσπερινο επαλ, 2 επαλ
        [4, 40, 13], // εεεεκ, επαλ ακρωτηρίου, γελ ακρωτηρίου
        [33, 19], // γυμσιο αλικινακου, γελ αλικιανου
        [42, 21], // γ/σιο βουκολιων, γελ βουκολιων
        [2, 18], // γ/σιο κολυμβαριου , γελ κολυμβαριου
        [43, 20], //επαλ κισαμου, γελ κισαμου
        [23, 7], //γ/σιο καντανου, επαλ καντανου
        [26, 3], // γ/σιο παλαιοχωρας, γελ παλαιοχωρας
        [28, 17], // γ/σιο βαμου, γελ βαμου
        [6, 34], // γ/σιο βρυσων, επαλ βρυσων
        [31, 15], // γ/σιο σφακιων, γελ σφακιων
        [32, 52], // γ/σιο πλατανια , εσπερινο επαλ πλατανια
        [24, 22], // 5 γ/σιο , 1 γελ
        [41, 11, 8], // 2 γ/σιο , 2 γελ, 1 επαλ
    ],

    'special_placements' => array(
        10001 => 'ΔΔΕ Χανίων',
        10002 => 'Τμήματα για ΤΕΦΦΑ & Στρατιωτικές Σχολες',
        10003 => 'Διάθεση στην Πρωτοβάθμια',
        10004 => 'ΕΚΦΕ Χανίων',
        10005 => 'Βιβλιοθήκη',
        10006 => 'Πρόγραμμα Κολύμβησης',
        10007 => 'Ε.Κ.',
        10008 => 'ΕΠΟΠΤΕΙΑ ΜΑΘΗΤΕΙΑΣ',
        10009 => 'Γραμματειακή Υποστήριξη (στην οργανική)',
        10010 => 'Απόσπαση σε άλλο Πυσδε/Πυσπε'
    ),

    'teacher_types'     => array(
        1000 => 'Διάθεση ΠΥΣΔΕ',
        2000 => 'Απόσπαση από άλλο ΠΥΣΔΕ',
        3000 => 'Αναπληρωτής',
        4000 => 'Διάθεση από Π/βθμια'
    ),

    'aitisis_type' => array(
        'ΑΠΛΗ',
        'E1',
        'E2',
        'E3',
        'E4',
        'E5',
        'E6',
        'E7',
        'ΟργανικήΥπ',
        'Βελτίωση',
        'Αίτηση-Μοριοδότησης-Μετάθεσης',
        'Αίτηση-Μοριοδότησης-Απόσπασης',
        'Υπεραρ'
    ),

    'reason' => array(
        'Είμαι Λειτουργικά Υπεράριθμος',
        'Ήρθα φέτος με μετάθεση από άλλο ΠΥΣΔΕ',
        'Είμαι στη Διάθεση του ΠΥΣΔΕ'
    ),

);